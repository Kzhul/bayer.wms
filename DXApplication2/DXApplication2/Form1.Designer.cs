﻿namespace DXApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.txtDocumentCodeSearch = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dtpFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dtpToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnView = new DevExpress.XtraEditors.SimpleButton();
            this.btnExportExcel = new DevExpress.XtraEditors.SimpleButton();
            this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatusBarMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.dtgDocument = new System.Windows.Forms.DataGridView();
            this.btnOpenFormInput = new DevExpress.XtraEditors.SimpleButton();
            this.colDocumentBarCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceivedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocumentCodeSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDocument)).BeginInit();
            this.SuspendLayout();
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 1;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.skinRibbonGalleryBarItem1});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 2;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.QuickToolbarItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl1.Size = new System.Drawing.Size(1181, 49);
            // 
            // txtDocumentCodeSearch
            // 
            this.txtDocumentCodeSearch.Location = new System.Drawing.Point(61, 65);
            this.txtDocumentCodeSearch.MenuManager = this.ribbonControl1;
            this.txtDocumentCodeSearch.Name = "txtDocumentCodeSearch";
            this.txtDocumentCodeSearch.Size = new System.Drawing.Size(210, 20);
            this.txtDocumentCodeSearch.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 69);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(43, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Mã hồ sơ";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 95);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(25, 13);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Ngày";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.EditValue = null;
            this.dtpFromDate.Location = new System.Drawing.Point(61, 92);
            this.dtpFromDate.MenuManager = this.ribbonControl1;
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFromDate.Size = new System.Drawing.Size(100, 20);
            this.dtpFromDate.TabIndex = 6;
            // 
            // dtpToDate
            // 
            this.dtpToDate.EditValue = null;
            this.dtpToDate.Location = new System.Drawing.Point(167, 92);
            this.dtpToDate.MenuManager = this.ribbonControl1;
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpToDate.Size = new System.Drawing.Size(104, 20);
            this.dtpToDate.TabIndex = 7;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(286, 64);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(104, 49);
            this.btnView.TabIndex = 9;
            this.btnView.Text = "Xem (F1)";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportExcel.Location = new System.Drawing.Point(1055, 64);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(122, 23);
            this.btnExportExcel.TabIndex = 10;
            this.btnExportExcel.Text = "Xuất Excel (Ctrl + P)";
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusBarMessage});
            this.statusStrip1.Location = new System.Drawing.Point(0, 635);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1181, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatusBarMessage
            // 
            this.lblStatusBarMessage.Name = "lblStatusBarMessage";
            this.lblStatusBarMessage.Size = new System.Drawing.Size(0, 17);
            // 
            // dtgDocument
            // 
            this.dtgDocument.AllowUserToAddRows = false;
            this.dtgDocument.AllowUserToDeleteRows = false;
            this.dtgDocument.AllowUserToOrderColumns = true;
            this.dtgDocument.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgDocument.AutoGenerateColumns = false;
            this.dtgDocument.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDocument.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDocumentBarCode,
            this.colPage,
            this.colSender,
            this.colDescription,
            this.colReceivedDate,
            this.colNone});
            this.dtgDocument.DataSource = this.bindingSource;
            this.dtgDocument.Location = new System.Drawing.Point(3, 119);
            this.dtgDocument.Name = "dtgDocument";
            this.dtgDocument.Size = new System.Drawing.Size(1174, 513);
            this.dtgDocument.TabIndex = 14;
            this.dtgDocument.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgDocument_CellEndEdit);
            // 
            // btnOpenFormInput
            // 
            this.btnOpenFormInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenFormInput.Location = new System.Drawing.Point(1055, 93);
            this.btnOpenFormInput.Name = "btnOpenFormInput";
            this.btnOpenFormInput.Size = new System.Drawing.Size(122, 23);
            this.btnOpenFormInput.TabIndex = 16;
            this.btnOpenFormInput.Text = "Mở màn hình nhập";
            this.btnOpenFormInput.Click += new System.EventHandler(this.btnOpenFormInput_Click);
            // 
            // colDocumentBarCode
            // 
            this.colDocumentBarCode.DataPropertyName = "DocumentBarCode";
            this.colDocumentBarCode.HeaderText = "Mã hồ sơ";
            this.colDocumentBarCode.Name = "colDocumentBarCode";
            this.colDocumentBarCode.ReadOnly = true;
            this.colDocumentBarCode.Width = 150;
            // 
            // colPage
            // 
            this.colPage.DataPropertyName = "Page";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colPage.DefaultCellStyle = dataGridViewCellStyle2;
            this.colPage.HeaderText = "Số trang";
            this.colPage.Name = "colPage";
            this.colPage.ReadOnly = true;
            // 
            // colSender
            // 
            this.colSender.DataPropertyName = "Sender";
            this.colSender.HeaderText = "Người gửi";
            this.colSender.Name = "colSender";
            this.colSender.ReadOnly = true;
            this.colSender.Width = 150;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "Description";
            this.colDescription.HeaderText = "Ghi chú";
            this.colDescription.Name = "colDescription";
            this.colDescription.Width = 200;
            // 
            // colReceivedDate
            // 
            this.colReceivedDate.DataPropertyName = "strReceivedDate";
            this.colReceivedDate.HeaderText = "Ngày nhập";
            this.colReceivedDate.Name = "colReceivedDate";
            this.colReceivedDate.ReadOnly = true;
            this.colReceivedDate.Width = 150;
            // 
            // colNone
            // 
            this.colNone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colNone.HeaderText = "";
            this.colNone.Name = "colNone";
            // 
            // Form1
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 657);
            this.Controls.Add(this.btnOpenFormInput);
            this.Controls.Add(this.dtgDocument);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnExportExcel);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtDocumentCodeSearch);
            this.Controls.Add(this.ribbonControl1);
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.Name = "Form1";
            this.Ribbon = this.ribbonControl1;
            this.Text = "Quản trị";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDocumentCodeSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDocument)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraEditors.TextEdit txtDocumentCodeSearch;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dtpFromDate;
        private DevExpress.XtraEditors.DateEdit dtpToDate;
        private DevExpress.XtraEditors.SimpleButton btnView;
        private DevExpress.XtraEditors.SimpleButton btnExportExcel;
        private System.Windows.Forms.BindingSource bindingSource;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView dtgDocument;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusBarMessage;
        private DevExpress.XtraEditors.SimpleButton btnOpenFormInput;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDocumentBarCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSender;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceivedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNone;
    }
}

