﻿using Bayer.WMS.Objs;
using BayerDocumentManagement;
using Microsoft.Office.Interop.Excel;
using ReadWriteCsv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Configuration;

namespace DXApplication2
{
    public partial class Form1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public Form1()
        {
            InitializeComponent();
            //ribbonControl1.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Program.fileName = System.Configuration.ConfigurationSettings.AppSettings["DataPath"].ToString();
            System.IO.FileInfo fl = new System.IO.FileInfo(Program.fileName);
            if (!fl.Exists)
            {
                fl.Create();
            }


            Program.OpenFormInput();


            txtDocumentCodeSearch.Select();
            dtpFromDate.Text = DateTime.Today.AddDays(-7).ToShortDateString();
            dtpToDate.Text = DateTime.Today.ToShortDateString();

            LoadExcel();
            LoadView();

            try
            {
                this.Icon = new System.Drawing.Icon(System.Windows.Forms.Application.StartupPath + "\\Bayer.ico");
            }
            catch
            {

            }
        }

        public static void SaveExcel(DocumentEntity documentToSave)
        {
            try
            {
                //
                if (Program.listDocuments.Exists(a => a.DocumentBarCode == documentToSave.DocumentBarCode))
                {
                    documentToSave.ExcelIndex = Program.listDocuments.Last(a => a.DocumentBarCode == documentToSave.DocumentBarCode).ExcelIndex;
                }
                else
                {
                    documentToSave.ExcelIndex = -1;
                }
                Program.listDocuments.Add(documentToSave);

                ////////////////////////////////SAVE EXCEL////////////////////////////////
                CsvRow row = new CsvRow();
                row.Add(documentToSave.DocumentBarCode);
                row.Add(documentToSave.Page.ToString());
                row.Add(documentToSave.Sender);
                row.Add(documentToSave.Description);
                row.Add(documentToSave.ReceivedDate.ToString("yyyy/MM/dd HH:mm:ss"));

                Utility.WriteTxtLine(Program.fileName, documentToSave.ExcelIndex, Utility.GetRow(row));
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                MessageBox.Show(ex.Message);
            }
        }

        public static void LoadExcel()
        {
            Program.listDocuments = new List<DocumentEntity>();
            Utility.LogEx(Program.fileName, "LoadExcel");
            using (CsvFileReader reader = new CsvFileReader(Program.fileName))
            {
                CsvRow row = new CsvRow();
                int i = 0;
                while (reader.ReadRow(row))
                {
                    try
                    {
                        string DocumentBarCode = row[0];

                        if (!string.IsNullOrEmpty(DocumentBarCode))
                        {
                            int Page = Utility.IntParse(row[1]);
                            string Sender = row[2];
                            string Description = row[3];
                            string StrReceivedDate = row[4];
                            double m_date;
                            double.TryParse(row[4], out m_date);
                            DateTime ReceivedDate = m_date > 0 ? DateTime.FromOADate(m_date) : DateTime.Parse(StrReceivedDate);

                            Program.listDocuments.Add(
                                new DocumentEntity()
                                {
                                    DocumentBarCode = DocumentBarCode,
                                    Page = Page,
                                    Sender = Sender,
                                    Description = Description,
                                    ReceivedDate = ReceivedDate,
                                    ExcelIndex = i
                                }
                            );
                            i++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.LogEx(ex);
                    }
                }
                Program.lastIndex = i;
                Utility.LogEx(i.ToString(), "lastIndex");
            }
        }


        public void LoadView()
        {
            try
            {
                DateTime fromDate = dtpFromDate.DateTime.Date;
                DateTime toDate = dtpToDate.DateTime.Date;
                toDate = toDate.AddDays(1);

                Program.listViews = Program.listDocuments.Where(a =>
                                    (
                                        !string.IsNullOrEmpty(txtDocumentCodeSearch.Text)
                                        ||
                                        (
                                            a.DocumentBarCode.Contains(txtDocumentCodeSearch.Text)
                                            || a.Sender.Contains(txtDocumentCodeSearch.Text)
                                            || a.Description.Contains(txtDocumentCodeSearch.Text)
                                        )
                                    )
                                    &&
                                    (
                                        (
                                            a.ReceivedDate >= fromDate
                                            && a.ReceivedDate <= toDate
                                            ||
                                            a.DeliveryDate >= fromDate
                                            && a.DeliveryDate <= toDate
                                        )
                                    )
                            ).ToList();

                bindingSource.DataSource = Program.listViews;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        public static void LoadViewStatic()
        {
            try
            {
                DateTime fromDate = DateTime.Today.AddDays(-7);
                DateTime toDate = DateTime.Today.AddDays(1);

                Program.listViews = Program.listDocuments.Where(a =>
                                    (
                                        a.ReceivedDate >= fromDate
                                        && a.ReceivedDate <= toDate
                                        ||
                                        a.DeliveryDate >= fromDate
                                        && a.DeliveryDate <= toDate
                                    )
                            ).ToList();

                BindingSource();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        public static void BindingSource()
        {
            //bindingSource.DataSource = Program.listViews;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            LoadView();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel();
        }

        private void ExportExcel()
        {
            try
            {
                ////////////////////////////////SAVE EXCEL////////////////////////////////
                Microsoft.Office.Interop.Excel.Application excelApp = null;
                Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
                Microsoft.Office.Interop.Excel.Workbook workbook = null;
                //Microsoft.Office.Interop.Excel.Sheets sheets = null;
                Microsoft.Office.Interop.Excel.Worksheet sheetDetail = null;
                Microsoft.Office.Interop.Excel.Range range = null;
                string templatePath = System.Windows.Forms.Application.StartupPath + "\\Data.xlsx";
                string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ReportExcel\";
                try
                {
                    try
                    {
                        System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                        if (!directory.Exists)
                        {
                            directory.Create();
                        }

                        directory.Empty();
                    }
                    catch
                    {
                        //Do Nothing
                    }
                    string end = String.Empty;

                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.DisplayAlerts = false;
                    workbooks = excelApp.Workbooks;
                    workbook = workbooks.Open(templatePath, ReadOnly: true);
                    sheetDetail = (Worksheet)workbook.ActiveSheet;

                    int i = 2;
                    foreach (DocumentEntity document in Program.listDocuments)
                    {
                        if (!string.IsNullOrEmpty(document.DocumentBarCode))
                        {
                            sheetDetail.Cells[i, 1] = document.DocumentBarCode;
                            sheetDetail.Cells[i, 2] = document.Page;
                            sheetDetail.Cells[i, 3] = document.Sender;
                            sheetDetail.Cells[i, 4] = document.Description;
                            sheetDetail.Cells[i, 5] = document.ReceivedDate;
                            lblStatusBarMessage.Text = "Đang xuất dòng: " + i.ToString();
                            i++;
                        }
                    }

                    excelApp.DisplayAlerts = false;
                    exportPath += "BaoCaoHoSo" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                    workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                    workbook.Close();
                    excelApp.Quit();

                    lblStatusBarMessage.Text = "Xuất excel thành công";
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                finally
                {
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);
                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void btnOpenFormInput_Click(object sender, EventArgs e)
        {
            Program.OpenFormInput();
        }

        private void dtgDocument_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string barcode = dtgDocument[0, e.RowIndex].Value.ToString();

                if (Program.listDocuments.Exists(a => a.DocumentBarCode == barcode))
                {
                    DocumentEntity document = Program.listDocuments.Last(a => a.DocumentBarCode == barcode);
                    document.Description = dtgDocument["colDescription", e.RowIndex].Value.ToString();
                    SaveExcel(document);
                }
                LoadExcel();
                LoadView();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }
    }
}
