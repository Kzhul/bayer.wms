﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BayerDocumentManagement
{
    public class DocumentEntity
    {
        public DocumentEntity() {
            ReceivedDate = DateTime.Now;
            Page = 0;
            Sender = string.Empty;
            Description = string.Empty;
            DocumentBarCode = string.Empty;
        }

        public int ExcelIndex { get; set; }
        public string DocumentBarCode { get; set; }
        public string Sender { get; set; }
        public int Page { get; set; }
        public string Description { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public bool IsDelivered
        {
            get {
                return DeliveryDate.HasValue ? true: false;
            }
            set {

            }
        }

        public string strReceivedDate
        {
            get { return ReceivedDate.ToString("dd/MM/yyyy HH:mm:ss"); }
            set
            {

            }
        }

        public string strDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty; }
            set
            {

            }
        }
    }
}
