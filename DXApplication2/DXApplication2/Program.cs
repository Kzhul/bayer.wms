﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using BayerDocumentManagement;
using Bayer.WMS.Objs;
using System.Reflection;

namespace DXApplication2
{
    static class Program
    {
        public static List<DocumentEntity> listDocuments;
        public static List<DocumentEntity> listViews;
        public static string fileName = System.Windows.Forms.Application.StartupPath + "\\SystemData.dll";
        public static int lastIndex = 0;
        public static string COMConfig = string.Empty;
        public static string COM_PortName = string.Empty;
        public static int COM_BaudRate = 0;
        public static string COM_Parity = string.Empty;
        public static int COM_DataBits = 0;
        public static string COM_StopBits = string.Empty;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            listDocuments = new List<DocumentEntity>();
            listViews = new List<DocumentEntity>();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            BonusSkins.Register();
            SkinManager.EnableFormSkins();

            ReadCOMConfig();

            Application.Run(new Form1());


        }

        public static void ReadCOMConfig()
        {
            try
            {
                COMConfig = System.Configuration.ConfigurationSettings.AppSettings["COMConfig"].ToString();
                string[] comConfigComponent = COMConfig.Split(';');
                COM_PortName = comConfigComponent.FirstOrDefault(p => p.StartsWith("PortName")).Replace("PortName=", String.Empty).Trim();
                COM_BaudRate = int.Parse(comConfigComponent.FirstOrDefault(p => p.StartsWith("BaudRate")).Replace("BaudRate=", String.Empty).Trim());
                COM_Parity = comConfigComponent.FirstOrDefault(p => p.StartsWith("Parity")).Replace("Parity=", String.Empty).Trim();
                COM_DataBits = int.Parse(comConfigComponent.FirstOrDefault(p => p.StartsWith("DataBits")).Replace("DataBits=", String.Empty).Trim());
                COM_StopBits = comConfigComponent.FirstOrDefault(p => p.StartsWith("StopBits")).Replace("StopBits=", String.Empty).Trim();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        public static void OpenFormInput()
        {
            Form2 fc = (Form2)System.Windows.Forms.Application.OpenForms["Form2"];
            if (fc != null)
            {
                fc.Show();
            }
            else
            {
                fc = new Form2();
                fc.Show();
            }
        }


    }
}
