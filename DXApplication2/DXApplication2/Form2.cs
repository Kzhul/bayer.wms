﻿using Bayer.WMS.Objs;
using BayerDocumentManagement;
using ReadWriteCsv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace DXApplication2
{
    public partial class Form2 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public DocumentEntity currentDocument;
        private SerialPort _serialPort;

        public Form2()
        {
            InitializeComponent();

            ////ribbonControl1.Visible = false;
            txtReceivedDate.ReadOnly = true;
            txtReceivedDate.Text = DateTime.Today.ToShortDateString();
            currentDocument = new DocumentEntity();

            try
            {
                this.Icon = new System.Drawing.Icon(System.Windows.Forms.Application.StartupPath + "\\Bayer.ico");
            }
            catch
            {

            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            txtDocumentBarCode.Select();
            CreateCOM(Program.COM_PortName, Program.COM_BaudRate, Program.COM_Parity, Program.COM_DataBits, Program.COM_StopBits);
        }

        public void BindingControl()
        {
            ClearControl();
            txtDocumentBarCode.Text = currentDocument.DocumentBarCode;
            txtPage.Text = currentDocument.Page.ToString();
            txtSender.Text = currentDocument.Sender;
            txtReceivedDate.Text = currentDocument.strReceivedDate;
        }

        public void ClearControl()
        {
            txtDocumentBarCode.Text = string.Empty;
            txtPage.Text = "0";
            txtSender.Text = string.Empty;
            txtReceivedDate.Text = DateTime.Today.ToShortDateString();
        }

        public void ResetControl()
        {
            txtPage.Text = "0";
            txtSender.Text = string.Empty;
            txtReceivedDate.Text = DateTime.Today.ToShortDateString();
        }

        public void LoadView()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        public bool Save()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtDocumentBarCode.Text.Trim()) || txtPage.Value == 0)
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
                    return false;
                }

                DocumentEntity documentToSave = new DocumentEntity();
                documentToSave.DocumentBarCode = txtDocumentBarCode.Text.Trim();
                documentToSave.Page = Utility.IntParse(txtPage.Text.Trim());
                documentToSave.Sender = txtSender.Text.Trim();
                documentToSave.ReceivedDate = DateTime.Now;

                if (Program.listDocuments.Exists(a => a.DocumentBarCode == documentToSave.DocumentBarCode))
                {
                    documentToSave.Description = Program.listDocuments.Last(a => a.DocumentBarCode == documentToSave.DocumentBarCode).Description;
                }

                ////////////////////////////////SAVE EXCEL////////////////////////////////
                Form1.SaveExcel(documentToSave);

                currentDocument = documentToSave;
                BindingControl();
                Form1.LoadExcel();
                Form1.LoadViewStatic();
                // Form1.LoadView();

                ClearControl();
                MessageBox.Show("Lưu thành công: " + txtDocumentBarCode.Text.Trim());

            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            return false;
        }

        private void txtDocumentBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            LoadDocument(txtDocumentBarCode.Text.Trim());
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void Delivery()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtDocumentBarCode.Text.Trim()))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
                    return;
                }

                if (Program.listDocuments.Exists(a => a.DocumentBarCode == txtDocumentBarCode.Text.Trim()))
                {
                    currentDocument = Program.listDocuments.First(a => a.DocumentBarCode == txtDocumentBarCode.Text.Trim());
                    currentDocument.DeliveryDate = DateTime.Now;
                    Save();

                    MessageBox.Show("Giao thành công: " + txtDocumentBarCode.Text.Trim());
                }
                else
                {
                    MessageBox.Show("Chưa nhập hồ sơ: " + txtDocumentBarCode.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (_serialPort != null && _serialPort.IsOpen)
                    _serialPort.Close();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                string data = _serialPort.ReadExisting();
                Utility.LogEx(data, "_serialPort_DataReceived");
                txtDocumentBarCode.Text = data.Trim();
                LoadDocument(txtDocumentBarCode.Text);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        public void LoadDocument(string barcode)
        {
            ResetControl();
            currentDocument = new DocumentEntity();
            if (!string.IsNullOrWhiteSpace(barcode))
            {
                try
                {
                    if (Program.listDocuments.Exists(a => a.DocumentBarCode == txtDocumentBarCode.Text.Trim()))
                    {
                        currentDocument = Program.listDocuments.Last(a => a.DocumentBarCode == txtDocumentBarCode.Text.Trim());
                        BindingControl();
                        if (MessageBox.Show("Chứng từ này đã được ghi nhận trước đây. Bạn có muốn lấy lại ?", "?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            Save();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
            }
        }

        public void CreateCOM(string portName, int baudRate, string parity, int dataBits, string stopBits)
        {
            try
            {
                Parity p = Parity.None;
                switch (parity)
                {
                    case "Even":
                        p = Parity.Even;
                        break;
                    case "Mark":
                        p = Parity.Mark;
                        break;
                    case "Odd":
                        p = Parity.Odd;
                        break;
                    case "Space":
                        p = Parity.Space;
                        break;
                    default:
                        break;
                }

                StopBits sb = StopBits.None;
                switch (stopBits)
                {
                    case "1":
                        sb = StopBits.One;
                        break;
                    case "2":
                        sb = StopBits.Two;
                        break;
                    case "1.5":
                        sb = StopBits.OnePointFive;
                        break;
                    default:
                        break;
                }

                _serialPort = new SerialPort(portName, baudRate, p, dataBits, sb);
                _serialPort.ReadTimeout = 1000;
                _serialPort.WriteTimeout = 1000;
                _serialPort.DataReceived += _serialPort_DataReceived;
                _serialPort.Open();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }
    }
}
