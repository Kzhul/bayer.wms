﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace BayerSmartDevice
{
    public partial class FrmLogin : Form
    {
        private byte[] bytes = Encoding.ASCII.GetBytes("Bayer_KZ");

        readonly LoginDAL _loginDAL;

        public FrmLogin()
        {
            InitializeComponent();
            _loginDAL = new LoginDAL();
        }

        private void FrmLoginLoad(object sender, EventArgs e)
        {
            GetConfig();
        }

        private void BtnLoginClick(object sender, EventArgs e)
        {
            try
            {
                if (_loginDAL.Login(txtUsername.Text, txtPassword.Text))
                {
                    Program.Username = txtUsername.Text;

                    Hide();
                    var frmMain = new FrmMain();
                    frmMain.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnExitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void GetConfig()
        {
            string assemplyPath = System.Reflection.Assembly.GetCallingAssembly().GetName().CodeBase;
            Program.AppPath = Path.GetDirectoryName(assemplyPath);
            string configPath = Path.Combine(Program.AppPath, "AppConfig.txt");

            try
            {
                var config = new Dictionary<string, string>();
                using (var sr = new StreamReader(configPath))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] tmp = sr.ReadLine().Split(':');
                        if (tmp.Length == 2)
                            config.Add(tmp[0].Trim(), tmp[1].Trim());
                    }
                }

                string[] connStrComponent = config["ConnectionString"].Split(';');
                string server = connStrComponent.FirstOrDefault(p => p.StartsWith("Server")).Replace("Server=", String.Empty).Trim();
                string database = connStrComponent.FirstOrDefault(p => p.StartsWith("Database")).Replace("Database=", String.Empty).Trim();
                string userid = connStrComponent.FirstOrDefault(p => p.StartsWith("Uid")).Replace("Uid=", String.Empty).Trim();
                string pwd = connStrComponent.FirstOrDefault(p => p.StartsWith("pwd")).Replace("pwd=", String.Empty).Trim();

                string connStr = String.Format("Server={0};Database={1};Uid={2};pwd={3};",
                    server, database, DecryptStringFromBytes_AES(userid), DecryptStringFromBytes_AES(pwd));
                Program.ConnStr = connStr;
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file config");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file config");
            }
        }

        private string EncryptStringToBytes_AES(string originalString)
        {
            if (String.IsNullOrEmpty(originalString))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);
            var writer = new StreamWriter(cryptoStream);
            writer.Write(originalString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();

            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        private string DecryptStringFromBytes_AES(string cryptedString)
        {
            if (String.IsNullOrEmpty(cryptedString))
                return String.Empty;

            //cryptedString = cryptedString.Replace("==", "=").Replace(" ", "+");

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
            var reader = new StreamReader(cryptoStream);

            return reader.ReadToEnd();
        }
    }
}