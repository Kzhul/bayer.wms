﻿using System;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Data;

namespace BayerSmartDevice
{
    public class LoginDAL
    {
        public bool Login(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
                throw new Exception("Tên đăng nhập không được rỗng");
            if (string.IsNullOrEmpty(password))
                throw new Exception("Password không được rỗng");

            password = Encrypt(password);
            var dt = new DataTable();
            using (var sqlConn = new SqlConnection(Program.ConnStr))
            {
                sqlConn.Open();

                var sqlCmd = new SqlCommand("proc_User_Select", sqlConn) {CommandType = CommandType.StoredProcedure};

                sqlCmd.Parameters.Add(new SqlParameter("@IsAccountList", 1));
                sqlCmd.Parameters.Add(new SqlParameter("@Username", username));

                var sqlAdapt = new SqlDataAdapter(sqlCmd);

                sqlAdapt.Fill(dt);
            }

            if (dt.Rows.Count == 0) throw new Exception("Tên đăng nhập không chính xác");
            if (password != dt.Rows[0]["Password"].ToString()) throw new Exception("Mật khẩu không chính xác");

            return true;
        }

        private string Encrypt(string password)
        {
            var md5Hasher = new MD5CryptoServiceProvider();
            var encoder = new UTF8Encoding();
            byte[] hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(password));

            return hashedBytes.Aggregate(String.Empty, (current, b) => current + b.ToString("x2"));
        }
    }
}
