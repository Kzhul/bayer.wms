﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BayerSmartDevice
{
    public partial class FrmUpdate : Form
    {
        decimal _receivedQuantity;

        public FrmUpdate()
        {
            InitializeComponent();
        }

        public string ProductCode 
        {
            set { txtProductCode.Text = value; }
        }

        public string ProductName
        {
            set { txtProductName.Text = value; }
        }

        public string Batch
        {
            set { txtBatchNumber.Text = value; }
        }

        public decimal ReceivedQuantity
        {
            get { return _receivedQuantity; }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                _receivedQuantity = decimal.Parse(txtQuantity.Text);
                DialogResult = DialogResult.Yes;
            }
            catch 
            {
                MessageBox.Show("Bạn chỉ được nhập số");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}