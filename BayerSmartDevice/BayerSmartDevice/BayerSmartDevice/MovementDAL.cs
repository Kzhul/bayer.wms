﻿using System.Data.SqlClient;
using System.Data;

namespace BayerSmartDevice
{
    public class MovementDAL
    {
        public DataTable SelectMovement(string documentNumber)
        {
            using (var sqlConn = new SqlConnection(Program.ConnStr))
            {
                sqlConn.Open();

                var sqlCmd = new SqlCommand("proc_MovementLine_Select", sqlConn)
                                 {CommandType = CommandType.StoredProcedure};

                sqlCmd.Parameters.Add(new SqlParameter("@DocumentNumber", documentNumber));

                var sqlAdapt = new SqlDataAdapter(sqlCmd);
                var dt = new DataTable();

                sqlAdapt.Fill(dt);

                return dt;
            }
        }

        public int UpdateMovementLineStatus(int movementID, string productCode, string lot, decimal quantity)
        {
            using (var sqlConn = new SqlConnection(Program.ConnStr))
            {
                sqlConn.Open();

                var sqlCmd = new SqlCommand("proc_MovementLine_Update_Status", sqlConn)
                                 {CommandType = CommandType.StoredProcedure};

                sqlCmd.Parameters.Add(new SqlParameter("@MovementID", movementID));
                sqlCmd.Parameters.Add(new SqlParameter("@ProductCode", productCode));
                sqlCmd.Parameters.Add(new SqlParameter("@Lot", lot));
                sqlCmd.Parameters.Add(new SqlParameter("@Quantity", quantity));
                sqlCmd.Parameters.Add(new SqlParameter("@Username", Program.Username));

                sqlCmd.ExecuteNonQuery();

                return 1;
            }
        }
    }
}
