﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BayerSmartDevice.Properties;
using Intermec.DataCollection;
using System.Text.RegularExpressions;
using System.Media;
using System.IO;

namespace BayerSmartDevice
{
    public partial class FrmMain : Form
    {
        readonly BarcodeReader _bcr;
        readonly MovementDAL _movementDAL;
        readonly AttributeSetDAL _attrSetDAL;
        readonly Regex _materialRegex;
        readonly Regex _materialRegex2;
        DataTable _dt;
        int _movementID;
        string _scannedValue;
        bool _isMatch, _isPackaging;
        FrmPackaging _packaging;

        public FrmMain()
        {
            InitializeComponent();
            _bcr = new BarcodeReader();
            _bcr.BarcodeRead += BcrBarcodeRead;
            _bcr.ThreadedRead(true);
            _movementDAL = new MovementDAL();
            _attrSetDAL = new AttributeSetDAL();
            _materialRegex = new Regex(@"^\d*\d{8}\w\d{6}$");
            _materialRegex2 = new Regex(@"^\d*\d{8}\w\d{12}$");
        }

        private void Form1Load(object sender, EventArgs e)
        {
            try
            {
                _isMatch = false;
                _scannedValue = ",";
                InitControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void InitControl()
        {
            var dtStyle = new DataGridTableStyle { MappingName = "MovementLine" };

            var colNumber = new ColumnStyle(1) { Width = 30, HeaderText = "#", MappingName = "Number" };
            colNumber.CheckCellEquals += CheckCellEquals;
            dtStyle.GridColumnStyles.Add(colNumber);

            var colLot = new ColumnStyle(1) { Width = 80, HeaderText = "Lô NL", MappingName = "Lot" };
            colLot.CheckCellEquals += CheckCellEquals;
            dtStyle.GridColumnStyles.Add(colLot);

            var colCode = new ColumnStyle(1) { Width = 80, HeaderText = "Mã NL", MappingName = "ProductCode" };
            colCode.CheckCellEquals += CheckCellEquals;
            dtStyle.GridColumnStyles.Add(colCode);

            var colName = new ColumnStyle(1) { Width = 200, HeaderText = "Tên NL", MappingName = "ProductName" };
            colName.CheckCellEquals += CheckCellEquals;
            dtStyle.GridColumnStyles.Add(colName);

            var colQuantity = new ColumnStyle(1) { Width = 100, HeaderText = "SL xuất", MappingName = "Quantity" };
            colQuantity.CheckCellEquals += CheckCellEquals;
            dtStyle.GridColumnStyles.Add(colQuantity);

            var colReceivedQty = new ColumnStyle(1) { Width = 100, HeaderText = "SL đã nhận", MappingName = "ReceivedQuantity" };
            colReceivedQty.CheckCellEquals += CheckCellEquals;
            dtStyle.GridColumnStyles.Add(colReceivedQty);

            var colRequestQty = new ColumnStyle(1) { Width = 100, HeaderText = "SL y.cầu", MappingName = "RequestQuantity" };
            colRequestQty.CheckCellEquals += CheckCellEquals;
            dtStyle.GridColumnStyles.Add(colRequestQty);

            dtgMovement.TableStyles.Add(dtStyle);
        }

        private void LoadMovementLine()
        {
            try
            {
                if (String.IsNullOrEmpty(txtMovementNumber.Text))
                    MessageBox.Show(Resources.FrmMain_LoadMovementLine_Số_phiếu_chuyển_kho_không_được_để_trống);

                _scannedValue = ",";
                MovementLines = _movementDAL.SelectMovement(txtMovementNumber.Text);
            }
            catch
            {
                MessageBox.Show(Resources.FrmMain_LoadMovementLine_Có_lỗi_xảy_ra);
            }
        }

        private void ProcessBarcode(string barcode)
        {
            if (!_isPackaging)
            {
                if (_materialRegex.IsMatch(barcode) ||
                    _materialRegex2.IsMatch(barcode))
                {
                    if (_scannedValue.Contains("," + barcode + ","))
                    {
                        PlaySoundFromResource();
                        MessageBox.Show(Resources.FrmMain_ProcessBarcode_Barcode_đã_quét_rồi);
                    }
                    else
                    {
                        bool isHasExpired = false;
                        if (_materialRegex2.IsMatch(barcode))
                        {
                            isHasExpired = true;
                            string str = barcode.Substring(barcode.Length - 10, 6);
                            var expiredDate = new DateTime(
                                int.Parse("20" + str.Substring(4, 2)),
                                int.Parse(str.Substring(2, 2)),
                                int.Parse(str.Substring(0, 2)));

                            if (expiredDate.Date <= DateTime.Today)
                            {
                                MessageBox.Show(Resources.FrmMain_ProcessBarcode_Lô_nguyên_liệu_đã_hết_hạn_sử_dụng);
                                return;
                            }
                        }

                        int number = isHasExpired ? 21 : 15;
                        int length = barcode.Length;

                        string matCode = barcode.Substring(0, length - number);
                        string packSize = barcode.Substring(length - number, 2);
                        string matBatch = barcode.Substring(length - number + 2, 9);
                        barcode = String.Concat(matCode, matBatch);

                        _scannedValue += barcode + ",";
                        dtgMovement.Refresh();

                        int rowCount = _dt.Rows.Count;
                        for (int i = 0; i < rowCount; i++)
                        {
                            string lot = dtgMovement[i, 1].ToString();
                            string productCode = dtgMovement[i, 2].ToString();
                            string productName = dtgMovement[i, 3].ToString();
                            decimal quantity = decimal.Parse(dtgMovement[i, 4].ToString());
                            decimal receivedQty = decimal.Parse(dtgMovement[i, 5].ToString());

                            string temp = String.Concat(productCode, lot);
                            if (barcode == temp)
                            {
                                if (!_attrSetDAL.IsValidPackageSize(productCode, packSize))
                                {
                                    PlaySoundFromResource();
                                    MessageBox.Show(Resources.FrmMain_ProcessBarcode_Quy_cách_không_đúng);
                                    return;
                                }

                                _isMatch = true;
                                using (var frm = new FrmUpdate())
                                {
                                    frm.ProductCode = productCode;
                                    frm.ProductName = productName;
                                    frm.Batch = lot;

                                    if (frm.ShowDialog() == DialogResult.Yes)
                                    {
                                        if (receivedQty + frm.ReceivedQuantity > quantity)
                                        {
                                            PlaySoundFromResource();
                                            MessageBox.Show(Resources.FrmMain_ProcessBarcode_Số_lượng_nhận_vượt_quá_số_lượng_xuất);
                                        }
                                        else
                                        {
                                            if (_movementDAL.UpdateMovementLineStatus(_movementID, productCode, lot, frm.ReceivedQuantity) > 0)
                                            {
                                                dtgMovement[i, 5] = receivedQty + frm.ReceivedQuantity;
                                                var currentCell = new DataGridCell(i, 5);
                                                dtgMovement.CurrentCell = currentCell;
                                                dtgMovement.Refresh();
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                        if (!_isMatch)
                        {
                            PlaySoundFromResource();
                            MessageBox.Show(Resources.FrmMain_ProcessBarcode_Nguyên_liệu_không_nằm_trong_phiếu_chuyển_kho_này__bạn_hãy_kiểm_tra_lại_mã_nguyên_liệu_và_mã_lô);
                        }
                        _isMatch = false;
                    }
                }
                else if (barcode.ToLower() == "packaging")
                {
                    _isPackaging = true;
                    if (_packaging == null || _packaging.IsDisposed)
                        _packaging = new FrmPackaging();
                    _packaging.Main = this;
                    _packaging.BackColor = SystemColors.Control;
                    _packaging.Show();
                }
                else
                {
                    _scannedValue = ",";
                    txtMovementNumber.Text = barcode;
                    LoadMovementLine();
                }
            }
            else
            {
                _packaging.ProcessBarcode(barcode);
            }
        }

        private void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            string lot = dtgMovement[e.Row, 1].ToString();
            string productCode = dtgMovement[e.Row, 2].ToString();
            string value = String.Concat(",", productCode, lot, ",");
            decimal quantity = decimal.Parse(dtgMovement[e.Row, 4].ToString());
            decimal receivedQty = decimal.Parse(dtgMovement[e.Row, 5].ToString());

            if (dtgMovement[e.Row, 1] != null)
            {
                if (_scannedValue.Contains(value))
                {
                    if (receivedQty == 0)
                        e.MeetsCriteria = 0;
                    else if (receivedQty < quantity)
                        e.MeetsCriteria = 1;
                    else
                        e.MeetsCriteria = 2;
                }
                else
                    e.MeetsCriteria = 0;
            }
            else
                e.MeetsCriteria = 0;
        }

        private void BcrBarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            string barcode = bre.strDataBuffer;
            ProcessBarcode(barcode);
        }

        private void BtnSearchClick(object sender, EventArgs e)
        {
            ProcessBarcode(txtMovementNumber.Text);
        }

        private void PlaySoundFromResource()
        {
            string path = Path.Combine(Program.AppPath, @"Resources\Error.wav");
            var player = new SoundPlayer(path);
            player.Play();
            System.Threading.Thread.Sleep(3000);
            player.Stop();
        }

        public DataTable MovementLines
        {
            set
            {
                _dt = value;
                if (!value.Columns.Contains("Number"))
                    value.Columns.Add("Number", typeof(int));

                int rowCount = value.Rows.Count;
                _movementID = rowCount > 0 ? int.Parse(value.Rows[0]["MovementID"].ToString()) : 0;

                for (int i = 0; i < rowCount; i++)
                {
                    value.Rows[i]["Number"] = i + 1;
                    if (value.Rows[i]["Status"] != null && int.Parse(value.Rows[i]["Status"].ToString()) == 2)
                    {
                        string productCode = value.Rows[i]["ProductCode"].ToString();
                        string lot = value.Rows[i]["Lot"].ToString();
                        _scannedValue += productCode + lot + ",";
                    }
                }

                value.TableName = "MovementLine";
                bdsMovement.DataSource = value;
            }
        }

        public bool IsPackaging
        {
            set
            {
                _isPackaging = value;
                if (!_isPackaging)
                    _packaging = null;
            }
        }

        private void BtnLogoutClick(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}