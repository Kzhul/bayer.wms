﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Threading;
using System.Media;

namespace BayerSmartDevice
{
    public partial class FrmPackaging : Form
    {
        int _currentUser, _lotID;
        string _productionCode;
        int[] _userID;
        Regex _lotRegex, _packageRegex, _userRegex;
        Thread _thread;
        System.Windows.Forms.Timer _timer;

        private void FrmPackaging_Load(object sender, EventArgs e)
        {
            _currentUser = 0;
            _userID = new int[3];

            _lotRegex = new Regex(@"^\d{6}\w\d{2}$");
            _packageRegex = new Regex(@"\d{13,15}\w\d{18}");
            _userRegex = new Regex(@"(NV|nv)\d{4}");

            this.lblFullName1.Text = String.Empty;
            this.lblFullName2.Text = String.Empty;
            this.lblFullName3.Text = String.Empty;
            this.lblProductionLot.Text = String.Empty;
            this.lblProductionName.Text = String.Empty;
            this.lblPackage.Text = String.Empty;
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            if (this.BackColor != Color.Yellow)
                this.BackColor = Color.Yellow;
            else
                this.BackColor = SystemColors.Control;
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            ProcessBarcode(this.txtBarcode.Text.Trim());
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            Check();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Main.IsPackaging = false;
            this.Close();
        }

        private void LoadEmployee(string empCode)
        {
            using (SqlConnection conn = new SqlConnection(Program.ConnStr))
            {
                try
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("proc_User_Select", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("@EmployeeCode", empCode));
                        cmd.Parameters.Add(new SqlParameter("@IsAccountList", "0"));

                        DataTable dt = new DataTable();

                        SqlDataAdapter sqlAdapt = new SqlDataAdapter(cmd);
                        sqlAdapt.Fill(dt);

                        if (dt.Rows.Count == 0)
                            throw new Exception("Mã nhân viên không hợp lệ");

                        if (_currentUser == 0)
                            this.lblFullName1.Text = dt.Rows[0]["FullName"].ToString();
                        else if (_currentUser == 1)
                            this.lblFullName2.Text = dt.Rows[0]["FullName"].ToString();
                        else
                            this.lblFullName3.Text = dt.Rows[0]["FullName"].ToString();

                        _userID[_currentUser] = int.Parse(dt.Rows[0]["UserID"].ToString());
                        _currentUser = _currentUser == 2 ? 0 : _currentUser + 1;
                    }
                }
                catch (Exception ex)
                {
                    PlayErrorSound();
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void LoadLot(string code)
        {
            using (SqlConnection conn = new SqlConnection(Program.ConnStr))
            {
                try
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("proc_Lot_Select", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("@LotCode", code));

                        DataTable dt = new DataTable();

                        SqlDataAdapter sqlAdapt = new SqlDataAdapter(cmd);
                        sqlAdapt.Fill(dt);

                        if (dt.Rows.Count == 0)
                            throw new Exception("Số lô không tồn tại");

                        _lotID = int.Parse(dt.Rows[0]["LotID"].ToString());
                        _productionCode = dt.Rows[0]["ProductCode"].ToString();
                        this.lblProductionLot.Text = dt.Rows[0]["Code"].ToString();
                        this.lblProductionName.Text = dt.Rows[0]["ProductName"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    PlayErrorSound();
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void Check()
        {
            try
            {
                if (_userID[0] == 0 || _userID[1] == 0 || _userID[2] == 0)
                    throw new Exception("Bạn phải quét barcode mã nhân viên");
                else if (String.IsNullOrEmpty(this.lblProductionLot.Text))
                    throw new Exception("Bạn phải quét barcode lô thành phẩm");
                else if (String.IsNullOrEmpty(this.lblPackage.Text))
                    throw new Exception("Bạn phải quét barcode bao bì thành phẩm");

                try
                {
                    if (this.lblPackage.Text.IndexOf(this.lblProductionLot.Text, 0) == -1)
                        throw new Exception("Bao bì thành phẩm không đúng với lô đã trộn");
                    if (this.lblPackage.Text.IndexOf(_productionCode, 0) == -1)
                        throw new Exception("Bao bì thành phẩm không đúng với lô đã trộn");

                    this.BackColor = Color.Green;
                }
                catch (Exception ex)
                {
                    PlayErrorSound();
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    SavePackageLog(this.lblPackage.Text);
                }
            }
            catch (Exception ex)
            {
                PlayErrorSound();
                MessageBox.Show(ex.Message);
            }
        }

        private void SavePackageLog(string package)
        {
            using (SqlConnection conn = new SqlConnection(Program.ConnStr))
            {
                try
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("proc_PackageLog_Insert", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("@User1ID", _userID[0]));
                        cmd.Parameters.Add(new SqlParameter("@User2ID", _userID[1]));
                        cmd.Parameters.Add(new SqlParameter("@User3ID", _userID[2]));
                        cmd.Parameters.Add(new SqlParameter("@LotID", _lotID));
                        cmd.Parameters.Add(new SqlParameter("@Package", package));
                        cmd.Parameters.Add(new SqlParameter("@Note", this.BackColor == Color.Green ? String.Empty : "Bao bì không đúng lô"));
                        cmd.Parameters.Add(new SqlParameter("@Status", this.BackColor == Color.Green ? 1 : 0));

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void PlayErrorSound()
        {
            _thread = new Thread(delegate()
            {
                this.BeginInvoke((ThreadStart)delegate()
                {
                    _timer = new System.Windows.Forms.Timer();
                    _timer.Interval = 250;
                    _timer.Tick += _timer_Tick;
                    _timer.Enabled = true;
                });
            });
            _thread.Start();

            string path = System.IO.Path.Combine(Program.AppPath, @"Resources\Error.wav");
            SoundPlayer player = new SoundPlayer(path);
            player.Play();
            System.Threading.Thread.Sleep(3000);
            player.Stop();
        }

        public FrmPackaging()
        {
            InitializeComponent();
        }

        public FrmMain Main { get; set; }

        public void ProcessBarcode(string barcode)
        {
            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\0", String.Empty);
                this.BackColor = SystemColors.Control;
                if (_thread != null)
                {
                    _thread.Abort();
                    _thread = null;
                }
                if (_timer != null)
                {
                    _timer.Enabled = false;
                    _timer = null;
                }

                if (_lotRegex.IsMatch(barcode))
                    LoadLot(barcode);
                else if (_packageRegex.IsMatch(barcode))
                {
                    this.lblPackage.Text = barcode;
                    Check();
                }
                else if (_userRegex.IsMatch(barcode))
                    LoadEmployee(barcode);
                else
                    throw new Exception("Barcode không hợp lệ");
            }
            catch (Exception ex)
            {
                PlayErrorSound();
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.txtBarcode.Text = String.Empty;
            }
        }
    }
}