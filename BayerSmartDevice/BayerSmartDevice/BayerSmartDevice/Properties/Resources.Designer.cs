﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.8000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BayerSmartDevice.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BayerSmartDevice.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Có lỗi xảy ra.
        /// </summary>
        internal static string FrmMain_LoadMovementLine_Có_lỗi_xảy_ra {
            get {
                return ResourceManager.GetString("FrmMain_LoadMovementLine_Có_lỗi_xảy_ra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Số phiếu chuyển kho không được để trống.
        /// </summary>
        internal static string FrmMain_LoadMovementLine_Số_phiếu_chuyển_kho_không_được_để_trống {
            get {
                return ResourceManager.GetString("FrmMain_LoadMovementLine_Số_phiếu_chuyển_kho_không_được_để_trống", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Barcode đã quét rồi.
        /// </summary>
        internal static string FrmMain_ProcessBarcode_Barcode_đã_quét_rồi {
            get {
                return ResourceManager.GetString("FrmMain_ProcessBarcode_Barcode_đã_quét_rồi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lô nguyên liệu đã hết hạn sử dụng.
        /// </summary>
        internal static string FrmMain_ProcessBarcode_Lô_nguyên_liệu_đã_hết_hạn_sử_dụng {
            get {
                return ResourceManager.GetString("FrmMain_ProcessBarcode_Lô_nguyên_liệu_đã_hết_hạn_sử_dụng", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nguyên liệu không nằm trong phiếu chuyển kho này, bạn hãy kiểm tra lại mã nguyên liệu và mã lô.
        /// </summary>
        internal static string FrmMain_ProcessBarcode_Nguyên_liệu_không_nằm_trong_phiếu_chuyển_kho_này__bạn_hãy_kiểm_tra_lại_mã_nguyên_liệu_và_mã_lô {
            get {
                return ResourceManager.GetString("FrmMain_ProcessBarcode_Nguyên_liệu_không_nằm_trong_phiếu_chuyển_kho_này__bạn_hãy_" +
                        "kiểm_tra_lại_mã_nguyên_liệu_và_mã_lô", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quy cách không đúng.
        /// </summary>
        internal static string FrmMain_ProcessBarcode_Quy_cách_không_đúng {
            get {
                return ResourceManager.GetString("FrmMain_ProcessBarcode_Quy_cách_không_đúng", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Số lượng nhận vượt quá số lượng xuất.
        /// </summary>
        internal static string FrmMain_ProcessBarcode_Số_lượng_nhận_vượt_quá_số_lượng_xuất {
            get {
                return ResourceManager.GetString("FrmMain_ProcessBarcode_Số_lượng_nhận_vượt_quá_số_lượng_xuất", resourceCulture);
            }
        }
    }
}
