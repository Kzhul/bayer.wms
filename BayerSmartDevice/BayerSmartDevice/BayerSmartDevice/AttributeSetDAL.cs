﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace BayerSmartDevice
{
    public class AttributeSetDAL
    {
        public bool IsValidPackageSize(string productCode, string packageSize)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(Program.ConnStr))
                {
                    sqlConn.Open();

                    SqlCommand sqlCmd = new SqlCommand("proc_AttributeSet_Select", sqlConn);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.Add(new SqlParameter("@ProductCode", productCode));
                    sqlCmd.Parameters.Add(new SqlParameter("@AttributeCode", packageSize));

                    SqlDataAdapter sqlAdapt = new SqlDataAdapter(sqlCmd);
                    DataTable dt = new DataTable();

                    sqlAdapt.Fill(dt);

                    return dt.Rows.Count > 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
