namespace Bayer.WMS.UpdateDelivery.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BayerWMSContext : DbContext
    {
        public BayerWMSContext()
            : base("name=BayerWMSContext")
        {
        }

        public virtual DbSet<DeliveryHistory> DeliveryHistories { get; set; }
        public virtual DbSet<ProductPacking> ProductPackings { get; set; }
        public virtual DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DeliveryHistory>()
                .Property(e => e.ProductLot)
                .IsUnicode(false);

            modelBuilder.Entity<DeliveryHistory>()
                .Property(e => e.ProductBarcode)
                .IsUnicode(false);

            modelBuilder.Entity<DeliveryHistory>()
                .Property(e => e.EncryptedProductBarcode)
                .IsUnicode(false);

            modelBuilder.Entity<DeliveryHistory>()
                .Property(e => e.CartonBarcode)
                .IsUnicode(false);

            modelBuilder.Entity<DeliveryHistory>()
                .Property(e => e.DeliveryTicketCode)
                .IsUnicode(false);

            modelBuilder.Entity<DeliveryHistory>()
                .Property(e => e.CompanyCode)
                .IsUnicode(false);

            modelBuilder.Entity<DeliveryHistory>()
                .Property(e => e.TruckNo)
                .IsUnicode(false);

            modelBuilder.Entity<DeliveryHistory>()
                .Property(e => e.RowVersion)
                .IsFixedLength();

            modelBuilder.Entity<ProductPacking>()
                .Property(e => e.ProductPackingCode)
                .IsUnicode(false);

            modelBuilder.Entity<ProductPacking>()
                .Property(e => e.Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ProductPacking>()
                .Property(e => e.RowVersion)
                .IsFixedLength();

            modelBuilder.Entity<Product>()
                .Property(e => e.ProductCode)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.UOM)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Status)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.RowVersion)
                .IsFixedLength();

            modelBuilder.Entity<Product>()
                .Property(e => e.PackingType)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
