namespace Bayer.WMS.UpdateDelivery.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DeliveryHistory
    {
        public int? ProductID { get; set; }

        [StringLength(255)]
        public string ProductLot { get; set; }

        [Key]
        [StringLength(255)]
        public string ProductBarcode { get; set; }

        [StringLength(255)]
        public string EncryptedProductBarcode { get; set; }

        [StringLength(255)]
        public string CartonBarcode { get; set; }

        [StringLength(255)]
        public string DeliveryTicketCode { get; set; }

        [StringLength(255)]
        public string CompanyCode { get; set; }

        [StringLength(255)]
        public string CompanyName { get; set; }

        [StringLength(255)]
        public string TruckNo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ActualDeliveryDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeliveryDate { get; set; }

        public int? CreatedBy { get; set; }

        public int? CreatedBySitemapID { get; set; }

        public DateTime? CreatedDateTime { get; set; }

        public int? UpdatedBy { get; set; }

        public int? UpdatedBySitemapID { get; set; }

        public DateTime? UpdatedDateTime { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RowVersion { get; set; }
    }
}
