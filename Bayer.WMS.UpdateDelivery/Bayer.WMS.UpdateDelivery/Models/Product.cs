namespace Bayer.WMS.UpdateDelivery.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product
    {
        public int ProductID { get; set; }

        [StringLength(255)]
        public string ProductCode { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public int? CategoryID { get; set; }

        [StringLength(1)]
        public string Type { get; set; }

        [StringLength(255)]
        public string UOM { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public bool? IsDeleted { get; set; }

        public int? CreatedBy { get; set; }

        public int? CreatedBySitemapID { get; set; }

        public DateTime? CreatedDateTime { get; set; }

        public int? UpdatedBy { get; set; }

        public int? UpdatedBySitemapID { get; set; }

        public DateTime? UpdatedDateTime { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RowVersion { get; set; }

        [StringLength(1)]
        public string PackingType { get; set; }

        public int? SampleQty { get; set; }

        public decimal? PrintLabelPercentage { get; set; }
    }
}
