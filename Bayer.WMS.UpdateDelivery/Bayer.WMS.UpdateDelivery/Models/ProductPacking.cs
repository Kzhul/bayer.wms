namespace Bayer.WMS.UpdateDelivery.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProductPacking
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string ProductPackingCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductID { get; set; }

        [StringLength(1)]
        public string Type { get; set; }

        public decimal? Quantity { get; set; }

        [StringLength(255)]
        public string Size { get; set; }

        [StringLength(255)]
        public string Weight { get; set; }

        [StringLength(255)]
        public string Note { get; set; }

        public int? CreatedBy { get; set; }

        public int? CreatedBySitemapID { get; set; }

        public DateTime? CreatedDateTime { get; set; }

        public int? UpdatedBy { get; set; }

        public int? UpdatedBySitemapID { get; set; }

        public DateTime? UpdatedDateTime { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RowVersion { get; set; }
    }
}
