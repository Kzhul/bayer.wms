﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.UpdateDelivery
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            string fileName;
            string safeFileName;

            using (var ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;

                fileName = ofd.FileName;
                safeFileName = ofd.SafeFileName;
            }

            var list = new List<string>();

            using (var sr = new StreamReader(fileName))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (line == "#N/A")
                        continue;

                    list.Add(line);
                }

                sr.Close();
            }

            var thread = new Thread(() => Process(safeFileName, list));
            thread.Start();
        }

        private void Process(string fileName, List<string> list)
        {
            Invoke((MethodInvoker)delegate
            {
                btnImport.Enabled = false;
                lblCurrent.Text = $"0 / {list.Count}";
                progressBar1.Step = 1;
                progressBar1.Maximum = list.Count;
            });

            var context = new Models.BayerWMSContext();
            var products = context.Products.ToList();


            foreach (string line in list)
            {
                string[] temp = line.Split('|');

                string delivery = temp[0];
                string customerCode = temp[4];
                string customerName = temp[1];
                string productCode = temp[5];
                string productName = temp[6];
                string productLot = temp[7];
                string deliveryDate = temp[9];
                int qty = int.Parse(temp[8]);

                Invoke((MethodInvoker)delegate
                {
                    lblCurrent.Text = $"{progressBar1.Value + 1} / {list.Count}";
                    lblCustomer.Text = customerName;
                    lblProduct.Text = productName;
                    lblQty.Text = qty.ToString();
                });

                var product = products.FirstOrDefault(p => p.ProductCode == productCode);

                var deliveryHistories = context.DeliveryHistories
                    .Where(p => p.CompanyCode == customerCode && p.ProductID == product.ProductID && p.ProductLot == productLot && p.CompanyName == null)
                    .Take(qty).ToList();

                if (deliveryHistories.Count > 0)
                {
                    foreach (var dh in deliveryHistories)
                    {
                        dh.DeliveryTicketCode = delivery;
                        dh.CompanyName = customerName;
                        dh.ActualDeliveryDate = DateTime.FromOADate(double.Parse(deliveryDate));
                        dh.DeliveryDate = DateTime.FromOADate(double.Parse(deliveryDate));

                        context.SaveChanges();
                    }
                }
                else
                {
                    using (var sw = new StreamWriter($"NotFound_{fileName}.csv", true))
                    {
                        sw.WriteLine(line);
                        sw.Flush();
                        sw.Close();
                    }
                }

                Invoke((MethodInvoker)delegate
                {
                    progressBar1.PerformStep();
                });
            }

            Invoke((MethodInvoker)delegate
            {
                btnImport.Enabled = true;
                MessageBox.Show("Hoàn thành");
            });
        }
    }
}
