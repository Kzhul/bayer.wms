Kiểm kê kho----------------------------------------------
	Report: số lô, số lượng, vị trí pallet	 
	Import tồn kho vào QR code --> so sánh?	 
	Physical count: quét từng mã pallet --> xuất report --> so sánh với SAP	 
	Sửa đổi thực tế --> ghi nhận báo cáo (audit trail)	 
	In danh sách không ổn --> báo cáo sai lệch kiểm tra lại --> approved của anh Lộc --> điều chỉnh

	Làm data migration: chuẩn bị dữ liệu cho việc rớt hàng theo DO
	Kiểm kê kho
		Thủ kho cầm CK3 đi xác nhận Mã pallet, mã vị trí, số lô, số lượng trên pallet đó
			Không đúng, thủ kho ghi lại để kiểm soát

Mã pallet----------------------------------------------
	Bỏ tháng năm
	Chuyển lại thành năm… số chạy
	Xem được tình trạng pallet
	In được mã pallet ngay từ màn hình, xuất ra thư mục danh sách pallet chọn
	Cần có mã pallet,ngân hàng mã pallet từ chương trình và in nhãn pallet A5 thành 4 bản. 

Xuất----------------------------------------------
	o	phần trạng thái trong CK3 cần chú thích màu cho từng trạng thái đó.
	o	phần trả hàng yêu cầu hiển thị cột trả hàng trên winapp.
	Phần nhận hàng cần có thêm chức năng đếm hàng để triểm tra số lượng hàng đã nhận.
	Trên CK3 phần nhận hàng cột trạng thái cần có sự phân biệt màu giữa hàng đã trả,hàng đã nhận và hàng chờ nhận.
		Thùng không đúng quy cách, ví dụ có 15 thùng đủ, 1 thùng ko đủ, làm sao cho dễ nhìn
	Báo cáo chi tiết kho, mã vị trí và tên vị trí dễ nhìn hơn
		Khi xuất hàng, chỉ gợi ý pallet vừa đủ số lượng xuất
		SL cần, gom lại thành nhiều pallet cho dễ nhìn
	o	Cần sửa lại phần rớt hàng thành phẩm trên CK3 hiện tại thì trên CK3 hiển thị cho câu là đưa hàng thành phẩm lên kệ vào vị trí, thay cho câu là rớt hàng thành phẩm vào vị trí.
	o	Khi đã rớt tất cả các hàng thành phẩm thì chỉ cần quét 1 lần vào vị trí tạm . hiện tại thì mỗi lần rớt 1 pallet thì quét 1 lần vị trí tạm. 
	o	Trên CK3 đưa thành phẩm lên kệ,trong phần danh sách chờ pallet nhưng danh sách pallet đó muốn giao đi cho. khách hàng khi đã giao xong,trên phần danh sách chờ pallet vẫn hiển thị chờ những pallet đã được giao cho khách rồi.
	Sắp xếp thứ tự cột tùy theo công việc. Ví dụ đưa lên kệ thì cần mã pallet trước, vị trí sau, còn khi rớt hàng thì cần mã vị trí trước, sau đó là hàng hóa, pallet sau cùng
	o	Xe nâng khi quét pallet không đúng với công việc đang chọn, cần có cảnh báo
	
Chia----------------------------------------------
Soạn----------------------------------------------
Giao----------------------------------------------
Kiểm Kê----------------------------------------------
San Hàng Thành Phẩm----------------------------------------------
San Hàng Nguyên Liệu----------------------------------------------




Rớt hàng----------------------------------------------
Chiến thuật lấy hàng: chưa làm
	Từ trên xuống, từ trong ra
	Né các đường đi đã bị chất hàng, xe nâng khó vào
		-> list ra các vị trí đang chứa pallet khi cần tới vị trí lấy
	Cần bổ sung phần phiếu yêu cầu nguyên liệu thêm cột số lô,nếu như có thì hệ thống tự động điền hoặc không có thì tự điền.


Thao tác màn hình----------------------------------------------
	Đổi logo trên các form
CK3
	Cần mã barcode hoặc QR code cho các activities:  như nhận hàng, soạn hàng, giao hàng
		Tùy theo quyền hạn của người sử dụng, sẽ mở màn hình default
WinApp
	Các phiếu liên quan:
		Bấm vào mã phiếu sẽ mở màn hình phiếu liên quan

Module trả hàng----------------------------------------------
	Bàn bạc lại
	Thủ kho làm trên CK3, chọn lý do
		Ck3 nhận hàng trả
			Còn QR code thì quét
			Ko còn điền số lượng

Nhập Nguyên liệu vào kho----------------------------------------------
	Nhãn A4 cho hoàn chỉnh

Trả hàng NL cho NCC----------------------------------------------
	Coi lại
	THủ kho quét từng pallet
	Xuất report: mã, tên, số lô, số lượng, PO, lý do

Xem lại master data, cần anh Tín gửi lại excel: thành phẩm, bao bì, nguyên liệu, hàng khuyến mãi, hàng nhập khẩu

Nhiệt độ bảo quản
	Làm dữ liệu chính xác với kho
	Demo lưu theo nhiệt độ bảo quản
	Cảnh báo khi lưu kho không đúng với nhiệt độ
Đóng gói
	In nhãn thùng: A6
		Máy in nhãn barcode
		Máy in A4 thường

Chuyển kho nguyên liệu----------------------------------------------
	Huấn luyện chuyển kho nguyên liệu
	Bấm vào số lô, hiện popup giống như truy vết barcode
		Double click chọn pallet
	Chọn pallet, bấm nút xuất
		Điền vào danh sách			
		Cần bỏ 2 số 0 cuối của các số liệu.
		Phần ghi chú trên winapp cần hiển thị màu trên CK3 của xe nâng khi rớt hàng để ưu tiên việc rớt hàng nào trước.
		Cần thay đổi câu lệnh pallet nguồn và pallet đích thành câu lệnh pallet có hàng và pallet trống của phần san hàng trên CK3.
	Phần trạng thái cần có thông báo là đã chuyển nguyên liệu vì hiện tại thủ kho làm xong thì trạng thái để trống.
		Cần bỏ thao tác bấm nút xác nhận khi xe nâng đã quét vào vị trí tạm,hiện tại là phải bấm nút xác nhận vào vị trí tạm.
		Cần có những danh sách đã làm trên CK3 để xe nâng biết được những pallet nào mình chưa làm.



