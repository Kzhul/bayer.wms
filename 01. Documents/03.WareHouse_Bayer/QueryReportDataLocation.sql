--Zone Selection
SELECT DISTINCT
	 Z.ZoneName
	 , Z.Description
	 , COUNT(DISTINCT L.LocationCode) AS Total
	 , COUNT(DISTINCT P.LocationCode) AS CheckedLocation
	 , EmptyLocation = COUNT(DISTINCT L.LocationCode) - COUNT(DISTINCT P.LocationCode)
FROM 
	Zones Z
	LEFT JOIN [dbo].[ZoneLines] ZL
		ON Z.ZoneCode = ZL.ZoneCode
	LEFT JOIN [dbo].[ZoneLocations] L
		ON ZL.ZoneCode = L.ZoneCode
		AND ZL.LineCode = L.LineCode
	LEFT JOIN [dbo].[Pallets] P
		ON P.LocationCode = L.LocationCode
		AND P.LocationCode IS NOT NULL
		AND P.LocationCode != ''
GROUP BY
	Z.ZoneCode
	 , Z.ZoneName
	 , Z.Description

--Zone Selection
SELECT DISTINCT
	 Z.ZoneName
	 , ZL.LineName
	 , ZL.Description
	 , ZL.Level
	 , ZL.Length
	 , COUNT(DISTINCT L.LocationCode) AS Total
	 , COUNT(DISTINCT P.LocationCode) AS CheckedLocation
	 , EmptyLocation = COUNT(DISTINCT L.LocationCode) - COUNT(DISTINCT P.LocationCode)
FROM 
	Zones Z
	LEFT JOIN [dbo].[ZoneLines] ZL
		ON Z.ZoneCode = ZL.ZoneCode
	LEFT JOIN [dbo].[ZoneLocations] L
		ON ZL.ZoneCode = L.ZoneCode
		AND ZL.LineCode = L.LineCode
	LEFT JOIN [dbo].[Pallets] P
		ON P.LocationCode = L.LocationCode
		AND P.LocationCode IS NOT NULL
		AND P.LocationCode != ''
GROUP BY
	Z.ZoneCode
	 , Z.ZoneName
	 , ZL.LineName
	 , ZL.Description
	 , ZL.Level
	 , ZL.Length

--Zone Selection
SELECT DISTINCT
	 Z.ZoneName
	 , ZL.LineName
	 , L.LocationCode
	 , P.PalletCode
	 , PS.ProductLot
	 , PR.ProductCode
	 , PR.Description AS ProductName
	 , PR.UOM
	 , PPS.Quantity AS PackSize
	 , COUNT(DISTINCT PS.CartonBarcode) AS CartonQuantity
	 , COUNT(DISTINCT PS.ProductBarcode) AS Quantity	 
	 , PP.ManufacturingDate
	 , PP.ExpiryDate
	 , P.LocationPutDate
FROM 
	Zones Z
	LEFT JOIN [dbo].[ZoneLines] ZL
		ON Z.ZoneCode = ZL.ZoneCode
	LEFT JOIN [dbo].[ZoneLocations] L
		ON ZL.ZoneCode = L.ZoneCode
		AND ZL.LineCode = L.LineCode
	LEFT JOIN [dbo].[Pallets] P
		ON P.LocationCode = L.LocationCode
		AND P.LocationCode IS NOT NULL
		AND P.LocationCode != ''
	LEFT JOIN PalletStatuses PS
		ON P.PalletCode = PS.PalletCode
	LEFT JOIN Products PR
		 ON PS.ProductID = PR.ProductID
	LEFT JOIN ProductionPlans pp
		ON PS.ProductLot = PP.ProductLot
	LEFT JOIN [dbo].[ProductPackings] PPS
		ON PR.ProductID = PPS.ProductID
		AND PPS.Type != 'P'
WHERE
	P.PalletCode IS NOT NULL
GROUP BY
	Z.ZoneName
	 , ZL.LineName
	 , L.LocationCode
	 , P.PalletCode
	 , PS.ProductLot
	 , PR.ProductCode
	 , PR.Description-- AS ProductName
	 , PR.UOM
	 , PPS.Quantity-- AS PackSize
	 , PP.ManufacturingDate
	 , PP.ExpiryDate
	 , P.LocationPutDate
ORDER BY 
	Z.ZoneName
	 , ZL.LineName
	 , L.LocationCode