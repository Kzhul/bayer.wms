SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [_ExcelLocation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FullPath] [nvarchar](550) NULL,
	[FileName] [nvarchar](50) NULL,
	[SheetName] [nvarchar](50) NULL,
	[ItemCode] [nvarchar](50) NULL,
	[ItemName] [nvarchar](50) NULL,
	[PackagingSize] [nvarchar](50) NULL,
	[Unit] [nvarchar](50) NULL,
	[Balance] [nvarchar](50) NULL,
	[Quantity] [nvarchar](50) NULL,
	[ProductLot] [nvarchar](50) NULL,
	[Expirydate] [nvarchar](50) NULL,
	[Location] [nvarchar](50) NULL,
	[Change] [nvarchar](500) NULL,
	[Remark] [nvarchar](500) NULL,
	[CreatedDateTime] [datetime] NULL,
	[RowVersion] [timestamp] NULL,
 CONSTRAINT [PK__ExcelLocation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [_ExcelPalletProductLot](
	[PalletCode] [nvarchar](50) NOT NULL,
	[ProductLot] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK__ExcelPalletProductLot] PRIMARY KEY CLUSTERED 
(
	[PalletCode] ASC,
	[ProductLot] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [_ExcelLocation] ON 

GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456233', N'57456233', N'AGR CHICKEN-BREEDER-25KG BG', N'25KG', N'PCE', N'30', N'6', N'171117B05', N'11/17/2018', N'', N'', N'', CAST(N'2017-12-01 00:55:43.893' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (2, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456233', N'57456233', N'AGR CHICKEN-BREEDER-25KG BG', N'25KG', N'PCE', N'30', N'30', N'171117B06', N'11/17/2018', N'T1-23', N'', N'', CAST(N'2017-12-01 00:55:43.973' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (3, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456179', N'57456179', N'AGR PIG -STARTER - 25KG BG', N'25kg', N'PCE', N'0', N'16', N'171122B09', N'#N/A', N'', N'', N'', CAST(N'2017-12-01 00:55:58.523' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (4, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456195', N'57456195', N'AGR PIG-BREEDER - 25KG BG', N'25KG', N'PCE', N'0', N'28', N'171124B23', N'#N/A', N'', N'', N'', CAST(N'2017-12-01 00:56:01.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (5, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'82649670', N'82649670', N'AQUA C FISH PLUS - 20 KG BAG', N'20KG', N'PCE', N'40', N'21', N'171109A03', N'11/9/2019', N'T1-22', N'', N'', CAST(N'2017-12-01 00:56:04.567' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (6, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'82649670', N'82649670', N'AQUA C FISH PLUS - 20 KG BAG', N'20KG', N'PCE', N'40', N'40', N'171109A04', N'11/9/2019', N'U1-06', N'', N'', CAST(N'2017-12-01 00:56:04.643' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (7, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81059055', N'81059055', N'BAPHASE 5000 25KG', N'25KG', N'PCE', N'7', N'7', N'171110B02', N'11/10/2018', N'X1-03', N'', N'', CAST(N'2017-12-01 00:56:10.663' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (8, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81059055', N'81059055', N'BAPHASE 5000 25KG', N'25KG', N'PCE', N'30', N'30', N'171116B01', N'11/16/2018', N'V1-18', N'', N'', CAST(N'2017-12-01 00:56:10.800' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (9, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'82648682', N'82648682', N'BATENOID RED 1% 25KG BAG', N'25KG', N'PCE', N'0', N'28', N'171122B06', N'#N/A', N'T2-26', N'', N'', CAST(N'2017-12-01 00:56:15.500' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (10, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'82648682', N'82648682', N'BATENOID RED 1% 25KG BAG', N'25KG', N'PCE', N'0', N'30', N'171122B07', N'#N/A', N'U2-24', N'', N'', CAST(N'2017-12-01 00:56:15.573' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (11, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57533114', N'57533114', N'BAYMET - 20KG BAG', N'20KG', N'PCE', N'40', N'4', N'171117A05', N'11/17/2019', N'X1-24', N'', N'', CAST(N'2017-12-01 00:56:28.360' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (12, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57533114', N'57533114', N'BAYMET - 20KG BAG', N'20KG', N'PCE', N'40', N'20', N'171117A06', N'11/17/2019', N'X1-15', N'', N'', CAST(N'2017-12-01 00:56:28.437' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (13, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57533114', N'57533114', N'BAYMET - 20KG BAG', N'20KG', N'PCE', N'', N'20', N'171117A06', N'11/17/2019', N'V1-17', N'', N'', CAST(N'2017-12-01 00:56:28.590' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (14, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57533114', N'57533114', N'BAYMET - 20KG BAG', N'20KG', N'PCE', N'1', N'1', N'170905A05', N'9/5/2019', N'V1-01', N'', N'', CAST(N'2017-12-01 00:56:28.890' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (15, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57533114', N'57533114', N'BAYMET - 20KG BAG', N'20KG', N'PCE', N'1', N'1', N'170905A06', N'9/5/2019', N'V1-01', N'', N'', CAST(N'2017-12-01 00:56:28.993' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (16, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57458244', N'57458244', N'BAYMIX VITAMIN E+-25KG BAG', N'25KG', N'PCE', N'30', N'30', N'171109V16', N'11/9/2018', N'U2-09', N'', N'', CAST(N'2017-12-01 00:56:30.300' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (17, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57455849', N'57455849', N'BAZYME P - 25KG BAG', N'25KG', N'PCE', N'30', N'13', N'171111V01', N'11/11/2018', N'', N'', N'', CAST(N'2017-12-01 00:56:36.930' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (18, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57455849', N'57455849', N'BAZYME P - 25KG BAG', N'25KG', N'PCE', N'30', N'30', N'171111V02', N'11/11/2018', N'X1-04', N'', N'', CAST(N'2017-12-01 00:56:37.017' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (19, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57455849', N'57455849', N'BAZYME P - 25KG BAG', N'25KG', N'PCE', N'1', N'1', N'170925B04', N'9/25/2018', N'X1-07', N'', N'', CAST(N'2017-12-01 00:56:37.107' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (20, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456802', N'57456802', N'BAYMIX BIOTIN PLUS - 25KG BAG', N'25KG', N'PCE', N'0', N'7', N'171118B01', N'#N/A', N'', N'', N'', CAST(N'2017-12-01 00:56:40.237' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (21, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456802', N'57456802', N'BAYMIX BIOTIN PLUS - 25KG BAG', N'25KG', N'PCE', N'0', N'30', N'171118V01', N'#N/A', N'X1-14', N'', N'', CAST(N'2017-12-01 00:56:40.317' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (22, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456802', N'57456802', N'BAYMIX BIOTIN PLUS - 25KG BAG', N'25KG', N'PCE', N'0', N'30', N'171125B01', N'#N/A', N'U2-10', N'', N'', CAST(N'2017-12-01 00:56:40.397' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (23, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81666555', N'81666555', N'GROWMIX SHRIMP - 25KG BAG', N'25KG', N'PCE', N'15', N'5', N'171020B05', N'10/20/2018', N'V1-08', N'', N'', CAST(N'2017-12-01 00:56:48.933' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (24, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'80600497', N'80600497', N'HADACLEAN A - 20KG BAG', N'20KG', N'PCE', N'24', N'23', N'170818F06', N'6/22/2019', N'X1-26', N'', N'', CAST(N'2017-12-01 00:56:56.060' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (25, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'80408455', N'80408455', N'IMMUNO MAX PLASMA REPLACER 20KG', N'20KG', N'PCE', N'9', N'9', N'171028B12', N'4/29/2019', N'V1-02', N'', N'', CAST(N'2017-12-01 00:57:04.000' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (26, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'80608543', N'80608543', N'MEGABIC - 25 KG BAG', N'25KG', N'PCE', N'30', N'22', N'171111B04', N'5/13/2019', N'U1-04', N'Bao bi moi', N'', CAST(N'2017-12-01 00:57:06.747' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (27, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57459860', N'57459860', N'ORGA-FER-25KG BAG', N'25KG', N'PCE', N'0', N'11', N'171125V01', N'#N/A', N'duoi nen', N'', N'', CAST(N'2017-12-01 00:57:14.160' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (28, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'82187066', N'82187066', N'OSAMET SHRIMP - 10KG BAG', N'10KG', N'PCE', N'36', N'36', N'170927A05', N'9/27/2019', N'V1-23', N'', N'', CAST(N'2017-12-01 00:57:18.480' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (29, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'40', N'171118V05', N'#N/A', N'X2-23', N'', N'HANG XK', CAST(N'2017-12-01 00:57:30.863' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (30, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'10', N'171118V05', N'#N/A', N'V2-23', N'', N'HANG XK', CAST(N'2017-12-01 00:57:30.953' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (31, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'35', N'171125V02', N'#N/A', N'U2-05', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.043' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (32, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'40', N'171125V02', N'#N/A', N'X1-25', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.137' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (33, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'40', N'171125V03', N'#N/A', N'V1-16', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.313' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (34, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'35', N'171125V03', N'#N/A', N'X1-17', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.393' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (35, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'35', N'171125V04', N'#N/A', N'X1-20', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.480' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (36, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'40', N'171125V04', N'#N/A', N'X1-18', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.570' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (37, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'40', N'171125V05', N'#N/A', N'X1-22', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.643' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (38, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'35', N'171125V05', N'#N/A', N'X1-13', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.720' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (39, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'35', N'171125V06', N'#N/A', N'V2-26', N'', N'HANG XK', CAST(N'2017-12-01 00:57:31.953' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (40, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'40', N'171125V06', N'#N/A', N'X1-29', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.027' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (41, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'35', N'171125V07', N'#N/A', N'V2-17', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.107' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (42, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'40', N'171125V07', N'#N/A', N'V2-22', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.197' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (43, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'35', N'171125V08', N'#N/A', N'V2-16', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.273' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (44, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'40', N'171125V08', N'#N/A', N'V2-12', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.357' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (45, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'35', N'171125V09', N'#N/A', N'X1-09', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.470' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (46, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'40', N'171125V09', N'#N/A', N'V1-19', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.617' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (47, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'0', N'35', N'171125V10', N'#N/A', N'X1-10', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.727' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (48, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81418307', N'81418307', N'STOMI - 10KG BAG', N'10KG', N'PCE', N'', N'40', N'171125V10', N'#N/A', N'V2-13', N'', N'HANG XK', CAST(N'2017-12-01 00:57:32.810' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (49, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'82187058', N'82187058', N'COFORTA A - 10KG BAG', N'10KG', N'PCE', N'29', N'13', N'171114V15', N'11/14/2019', N'T1-26', N'', N'', CAST(N'2017-12-01 00:57:36.103' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (50, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81828768', N'81828768', N'BAYMIX AQUALASE PLUS - 15KG', N'15KG', N'PCE', N'53', N'14', N'171108A01', N'2/7/2019', N'V1-03', N'', N'', CAST(N'2017-12-01 00:57:44.480' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (51, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81828768', N'81828768', N'BAYMIX AQUALASE PLUS - 15KG', N'15KG', N'PCE', N'53', N'25', N'171108A02', N'2/7/2019', N'V1-27', N'', N'', CAST(N'2017-12-01 00:57:44.560' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (52, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81828768', N'81828768', N'BAYMIX AQUALASE PLUS - 15KG', N'15KG', N'PCE', N'', N'28', N'171108A02', N'2/7/2019', N'X2-25', N'', N'', CAST(N'2017-12-01 00:57:44.700' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (53, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57533254', N'57533254', N'VIRKON A - 25KG BAG', N'25KG', N'PCE', N'0', N'1', N'171118F02', N'#N/A', N'', N'', N'', CAST(N'2017-12-01 00:57:48.563' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (54, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57533254', N'57533254', N'VIRKON A - 25KG BAG', N'25KG', N'PCE', N'0', N'30', N'171125F03', N'#N/A', N'X1-19', N'', N'', CAST(N'2017-12-01 00:57:48.650' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (55, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57533254', N'57533254', N'VIRKON A - 25KG BAG', N'25KG', N'PCE', N'', N'30', N'171125F03', N'#N/A', N'V1-13', N'', N'', CAST(N'2017-12-01 00:57:48.770' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (56, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456071', N'57456071', N'ORGA-BREED - 25KG BAG', N'25 KG', N'PCE', N'0', N'21', N'171123B01', N'#N/A', N'V1-24', N'', N'', CAST(N'2017-12-01 00:57:56.110' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (57, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'82019007', N'82019007', N'AQUA C - 25KG BAG', N'25KG', N'PCE', N'31', N'5', N'171106A01', N'11/6/2019', N'V1-05', N'', N'', CAST(N'2017-12-01 00:58:03.507' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (58, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'85090577', N'85090577', N'SUPASTOCK 20KG BAG', N'20KG', N'PCE', N'0', N'10', N'171120B09', N'#N/A', N'U1-18', N'', N'', CAST(N'2017-12-01 00:58:11.127' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (59, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'85090577', N'85090577', N'SUPASTOCK 20KG BAG', N'20KG', N'PCE', N'0', N'34', N'171120B10', N'#N/A', N'U2-03', N'', N'', CAST(N'2017-12-01 00:58:11.213' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (60, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'81576424', N'81576424', N'AQUADOR - 20KG BAG', N'20 KG', N'PCE', N'25', N'24', N'171111B01', N'11/11/2019', N'V1-15', N'', N'', CAST(N'2017-12-01 00:58:18.653' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (61, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'80615965', N'80615965', N'BAGROVIT - 25 KG BAG', N'25KG', N'PCE', N'22', N'10', N'170602B13', N'6/2/2018', N'X1-23', N'', N'', CAST(N'2017-12-01 00:58:39.740' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (62, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'80615965', N'80615965', N'BAGROVIT - 25 KG BAG', N'25KG', N'PCE', N'0', N'30', N'171121B12', N'#N/A', N'U1-26', N'', N'', CAST(N'2017-12-01 00:58:39.830' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (63, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456241', N'57456241', N'AGR QUAIL-LAYER - 25KG BG', N'25KG', N'PCE', N'30', N'30', N'170911B23', N'9/11/2018', N'V1-12', N'Bao bi moi', N'', CAST(N'2017-12-01 00:58:55.223' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (64, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'57456241', N'57456241', N'AGR QUAIL-LAYER - 25KG BG', N'25KG', N'PCE', N'9', N'9', N'170911B22', N'9/11/2018', N'V1-07', N'Bao bi moi', N'', CAST(N'2017-12-01 00:58:55.337' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (65, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'24', N'171111B01', N'11/11/2019', N'V1-15', N'', N'', CAST(N'2017-12-01 00:59:05.677' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (66, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'5', N'171020B05', N'10/20/2018', N'V1-08', N'', N'', CAST(N'2017-12-01 00:59:05.747' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (67, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:05.847' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (68, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:05.930' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (69, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'5', N'171106A01', N'11/6/2019', N'V1-05', N'', N'', CAST(N'2017-12-01 00:59:06.007' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (70, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'13', N'171114V15', N'11/14/2019', N'T1-26', N'', N'', CAST(N'2017-12-01 00:59:06.087' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (71, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:06.217' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (72, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'28', N'171122B06', N'#N/A', N'T2-26', N'', N'', CAST(N'2017-12-01 00:59:06.307' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (73, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'X1-06', N'', N'', CAST(N'2017-12-01 00:59:06.387' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (74, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'13', N'171111V01', N'11/11/2018', N'0', N'', N'', CAST(N'2017-12-01 00:59:06.487' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (75, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'30', N'171117B06', N'11/17/2018', N'T1-23', N'', N'', CAST(N'2017-12-01 00:59:06.567' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (76, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'9', N'170911B22', N'9/11/2018', N'V1-07', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:06.647' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (77, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:06.733' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (78, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'0', N'#REF!', N'#REF!', N'0', N'', N'', CAST(N'2017-12-01 00:59:06.817' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (79, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'30', N'171116B01', N'11/16/2018', N'V1-18', N'', N'', CAST(N'2017-12-01 00:59:06.907' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (80, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'Q1-17', N'', N'', CAST(N'2017-12-01 00:59:07.030' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (81, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'V1-03', N'', N'', CAST(N'2017-12-01 00:59:07.107' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (82, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:07.197' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (83, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'36', N'170927A05', N'9/27/2019', N'V1-23', N'', N'', CAST(N'2017-12-01 00:59:07.287' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (84, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:07.367' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (85, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'30', N'171111V02', N'11/11/2018', N'X1-04', N'', N'', CAST(N'2017-12-01 00:59:07.470' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (86, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:07.543' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (87, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:07.650' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (88, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'O3-19', N'', N'', CAST(N'2017-12-01 00:59:07.727' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (89, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:07.800' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (90, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:07.913' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (91, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'21', N'171109A03', N'11/9/2019', N'T1-22', N'', N'', CAST(N'2017-12-01 00:59:07.990' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (92, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:08.063' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (93, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:08.143' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (94, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'O3-17', N'', N'', CAST(N'2017-12-01 00:59:08.273' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (95, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'14', N'171108A01', N'2/7/2019', N'V1-03', N'', N'', CAST(N'2017-12-01 00:59:08.350' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (96, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:08.440' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (97, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'40', N'171109A04', N'11/9/2019', N'U1-06', N'', N'', CAST(N'2017-12-01 00:59:08.537' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (98, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:08.607' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (99, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:08.687' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (100, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'0', N'', N'', CAST(N'2017-12-01 00:59:08.783' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (101, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'25', N'171108A02', N'2/7/2019', N'V1-27', N'', N'', CAST(N'2017-12-01 00:59:08.863' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (102, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'0', N'0', N'#N/A', N'0', N'', N'', CAST(N'2017-12-01 00:59:08.980' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (103, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:09.063' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (104, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:09.147' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (105, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'28', N'171108A02', N'2/7/2019', N'X2-25', N'', N'', CAST(N'2017-12-01 00:59:09.233' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (106, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:09.310' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (107, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'4', N'171117A05', N'11/17/2019', N'X1-24', N'', N'', CAST(N'2017-12-01 00:59:09.420' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (108, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:09.530' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (109, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:09.613' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (110, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 00:59:09.707' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (111, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'Z5-27', N'', N'', CAST(N'2017-12-01 00:59:09.787' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (112, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'20', N'171117A06', N'11/17/2019', N'X1-15', N'', N'', CAST(N'2017-12-01 00:59:09.883' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (113, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'Z3-24', N'', N'', CAST(N'2017-12-01 00:59:09.967' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (114, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'20', N'171117A06', N'11/17/2019', N'V1-17', N'', N'', CAST(N'2017-12-01 00:59:10.083' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (115, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'Z2-22', N'', N'', CAST(N'2017-12-01 00:59:10.160' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (116, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG BAO.xlsx', N'THE KHO-THE DINH VI HANG BAO.xlsx', N'Location', N'#REF!', N'#REF!', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'Z4-21', N'', N'', CAST(N'2017-12-01 00:59:10.250' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (117, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57529486', N'57529486', N'ADVOCATE DOG 4-10KG-FG', N'10', N'PCE', N'127', N'127', N'170405F06', N'10/31/2018', N'Q1-01', N'BAO BI MOI', N'', CAST(N'2017-12-01 00:59:10.950' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (118, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57529486', N'57529486', N'ADVOCATE DOG 4-10KG-FG', N'10', N'PCE', N'956', N'956', N'171010F06', N'2/28/2019', N'Q2-19', N'BAO BI MOI', N'', CAST(N'2017-12-01 00:59:11.053' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (119, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81835846', N'81835846', N'ADVOCATE DOG 10-25 KG (FG)', N'12', N'PCE', N'697', N'682', N'170901F08', N'3/31/2019', N'Q1-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 00:59:17.950' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (120, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81835846', N'81835846', N'ADVOCATE DOG 10-25 KG (FG)', N'12', N'PCE', N'472', N'472', N'171010F07', N'6/30/2019', N'O2-12', N'BAO BI MOI', N'', CAST(N'2017-12-01 00:59:18.027' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (121, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81835846', N'81835846', N'ADVOCATE DOG 10-25 KG (FG)', N'12', N'PCE', N'358', N'358', N'171010F08', N'6/30/2019', N'O2-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 00:59:18.127' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (122, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'9', N'9', N'160827R01', N'4/18/2018', N'P1-26', N'', N'hàng bán KM', CAST(N'2017-12-01 00:59:25.397' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (123, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'17204', N'575', N'170530R04', N'2/16/2019', N'O1-11', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:25.473' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (124, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'1799', N'170530R04', N'2/16/2019', N'O3-05', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:25.560' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (125, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170530R04', N'2/16/2019', N'P3-03', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:25.660' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (126, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170530R04', N'2/16/2019', N'O3-04', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:25.760' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (127, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170530R04', N'2/16/2019', N'P2-11', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:25.833' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (128, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170530R04', N'2/16/2019', N'P3-13', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:25.933' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (129, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170530R04', N'2/16/2019', N'O1-25', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:26.023' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (130, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'360', N'170530R04', N'2/16/2019', N'P3-17', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:26.123' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (131, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'214', N'170530R04', N'2/16/2019', N'Q1-04', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:26.230' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (132, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'19220', N'1076', N'170810R01', N'4/21/2019', N'O2-08', N'', N'', CAST(N'2017-12-01 00:59:26.360' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (133, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R01', N'4/21/2019', N'O3-14', N'', N'', CAST(N'2017-12-01 00:59:26.437' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (134, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R01', N'4/21/2019', N'P2-16', N'', N'', CAST(N'2017-12-01 00:59:26.553' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (135, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R01', N'4/21/2019', N'O2-16', N'', N'', CAST(N'2017-12-01 00:59:26.627' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (136, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R01', N'4/21/2019', N'P2-19', N'', N'', CAST(N'2017-12-01 00:59:26.710' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (137, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R01', N'4/21/2019', N'Q2-18', N'', N'', CAST(N'2017-12-01 00:59:26.800' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (138, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R01', N'4/21/2019', N'O2-18', N'', N'', CAST(N'2017-12-01 00:59:26.890' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (139, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R01', N'4/21/2019', N'P2-20', N'', N'', CAST(N'2017-12-01 00:59:26.963' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (140, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'10718', N'352', N'170810R02', N'4/25/2019', N'P3-01', N'', N'', CAST(N'2017-12-01 00:59:27.043' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (141, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2590', N'170810R02', N'4/25/2019', N'O3-12', N'', N'', CAST(N'2017-12-01 00:59:27.120' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (142, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R02', N'4/25/2019', N'Q3-20', N'', N'', CAST(N'2017-12-01 00:59:27.260' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (143, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R02', N'4/25/2019', N'Q2-22', N'', N'', CAST(N'2017-12-01 00:59:27.340' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (144, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'170810R02', N'4/25/2019', N'O2-25', N'', N'', CAST(N'2017-12-01 00:59:27.413' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (145, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'19258', N'2592', N'171107R01', N'7/10/2019', N'P2-12', N'', N'', CAST(N'2017-12-01 00:59:27.500' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (146, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'171107R01', N'7/10/2019', N'P2-10', N'', N'', CAST(N'2017-12-01 00:59:27.573' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (147, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'171107R01', N'7/10/2019', N'O3-08', N'', N'', CAST(N'2017-12-01 00:59:27.690' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (148, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'171107R01', N'7/10/2019', N'P3-20', N'', N'', CAST(N'2017-12-01 00:59:27.787' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (149, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'171107R01', N'7/10/2019', N'P2-23', N'', N'', CAST(N'2017-12-01 00:59:27.870' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (150, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'171107R01', N'7/10/2019', N'P2-03', N'', N'', CAST(N'2017-12-01 00:59:27.950' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (151, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'2592', N'171107R01', N'7/10/2019', N'O3-10', N'', N'', CAST(N'2017-12-01 00:59:28.030' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (152, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459151', N'57459151', N'AMOXISOL LA - 100 ML BOTTLE', N'72', N'PCE', N'', N'1114', N'171107R01', N'7/10/2019', N'Q2-02', N'', N'', CAST(N'2017-12-01 00:59:28.130' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (153, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81443521', N'81443521', N'AQUI S 100ML', N'60', N'PCE', N'259', N'259', N'160729R01', N'5/24/2019', N'Q3-11', N'BAO BI MOI', N'', CAST(N'2017-12-01 00:59:29.500' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (154, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81443521', N'81443521', N'AQUI S 100ML', N'60', N'PCE', N'357', N'357', N'170306R04', N'5/15/2019', N'Q1-06', N'BAO BI MOI', N'Qc lấy mẫu 2, Navet I lay mau 3', CAST(N'2017-12-01 00:59:29.583' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (155, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80641303', N'80641303', N'BAYTRIL 10% 1000 ML (EXP VN)', N'12', N'PCE', N'928', N'384', N'170403R04', N'1/6/2020', N'O1-14', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:36.287' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (156, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80641303', N'80641303', N'BAYTRIL 10% 1000 ML (EXP VN)', N'12', N'PCE', N'', N'163', N'170403R04', N'1/6/2020', N'Q1-11', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:36.520' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (157, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80641303', N'80641303', N'BAYTRIL 10% 1000 ML (EXP VN)', N'12', N'PCE', N'', N'213', N'170403R04', N'1/6/2020', N'P2-02', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:36.620' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (158, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84667676', N'84667676', N'BAYCOX 2.5% 100ML   (EXP-VN)', N'30', N'PCE', N'7423', N'1920', N'170313R07', N'2/6/2019', N'O3-18', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:49.293' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (159, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84667676', N'84667676', N'BAYCOX 2.5% 100ML   (EXP-VN)', N'30', N'PCE', N'', N'1380', N'170313R07', N'2/6/2019', N'O2-27', N'Bao bi moi', N'Navet II lấy mẫu 2
Qc lấy mẫu 2', CAST(N'2017-12-01 00:59:49.750' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (160, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84667676', N'84667676', N'BAYCOX 2.5% 100ML   (EXP-VN)', N'30', N'PCE', N'', N'1983', N'170313R07', N'2/6/2019', N'Duoi nen', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:49.827' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (161, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84667676', N'84667676', N'BAYCOX 2.5% 100ML   (EXP-VN)', N'30', N'PCE', N'', N'1800', N'170313R07', N'2/6/2019', N'Q1-07', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:49.907' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (162, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600063', N'80600063', N'BAYCOX 5% SUSP - 100ML BOTTLE', N'60', N'PCE', N'4318', N'1440', N'170612F01', N'1/12/2022', N'P1-23', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:55.183' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (163, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600063', N'80600063', N'BAYCOX 5% SUSP - 100ML BOTTLE', N'60', N'PCE', N'', N'1440', N'170612F01', N'1/12/2022', N'O1-22', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:55.270' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (164, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600063', N'80600063', N'BAYCOX 5% SUSP - 100ML BOTTLE', N'60', N'PCE', N'', N'480', N'170612F01', N'1/12/2022', N'Duoi SX', N'', N'', CAST(N'2017-12-01 00:59:55.353' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (165, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600063', N'80600063', N'BAYCOX 5% SUSP - 100ML BOTTLE', N'60', N'PCE', N'', N'799', N'170612F01', N'1/12/2022', N'O1-24', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:55.450' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (166, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600063', N'80600063', N'BAYCOX 5% SUSP - 100ML BOTTLE', N'60', N'PCE', N'0', N'838', N'171118F01', N'#N/A', N'Q1-09', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:55.530' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (167, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600063', N'80600063', N'BAYCOX 5% SUSP - 100ML BOTTLE', N'60', N'PCE', N'', N'1500', N'171118F01', N'#N/A', N'P3-15', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:55.607' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (168, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600063', N'80600063', N'BAYCOX 5% SUSP - 100ML BOTTLE', N'60', N'PCE', N'', N'1920', N'171118F01', N'#N/A', N'P1-21', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:55.753' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (169, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'21', N'21', N'161124F04', N'9/13/2018', N'P1-26', N'', N'Ban Khuyen mai', CAST(N'2017-12-01 00:59:59.397' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (170, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'1', N'1', N'170105F07', N'11/3/2018', N'P1-26', N'', N'Ban Khuyen mai', CAST(N'2017-12-01 00:59:59.473' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (171, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'1', N'1', N'170415F03', N'12/22/2018', N'P1-26', N'', N'Ban Khuyen mai', CAST(N'2017-12-01 00:59:59.560' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (172, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'8', N'8', N'170417F08', N'1/3/2019', N'P1-26', N'', N'Ban Khuyen mai', CAST(N'2017-12-01 00:59:59.660' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (173, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'3392', N'1800', N'170605F08', N'2/7/2019', N'O2-14', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:59.740' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (174, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'', N'1078', N'170605F08', N'2/7/2019', N'Q1-10', N'Bao bi moi', N'', CAST(N'2017-12-01 00:59:59.817' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (175, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'4928', N'2400', N'170628F02', N'1/5/2019', N'O3-24', N'', N'', CAST(N'2017-12-01 00:59:59.923' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (176, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'', N'2528', N'170628F02', N'1/5/2019', N'O3-15', N'', N'', CAST(N'2017-12-01 00:59:59.990' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (177, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'20', N'20', N'170321F05', N'12/22/2018', N'P1-26', N'', N'', CAST(N'2017-12-01 01:00:00.067' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (178, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600187', N'80600187', N'BAYTRIL 0.5% ORAL - 100ML BT', N'20', N'PCE', N'20', N'20', N'170415F02', N'12/22/2018', N'P1-26', N'', N'', CAST(N'2017-12-01 01:00:00.167' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (179, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600195', N'80600195', N'BAYTRIL 10% ORAL - 100ML BT', N'40', N'PCE', N'527', N'527', N'151225F05', N'6/30/2018', N'Q1-12', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:03.747' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (180, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80600195', N'80600195', N'BAYTRIL 10% ORAL - 100ML BT', N'40', N'PCE', N'1798', N'1798', N'151226F02', N'6/30/2018', N'Q3-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:03.810' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (181, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541245', N'80541245', N'BAYTRIL 25 INJ.(EXP-VN) 50ML', N'60', N'PCE', N'1902', N'1782', N'170221R01', N'1/11/2019', N'Q1-13', N'Bao bi moi', N'', CAST(N'2017-12-01 01:00:09.797' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (182, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541245', N'80541245', N'BAYTRIL 25 INJ.(EXP-VN) 50ML', N'60', N'PCE', N'3598', N'3598', N'170327R05', N'2/19/2019', N'Q2-12', N'Bao bi moi', N'', CAST(N'2017-12-01 01:00:10.100' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (183, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541245', N'80541245', N'BAYTRIL 25 INJ.(EXP-VN) 50ML', N'60', N'PCE', N'5129', N'2249', N'170612R04', N'2/19/2019', N'P2-07', N'Bao bi moi', N'', CAST(N'2017-12-01 01:00:10.177' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (184, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541245', N'80541245', N'BAYTRIL 25 INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'2880', N'170612R04', N'2/19/2019', N'P2-05', N'Bao bi moi', N'', CAST(N'2017-12-01 01:00:10.260' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (185, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541245', N'80541245', N'BAYTRIL 25 INJ.(EXP-VN) 50ML', N'60', N'PCE', N'9195', N'3600', N'171115R06', N'6/18/2019', N'O3-26', N'', N'', CAST(N'2017-12-01 01:00:10.430' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (186, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541245', N'80541245', N'BAYTRIL 25 INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'3600', N'171115R06', N'6/18/2019', N'O1-27', N'', N'', CAST(N'2017-12-01 01:00:10.500' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (187, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541245', N'80541245', N'BAYTRIL 25 INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'1995', N'171115R06', N'6/18/2019', N'P1-13', N'', N'', CAST(N'2017-12-01 01:00:10.577' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (188, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'7426', N'2040', N'170110R01', N'5/25/2019', N'Q3-05', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:15.700' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (189, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'', N'2080', N'170110R01', N'5/25/2019', N'Q3-12', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:15.770' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (190, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'', N'2080', N'170110R01', N'5/25/2019', N'Q3-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:15.887' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (191, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'', N'705', N'170110R01', N'5/25/2019', N'Q1-14', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:15.953' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (192, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'', N'11', N'170110R01', N'5/25/2019', N'P1-26', N'11 Chai Loi', N'', CAST(N'2017-12-01 01:00:16.027' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (193, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'1286', N'1286', N'170411R07', N'10/30/2019', N'O2-22', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:16.107' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (194, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'2878', N'1960', N'170411R08', N'11/1/2019', N'O1-15', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:16.207' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (195, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'', N'918', N'170411R08', N'11/1/2019', N'O3-25', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:16.297' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (196, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'4158', N'2080', N'170504R01', N'12/12/2019', N'P2-06', N'', N'', CAST(N'2017-12-01 01:00:16.390' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (197, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'622900', N'622900', N'BAYTRIL INJ SOL 5% 100 ML', N'40', N'PCE', N'', N'2078', N'170504R01', N'12/12/2019', N'O3-06', N'', N'', CAST(N'2017-12-01 01:00:16.497' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (198, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'8318', N'1478', N'170113R01', N'8/21/2019', N'Q2-20', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:20.990' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (199, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'', N'1920', N'170113R01', N'8/21/2019', N'Q2-23', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:21.057' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (200, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'', N'680', N'170113R01', N'8/21/2019', N'Q1-15', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:21.137' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (201, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'', N'1920', N'170113R01', N'8/21/2019', N'Q2-17', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:21.263' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (202, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'', N'1960', N'170113R01', N'8/21/2019', N'Q2-16', N'bao bi moi', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:00:21.330' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (203, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'2078', N'2078', N'170314R02', N'9/9/2019', N'P3-18', N'bao bi moi', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:00:21.410' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (204, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'4156', N'2080', N'170411R06', N'9/27/2019', N'O2-07', N'Data logger Pallet 3', N'', CAST(N'2017-12-01 01:00:21.497' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (205, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'', N'2076', N'170411R06', N'9/27/2019', N'Q3-03', N'Data logger Pallet 4', N'Qc lấy mẫu 2 Navet II lấy mẫu 2', CAST(N'2017-12-01 01:00:21.560' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (206, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'4158', N'2078', N'170622R01', N'11/7/2019', N'O1-19', N'', N'', CAST(N'2017-12-01 01:00:21.647' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (207, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4031338', N'4031338', N'BAYTRIL MAX 10%          100ML', N'40', N'PCE', N'', N'2080', N'170622R01', N'11/7/2019', N'P1-22', N'', N'', CAST(N'2017-12-01 01:00:21.760' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (208, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'6175', N'1920', N'170327R04', N'2/3/2020', N'O3-07', N'', N'', CAST(N'2017-12-01 01:00:26.913' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (209, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'', N'229', N'170327R04', N'2/3/2020', N'Q1-16', N'Bao bi moi', N'', CAST(N'2017-12-01 01:00:27.113' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (210, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'6187', N'120', N'170511R01', N'3/8/2020', N'O3-21', N'Bao bi moi', N'', CAST(N'2017-12-01 01:00:27.197' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (211, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'', N'1920', N'170511R01', N'3/8/2020', N'O2-13', N'Bao bi moi', N'', CAST(N'2017-12-01 01:00:27.347' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (212, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'', N'1920', N'170511R01', N'3/8/2020', N'O1-16', N'Bao bi moi', N'', CAST(N'2017-12-01 01:00:27.427' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (213, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'', N'2227', N'170511R01', N'3/8/2020', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:00:27.500' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (214, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'6184', N'2400', N'170612R03', N'3/31/2020', N'duoi nen', N'', N'', CAST(N'2017-12-01 01:00:27.573' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (215, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'', N'2400', N'170612R03', N'3/31/2020', N'Q4-13', N'', N'', CAST(N'2017-12-01 01:00:27.650' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (216, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541172', N'80541172', N'CALPHON FORTE (EXP-VN) 100ML', N'60', N'PCE', N'', N'1384', N'170612R03', N'3/31/2020', N'Q3-02', N'', N'', CAST(N'2017-12-01 01:00:27.740' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (217, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'2', N'2', N'160412R01', N'3/9/2018', N'P1-27', N'', N'Hàng hold', CAST(N'2017-12-01 01:00:30.910' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (218, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'5', N'5', N'160727R04', N'6/16/2018', N'P1-26', N'', N'Bán khuyến mãi', CAST(N'2017-12-01 01:00:31.010' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (219, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10639', N'1', N'170327R01', N'2/5/2019', N'P1-26', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:31.083' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (220, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13429', N'508', N'170327R02', N'2/12/2019', N'Q1-17', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:31.180' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (221, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13425', N'2400', N'170327R03', N'2/14/2019', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:00:31.260' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (222, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1425', N'170327R03', N'2/14/2019', N'P3-12', N'', N'', CAST(N'2017-12-01 01:00:31.340' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (223, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170327R03', N'2/14/2019', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:00:31.467' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (224, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170327R03', N'2/14/2019', N'O4-24', N'', N'', CAST(N'2017-12-01 01:00:31.547' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (225, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170327R03', N'2/14/2019', N'O4-25', N'', N'', CAST(N'2017-12-01 01:00:31.623' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (226, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170327R03', N'2/14/2019', N'O4-26', N'', N'', CAST(N'2017-12-01 01:00:31.720' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (227, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13618', N'1620', N'170403R05', N'2/21/2019', N'P3-25', N'', N'', CAST(N'2017-12-01 01:00:31.793' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (228, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R05', N'2/21/2019', N'P4-07', N'', N'', CAST(N'2017-12-01 01:00:31.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (229, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2398', N'170403R05', N'2/21/2019', N'P4-19', N'', N'', CAST(N'2017-12-01 01:00:31.973' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (230, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R05', N'2/21/2019', N'O4-15', N'', N'', CAST(N'2017-12-01 01:00:32.113' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (231, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R05', N'2/21/2019', N'O4-02', N'', N'', CAST(N'2017-12-01 01:00:32.233' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (232, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R05', N'2/21/2019', N'O4-05', N'', N'', CAST(N'2017-12-01 01:00:32.380' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (233, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13638', N'1638', N'170403R06', N'2/23/2019', N'O2-17', N'', N'', CAST(N'2017-12-01 01:00:32.467' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (234, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R06', N'2/23/2019', N'P4-17', N'', N'', CAST(N'2017-12-01 01:00:32.543' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (235, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R06', N'2/23/2019', N'P4-14', N'', N'', CAST(N'2017-12-01 01:00:32.723' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (236, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R06', N'2/23/2019', N'O4-03', N'', N'', CAST(N'2017-12-01 01:00:32.793' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (237, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R06', N'2/23/2019', N'P4-22', N'', N'', CAST(N'2017-12-01 01:00:32.870' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (238, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R06', N'2/23/2019', N'O4-27', N'', N'', CAST(N'2017-12-01 01:00:32.970' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (239, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10718', N'2400', N'170403R07', N'2/24/2019', N'O4-06', N'', N'', CAST(N'2017-12-01 01:00:33.037' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (240, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R07', N'2/24/2019', N'P4-03', N'', N'', CAST(N'2017-12-01 01:00:33.113' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (241, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1118', N'170403R07', N'2/24/2019', N'O3-13', N'data logger (Pallet 12)', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:00:33.183' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (242, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R07', N'2/24/2019', N'O4-04', N'', N'', CAST(N'2017-12-01 01:00:33.353' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (243, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170403R07', N'2/24/2019', N'P4-25', N'', N'', CAST(N'2017-12-01 01:00:33.437' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (244, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'3478', N'2400', N'170511R02', N'3/22/2019', N'P4-15', N'', N'', CAST(N'2017-12-01 01:00:33.507' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (245, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1078', N'170511R02', N'3/22/2019', N'P3-09', N'', N'', CAST(N'2017-12-01 01:00:33.593' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (246, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13641', N'2400', N'170511R03', N'3/23/2019', N'Q4-16', N'', N'', CAST(N'2017-12-01 01:00:33.680' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (247, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1641', N'170511R03', N'3/23/2019', N'Q3-16', N'', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:00:33.750' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (248, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170511R03', N'3/23/2019', N'Q4-27', N'', N'', CAST(N'2017-12-01 01:00:33.833' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (249, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170511R03', N'3/23/2019', N'O4-18', N'', N'', CAST(N'2017-12-01 01:00:33.940' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (250, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170511R03', N'3/23/2019', N'P4-18', N'', N'', CAST(N'2017-12-01 01:00:34.013' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (251, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170511R03', N'3/23/2019', N'Q4-23', N'', N'', CAST(N'2017-12-01 01:00:34.120' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (252, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10638', N'1038', N'170511R04', N'3/24/2019', N'P3-14', N'', N'', CAST(N'2017-12-01 01:00:34.200' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (253, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170511R04', N'3/24/2019', N'O4-23', N'', N'', CAST(N'2017-12-01 01:00:34.293' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (254, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170511R04', N'3/24/2019', N'Q4-01', N'', N'', CAST(N'2017-12-01 01:00:34.373' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (255, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170511R04', N'3/24/2019', N'Q4-08', N'', N'', CAST(N'2017-12-01 01:00:34.440' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (256, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170511R04', N'3/24/2019', N'P4-20', N'', N'', CAST(N'2017-12-01 01:00:34.517' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (257, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13652', N'1652', N'170512R02', N'3/26/2019', N'Q3-23', N'', N'', CAST(N'2017-12-01 01:00:34.597' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (258, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R02', N'3/26/2019', N'P4-13', N'', N'', CAST(N'2017-12-01 01:00:34.707' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (259, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R02', N'3/26/2019', N'Q4-14', N'', N'', CAST(N'2017-12-01 01:00:34.843' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (260, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R02', N'3/26/2019', N'Q4-10', N'', N'', CAST(N'2017-12-01 01:00:34.917' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (261, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R02', N'3/26/2019', N'O4-14', N'', N'', CAST(N'2017-12-01 01:00:34.993' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (262, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R02', N'3/26/2019', N'Q4-15', N'', N'', CAST(N'2017-12-01 01:00:35.077' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (263, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13650', N'1650', N'170512R03', N'3/28/2019', N'Q2-26', N'', N'Qc lấy mẫu 2, Navet II
lay mau 2', CAST(N'2017-12-01 01:00:35.170' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (264, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R03', N'3/28/2019', N'Q4-19', N'', N'', CAST(N'2017-12-01 01:00:35.257' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (265, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R03', N'3/28/2019', N'Q4-17', N'', N'', CAST(N'2017-12-01 01:00:35.340' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (266, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R03', N'3/28/2019', N'O4-08', N'', N'', CAST(N'2017-12-01 01:00:35.417' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (267, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R03', N'3/28/2019', N'P4-04', N'', N'', CAST(N'2017-12-01 01:00:35.493' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (268, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R03', N'3/28/2019', N'O4-22', N'', N'', CAST(N'2017-12-01 01:00:35.607' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (269, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10684', N'2400', N'170512R05', N'3/12/2019', N'O4-16', N'', N'', CAST(N'2017-12-01 01:00:35.677' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (270, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R05', N'3/12/2019', N'O4-01', N'', N'', CAST(N'2017-12-01 01:00:35.757' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (271, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1084', N'170512R05', N'3/12/2019', N'O3-11', N'', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:00:35.857' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (272, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R05', N'3/12/2019', N'Q4-20', N'', N'', CAST(N'2017-12-01 01:00:35.940' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (273, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R05', N'3/12/2019', N'Q4-26', N'', N'', CAST(N'2017-12-01 01:00:36.030' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (274, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13624', N'2400', N'170512R06', N'3/14/2019', N'O4-20', N'', N'', CAST(N'2017-12-01 01:00:36.110' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (275, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1624', N'170512R06', N'3/14/2019', N'Q2-27', N'', N'', CAST(N'2017-12-01 01:00:36.223' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (276, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R06', N'3/14/2019', N'Q4-11', N'', N'', CAST(N'2017-12-01 01:00:36.307' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (277, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R06', N'3/14/2019', N'Q4-12', N'', N'', CAST(N'2017-12-01 01:00:36.400' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (278, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R06', N'3/14/2019', N'P4-06', N'', N'', CAST(N'2017-12-01 01:00:36.470' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (279, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R06', N'3/14/2019', N'Q4-07', N'', N'', CAST(N'2017-12-01 01:00:36.550' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (280, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13661', N'1661', N'170512R07', N'3/16/2019', N'P1-20', N'', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:00:36.633' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (281, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R07', N'3/16/2019', N'P4-26', N'', N'', CAST(N'2017-12-01 01:00:36.770' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (282, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R07', N'3/16/2019', N'P4-27', N'', N'', CAST(N'2017-12-01 01:00:36.853' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (283, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R07', N'3/16/2019', N'P4-21', N'', N'', CAST(N'2017-12-01 01:00:36.953' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (284, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R07', N'3/16/2019', N'Q4-09', N'', N'', CAST(N'2017-12-01 01:00:37.047' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (285, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R07', N'3/16/2019', N'O4-19', N'', N'', CAST(N'2017-12-01 01:00:37.140' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (286, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'7198', N'2400', N'170512R08', N'3/22/2019', N'P4-24', N'', N'', CAST(N'2017-12-01 01:00:37.330' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (287, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2398', N'170512R08', N'3/22/2019', N'P4-23', N'', N'', CAST(N'2017-12-01 01:00:37.493' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (288, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170512R08', N'3/22/2019', N'O4-13', N'', N'', CAST(N'2017-12-01 01:00:37.567' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (289, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'4448', N'400', N'170612R07', N'4/2/2019', N'P1-15', N'', N'', CAST(N'2017-12-01 01:00:37.637' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (290, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R07', N'4/2/2019', N'Q4-18', N'', N'', CAST(N'2017-12-01 01:00:37.733' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (291, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1648', N'170612R07', N'4/2/2019', N'Q2-14', N'', N'', CAST(N'2017-12-01 01:00:37.803' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (292, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'13646', N'2400', N'170612R08', N'4/5/2019', N'Q4-24', N'', N'', CAST(N'2017-12-01 01:00:37.900' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (293, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R08', N'4/5/2019', N'P4-16', N'', N'', CAST(N'2017-12-01 01:00:37.973' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (294, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R08', N'4/5/2019', N'O4-09', N'', N'', CAST(N'2017-12-01 01:00:38.080' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (295, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R08', N'4/5/2019', N'O4-07', N'', N'', CAST(N'2017-12-01 01:00:38.197' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (296, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R08', N'4/5/2019', N'Q4-06', N'', N'', CAST(N'2017-12-01 01:00:38.323' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (297, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1646', N'170612R08', N'4/5/2019', N'O2-15', N'', N'', CAST(N'2017-12-01 01:00:38.420' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (298, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10678', N'2400', N'170612R09', N'4/6/2019', N'P4-12', N'', N'', CAST(N'2017-12-01 01:00:38.500' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (299, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R09', N'4/6/2019', N'P4-11', N'', N'', CAST(N'2017-12-01 01:00:38.577' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (300, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R09', N'4/6/2019', N'O4-12', N'', N'', CAST(N'2017-12-01 01:00:38.653' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (301, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R09', N'4/6/2019', N'Q4-04', N'', N'', CAST(N'2017-12-01 01:00:38.937' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (302, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1078', N'170612R09', N'4/6/2019', N'Q3-22', N'', N'', CAST(N'2017-12-01 01:00:39.047' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (303, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10690', N'2400', N'170612R10', N'4/7/2019', N'O4-11', N'', N'', CAST(N'2017-12-01 01:00:39.167' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (304, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R10', N'4/7/2019', N'O4-10', N'', N'', CAST(N'2017-12-01 01:00:39.300' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (305, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R10', N'4/7/2019', N'P4-02', N'', N'', CAST(N'2017-12-01 01:00:39.403' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (306, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'170612R10', N'4/7/2019', N'Q4-21', N'', N'', CAST(N'2017-12-01 01:00:39.563' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (307, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1090', N'170612R10', N'4/7/2019', N'Q2-10', N'', N'', CAST(N'2017-12-01 01:00:39.700' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (308, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'3519', N'1119', N'171115R01', N'4/9/2019', N'O2-10', N'', N'', CAST(N'2017-12-01 01:00:39.797' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (309, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'171115R01', N'4/9/2019', N'P4-05', N'', N'', CAST(N'2017-12-01 01:00:39.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (310, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10646', N'1046', N'171115R02', N'5/7/2019', N'P1-12', N'', N'', CAST(N'2017-12-01 01:00:39.950' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (311, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'7200', N'171115R02', N'5/7/2019', N'duoi nen', N'', N'', CAST(N'2017-12-01 01:00:40.020' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (312, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'171115R02', N'5/7/2019', N'O4-17', N'', N'', CAST(N'2017-12-01 01:00:40.120' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (313, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10664', N'2400', N'171115R03', N'4/11/2019', N'P4-01', N'', N'', CAST(N'2017-12-01 01:00:40.193' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (314, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'171115R03', N'4/11/2019', N'Q4-03', N'', N'', CAST(N'2017-12-01 01:00:40.303' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (315, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1064', N'171115R03', N'4/11/2019', N'P3-22', N'', N'', CAST(N'2017-12-01 01:00:40.373' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (316, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'171115R03', N'4/11/2019', N'P4-08', N'', N'', CAST(N'2017-12-01 01:00:40.463' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (317, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'171115R03', N'4/11/2019', N'duoi nen', N'', N'', CAST(N'2017-12-01 01:00:40.543' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (318, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'10713', N'2400', N'171115R04', N'5/25/2019', N'O4-21', N'', N'', CAST(N'2017-12-01 01:00:40.633' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (319, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2400', N'171115R04', N'5/25/2019', N'Q4-02', N'', N'', CAST(N'2017-12-01 01:00:40.703' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (320, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'1113', N'171115R04', N'5/25/2019', N'P3-23', N'', N'', CAST(N'2017-12-01 01:00:40.870' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (321, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'4800', N'171115R04', N'5/25/2019', N'duoi nen', N'', N'', CAST(N'2017-12-01 01:00:40.943' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (322, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'4798', N'2400', N'171115R05', N'5/30/2019', N'P4-10', N'', N'', CAST(N'2017-12-01 01:00:41.020' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (323, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80537698', N'80537698', N'CATOSAL (EXP-VN)       100ML', N'60', N'PCE', N'', N'2398', N'171115R05', N'5/30/2019', N'P4-09', N'', N'', CAST(N'2017-12-01 01:00:41.093' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (324, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459429', N'57459429', N'DRONTAL FLAVOUR 16V-FG', N'30', N'PCE', N'1736', N'1736', N'171012F04', N'4/30/2022', N'Q1-19', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:41.247' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (325, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459429', N'57459429', N'DRONTAL FLAVOUR 16V-FG', N'30', N'PCE', N'2530', N'2530', N'171030F07', N'4/30/2022', N'Q3-15', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:41.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (326, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80539879', N'80539879', N'DURANIXIN LA 100 ML', N'72', N'PCE', N'1522', N'1522', N'170124R02', N'7/18/2018', N'Q1-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:48.097' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (327, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80539879', N'80539879', N'DURANIXIN LA 100 ML', N'72', N'PCE', N'4030', N'2592', N'170420R07', N'12/8/2018', N'Q2-07', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:48.163' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (328, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80539879', N'80539879', N'DURANIXIN LA 100 ML', N'72', N'PCE', N'', N'1366', N'170420R07', N'12/8/2018', N'P1-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:48.250' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (329, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80539879', N'80539879', N'DURANIXIN LA 100 ML', N'72', N'PCE', N'6044', N'2592', N'170706R04', N'3/20/2019', N'O3-27', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:48.327' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (330, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80539879', N'80539879', N'DURANIXIN LA 100 ML', N'72', N'PCE', N'', N'864', N'170706R04', N'3/20/2019', N'O1-26', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:48.453' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (331, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80539879', N'80539879', N'DURANIXIN LA 100 ML', N'72', N'PCE', N'', N'2588', N'170706R04', N'3/20/2019', N'O1-23', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:48.597' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (332, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80539879', N'80539879', N'DURANIXIN LA 100 ML', N'72', N'PCE', N'5038', N'2592', N'171107R02', N'4/11/2019', N'Q2-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:48.693' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (333, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80539879', N'80539879', N'DURANIXIN LA 100 ML', N'72', N'PCE', N'', N'2446', N'171107R02', N'4/11/2019', N'Q3-10', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:00:48.777' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (334, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80608497', N'80608497', N'KILTIX COLLAR LARGE 1 PC - FG', N'10', N'PCE', N'373', N'373', N'170211F02', N'6/16/2021', N'Q1-21', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:54.687' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (335, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80608497', N'80608497', N'KILTIX COLLAR LARGE 1 PC - FG', N'10', N'PCE', N'3051', N'900', N'171009F06', N'12/2/2021', N'Q2-11', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:54.757' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (336, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80608497', N'80608497', N'KILTIX COLLAR LARGE 1 PC - FG', N'10', N'PCE', N'', N'900', N'171009F06', N'12/2/2021', N'Q3-07', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:54.823' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (337, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80608497', N'80608497', N'KILTIX COLLAR LARGE 1 PC - FG', N'10', N'PCE', N'', N'900', N'171009F06', N'12/2/2021', N'Q3-06', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:54.920' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (338, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80608497', N'80608497', N'KILTIX COLLAR LARGE 1 PC - FG', N'10', N'PCE', N'', N'351', N'171009F06', N'12/2/2021', N'Q3-04', N'bao bi moi', N'', CAST(N'2017-12-01 01:00:54.993' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (339, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80608551', N'80608551', N'KILTIX COLLAR MEDIUM 1 PC - FG', N'10', N'PCE', N'53', N'53', N'170812F05', N'12/1/2021', N'Q1-22', N'', N'', CAST(N'2017-12-01 01:01:02.260' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (340, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541024', N'80541024', N'PROLONGAL INJ.(EXP-VN)  50ML', N'60', N'PCE', N'1', N'1', N'160725R03', N'6/12/2018', N'P1-26', N'', N'', CAST(N'2017-12-01 01:01:10.513' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (341, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541024', N'80541024', N'PROLONGAL INJ.(EXP-VN)  50ML', N'60', N'PCE', N'3733', N'2880', N'170313R06', N'12/25/2018', N'Q2-01', N'Bao bi moi', N'', CAST(N'2017-12-01 01:01:10.593' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (342, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541024', N'80541024', N'PROLONGAL INJ.(EXP-VN)  50ML', N'60', N'PCE', N'', N'853', N'170313R06', N'12/25/2018', N'Q1-26', N'Bao bi moi', N'', CAST(N'2017-12-01 01:01:10.707' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (343, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541024', N'80541024', N'PROLONGAL INJ.(EXP-VN)  50ML', N'60', N'PCE', N'12521', N'3600', N'170512R04', N'1/31/2019', N'Q3-21', N'', N'', CAST(N'2017-12-01 01:01:10.793' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (344, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541024', N'80541024', N'PROLONGAL INJ.(EXP-VN)  50ML', N'60', N'PCE', N'', N'3600', N'170512R04', N'1/31/2019', N'Q2-21', N'', N'', CAST(N'2017-12-01 01:01:10.870' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (345, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541024', N'80541024', N'PROLONGAL INJ.(EXP-VN)  50ML', N'60', N'PCE', N'', N'3600', N'170512R04', N'1/31/2019', N'Q2-03', N'', N'', CAST(N'2017-12-01 01:01:10.943' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (346, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80541024', N'80541024', N'PROLONGAL INJ.(EXP-VN)  50ML', N'60', N'PCE', N'', N'1721', N'170512R04', N'1/31/2019', N'Q3-24', N'', N'', CAST(N'2017-12-01 01:01:11.037' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (347, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'5000', N'2940', N'170207R06', N'12/15/2019', N'P3-27', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:01:16.497' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (348, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'1286', N'170207R06', N'12/15/2019', N'O1-21', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:01:16.570' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (349, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'12592', N'3600', N'170403R08', N'2/22/2020', N'P2-24', N'', N'', CAST(N'2017-12-01 01:01:16.637' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (350, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'3600', N'170403R08', N'2/22/2020', N'O2-23', N'', N'', CAST(N'2017-12-01 01:01:16.703' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (351, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'3600', N'170403R08', N'2/22/2020', N'P3-21', N'', N'', CAST(N'2017-12-01 01:01:16.807' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (352, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'1792', N'170403R08', N'2/22/2020', N'P1-25', N'', N'Qc lấy mẫu 2, Navet II lay mau 2', CAST(N'2017-12-01 01:01:16.890' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (353, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'12527', N'3600', N'170612R06', N'4/26/2020', N'O2-11', N'', N'', CAST(N'2017-12-01 01:01:16.970' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (354, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'3600', N'170612R06', N'4/26/2020', N'P2-15', N'', N'', CAST(N'2017-12-01 01:01:17.040' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (355, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'3600', N'170612R06', N'4/26/2020', N'O3-23', N'', N'', CAST(N'2017-12-01 01:01:17.147' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (356, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'80534478', N'80534478', N'VIGANTOL-E INJ.(EXP-VN) 50ML', N'60', N'PCE', N'', N'1727', N'170612R06', N'4/26/2020', N'Q2-05', N'', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:01:17.223' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (357, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84822418', N'84822418', N'PROLONGAL 100ML (EXP-VN)', N'60', N'PCE', N'606', N'606', N'170327R06', N'2/15/2019', N'O1-20', N'bao bi moi', N'', CAST(N'2017-12-01 01:01:26.330' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (358, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84822418', N'84822418', N'PROLONGAL 100ML (EXP-VN)', N'60', N'PCE', N'6298', N'1920', N'170612R05', N'4/19/2019', N'Q3-17', N'bao bi moi', N'', CAST(N'2017-12-01 01:01:26.530' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (359, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84822418', N'84822418', N'PROLONGAL 100ML (EXP-VN)', N'60', N'PCE', N'', N'1920', N'170612R05', N'4/19/2019', N'Q3-14', N'bao bi moi', N'', CAST(N'2017-12-01 01:01:26.690' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (360, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84822418', N'84822418', N'PROLONGAL 100ML (EXP-VN)', N'60', N'PCE', N'', N'1920', N'170612R05', N'4/19/2019', N'Q3-09', N'bao bi moi', N'', CAST(N'2017-12-01 01:01:26.810' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (361, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84822418', N'84822418', N'PROLONGAL 100ML (EXP-VN)', N'60', N'PCE', N'', N'538', N'170612R05', N'4/19/2019', N'Q2-08', N'bao bi moi', N'', CAST(N'2017-12-01 01:01:26.970' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (362, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4030676', N'4030676', N'BAYCOX 5 % SUSPENSION  100ML', N'40', N'PCE', N'0', N'2158', N'170620R01', N'#N/A', N'duoi SX', N'', N'QC lay mau 2', CAST(N'2017-12-01 01:01:35.513' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (363, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4030676', N'4030676', N'BAYCOX 5 % SUSPENSION  100ML', N'40', N'PCE', N'', N'2160', N'170620R01', N'#N/A', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:01:35.653' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (364, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'4030676', N'4030676', N'BAYCOX 5 % SUSPENSION  100ML', N'40', N'PCE', N'2796', N'2796', N'170720R05', N'3/16/2022', N'Q2-13', N'', N'QC lay mau 2, navet II lay mau 2', CAST(N'2017-12-01 01:01:35.760' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (365, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81425205', N'81425205', N'OVUPROST 20ML HDPE (VIETNAM)', N'420', N'PCE', N'1579', N'1483', N'171111R01', N'9/26/2019', N'Q1-23', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:01:42.403' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (366, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81425191', N'81425191', N'OVURELIN 20ML HDPE (VIETNAM)', N'420', N'PCE', N'641', N'           557 ', N'170707R02', N'5/8/2019', N'Q1-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:01:50.550' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (367, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81043108', N'81043108', N'DERMALEEN ITCH WASH SHAMPOO 250ML', N'24', N'PCE', N'781', N'781', N'170419F08', N'7/31/2020', N'Q1-18', N'bao bi moi', N'', CAST(N'2017-12-01 01:01:59.150' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (368, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'57459143', N'57459143', N'TETRAVET AEROSOL 200GM BOTTLE', N'12', N'PCE', N'371', N'341', N'170918R01', N'12/1/2018', N'Q1-25', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:02:07.460' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (369, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81425183', N'81425183', N'TERMINATOR 1L ROUND (VIETNAM)', N'12', N'PCE', N'12', N'11', N'170523R01', N'1/1/2019', N'Y1-12', N'', N'', CAST(N'2017-12-01 01:02:13.477' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (370, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'81425183', N'81425183', N'TERMINATOR 1L ROUND (VIETNAM)', N'12', N'PCE', N'', N'1', N'170523R01', N'1/1/2019', N'Y2-15', N'', N'', CAST(N'2017-12-01 01:02:13.610' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (371, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84632961', N'84632961', N'AMOXISOL LA - 250 ML BOTTLE', N'12', N'PCE', N'2232', N'1224', N'170911R01', N'5/24/2019', N'P3-10', N'Bao bi moi', N'', CAST(N'2017-12-01 01:02:18.533' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (372, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84632961', N'84632961', N'AMOXISOL LA - 250 ML BOTTLE', N'12', N'PCE', N'', N'728', N'170911R01', N'5/24/2019', N'duoi nen', N'Bao bi moi', N'', CAST(N'2017-12-01 01:02:18.603' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (373, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84632961', N'84632961', N'AMOXISOL LA - 250 ML BOTTLE', N'12', N'PCE', N'3488', N'464', N'171025R01', N'7/13/2019', N'O1-17', N'', N'', CAST(N'2017-12-01 01:02:18.677' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (374, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84632961', N'84632961', N'AMOXISOL LA - 250 ML BOTTLE', N'12', N'PCE', N'', N'1008', N'171025R01', N'7/13/2019', N'P3-07', N'', N'', CAST(N'2017-12-01 01:02:18.800' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (375, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84632961', N'84632961', N'AMOXISOL LA - 250 ML BOTTLE', N'12', N'PCE', N'', N'1008', N'171025R01', N'7/13/2019', N'P3-06', N'', N'', CAST(N'2017-12-01 01:02:18.883' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (376, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84632961', N'84632961', N'AMOXISOL LA - 250 ML BOTTLE', N'12', N'PCE', N'', N'1008', N'171025R01', N'7/13/2019', N'P1-19', N'', N'', CAST(N'2017-12-01 01:02:18.957' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (377, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'84547476', N'84547476', N'ADVOCATE S.O. DOG 25-40KG 3X4.0ML', N'12', N'PCE', N'106', N'106', N'170325F03', N'12/31/2018', N'Q1-03', N'Bao bi moi', N'', CAST(N'2017-12-01 01:02:32.097' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (378, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'3856031', N'3856031', N'DRONTAL PLUS TAB FLAVOUR H 1 X 8 TAB', N'72', N'PCE', N'3838', N'3838', N'171013R01', N'7/31/2022', N'P2-25', N'', N'', CAST(N'2017-12-01 01:02:39.193' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (379, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'3856031', N'3856031', N'DRONTAL PLUS TAB FLAVOUR H 1 X 8 TAB', N'72', N'PCE', N'538', N'538', N'170921R01', N'4/30/2022', N'O1-08', N'', N'', CAST(N'2017-12-01 01:02:39.280' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (380, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85159860', N'85159860', N'KILTIX COLLAR LARGE 1 PCE 79596030', N'170', N'PCE', N'673', N'673', N'170925R01', N'2/28/2022', N'Q2-15', N'Pallet 1 data logger', N'', CAST(N'2017-12-01 01:02:46.340' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (381, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85159860', N'85159860', N'KILTIX COLLAR LARGE 1 PCE 79596030', N'170', N'PCE', N'1358', N'1358', N'170925R02', N'2/28/2022', N'Q3-08', N'Pallet 1 data logger', N'', CAST(N'2017-12-01 01:02:46.410' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (382, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85159984', N'85159984', N'KILTIX HALSBAND MITTEL 1 ST 79596030', N'170', N'PCE', N'1092', N'1092', N'170703R01', N'12/1/2021', N'Q3-19', N'', N'', CAST(N'2017-12-01 01:02:53.420' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (383, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85159984', N'85159984', N'KILTIX HALSBAND MITTEL 1 ST 79596030', N'170', N'PCE', N'1698', N'1698', N'171107R03', N'4/11/2022', N'O3-01', N'', N'', CAST(N'2017-12-01 01:02:53.510' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (384, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'297', N'81', N'171107F01', N'11/2/2020', N'P2-08', N'', N'', CAST(N'2017-12-01 01:03:05.457' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (385, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'216', N'171107F01', N'11/2/2020', N'P3-16', N'', N'', CAST(N'2017-12-01 01:03:05.530' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (386, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'300', N'84', N'171107F02', N'11/2/2020', N'P3-26', N'', N'', CAST(N'2017-12-01 01:03:05.610' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (387, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'216', N'171107F02', N'11/2/2020', N'P3-05', N'', N'', CAST(N'2017-12-01 01:03:05.750' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (388, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'302', N'216', N'171107F03', N'11/2/2020', N'P3-11', N'', N'', CAST(N'2017-12-01 01:03:05.827' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (389, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'86', N'171107F03', N'11/2/2020', N'O3-20', N'', N'', CAST(N'2017-12-01 01:03:05.907' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (390, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'301', N'85', N'171107F04', N'11/2/2020', N'P3-08', N'', N'', CAST(N'2017-12-01 01:03:05.980' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (391, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'216', N'171107F04', N'11/2/2020', N'Q2-04', N'', N'', CAST(N'2017-12-01 01:03:06.060' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (392, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'291', N'216', N'171107F05', N'1/11/2020', N'P2-26', N'', N'', CAST(N'2017-12-01 01:03:06.130' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (393, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'75', N'171107F05', N'1/11/2020', N'P2-22', N'', N'', CAST(N'2017-12-01 01:03:06.200' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (394, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'291', N'216', N'171107F06', N'1/12/2020', N'O3-17', N'', N'', CAST(N'2017-12-01 01:03:06.277' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (395, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'75', N'171107F06', N'1/12/2020', N'P1-11', N'', N'', CAST(N'2017-12-01 01:03:06.350' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (396, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'386', N'216', N'171107F07', N'1/13/2020', N'O2-21', N'', N'', CAST(N'2017-12-01 01:03:06.487' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (397, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'170', N'171107F07', N'1/13/2020', N'Q2-09', N'', N'', CAST(N'2017-12-01 01:03:06.563' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (398, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'301', N'216', N'171108F01', N'11/2/2020', N'O3-02', N'', N'', CAST(N'2017-12-01 01:03:06.643' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (399, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'85', N'171108F01', N'11/2/2020', N'O1-18', N'', N'', CAST(N'2017-12-01 01:03:06.713' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (400, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'301', N'85', N'171108F02', N'11/2/2020', N'O3-16', N'', N'', CAST(N'2017-12-01 01:03:06.783' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (401, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'216', N'171108F02', N'11/2/2020', N'P1-01', N'', N'', CAST(N'2017-12-01 01:03:06.860' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (402, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'302', N'216', N'171108F03', N'11/2/2020', N'O3-22', N'', N'', CAST(N'2017-12-01 01:03:06.980' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (403, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'86', N'171108F03', N'11/2/2020', N'P3-04', N'', N'', CAST(N'2017-12-01 01:03:07.057' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (404, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'301', N'85', N'171108F04', N'11/2/2020', N'O2-26', N'', N'', CAST(N'2017-12-01 01:03:07.123' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (405, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'216', N'171108F04', N'11/2/2020', N'O2-20', N'', N'', CAST(N'2017-12-01 01:03:07.200' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (406, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'301', N'216', N'171109F01', N'11/2/2020', N'O3-09', N'', N'', CAST(N'2017-12-01 01:03:07.277' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (407, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'85', N'171109F01', N'11/2/2020', N'P2-14', N'', N'', CAST(N'2017-12-01 01:03:07.390' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (408, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'302', N'86', N'171109F02', N'11/2/2020', N'O3-19', N'', N'', CAST(N'2017-12-01 01:03:07.460' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (409, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85671987', N'85671987', N'ERYTHROSOL - 5X200G BOX (IQ-JO)', N'50', N'PCE', N'', N'216', N'171109F02', N'11/2/2020', N'P3-19', N'', N'', CAST(N'2017-12-01 01:03:07.530' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (410, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85645242', N'85645242', N'FERRIADE', N'12', N'PCE', N'1958', N'10', N'170928R03', N'7/1/2019', N'P1-26', N'Bao bi moi', N'BAN KM', CAST(N'2017-12-01 01:03:20.970' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (411, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85645242', N'85645242', N'FERRIADE', N'12', N'PCE', N'', N'830', N'170928R03', N'7/1/2019', N'P1-06', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:21.180' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (412, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85645242', N'85645242', N'FERRIADE', N'12', N'PCE', N'', N'960', N'170928R03', N'7/1/2019', N'P2-01', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:21.300' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (413, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85645242', N'85645242', N'FERRIADE', N'12', N'PCE', N'1878', N'960', N'170928R04', N'7/1/2019', N'P3-02', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:21.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (414, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'85645242', N'85645242', N'FERRIADE', N'12', N'PCE', N'', N'918', N'170928R04', N'7/1/2019', N'P2-27', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:21.443' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (415, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'53', N'170812F05', N'12/1/2021', N'Q1-22', N'', N'', CAST(N'2017-12-01 01:03:25.990' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (416, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'384', N'170403R04', N'1/6/2020', N'O1-14', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:26.063' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (417, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'781', N'170419F08', N'7/31/2020', N'Q1-18', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:26.167' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (418, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'11', N'170523R01', N'1/1/2019', N'Y1-12', N'', N'', CAST(N'2017-12-01 01:03:26.250' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (419, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'557', N'170707R02', N'5/8/2019', N'Q1-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:26.333' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (420, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:26.403' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (421, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'259', N'160729R01', N'5/24/2019', N'Q3-11', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:26.473' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (422, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'682', N'170901F08', N'3/31/2019', N'Q1-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:26.543' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (423, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'106', N'170325F03', N'12/31/2018', N'Q1-03', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:26.640' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (424, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1224', N'170911R01', N'5/24/2019', N'P3-10', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:26.727' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (425, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170313R07', N'2/6/2019', N'O3-18', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:26.820' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (426, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'606', N'170327R06', N'2/15/2019', N'O1-20', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:26.967' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (427, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'673', N'170925R01', N'2/28/2022', N'Q2-15', N'Pallet 1 data logger', N'', CAST(N'2017-12-01 01:03:27.070' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (428, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1092', N'170703R01', N'12/1/2021', N'Q3-19', N'', N'', CAST(N'2017-12-01 01:03:27.267' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (429, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:27.463' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (430, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2160', N'170620R01', N'#N/A', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:03:27.647' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (431, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170113R01', N'8/21/2019', N'Q2-23', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:27.713' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (432, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:27.810' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (433, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2530', N'171030F07', N'4/30/2022', N'Q3-15', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:27.893' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (434, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'956', N'171010F06', N'2/28/2019', N'Q2-19', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:27.970' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (435, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2080', N'170110R01', N'5/25/2019', N'Q3-12', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:28.090' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (436, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1286', N'170207R06', N'12/15/2019', N'O1-21', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:28.180' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (437, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'#REF!', CAST(N'2017-12-01 01:03:28.277' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (438, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170420R07', N'12/8/2018', N'Q2-07', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:28.353' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (439, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2880', N'170313R06', N'12/25/2018', N'Q2-01', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:28.480' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (440, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170327R04', N'2/3/2020', N'O3-07', N'', N'', CAST(N'2017-12-01 01:03:28.580' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (441, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3598', N'170327R05', N'2/19/2019', N'Q2-12', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:28.663' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (442, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1440', N'170612F01', N'1/12/2022', N'P1-23', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:28.743' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (443, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1', N'170105F07', N'11/3/2018', N'P1-26', N'', N'Ban Khuyen mai', CAST(N'2017-12-01 01:03:28.827' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (444, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1798', N'151226F02', N'6/30/2018', N'Q3-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:28.990' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (445, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'900', N'171009F06', N'12/2/2021', N'Q2-11', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:29.123' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (446, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'163', N'170403R04', N'1/6/2020', N'Q1-11', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:29.220' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (447, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1', N'170523R01', N'1/1/2019', N'Y2-15', N'', N'', CAST(N'2017-12-01 01:03:29.320' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (448, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1483', N'171111R01', N'9/26/2019', N'Q1-23', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:29.430' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (449, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'357', N'170306R04', N'5/15/2019', N'Q1-06', N'BAO BI MOI', N'Qc lấy mẫu 2, Navet I lay mau 3', CAST(N'2017-12-01 01:03:29.510' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (450, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'472', N'171010F07', N'6/30/2019', N'O2-12', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:29.597' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (451, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'728', N'170911R01', N'5/24/2019', N'duoi nen', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:29.690' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (452, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1380', N'170313R07', N'2/6/2019', N'O2-27', N'Bao bi moi', N'Navet II lấy mẫu 2
Qc lấy mẫu 2', CAST(N'2017-12-01 01:03:29.807' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (453, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170612R05', N'4/19/2019', N'Q3-17', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:29.897' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (454, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1358', N'170925R02', N'2/28/2022', N'Q3-08', N'Pallet 1 data logger', N'', CAST(N'2017-12-01 01:03:29.990' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (455, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'10', N'170928R03', N'7/1/2019', N'P1-26', N'Bao bi moi', N'BAN KM', CAST(N'2017-12-01 01:03:30.077' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (456, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2796', N'170720R05', N'3/16/2022', N'Q2-13', N'', N'QC lay mau 2, navet II lay mau 2', CAST(N'2017-12-01 01:03:30.180' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (457, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'680', N'170113R01', N'8/21/2019', N'Q1-15', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:30.277' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (458, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'575', N'170530R04', N'2/16/2019', N'O1-11', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:30.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (459, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2080', N'170110R01', N'5/25/2019', N'Q3-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:30.460' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (460, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', CAST(N'2017-12-01 01:03:30.530' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (461, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'5', N'160727R04', N'6/16/2018', N'P1-26', N'', N'Bán khuyến mãi', CAST(N'2017-12-01 01:03:30.617' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (462, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1366', N'170420R07', N'12/8/2018', N'P1-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:30.730' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (463, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'853', N'170313R06', N'12/25/2018', N'Q1-26', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:30.813' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (464, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 01:03:30.960' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (465, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2249', N'170612R04', N'2/19/2019', N'P2-07', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:31.030' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (466, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1440', N'170612F01', N'1/12/2022', N'O1-22', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:31.120' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (467, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1', N'170415F03', N'12/22/2018', N'P1-26', N'', N'Ban Khuyen mai', CAST(N'2017-12-01 01:03:31.223' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (468, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'900', N'171009F06', N'12/2/2021', N'Q3-07', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:31.310' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (469, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'213', N'170403R04', N'1/6/2020', N'P2-02', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:31.397' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (470, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'358', N'171010F08', N'6/30/2019', N'O2-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:31.470' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (471, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'464', N'171025R01', N'7/13/2019', N'O1-17', N'', N'', CAST(N'2017-12-01 01:03:31.603' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (472, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1983', N'170313R07', N'2/6/2019', N'Duoi nen', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:31.680' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (473, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170612R05', N'4/19/2019', N'Q3-14', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:31.767' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (474, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'830', N'170928R03', N'7/1/2019', N'P1-06', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:31.863' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (475, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170113R01', N'8/21/2019', N'Q2-17', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:31.943' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (476, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1799', N'170530R04', N'2/16/2019', N'O3-05', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:32.020' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (477, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'705', N'170110R01', N'5/25/2019', N'Q1-14', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:32.093' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (478, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170403R08', N'2/22/2020', N'P2-24', N'', N'', CAST(N'2017-12-01 01:03:32.250' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (479, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:32.337' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (480, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170706R04', N'3/20/2019', N'O3-27', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:32.413' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (481, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170512R04', N'1/31/2019', N'Q3-21', N'', N'', CAST(N'2017-12-01 01:03:32.500' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (482, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'229', N'170327R04', N'2/3/2020', N'Q1-16', N'', N'', CAST(N'2017-12-01 01:03:32.577' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (483, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2880', N'170612R04', N'2/19/2019', N'P2-05', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:32.677' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (484, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'480', N'170612F01', N'1/12/2022', N'Duoi SX', N'', N'', CAST(N'2017-12-01 01:03:32.757' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (485, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'8', N'170417F08', N'1/3/2019', N'P1-26', N'', N'Ban Khuyen mai', CAST(N'2017-12-01 01:03:32.827' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (486, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'900', N'171009F06', N'12/2/2021', N'Q3-06', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:32.957' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (487, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1008', N'171025R01', N'7/13/2019', N'P3-07', N'', N'', CAST(N'2017-12-01 01:03:33.030' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (488, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1800', N'170313R07', N'2/6/2019', N'Q1-07', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:33.113' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (489, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170612R05', N'4/19/2019', N'Q3-09', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:33.207' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (490, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'960', N'170928R03', N'7/1/2019', N'P2-01', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:33.283' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (491, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1960', N'170113R01', N'8/21/2019', N'Q2-16', N'bao bi moi', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:03:33.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (492, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170530R04', N'2/16/2019', N'P3-03', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:33.447' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (493, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'11', N'170110R01', N'5/25/2019', N'P1-26', N'11 Chai Loi', N'', CAST(N'2017-12-01 01:03:33.517' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (494, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170403R08', N'2/22/2020', N'O2-23', N'', N'', CAST(N'2017-12-01 01:03:33.657' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (495, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:33.747' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (496, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'864', N'170706R04', N'3/20/2019', N'O1-26', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:33.830' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (497, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170512R04', N'1/31/2019', N'Q2-21', N'', N'', CAST(N'2017-12-01 01:03:33.930' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (498, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'120', N'170511R01', N'3/8/2020', N'O3-21', N'', N'', CAST(N'2017-12-01 01:03:34.010' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (499, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'171115R06', N'6/18/2019', N'O3-26', N'', N'', CAST(N'2017-12-01 01:03:34.080' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (500, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'799', N'170612F01', N'1/12/2022', N'O1-24', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:34.160' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (501, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:34.307' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (502, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'351', N'171009F06', N'12/2/2021', N'Q3-04', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:34.393' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (503, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1008', N'171025R01', N'7/13/2019', N'P3-06', N'', N'', CAST(N'2017-12-01 01:03:34.463' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (504, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'538', N'170612R05', N'4/19/2019', N'Q2-08', N'bao bi moi', N'', CAST(N'2017-12-01 01:03:34.590' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (505, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'960', N'170928R04', N'7/1/2019', N'P3-02', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:34.683' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (506, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2078', N'170314R02', N'9/9/2019', N'P3-18', N'bao bi moi', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:03:34.783' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (507, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170530R04', N'2/16/2019', N'O3-04', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:34.890' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (508, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1286', N'170411R07', N'10/30/2019', N'O2-22', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:35.010' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (509, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170403R08', N'2/22/2020', N'P3-21', N'', N'', CAST(N'2017-12-01 01:03:35.087' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (510, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1', N'170327R01', N'2/5/2019', N'P1-26', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:35.177' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (511, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2588', N'170706R04', N'3/20/2019', N'O1-23', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:35.270' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (512, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170512R04', N'1/31/2019', N'Q2-03', N'', N'', CAST(N'2017-12-01 01:03:35.367' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (513, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170511R01', N'3/8/2020', N'O2-13', N'', N'', CAST(N'2017-12-01 01:03:35.443' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (514, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'171115R06', N'6/18/2019', N'O1-27', N'', N'', CAST(N'2017-12-01 01:03:35.527' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (515, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1800', N'170605F08', N'2/7/2019', N'O2-14', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:35.670' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (516, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1008', N'171025R01', N'7/13/2019', N'P1-19', N'', N'', CAST(N'2017-12-01 01:03:35.753' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (517, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'918', N'170928R04', N'7/1/2019', N'P2-27', N'', N'', CAST(N'2017-12-01 01:03:35.847' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (518, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2080', N'170411R06', N'9/27/2019', N'O2-07', N'Data logger Pallet 3', N'', CAST(N'2017-12-01 01:03:35.970' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (519, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170530R04', N'2/16/2019', N'P2-11', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:36.077' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (520, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1960', N'170411R08', N'11/1/2019', N'O1-15', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:36.167' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (521, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1792', N'170403R08', N'2/22/2020', N'P1-25', N'', N'Qc lấy mẫu 2, Navet II lay mau 2', CAST(N'2017-12-01 01:03:36.287' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (522, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:36.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (523, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'171107R02', N'4/11/2019', N'Q2-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:36.450' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (524, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1721', N'170512R04', N'1/31/2019', N'Q3-24', N'', N'', CAST(N'2017-12-01 01:03:36.533' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (525, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1920', N'170511R01', N'3/8/2020', N'O1-16', N'', N'', CAST(N'2017-12-01 01:03:36.620' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (526, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1995', N'171115R06', N'6/18/2019', N'P1-13', N'', N'', CAST(N'2017-12-01 01:03:36.743' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (527, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1078', N'170605F08', N'2/7/2019', N'Q1-10', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:36.817' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (528, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2076', N'170411R06', N'9/27/2019', N'Q3-03', N'Data logger Pallet 4', N'Qc lấy mẫu 2 Navet II lấy mẫu 2', CAST(N'2017-12-01 01:03:36.920' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (529, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170530R04', N'2/16/2019', N'P3-13', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:37.077' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (530, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'918', N'170411R08', N'11/1/2019', N'O3-25', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:37.150' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (531, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170612R06', N'4/26/2020', N'O2-11', N'', N'', CAST(N'2017-12-01 01:03:37.247' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (532, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:37.387' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (533, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2446', N'171107R02', N'4/11/2019', N'Q3-10', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:37.473' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (534, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R03', N'3/31/2020', N'duoi nen', N'', N'', CAST(N'2017-12-01 01:03:37.563' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (535, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170628F02', N'1/5/2019', N'O3-24', N'', N'', CAST(N'2017-12-01 01:03:37.637' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (536, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2078', N'170622R01', N'11/7/2019', N'O1-19', N'', N'', CAST(N'2017-12-01 01:03:37.793' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (537, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170530R04', N'2/16/2019', N'O1-25', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:37.923' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (538, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2080', N'170504R01', N'12/12/2019', N'P2-06', N'', N'', CAST(N'2017-12-01 01:03:38.010' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (539, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170612R06', N'4/26/2020', N'P2-15', N'', N'', CAST(N'2017-12-01 01:03:38.093' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (540, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:38.307' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (541, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R03', N'3/31/2020', N'Q4-13', N'', N'', CAST(N'2017-12-01 01:03:38.387' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (542, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2528', N'170628F02', N'1/5/2019', N'O3-15', N'', N'', CAST(N'2017-12-01 01:03:38.470' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (543, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2080', N'170622R01', N'11/7/2019', N'P1-22', N'', N'', CAST(N'2017-12-01 01:03:38.590' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (544, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'360', N'170530R04', N'2/16/2019', N'P3-17', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:38.667' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (545, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2078', N'170504R01', N'12/12/2019', N'O3-06', N'', N'', CAST(N'2017-12-01 01:03:38.787' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (546, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'3600', N'170612R06', N'4/26/2020', N'O3-23', N'', N'', CAST(N'2017-12-01 01:03:38.870' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (547, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:38.977' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (548, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1384', N'170612R03', N'3/31/2020', N'Q3-02', N'', N'', CAST(N'2017-12-01 01:03:39.057' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (549, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'20', N'170321F05', N'12/22/2018', N'P1-26', N'', N'', CAST(N'2017-12-01 01:03:39.133' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (550, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'214', N'170530R04', N'2/16/2019', N'Q1-04', N'Bao bi moi', N'', CAST(N'2017-12-01 01:03:39.217' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (551, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1727', N'170612R06', N'4/26/2020', N'Q2-05', N'', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:03:39.333' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (552, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:39.410' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (553, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'20', N'170415F02', N'12/22/2018', N'P1-26', N'', N'', CAST(N'2017-12-01 01:03:39.590' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (554, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1076', N'170810R01', N'4/21/2019', N'O2-08', N'', N'', CAST(N'2017-12-01 01:03:39.673' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (555, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:39.763' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (556, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R01', N'4/21/2019', N'O3-14', N'', N'', CAST(N'2017-12-01 01:03:39.890' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (557, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:39.993' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (558, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R01', N'4/21/2019', N'P2-16', N'', N'', CAST(N'2017-12-01 01:03:40.100' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (559, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'508', N'170327R02', N'2/12/2019', N'Q1-17', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:03:40.170' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (560, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R01', N'4/21/2019', N'O2-16', N'', N'', CAST(N'2017-12-01 01:03:40.263' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (561, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:40.343' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (562, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R01', N'4/21/2019', N'P2-19', N'', N'', CAST(N'2017-12-01 01:03:40.517' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (563, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:40.633' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (564, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R01', N'4/21/2019', N'Q2-18', N'', N'', CAST(N'2017-12-01 01:03:40.777' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (565, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:40.850' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (566, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R01', N'4/21/2019', N'O2-18', N'', N'', CAST(N'2017-12-01 01:03:40.927' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (567, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:41.037' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (568, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R01', N'4/21/2019', N'P2-20', N'', N'', CAST(N'2017-12-01 01:03:41.110' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (569, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:41.190' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (570, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'352', N'170810R02', N'4/25/2019', N'P3-01', N'', N'', CAST(N'2017-12-01 01:03:41.293' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (571, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:41.367' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (572, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2590', N'170810R02', N'4/25/2019', N'O3-12', N'', N'', CAST(N'2017-12-01 01:03:41.443' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (573, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:03:41.603' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (574, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R02', N'4/25/2019', N'Q3-20', N'', N'', CAST(N'2017-12-01 01:03:41.680' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (575, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170327R03', N'2/14/2019', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:03:41.763' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (576, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R02', N'4/25/2019', N'Q2-22', N'', N'', CAST(N'2017-12-01 01:03:41.910' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (577, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1425', N'170327R03', N'2/14/2019', N'P3-12', N'', N'', CAST(N'2017-12-01 01:03:41.997' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (578, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'170810R02', N'4/25/2019', N'O2-25', N'', N'', CAST(N'2017-12-01 01:03:42.090' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (579, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170327R03', N'2/14/2019', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:03:42.293' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (580, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'171107R01', N'7/10/2019', N'P2-12', N'', N'', CAST(N'2017-12-01 01:03:42.373' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (581, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170327R03', N'2/14/2019', N'O4-24', N'', N'', CAST(N'2017-12-01 01:03:42.473' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (582, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'171107R01', N'7/10/2019', N'P2-10', N'', N'', CAST(N'2017-12-01 01:03:42.600' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (583, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170327R03', N'2/14/2019', N'O4-25', N'', N'', CAST(N'2017-12-01 01:03:42.673' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (584, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'171107R01', N'7/10/2019', N'O3-08', N'', N'', CAST(N'2017-12-01 01:03:42.750' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (585, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170327R03', N'2/14/2019', N'O4-26', N'', N'', CAST(N'2017-12-01 01:03:42.880' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (586, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'171107R01', N'7/10/2019', N'P3-20', N'', N'', CAST(N'2017-12-01 01:03:42.980' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (587, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1620', N'170403R05', N'2/21/2019', N'P3-25', N'', N'', CAST(N'2017-12-01 01:03:43.050' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (588, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'171107R01', N'7/10/2019', N'P2-23', N'', N'', CAST(N'2017-12-01 01:03:43.217' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (589, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R05', N'2/21/2019', N'P4-07', N'', N'', CAST(N'2017-12-01 01:03:43.297' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (590, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'171107R01', N'7/10/2019', N'P2-03', N'', N'', CAST(N'2017-12-01 01:03:43.383' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (591, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2398', N'170403R05', N'2/21/2019', N'P4-19', N'', N'', CAST(N'2017-12-01 01:03:43.453' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (592, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2592', N'171107R01', N'7/10/2019', N'O3-10', N'', N'', CAST(N'2017-12-01 01:03:43.537' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (593, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R05', N'2/21/2019', N'O4-15', N'', N'', CAST(N'2017-12-01 01:03:43.610' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (594, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1114', N'171107R01', N'7/10/2019', N'Q2-02', N'', N'', CAST(N'2017-12-01 01:03:43.707' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (595, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R05', N'2/21/2019', N'O4-02', N'', N'', CAST(N'2017-12-01 01:03:43.823' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (596, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R05', N'2/21/2019', N'O4-05', N'', N'', CAST(N'2017-12-01 01:03:43.950' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (597, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1638', N'170403R06', N'2/23/2019', N'O2-17', N'', N'', CAST(N'2017-12-01 01:03:44.037' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (598, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R06', N'2/23/2019', N'P4-17', N'', N'', CAST(N'2017-12-01 01:03:44.153' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (599, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R06', N'2/23/2019', N'P4-14', N'', N'', CAST(N'2017-12-01 01:03:44.250' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (600, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R06', N'2/23/2019', N'O4-03', N'', N'', CAST(N'2017-12-01 01:03:44.323' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (601, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R06', N'2/23/2019', N'P4-22', N'', N'', CAST(N'2017-12-01 01:03:44.457' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (602, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R06', N'2/23/2019', N'O4-27', N'', N'', CAST(N'2017-12-01 01:03:44.533' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (603, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R07', N'2/24/2019', N'O4-06', N'', N'', CAST(N'2017-12-01 01:03:44.617' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (604, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R07', N'2/24/2019', N'P4-03', N'', N'', CAST(N'2017-12-01 01:03:44.717' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (605, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1118', N'170403R07', N'2/24/2019', N'O3-13', N'data logger (Pallet 12)', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:03:44.797' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (606, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R07', N'2/24/2019', N'O4-04', N'', N'', CAST(N'2017-12-01 01:03:44.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (607, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170403R07', N'2/24/2019', N'P4-25', N'', N'', CAST(N'2017-12-01 01:03:44.987' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (608, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R02', N'3/22/2019', N'P4-15', N'', N'', CAST(N'2017-12-01 01:03:45.123' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (609, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1078', N'170511R02', N'3/22/2019', N'P3-09', N'', N'', CAST(N'2017-12-01 01:03:45.227' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (610, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R03', N'3/23/2019', N'', N'', N'', CAST(N'2017-12-01 01:03:45.323' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (611, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1641', N'170511R03', N'3/23/2019', N'Q3-16', N'', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:03:45.410' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (612, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R03', N'3/23/2019', N'Q4-27', N'', N'', CAST(N'2017-12-01 01:03:45.543' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (613, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R03', N'3/23/2019', N'O4-18', N'', N'', CAST(N'2017-12-01 01:03:45.627' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (614, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R03', N'3/23/2019', N'P4-18', N'', N'', CAST(N'2017-12-01 01:03:45.700' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (615, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R03', N'3/23/2019', N'Q4-23', N'', N'', CAST(N'2017-12-01 01:03:45.800' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (616, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1038', N'170511R04', N'3/24/2019', N'P3-14', N'', N'', CAST(N'2017-12-01 01:03:45.883' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (617, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R04', N'3/24/2019', N'O4-23', N'', N'', CAST(N'2017-12-01 01:03:45.990' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (618, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R04', N'3/24/2019', N'Q4-01', N'', N'', CAST(N'2017-12-01 01:03:46.083' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (619, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R04', N'3/24/2019', N'Q4-08', N'', N'', CAST(N'2017-12-01 01:03:46.227' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (620, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170511R04', N'3/24/2019', N'P4-20', N'Q4-16', N'', CAST(N'2017-12-01 01:03:46.297' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (621, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1652', N'170512R02', N'3/26/2019', N'Q3-23', N'', N'', CAST(N'2017-12-01 01:03:46.417' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (622, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R02', N'3/26/2019', N'P4-13', N'', N'', CAST(N'2017-12-01 01:03:46.510' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (623, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R02', N'3/26/2019', N'Q4-14', N'', N'', CAST(N'2017-12-01 01:03:46.613' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (624, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R02', N'3/26/2019', N'Q4-10', N'', N'', CAST(N'2017-12-01 01:03:46.697' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (625, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R02', N'3/26/2019', N'O4-14', N'', N'', CAST(N'2017-12-01 01:03:46.783' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (626, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R02', N'3/26/2019', N'Q4-15', N'', N'', CAST(N'2017-12-01 01:03:46.867' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (627, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1650', N'170512R03', N'3/28/2019', N'Q2-26', N'', N'Qc lấy mẫu 2, Navet II
lay mau 2', CAST(N'2017-12-01 01:03:46.993' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (628, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R03', N'3/28/2019', N'Q4-19', N'', N'', CAST(N'2017-12-01 01:03:47.067' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (629, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R03', N'3/28/2019', N'Q4-17', N'', N'', CAST(N'2017-12-01 01:03:47.147' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (630, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R03', N'3/28/2019', N'O4-08', N'', N'', CAST(N'2017-12-01 01:03:47.250' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (631, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R03', N'3/28/2019', N'P4-04', N'', N'', CAST(N'2017-12-01 01:03:47.323' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (632, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R03', N'3/28/2019', N'O4-22', N'P4-06', N'', CAST(N'2017-12-01 01:03:47.403' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (633, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R05', N'3/12/2019', N'O4-16', N'', N'', CAST(N'2017-12-01 01:03:47.507' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (634, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R05', N'3/12/2019', N'O4-01', N'', N'', CAST(N'2017-12-01 01:03:47.580' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (635, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1084', N'170512R05', N'3/12/2019', N'O3-11', N'', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:03:47.660' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (636, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R05', N'3/12/2019', N'Q4-20', N'', N'', CAST(N'2017-12-01 01:03:47.780' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (637, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R05', N'3/12/2019', N'Q4-26', N'', N'', CAST(N'2017-12-01 01:03:47.867' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (638, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R06', N'3/14/2019', N'O4-20', N'', N'', CAST(N'2017-12-01 01:03:47.950' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (639, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1624', N'170512R06', N'3/14/2019', N'Q2-27', N'', N'', CAST(N'2017-12-01 01:03:48.050' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (640, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R06', N'3/14/2019', N'Q4-11', N'', N'', CAST(N'2017-12-01 01:03:48.140' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (641, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R06', N'3/14/2019', N'Q4-12', N'', N'', CAST(N'2017-12-01 01:03:48.247' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (642, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R06', N'3/14/2019', N'#REF!', N'', N'', CAST(N'2017-12-01 01:03:48.320' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (643, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R06', N'3/14/2019', N'Q4-07', N'', N'', CAST(N'2017-12-01 01:03:48.400' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (644, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1661', N'170512R07', N'3/16/2019', N'P1-20', N'', N'Qc lấy mẫu 2', CAST(N'2017-12-01 01:03:48.520' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (645, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R07', N'3/16/2019', N'P4-26', N'', N'', CAST(N'2017-12-01 01:03:48.597' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (646, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R07', N'3/16/2019', N'P4-27', N'', N'', CAST(N'2017-12-01 01:03:48.683' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (647, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R07', N'3/16/2019', N'P4-21', N'', N'', CAST(N'2017-12-01 01:03:48.787' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (648, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R07', N'3/16/2019', N'Q4-09', N'', N'', CAST(N'2017-12-01 01:03:48.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (649, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R07', N'3/16/2019', N'O4-19', N'', N'', CAST(N'2017-12-01 01:03:48.947' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (650, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R08', N'3/22/2019', N'P4-24', N'', N'', CAST(N'2017-12-01 01:03:49.033' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (651, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2398', N'170512R08', N'3/22/2019', N'P4-23', N'', N'', CAST(N'2017-12-01 01:03:49.137' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (652, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170512R08', N'3/22/2019', N'O4-13', N'', N'', CAST(N'2017-12-01 01:03:49.220' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (653, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'400', N'170612R07', N'4/2/2019', N'P1-15', N'', N'', CAST(N'2017-12-01 01:03:49.307' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (654, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R07', N'4/2/2019', N'Q4-18', N'', N'', CAST(N'2017-12-01 01:03:49.433' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (655, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1648', N'170612R07', N'4/2/2019', N'Q2-14', N'', N'', CAST(N'2017-12-01 01:03:49.510' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (656, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R08', N'4/5/2019', N'Q4-24', N'', N'', CAST(N'2017-12-01 01:03:49.587' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (657, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R08', N'4/5/2019', N'P4-16', N'', N'', CAST(N'2017-12-01 01:03:49.683' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (658, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R08', N'4/5/2019', N'O4-09', N'', N'', CAST(N'2017-12-01 01:03:49.750' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (659, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R08', N'4/5/2019', N'O4-07', N'', N'', CAST(N'2017-12-01 01:03:49.830' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (660, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R08', N'4/5/2019', N'Q4-06', N'', N'', CAST(N'2017-12-01 01:03:49.917' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (661, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1646', N'170612R08', N'4/5/2019', N'O2-15', N'', N'', CAST(N'2017-12-01 01:03:49.997' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (662, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R09', N'4/6/2019', N'P4-12', N'', N'', CAST(N'2017-12-01 01:03:50.120' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (663, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R09', N'4/6/2019', N'P4-11', N'', N'', CAST(N'2017-12-01 01:03:50.203' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (664, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R09', N'4/6/2019', N'O4-12', N'', N'', CAST(N'2017-12-01 01:03:50.293' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (665, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R09', N'4/6/2019', N'Q4-04', N'', N'', CAST(N'2017-12-01 01:03:50.373' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (666, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1078', N'170612R09', N'4/6/2019', N'Q3-22', N'', N'', CAST(N'2017-12-01 01:03:50.447' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (667, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R10', N'4/7/2019', N'O4-11', N'', N'', CAST(N'2017-12-01 01:03:50.550' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (668, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R10', N'4/7/2019', N'O4-10', N'', N'', CAST(N'2017-12-01 01:03:50.623' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (669, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R10', N'4/7/2019', N'P4-02', N'', N'', CAST(N'2017-12-01 01:03:50.700' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (670, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'170612R10', N'4/7/2019', N'Q4-21', N'', N'', CAST(N'2017-12-01 01:03:50.830' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (671, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1090', N'170612R10', N'4/7/2019', N'Q2-10', N'', N'', CAST(N'2017-12-01 01:03:50.917' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (672, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1119', N'171115R01', N'4/9/2019', N'O2-10', N'', N'', CAST(N'2017-12-01 01:03:50.993' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (673, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1046', N'171115R02', N'5/7/2019', N'P1-12', N'', N'', CAST(N'2017-12-01 01:03:51.087' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (674, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'171115R03', N'4/11/2019', N'P4-01', N'', N'', CAST(N'2017-12-01 01:03:51.167' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (675, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'171115R03', N'4/11/2019', N'Q4-03', N'', N'', CAST(N'2017-12-01 01:03:51.243' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (676, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1064', N'171115R03', N'4/11/2019', N'P3-22', N'', N'', CAST(N'2017-12-01 01:03:51.373' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (677, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'171115R04', N'5/25/2019', N'O4-21', N'', N'', CAST(N'2017-12-01 01:03:51.447' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (678, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'171115R04', N'5/25/2019', N'Q4-02', N'', N'', CAST(N'2017-12-01 01:03:51.530' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (679, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'1113', N'171115R04', N'5/25/2019', N'P3-23', N'', N'', CAST(N'2017-12-01 01:03:51.607' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (680, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG CHAI.xlsx', N'THE KHO-THE DINH VI HANG CHAI.xlsx', N'Location', N'127', N'2', N'Hàng hold', N'', N'', N'2400', N'171115R05', N'5/30/2019', N'P4-10', N'', N'', CAST(N'2017-12-01 01:03:51.717' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (681, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82649794', N'82649794', N'AQUA C FISH PLUS - 1KG BAG', N'15 gói/thùng', N'PCE', N'190', N'57', N'170925F03', N'9/16/2019', N'Z1-03', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:02.360' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (682, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82649794', N'82649794', N'AQUA C FISH PLUS - 1KG BAG', N'15 gói/thùng', N'PCE', N'0', N'396', N'171116F05', N'#N/A', N'Z4-26', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:02.460' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (683, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57529435', N'57529435', N'A-T 104 VIT ADE - 1KG BAG', N'20 gói/thùng', N'PCE', N'999', N'372', N'171117F01', N'11/8/2018', N'Z1-08', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:08.843' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (684, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454516', N'57454516', N'A-T 110 ELECTROLYTES-100GM BAG', N'100 gói/thùng', N'PCE', N'2500', N'2500', N'171106F02', N'10/30/2018', N'Y2-16', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:09.403' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (685, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454516', N'57454516', N'A-T 110 ELECTROLYTES-100GM BAG', N'100 gói/thùng', N'PCE', N'1465', N'1265', N'171106F01', N'10/30/2018', N'Z1-09', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:09.553' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (686, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454516', N'57454516', N'A-T 110 ELECTROLYTES-100GM BAG', N'100 gói/thùng', N'PCE', N'0', N'1000', N'171120F01', N'#N/A', N'Y2-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:09.640' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (687, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454516', N'57454516', N'A-T 110 ELECTROLYTES-100GM BAG', N'100 gói/thùng', N'PCE', N'0', N'1650', N'171121F01', N'#N/A', N'Z3-16', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:09.727' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (688, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57529478', N'57529478', N'A-T 110 ELECTROLYTES-1KG BAG', N'15 gói/thùng', N'PCE', N'495', N'33', N'171110F01', N'10/30/2018', N'Z1-10', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:15.173' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (689, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454486', N'57454486', N'A-T 111 VIT C ANTI - 100GM BAG', N'100 gói/thùng', N'PCE', N'0', N'3935', N'171120F01', N'#N/A', N'Z1-11', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:15.907' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (690, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454494', N'57454494', N'A-T 111 VIT C ANTI - 1KG BAG', N'15 gói/thùng', N'PCE', N'0', N'360', N'171120F03', N'#N/A', N'Y3-07', N'bao bi moi', N'', CAST(N'2017-12-01 01:04:23.297' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (691, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454494', N'57454494', N'A-T 111 VIT C ANTI - 1KG BAG', N'15 gói/thùng', N'PCE', N'', N'4', N'171120F03', N'#N/A', N'Z1-12', N'bao bi moi', N'', CAST(N'2017-12-01 01:04:23.387' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (692, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454524', N'57454524', N'A-T 112 MULTIVITAMIN-100GM BAG', N'100 gói/thùng', N'PCE', N'2495', N'1553', N'171113F04', N'11/2/2018', N'Z1-13', N'bao bi moi', N'', CAST(N'2017-12-01 01:04:24.207' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (693, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454443', N'57454443', N'A-T 206 TYLO SULPHA C - 1KG BG', N'20 gói/thùng', N'PCE', N'2', N'2', N'170608F06', N'6/7/2018', N'Z1-15', N'', N'', CAST(N'2017-12-01 01:04:38.070' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (694, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454834', N'57454834', N'AQUA C - 1KG BAG', N'15 gói/thùng', N'PCE', N'986', N'42', N'171028F01', N'10/19/2019', N'Z1-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:43.500' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (695, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454834', N'57454834', N'AQUA C - 1KG BAG', N'15 gói/thùng', N'PCE', N'', N'512', N'171028F01', N'10/19/2019', N'Z2-29', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:43.580' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (696, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81229694', N'81229694', N'AQUADOR - 1KG BAG', N'12 gói/thùng', N'PCE', N'20', N'20', N'160915F08', N'9/13/2018', N'KV HOLD', N'', N'', CAST(N'2017-12-01 01:04:47.763' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (697, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81229694', N'81229694', N'AQUADOR - 1KG BAG', N'12 gói/thùng', N'PCE', N'12', N'12', N'161010F04', N'10/5/2018', N'KV HOLD', N'', N'', CAST(N'2017-12-01 01:04:47.847' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (698, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81229694', N'81229694', N'AQUADOR - 1KG BAG', N'12 gói/thùng', N'PCE', N'1', N'1', N'161027F05', N'10/25/2018', N'KV HOLD', N'', N'', CAST(N'2017-12-01 01:04:47.943' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (699, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81229694', N'81229694', N'AQUADOR - 1KG BAG', N'12 gói/thùng', N'PCE', N'19', N'19', N'161209F04', N'12/7/2018', N'KV HOLD', N'', N'', CAST(N'2017-12-01 01:04:48.020' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (700, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81229694', N'81229694', N'AQUADOR - 1KG BAG', N'12 gói/thùng', N'PCE', N'9', N'9', N'161229F08', N'12/21/2018', N'KV HOLD', N'', N'', CAST(N'2017-12-01 01:04:48.103' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (701, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81229694', N'81229694', N'AQUADOR - 1KG BAG', N'12 gói/thùng', N'PCE', N'45', N'45', N'170727F01', N'7/11/2019', N'Y4-26', N'', N'', CAST(N'2017-12-01 01:04:48.193' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (702, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81229694', N'81229694', N'AQUADOR - 1KG BAG', N'12 gói/thùng', N'PCE', N'73', N'73', N'170823F01', N'8/21/2019', N'Y5-26', N'', N'', CAST(N'2017-12-01 01:04:48.350' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (703, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600012', N'80600012', N'AQUASEPT 1000A - TUB OF 60 TAB', N'12 gói/thùng', N'PCE', N'2', N'2', N'170421F05', N'4/21/2019', N'Y1-17', N'BAN KM', N'', CAST(N'2017-12-01 01:04:48.867' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (704, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600012', N'80600012', N'AQUASEPT 1000A - TUB OF 60 TAB', N'12 gói/thùng', N'PCE', N'944', N'464', N'171010F05', N'6/21/2019', N'Z1-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:49.003' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (705, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600012', N'80600012', N'AQUASEPT 1000A - TUB OF 60 TAB', N'12 gói/thùng', N'PCE', N'', N'480', N'171010F05', N'6/21/2019', N'Z2-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:49.080' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (706, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600012', N'80600012', N'AQUASEPT 1000A - TUB OF 60 TAB', N'12 gói/thùng', N'PCE', N'697', N'384', N'171101F07', N'11/1/2019', N'Y2-01', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:49.180' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (707, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600012', N'80600012', N'AQUASEPT 1000A - TUB OF 60 TAB', N'12 gói/thùng', N'PCE', N'', N'313', N'171101F07', N'11/1/2019', N'Z2-09', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:04:49.270' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (708, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81201081', N'81201081', N'ASUNTOL SOAP 75G', N'140 cục/thùng', N'PCE', N'4648', N'4420', N'161108R06', N'8/1/2019', N'Z1-07', N'', N'', CAST(N'2017-12-01 01:04:53.190' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (709, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81201081', N'81201081', N'ASUNTOL SOAP 75G', N'140 cục/thùng', N'PCE', N'', N'28', N'161108R06', N'8/1/2019', N'Y1-15', N'', N'', CAST(N'2017-12-01 01:04:53.280' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (710, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'85464698', N'85464698', N'BAGROVIT 500G BAG', N'500GM', N'PCE', N'527', N'467', N'170313F07', N'3/2/2018', N'Y1-11', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:00.380' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (711, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600101', N'80600101', N'BAYMET - 1KG BAG', N'15 gói/thùng', N'PCE', N'0', N'512', N'171120F05', N'#N/A', N'Y3-21', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:07.557' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (712, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81233659', N'81233659', N'BAYMIX AQUALASE PLUS - 1KG', N'15 gói/thùng', N'PCE', N'798', N'384', N'171103F02', N'1/29/2019', N'Z2-23', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:07.750' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (713, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600160', N'80600160', N'BAYTICOL 6% - 10ML BOTTLE', N'56 chai/thùng', N'PCE', N'1680', N'1120', N'171026F06', N'4/3/2021', N'W2-01', N'bao bi moi', N'', CAST(N'2017-12-01 01:05:08.073' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (714, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57456810', N'57456810', N'BAYMIX BIOTIN PLUS 1KG BAG', N'20 gói/thùng', N'PCE', N'557', N'117', N'171114F02', N'11/2/2018', N'Z1-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:14.533' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (715, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57529559', N'57529559', N'BOOSTSTART - 100GM BAG', N'100 gói/thùng', N'PCE', N'2380', N'2380', N'170403F01', N'3/30/2018', N'Z1-21', N'', N'', CAST(N'2017-12-01 01:05:16.603' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (716, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81404055', N'81404055', N'BOOSTSTART - 1KG BAG', N'12 gói/thùng', N'PCE', N'79', N'79', N'170901F05', N'8/29/2018', N'Z1-22', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:21.903' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (717, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'85231635', N'85231635', N'    ', N'5kg/1 xô', N'PCE', N'0', N'26', N'171120F04', N'#N/A', N'Z1-23', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:29.797' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (718, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'85231635', N'85231635', N'    ', N'5kg/1 xô', N'PCE', N'', N'39', N'171120F04', N'#N/A', N'Z2-14', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:30.000' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (719, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57456055', N'57456055', N'COFORTA A - 500GM BOX', N'12 hủ/thùng', N'PCE', N'868', N'56', N'171031F01', N'9/14/2019', N'Z1-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:36.743' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (720, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600373', N'80600373', N'DEOCARE A - 1KG BAG', N'15 gói/thùng', N'PCE', N'0', N'309', N'171122F02', N'#N/A', N'Z1-25', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:40.517' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (721, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600373', N'80600373', N'DEOCARE A - 1KG BAG', N'15 gói/thùng', N'PCE', N'0', N'360', N'171122F03', N'#N/A', N'Z2-26', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:40.593' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (722, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600373', N'80600373', N'DEOCARE A - 1KG BAG', N'15 gói/thùng', N'PCE', N'', N'438', N'171122F03', N'#N/A', N'Z4-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:40.670' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (723, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600373', N'80600373', N'DEOCARE A - 1KG BAG', N'15 gói/thùng', N'PCE', N'0', N'150', N'171124F01', N'#N/A', N'', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:40.763' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (724, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600373', N'80600373', N'DEOCARE A - 1KG BAG', N'15 gói/thùng', N'PCE', N'0', N'185', N'171124F03', N'#N/A', N'duoi nen', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:40.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (725, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600411', N'80600411', N'FARMFLUID S - 100ML BOTTLE', N'40 chai/thùng', N'PCE', N'3', N'3', N'170209F09', N'2/9/2019', N'Z1-26', N'', N'', CAST(N'2017-12-01 01:05:41.247' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (726, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80601043', N'80601043', N'FARMFLUID S 5 LIT DRUM', N'5 lit/1 can', N'PCE', N'111', N'6', N'171115F02', N'3/1/2020', N'Z1-27', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:48.077' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (727, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600462', N'80600462', N'FORTOCA - 1KG BAG', N'15 gói/thùng', N'PCE', N'186', N'150', N'170915F04', N'9/14/2019', N'Z1-29', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:05:59.983' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (728, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600500', N'80600500', N'HADACLEAN A 5% - 100GM BAG', N'100 gói/thùng', N'PCE', N'3146', N'346', N'171114F01', N'10/31/2019', N'W1-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:09.683' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (729, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600500', N'80600500', N'HADACLEAN A 5% - 100GM BAG', N'100 gói/thùng', N'PCE', N'', N'2400', N'171114F01', N'10/31/2019', N'Z2-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:09.770' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (730, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600519', N'80600519', N'HADACLEAN A 5% - 1KG BAG', N'12 gói/thùng', N'PCE', N'341', N'170', N'171113F02', N'10/31/2019', N'W1-03', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:16.400' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (731, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600527', N'80600527', N'HADACLEAN A 5% - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'0', N'60', N'171123F03', N'#N/A', N'W3-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:21.897' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (732, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600527', N'80600527', N'HADACLEAN A 5% - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'', N'27', N'171123F03', N'#N/A', N'W1-04', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:21.980' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (733, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80608535', N'80608535', N'MEGABIC - 5 KG BUCKET', N'5kg/1 xô', N'PCE', N'200', N'46', N'171115F01', N'5/8/2019', N'W1-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:28.240' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (734, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80608535', N'80608535', N'MEGABIC - 5 KG BUCKET', N'5kg/1 xô', N'PCE', N'', N'60', N'171115F01', N'5/8/2019', N'Z2-16', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:28.340' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (735, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80608535', N'80608535', N'MEGABIC - 5 KG BUCKET', N'5kg/1 xô', N'PCE', N'', N'60', N'171115F01', N'5/8/2019', N'Z2-15', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:28.430' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (736, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'83911522', N'83911522', N'MIVISOL - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'0', N'60', N'171121F04', N'#N/A', N'Y2-08', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:32.567' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (737, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'83911522', N'83911522', N'MIVISOL - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'', N'60', N'171121F04', N'#N/A', N'Y2-11', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:32.647' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (738, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'83911522', N'83911522', N'MIVISOL - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'', N'9', N'171121F04', N'#N/A', N'Y1-03', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:32.727' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (739, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81907773', N'81907773', N'MIVISOL 1KG BAG', N'15 gói/thùng', N'PCE', N'587', N'404', N'171113F05', N'11/6/2018', N'W1-07', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:34.043' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (740, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84857971', N'84857971', N'NUSTIC 1KG', N'15 gói/thùng', N'PCE', N'1000', N'512', N'171106F04', N'4/28/2019', N'Y2-26', N'', N'', CAST(N'2017-12-01 01:06:38.787' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (741, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84857971', N'84857971', N'NUSTIC 1KG', N'15 gói/thùng', N'PCE', N'', N'226', N'171106F04', N'4/28/2019', N'W1-10', N'', N'', CAST(N'2017-12-01 01:06:38.893' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (742, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454648', N'57454648', N'ORGA-BREED - 1KG BAG', N'15 gói/thùng', N'PCE', N'764', N'284', N'170518F01', N'5/17/2018', N'W1-14', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:42.490' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (743, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454648', N'57454648', N'ORGA-BREED - 1KG BAG', N'15 gói/thùng', N'PCE', N'999', N'360', N'170518F02', N'5/17/2018', N'Z3-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:42.693' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (744, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454648', N'57454648', N'ORGA-BREED - 1KG BAG', N'15 gói/thùng', N'PCE', N'', N'480', N'170518F02', N'5/17/2018', N'W2-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:42.780' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (745, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454648', N'57454648', N'ORGA-BREED - 1KG BAG', N'15 gói/thùng', N'PCE', N'', N'159', N'170518F02', N'5/17/2018', N'W2-29', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:42.890' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (746, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454648', N'57454648', N'ORGA-BREED - 1KG BAG', N'15 gói/thùng', N'PCE', N'1002', N'360', N'170518F03', N'5/17/2018', N'W2-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:42.973' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (747, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454648', N'57454648', N'ORGA-BREED - 1KG BAG', N'15 gói/thùng', N'PCE', N'', N'390', N'170518F03', N'5/17/2018', N'Y2-25', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:43.060' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (748, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454648', N'57454648', N'ORGA-BREED - 1KG BAG', N'15 gói/thùng', N'PCE', N'', N'252', N'170518F03', N'5/17/2018', N'Y1-22', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:43.147' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (749, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600675', N'80600675', N'OSAMET FISH - 1KG BAG', N'15 gói/thùng', N'PCE', N'221', N'221', N'161221F11', N'12/15/2018', N'W1-15', N'', N'', CAST(N'2017-12-01 01:06:44.543' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (750, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600675', N'80600675', N'OSAMET FISH - 1KG BAG', N'15 gói/thùng', N'PCE', N'129', N'129', N'150328F04', N'3/25/2017', N'KV HOLD', N'', N'Hàng Reword', CAST(N'2017-12-01 01:06:44.630' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (751, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600675', N'80600675', N'OSAMET FISH - 1KG BAG', N'15 gói/thùng', N'PCE', N'167', N'117', N'170824F02', N'8/21/2019', N'Y1-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:44.717' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (752, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600691', N'80600691', N'OSAMET FISH - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'4', N'4', N'161221F10', N'12/15/2018', N'W1-16', N'', N'', CAST(N'2017-12-01 01:06:51.547' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (753, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600713', N'80600713', N'OSAMET SHRIMP - 500GM BAG', N'500GM', N'PCE', N'529', N'269', N'171016F02', N'9/27/2019', N'W1-17', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:55.977' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (754, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600713', N'80600713', N'OSAMET SHRIMP - 500GM BAG', N'500GM', N'PCE', N'1598', N'480', N'171103F01', N'10/30/2019', N'Z3-21', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:56.063' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (755, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600713', N'80600713', N'OSAMET SHRIMP - 500GM BAG', N'500GM', N'PCE', N'', N'480', N'171103F01', N'10/30/2019', N'Z4-17', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:56.140' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (756, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600713', N'80600713', N'OSAMET SHRIMP - 500GM BAG', N'500GM', N'PCE', N'', N'158', N'171103F01', N'10/30/2019', N'Z4-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:56.290' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (757, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600713', N'80600713', N'OSAMET SHRIMP - 500GM BAG', N'500GM', N'PCE', N'', N'480', N'171103F01', N'10/30/2019', N'Z4-08', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:06:56.337' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (758, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'2479', N'200', N'171025R02', N'8/22/2018', N'W2-02', N'', N'', CAST(N'2017-12-01 01:06:57.567' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (759, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'W2-04', N'', N'', CAST(N'2017-12-01 01:06:57.643' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (760, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'W2-07', N'', N'', CAST(N'2017-12-01 01:06:57.733' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (761, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'W2-09', N'', N'', CAST(N'2017-12-01 01:06:57.817' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (762, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'W2-11', N'', N'', CAST(N'2017-12-01 01:06:57.907' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (763, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'W2-10', N'', N'', CAST(N'2017-12-01 01:06:58.040' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (764, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'W2-14', N'', N'', CAST(N'2017-12-01 01:06:58.117' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (765, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'W2-16', N'', N'', CAST(N'2017-12-01 01:06:58.207' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (766, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'W2-19', N'', N'', CAST(N'2017-12-01 01:06:58.320' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (767, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'Y1-25', N'', N'', CAST(N'2017-12-01 01:06:58.393' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (768, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'Y1-23', N'', N'', CAST(N'2017-12-01 01:06:58.477' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (769, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171025R02', N'8/22/2018', N'Y1-27', N'', N'', CAST(N'2017-12-01 01:06:58.560' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (770, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82619381', N'82619381', N'PONDDTOX - 1KG BOX', N'1KG', N'PCE', N'', N'79', N'171025R02', N'8/22/2018', N'W1-18', N'', N'', CAST(N'2017-12-01 01:06:58.647' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (771, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'1890', N'200', N'170927R05', N'7/23/2019', N'Z2-28', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:04.030' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (772, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-21', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:04.110' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (773, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:04.247' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (774, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:04.337' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (775, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-17', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:04.423' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (776, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-22', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:04.517' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (777, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-19', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:04.657' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (778, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'106', N'170927R05', N'7/23/2019', N'W1-19', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:04.747' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (779, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'0', N'200', N'171124R01', N'#N/A', N'W2-08', N'', N'', CAST(N'2017-12-01 01:07:04.970' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (780, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-03', N'', N'', CAST(N'2017-12-01 01:07:05.080' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (781, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-05', N'', N'', CAST(N'2017-12-01 01:07:05.180' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (782, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-04', N'', N'', CAST(N'2017-12-01 01:07:05.300' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (783, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-06', N'', N'', CAST(N'2017-12-01 01:07:05.397' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (784, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-07', N'', N'', CAST(N'2017-12-01 01:07:05.520' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (785, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-09', N'', N'', CAST(N'2017-12-01 01:07:05.640' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (786, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-10', N'', N'', CAST(N'2017-12-01 01:07:05.750' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (787, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-13', N'', N'', CAST(N'2017-12-01 01:07:05.850' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (788, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-12', N'', N'', CAST(N'2017-12-01 01:07:05.960' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (789, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'W2-23', N'', N'', CAST(N'2017-12-01 01:07:06.053' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (790, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-15', N'', N'', CAST(N'2017-12-01 01:07:06.143' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (791, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-17', N'', N'', CAST(N'2017-12-01 01:07:06.293' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (792, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-18', N'', N'', CAST(N'2017-12-01 01:07:06.390' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (793, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'W2-15', N'', N'', CAST(N'2017-12-01 01:07:06.493' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (794, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-22', N'', N'', CAST(N'2017-12-01 01:07:06.583' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (795, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-20', N'', N'', CAST(N'2017-12-01 01:07:06.670' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (796, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'W2-21', N'', N'', CAST(N'2017-12-01 01:07:06.800' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (797, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'W2-22', N'', N'', CAST(N'2017-12-01 01:07:06.940' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (798, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'W2-28', N'', N'', CAST(N'2017-12-01 01:07:07.020' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (799, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y2-24', N'', N'', CAST(N'2017-12-01 01:07:07.157' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (800, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'W2-24', N'', N'', CAST(N'2017-12-01 01:07:07.437' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (801, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'Y1-28', N'', N'', CAST(N'2017-12-01 01:07:07.553' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (802, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'200', N'171124R01', N'#N/A', N'W2-13', N'', N'', CAST(N'2017-12-01 01:07:07.640' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (803, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82043722', N'82043722', N'PONDPLUS - 1KG BOX', N'1KG', N'PCE', N'', N'179', N'171124R01', N'#N/A', N'Z2-27', N'', N'', CAST(N'2017-12-01 01:07:07.720' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (804, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600764', N'80600764', N'QUICK BAYT - 20GM BAG', N'20GM', N'PCE', N'4200', N'1103', N'171019F05', N'10/19/2019', N'W1-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:08.027' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (805, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600764', N'80600764', N'QUICK BAYT - 20GM BAG', N'20GM', N'PCE', N'4200', N'4200', N'171023F06', N'10/23/2019', N'Y3-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:08.127' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (806, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600764', N'80600764', N'QUICK BAYT - 20GM BAG', N'20GM', N'PCE', N'0', N'9000', N'171118F03', N'#N/A', N'W2-26', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:08.210' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (807, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'739', N'383', N'170830R01', N'8/10/2019', N'W2-25', N'bao bi moi', N'', CAST(N'2017-12-01 01:07:12.193' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (808, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'', N'84', N'170830R01', N'8/10/2019', N'W1-21', N'bao bi moi', N'', CAST(N'2017-12-01 01:07:12.530' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (809, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'1151', N'480', N'170830R02', N'8/10/2019', N'Z3-17', N'bao bi moi', N'', CAST(N'2017-12-01 01:07:12.640' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (810, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'', N'191', N'170830R02', N'8/10/2019', N'Z4-19', N'bao bi moi', N'', CAST(N'2017-12-01 01:07:12.727' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (811, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'', N'480', N'170830R02', N'8/10/2019', N'Z3-20', N'bao bi moi', N'', CAST(N'2017-12-01 01:07:12.853' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (812, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'1151', N'384', N'170830R03', N'8/10/2019', N'W3-20', N'', N'', CAST(N'2017-12-01 01:07:12.963' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (813, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'', N'384', N'170830R03', N'8/10/2019', N'W3-17', N'', N'', CAST(N'2017-12-01 01:07:13.157' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (814, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'', N'383', N'170830R03', N'8/10/2019', N'W3-15', N'', N'', CAST(N'2017-12-01 01:07:13.397' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (815, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600799', N'80600799', N'RACUMIN TP - 1KG BAG', N'1kg', N'PCE', N'383', N'383', N'170830R04', N'8/10/2019', N'W3-16', N'', N'', CAST(N'2017-12-01 01:07:13.543' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (816, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'51838', N'5472', N'170808R07', N'7/12/2019', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:07:18.400' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (817, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R07', N'7/12/2019', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:07:18.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (818, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R07', N'7/12/2019', N'Z4-25', N'', N'', CAST(N'2017-12-01 01:07:18.950' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (819, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10366', N'170808R07', N'7/12/2019', N'Z4-10', N'', N'', CAST(N'2017-12-01 01:07:19.040' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (820, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'286', N'286', N'170808R08', N'5/31/2019', N'Z2-02', N'', N'', CAST(N'2017-12-01 01:07:19.117' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (821, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'51838', N'10368', N'170808R09', N'7/12/2019', N'Z5-19', N'', N'', CAST(N'2017-12-01 01:07:19.197' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (822, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R09', N'7/12/2019', N'Z4-14', N'', N'', CAST(N'2017-12-01 01:07:19.300' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (823, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R09', N'7/12/2019', N'Z4-05', N'', N'', CAST(N'2017-12-01 01:07:19.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (824, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R09', N'7/12/2019', N'Z4-02', N'', N'', CAST(N'2017-12-01 01:07:19.450' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (825, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10366', N'170808R09', N'7/12/2019', N'Z2-04', N'', N'', CAST(N'2017-12-01 01:07:19.523' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (826, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'51838', N'10368', N'170808R10', N'7/12/2019', N'Z5-21', N'', N'', CAST(N'2017-12-01 01:07:19.603' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (827, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R10', N'7/12/2019', N'Z3-22', N'', N'', CAST(N'2017-12-01 01:07:19.747' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (828, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R10', N'7/12/2019', N'Z5-28', N'', N'', CAST(N'2017-12-01 01:07:19.820' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (829, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R10', N'7/12/2019', N'Z4-12', N'', N'', CAST(N'2017-12-01 01:07:19.900' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (830, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10366', N'170808R10', N'7/12/2019', N'Z4-01', N'', N'', CAST(N'2017-12-01 01:07:19.973' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (831, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'51550', N'10368', N'170808R11', N'7/12/2019', N'Z4-23', N'', N'', CAST(N'2017-12-01 01:07:20.083' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (832, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R11', N'7/12/2019', N'Z5-13', N'', N'', CAST(N'2017-12-01 01:07:20.163' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (833, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R11', N'7/12/2019', N'Z4-27', N'', N'', CAST(N'2017-12-01 01:07:20.243' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (834, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10368', N'170808R11', N'7/12/2019', N'Z4-04', N'', N'', CAST(N'2017-12-01 01:07:20.360' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (835, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600802', N'80600802', N'RACUMIN TP - 20GM BAG', N'20gm', N'PCE', N'', N'10078', N'170808R11', N'7/12/2019', N'Z2-03', N'', N'', CAST(N'2017-12-01 01:07:20.437' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (836, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454737', N'57454737', N'SAIGO-NOX POULTRY - 1KG BAG', N'1KG', N'PCE', N'94', N'94', N'170608F01', N'6/7/2018', N'W1-26', N'', N'', CAST(N'2017-12-01 01:07:33.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (837, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600810', N'80600810', N'SEBACIL POUR ON - 100ML BOTTLE', N'100 ml', N'PCE', N'2127', N'555', N'171111F03', N'12/31/2018', N'O1-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:40.787' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (838, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600810', N'80600810', N'SEBACIL POUR ON - 100ML BOTTLE', N'100 ml', N'PCE', N'', N'1460', N'171111F03', N'12/31/2018', N'O1-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:40.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (839, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600829', N'80600829', N'SOLFAC WP 10 - 20GM BAG', N'20GM', N'PCE', N'2915', N'1515', N'171028F07', N'4/10/2019', N'W1-27', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:46.960' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (840, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600845', N'80600845', N'SOLFAC WP 10 - 4GM BAG', N'4GM', N'PCE', N'2730', N'582', N'171111F01', N'4/10/2019', N'W1-28', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:07:55.887' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (841, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'57454435', N'57454435', N'STOCKMILK - 1KG BAG', N'1KG', N'PCE', N'1000', N'101', N'171110F02', N'11/6/2018', N'W1-29', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:03.177' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (842, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'82407341', N'82407341', N'SUPASTOCK POWER PIG PACK - 1KG BAG', N'1KG', N'PCE', N'828', N'378', N'171113F06', N'11/6/2018', N'Y1-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:04.650' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (843, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600926', N'80600926', N'VIRKON A - 100GM BAG', N'100GM', N'PCE', N'4196', N'2118', N'171031F06', N'10/31/2019', N'Y1-05', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:19.753' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (844, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600926', N'80600926', N'VIRKON A - 100GM BAG', N'100GM', N'PCE', N'', N'996', N'171031F06', N'10/31/2019', N'', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:19.830' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (845, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600977', N'80600977', N'VIRKON S - 10GM BAG', N'10GM', N'PCE', N'2938', N'1538', N'170421F01', N'4/21/2019', N'P1-08', N'Bao bi moi', N'', CAST(N'2017-12-01 01:08:27.620' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (846, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600977', N'80600977', N'VIRKON S - 10GM BAG', N'10GM', N'PCE', N'4200', N'4200', N'170421F02', N'4/21/2019', N'P1-07', N'Bao bi moi', N'', CAST(N'2017-12-01 01:08:27.707' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (847, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600977', N'80600977', N'VIRKON S - 10GM BAG', N'10GM', N'PCE', N'4200', N'4200', N'170421F03', N'4/21/2019', N'Q3-07', N'', N'', CAST(N'2017-12-01 01:08:27.873' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (848, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600977', N'80600977', N'VIRKON S - 10GM BAG', N'10GM', N'PCE', N'4200', N'4200', N'170425F07', N'4/25/2019', N'P1-03', N'', N'', CAST(N'2017-12-01 01:08:27.983' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (849, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600977', N'80600977', N'VIRKON S - 10GM BAG', N'10GM', N'PCE', N'4200', N'4200', N'170425F08', N'4/25/2019', N'O2-02', N'', N'', CAST(N'2017-12-01 01:08:28.220' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (850, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'80600977', N'80600977', N'VIRKON S - 10GM BAG', N'10GM', N'PCE', N'6600', N'6600', N'170619F01', N'6/19/2019', N'P3-25', N'', N'', CAST(N'2017-12-01 01:08:28.567' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (851, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'85090593', N'85090593', N'SUPASTOCK 5KG BUCKET', N'5 KG', N'PCE', N'0', N'6', N'171123F06', N'#N/A', N'Y1-01', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:40.133' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (852, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81553378', N'81553378', N'AQUA C FISH PLUS - 5 KG BUCKET', N'5kg/1 xô', N'PCE', N'23', N'23', N'150326F04', N'3/23/2017', N'KV HOLD', N'', N'Hàng Reword', CAST(N'2017-12-01 01:08:40.527' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (853, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81553378', N'81553378', N'AQUA C FISH PLUS - 5 KG BUCKET', N'5kg/1 xô', N'PCE', N'30', N'12', N'171012F02', N'10/11/2019', N'Z1-04', N'', N'', CAST(N'2017-12-01 01:08:40.600' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (854, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81553378', N'81553378', N'AQUA C FISH PLUS - 5 KG BUCKET', N'5kg/1 xô', N'PCE', N'80', N'60', N'171116F04', N'11/7/2019', N'Y2-23', N'', N'', CAST(N'2017-12-01 01:08:40.683' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (855, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'81553378', N'81553378', N'AQUA C FISH PLUS - 5 KG BUCKET', N'5kg/1 xô', N'PCE', N'', N'20', N'171116F04', N'11/7/2019', N'Z3-28', N'', N'', CAST(N'2017-12-01 01:08:40.757' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (856, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84377856', N'84377856', N'NUSTIC - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'0', N'60', N'171122F07', N'#N/A', N'W2-12', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:46.850' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (857, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84377856', N'84377856', N'NUSTIC - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'', N'3', N'171122F07', N'#N/A', N'W1-11', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:46.943' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (858, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84377856', N'84377856', N'NUSTIC - 5KG BUCKET', N'5kg/1 xô', N'PCE', N'0', N'60', N'171122F08', N'#N/A', N'Z4-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:47.027' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (859, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84857955', N'84857955', N'OLI-MOS EXTRA 5KG', N'5kg/1 xô', N'PCE', N'0', N'19', N'171121F02', N'#N/A', N'W1-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:47.207' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (860, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84857955', N'84857955', N'OLI-MOS EXTRA 5KG', N'5kg/1 xô', N'PCE', N'0', N'60', N'171123F04', N'#N/A', N'W2-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:47.287' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (861, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84857955', N'84857955', N'OLI-MOS EXTRA 5KG', N'5kg/1 xô', N'PCE', N'', N'60', N'171123F04', N'#N/A', N'W2-05', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:47.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (862, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84857955', N'84857955', N'OLI-MOS EXTRA 5KG', N'5kg/1 xô', N'PCE', N'', N'60', N'171123F04', N'#N/A', N'W2-03', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:47.457' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (863, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'84857955', N'84857955', N'OLI-MOS EXTRA 5KG', N'5kg/1 xô', N'PCE', N'', N'20', N'171123F04', N'#N/A', N'W2-01', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:47.597' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (864, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 01:08:58.930' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (865, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'3', N'170209F09', N'2/9/2019', N'Z1-26', N'', N'', CAST(N'2017-12-01 01:08:59.007' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (866, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'150', N'170915F04', N'9/14/2019', N'Z1-29', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:59.090' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (867, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'346', N'171114F01', N'10/31/2019', N'W1-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:59.167' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (868, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'170', N'171113F02', N'10/31/2019', N'W1-03', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:59.270' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (869, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'221', N'161221F11', N'12/15/2018', N'W1-15', N'', N'', CAST(N'2017-12-01 01:08:59.347' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (870, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'4', N'161221F10', N'12/15/2018', N'W1-16', N'', N'', CAST(N'2017-12-01 01:08:59.487' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (871, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'269', N'171016F02', N'9/27/2019', N'W1-17', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:59.570' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (872, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:08:59.653' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (873, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'383', N'170830R01', N'8/10/2019', N'W2-25', N'bao bi moi', N'', CAST(N'2017-12-01 01:08:59.743' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (874, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:08:59.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (875, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'555', N'171111F03', N'12/31/2018', N'O1-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:08:59.967' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (876, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'1515', N'171028F07', N'4/10/2019', N'W1-27', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:00.073' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (877, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'582', N'171111F01', N'4/10/2019', N'W1-28', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:00.157' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (878, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:00.287' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (879, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:00.363' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (880, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'1538', N'170421F01', N'4/21/2019', N'P1-08', N'Bao bi moi', N'', CAST(N'2017-12-01 01:09:00.437' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (881, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:00.513' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (882, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:00.593' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (883, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:00.720' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (884, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:00.807' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (885, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'4420', N'161108R06', N'8/1/2019', N'Z1-07', N'', N'', CAST(N'2017-12-01 01:09:00.887' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (886, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:00.963' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (887, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:01.040' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (888, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'79', N'170901F05', N'8/29/2018', N'Z1-22', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:01.120' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (889, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'23', N'150326F04', N'3/23/2017', N'KV HOLD', N'', N'Hàng Reword', CAST(N'2017-12-01 01:09:01.257' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (890, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'0', N'0', N'#N/A', N'0', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:01.343' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (891, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'404', N'171113F05', N'11/6/2018', N'W1-07', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:01.433' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (892, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-28', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:01.510' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (893, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'378', N'171113F06', N'11/6/2018', N'Y1-02', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:01.583' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (894, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:01.660' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (895, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-02', N'', N'', CAST(N'2017-12-01 01:09:01.757' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (896, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'57', N'170925F03', N'9/16/2019', N'Z1-03', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:01.830' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (897, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:01.910' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (898, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:02.037' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (899, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:02.247' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (900, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', N'', CAST(N'2017-12-01 01:09:02.353' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (901, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:02.433' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (902, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'467', N'170313F07', N'3/2/2018', N'Y1-11', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:02.507' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (903, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'101', N'171110F02', N'11/6/2018', N'W1-29', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:02.597' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (904, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'1553', N'171113F04', N'11/2/2018', N'Z1-13', N'bao bi moi', N'', CAST(N'2017-12-01 01:09:02.683' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (905, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:02.760' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (906, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:02.830' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (907, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'512', N'171028F01', N'10/19/2019', N'Z2-29', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:02.950' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (908, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'56', N'171031F01', N'9/14/2019', N'Z1-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:03.027' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (909, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:03.110' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (910, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:03.183' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (911, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:03.287' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (912, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'464', N'171010F05', N'6/21/2019', N'Z1-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:03.380' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (913, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:03.460' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (914, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'1120', N'171026F06', N'4/3/2021', N'W2-01', N'bao bi moi', N'', CAST(N'2017-12-01 01:09:03.590' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (915, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'2400', N'171114F01', N'10/31/2019', N'Z2-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:03.670' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (916, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'129', N'150328F04', N'3/25/2017', N'KV HOLD', N'', N'Hàng Reword', CAST(N'2017-12-01 01:09:03.753' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (917, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'480', N'171103F01', N'10/30/2019', N'Z3-21', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:03.837' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (918, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'1103', N'171019F05', N'10/19/2019', N'W1-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:03.913' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (919, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'84', N'170830R01', N'8/10/2019', N'W1-21', N'bao bi moi', N'', CAST(N'2017-12-01 01:09:04.033' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (920, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'2118', N'171031F06', N'10/31/2019', N'Y1-05', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:04.180' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (921, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'4200', N'170421F02', N'4/21/2019', N'P1-07', N'Bao bi moi', N'', CAST(N'2017-12-01 01:09:04.293' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (922, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'0', N'0', N'#N/A', N'0', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:04.370' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (923, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'6', N'171115F02', N'3/1/2020', N'Z1-27', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:04.450' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (924, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'46', N'171115F01', N'5/8/2019', N'W1-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:04.527' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (925, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:04.600' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (926, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:04.730' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (927, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:04.807' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (928, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'12', N'171012F02', N'10/11/2019', N'Z1-04', N'', N'', CAST(N'2017-12-01 01:09:04.883' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (929, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-21', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:04.963' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (930, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:05.083' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (931, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:05.163' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (932, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-04', N'', N'', CAST(N'2017-12-01 01:09:05.243' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (933, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'396', N'171116F05', N'#N/A', N'Z4-26', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:05.337' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (934, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'88', N'171031F04', N'10/27/2019', N'W1-12', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:05.480' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (935, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:05.593' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (936, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'512', N'171106F04', N'4/28/2019', N'Y2-26', N'', N'', CAST(N'2017-12-01 01:09:05.730' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (937, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:05.800' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (938, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:05.880' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (939, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'360', N'170518F02', N'5/17/2018', N'Z3-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:05.963' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (940, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:06.043' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (941, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'480', N'171010F05', N'6/21/2019', N'Z2-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:06.190' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (942, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:06.300' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (943, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'117', N'170824F02', N'8/21/2019', N'Y1-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:06.380' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (944, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'480', N'171103F01', N'10/30/2019', N'Z4-17', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:06.460' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (945, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'4200', N'171023F06', N'10/23/2019', N'Y3-24', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:06.540' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (946, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'480', N'170830R02', N'8/10/2019', N'Z3-17', N'', N'', CAST(N'2017-12-01 01:09:06.660' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (947, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:06.747' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (948, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'996', N'171031F06', N'10/31/2019', N'0', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:06.830' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (949, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'4200', N'170421F03', N'4/21/2019', N'Q3-07', N'', N'', CAST(N'2017-12-01 01:09:06.953' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (950, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:07.040' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (951, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:07.113' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (952, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'28', N'161108R06', N'8/1/2019', N'Y1-15', N'', N'', CAST(N'2017-12-01 01:09:07.217' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (953, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:07.317' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (954, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:07.393' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (955, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'60', N'171116F04', N'11/7/2019', N'Y2-23', N'', N'', CAST(N'2017-12-01 01:09:07.540' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (956, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:07.633' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (957, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'0', N'0', N'#N/A', N'0', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:07.713' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (958, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-07', N'', N'', CAST(N'2017-12-01 01:09:07.810' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (959, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:07.913' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (960, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'226', N'171106F04', N'4/28/2019', N'W1-10', N'', N'', CAST(N'2017-12-01 01:09:07.990' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (961, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'320', N'171116F03', N'11/2/2018', N'Z1-14', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:08.070' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (962, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'480', N'170518F02', N'5/17/2018', N'W2-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:08.160' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (963, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'33', N'171110F01', N'10/30/2018', N'Z1-10', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:08.260' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (964, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'384', N'171101F07', N'11/1/2019', N'Y2-01', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:08.340' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (965, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:08.413' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (966, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'158', N'171103F01', N'10/30/2019', N'Z4-13', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:08.527' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (967, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'191', N'170830R02', N'8/10/2019', N'Z4-19', N'', N'', CAST(N'2017-12-01 01:09:08.630' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (968, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:08.703' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (969, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'4200', N'170425F07', N'4/25/2019', N'P1-03', N'', N'', CAST(N'2017-12-01 01:09:08.780' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (970, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:08.890' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (971, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'60', N'171115F01', N'5/8/2019', N'Z2-16', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:08.970' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (972, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'384', N'171103F02', N'1/29/2019', N'Z2-23', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:09.047' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (973, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'20', N'171116F04', N'11/7/2019', N'Z3-28', N'', N'', CAST(N'2017-12-01 01:09:09.143' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (974, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-06', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:09.243' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (975, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:09.320' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (976, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-09', N'', N'', CAST(N'2017-12-01 01:09:09.393' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (977, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:09.520' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (978, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'159', N'170518F02', N'5/17/2018', N'W2-29', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:09.593' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (979, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'313', N'171101F07', N'11/1/2019', N'Z2-09', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:09.683' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (980, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'480', N'171103F01', N'10/30/2019', N'Z4-08', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:09.770' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (981, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'480', N'170830R02', N'8/10/2019', N'Z3-20', N'', N'', CAST(N'2017-12-01 01:09:09.847' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (982, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:09.927' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (983, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'4200', N'170425F08', N'4/25/2019', N'O2-02', N'', N'', CAST(N'2017-12-01 01:09:10.060' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (984, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'60', N'171115F01', N'5/8/2019', N'Z2-15', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:10.137' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (985, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'11', N'171103F02', N'1/29/2019', N'Z1-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:10.250' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (986, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-17', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:10.333' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (987, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:10.407' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (988, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-11', N'', N'', CAST(N'2017-12-01 01:09:10.490' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (989, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:10.607' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (990, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'360', N'170518F03', N'5/17/2018', N'W2-18', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:10.683' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (991, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'384', N'170830R03', N'8/10/2019', N'W3-20', N'', N'', CAST(N'2017-12-01 01:09:10.760' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (992, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:10.853' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (993, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'6600', N'170619F01', N'6/19/2019', N'P3-25', N'', N'', CAST(N'2017-12-01 01:09:10.923' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (994, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'384', N'171103F03', N'1/29/2019', N'Y3-20', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:11.003' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (995, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-22', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:11.097' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (996, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-10', N'', N'', CAST(N'2017-12-01 01:09:11.217' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (997, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'390', N'170518F03', N'5/17/2018', N'Y2-25', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:11.317' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (998, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'384', N'170830R03', N'8/10/2019', N'W3-17', N'', N'', CAST(N'2017-12-01 01:09:11.393' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (999, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:11.483' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1000, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'414', N'171103F03', N'1/29/2019', N'Y3-16', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:11.560' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1001, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'170927R05', N'7/23/2019', N'Z2-19', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:11.633' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1002, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-14', N'', N'', CAST(N'2017-12-01 01:09:11.713' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1003, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'252', N'170518F03', N'5/17/2018', N'Y1-22', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:11.840' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1004, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'383', N'170830R03', N'8/10/2019', N'W3-15', N'', N'', CAST(N'2017-12-01 01:09:11.917' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1005, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:11.997' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1006, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:12.090' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1007, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-16', N'', N'', CAST(N'2017-12-01 01:09:12.163' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1008, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'383', N'170830R04', N'8/10/2019', N'W3-16', N'', N'', CAST(N'2017-12-01 01:09:12.317' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1009, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:12.393' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1010, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:12.473' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1011, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'W2-19', N'', N'', CAST(N'2017-12-01 01:09:12.570' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1012, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'#REF!', N'', CAST(N'2017-12-01 01:09:12.647' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1013, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'106', N'170927R05', N'7/23/2019', N'W1-19', N'BAO BI MOI', N'', CAST(N'2017-12-01 01:09:12.723' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1014, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'Y1-25', N'', N'', CAST(N'2017-12-01 01:09:12.807' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1015, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R07', N'7/12/2019', N'duoi SX', N'', N'', CAST(N'2017-12-01 01:09:12.913' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1016, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'Y1-23', N'', N'', CAST(N'2017-12-01 01:09:13.000' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1017, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R07', N'7/12/2019', N'Z4-25', N'', N'', CAST(N'2017-12-01 01:09:13.077' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1018, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'200', N'171025R02', N'8/22/2018', N'Y1-27', N'', N'', CAST(N'2017-12-01 01:09:13.153' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1019, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10366', N'170808R07', N'7/12/2019', N'Z4-10', N'', N'', CAST(N'2017-12-01 01:09:13.263' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1020, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'79', N'171025R02', N'8/22/2018', N'W1-18', N'', N'', CAST(N'2017-12-01 01:09:13.340' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1021, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'286', N'170808R08', N'5/31/2019', N'Z2-02', N'', N'', CAST(N'2017-12-01 01:09:13.413' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1022, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R09', N'7/12/2019', N'Z5-19', N'', N'', CAST(N'2017-12-01 01:09:13.497' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1023, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R09', N'7/12/2019', N'Z4-14', N'', N'', CAST(N'2017-12-01 01:09:13.623' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1024, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R09', N'7/12/2019', N'Z4-05', N'', N'', CAST(N'2017-12-01 01:09:13.703' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1025, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R09', N'7/12/2019', N'Z4-02', N'', N'', CAST(N'2017-12-01 01:09:13.780' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1026, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10366', N'170808R09', N'7/12/2019', N'Z2-04', N'', N'', CAST(N'2017-12-01 01:09:13.877' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1027, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R10', N'7/12/2019', N'Z5-21', N'', N'', CAST(N'2017-12-01 01:09:13.960' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1028, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R10', N'7/12/2019', N'Z3-22', N'', N'', CAST(N'2017-12-01 01:09:14.047' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1029, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R10', N'7/12/2019', N'Z5-28', N'', N'', CAST(N'2017-12-01 01:09:14.123' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1030, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R10', N'7/12/2019', N'Z4-12', N'', N'', CAST(N'2017-12-01 01:09:14.200' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1031, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10366', N'170808R10', N'7/12/2019', N'Z4-01', N'', N'', CAST(N'2017-12-01 01:09:14.297' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1032, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R11', N'7/12/2019', N'Z4-23', N'', N'', CAST(N'2017-12-01 01:09:14.433' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1033, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R11', N'7/12/2019', N'Z5-13', N'', N'', CAST(N'2017-12-01 01:09:14.513' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1034, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R11', N'7/12/2019', N'Z4-27', N'', N'', CAST(N'2017-12-01 01:09:14.593' AS DateTime))
GO
INSERT [_ExcelLocation] ([ID], [FullPath], [FileName], [SheetName], [ItemCode], [ItemName], [PackagingSize], [Unit], [Balance], [Quantity], [ProductLot], [Expirydate], [Location], [Change], [Remark], [CreatedDateTime]) VALUES (1035, N'H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\THE KHO-THE DINH VI HANG GOI.xlsx', N'THE KHO-THE DINH VI HANG GOI.xlsx', N'Location', N'#REF!', N'42', N'', N'', N'', N'10368', N'170808R11', N'7/12/2019', N'Z4-04', N'', N'', CAST(N'2017-12-01 01:09:14.680' AS DateTime))
GO
SET IDENTITY_INSERT [_ExcelLocation] OFF
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700001', N'171128V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700003', N'170530R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700006', N'171123F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700007', N'151226F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700008', N'170818A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700010', N'171101F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700011', N'171122F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700019', N'171122F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700020', N'171123F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700024', N'170706R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700029', N'171118F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700034', N'171117A06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700037', N'171028B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700038', N'171129B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700039', N'170113R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700042', N'170113R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700045', N'171003F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700051', N'170626R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700053', N'170207R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700053', N'170325F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700053', N'170327R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700053', N'170926F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700053', N'171002F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700053', N'171004F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700053', N'171010F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700053', N'171020F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700054', N'170927R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700059', N'171114F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700067', N'171128A10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700068', N'171108A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700070', N'170612R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700074', N'171122F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700075', N'170918R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700076', N'170314R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700077', N'170605F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700078', N'170808R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700081', N'170411R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700083', N'170808R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700084', N'171113F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700087', N'170927R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700092', N'171129A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700097', N'171130V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700100', N'171201F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700107', N'171103F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071700110', N'171103F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'161108R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170211F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170221R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170306R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170316R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170327R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170403R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170413F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170519F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170529F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170706R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170808R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170814A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170814A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170814A03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170814A04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170814A05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170814A06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170818A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170818F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170818F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170819A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170819A03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170819A04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170819F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170819F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170823F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170826A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170826F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170830F13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170830F14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170830R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170914F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170915F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170915F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170916F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170918F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170918F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170918F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170918V18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170919F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170919V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170922V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170923A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170923A03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170926B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170927A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170927A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170927A03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170927A05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170927R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'170930B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171003F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171010F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171027F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171027F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171108A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171110A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171113F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171114F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PA071799999', N'171128A08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700003', N'170714R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700004', N'170726F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700005', N'170727F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700006', N'170727F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700007', N'170727F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700008', N'170727F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700009', N'170727F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700010', N'170727F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700011', N'170727F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700013', N'170728F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700014', N'170728F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700015', N'171111F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700016', N'171128F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700017', N'170728F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700017', N'170731F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700021', N'171201F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700022', N'170411R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700023', N'171120F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700024', N'171128F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700025', N'170327R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700026', N'171031F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700027', N'171129F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700029', N'171109V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700029', N'171125V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700030', N'171118F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700031', N'170825F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700033', N'170802F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700034', N'170802F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700035', N'171111V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700040', N'170110R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700041', N'170805F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700043', N'170612F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700044', N'170612R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700045', N'170602B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700046', N'170807V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700046', N'170824F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700048', N'170808V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700048', N'170824F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700049', N'170707R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700049', N'170803F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700050', N'170808F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700051', N'170706R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700052', N'171201F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700054', N'170518F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700055', N'171103F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700057', N'171121F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700059', N'170928R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700060', N'171127F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700064', N'171114F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700065', N'171128F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700066', N'170530R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700068', N'170605F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700069', N'170612R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700070', N'170808R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700071', N'171128F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700074', N'171116F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700077', N'171116F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700080', N'170927R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700081', N'170301R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700086', N'171010F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700086', N'171127F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700087', N'170912F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700089', N'170518F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700090', N'171129F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700091', N'170925F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700091', N'171026F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700091', N'171103F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700091', N'171113F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700091', N'171116F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700091', N'171117F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700091', N'171120F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700091', N'171127F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700092', N'171122F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700093', N'171122F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700094', N'170817R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700097', N'171129F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700098', N'171122F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700099', N'170530R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700101', N'170419F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700102', N'170530R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700103', N'170817F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700105', N'170817F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700106', N'170816F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700106', N'170830F16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700106', N'170911F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700109', N'171106F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700110', N'170511R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700111', N'170313R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700111', N'170327R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700111', N'170327R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700111', N'170605F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700111', N'170830R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700115', N'170612R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700116', N'171128B21')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700118', N'171128F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700121', N'170815F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700121', N'170828V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700121', N'170830F12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700121', N'170901F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700122', N'170915F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700123', N'170511R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700124', N'170313R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700124', N'170829F11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700124', N'170905F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700126', N'171129V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700127', N'170518F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700128', N'170830R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700130', N'171127F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700133', N'170927R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700134', N'170411R08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700135', N'170313R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700138', N'170313F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700138', N'170817F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700139', N'171129B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700140', N'170817F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700140', N'170821F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700140', N'170830F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700141', N'170313R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700142', N'171109A04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700143', N'171110B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700145', N'171123F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700147', N'171130B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700148', N'170909B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700150', N'160729R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700151', N'170816F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700152', N'170818F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700154', N'170110R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700154', N'170207R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700156', N'170518F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700159', N'171118F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700159', N'171125F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700160', N'171123F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700161', N'170912F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700162', N'170818B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700164', N'171128F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700165', N'170818F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700167', N'170313R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700168', N'170818F11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700169', N'171118F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700171', N'170818F12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700172', N'171106F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700174', N'170830R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700175', N'171115F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700180', N'171128F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700181', N'171130B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700182', N'171201F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700183', N'171106A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700187', N'171129B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700189', N'171125F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700191', N'170124R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700192', N'170830R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700192', N'171127F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700194', N'171130V23')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700198', N'170327R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700201', N'170819F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700201', N'170909F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700204', N'171106F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700205', N'171130B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700206', N'171128F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700208', N'170928R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700210', N'170327R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700211', N'171023F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700212', N'171201F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700213', N'170919V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700214', N'170808R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700215', N'170313R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700216', N'171103F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700217', N'171118V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700218', N'171130B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700219', N'170417F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700220', N'171111B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700221', N'170828B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700222', N'171026F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700223', N'170113R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700223', N'170707R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700223', N'170725R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700223', N'170918R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700225', N'171010F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700227', N'170518F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700228', N'171120F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700229', N'171016F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700229', N'171128F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700231', N'170207R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700232', N'171128F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700233', N'171127F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700234', N'170928R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700235', N'170211F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700236', N'171123F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700237', N'171127F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700238', N'171122F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700240', N'170327R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700240', N'170327R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700240', N'171125F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700240', N'171127F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700241', N'171129F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700242', N'170830R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700244', N'171121B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700245', N'170530R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700246', N'170113R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700247', N'171113F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700248', N'171130B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700249', N'151225F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700249', N'170221R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700251', N'171018F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700252', N'170511R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700253', N'171122F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700254', N'171122F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700255', N'171122F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700256', N'171121F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700258', N'171103F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700259', N'170421F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700260', N'170612R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700261', N'170826B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700262', N'171201F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700264', N'171129B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700265', N'170927R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700267', N'170403R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700268', N'171124F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700269', N'171121F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700272', N'171031F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700273', N'171028F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700274', N'170327R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700275', N'171128F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700278', N'171127F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700284', N'171122F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700286', N'171130V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700288', N'170420R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700290', N'171118F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700291', N'170313R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700292', N'170110R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700293', N'171128F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700294', N'170612R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700295', N'170403F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700296', N'171116F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700297', N'171101F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700299', N'171125F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700301', N'170113R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700302', N'171130V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700304', N'170612R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700305', N'170909F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700306', N'170518F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700307', N'171123F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700310', N'171124B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700311', N'170530R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700312', N'171127V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700312', N'171127V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700313', N'171122A07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700316', N'170313F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700316', N'170830R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700316', N'171117F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700316', N'171129F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700317', N'170928R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700318', N'171028F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700319', N'170911F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700321', N'171128F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700322', N'171122F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700323', N'171111F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700324', N'170327R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700324', N'171127F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700324', N'171127F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700326', N'171129B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700327', N'170207R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700328', N'171122F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700328', N'171122F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700328', N'171128F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700328', N'171129F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700330', N'171129F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700331', N'171127V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700333', N'171127F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700334', N'170923F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700335', N'170518F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700336', N'170325F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700338', N'170405F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700339', N'170216F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700372', N'171130V20')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700381', N'171128V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700382', N'170113R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700383', N'171127F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700385', N'170327R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700385', N'170911R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700385', N'171127F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700387', N'170926F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700388', N'170518F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700390', N'170327R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700392', N'171115F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700395', N'171110F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700396', N'171108A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700396', N'171129B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700397', N'171010F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700406', N'171012F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700407', N'170530R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700408', N'170411R08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700409', N'171016F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700410', N'170411R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700411', N'171116F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700412', N'171115F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700422', N'171110F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700423', N'171110F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700424', N'170421F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700428', N'170612F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700430', N'171111R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700598', N'170324F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700598', N'170520F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700598', N'170621F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071700598', N'170804F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'151225F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'160810F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'161108R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'161128R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'161128R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'161228R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170113R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170118R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170124R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170207R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170207R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170207R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170207R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170211F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170211F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170221R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170221R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170221R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170301R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170301R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170301R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170301R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170302F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170313R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170313R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170313R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170316R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170316R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170316R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170316R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170324F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170325F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170327R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170327R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170327R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170327R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170330F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170403F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170404F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170405F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170411F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170413F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170415F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170417F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170417F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170417F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170417F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170417F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170417F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170418F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170419F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170419F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170420F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170420F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170420R07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170425F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170425F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170426F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170428F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170428F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170503F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170503F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170504F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170504F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170504F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170505F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170511R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170517F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170518R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170519F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170519F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170519F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170520F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170520R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170522F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170523F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170523R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170525F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170531F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170531F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170601F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170602B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170602F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170605F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170606F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170607F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170612F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170612R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170613F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170620F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170621F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170622F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170626R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170629F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170630F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170707R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170714F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170714R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170714R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170720F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170720F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170720F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170724F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170724F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170725F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170725F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170725R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170726F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170726F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170727F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170727F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170727F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170727F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170728F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170728F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170731F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170801F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170801F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170801F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170801F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170802F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170802F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170802V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170803F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170803V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170803V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170804F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170805F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170805F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170807F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170807V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170808F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170808F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170808V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170810F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170810F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170810F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170811B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170811F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170811F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170811F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170811F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170812F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170812F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170812F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170812F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170812F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170814B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170814F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170814F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170814F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170815F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170815F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170815F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170815F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170815F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170816F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170816F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170816F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170817B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170817F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170817F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170817F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170817F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170817F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170817R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F20')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170818F21')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170819F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170819F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170819F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170819F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170819F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170819F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170821F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170821F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170821F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170821F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170822F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170822F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170822F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170822F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170822F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170822F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170822F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170823B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170823F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170823F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170823F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170823F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170823F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B20')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B21')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824B22')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170824F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170825F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170826F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828V19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828V20')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828V21')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170828V22')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829F11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170829V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830B17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170830F17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170831F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170901F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170901F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170901F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170901F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170901F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170901F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170901F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905A04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905A05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905A06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170905F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170906F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170906F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170906F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170906F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170906F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170906F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170906V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170906V19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B20')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907B22')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170907F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170908F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170909F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170910F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170910F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911B17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911B21')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170911R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170912V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170913F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170914F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170914F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170914F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170914F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170914F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170915F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170915F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170915F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170915F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170915F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170915F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170915F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170916F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170916F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170916F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170916V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170918V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170919F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170919F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170919F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170919F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170919V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170919V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170921V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170922F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170922F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170922F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170922F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170922F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170922F10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170922V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170923B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170923B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170923F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170923F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170923F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170923F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170925B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170925F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170925F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170925F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170925F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170925F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170925V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170926F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170926F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170926F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170926F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170926F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170926F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170926V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170927F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170927F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170927F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170927R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170928B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170928R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170928V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170928V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170928V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170928V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170928V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170928V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170929F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'170930B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171002F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171002F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171002F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171003A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171003F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171003F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171004B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171005F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171005F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171005F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171005F10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171006F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171007B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171009F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171010F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171010F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171010F99')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171013F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171014F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171016F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171018F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171018F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171018F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171020A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171025F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171026F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171026F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171031F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171031F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171101B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171101F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171102F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171103F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171110A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171110F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171111F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171113F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171113F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171113F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171114F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171116F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171117F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171120F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171120F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171121F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171122F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171122F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171122F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171123F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171124F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171124F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171125F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171125F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171127F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171127F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171127F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171128F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171128F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171129F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PF071799999', N'171129F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700001', N'171128A08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700002', N'170602B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700002', N'170905A05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700002', N'170906V19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700002', N'170911B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700002', N'170912B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700002', N'170912B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700002', N'170912B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700003', N'170923B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700005', N'170530R04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700006', N'170418F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700006', N'170916F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700009', N'171122F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700009', N'171122F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700010', N'170818A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700012', N'170221R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700014', N'170927R05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700016', N'171129B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700019', N'171114F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700020', N'171120F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700022', N'170330F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700023', N'171120F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700032', N'170901F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700048', N'171012F98')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700052', N'170408F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700052', N'170410F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700052', N'170519F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700056', N'171106F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700057', N'171128F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700058', N'171129A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700059', N'170911R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700060', N'170911R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700061', N'171122F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700062', N'171128F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700067', N'171103F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700068', N'171103F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700069', N'171201F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700071', N'171117A05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700072', N'171117A06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700073', N'171122A07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700074', N'171128A09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700075', N'171129B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700076', N'171128A10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700077', N'171129A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071700078', N'171129A02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170301R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170316R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170411F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170412F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170609F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170612R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170817R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170818A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170818F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170818F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170818F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170818F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170819A01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170826B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170826B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170905A04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170906V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170909F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170913B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170918F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170919V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170919V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170921F09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170921F10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170923B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'170926F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171010F98')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171010F99')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171011F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171012F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171012F98')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171012F99')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171028F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171031F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171117F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171122F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PP071799999', N'171128A09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700016', N'170922F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700017', N'171010F99')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700017', N'171011F97')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700018', N'171010F99')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700018', N'171011F97')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700020', N'171130B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700021', N'170420F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700021', N'170523R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700021', N'170626R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700021', N'170910F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700021', N'171016F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700021', N'171109V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700021', N'171109V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700026', N'170316R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700026', N'170908F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700028', N'170913F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700028', N'170921F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700050', N'171111F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700055', N'171129B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700057', N'170803V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700061', N'171130B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700063', N'170804V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700066', N'171129V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700067', N'170804V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700069', N'171129B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700070', N'170805B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700071', N'170805B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700073', N'170805B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700074', N'170814B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700075', N'170923B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700076', N'171129V19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700077', N'170805B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700077', N'170901V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700078', N'170915F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700078', N'170918V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700079', N'170805V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700080', N'171130V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700081', N'170805V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700082', N'171130B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700085', N'171123F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700087', N'171124B23')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700093', N'170926B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700095', N'170807V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700097', N'171129V20')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700100', N'171129V22')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700101', N'170930B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700102', N'170816V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700103', N'171129B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700107', N'170927B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700108', N'171114V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700109', N'170814B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700112', N'170814B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700121', N'171129B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700124', N'170816V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700125', N'170816V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700125', N'170816V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700126', N'170816V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700126', N'170921V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700127', N'170918V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700131', N'170816V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700134', N'170926V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700135', N'170926V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700136', N'170817V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700136', N'170829B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700137', N'170925B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700138', N'171130V22')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700139', N'171130V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700140', N'170817V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700140', N'170824B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700140', N'170830B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700142', N'170817V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700142', N'170824B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700142', N'170830B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700143', N'170817V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700144', N'171123B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700147', N'171130V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700149', N'171130B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700150', N'171129B17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700151', N'170828B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700152', N'170829B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700153', N'170922V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700154', N'170829B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700154', N'170906V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700155', N'170829B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700155', N'170906V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700156', N'171130B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700159', N'170829B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700160', N'170830B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700160', N'170907B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700161', N'170829V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700162', N'170830B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700166', N'170916V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700167', N'170817V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700167', N'170824B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700168', N'171129V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700170', N'171130V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700171', N'170925B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700176', N'171130B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700179', N'170825B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700180', N'171116B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700184', N'171128B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700185', N'170818B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700186', N'171130V19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700187', N'170818B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700190', N'170829B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700191', N'170819B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700193', N'170819B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700195', N'171130B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700198', N'171128F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700200', N'171201F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700202', N'170523R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700202', N'171106F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700202', N'171127F04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700202', N'171129F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700205', N'170911B23')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700209', N'171129B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700217', N'171129V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700219', N'170918V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700222', N'171129V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700228', N'170916V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700228', N'170916V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700230', N'170207R06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700230', N'170325F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700230', N'170926F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700230', N'171020F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700230', N'171101F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700230', N'171101F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700234', N'171129B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700237', N'170925B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700239', N'171129B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700241', N'171122B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700242', N'170830R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700242', N'171023F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700242', N'171028F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700242', N'171113F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700242', N'171129F02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700242', N'171129F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700243', N'171130V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700246', N'171129B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700248', N'171129V18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700254', N'171129V23')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700260', N'171130B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700265', N'171111V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700269', N'171129V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700272', N'171130V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700275', N'170929V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700278', N'171130B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700285', N'171009V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700286', N'171009B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700291', N'171110B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700294', N'171020B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700295', N'171111B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700302', N'171130B17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700459', N'171128V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700463', N'171130B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700466', N'171129V17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700472', N'171130V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700474', N'171109V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700476', N'171127V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700484', N'171128V23')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700487', N'171129V21')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700489', N'171201F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700521', N'171129B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700531', N'171130V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700533', N'171129B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700533', N'171129B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700534', N'171124B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700540', N'171125B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700542', N'171127V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700545', N'171130V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700551', N'171129B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700552', N'171130B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700553', N'171201B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700554', N'171201B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700555', N'171201B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700562', N'171201B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700563', N'171130V21')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700564', N'171201F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700565', N'171201F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700568', N'171130V18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700570', N'171130V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700571', N'171130V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700572', N'171130V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700573', N'171130V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071700574', N'171130V17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170110R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170316R03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170324F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170412F05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170519F08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170602B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170707R02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170714R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170720F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170725F03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170801V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170801V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170801V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170801V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170802V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170802V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170803V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170804V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170804V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170804V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170804V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170804V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170804V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170804V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170805V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170807V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170807V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170807V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170807V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170807V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170807V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170807V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170808V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170808V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170808V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170808V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170808V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170808V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170808V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170810B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170811B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170814B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170815B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170815B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170815B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170815B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170815B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170815B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170816V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817R01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170817V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170818B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170818B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170818B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170818B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170818B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170818B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170819B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170819B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170819B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170819B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170819B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170821F01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170823B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170823B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170823B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170823B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170823B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170824B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170824B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170824B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170824B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170824B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170826B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170826B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170826B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170826B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170826B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828B17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828V17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170828V18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170829V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170830B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170831B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170831B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170831B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170831B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170831B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170831B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170831B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170831B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170905B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170905B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170905B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170906V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170906V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170906V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170906V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170906V17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170906V18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170907B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170909B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B19')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170911B22')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170912V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170913B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170913B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170913B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170913B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170913B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170913B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170913B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170913B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170915F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170916F07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170916V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170916V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170916V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170916V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170918V17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170919V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170919V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170919V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170919V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170919V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170919V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170921F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170921V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170921V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170921V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170921V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170921V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170921V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170921V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170922V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170922V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170922V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170922V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170922V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170922V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170922V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170922V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170923B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170925V18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926B18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V14')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V16')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V17')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170926V18')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B12')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B13')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170927B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170928B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170928V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170928V03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170928V08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170928V10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170929V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170929V02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170929V04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170929V05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170930B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170930B05')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170930B06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170930B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170930B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170930B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'170930V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171005B11')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171005F10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171007B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171007V06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171009V09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171010B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171010B03')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171010B07')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171010B08')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171010B09')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171010B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171011B02')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171011B04')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171023B15')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171023F06')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171030B10')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171118B01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171125V01')
GO
INSERT [_ExcelPalletProductLot] ([PalletCode], [ProductLot]) VALUES (N'PW071799999', N'171129F02')
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [proc_Barcode_Tracking]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type

	CREATE TABLE #tmp
	(
		LocationCode NVARCHAR(255)
		, PalletCode NVARCHAR(255)
		, CartonBarcode NVARCHAR(255)
		, ProductBarcode NVARCHAR(255)
		, EncryptedProductBarcode NVARCHAR(255)
		, ProductLot NVARCHAR(255)
		, ProductID INT
		, ProductCode NVARCHAR(255)
		, ProductDescription NVARCHAR(255)
	)

	IF @_Type = 'P'
		BEGIN
			INSERT INTO #tmp
			SELECT DISTINCT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = COUNT(DISTINCT ProductBarcode)
				, EncryptedProductBarcode = ''
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.PalletCode LIKE '%' + @_Barcode + '%'
			GROUP BY 
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT DISTINCT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.PalletCode LIKE '%' + @_Barcode + '%'
		END
	ELSE IF @_Type = 'C'
		BEGIN
			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.CartonBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.CartonBarcode LIKE '%' + @_Barcode + '%'
		END
	ELSE IF @_Type = 'E'
		BEGIN
			INSERT INTO #tmp
			SELECT TOP 100
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmp
			SELECT TOP 100
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
		END
	ELSE IF @_Type = 'L'
		BEGIN
			--------Carton Sample--------
			INSERT INTO #tmp
			SELECT DISTINCT TOP 1000
				PL.LocationCode
				, ps.PalletCode
				, CartonBarcode = N'_Mẫu:' + ISNULL(ps.Barcode,'')
				, ProductBarcode = ''
				, EncryptedProductBarcode = ''
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.CartonBarcodes ps WITH (NOLOCK) 
				LEFT JOIN ProductionPlans pp WITH (NOLOCK) 
					ON ps.ProductLot = pp.ProductLot
				LEFT JOIN dbo.Products p WITH (NOLOCK) 
					ON p.ProductID = pp.ProductID
					AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot LIKE '%' + @_Barcode + '%'
				AND ps.[Status] = 'S'

			--------Product Sample--------
			INSERT INTO #tmp
			SELECT DISTINCT TOP 1000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = N'_Mẫu: ' + ISNULL(ps.Barcode,'')
				, EncryptedProductBarcode = N'_Mẫu: ' + ISNULL(ps.EncryptedBarcode,'')
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.ProductBarcodes ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot LIKE '%' + @_Barcode + '%'
				AND ps.[Status] = 'S'

			INSERT INTO #tmp
			SELECT DISTINCT
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = COUNT(DISTINCT ProductBarcode)
				, EncryptedProductBarcode = N'Trong Kho'
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot LIKE '%' + @_Barcode + '%'
			GROUP BY
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT DISTINCT
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = COUNT(DISTINCT ProductBarcode)
				, EncryptedProductBarcode = N'Đã giao'
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot LIKE '%' + @_Barcode + '%'
			GROUP BY
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot LIKE '%' + @_Barcode + '%'
		END


	SELECT * FROM #tmp --ORDER BY PalletCode, CartonBarcode, ProductLot, ProductDescription, ProductBarcode
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [proc_Pallets_SetLocation]
	@PalletCode VARCHAR(255)
	, @LocationCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_LocationCode VARCHAR(255) = NULLIF(@LocationCode, '')
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'SLC' + @_PalletCode + '->' + @_LocationCode

	UPDATE dbo.Pallets 
	SET 
		LocationCode = @_LocationCode
		, LocationPutDate = @_Date
		, Driver = @UserID
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE 
		PalletCode = @_PalletCode

	UPDATE dbo.ZoneLocations 
	SET 
		CurrentPalletCode = @_PalletCode
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE 
		LocationCode = @_LocationCode

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallet">' 
		+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode FOR XML PATH(''))
		+ '</BaseEntity>'
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'SLC', @_Method, @_Date, @_Date, @_AuditDescription
END

GO
