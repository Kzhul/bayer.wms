--SELECT DISTINCT L.Location, L.ProductLot, P.PalletCode, L.ItemCode, L.ItemName, L.Quantity FROM [dbo].[_ExcelLocation] L JOIN [dbo].[_ExcelPalletProductLot] P
--ON L.ProductLot = P.ProductLot

SELECT * 
INTO #A
FROM
(
	SELECT DISTINCT 
		L.Location, L.ProductLot, P.PalletCode, L.ItemCode, L.ItemName, L.Quantity 
		,ROW_NUMBER() OVER (PARTITION BY L.Location		ORDER BY L.Quantity DESC)  AS RN
	FROM 
		[dbo].[_ExcelLocation] L 
		JOIN [dbo].[_ExcelPalletProductLot] P
			ON L.ProductLot = P.ProductLot
	WHERE LEN(L.ItemCode) > 6
) AS A WHERE RN = 1


SELECT * FROM #A


UPDATE Pallets 
SET LocationCode = A.Location
FROM
	Pallets P
	LEFT JOIN #A AS A
		ON P.PalletCode = A.PalletCode


SELECT * FROM Pallets WHERE LocationCode IS NOT NULL