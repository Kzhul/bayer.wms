﻿clear all

#http://www.sqlservercentral.com/blogs/nycnet/2013/05/24/performing-an-insert-from-a-powershell-script/
# Open SQL connection (you have to change these variables)
$DBServer = "."
$DBName = "Bayer.WMS"
$sqlConnection = New-Object System.Data.SqlClient.SqlConnection
$sqlConnection.ConnectionString = "Server=$DBServer;Database=$DBName;user id=sa;pwd=123456aA@;"
$sqlConnection.Open()
 
# Quit if the SQL connection didn't open properly.
if ($sqlConnection.State -ne [Data.ConnectionState]::Open) {
    "Connection to DB is not open."
    Exit
}

#Process Folder
$folder = "H:\1.DEV\1.Bayer\01. Documents\03.WareHouse_Bayer\03_The dinh vi kho\"
$excel=new-object -com excel.application
Get-ChildItem "$folder" -Filter *.xlsx | 
Foreach-Object {
    #https://www.codykonior.com/2013/03/26/powershell-how-to-show-all-of-an-objects-properties-and-values/
    $FullPath = $_.FullName
    $fileName = $_.Name
    $wb=$excel.workbooks.open("$folder$fileName")
    Write-Host "----------------------------------------------------------------------------"
    for ($i=3; $i -le $wb.sheets.count; $i++)
    {
        $sheet = $wb.Sheets.Item($i);    
        $sheetName = $sheet.Name
        $rowMax = ($sheet.UsedRange.Rows).count
        Write-Host "---------------------------"
        Write-Host "Sheet" $i $sheet.Name $rowMax;

        $itemCode = $sheet.Cells.Item(8,"B").Text
        $itemName = $sheet.Cells.Item(11,"B").Text
        $packagingSize = $sheet.Cells.Item(11,"G").Text
        $unit = $sheet.Cells.Item(14,"G").Text

        Write-Host $itemCode " " $itemName " " $packagingSize " " $unit
    
        $rowName,$colName = 0,0;
        for ($r=20; $r -le $rowMax-1; $r++)
        {
            $balance = $sheet.Cells.Item($rowName+$r,"A").Text
            $quantity = $sheet.Cells.Item($rowName+$r,"B").Text        
            $productLot = $sheet.Cells.Item($rowName+$r,"C").Text
            $Expirydate = $sheet.Cells.Item($rowName+$r,"D").Text
            $location = $sheet.Cells.Item($rowName+$r,"E").Text
            $change = $sheet.Cells.Item($rowName+$r,"F").Text
            $remark = $sheet.Cells.Item($rowName+$r,"G").Text

            if (
                -not ([string]::IsNullOrEmpty($productLot)) -And -not ([string]::IsNullOrEmpty($quantity))
            )
            {
                Write-Host $balance " " $quantity " " $productLot " " $Expirydate " " $location " " $change " " $remark

                $sqlCommand = New-Object System.Data.SqlClient.SqlCommand
                $sqlCommand.Connection = $sqlConnection

                $sqlCommand.CommandText =   "SET NOCOUNT ON; " +
                                            "INSERT INTO [dbo].[_ExcelLocation]
                                                ([FullPath]
                                                ,[FileName]
                                                ,[SheetName]
                                                ,[ItemCode]
                                                ,[ItemName]
                                                ,[PackagingSize]
                                                ,[Unit]
                                                ,[Balance]
                                                ,[Quantity]
                                                ,[ProductLot]
                                                ,[Expirydate]
                                                ,[Location]
                                                ,[Change]
                                                ,[Remark]
                                                ,[CreatedDateTime])
                                            VALUES
                                                (@FullPath
			                                    ,@FileName
			                                    ,@SheetName
			                                    ,@ItemCode
			                                    ,@ItemName
			                                    ,@PackagingSize
			                                    ,@Unit
			                                    ,@Balance
			                                    ,@Quantity
			                                    ,@ProductLot
			                                    ,@Expirydate
			                                    ,@Location
			                                    ,@Change
			                                    ,@Remark
			                                    ,GETDATE()			
			                                    )" +
                                            "SELECT SCOPE_IDENTITY() as [InsertedID]; "

                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@FullPath",[Data.SQLDBType]::NVarChar, 500))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@FileName",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@SheetName",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@ItemCode",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@ItemName",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@PackagingSize",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@Unit",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@Balance",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@Quantity",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@ProductLot",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@Expirydate",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@Location",[Data.SQLDBType]::NVarChar, 50))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@Change",[Data.SQLDBType]::NVarChar, 500))) | Out-Null
                $sqlCommand.Parameters.Add((New-Object Data.SqlClient.SqlParameter("@Remark",[Data.SQLDBType]::NVarChar, 500))) | Out-Null

                $sqlCommand.Parameters[0].Value = $FullPath
                $sqlCommand.Parameters[1].Value = $FileName
                $sqlCommand.Parameters[2].Value = $SheetName
                $sqlCommand.Parameters[3].Value = $ItemCode
                $sqlCommand.Parameters[4].Value = $ItemName
                $sqlCommand.Parameters[5].Value = $PackagingSize
                $sqlCommand.Parameters[6].Value = $Unit
                $sqlCommand.Parameters[7].Value = $Balance
                $sqlCommand.Parameters[8].Value = $Quantity
                $sqlCommand.Parameters[9].Value = $ProductLot
                $sqlCommand.Parameters[10].Value = $Expirydate
                $sqlCommand.Parameters[11].Value = $Location
                $sqlCommand.Parameters[12].Value = $Change
                $sqlCommand.Parameters[13].Value = $Remark
                                
                # Run the query and get the scope ID back into $InsertedID
                $InsertedID = $sqlCommand.ExecuteScalar()
                # Write to the console.
                "Inserted row ID $InsertedID"
                #sleep -Seconds 2
            }
        }  
    }
    $wb.Close()
}
$excel.Quit()
[System.Runtime.Interopservices.Marshal]::ReleaseComObject($excel)

# Close the connection.
if ($sqlConnection.State -eq [Data.ConnectionState]::Open) {
    $sqlConnection.Close()
}