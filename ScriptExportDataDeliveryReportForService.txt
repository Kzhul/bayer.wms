SELECT DISTINCT
	DH.ProductID
	, ProductBarcode
	, EncryptedProductBarcode
	, DH.CartonBarcode
	, DeliveryTicketCode
	, DeliveryCode = DeliveryTicketCode
	, CompanyCode = C.CompanyCode
	, CompanyName = C.CompanyName
	, Province = C.ProvinceShipTo
	, ActualDeliveryDate
	, DeliveryDate
	, PalletCode
	, ProductLot = DH.ProductLot
	, ProductCode
	, PRoductDescription = PD.Description
	, PackingType
	, CartonPacking = PC.Quantity
	, PalletPacking = PP.Quantity
	, DeliveryMan = ISNULL(DU.LastName + ' ', '') + ISNULL(DU.FirstName, '')	 
	, DeliveryClickDate = DH.CreatedDateTime
	, PackagingMan = ISNULL(DL.LastName + ' ', '') + ISNULL(DL.FirstName, '')	 
	, PackagingDate = PL.CreatedDateTime
FROM 
	DeliveryHistories DH WITH (NOLOCK)
	LEFT JOIN Companies C WITH (NOLOCK)
		ON DH.CompanyCode = C.CompanyCode
	LEFT JOIN Products PD WITH (NOLOCK)
		ON DH.ProductID = PD.ProductID
	LEFT JOIN ProductPackings PP WITH (NOLOCK)
		ON PD.ProductID = PP.ProductID
		AND PP.Type = 'P'
	LEFT JOIN ProductPackings PC WITH (NOLOCK)
		ON PD.ProductID = PC.ProductID
		AND PC.Type = 'P'
	LEFT JOIN PackagingLogs PL WITH (NOLOCK)
		ON DH.ProductBarcode = PL.Barcode
	LEFT JOIN Users DU WITH (NOLOCK)
		ON DH.CreatedBy = DU.UserID
	LEFT JOIN Users DL WITH (NOLOCK)
		ON PL.CreatedBy = DL.UserID