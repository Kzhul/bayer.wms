﻿namespace UpdateApp
{
    partial class UpdateApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.txtCK3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCheckConnection = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCopyFTP = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Thoát";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(1, 25);
            this.txtServer.Multiline = true;
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(236, 60);
            this.txtServer.TabIndex = 0;
            // 
            // txtCK3
            // 
            this.txtCK3.Location = new System.Drawing.Point(1, 111);
            this.txtCK3.Multiline = true;
            this.txtCK3.Name = "txtCK3";
            this.txtCK3.Size = new System.Drawing.Size(236, 32);
            this.txtCK3.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.Text = "Server";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(4, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.Text = "CK3";
            // 
            // btnCheckConnection
            // 
            this.btnCheckConnection.Location = new System.Drawing.Point(0, 149);
            this.btnCheckConnection.Name = "btnCheckConnection";
            this.btnCheckConnection.Size = new System.Drawing.Size(237, 28);
            this.btnCheckConnection.TabIndex = 5;
            this.btnCheckConnection.Text = "Kiểm tra kết nối";
            this.btnCheckConnection.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(0, 183);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(237, 28);
            this.button2.TabIndex = 6;
            this.button2.Text = "Copy";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(0, 214);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(237, 54);
            // 
            // btnCopyFTP
            // 
            this.btnCopyFTP.Location = new System.Drawing.Point(0, 214);
            this.btnCopyFTP.Name = "btnCopyFTP";
            this.btnCopyFTP.Size = new System.Drawing.Size(237, 28);
            this.btnCopyFTP.TabIndex = 9;
            this.btnCopyFTP.Text = "Copy FTP";
            this.btnCopyFTP.Click += new System.EventHandler(this.btnCopyFTP_Click);
            // 
            // UpdateApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.btnCopyFTP);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnCheckConnection);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCK3);
            this.Controls.Add(this.txtServer);
            this.Menu = this.mainMenu1;
            this.Name = "UpdateApp";
            this.Text = "Update CK3";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.TextBox txtCK3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCheckConnection;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.Button btnCopyFTP;
    }
}

