﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.ComponentModel;
namespace UpdateApp
{
    public class NetworkConnection : IDisposable
    {
        string _networkName;
        int _result;
        IntPtr _handle = IntPtr.Zero;

        public NetworkConnection(string networkName, NetworkCredential credentials)
        {
            _networkName = networkName;

            var netResource = new NetResource()
            {
                dwScope = (uint)ResourceScope.GlobalNetwork,
                dwType = (uint)ResourceType.Disk,
                dwDisplayType = (uint)ResourceDisplayType.Share,
                dwUsage = (uint)ResourceUsage.Connectable,
                lpRemoteName = networkName,
            };

            var userName = string.IsNullOrEmpty(credentials.Domain)
                ? credentials.UserName
                : string.Format(@"{0}\{1}", credentials.Domain, credentials.UserName);

            _result = WNetAddConnection3(
                _handle,
                ref netResource,
                credentials.Password,
                userName,
                0);

            if (_result != 0)
            {
                throw new Win32Exception(_result, "Error connecting to remote share");
            }
        }

        ~NetworkConnection()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            WNetCancelConnection2(_networkName, 0, true);
        }

        [DllImport("coredll.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern int WNetAddConnection3(IntPtr hWndOwner, ref NetResource lpNetResource,
            string lpPassword, string lpUserName, uint dwFlags);

        [DllImport("coredll.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern int WNetCancelConnection2(string name, uint dwFlags, bool force);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct NetResource
    {
        public uint dwScope;
        public uint dwType;
        public uint dwDisplayType;
        public uint dwUsage;
        [MarshalAs(UnmanagedType.LPWStr, SizeConst = 64)]
        public string lpLocalName;
        [MarshalAs(UnmanagedType.LPWStr, SizeConst = 64)]
        public string lpRemoteName;
        [MarshalAs(UnmanagedType.LPWStr, SizeConst = 64)]
        public string lpComment;
        [MarshalAs(UnmanagedType.LPWStr, SizeConst = 64)]
        public string lpProvider;
    }

    internal enum ResourceScope : uint
    {
        Connected = 1,
        GlobalNetwork,
        Remembered,
        Recent,
        Context
    };

    internal enum ResourceType : uint
    {
        Any = 0x0,
        Disk = 0x1,
        Print = 0x2,
        Reserved = 0x8,
    }

    internal enum ResourceDisplayType : uint
    {
        Generic = 0x0,
        Domain = 0x01,
        Server = 0x02,
        Share = 0x03,
        File = 0x04,
        Group = 0x05,
        Network = 0x06,
        Root = 0x07,
        Shareadmin = 0x08,
        Directory = 0x09,
        Tree = 0x0a,
        Ndscontainer = 0x0b
    }

    internal enum ResourceUsage : uint
    {
        Connectable = 0x00000001,
        Container = 0x00000002,
        NoLocalDevice = 0x00000004,
        Sibling = 0x00000008,
        Attached = 0x00000010,
        All = (Connectable | Container | Attached)
    }
}