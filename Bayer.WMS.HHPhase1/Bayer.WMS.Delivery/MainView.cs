﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Bayer.WMS.Delivery.Models;
using Intermec.DataCollection;

namespace Bayer.WMS.Delivery
{
    public partial class MainView : Form
    {
        #region Param
        protected BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        protected DataTable _dt;
        private int _wrongPINTimes = 0;
        #endregion

        #region MainView
        public MainView()
        {
            InitializeComponent();
            Utility.AppPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().GetName().CodeBase);
        }

        public void MainView_Closing(object sender, CancelEventArgs e)
        {
            DisposeBarcodeReader();
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            LoadConfig();
            LoadCartonLabelTemplate();
            LoadCartonLabelForMultiProductTemplate();
            LoadPalletLabelTemplate();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            _scannedValue = new List<string>();
            menuFunction.Enabled = false;

            if (Utility.debugMode)
            {
                ProcessEmployeeBarcode("NV_vnvut");
            }
        }
        #endregion

        #region Menu
        private void menuPrepare_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.PrepareView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuDelivery_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.DeliveryView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuQRCode_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.BarcodeTrackingView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuSetPosition_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ForkLiftView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuPrepareNonDO_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    DisposeBarcodeReader();

            //    using (var view = new Bayer.WMS.Handheld.Views.PrepareView_NonDO())
            //    {
            //        view.ShowDialog();
            //    }
            //}
            //finally
            //{
            //    ReInitBarcodeReader();
            //}
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region Reader
        protected virtual void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    ProcessEmployeeBarcode(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {

            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            try
            {
                txtBarcode.Visible = false;
                menuFunction.Enabled = false;
                txtBarcode.Text = string.Empty;
                //Utility.WriteLog(barcode);
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty).Remove(0, 3);
                User _user = User.Load(barcode);
                Utility.UserID = _user.UserID;
                Utility.UserPIN = _user.PIN;
                Utility.CurrentUser = _user;
                lblStepHints.Text = "Xin chào: " + String.Format("{0} {1} . Vui lòng nhập mã PIN", _user.LastName, _user.FirstName);
                txtBarcode.Visible = true;
                txtBarcode.Focus();
                //menuFunction.Enabled = true;

                _wrongPINTimes = 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public virtual void DisposeBarcodeReader()
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public virtual void ReInitBarcodeReader()
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public virtual bool IsComplete()
        {
            return _dt.Rows.Count == _dt.Select("IsComplete = 1").Length;
        }
        #endregion

        #region Load Config
        public void ReadConnectionString(string config)
        {
            string[] connStrComponent = config.Split(';');
            string server = connStrComponent.FirstOrDefault(p => p.StartsWith("Server")).Replace("Server=", String.Empty).Trim();
            string database = connStrComponent.FirstOrDefault(p => p.StartsWith("Database")).Replace("Database=", String.Empty).Trim();
            string userid = connStrComponent.FirstOrDefault(p => p.StartsWith("Uid")).Replace("Uid=", String.Empty).Trim();
            string pwd = connStrComponent.FirstOrDefault(p => p.StartsWith("Pwd")).Replace("Pwd=", String.Empty).Trim();
            string connectionTimeout = connStrComponent.FirstOrDefault(p => p.StartsWith("Connection Timeout")).Replace("Connection Timeout=", String.Empty).Trim();

            //string connStr = String.Format("Server={0};Database={1};Uid={2};pwd={3};",
            //    server, database, AESDecrypt(userid), AESDecrypt(pwd));
            Utility.ConnStr = config;

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                conn.Close();
            }
        }

        public void ReadCOMConfig(string config)
        {
            string[] comConfigComponent = config.Split(';');
            Utility.COM_PortName = comConfigComponent.FirstOrDefault(p => p.StartsWith("PortName")).Replace("PortName=", String.Empty).Trim();
            Utility.COM_BaudRate = int.Parse(comConfigComponent.FirstOrDefault(p => p.StartsWith("BaudRate")).Replace("BaudRate=", String.Empty).Trim());
            Utility.COM_Parity = comConfigComponent.FirstOrDefault(p => p.StartsWith("Parity")).Replace("Parity=", String.Empty).Trim();
            Utility.COM_DataBits = int.Parse(comConfigComponent.FirstOrDefault(p => p.StartsWith("DataBits")).Replace("DataBits=", String.Empty).Trim());
            Utility.COM_StopBits = comConfigComponent.FirstOrDefault(p => p.StartsWith("StopBits")).Replace("StopBits=", String.Empty).Trim();
        }

        public void LoadConfig()
        {
            string configPath = Path.Combine(Utility.AppPath, "AppConfig.txt");

            try
            {
                //Load app setting from config file
                var config = new Dictionary<string, string>();
                using (var sr = new StreamReader(configPath))
                {
                    int i = 0;
                    while (!sr.EndOfStream)
                    {
                        i++;
                        try
                        {
                            string[] tmp = sr.ReadLine().Split(':');
                            if (tmp.Length == 2)
                                config.Add(tmp[0].Trim(), tmp[1].Trim());
                        }
                        catch
                        {
                            //Do nothing
                        }
                    }
                    if (i >= 3)
                    {
                        Utility.debugMode = true;
                    }
                }

                ReadConnectionString(config["ConnectionString"]);
                ReadCOMConfig(config["COMConfig"]);

                //Load app settting from database
                List<SqlParameter> listParam = new List<SqlParameter>();
                DataTable dt = Utility.LoadDataFromStore("proc_AppSettings_GetAll", listParam);
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["SettingID"].ToString() == "MultiProductPerPallet")
                    {
                        if (dr["SettingValue"].ToString() == "1")
                        {
                            Utility.SettingMultiProductPerPallet = 1;
                        }
                        else
                        {
                            Utility.SettingMultiProductPerPallet = 0;
                        }
                    }
                }
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
            catch (SqlException)
            {
                throw new Exception("Không thể kết nối đến hệ thống.");
            }
        }

        public void LoadCartonLabelTemplate()
        {
            string templatePath = Path.Combine(Utility.AppPath, @"Template\CartonLabelTemplate.txt");

            try
            {
                using (var sr = new StreamReader(templatePath))
                {
                    Utility.CartonLabelTemplate = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
        }

        public void LoadCartonLabelForMultiProductTemplate()
        {
            string templatePath = Path.Combine(Utility.AppPath, @"Template\CartonLabelForMultiProductTemplate.txt");

            try
            {
                using (var sr = new StreamReader(templatePath))
                {
                    Utility.CartonLabelForMultiProductTemplate = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
        }

        public void LoadPalletLabelTemplate()
        {
            string templatePath = Path.Combine(Utility.AppPath, @"Template\PalletLabelTemplate.txt");

            try
            {
                using (var sr = new StreamReader(templatePath))
                {
                    Utility.PalletLabelTemplate = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
        }
        #endregion

        private void menuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ReceivePalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ReceiveMaterialPalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.WarehouseManagerView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuItem5_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveProductPallet())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuItem6_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveMaterialPalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuSplit_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.SplitView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void mnuUpdateSoftware_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Bạn có chắc muốn cập nhật phần mềm ?", "Cập nhật", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    ProcessStartInfo updateApp = new ProcessStartInfo(Path.Combine(Utility.AppPath, "UpdateApp.exe"), string.Empty);
                    System.Diagnostics.Process.Start(updateApp);
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            if (txtBarcode.Text.Length == 4)
            {
                if (txtBarcode.Text == Utility.UserPIN)
                {
                    lblStepHints.Text = "Xin chào: " + String.Format("{0} {1} .", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
                    txtBarcode.Visible = false;
                    menuFunction.Enabled = true;
                }
                else
                {
                    _wrongPINTimes = _wrongPINTimes + 1;
                    lblStepHints.Text = "Mã PIN không đúng. Sai lần: " + _wrongPINTimes.ToString();
                    Utility.RecordAuditTrail("Users", "PIN", "LoginCK3", Utility.CurrentUser.Username + "_wrongPINTimes:" + _wrongPINTimes.ToString());

                    if (_wrongPINTimes >= 5)
                    {
                        lblStepHints.Text = "Tài khoản của bạn " + String.Format("{0} {1} .", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName) + " đã bị khóa.";
                        User.LockUser(Utility.CurrentUser.Username);
                    }
                }
            }
        }

        private void menuItem1_Click_1(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.PINView())
                {
                    view.StrAction = "ChangePIN";
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuItem3_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}