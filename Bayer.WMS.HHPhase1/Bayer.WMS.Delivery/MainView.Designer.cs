﻿namespace Bayer.WMS.Delivery
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuFunction = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuSplit = new System.Windows.Forms.MenuItem();
            this.menuPrepare = new System.Windows.Forms.MenuItem();
            this.menuDelivery = new System.Windows.Forms.MenuItem();
            this.menuQRCode = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuSetPosition = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.mnuUpdateSoftware = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuFunction);
            this.mainMenu1.MenuItems.Add(this.menuItem3);
            // 
            // menuFunction
            // 
            this.menuFunction.MenuItems.Add(this.menuItem2);
            this.menuFunction.MenuItems.Add(this.menuSplit);
            this.menuFunction.MenuItems.Add(this.menuPrepare);
            this.menuFunction.MenuItems.Add(this.menuDelivery);
            this.menuFunction.MenuItems.Add(this.menuQRCode);
            this.menuFunction.MenuItems.Add(this.menuItem4);
            this.menuFunction.MenuItems.Add(this.menuSetPosition);
            this.menuFunction.MenuItems.Add(this.menuItem5);
            this.menuFunction.MenuItems.Add(this.menuItem6);
            this.menuFunction.MenuItems.Add(this.mnuUpdateSoftware);
            this.menuFunction.MenuItems.Add(this.menuItem1);
            this.menuFunction.Text = "F1.Chức năng";
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "1.Nhận Hàng";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuSplit
            // 
            this.menuSplit.Text = "2.Chia hàng";
            this.menuSplit.Click += new System.EventHandler(this.menuSplit_Click);
            // 
            // menuPrepare
            // 
            this.menuPrepare.Text = "3.Soạn hàng";
            this.menuPrepare.Click += new System.EventHandler(this.menuPrepare_Click);
            // 
            // menuDelivery
            // 
            this.menuDelivery.Text = "4.Giao hàng";
            this.menuDelivery.Click += new System.EventHandler(this.menuDelivery_Click);
            // 
            // menuQRCode
            // 
            this.menuQRCode.Text = "5.Truy vết mã QR";
            this.menuQRCode.Click += new System.EventHandler(this.menuQRCode_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Text = "6.Thủ kho";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuSetPosition
            // 
            this.menuSetPosition.Text = "7.Xe nâng";
            this.menuSetPosition.Click += new System.EventHandler(this.menuSetPosition_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Text = "8.San hàng thành phẩm";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Text = "9.San Nguyên Liệu";
            this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
            // 
            // mnuUpdateSoftware
            // 
            this.mnuUpdateSoftware.Text = "10.Cập Nhật Phần Mềm";
            this.mnuUpdateSoftware.Click += new System.EventHandler(this.mnuUpdateSoftware_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "11.Đổi mã PIN";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(60, 74);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 120);
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã nhân viên";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(60, 200);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.PasswordChar = '*';
            this.txtBarcode.Size = new System.Drawing.Size(120, 21);
            this.txtBarcode.TabIndex = 12;
            this.txtBarcode.Visible = false;
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            // 
            // menuItem3
            // 
            this.menuItem3.Text = "Thoát";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click_1);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.pictureBox1);
            this.KeyPreview = true;
            this.Menu = this.mainMenu1;
            this.Name = "MainView";
            this.Text = "Bayer.WMS";
            this.Load += new System.EventHandler(this.MainView_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem menuFunction;
        private System.Windows.Forms.MenuItem menuPrepare;
        private System.Windows.Forms.MenuItem menuDelivery;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuItem menuQRCode;
        private System.Windows.Forms.MenuItem menuSetPosition;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem menuSplit;
        private System.Windows.Forms.MenuItem mnuUpdateSoftware;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem3;
    }
}