﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Bayer.WMS.Delivery.Models
{
    public class Delivery
    {
        public static void Insert(string barcode, string type, Company company, User user)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open(); 
                var tran = conn.BeginTransaction();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryHistories_Insert_Manual", conn, tran))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@Barcode", Value = barcode, SqlDbType = SqlDbType.VarChar, Size = 255 });
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@Type", Value = type, SqlDbType = SqlDbType.Char, Size = 1 });
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@CompanyCode", Value = company.CompanyCode, SqlDbType = SqlDbType.VarChar, Size = 255 });
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@CompanyName", Value = company.CompanyName, SqlDbType = SqlDbType.NVarChar, Size = 255 });
                        cmd.Parameters.Add(new SqlParameter { ParameterName = "@UserID", Value = user.UserID, SqlDbType = SqlDbType.Int });
                        
                        cmd.ExecuteNonQuery();
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }
}
