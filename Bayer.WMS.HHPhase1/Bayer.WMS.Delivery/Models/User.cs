﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Bayer.WMS.Delivery.Models
{
    public class User
    {
        public int UserID { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PIN { get; set; }

        public static User Load(string userName)
        {
            User user;
            DataTable dt = new DataTable();
            //Utility.WriteLog(Utility.ConnStr);
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open(); 

                try
                {
                    string query = "SELECT UserID, Username, FirstName, LastName,PIN FROM dbo.Users WITH (NOLOCK) WHERE Username = '{0}' AND IsDeleted = 0 AND Status = 'A'";
                    //Utility.WriteLog(String.Format(query, userName));
                    using (var cmd = new SqlCommand(String.Format(query, userName), conn))
                    {
                        cmd.CommandType = CommandType.Text;

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Không tìm thấy nhân viên");

            user = new User
            {
                UserID = int.Parse(dt.Rows[0]["UserID"].ToString()),
                Username = dt.Rows[0]["Username"].ToString(),
                FirstName = dt.Rows[0]["FirstName"].ToString(),
                LastName = dt.Rows[0]["LastName"].ToString(),
                PIN = dt.Rows[0]["PIN"].ToString()
            };

            return user;
        }

        public static bool CheckAccessCode(string barcode)
        {
            bool value = false;
            var dt = new DataTable();

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_PassCode_Get", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }

                        if (dt.Rows.Count == 0)
                            throw new Exception("Mã truy cập chưa được tạo cho ngày hôm nay");

                        if (barcode == dt.Rows[0]["Code"].ToString())
                        {
                            value = true;
                        }
                        else
                        {
                            throw new Exception("Mã truy cập không đúng");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return value;
        }

        public static bool LockUser(string username)
        {
            bool value = false;
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_Users_Lock", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Username", username));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", "LockUser"));
                        cmd.ExecuteNonQuery();
                        value = true;
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return value;
        }
    }
}