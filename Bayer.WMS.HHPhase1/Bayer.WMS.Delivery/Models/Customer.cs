﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Bayer.WMS.Delivery.Models
{
    public class Company
    {
        public string CompanyCode { get; set; }

        public string CompanyName { get; set; }

        public static Company Load(string companyCode)
        {
            Company company;

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open(); 

                try
                {
                    string query = "SELECT TOP 1 CompanyCode, CompanyName FROM dbo.Companies WHERE CompanyCode = '{0}'";
                    query = String.Format(query, companyCode);
                    //Utility.WriteLog(query);
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        cmd.CommandType = CommandType.Text;

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            var dt = new DataTable();
                            adapt.Fill(dt);

                            if (dt.Rows.Count == 0)
                                throw new Exception("Không tìm thấy khách hàng");

                            company = new Company
                            {
                                CompanyCode = dt.Rows[0]["CompanyCode"].ToString(),
                                CompanyName = dt.Rows[0]["CompanyName"].ToString()
                            };
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return company;
        }

        public static Company SelectCompany(string doImportCode, string companyCode, string deliveryCode)
        {
            Company company;
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryTicket_SelectCompany", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                        cmd.Parameters.Add(new SqlParameter("@DeliveryCode", deliveryCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }

                        if (dt.Rows.Count == 0)
                            throw new Exception("Không tìm thấy khách hàng");

                        if (dt.Rows[0]["Status"].ToString() != "P")
                            throw new Exception("Phiếu này chưa in, Vui lòng in phiếu trước khi giao hàng.");

                        company = new Company
                        {
                            CompanyCode = dt.Rows[0]["CompanyCode"].ToString(),
                            CompanyName = dt.Rows[0]["CompanyName"].ToString()
                        };
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }

            return company;
        }

        public static DataTable VerifyPreparedQuantity(string doImportCode,string companyCode, string barcode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryOrderDetails_VerifyPreparedQuantity", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                        cmd.Parameters.Add(new SqlParameter("@BarCode", barcode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable VerifyPreparedQuantity(string palletCode, string barcode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryOrderDetails_VerifyPreparedQuantityByPallet", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@BarCode", barcode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable VerifyExportedQuantity(string doImportCode, string companyCode, string barcode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryOrderDetails_VerifyExportedQuantity", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                        cmd.Parameters.Add(new SqlParameter("@BarCode", barcode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable Barcode_Test()
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Barcode_Test", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable VerifyReceivedQuantity(string doImportCode, string companyCode, string palletCode, string barcode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryOrderDetails_VerifyReceivedQuantity", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@BarCode", barcode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable VerifyProductReturnQuantity(string doImportCode, string barcode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_ProductReturn_VerifyPreparedQuantity", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@BarCode", barcode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }
    }
}
