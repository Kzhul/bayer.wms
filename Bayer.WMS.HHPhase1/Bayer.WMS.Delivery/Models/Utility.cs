﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Intermec.DataCollection;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
using System.Media;
using System.Security.Cryptography;
using System.Threading;
using Bayer.WMS.Handheld;
using System.Windows.Forms;

namespace Bayer.WMS.Delivery.Models
{
    public static class Utility
    {
        #region Param & Constant
        public static bool debugMode = false;

        public const string PalletStatusNew = "N";
        public const string PalletStatusExported = "E";
        public const string PalletStatusReceived = "R";
        public const string PalletStatusSplitted = "S";
        public const string PalletStatusPrepared = "P";
        public const string PalletStatusExportedReturn = "X";

        public const int ExportView = -1;
        public const int ReceiveView = -2;
        public const int SplitView = -3;
        public const int PrepareView = -4;
        public const int DeliveryView = -5;
        public const int ExportReturnView = -6;
        public const int ReceiveReturnView = -7;

        public static string ConnStr;
        public static int ConnTimeOut = 30;
        public static int SettingMultiProductPerPallet = 0;
        public static string AppPath;
        public static string CartonLabelTemplate;
        public static string CartonLabelForMultiProductTemplate;
        public static string PalletLabelTemplate;
        public static string PalletLabelTemplateTest;
        public static string COM_PortName;
        public static int COM_BaudRate;
        public static string COM_Parity;
        public static int COM_DataBits;
        public static string COM_StopBits;
        public static int SitemapID;
        public static int UserID;
        public static string UserPIN;
        public static User CurrentUser;
        public static byte[] bytes = Encoding.ASCII.GetBytes("BayerWMS");
        public static Regex ReceivingMaterialDocumentRegex = new Regex(@"^SR\d{8}$", RegexOptions.IgnoreCase);
        public static Regex ExportRegex = new Regex(@"^XH\d{8}$", RegexOptions.IgnoreCase);
        public static Regex ExportReturnRegex = new Regex(@"^RD\d{8}$", RegexOptions.IgnoreCase);
        public static Regex SplitRegex = new Regex(@"^CH\d{8}$", RegexOptions.IgnoreCase);
        public static Regex PrepareNoteRegex = new Regex(@"^SH\d{8}$", RegexOptions.IgnoreCase);
        public static Regex DeliveryRegex = new Regex(@"^GH\d{8}$", RegexOptions.IgnoreCase);
        public static Regex PalletBarcodeRegex = new Regex(@"^P[A-Z][01]\d\d{2}\d{5}$", RegexOptions.IgnoreCase);
        public static Regex CartonBarcodeRegex = new Regex(@"^\d{6}\w\d{2}C\d{3}$", RegexOptions.IgnoreCase);
        public static Regex CartonBarcode2Regex = new Regex(@"^SH\d{8}C\d{3}$", RegexOptions.IgnoreCase);
        public static Regex CartonBarcode3Regex = new Regex(@"\d{6}\w\d{2}(C|c)\w{3}$", RegexOptions.IgnoreCase);
        public static Regex CartonBarcodeTempRegex = new Regex(@"^DO\d{8}C\d{3}$", RegexOptions.IgnoreCase);
        public static Regex ProductCartonRegex = new Regex(@"^\d{6}\w\d{2}\d{5}", RegexOptions.IgnoreCase);
        public static Regex TruckNoRegex = new Regex(@"^SX_.*$", RegexOptions.IgnoreCase);
        public static Regex EmployeeRegex = new Regex(@"^NV_", RegexOptions.IgnoreCase);
        public static Regex CustomerRegex = new Regex(@"^KH_", RegexOptions.IgnoreCase);
        public static Regex ProductRegex = new Regex(@"^\d{6}\w\d{2}\d{5}", RegexOptions.IgnoreCase);
        public static Regex Product2Regex = new Regex(@"\d{10}", RegexOptions.IgnoreCase);
        public static Regex Product3Regex = new Regex(@"\d{26}", RegexOptions.IgnoreCase);
        public static Regex ProductLotRegex = new Regex(@"^\d{6}\w\d{2}$", RegexOptions.IgnoreCase);
        public static Regex ProductLot4Regex = new Regex(@"^\d{6}\w\d{2}-\d{3}$", RegexOptions.IgnoreCase);
        public static Regex ProductLot2Regex = new Regex(@"^\d*\d{8}\w\d{6}$", RegexOptions.IgnoreCase);
        public static Regex ProductLot3Regex = new Regex(@"^\d*\d{8}\w\d{12}$", RegexOptions.IgnoreCase);
        public static Regex LocationCodeRegex = new Regex(@"^\w{3}_\w{2}_\w{2}\d-\d\d$", RegexOptions.IgnoreCase);
        public static Regex AccessCodeRegex = new Regex(@"^\d{4}$", RegexOptions.IgnoreCase);

        public static string PrepareLocationCode = "SH1_SH_SH1-01";

        #endregion

        #region WriteLog
        public static void WriteLog(Exception ex)
        {
            //Write log
            using (var sw = new StreamWriter(Path.Combine(Utility.AppPath, "Error.txt"), true))
            {
                sw.WriteLine(String.Format("{0:yyyy.MM.dd HH:mm:ss}", DateTime.Now));
                sw.WriteLine("================Exception===================");
                sw.WriteLine(ex.Message);
                sw.WriteLine(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    sw.WriteLine("================Exception.InnerException===================");
                    sw.WriteLine(ex.InnerException.Message);
                    sw.WriteLine(ex.InnerException.StackTrace);

                    if (ex.InnerException.InnerException != null)
                    {
                        sw.WriteLine("================Exception.InnerException.InnerException===================");
                        sw.WriteLine(ex.InnerException.InnerException.Message);
                        sw.WriteLine(ex.InnerException.InnerException.StackTrace);

                        if (ex.InnerException.InnerException.InnerException != null)
                        {
                            sw.WriteLine("================InnerException.InnerException.InnerException===================");
                            sw.WriteLine(ex.InnerException.InnerException.InnerException.Message);
                            sw.WriteLine(ex.InnerException.InnerException.InnerException.StackTrace);
                        }
                    }
                }
                sw.WriteLine();
                sw.WriteLine();
            }
        }

        public static void WriteLog(string ex)
        {
            //Write log
            using (var sw = new StreamWriter(Path.Combine(Utility.AppPath, "Log.txt"), true))
            {
                sw.WriteLine(String.Format("{0:yyyy.MM.dd HH:mm:ss}", DateTime.Now) + ": " + ex);
                sw.WriteLine();
            }
        }

        public static void WriteLog(string ex, string filename)
        {
            //Write log
            using (var sw = new StreamWriter(Path.Combine(Utility.AppPath, filename + ".txt"), true))
            {
                sw.WriteLine(String.Format("{0:yyyy.MM.dd HH:mm:ss}", DateTime.Now) + ": " + ex);
                sw.WriteLine();
            }
        }

        public static void WriteLogToDB(Exception ex)
        {
            if (ex == null)
                return;

            string message = ex.Message;
            string stackTrace = ex.StackTrace;
            string innerException_Message = string.Empty;
            string innerException_StackTrace = string.Empty;

            if (ex.InnerException != null)
            {
                innerException_Message = ex.InnerException.Message;
                innerException_StackTrace = ex.InnerException.StackTrace;
            }
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_WriteLog", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@Message", message));
                        cmd.Parameters.Add(new SqlParameter("@StackTrace", stackTrace));
                        cmd.Parameters.Add(new SqlParameter("@InnerException_Message", innerException_Message));
                        cmd.Parameters.Add(new SqlParameter("@InnerException_StackTrace", innerException_StackTrace));
                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }
        }

        public static void WriteLogWithSound(Exception ex)
        {
            Utility.PlayErrorSound();
            Utility.WriteLog(ex);
            MessageBox.Show(ex.Message, "Lỗi");
        }
        #endregion

        public static void PlayErrorSound()
        {
            string path = Path.Combine(AppPath, @"Resources\Error.wav");
            var player = new SoundPlayer(path);
            player.Play();
            System.Threading.Thread.Sleep(2000);
            player.Stop();
        }

        public static void PlayLocationLevelSound(string strLevel)
        {
            string path = Path.Combine(AppPath, @"Resources\" + strLevel + ".wav");
            var player = new SoundPlayer(path);
            player.Play();
            System.Threading.Thread.Sleep(1000);
            player.Play();
            //System.Threading.Thread.Sleep(1000);
            //player.Play();
            System.Threading.Thread.Sleep(1000);
            player.Stop();
            player.Dispose();
        }

        public static string ProcessProductBarcode(string barcode)
        {
            barcode = Utility.AESDecrypt(barcode);
            return barcode;
        }

        public static string GenCartonWarehouseBarcode()
        {
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_CartonWarehouseBarcodes_Gen", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));

                        var dt = new DataTable();

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }

                        if (dt.Rows.Count == 0)
                            throw new Exception("Lỗi khi gen mã thùng");

                        return dt.Rows[0]["LastNumber"].ToString();
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }
        }

        public static void UpdatePalletStatus_Packaging2(string palletCode, string cartonBarcode, string productBarcode)
        {
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Update_WHPackaging2", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@CartonBarcode", cartonBarcode));
                        cmd.Parameters.Add(new SqlParameter("@ProductBarcode", productBarcode));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", "UpdatePalletStatus_Packaging2"));

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }
        }

        public static void UpdatePalletStatus_Packaging3(string palletCode, string cartonBarcodeOLD, string cartonBarcodeNEW)
        {
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Update_WHPackaging3", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@CartonBarcodeOld", cartonBarcodeOLD));
                        cmd.Parameters.Add(new SqlParameter("@CartonBarcodeNew", cartonBarcodeNEW));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", "UpdatePalletStatus_Packaging2"));

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }
        }

        public static void UpdatePalletStatus_Packaging(string palletCode, string cartonBarcode, string productBarcodeList, string method, SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_PalletStatuses_Update_WHPackaging", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                cmd.Parameters.Add(new SqlParameter("@CartonBarcode", cartonBarcode));
                cmd.Parameters.Add(new SqlParameter("@ProductBarcodeList", productBarcodeList));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        #region Encrypt & Decrypt
        public static string AESEncrypt(string planString)
        {
            if (String.IsNullOrEmpty(planString))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);

            var writer = new StreamWriter(cryptoStream);
            writer.Write(planString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();

            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        public static string AESDecrypt(string cipherText)
        {
            if (String.IsNullOrEmpty(cipherText))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream(Convert.FromBase64String(cipherText));
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
            var reader = new StreamReader(cryptoStream);

            return reader.ReadToEnd();
            //return cipherText;
        }

        public static string MD5Encrypt(string planString)
        {
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] hashedBytes;
            UTF8Encoding encoder = new UTF8Encoding();
            hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(planString));

            string result = String.Empty;
            foreach (Byte b in hashedBytes)
                result += b.ToString("x2");
            return result;
        }
        #endregion

        #region BarcodeReader
        public static string GetProductLotFromString(string barcode)
        {
            string _productLot = string.Empty;
            try
            {
                Match match = Regex.Match(barcode, @"\d{6}\w\d{2}");
                if (match.Success)
                {
                    _productLot = match.Value;
                }
            }
            catch (Exception ex)
            {
                //Write Log
            }
            return _productLot;
        }

        public static BarcodeReader InitBarcodeReader(BarcodeReadEventHandler handler)
        {
            try
            {
                BarcodeReader barcodeReader = new BarcodeReader();
                barcodeReader.ThreadedRead(true);
                barcodeReader.BarcodeRead += handler;

                return barcodeReader;
            }
            catch (Exception ex)
            {
                //Write Log
            }
            return null;
        }

        public static BarcodeReader ReInitBarcodeReader()
        {
            try
            {
                BarcodeReader barcodeReader = new BarcodeReader();
                barcodeReader.ThreadedRead(true);

                return barcodeReader;
            }
            catch (Exception ex)
            {
                //Write Log
            }
            return null;
        }
        #endregion

        #region AuditTrails
        public static void RecordAuditTrail(string data, string action, MethodInfo method, string description)
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    using (var conn = new SqlConnection(Utility.ConnStr))
                    {
                        conn.Open();

                        try
                        {
                            using (var cmd = new SqlCommand("proc_AuditTrails_Insert", conn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = Utility.UserID });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@SitemapID", SqlDbType = SqlDbType.Int, Value = Utility.SitemapID });
                                //cmd.Parameters.Add(new SqlParameter { ParameterName = "@Data", SqlDbType = SqlDbType.Xml, Value = data });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@Action", SqlDbType = SqlDbType.VarChar, Value = action });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@Method", SqlDbType = SqlDbType.VarChar, Value = method.ReflectedType.FullName });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@Date", SqlDbType = SqlDbType.DateTime, Value = DateTime.Today });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@DateTime", SqlDbType = SqlDbType.DateTime, Value = DateTime.Now });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = description });
                                cmd.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            Utility.WriteLog(ex);
                            throw new Exception("Lỗi kết nối đến hệ thống");
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
                catch
                {
                }
            });

            thread.Start();
        }

        public static void RecordAuditTrail(string data, string action, string method, string description)
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    using (var conn = new SqlConnection(Utility.ConnStr))
                    {
                        conn.Open();

                        try
                        {
                            using (var cmd = new SqlCommand("proc_AuditTrails_Insert", conn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = Utility.UserID });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@SitemapID", SqlDbType = SqlDbType.Int, Value = Utility.SitemapID });
                                //cmd.Parameters.Add(new SqlParameter { ParameterName = "@Data", SqlDbType = SqlDbType.Xml, Value = data });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@Action", SqlDbType = SqlDbType.VarChar, Value = action });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@Method", SqlDbType = SqlDbType.VarChar, Value = method });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@Date", SqlDbType = SqlDbType.DateTime, Value = DateTime.Today });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@DateTime", SqlDbType = SqlDbType.DateTime, Value = DateTime.Now });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = description });
                                cmd.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            Utility.WriteLog(ex);
                            throw new Exception("Lỗi kết nối đến hệ thống");
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
                catch
                {
                }
            });

            thread.Start();
        }
        #endregion

        #region DataGridUtility
        public static ColumnStyle AddColumn(string caption, string mappingName, int width, string format, CheckCellEventHandler eventHandler)
        {
            var column = new ColumnStyle(1) { Width = 60, HeaderText = "SL y.cầu", MappingName = "RemainQty", Format = "N0" };
            column.HeaderText = caption;
            column.MappingName = mappingName;
            column.Width = width;

            if (!String.IsNullOrEmpty(format))
                column.Format = format;

            if (eventHandler != null)
                column.CheckCellEquals += eventHandler;

            return column;
        }

        public static DataTable LoadDataFromStore(string storeName, List<SqlParameter> listParam)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand(storeName, sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (SqlParameter item in listParam)
                        {
                            cmd.Parameters.Add(new SqlParameter("@" + item.ParameterName, item.Value));
                        }

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLogWithSound(ex);
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            return dt;
        }

        public static string ExecuteScalarFromStore(string storeName, List<SqlParameter> listParam)
        {
            string dt = string.Empty;
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand(storeName, sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (SqlParameter item in listParam)
                        {
                            cmd.Parameters.Add(new SqlParameter("@" + item.ParameterName, item.Value));
                        }

                        dt = (string)cmd.ExecuteScalar();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLogWithSound(ex);
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            return dt;
        }
        #endregion

        #region PlayFile
        //public static void PlayFile(String url)
        //{
        //    WMPLib.WindowsMediaPlayer player = new WMPLib.WindowsMediaPlayer();
        //    player.URL = url;
        //    player.settings.volume = 100;
        //    player.controls.play();
        //    System.Threading.Thread.Sleep(2000);
        //    player.controls.stop();
        //}

        //public static void PlayFile(WMPLib.WindowsMediaPlayer player, String url)
        //{
        //    if (player.playState == WMPLib.WMPPlayState.wmppsReady || player.playState == WMPLib.WMPPlayState.wmppsStopped)
        //    {
        //        player.URL = url;
        //        player.controls.play();
        //        System.Threading.Thread.Sleep(2000);
        //        player.controls.stop();
        //    }
        //    else
        //    {
        //        System.Threading.Thread.Sleep(1000);
        //        player.controls.stop();
        //        PlayFile(player, url);
        //    }
        //}

        //public static Random random = new Random();

        //public static char GetLetter()
        //{
        //    // This method returns a random lowercase letter
        //    // ... Between 'a' and 'z' inclusize.
        //    int num = random.Next(0, 26); // Zero to 25
        //    char let = (char)('A' + num);
        //    return let;
        //} 
        #endregion


    }
}
