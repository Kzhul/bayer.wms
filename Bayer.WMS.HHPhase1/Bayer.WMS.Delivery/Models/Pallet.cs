﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Bayer.WMS.Delivery.Models
{
    public class Pallet
    {
        public string PalletCode { get; set; }

        public string DOImportCode { get; set; }

        public string ReferenceNbr { get; set; }

        public string CompanyCode { get; set; }

        public string Status { get; set; }

        public int ProductQty { get; set; }

        public string CompanyName { get; set; }

        public string StrStatus { get; set; }

        public static void Clear(string palletCode, string productLot)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Clear_Audit", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@ProductLot", productLot));
                        cmd.Parameters.Add(new SqlParameter("@NotProductLot", string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", "Pallets_Clear"));

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static void MoveMaterial(string palletSource, string palletDestination, string productLot, int quantity)
        {
            bool canMove = Check_MultiProductPerPallet(palletSource, palletDestination, productLot, "L", "MoveMaterial");

            if (canMove)
            {
                using (var conn = new SqlConnection(Utility.ConnStr))
                {
                    conn.Open();

                    try
                    {
                        using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MoveMaterial", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@PalletSource", palletSource));
                            cmd.Parameters.Add(new SqlParameter("@PalletDestination", palletDestination));
                            cmd.Parameters.Add(new SqlParameter("@ProductLot", productLot));
                            cmd.Parameters.Add(new SqlParameter("@Quantity", quantity));

                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.WriteLog(ex);
                        throw new Exception("Lỗi kết nối đến hệ thống");
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        public static bool SetLocation(string palletCode, string locationCode, string _currentWork)
        {
            bool canSet = Check_HeatProtection(palletCode, locationCode);
            bool result = false;
            if (canSet)
            {
                DataTable dt = new DataTable();
                try
                {
                    using (var conn = new SqlConnection(Utility.ConnStr))
                    {
                        conn.Open();

                        try
                        {
                            using (var cmd = new SqlCommand("proc_Pallets_SetLocation", conn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                                cmd.Parameters.Add(new SqlParameter("@LocationCode", locationCode));
                                cmd.Parameters.Add(new SqlParameter("@CurrentWork", _currentWork));
                                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                                cmd.Parameters.Add(new SqlParameter("@Method", "Pallets_SetLocation"));

                                using (var adapt = new SqlDataAdapter(cmd))
                                {
                                    adapt.Fill(dt);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Utility.WriteLog(ex);
                            throw new Exception("Lỗi kết nối đến hệ thống");
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }

                    if (dt.Rows.Count == 0)
                    {
                        result = false;
                        throw new Exception("Mã vị trí không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                    }
                    else
                    {
                        result = true;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return result;
        }

        public static bool SetLocationTransit(string palletCode)
        {
            DataTable dt = new DataTable();
            bool result = false;
            try
            {
                using (var conn = new SqlConnection(Utility.ConnStr))
                {
                    conn.Open();

                    try
                    {
                        using (var cmd = new SqlCommand("proc_Pallets_SetLocation", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                            cmd.Parameters.Add(new SqlParameter("@LocationCode", "ZW1_TS_TS1-01"));
                            cmd.Parameters.Add(new SqlParameter("@CurrentWork", string.Empty));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", "Pallets_SetLocation"));

                            using (var adapt = new SqlDataAdapter(cmd))
                            {
                                adapt.Fill(dt);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.WriteLog(ex);
                        throw new Exception("Lỗi kết nối đến hệ thống");
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                if (dt.Rows.Count == 0)
                {
                    result = false;
                    throw new Exception("Mã vị trí không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static bool UpdateVerifyStatus(string palletCode, string status)
        {
            DataTable dt = new DataTable();
            bool result = false;
            try
            {
                using (var conn = new SqlConnection(Utility.ConnStr))
                {
                    conn.Open();

                    try
                    {
                        using (var cmd = new SqlCommand("proc_Pallets_UpdateVerifyStatus", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                            cmd.Parameters.Add(new SqlParameter("@WarehouseVerifyStatus", status));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", "Pallets_SetLocation"));

                            using (var adapt = new SqlDataAdapter(cmd))
                            {
                                adapt.Fill(dt);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.WriteLog(ex);
                        throw new Exception("Lỗi kết nối đến hệ thống");
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static void Merge(string palletCode1, string palletCode2)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Merge", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode1", palletCode1));
                        cmd.Parameters.Add(new SqlParameter("@PalletCode2", palletCode2));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", "PalletStatuses_Merge"));

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static Pallet Load(string palletCode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Select", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã pallet không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            Pallet pl = new Pallet
            {
                PalletCode = dt.Rows[0]["PalletCode"].ToString(),
                DOImportCode = dt.Rows[0]["DOImportCode"] == null ? string.Empty : dt.Rows[0]["DOImportCode"].ToString(),
                ReferenceNbr = dt.Rows[0]["ReferenceNbr"] == null ? string.Empty : dt.Rows[0]["ReferenceNbr"].ToString(),
                CompanyCode = dt.Rows[0]["CompanyCode"] == null ? string.Empty : dt.Rows[0]["CompanyCode"].ToString(),
                CompanyName = dt.Rows[0]["CompanyName"] == null ? string.Empty : dt.Rows[0]["CompanyName"].ToString(),
                Status = dt.Rows[0]["Status"].ToString(),
                StrStatus = dt.Rows[0]["StrStatus"].ToString()
            };

            if (!string.IsNullOrEmpty(pl.CompanyCode))
            {
                MessageBox.Show(
                    pl.CompanyCode + Environment.NewLine
                    + "Trạng Thái: " + pl.StrStatus + Environment.NewLine
                    + "KH: " + pl.CompanyCode + " " + pl.CompanyName
                );
            }

            return pl;
        }

        //public static Pallet Load(string palletCode, string companyCode)
        //{
        //    DataTable dt = new DataTable();
        //    using (var conn = new SqlConnection(Utility.ConnStr))
        //    {
        //        conn.Open();

        //        try
        //        {
        //            using (var cmd = new SqlCommand("proc_Pallets_Select2", conn))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
        //                cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));

        //                using (var adapt = new SqlDataAdapter(cmd))
        //                {
        //                    adapt.Fill(dt);
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Utility.WriteLog(ex);
        //            throw new Exception("Lỗi kết nối đến hệ thống");
        //        }
        //        finally
        //        {
        //            conn.Close();
        //        }
        //    }

        //    if (dt.Rows.Count == 0)
        //        throw new Exception("Vui lòng quét đúng mã pallet của khách hàng.");

        //    return new Pallet
        //    {
        //        PalletCode = dt.Rows[0]["PalletCode"].ToString(),
        //        DOImportCode = dt.Rows[0]["DOImportCode"] == null ? null : dt.Rows[0]["DOImportCode"].ToString(),
        //        ReferenceNbr = dt.Rows[0]["ReferenceNbr"] == null ? null : dt.Rows[0]["ReferenceNbr"].ToString(),
        //        CompanyCode = dt.Rows[0]["CompanyCode"] == null ? null : dt.Rows[0]["CompanyCode"].ToString(),
        //        Status = dt.Rows[0]["Status"].ToString()
        //    };
        //}

        public static DataTable LoadPalletStatuses(string barcode, string type)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select_Details", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadPalletStatusesWithLocation(string barcode, string type)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_SelectIncludeLocation", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadPalletStatusesWithLocation(string barcode, string type, string productbarcode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_SelectPalletIncludeLocation2", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));
                        cmd.Parameters.Add(new SqlParameter("@ProductBarcode", productbarcode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }


        public static DataTable LoadPalletStatuses(string referenceNbr)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select_ByRefereneNbr", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadOptionPalletStatus(string palletCode, params string[] statuses)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Select_With_PalletStatuses", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã pallet không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            //if (statuses.Length > 0)
            //{
            //    foreach (string stt in statuses)
            //    {
            //        if (dt.Select(String.Format("Status = '{0}'", stt)).Length > 0)
            //            return dt;
            //    }

            //    throw new Exception("Trạng thái pallet không hợp lệ, pallet này đã được sử dụng vào giai đoạn khác, vui lòng kiểm tra lại dữ liệu.");
            //}

            return dt;
        }

        public static DataTable LoadRequirePalletStatus(string barcode, string type, params string[] statuses)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return dt;
        }

        public static DataTable LoadRequirePalletStatusLocation(string barcode, string type)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_SelectPalletIncludeLocation", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 5;
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã không tồn tại, vui lòng kiểm tra lại dữ liệu.");
            return dt;
        }

        public static DataTable LoadPalletReady(string type)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_ReportLocation_PalletReady", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadPalletProductWaitingForConfirm()
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallet_ProductWaitingForConfirm", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadPalletDoneByUser(string workType)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_SelectPalletDoneByUser", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@WorkType", workType));
                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadPalletMaterialWaitingForConfirm()
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallet_MaterialMoveWaitingForConfirm", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadRequirePalletStatusGroupByBarcode(string palletCode, params string[] statuses)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select_GroupByBarcode", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã QR không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return dt;
        }

        public static DataTable LoadPalletStatusGroupByPackingType(string palletCode, params string[] statuses)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select_GroupByPackingType", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã QR không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return dt;
        }

        public static void UpdatePallet_MovePromotion(string palletCode, int productID, string productCode, string referenceNbr, string companyCode, int qty, string method, SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MovePromotion", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                cmd.Parameters.Add(new SqlParameter("@ProductID", productID));
                cmd.Parameters.Add(new SqlParameter("@ProductCode", productCode));
                cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));
                cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                cmd.Parameters.Add(new SqlParameter("@Qty", qty));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Move product from another pallet to this pallet
        /// NOT USED
        /// </summary>
        /// <param name="palletCode">destination pallet</param>
        public static void UpdatePallet_MoveProduct(string palletCode, string barcode, string type, string method, SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MoveProduct", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                cmd.Parameters.Add(new SqlParameter("@Type", type));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Move product from another pallet to this pallet
        /// </summary>
        /// <param name="palletCode">destination pallet</param>
        public static void UpdatePallet_MoveProduct(string palletCode, string barcode, string type, string method)
        {
            bool canMove = Check_MultiProductPerPallet(string.Empty, palletCode, barcode, type, "UpdatePallet_MoveProduct");

            if (canMove)
            {
                using (var conn = new SqlConnection(Utility.ConnStr))
                {
                    conn.Open();

                    try
                    {
                        using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MoveProduct", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                            cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                            cmd.Parameters.Add(new SqlParameter("@Type", type));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method));

                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.WriteLog(ex);
                        throw new Exception("Lỗi kết nối đến hệ thống");
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Check_MultiProductPerPallet
        /// </summary>
        /// <param name="palletSourceCode">can be empty</param>
        /// <param name="palletDestinationCode"></param>
        /// <param name="barcode"></param>
        /// <param name="type">type of barcode: C L E P </param>
        /// <param name="method"></param>
        /// <returns></returns>
        public static bool Check_MultiProductPerPallet(string palletSourceCode, string palletDestinationCode, string barcode, string type, string method)
        {
            if (Utility.SettingMultiProductPerPallet == 0)
            {
                DataTable dt = new DataTable();
                using (var sqlConn = new SqlConnection(Utility.ConnStr))
                {
                    sqlConn.Open();

                    try
                    {
                        using (var cmd = new SqlCommand("proc_PalletStatuses_CheckMultiProductPerPallet", sqlConn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@PalletSource", palletSourceCode));
                            cmd.Parameters.Add(new SqlParameter("@PalletDestination", palletDestinationCode));
                            cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                            cmd.Parameters.Add(new SqlParameter("@Type", type));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method));

                            using (var adapt = new SqlDataAdapter(cmd))
                            {
                                adapt.Fill(dt);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.WriteLog(ex);
                        throw new Exception("Lỗi kết nối đến hệ thống");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
                }

                if (dt.Rows.Count != 0)
                {
                    MessageBox.Show(dt.Rows[0]["Reason"].ToString());
                    return false;
                }
            }
            return true;
        }

        public static bool Check_HeatProtection(string palletCode, string locationCode)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Check_HeatProtection", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@LocationCode", locationCode));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", "Check_HeatProtection"));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count != 0)
            {
                MessageBox.Show(dt.Rows[0]["Reason"].ToString());
                return false;
            }
            return true;
        }

        public static void UpdatePallet_ExportProduct(string palletCode, string barcode, string type, string method)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Update_ExportProduct", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static void ProductReturnUpdateDeliveredProduct(string documentNBR, string barcode, string method)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_ProductReturn_Update_Delivery", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@DocumentNBR", documentNBR));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// NOT USED
        /// </summary>
        /// <param name="palletSource"></param>
        /// <param name="palletCode"></param>
        /// <param name="barcode"></param>
        /// <param name="type"></param>
        /// <param name="method"></param>
        public static void UpdatePallet_MoveProduct_VerifyPalletSource(string palletSource, string palletCode, string barcode, string type, string method)
        {
            DataTable dt = new DataTable();



            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MoveProduct_VerifyPalletSource", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletSource", palletSource));
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã QR không tồn tại trên pallet nguồn, vui lòng kiểm tra lại dữ liệu.");
        }

        /// <summary>
        /// Move product lot from another pallet to this pallet
        /// </summary>
        /// <param name="palletCode">destination pallet</param>
        public static void UpdatePallet_MoveProductLot(string palletSource, string palletCode, string barcode, string type, string method)
        {
            bool canMove = Check_MultiProductPerPallet(string.Empty, palletCode, barcode, type, "UpdatePallet_MoveProductLot");

            if (canMove)
            {
                DataTable dt = new DataTable();
                using (var conn = new SqlConnection(Utility.ConnStr))
                {
                    conn.Open();
                    try
                    {
                        using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MoveProductLot", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@PalletSource", palletSource));
                            cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                            cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                            cmd.Parameters.Add(new SqlParameter("@Type", type));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method));

                            using (var adapt = new SqlDataAdapter(cmd))
                            {
                                adapt.Fill(dt);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.WriteLog(ex);
                        throw new Exception("Lỗi kết nối đến hệ thống");
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                if (dt.Rows.Count == 0)
                    throw new Exception("Mã lô không tồn tại trên pallet nguồn, vui lòng kiểm tra lại dữ liệu.");
            }
        }

        public static void UpdatePallet_Status(string palletCode, string doImportCode, string referenceNbr, string companyCode, string status, string method,
            SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_Pallets_Update_Status", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));
                cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                cmd.Parameters.Add(new SqlParameter("@Status", status));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdatePallet_Status(string palletCode, string doImportCode, string referenceNbr, string companyCode, string status, string method)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Update_Status", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                        cmd.Parameters.Add(new SqlParameter("@Status", status));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static void UpdatePallet_ExportStatus(string palletCode, string doImportCode, string referenceNbr, string companyCode, string status, string method)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Update_ExportStatus", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                        cmd.Parameters.Add(new SqlParameter("@Status", status));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static void UpdatePallet_ReceivedStatus(string palletCode, string doImportCode, string referenceNbr, string companyCode, string status, string method)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Update_ReceivedStatus", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                        cmd.Parameters.Add(new SqlParameter("@Status", status));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }
        }



        public static void UpdatePalletStatus(string barcode, string type, string newPalletCode, string method,
            SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_PalletStatuses_Update", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                cmd.Parameters.Add(new SqlParameter("@Type", type));
                cmd.Parameters.Add(new SqlParameter("@NewPalletCode", newPalletCode));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateUpdate_Delivery2(string palletCode, string method)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Update_Delivery2", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static DataTable LoadReferences(string groupName)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_References_Select", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@GroupName", groupName));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static bool ConfirmReceiveProduct(string palletCode)
        {
            bool result = false;
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallet_ConfirmReceiveProduct", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", "ConfirmReceiveProduct"));

                        cmd.ExecuteNonQuery();
                    }

                    result = true;
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return result;
        }

        public static bool ConfirmMoveMaterial(string palletCode)
        {
            bool result = false;
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallet_ConfirmMoveMaterial", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", "ConfirmReceiveProduct"));

                        cmd.ExecuteNonQuery();
                    }

                    result = true;
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return result;
        }
    }
}
