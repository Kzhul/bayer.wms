﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading;
using Bayer.WMS.Delivery.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class PrepareView_NonDO : Form
    {
        #region Param
        protected BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;

        private bool _isProcess = false;
        private Queue<string> _queue;
        private Company _company;
        private Pallet _currentPallet;
        private DataTable _dt;
        private DataTable _dtProductByCompany;
        private bool dataChanged = false;
        private string _userInfo = "Soạn hàng không DO";
        private string _palletInfo = string.Empty;
        private bool _isAccessCodePass = false;
        #endregion

        #region InitForm
        public PrepareView_NonDO()
        {
            try
            {
                InitializeComponent();
                InitDataGridView();
                this.KeyPreview = true;
                _queue = new Queue<string>();
                _stepHints = new List<string> 
                { 
                    "Quét mã nhân viên",
                    "Quét mã khách hàng",
                    "Quét mã pallet có hàng để xuất chẳn" + Environment.NewLine + "Quét mã pallet trống để xuất lẻ",
                    "Bấm xác nhận để xuất pallet này cho khách hàng",
                    "Quét mã truy cập để soạn hàng không theo DO"
                };

                lblStepHints.Text = _stepHints[4];
                _company = new Company();
                _currentPallet = new Pallet();
                _dt = new DataTable();
                _dtProductByCompany = new DataTable();
                _queue = new Queue<string>();

                _userInfo = "SH_No_DO: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
                this.Text = _userInfo;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);

            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isProcess)
                    QueueProcess(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }
        #endregion

        #region MENU BUTTON
        public void mniClearPallet_Click(object sender, EventArgs e)
        {
            clearPallet();
        }

        public void clearPallet()
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ClearPalletView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[2];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        public void mniMergePallet_Click(object sender, EventArgs e)
        {
            mergePallet();
        }

        public void mergePallet()
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MergePalletView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[2];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        public void mniMergeCarton_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MergeCartonView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[2];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        public void mniExit_Click(object sender, EventArgs e)
        {
            exitForm();
        }

        public void exitForm()
        {
            try
            {
                DisposeBarcodeReader();
                this.Close();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        //Xác nhận xuất lấy luôn pallet này cho KH
        private void menuConfirmPallet_Click(object sender, EventArgs e)
        {
            confirmPallet();
        }

        private void confirmPallet()
        {
            try
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    if (_currentPallet.Status == "N")
                    {
                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_Status(_currentPallet.PalletCode, string.Empty, string.Empty, _company.CompanyCode, "P", method.Name);

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                    else
                    {
                        MessageBox.Show("Trạng thái pallet không đúng, không thể xác nhận.");
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        //Trả lại pallet
        private void menuReturnPallet_Click(object sender, EventArgs e)
        {
            returnPallet();
        }

        private void returnPallet()
        {
            try
            {
                //Check trạng thái
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode) && _currentPallet.Status == "P")
                {
                    //Xác nhận có muốn thực hiện hay ko
                    if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn trả lại pallet: " + _currentPallet.PalletCode + " ?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_Status(_currentPallet.PalletCode, string.Empty, string.Empty, string.Empty, "N", method.Name);

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void menuPackaging_Click(object sender, EventArgs e)
        {
            packaging();
        }

        private void packaging()
        {
            try
            {
                DisposeBarcodeReader();
                if (_company != null)
                {
                    using (var view = new PackagingView())
                    {
                        view.OddQty = 0;// _process.OddQty;
                        view.PrepareCode = "SH" + DateTime.Today.ToString("yyMMdd") + "01";//_process.ReferenceNbr;SH17092701
                        view.CompanyName = _company.CompanyName;
                        view.Executor = Utility.CurrentUser.LastName + " " + Utility.CurrentUser.FirstName;
                        view.DeliveryDate = DateTime.Today;

                        view.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void menuPrintPallet_Click(object sender, EventArgs e)
        {
            printPallet();
        }

        private void printPallet()
        {
            try
            {
                DisposeBarcodeReader();
                if (_company != null)
                {
                    using (var view = new PrintPalletLabelView())
                    {
                        view.CompanyCode = _company.CompanyCode;
                        view.CompanyName = _company.CompanyName;
                        view.DeliveryDate = DateTime.Today.ToString();
                        view.Executor = Utility.CurrentUser.LastName + " " + Utility.CurrentUser.FirstName;
                        if (view.ShowDialog() != DialogResult.OK)
                            return;
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }
        #endregion

        #region Init
        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            //decimal requestQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["DOQuantity"].ToString());
            //decimal exportedQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["Quantity"].ToString());

            //if (exportedQty == 0)
            //    e.MeetsCriteria = 0;
            //else if (exportedQty < requestQty)
            //    e.MeetsCriteria = 1;
            //else if (exportedQty > requestQty)
            //    e.MeetsCriteria = 3;
            //else
            //    e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            try
            {
                #region dtgPallets
                var dtgStyle = new DataGridTableStyle { MappingName = "PalletList" };
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã", "PalletCode", 75, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng", "CartonQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Bao", "BagQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Xô", "ShoveQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Can", "CanQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Người dùng", "UserFullName", 150, String.Empty, null));

                dtgPallets.TableStyles.Add(dtgStyle);
                #endregion

                #region dtgProducts
                var dtgProductStyle = new DataGridTableStyle { MappingName = "ProductList" };
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));
                //dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL(DO)", "DOQuantity", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 20, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCellEquals));              
                dtgProducts.TableStyles.Add(dtgProductStyle);
                #endregion

                #region dtgPalletInfo
                var dtgPalletInfoStyle = new DataGridTableStyle { MappingName = "CartonList" };
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã", "CartonBarcode", 90, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 40, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
                dtgPalletInfo.TableStyles.Add(dtgPalletInfoStyle);
                #endregion

                #region dtgPalletDetailProduct
                var dtgPalletDetailProductStyle = new DataGridTableStyle { MappingName = "PalletDetailProduct" };
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 40, String.Empty, null));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, null));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 60, String.Empty, null));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
                dtgPalletDetailProduct.TableStyles.Add(dtgPalletDetailProductStyle);
                #endregion

                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ProcessBarcode
        public string currentBarCode = string.Empty;
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                currentBarCode = barcode;

                //// step 0: scan acess code
                if (Utility.AccessCodeRegex.IsMatch(barcode))
                {
                    ProcessAccessCode(barcode);
                }
                else if (Utility.CustomerRegex.IsMatch(barcode))
                {
                    barcode = barcode.Remove(0, 3);
                    ProcessCustomerBarcode(barcode);
                }
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessAccessCode(string barcode)
        {
            try
            {
                if (User.CheckAccessCode(barcode))
                {
                    _isAccessCodePass = true;
                    lblStepHints.Text = _stepHints[1];
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                Utility.WriteLog(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        private void ProcessCustomerBarcode(string barcode)
        {
            try
            {
                if (!_isAccessCodePass)
                {
                    throw new Exception("Vui lòng quét mã PIN.");
                }

                Cursor.Current = Cursors.WaitCursor;
                dataChanged = false;
                _company = Company.Load(barcode);
                lblCurrentActionInfo.Text = _company.CompanyName;

                LoadPalletByCompany(barcode);
                LoadProductByCompany(barcode);

                bdsPalletInfo.DataSource = null;
                bdsPalletDetailProduct.DataSource = null;

                tpPrepareInfo.SelectedIndex = 1;

                lblStepHints.Text = "Quét mã pallet / thùng / lẻ";
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");

                //// get quantity details of pallet
                LoadPalletInfo(barcode);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessProductBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                #region Validate Data
                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");
                if (String.IsNullOrEmpty(_currentPallet.PalletCode))
                    throw new Exception("Vui lòng quét mã pallet.");
                #endregion

                #region
                if (_dt.Select(String.Format("CartonBarcode = '{0}'", barcode)).Length > 0)
                    throw new Exception("Mã QR đã được quét, vui lòng kiểm tra lại");
                #endregion

                #region Verify Prepared Quantity
                //Do thing here
                #endregion

                #region InsertData
                if (
                     Utility.CartonBarcode3Regex.IsMatch(barcode)
                    || Utility.CartonBarcode2Regex.IsMatch(barcode)
                    || Utility.CartonBarcodeRegex.IsMatch(barcode)
                    )
                {
                    var method = this.GetType().GetMethod("ProcessBarcode");
                    Pallet.UpdatePallet_MoveProduct(_currentPallet.PalletCode, barcode, "C", method.Name);
                }
                else
                {
                    try
                    {
                        string encryptedBarcode = barcode;
                        string type;
                        barcode = Barcode.Process(encryptedBarcode, false, false, true, out type);
                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_MoveProduct(_currentPallet.PalletCode, barcode, "E", method.Name);
                    }
                    catch
                    {
                        throw new Exception("Mã không hợp lệ");
                    }
                }
                #endregion

                #region Reload Data
                LoadPalletInfo(_currentPallet.PalletCode);
                tpPrepareInfo.SelectedIndex = 3;

                dataChanged = true;
                #endregion
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void LoadPalletByCompany(string barcode)
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("CompanyCode", barcode));
                bdsPallets.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_Pallets_SelectByCompany", listParam);
                a.TableName = "PalletList";
                bdsPallets.DataSource = a;

                tpPrepareInfo.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadProductByCompany(string barcode)
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("CompanyCode", barcode));
                bdsProduct.DataSource = null;
                _dtProductByCompany = new DataTable();
                _dtProductByCompany = Utility.LoadDataFromStore("proc_PalletStatuses_SelectByCompany_GroupByBarCode", listParam);
                _dtProductByCompany.TableName = "ProductList";
                bdsProduct.DataSource = _dtProductByCompany;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadPalletInfo(string barcode)
        {
            try
            {
                _currentPallet = Pallet.Load(barcode);
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    #region bdsPalletInfo
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    listParam.Add(new SqlParameter("Barcode", barcode));
                    listParam.Add(new SqlParameter("Type", "P"));
                    bdsPalletInfo.DataSource = null;
                    DataTable a = Utility.LoadDataFromStore("proc_PalletStatuses_Select2", listParam);
                    a.TableName = "CartonList";
                    bdsPalletInfo.DataSource = a;
                    _dt = a;
                    #endregion


                    #region bdsPalletDetailProduct
                    List<SqlParameter> listParam2 = new List<SqlParameter>();
                    listParam2.Add(new SqlParameter("Barcode", barcode));
                    listParam2.Add(new SqlParameter("CompanyCode", string.Empty));
                    listParam2.Add(new SqlParameter("DOImportCode", string.Empty));
                    bdsPalletDetailProduct.DataSource = null;
                    DataTable b = Utility.LoadDataFromStore("proc_PalletStatuses_SelectByPallet_GroupByBarCode", listParam2);
                    b.TableName = "PalletDetailProduct";
                    bdsPalletDetailProduct.DataSource = b;
                    #endregion

                    #region ThungBaoXoCan
                    try
                    {
                        List<SqlParameter> listParam3 = new List<SqlParameter>();
                        listParam3.Add(new SqlParameter("Barcode", barcode));
                        DataTable c = Utility.LoadDataFromStore("proc_Pallets_Select_SumByPackingType", listParam3);
                        _palletInfo = c.Rows[0]["StrQuantity"].ToString();

                        //Nếu quét pallet và pallet này đang soạn hàng
                        if (currentBarCode == barcode && c.Rows[0]["CompanyCode"].ToString() != string.Empty)
                        {
                            MessageBox.Show(barcode + Environment.NewLine + "Trạng Thái: " + c.Rows[0]["StrStatus"].ToString()
                                                     + Environment.NewLine + "KH: " + c.Rows[0]["CompanyCode"].ToString() + " " + c.Rows[0]["CompanyName"].ToString()
                                );
                        }
                    }
                    catch
                    {

                    }
                    #endregion

                    //Nếu đây là pallet mới và có SP
                    if (_currentPallet != null && _currentPallet.Status == "N" && b.Rows.Count > 0)
                    {
                        lblStepHints.Text = _stepHints[3];
                    }

                    tpPrepareInfo.SelectedIndex = 2;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ActionOnScreen
        private void dtgPallets_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                //MessageBox.Show(dtgPallets[dtgPallets.CurrentRowIndex, 0].ToString());
                txtBarcode.Text = dtgPallets[dtgPallets.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void tpPrepareInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                #region ForReloadData
                if (dataChanged)
                {
                    //Nếu check vào 2 tab tổng của KH, load lại list pallet và sl
                    if (tpPrepareInfo.SelectedIndex <= 1)
                    {
                        dataChanged = false;

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        #endregion
                    }
                }
                #endregion

                if (tpPrepareInfo.SelectedIndex <= 1)
                {
                    if (_company != null && !string.IsNullOrEmpty(_company.CompanyName))
                    {
                        lblCurrentActionInfo.Text = "KH:" + _company.CompanyName;
                        this.Text = _userInfo;
                    }
                }
                else
                {
                    if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                    {
                        lblCurrentActionInfo.Text = _currentPallet.PalletCode + ": " + _palletInfo;
                        //this.Text = _palletInfo;
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        private void menuQRCode_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.BarcodeTrackingView())
                {
                    view.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void PrepareView_NonDO_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }
    }
}