﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ReceiveMaterialPalletView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblCurrentTopic = new System.Windows.Forms.Label();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblPallet = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.lblProduct = new System.Windows.Forms.Label();
            this.lblProductLot = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.bdsProduct = new System.Windows.Forms.BindingSource(this.components);
            this.dtgProduct = new System.Windows.Forms.DataGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bdsPallet = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPallet = new System.Windows.Forms.DataGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.bdsListDocument = new System.Windows.Forms.BindingSource(this.components);
            this.dtgListDocument = new System.Windows.Forms.DataGrid();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProduct)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallet)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsListDocument)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 14;
            // 
            // lblCurrentTopic
            // 
            this.lblCurrentTopic.Location = new System.Drawing.Point(3, 27);
            this.lblCurrentTopic.Name = "lblCurrentTopic";
            this.lblCurrentTopic.Size = new System.Drawing.Size(234, 21);
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 221);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 47);
            this.lblStepHints.Text = "Quét mã Pallet để bắt đầu\r\nQuét mã vị trí để xác nhận";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Enabled = false;
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(0, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 167);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblPallet);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtQuantity);
            this.tabPage1.Controls.Add(this.lblProduct);
            this.tabPage1.Controls.Add(this.lblProductLot);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(240, 144);
            this.tabPage1.Text = "1.Nhập hàng";
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(2, 102);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(237, 21);
            this.lblPallet.Text = "Pallet:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(1, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 21);
            this.label4.Text = "SL:";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(34, 123);
            this.txtQuantity.MaxLength = 100;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(206, 21);
            this.txtQuantity.TabIndex = 29;
            // 
            // lblProduct
            // 
            this.lblProduct.Location = new System.Drawing.Point(1, 24);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(237, 78);
            this.lblProduct.Text = "Sản phẩm:";
            // 
            // lblProductLot
            // 
            this.lblProductLot.Location = new System.Drawing.Point(3, 0);
            this.lblProductLot.Name = "lblProductLot";
            this.lblProductLot.Size = new System.Drawing.Size(237, 21);
            this.lblProductLot.Text = "Mã lô:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dtgProduct);
            this.tabPage3.Location = new System.Drawing.Point(0, 0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(232, 141);
            this.tabPage3.Text = "2.SP";
            // 
            // dtgProduct
            // 
            this.dtgProduct.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgProduct.DataSource = this.bdsProduct;
            this.dtgProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgProduct.Location = new System.Drawing.Point(0, 0);
            this.dtgProduct.Name = "dtgProduct";
            this.dtgProduct.RowHeadersVisible = false;
            this.dtgProduct.Size = new System.Drawing.Size(232, 141);
            this.dtgProduct.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgPallet);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(240, 144);
            this.tabPage2.Text = "3.Pallet";
            // 
            // bdsPallet
            // 
            this.bdsPallet.Position = 0;
            // 
            // dtgPallet
            // 
            this.dtgPallet.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPallet.DataSource = this.bdsPallet;
            this.dtgPallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallet.Location = new System.Drawing.Point(0, 0);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.RowHeadersVisible = false;
            this.dtgPallet.Size = new System.Drawing.Size(240, 144);
            this.dtgPallet.TabIndex = 1;
            this.dtgPallet.DoubleClick += new System.EventHandler(this.dtgPallet_DoubleClick);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dtgListDocument);
            this.tabPage4.Location = new System.Drawing.Point(0, 0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(240, 144);
            this.tabPage4.Text = "4.DS Phiếu";
            // 
            // dtgListDocument
            // 
            this.dtgListDocument.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgListDocument.DataSource = this.bdsListDocument;
            this.dtgListDocument.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgListDocument.Location = new System.Drawing.Point(0, 0);
            this.dtgListDocument.Name = "dtgListDocument";
            this.dtgListDocument.RowHeadersVisible = false;
            this.dtgListDocument.Size = new System.Drawing.Size(240, 144);
            this.dtgListDocument.TabIndex = 2;
            this.dtgListDocument.DoubleClick += new System.EventHandler(this.dtgListDocument_DoubleClick);
            // 
            // ReceiveMaterialPalletView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.lblCurrentTopic);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "ReceiveMaterialPalletView";
            this.Text = "Nhập hàng";
            this.Load += new System.EventHandler(this.ReceiveMaterialPalletView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ReceiveMaterialPalletView_Closing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsProduct)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallet)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsListDocument)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblCurrentTopic;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGrid dtgPallet;
        private System.Windows.Forms.BindingSource bdsPallet;
        private System.Windows.Forms.Label lblProductLot;
        private System.Windows.Forms.Label lblProduct;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGrid dtgProduct;
        private System.Windows.Forms.BindingSource bdsProduct;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGrid dtgListDocument;
        private System.Windows.Forms.BindingSource bdsListDocument;
    }
}