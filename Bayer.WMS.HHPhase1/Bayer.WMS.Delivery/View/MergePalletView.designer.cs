﻿namespace Bayer.WMS.Handheld.Views
{
    partial class MergePalletView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPallet1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPallet2 = new System.Windows.Forms.Label();
            this.lblEach = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblBag = new System.Windows.Forms.Label();
            this.lblShove = new System.Windows.Forms.Label();
            this.lblCan = new System.Windows.Forms.Label();
            this.lblCarton = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 21);
            this.label5.Text = "Pallet 1:";
            // 
            // lblPallet1
            // 
            this.lblPallet1.Location = new System.Drawing.Point(59, 27);
            this.lblPallet1.Name = "lblPallet1";
            this.lblPallet1.Size = new System.Drawing.Size(178, 21);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 21);
            this.label1.Text = "Pallet 2:";
            // 
            // lblPallet2
            // 
            this.lblPallet2.Location = new System.Drawing.Point(59, 48);
            this.lblPallet2.Name = "lblPallet2";
            this.lblPallet2.Size = new System.Drawing.Size(178, 21);
            // 
            // lblEach
            // 
            this.lblEach.Location = new System.Drawing.Point(59, 111);
            this.lblEach.Name = "lblEach";
            this.lblEach.Size = new System.Drawing.Size(50, 21);
            this.lblEach.Text = "0";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 21);
            this.label6.Text = "Lẻ:";
            // 
            // lblBag
            // 
            this.lblBag.Location = new System.Drawing.Point(156, 90);
            this.lblBag.Name = "lblBag";
            this.lblBag.Size = new System.Drawing.Size(50, 21);
            this.lblBag.Text = "0";
            // 
            // lblShove
            // 
            this.lblShove.Location = new System.Drawing.Point(156, 69);
            this.lblShove.Name = "lblShove";
            this.lblShove.Size = new System.Drawing.Size(50, 21);
            this.lblShove.Text = "0";
            // 
            // lblCan
            // 
            this.lblCan.Location = new System.Drawing.Point(59, 90);
            this.lblCan.Name = "lblCan";
            this.lblCan.Size = new System.Drawing.Size(50, 21);
            this.lblCan.Text = "0";
            // 
            // lblCarton
            // 
            this.lblCarton.Location = new System.Drawing.Point(59, 69);
            this.lblCarton.Name = "lblCarton";
            this.lblCarton.Size = new System.Drawing.Size(50, 21);
            this.lblCarton.Text = "0";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(115, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 21);
            this.label4.Text = "Bao:";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(3, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 21);
            this.label7.Text = "Can:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(115, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 21);
            this.label8.Text = "Xô:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(3, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 21);
            this.label9.Text = "Thùng:";
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 248);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 20);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic);
            this.label2.Location = new System.Drawing.Point(3, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(234, 47);
            this.label2.Text = "Pallet 1 sẽ được dồn vào Pallet 2. Pallet 1 sẽ được clear trạng thái thành pallet" +
                " trống";
            // 
            // MergePalletView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.lblEach);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblBag);
            this.Controls.Add(this.lblShove);
            this.Controls.Add(this.lblCan);
            this.Controls.Add(this.lblCarton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblPallet2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPallet1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "MergePalletView";
            this.Text = "Dồn pallet";
            this.Load += new System.EventHandler(this.MergePalletView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MergePalletView_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPallet1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPallet2;
        private System.Windows.Forms.Label lblEach;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblBag;
        private System.Windows.Forms.Label lblShove;
        private System.Windows.Forms.Label lblCan;
        private System.Windows.Forms.Label lblCarton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.Label label2;
    }
}