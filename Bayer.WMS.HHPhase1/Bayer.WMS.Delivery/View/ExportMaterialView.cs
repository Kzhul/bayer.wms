﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading;
using Bayer.WMS.Delivery.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ExportMaterialView : Form
    {
        #region Param
        protected BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;

        private bool _isProcess = false;
        private Queue<string> _queue;
        private Pallet _currentPallet;
        private DataTable _dt;
        private DataTable _dtProductByCompany;
        private bool dataChanged = false;
        private string _userInfo = "Soạn hàng theo DO";
        private string _palletInfo = string.Empty;
        #endregion

        #region InitForm
        public ExportMaterialView()
        {
            try
            {
                InitializeComponent();
                InitDataGridView();
                this.KeyPreview = true;
                _queue = new Queue<string>();
                _stepHints = new List<string> 
                { 
                    "Quét mã phiếu YC-NL",
                    "Quét mã pallet có hàng để xuất chẳn" + Environment.NewLine + "Quét mã pallet trống để xuất lẻ",
                    "Bấm xác nhận để xuất pallet này",
                };

                lblStepHints.Text = _stepHints[0];
                _currentPallet = new Pallet();
                _dt = new DataTable();
                _dtProductByCompany = new DataTable();
                _queue = new Queue<string>();

                _userInfo = "XH: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
                this.Text = _userInfo;

                LoadListDO();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);

            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isProcess)
                    QueueProcess(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }
        #endregion

        #region MENU BUTTON
        public void mniExit_Click(object sender, EventArgs e)
        {
            exitForm();
        }

        public void exitForm()
        {
            try
            {
                DisposeBarcodeReader();
                this.Close();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        //Xác nhận xuất lấy luôn pallet này cho KH
        private void menuConfirmPallet_Click(object sender, EventArgs e)
        {
            confirmPallet();
        }

        private void confirmPallet()
        {
            try
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    if (_currentPallet.Status == "N")
                    {
                        #region Verify Data
                        List<SqlParameter> listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("DocumentNbr", currentDocument));
                        listParam.Add(new SqlParameter("PalletCode", _currentPallet.PalletCode));
                        DataTable dt = Utility.LoadDataFromStore("proc_RequestMaterial_VerifyPallet", listParam);
                        if (dt.Rows.Count > 0)
                        {
                            string mes =
                                "Vượt quá số lượng yêu cầu" + Environment.NewLine
                                + "SL: " + dt.Rows[0]["PalletQuantity"].ToString() + " / " + dt.Rows[0]["RequestQty"].ToString() + Environment.NewLine
                                + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                                ;

                            Utility.PlayErrorSound();
                            MessageBox.Show(mes, "Lỗi");
                            return;
                        }
                        #endregion

                        #region Update Data
                        listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("DocumentNbr", currentDocument));
                        listParam.Add(new SqlParameter("PalletCode", _currentPallet.PalletCode));
                        listParam.Add(new SqlParameter("@UserID", Utility.UserID));
                        listParam.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        listParam.Add(new SqlParameter("@Method", "ConfirmReceiveProduct"));
                        Utility.LoadDataFromStore("proc_RequestMaterial_ConfirmPallet", listParam);
                        #endregion

                        #region Load lại data 2 table
                        ProcessDocumentBarcode(currentDocument);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                    else
                    {
                        MessageBox.Show("Trạng thái pallet không đúng, không thể xác nhận.");
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        //Trả lại pallet
        private void menuReturnPallet_Click(object sender, EventArgs e)
        {
            returnPallet();
        }

        private void returnPallet()
        {
            try
            {
                //Check trạng thái
                if (_currentPallet != null 
                    && !string.IsNullOrEmpty(_currentPallet.PalletCode) 
                    && _currentPallet.Status == "B")
                {
                    //Xác nhận có muốn thực hiện hay ko
                    if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn trả lại pallet: " + _currentPallet.PalletCode + " ?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        #region Update Data
                        List<SqlParameter> listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("DocumentNbr", currentDocument));
                        listParam.Add(new SqlParameter("PalletCode", _currentPallet.PalletCode));

                        var method = this.GetType().GetMethod("ProcessBarcode");
                        DataTable dt = Utility.LoadDataFromStore("proc_RequestMaterial_ReturnPallet", listParam);
                        #endregion

                        #region Load lại data 2 table
                        ProcessDocumentBarcode(currentDocument);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region Init
        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["RequestQty"].ToString());
            decimal exportedQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["PickedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            try
            {
                #region dtgPallets
                var dtgStyle = new DataGridTableStyle { MappingName = "PalletList" };
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "ID", 25, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 75, String.Empty, null));                
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "PalletQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL yêu cầu", "RequestQty", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL cần lấy", "NeedQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Ngày HH", "ExpiredDate", 60, String.Empty, null));
                dtgPallets.TableStyles.Add(dtgStyle);
                #endregion

                #region dtgProducts
                var dtgProductStyle = new DataGridTableStyle { MappingName = "ProductList" };
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "ID", 25, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL cần lấy", "NeedQty", 60, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL yêu cầu", "RequestQty", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL đã lấy", "PickedQty", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCellEquals));
                dtgProducts.TableStyles.Add(dtgProductStyle);
                #endregion

                #region dtgPalletInfo
                var dtgPalletInfoStyle = new DataGridTableStyle { MappingName = "CartonList" };
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("#", "ID", 25, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã PL", "PalletCode", 75, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Vị trí", "LocationName", 60, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 40, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
                dtgPalletInfo.TableStyles.Add(dtgPalletInfoStyle);
                #endregion

                #region dtgDO
                var dtgDOStyle = new DataGridTableStyle { MappingName = "DOList" };
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Ngày giao", "ExportDate", 80, String.Empty, null));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Mã phiếu", "DocumentNbr", 100, String.Empty, null));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Yêu cầu", "RequestQty", 50, String.Empty, null));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Đã Xuất", "ExportedQty", 50, String.Empty, null));
                dtgDocuments.TableStyles.Add(dtgDOStyle);
                #endregion

                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ProcessBarcode
        public string currentBarCode = string.Empty;
        public string currentDocument = string.Empty;
        public int documentType = 0;

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                currentBarCode = barcode;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (string.IsNullOrEmpty(currentDocument))
                {
                    ProcessDocumentBarcode(barcode);
                    lblStepHints.Text = _stepHints[1];
                }
                else 
                {
                    ProcessPalletBarcode(barcode);
                    lblStepHints.Text = _stepHints[2];
                }
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", lblCurrentActionInfo.Text, barcode, ex.Message);
                Utility.PlayErrorSound();
                Utility.WriteLog(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessDocumentBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                dataChanged = false;

                currentDocument = barcode;
                LoadPalletByDocument(barcode);
                LoadProductByDocument(barcode);

                bdsPalletInfo.DataSource = null;
                tpPrepareInfo.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(currentDocument))
                    throw new Exception("Vui lòng quét mã phiếu.");

                //// get quantity details of pallet
                LoadPalletInfo(barcode);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #region LoadData
        public void LoadPalletByDocument(string barcode)
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", barcode));
                bdsPallets.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_RequestMaterial_SelectPallet", listParam);
                a.TableName = "PalletList";
                bdsPallets.DataSource = a;

                tpPrepareInfo.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadProductByDocument(string barcode)
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", barcode));
                bdsProduct.DataSource = null;
                _dtProductByCompany = new DataTable();
                _dtProductByCompany = Utility.LoadDataFromStore("proc_RequestMaterial_SelectProduct", listParam);
                _dtProductByCompany.TableName = "ProductList";
                bdsProduct.DataSource = _dtProductByCompany;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadPalletInfo(string barcode)
        {
            try
            {
                #region bdsPalletInfo
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("Barcode", barcode));
                listParam.Add(new SqlParameter("Type", "P"));
                bdsPalletInfo.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_PalletStatuses_SelectPalletIncludeLocation", listParam);
                a.TableName = "CartonList";
                bdsPalletInfo.DataSource = a;
                _dt = a;
                #endregion
                tpPrepareInfo.SelectedIndex = 2;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadListDO()
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                bdsDocuments.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_RequestMaterial_SelectDocuments", listParam);
                a.TableName = "DOList";
                bdsDocuments.DataSource = a;
                tpPrepareInfo.SelectedIndex = 3;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion
        #endregion

        #region ActionOnScreen
        private void dtgPallets_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgPallets[dtgPallets.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void tpPrepareInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                #region ForReloadData
                if (dataChanged)
                {
                    //Nếu check vào 2 tab tổng của KH, load lại list pallet và sl
                    if (tpPrepareInfo.SelectedIndex <= 1)
                    {
                        dataChanged = false;

                        #region Load lại data 2 table
                        ProcessDocumentBarcode(currentDocument);
                        #endregion
                    }
                }
                #endregion

                if (tpPrepareInfo.SelectedIndex <= 1)
                {
                    lblCurrentActionInfo.Text = "Mã:" + currentDocument;
                    this.Text = _userInfo;
                }
                else
                {
                    if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                    {
                        lblCurrentActionInfo.Text = _currentPallet.PalletCode + ": " + _palletInfo;
                        //this.Text = _palletInfo;
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        private void menuQRCode_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.BarcodeTrackingView())
                {
                    view.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuMovePallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveMaterialPalletView())
                {
                    view.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void ExportMaterialView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        private void dtgDocuments_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgDocuments[dtgDocuments.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
    }
}