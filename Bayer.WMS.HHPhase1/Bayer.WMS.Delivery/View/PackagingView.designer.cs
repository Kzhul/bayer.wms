﻿namespace Bayer.WMS.Handheld.Views
{
    partial class PackagingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsPackaging = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPackaging = new System.Windows.Forms.DataGrid();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniFeature = new System.Windows.Forms.MenuItem();
            this.mniFinish = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.lblCarton = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            this.lblPallet = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackaging)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgPackaging
            // 
            this.dtgPackaging.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPackaging.DataSource = this.bdsPackaging;
            this.dtgPackaging.Location = new System.Drawing.Point(0, 102);
            this.dtgPackaging.Name = "dtgPackaging";
            this.dtgPackaging.RowHeadersVisible = false;
            this.dtgPackaging.Size = new System.Drawing.Size(240, 128);
            this.dtgPackaging.TabIndex = 5;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 11;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(36, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(145, 21);
            this.txtBarcode.TabIndex = 10;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã lẻ để đóng gói";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniFeature);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniFeature
            // 
            this.mniFeature.MenuItems.Add(this.mniFinish);
            this.mniFeature.Text = "Chức năng";
            // 
            // mniFinish
            // 
            this.mniFinish.Text = "Kết thúc";
            this.mniFinish.Click += new System.EventHandler(this.mniFinish_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // lblCarton
            // 
            this.lblCarton.Location = new System.Drawing.Point(3, 68);
            this.lblCarton.Name = "lblCarton";
            this.lblCarton.Size = new System.Drawing.Size(237, 20);
            // 
            // lblQty
            // 
            this.lblQty.Location = new System.Drawing.Point(3, 88);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(237, 20);
            this.lblQty.Text = "SL: 0";
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(3, 48);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(237, 20);
            // 
            // lblCompany
            // 
            this.lblCompany.Location = new System.Drawing.Point(3, 27);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(237, 20);
            // 
            // PackagingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.dtgPackaging);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.lblPallet);
            this.Controls.Add(this.lblQty);
            this.Controls.Add(this.lblCarton);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "PackagingView";
            this.Text = "Đóng thùng lẻ";
            this.Load += new System.EventHandler(this.PackagingView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.PackagingView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackaging)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dtgPackaging;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.BindingSource bdsPackaging;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniFeature;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.MenuItem mniFinish;
        private System.Windows.Forms.Label lblCarton;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.Label lblCompany;
    }
}