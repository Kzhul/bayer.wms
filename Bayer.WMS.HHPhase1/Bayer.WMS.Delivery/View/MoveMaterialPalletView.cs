﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Threading;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Views
{
    public partial class MoveMaterialPalletView : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public string palletSource = string.Empty;
        public string palletDestination = string.Empty;
        private string _userInfo = "San hàng";
        public string _productLot = string.Empty;
        public int _currentQuantity = 0;
        public int _queryByNBR = 1;
        #endregion

        #region InitForm
        public MoveMaterialPalletView()
        {
            InitializeComponent();
            InitDataGridView();
            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã Pallet chứa hàng",
                "Quét mã Pallet trống",
                "Điền số lượng muốn chuyển và chọn vào số lô trên pallet chứa hàng để chuyển"
            };
            lblStepHints.Text = _stepHints[1];
            _userInfo = "San hàng: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            lblStepHints.Text = _stepHints[0];

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void MovePalletView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("VT", "LocationCode", 60, String.Empty, null));
            dtgPallet.TableStyles.Add(dtgStyle);

            var dtgStyleReady = new DataGridTableStyle { MappingName = "PalletReady" };
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("VT", "LocationCode", 60, String.Empty, null));
            dtgPalletDestination.TableStyles.Add(dtgStyleReady);
        }
        #endregion

        #region BarcodeReader
        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;
                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);
            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }
        #endregion

        #region ActionOnScreen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        private void dtgPallet_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (
                    !string.IsNullOrEmpty(palletSource)
                    && !string.IsNullOrEmpty(palletDestination)
                    )
                {
                    string productLot = dtgPallet[dtgPallet.CurrentRowIndex, 0].ToString();
                    int quantity = 0;
                    if (!string.IsNullOrEmpty(txtBarcode.Text))
                    {
                        try
                        {
                            quantity = Convert.ToInt32(txtBarcode.Text);
                        }
                        catch
                        {
                            //Do nothing here
                        }
                    }
                    else
                    {
                        quantity = Convert.ToInt32(dtgPallet[dtgPallet.CurrentRowIndex, 1].ToString());
                    }

                    if (quantity > 0)
                    {
                        //Move product
                        Pallet.MoveMaterial(palletSource, palletDestination, productLot, quantity);

                        //Reload 2 pallet
                        LoadPalletDestination(true, palletSource);
                        LoadPalletDestination(false, palletDestination);

                        lblStepHints.Text = string.Format("Đã san {0} của lô {1} sang {2}", quantity.ToString(), productLot, palletDestination);
                    }
                }
                else
                {
                    Utility.PlayErrorSound();
                    lblStepHints.Text = "Vui lòng quét mã pallet chứa hàng";
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }
        #endregion

        #region ProcessBarcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (
                    Utility.PalletBarcodeRegex.IsMatch(barcode)
                )
                {
                    if (string.IsNullOrEmpty(palletSource))
                    {
                        palletSource = barcode;
                        LoadPalletDestination(true, palletSource);
                        lblPallet.Text = palletSource;
                        lblStepHints.Text = _stepHints[1];
                    }
                    else
                    {
                        palletDestination = barcode;
                        LoadPalletDestination(false, palletDestination);
                        lblPallet.Text = palletSource + " -> " + palletDestination;
                        lblStepHints.Text = _stepHints[2];
                    }
                }
                else
                {
                    //Lấy mã lô
                    _productLot = Utility.GetProductLotFromString(barcode);
                    string _NBR = string.Empty;
                    try
                    {
                        if (barcode.Contains("-"))
                        {
                            _NBR = barcode.Split('-').Last();
                            if (_NBR.Length > 3)
                            {
                                _NBR = _NBR.Substring(_NBR.Length - 3, 3);
                            }
                        }
                    }
                    catch
                    {

                    }

                    //Nếu không lấy được, cảnh báo
                    if (string.IsNullOrEmpty(_productLot))
                    {
                        MessageBox.Show("Vui lòng quét mã lô có kèm số thứ tự");
                        return;
                    }

                    //Nếu chưa quét mã pallet souce
                    //Tìm pallet source dựa trên Product Lot NBR 
                    if (string.IsNullOrEmpty(palletSource))
                    {
                        if (_queryByNBR == 1)
                        {
                            if (string.IsNullOrEmpty(_NBR))
                            {
                                MessageBox.Show("Vui lòng quét mã lô có kèm số thứ tự");
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Vui lòng quét mã Pallet nguồn");
                            return;                        
                        }
                    }

                    //Lấy thông tin lô hiển thị lên
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    listParam.Add(new SqlParameter("PalletCode", palletSource));
                    listParam.Add(new SqlParameter("ProductLot", _productLot));
                    listParam.Add(new SqlParameter("NBR", _NBR));
                    DataTable dt = Utility.LoadDataFromStore("proc_PalletStatuses_SelectPalletFromProductLotNBR", listParam);
                    if (dt.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(_NBR))
                        {
                            palletSource = dt.Rows[0]["PalletCode"].ToString();
                            LoadPalletDestination(true, palletSource);
                            lblPallet.Text = palletSource;
                            lblStepHints.Text = _stepHints[1];
                        }

                        lblProductLot.Text = _productLot + "-" + _NBR;
                        lblProduct.Text = "Sản phẩm: " + dt.Rows[0]["ProductCode"].ToString() + "- " + dt.Rows[0]["ProductDescription"].ToString()
                                            + Environment.NewLine + "SL đang có: " + dt.Rows[0]["ProductQty"].ToString();
                        _currentQuantity = Convert.ToInt32(dt.Rows[0]["ProductQty"].ToString());
                        txtQuantity.Focus();
                        tabControl1.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;

                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void LoadPalletDestination(bool isPalletSource, string barcode)
        {
            try
            {
                var dt = Pallet.LoadPalletStatusesWithLocation(barcode, "P");
                if (isPalletSource)
                {
                    dt.TableName = "PalletSummary";
                    bdsPalletSummary.DataSource = dt;
                }
                else
                {
                    dt.TableName = "PalletReady";
                    bdsPalletDestination.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        private void mnuAnotherPallet_Click(object sender, EventArgs e)
        {
            ClearScreen();
        }

        private void mnuMoveAll_Click(object sender, EventArgs e)
        {
            try
            {
                Pallet.UpdatePallet_MoveProduct(palletDestination, palletSource, "P", "UpdatePallet_MoveProducts");

                #region Reload Data
                //Reload 2 pallet
                LoadPalletDestination(true, palletSource);
                LoadPalletDestination(false, palletDestination);

                lblStepHints.Text = string.Format("Đã san {0} sang {1}", palletSource, palletDestination);
                #endregion
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        private void mnuClearPallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ClearPalletView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ClearScreen();
                #endregion

                lblStepHints.Text = _stepHints[2];
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void ClearScreen()
        {
            bdsPalletSummary.DataSource = null;
            bdsPalletDestination.DataSource = null;
            palletDestination = string.Empty;
            palletSource = string.Empty;
            lblPallet.Text = string.Empty;
            lblStepHints.Text = "Quét mã Pallet chứa hàng";
        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            MoveMaterial();
        }

        private void txtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                try
                {
                    MoveMaterial();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {

                }
            }
        }

        private void MoveMaterial()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (string.IsNullOrEmpty(_productLot))
                {
                    MessageBox.Show("Vui lòng quét mã lô có kèm số thứ tự");
                    return;
                }

                int quantity = 0;
                if (!string.IsNullOrEmpty(txtQuantity.Text))
                {
                    try
                    {
                        quantity = Convert.ToInt32(txtQuantity.Text);
                    }
                    catch
                    {
                        //Do nothing here
                    }
                }

                if (quantity > _currentQuantity)
                {
                    MessageBox.Show("Không thể san hàng quá số lượng đang có");
                    return;
                }

                //Move product
                Pallet.MoveMaterial(palletSource, palletDestination, _productLot, quantity);

                //Reload 2 pallet
                LoadPalletDestination(true, palletSource);
                LoadPalletDestination(false, palletDestination);

                tabControl1.SelectedIndex = 2;
                lblStepHints.Text = string.Format("Đã san {0} của lô {1} sang {2}", quantity.ToString(), _productLot, palletDestination);

                //RESET DATA
                lblProductLot.Text = string.Empty;
                lblProduct.Text = string.Empty;
                //lblPallet.Text = string.Empty;
                txtQuantity.Text = string.Empty;
                _productLot = string.Empty;
                _currentQuantity = 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }
    }
}