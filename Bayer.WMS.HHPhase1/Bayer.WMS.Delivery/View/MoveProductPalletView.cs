﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Threading;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Views
{
    public partial class MoveProductPallet : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        private bool _isProcess = false;
        private Queue<string> _queue;

        private Pallet _palletSource;
        private Pallet _palletDestination;
        public string palletSource = string.Empty;
        public string palletDestination = string.Empty;
        private string _userInfo = "San hàng";
        #endregion

        #region InitForm
        public MoveProductPallet()
        {
            InitializeComponent();
            InitDataGridView();
            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã Pallet chứa hàng",
                "Quét mã Pallet trống",
                "Quét mã Lô/Thùng/Hàng Lẻ để san hàng",
            };
            lblStepHints.Text = _stepHints[1];
            _userInfo = "San hàng: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            lblStepHints.Text = _stepHints[0];

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void MoveProductPallet_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("VT", "LocationCode", 60, String.Empty, null));
            dtgPallet.TableStyles.Add(dtgStyle);

            var dtgStyleReady = new DataGridTableStyle { MappingName = "PalletReady" };
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("VT", "LocationCode", 60, String.Empty, null));
            dtgPalletDestination.TableStyles.Add(dtgStyleReady);
        }
        #endregion

        #region BarcodeReader
        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;
                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);
            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }
        #endregion

        #region ActionOnScreen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        private void dtgPallet_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string productLot = dtgPallet[dtgPallet.CurrentRowIndex, 0].ToString();
                ProcessBarcode(productLot);
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }
        #endregion

        #region ProcessBarcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (
                    Utility.PalletBarcodeRegex.IsMatch(barcode)
                )
                {
                    //Nếu chưa quét mã pallet nào
                    if (string.IsNullOrEmpty(palletSource) && string.IsNullOrEmpty(palletDestination))
                    {
                        palletSource = barcode;
                        LoadPalletDestination(true, palletSource, string.Empty);
                    }
                    //Nếu đã quét pallet nguồn, chưa quét pallet đích
                    else if (!string.IsNullOrEmpty(palletSource) && string.IsNullOrEmpty(palletDestination))
                    {
                        palletDestination = barcode;
                        LoadPalletDestination(false, palletDestination, string.Empty);
                    }
                    //Nếu đã quét cả 2 và quét tiếp pallet khác
                    //tạm thời ngưng xử lý case này
                    else if (!string.IsNullOrEmpty(palletSource) && !string.IsNullOrEmpty(palletDestination))
                    {
                        Utility.PlayErrorSound();
                        MessageBox.Show("Vui lòng bấm nút làm tiếp pallet khác");

                        //palletSource = barcode;
                        ////Clear pallet destination
                        //palletDestination = string.Empty;
                        //LoadPalletDestination(true, palletSource);
                    }
                }
                else if (Utility.ProductLotRegex.IsMatch(barcode))
                {
                    ProcessProductLot(barcode);
                }
                else
                {
                    //Utility.WriteLog("ProcessProductBarcode" + barcode);
                    ProcessProductBarcode(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;

                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void LoadPalletDestination(bool isPalletSource, string palletBarCode, string productBarcode)
        {
            try
            {
                //if (isPalletSource)
                //{
                //    string encryptedBarcode = barcode;
                //    string type;
                //    barcode = Barcode.Process(encryptedBarcode, true, true, true, out type);
                //    _palletSource = Pallet.Load(barcode);
                //    palletSource = _palletSource.PalletCode;
                //    barcode = _palletSource.PalletCode;
                //}

                #region ThungBaoXoCan
                try
                {
                    List<SqlParameter> listParam3 = new List<SqlParameter>();
                    listParam3.Add(new SqlParameter("Barcode", palletBarCode));
                    DataTable c = Utility.LoadDataFromStore("proc_Pallets_Select_SumByPackingType", listParam3);
                    lblStepHints.Text = palletBarCode + ": " + c.Rows[0]["StrQuantity"].ToString();

                    //Nếu quét pallet và pallet này đang soạn hàng
                    if (c.Rows[0]["CompanyCode"].ToString() != string.Empty)
                    {
                        Utility.PlayErrorSound();
                        MessageBox.Show(
                            "Pallet không hợp lệ" + Environment.NewLine
                            + palletBarCode + Environment.NewLine + "Trạng Thái: " + c.Rows[0]["StrStatus"].ToString()
                            + Environment.NewLine + "KH: " + c.Rows[0]["CompanyCode"].ToString() + " " + c.Rows[0]["CompanyName"].ToString()
                            );
                        //return;
                    }
                }
                catch
                {

                }
                #endregion

                DataTable dt = new DataTable();
                if (string.IsNullOrEmpty(productBarcode))
                {
                    dt = Pallet.LoadPalletStatusesWithLocation(palletBarCode, "P");
                }
                else
                {
                    dt = Pallet.LoadPalletStatusesWithLocation(palletBarCode, "P", productBarcode);
                }
                if (isPalletSource)
                {
                    //palletSource = barcode;
                    //_palletSource = Pallet.Load(palletSource);
                    dt.TableName = "PalletSummary";
                    bdsPalletSummary.DataSource = dt;
                    lblPallet.Text = palletSource;
                    lblStepHints.Text = _stepHints[1];
                }
                else
                {
                    //palletDestination = barcode;
                    //_palletDestination = Pallet.Load(palletDestination);
                    dt.TableName = "PalletReady";
                    bdsPalletDestination.DataSource = dt;
                    lblPallet.Text = palletSource + " -> " + palletDestination;
                    lblStepHints.Text = _stepHints[2];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ProcessProductLot(string productLot)
        {
            try
            {
                if (
                    !string.IsNullOrEmpty(palletSource)
                    && !string.IsNullOrEmpty(palletDestination)
                )
                {
                    //Move product
                    Pallet.UpdatePallet_MoveProductLot(palletSource, palletDestination, productLot, "L", "UpdatePallet_MoveProductLot");

                    //Reload 2 pallet
                    LoadPalletDestination(true, palletSource, string.Empty);
                    LoadPalletDestination(false, palletDestination, string.Empty);

                    lblStepHints.Text = string.Format("Đã san lô {0} sang {1}", productLot, palletDestination);
                }
                else
                {
                    Utility.PlayErrorSound();
                    lblStepHints.Text = "Vui lòng quét mã pallet";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ProcessProductBarcode(string barcode)
        {
            try
            {
                #region Move Product
                if (
                     !string.IsNullOrEmpty(palletSource)
                     && !string.IsNullOrEmpty(palletDestination)
                )
                {
                    #region InsertData
                    if (
                         Utility.CartonBarcode3Regex.IsMatch(barcode)
                        || Utility.CartonBarcode2Regex.IsMatch(barcode)
                        || Utility.CartonBarcodeRegex.IsMatch(barcode)
                        )
                    {
                        try
                        {
                            Pallet.UpdatePallet_MoveProduct(palletDestination, barcode, "C", "UpdatePallet_MoveProducts");
                            //Utility.WriteLog("UpdatePallet_MoveProduct_VerifyPalletSource.CartonBarcode" + barcode);
                            //Pallet.UpdatePallet_MoveProduct_VerifyPalletSource(palletSource, palletDestination, barcode, "C", "UpdatePallet_MoveProduct_VerifyPalletSource");
                        }
                        catch
                        {
                            throw new Exception("Mã không hợp lệ");
                        }
                    }
                    else
                    {
                        try
                        {
                            string encryptedBarcode = barcode;
                            string type;
                            barcode = Barcode.Process(encryptedBarcode, false, false, true, out type);
                            Pallet.UpdatePallet_MoveProduct(palletDestination, barcode, "E", "UpdatePallet_MoveProducts");

                            //Utility.WriteLog("UpdatePallet_MoveProduct_VerifyPalletSource.ProductBarcode" + barcode);
                            //Pallet.UpdatePallet_MoveProduct_VerifyPalletSource(palletSource, palletDestination, barcode, "E", "UpdatePallet_MoveProduct_VerifyPalletSource");
                        }
                        catch
                        {
                            throw new Exception("Mã không hợp lệ");
                        }
                    }
                    #endregion

                    #region Reload Data
                    //Reload 2 pallet
                    LoadPalletDestination(true, palletSource, string.Empty);
                    LoadPalletDestination(false, palletDestination, string.Empty);

                    lblStepHints.Text = string.Format("Đã san {0} sang {1}", barcode, palletDestination);
                    #endregion
                }
                else
                {
                    //Nếu chưa quét mã pallet nào
                    if (string.IsNullOrEmpty(palletSource))
                    {
                        string productBarcode = barcode;
                        string type;
                        barcode = Barcode.Process(barcode, true, true, true, out type);
                        DataTable dt = Pallet.LoadRequirePalletStatusLocation(barcode, type);
                        barcode = dt.Rows[0]["PalletCode"].ToString();
                        palletSource = barcode;
                        LoadPalletDestination(true, palletSource, productBarcode);
                    }
                    else
                    {
                        Utility.PlayErrorSound();
                        lblStepHints.Text = "Vui lòng quét mã pallet";
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }
        #endregion

        private void menuItem1_Click(object sender, EventArgs e)
        {
            ClearScreen();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                Pallet.UpdatePallet_MoveProduct(palletDestination, palletSource, "P", "UpdatePallet_MoveProducts");

                #region Reload Data
                //Reload 2 pallet
                LoadPalletDestination(true, palletSource, string.Empty);
                LoadPalletDestination(false, palletDestination, string.Empty);

                lblStepHints.Text = string.Format("Đã san {0} sang {1}", palletSource, palletDestination);
                #endregion
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void ClearScreen()
        {
            bdsPalletSummary.DataSource = null;
            bdsPalletDestination.DataSource = null;
            palletDestination = string.Empty;
            palletSource = string.Empty;
            lblPallet.Text = string.Empty;
            lblStepHints.Text = "Quét mã Pallet chứa hàng";
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ClearPalletView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ClearScreen();
                #endregion

                lblStepHints.Text = _stepHints[2];
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}