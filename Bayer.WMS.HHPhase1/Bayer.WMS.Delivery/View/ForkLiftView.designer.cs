﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ForkLiftView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPallet = new System.Windows.Forms.Label();
            this.bdsPalletSummary = new System.Windows.Forms.BindingSource(this.components);
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.lblProductLot = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnExportProductPallet = new System.Windows.Forms.Button();
            this.btnMoveMaterial = new System.Windows.Forms.Button();
            this.btnDropMaterial = new System.Windows.Forms.Button();
            this.btnMaterialForLocation = new System.Windows.Forms.Button();
            this.btnProductForLocation = new System.Windows.Forms.Button();
            this.btnProductWaiting = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bdsPalletWaiting = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPalletWaiting = new System.Windows.Forms.DataGrid();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgPallet = new System.Windows.Forms.DataGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.bdsPalletDone = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPalletDone = new System.Windows.Forms.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletWaiting)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDone)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 21);
            this.label1.Text = "Pallet:";
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(49, 27);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(188, 21);
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã Pallet để bắt đầu\r\nQuét mã vị trí để xác nhận";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 21);
            this.label2.Text = "Vị trí:";
            // 
            // lblProductLot
            // 
            this.lblProductLot.Location = new System.Drawing.Point(49, 48);
            this.lblProductLot.Name = "lblProductLot";
            this.lblProductLot.Size = new System.Drawing.Size(188, 21);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(0, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 167);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(this.btnExportProductPallet);
            this.tabPage3.Controls.Add(this.btnMoveMaterial);
            this.tabPage3.Controls.Add(this.btnDropMaterial);
            this.tabPage3.Controls.Add(this.btnMaterialForLocation);
            this.tabPage3.Controls.Add(this.btnProductForLocation);
            this.tabPage3.Controls.Add(this.btnProductWaiting);
            this.tabPage3.Location = new System.Drawing.Point(0, 0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(240, 144);
            this.tabPage3.Text = "1.Công việc";
            // 
            // btnExportProductPallet
            // 
            this.btnExportProductPallet.Location = new System.Drawing.Point(7, 9);
            this.btnExportProductPallet.Name = "btnExportProductPallet";
            this.btnExportProductPallet.Size = new System.Drawing.Size(219, 25);
            this.btnExportProductPallet.TabIndex = 5;
            this.btnExportProductPallet.Text = "Rớt hàng thành phẩm";
            this.btnExportProductPallet.Click += new System.EventHandler(this.btnExportProductPallet_Click);
            // 
            // btnMoveMaterial
            // 
            this.btnMoveMaterial.Location = new System.Drawing.Point(7, 163);
            this.btnMoveMaterial.Name = "btnMoveMaterial";
            this.btnMoveMaterial.Size = new System.Drawing.Size(219, 25);
            this.btnMoveMaterial.TabIndex = 4;
            this.btnMoveMaterial.Text = "Chuyển NL->SX";
            this.btnMoveMaterial.Click += new System.EventHandler(this.btnMoveMaterial_Click);
            // 
            // btnDropMaterial
            // 
            this.btnDropMaterial.Location = new System.Drawing.Point(7, 133);
            this.btnDropMaterial.Name = "btnDropMaterial";
            this.btnDropMaterial.Size = new System.Drawing.Size(219, 25);
            this.btnDropMaterial.TabIndex = 3;
            this.btnDropMaterial.Text = "Rớt hàng NL";
            this.btnDropMaterial.Click += new System.EventHandler(this.btnDropMaterial_Click);
            // 
            // btnMaterialForLocation
            // 
            this.btnMaterialForLocation.Location = new System.Drawing.Point(7, 102);
            this.btnMaterialForLocation.Name = "btnMaterialForLocation";
            this.btnMaterialForLocation.Size = new System.Drawing.Size(219, 25);
            this.btnMaterialForLocation.TabIndex = 2;
            this.btnMaterialForLocation.Text = "Đưa NL lên kệ";
            this.btnMaterialForLocation.Click += new System.EventHandler(this.btnMaterialForLocation_Click);
            // 
            // btnProductForLocation
            // 
            this.btnProductForLocation.Location = new System.Drawing.Point(7, 71);
            this.btnProductForLocation.Name = "btnProductForLocation";
            this.btnProductForLocation.Size = new System.Drawing.Size(219, 25);
            this.btnProductForLocation.TabIndex = 1;
            this.btnProductForLocation.Text = "Đưa TP lên kệ";
            this.btnProductForLocation.Click += new System.EventHandler(this.btnProductForLocation_Click);
            // 
            // btnProductWaiting
            // 
            this.btnProductWaiting.Location = new System.Drawing.Point(7, 40);
            this.btnProductWaiting.Name = "btnProductWaiting";
            this.btnProductWaiting.Size = new System.Drawing.Size(219, 25);
            this.btnProductWaiting.TabIndex = 0;
            this.btnProductWaiting.Text = "TP SX->Kho";
            this.btnProductWaiting.Click += new System.EventHandler(this.btnProductWaiting_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgPalletWaiting);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(232, 141);
            this.tabPage2.Text = "2.DS Pallet chờ";
            // 
            // dtgPalletWaiting
            // 
            this.dtgPalletWaiting.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPalletWaiting.DataSource = this.bdsPalletWaiting;
            this.dtgPalletWaiting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPalletWaiting.Location = new System.Drawing.Point(0, 0);
            this.dtgPalletWaiting.Name = "dtgPalletWaiting";
            this.dtgPalletWaiting.RowHeadersVisible = false;
            this.dtgPalletWaiting.Size = new System.Drawing.Size(232, 141);
            this.dtgPalletWaiting.TabIndex = 1;
            this.dtgPalletWaiting.DoubleClick += new System.EventHandler(this.dtgPalletWaiting_DoubleClick);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgPallet);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(232, 141);
            this.tabPage1.Text = "3.Hàng";
            // 
            // dtgPallet
            // 
            this.dtgPallet.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPallet.DataSource = this.bdsPalletSummary;
            this.dtgPallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallet.Location = new System.Drawing.Point(0, 0);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.RowHeadersVisible = false;
            this.dtgPallet.Size = new System.Drawing.Size(232, 141);
            this.dtgPallet.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dtgPalletDone);
            this.tabPage4.Location = new System.Drawing.Point(0, 0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(240, 144);
            this.tabPage4.Text = "4.DS đã làm";
            // 
            // bdsPalletDone
            // 
            this.bdsPalletDone.Position = 0;
            // 
            // dtgPalletDone
            // 
            this.dtgPalletDone.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPalletDone.DataSource = this.bdsPalletDone;
            this.dtgPalletDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPalletDone.Location = new System.Drawing.Point(0, 0);
            this.dtgPalletDone.Name = "dtgPalletDone";
            this.dtgPalletDone.RowHeadersVisible = false;
            this.dtgPalletDone.Size = new System.Drawing.Size(240, 144);
            this.dtgPalletDone.TabIndex = 3;
            // 
            // ForkLiftView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblProductLot);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.lblPallet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "ForkLiftView";
            this.Text = "Xe nâng";
            this.Load += new System.EventHandler(this.SetupLocationView_Load);
            this.Closed += new System.EventHandler(this.SetupLocationView_Closed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.SetupLocationView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletWaiting)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDone)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsPalletSummary;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblProductLot;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGrid dtgPallet;
        private System.Windows.Forms.DataGrid dtgPalletWaiting;
        private System.Windows.Forms.BindingSource bdsPalletWaiting;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnProductWaiting;
        private System.Windows.Forms.Button btnProductForLocation;
        private System.Windows.Forms.Button btnMaterialForLocation;
        private System.Windows.Forms.Button btnDropMaterial;
        private System.Windows.Forms.Button btnMoveMaterial;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGrid dtgPalletDone;
        private System.Windows.Forms.BindingSource bdsPalletDone;
        private System.Windows.Forms.Button btnExportProductPallet;
    }
}