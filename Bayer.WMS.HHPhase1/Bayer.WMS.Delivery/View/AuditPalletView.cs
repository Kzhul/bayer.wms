﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Threading;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Bayer.WMS.Handheld.Views
{
    public partial class AuditPalletView : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public bool isPalletScanned = false;
        public bool isLocationScanned = false;
        public string _palletCode = string.Empty;
        public string _productLot = string.Empty;
        public string _productCode = string.Empty;
        public int _productID = 0;
        public int _quantity = 0;
        public int _oldQuantity = 0;
        public string _reasonID = string.Empty;
        private string _userInfo = "Kiểm kê";
        private string _locationCode = string.Empty;
        private string _locationName = string.Empty;
        private string _locationSuggestionCode = string.Empty;
        private string _locationSuggestionName = string.Empty;
        #endregion

        #region InitForm
        public AuditPalletView()
        {
            InitializeComponent();
            InitDataGridView();
            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã Pallet",
                "Quét mã Vị trí",
                "Quét mã Lô",
                "Điền số lượng đúng và chọn lý do. Bấm xác nhận để cập nhật"
            };
            lblStepHints.Text = _stepHints[1];
            _userInfo = "Kiểm kê: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            lblStepHints.Text = _stepHints[0];

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void AuditPalletView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "PalletQuantity", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("VT", "LocationCode", 60, String.Empty, null));
            dtgPallet.TableStyles.Add(dtgStyle);

            DataTable dt = Pallet.LoadReferences("WarehouseAuditReason");
            cmbReason.DataSource = dt;
            cmbReason.ValueMember = "RefValue";
            cmbReason.DisplayMember = "Description";
        }
        #endregion

        #region BarcodeReader
        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;
                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);
            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }
        #endregion

        #region ActionOnScreen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                QueueProcess(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        private void dtgPallet_DoubleClick(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }
        #endregion

        #region ProcessBarcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                {
                    ProcessPalletBarcode(barcode);
                }
                else if (Utility.LocationCodeRegex.IsMatch(barcode))
                {
                    if (!isPalletScanned)
                    {
                        Utility.PlayErrorSound();
                        lblStepHints.Text = "Vui lòng quét mã pallet trước.";
                        isPalletScanned = false;
                        isLocationScanned = false;
                        return;
                    }

                    ProcessLocationBarcode(barcode);
                }
                else
                {
                    if (!isPalletScanned)
                    {
                        Utility.PlayErrorSound();
                        lblStepHints.Text = "Vui lòng quét mã pallet trước.";
                        isPalletScanned = false;
                        isLocationScanned = false;
                        return;
                    }

                    _productCode = string.Empty;
                    _productLot = string.Empty;
                    //_productLot = barcode;
                    _productCode = barcode;
                    Match match = Regex.Match(barcode, @"\d{6}\w\d{2}");
                    if (match.Success)
                    {
                        _productLot = match.Value;
                    }
                    ProcessProductLotBarcode();
                }
                
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;

                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void ProcessLocationBarcode(string barcode)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("LocationCode", barcode));

                DataTable dt = Utility.LoadDataFromStore("proc_ZoneLocations_Select", listParam);
                if (dt.Rows.Count == 0)
                {
                    throw new Exception("Mã vị trí không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }
                else
                {
                    _locationCode = barcode;
                    _locationName = dt.Rows[0]["LocationName"].ToString();
                    //lblProductLot.Text = _locationName;
                    isLocationScanned = true;

                    Cursor.Current = Cursors.WaitCursor;
                    if (Pallet.SetLocation(_palletCode, _locationCode, string.Empty))
                    {
                        lblStepHints.Text = "Đã đưa pallet " + _palletCode + " lên kệ " + _locationName + ".";
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            _palletCode = barcode;

            isLocationScanned = false;
            _productLot = string.Empty;
            _productCode = string.Empty;
            _productID = 0;
            _quantity = 0;
            _oldQuantity = 0;
            _userInfo = "Kiểm kê";
            _locationCode = string.Empty;
            _locationName = string.Empty;
            lblProductLot.Text = "Mã lô:";
            lblProduct.Text = "Sản phẩm: ";
            txtQuantity.Text = string.Empty;

            List<SqlParameter> listParam = new List<SqlParameter>();
            listParam.Add(new SqlParameter("PalletCode", _palletCode));
            listParam.Add(new SqlParameter("ProductLot", string.Empty));
            bdsPallet.DataSource = null;
            DataTable dt = Utility.LoadDataFromStore("proc_PalletStatuses_AuditSelect", listParam);
            dt.TableName = "PalletSummary";
            bdsPallet.DataSource = dt;

            lblCurrentTopic.Text = _palletCode + ";" + dt.Rows[0]["LocationName"].ToString();
            lblStepHints.Text = _stepHints[1];
            isPalletScanned = true;
            tabControl1.SelectedIndex = 1;
        }


        public void ProcessProductLotBarcode()
        {
            if (!string.IsNullOrEmpty(_palletCode))
            {
                string strScanned = _productCode;
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("PalletCode", _palletCode));
                listParam.Add(new SqlParameter("ProductLot", _productLot));
                listParam.Add(new SqlParameter("ProductCode", _productCode));
                DataTable dt = Utility.LoadDataFromStore("proc_PalletStatuses_AuditSelect2", listParam);

                _oldQuantity = Convert.ToInt32(dt.Rows[0]["PalletQuantity"].ToString());
                _productID = Convert.ToInt32(dt.Rows[0]["ProductID"].ToString());
                _productCode = dt.Rows[0]["ProductCode"].ToString();
                _productLot = dt.Rows[0]["ProductLot"].ToString();
                //strScanned = strScanned.Replace(_productCode, "");
                //_productLot = strScanned.Substring(2, 9);
                //DateTime expiryDate = dt.Rows[0]["ExpiredDate"];

                lblProductLot.Text = "Mã lô:" + _productLot;
                lblProduct.Text = "Sản phẩm: " + dt.Rows[0]["ProductCode"].ToString() + "- " + dt.Rows[0]["ProductName"].ToString() + ". QC: " + dt.Rows[0]["PackageSize"].ToString() + dt.Rows[0]["UOM"].ToString() + " . HSD: " + dt.Rows[0]["ExpiredDate"].ToString() + Environment.NewLine + "SL: " + _oldQuantity.ToString() + "";
                if (_oldQuantity == 0)
                {
                    txtQuantity.Text = dt.Rows[0]["PackageSize"].ToString();
                }
                else
                {
                    txtQuantity.Text = _oldQuantity.ToString();
                }
                lblStepHints.Text = _stepHints[2];
                txtQuantity.Focus();
                tabControl1.SelectedIndex = 0;
            }
            else
            {
                Utility.PlayErrorSound();
                lblStepHints.Text = "Vui lòng quét mã pallet";
            }
        }
        #endregion

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(_productLot))
                {
                    //Get value
                    _quantity = Convert.ToInt32(txtQuantity.Text);
                    _reasonID = cmbReason.SelectedValue.ToString();
                    
                    if (string.IsNullOrEmpty(_productLot))
                    {
                        //Update
                        List<SqlParameter> listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("PalletCode", _palletCode));
                        listParam.Add(new SqlParameter("UserID", Utility.UserID));
                        DataTable dt = Utility.LoadDataFromStore("proc_PalletStatuses_Update_AuditMaterial2", listParam);
                    }
                    else
                    {
                        //Update
                        List<SqlParameter> listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("PalletCode", _palletCode));
                        listParam.Add(new SqlParameter("ProductLot", _productLot));
                        listParam.Add(new SqlParameter("ProductID", _productID));
                        listParam.Add(new SqlParameter("Quantity", _quantity));
                        listParam.Add(new SqlParameter("OldQuantity", _oldQuantity));
                        listParam.Add(new SqlParameter("ReasonID", _reasonID));
                        listParam.Add(new SqlParameter("UserID", Utility.UserID));
                        DataTable dt = Utility.LoadDataFromStore("proc_PalletStatuses_Update_AuditMaterial", listParam);
                    }

                    //Reload Data
                    ProcessPalletBarcode(_palletCode);
                }
                else
                {
                    Utility.PlayErrorSound();
                    lblStepHints.Text = "Vui lòng quét mã lô";
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        private void cmbReason_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}