﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Views
{
    public partial class WarehouseManagerView : Form
    {
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        private string _currentWork = string.Empty;
        public bool isPalletScanned = false;
        private string _userInfo = "Thủ kho";

        public WarehouseManagerView()
        {
            InitializeComponent();
            InitDataGridView();

            _stepHints = new List<string> 
            { 
                "Quét mã Pallet",
                "Bấm xác nhận để xác nhận đúng số lượng, số lô"
            };
            lblStepHints.Text = _stepHints[0];
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);

            _userInfo = "Thủ kho: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            mniConfirm.Enabled = false;
        }

        public void ReceiveProductConfirmView_Closed(object sender, EventArgs e)
        {
            lblStepHints.Text = _stepHints[0];
        }

        public void ReceiveProductConfirmView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (_currentWork == "MP")
                {
                    if (Pallet.ConfirmReceiveProduct(lblPallet.Text))
                    {
                        //Play sound set vị trí thành công
                        lblStepHints.Text = "Đã xác nhận pallet " + lblPallet.Text + ". Quét pallet để làm tiếp.";
                        bdsPalletSummary.DataSource = null;
                        isPalletScanned = false;
                        lblPallet.Text = string.Empty;
                    }
                }
                else if (_currentWork == "MM")
                {
                    if (Pallet.ConfirmMoveMaterial(lblPallet.Text))
                    {
                        //Play sound set vị trí thành công
                        lblStepHints.Text = "Đã xác nhận chuyển pallet " + lblPallet.Text + " xuống SX. Quét pallet để làm tiếp.";
                        bdsPalletSummary.DataSource = null;
                        isPalletScanned = false;
                        lblPallet.Text = string.Empty;
                    }
                }
                mniConfirm.Enabled = false;
                LoadPalletWaiting();
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 80, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng", "CartonQuantity", 40, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 40, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 20, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, string.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgPallet.TableStyles.Add(dtgStyle);

            var dtgStyleReady = new DataGridTableStyle { MappingName = "PalletReady" };
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("#", "IDS", 25, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));            
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Quy Cách", "PackageSize", 60, "N0", null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, string.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("SL Thùng", "CartonQuantity", 60, "N0", null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 60, "N0", null));
            dtgPalletWaiting.TableStyles.Add(dtgStyleReady);

            var dtgStyleDone = new DataGridTableStyle { MappingName = "PalletDone" };
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("#", "IDS", 25, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Quy Cách", "PackageSize", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, string.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("SL Thùng", "CartonQuantity", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Thời Gian", "StrTime", 100, string.Empty, null));
            dtgPalletDone.TableStyles.Add(dtgStyleDone);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else 
                //if (
                //    Utility.PalletBarcodeRegex.IsMatch(barcode)
                //    || Utility.CartonBarcode3Regex.IsMatch(barcode)
                //    || Utility.CartonBarcode2Regex.IsMatch(barcode)
                //    || Utility.CartonBarcodeRegex.IsMatch(barcode)
                //)
                {
                    string type = string.Empty;
                    bdsPalletSummary.DataSource = null;
                    barcode = Barcode.Process(barcode, true, true, true, out type);
                    DataTable dt = Pallet.LoadRequirePalletStatusLocation(barcode, type);
                    dt.TableName = "PalletSummary";
                    bdsPalletSummary.DataSource = dt;
                    lblPallet.Text = dt.Rows[0]["PalletCode"].ToString();
                    
                    //Nếu đang là nhập hàng thành phẩm
                    if (_currentWork == "MP")
                    {
                        //Nếu đã nhận
                        if (dt.Rows[0]["WarehouseKeeper"].ToString() != "0")
                        {
                            MessageBox.Show(barcode + Environment.NewLine + "Trạng Thái: " + dt.Rows[0]["StrStatus"].ToString() + Environment.NewLine + "Bạn đã xác nhận pallet này.");
                        }
                        else
                        {
                            lblStepHints.Text = _stepHints[1];
                            isPalletScanned = true;
                            mniConfirm.Enabled = true;
                            tabControl1.SelectedIndex = 2;
                        }
                    }
                    else
                    {
                        lblStepHints.Text = _stepHints[1];
                        isPalletScanned = true;
                        mniConfirm.Enabled = true;
                        tabControl1.SelectedIndex = 2;
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;
            }
        }

        private void LoadPalletWaiting()
        {
            try
            {
                if (_currentWork == "MP")
                {
                    bdsPalletWaiting.DataSource = null;
                    DataTable dt = Pallet.LoadPalletProductWaitingForConfirm();
                    dt.TableName = "PalletReady";
                    bdsPalletWaiting.DataSource = dt;
                    //lblStepHints.Text = _stepHints[0];
                    tabControl1.SelectedIndex = 1;

                    bdsPalletDone.DataSource = null;
                    DataTable dt2 = Pallet.LoadPalletDoneByUser(_currentWork);
                    dt2.TableName = "PalletDone";
                    bdsPalletDone.DataSource = dt2;
                }
                else if (_currentWork == "MM")
                {
                    bdsPalletWaiting.DataSource = null;
                    DataTable dt = Pallet.LoadPalletMaterialWaitingForConfirm();
                    dt.TableName = "PalletReady";
                    bdsPalletWaiting.DataSource = dt;
                    //lblStepHints.Text = _stepHints[0];
                    tabControl1.SelectedIndex = 1;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void btnProductWaiting_Click(object sender, EventArgs e)
        {
            _currentWork = "MP";
            LoadPalletWaiting();
        }

        private void dtgPalletWaiting_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgPalletWaiting[dtgPalletWaiting.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void btnConfirmMoveMaterial_Click(object sender, EventArgs e)
        {
            //___OLD PROCESS
            //_currentWork = "MM";
            //LoadPalletWaiting();

            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ExportMaterialView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnMaterialAudit_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.AuditPalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnStockReturn_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.StockReturnMaterialView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnProductReturn_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ProductReturnView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnExportProduct_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ExportView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnMaterialMove_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveMaterialPalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnReceiveMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ReceiveMaterialPalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnVeirifyProductPallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.AuditProductPalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnMoveProductPallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveProductPallet())
                {
                    view.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void btnClearPallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ClearPalletView())
                {
                    view.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}