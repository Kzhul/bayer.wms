﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    public partial class MergePalletView : Form
    {
        private int _step = 0;
        private string _status;
        private string _referenceNbr;
        private BarcodeReader _barcodeReader;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public MergePalletView()
        {
            InitializeComponent();

            _queue = new Queue<string>();
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        private void MergePalletView_Load(object sender, EventArgs e)
        {
            lblStepHints.Text = "Quét mã pallet muốn dồn";
            mniConfirm.Enabled = false;
        }

        private void MergePalletView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        private void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Pallet.Merge(lblPallet1.Text, lblPallet2.Text);
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                string type;
                Barcode.Process(barcode, true, false, false, out type);

                if (_step == 0)
                {
                    var dt = Pallet.LoadRequirePalletStatusGroupByBarcode(barcode);
                    _referenceNbr = dt.Rows[0]["ReferenceNbr"].ToString();
                    _status = dt.Rows[0]["Status"].ToString();

                    var rows = dt.Select("PackingType = 'C' AND OddQuantity = 0");
                    lblCarton.Text = rows.Length.ToString();

                    rows = dt.Select("PackingType = 'S'");
                    lblShove.Text = rows.Length.ToString();

                    rows = dt.Select("PackingType = 'A'");
                    lblCan.Text = rows.Length.ToString();

                    rows = dt.Select("PackingType = 'B'");
                    lblBag.Text = rows.Length.ToString();

                    rows = dt.Select("PackingType = 'C' AND OddQuantity > 0");
                    lblEach.Text = rows.Length.ToString();

                    _step++;
                    lblPallet1.Text = dt.Rows[0]["PalletCode"].ToString();
                    lblStepHints.Text = "Quét mã pallet được dồn";
                }
                else
                {
                    var pallet = Pallet.Load(barcode);

                    //SyVN Comment at 2017-10-03
                    //if (pallet.ReferenceNbr != _referenceNbr)
                    //    throw new Exception("Không thể dồn 2 pallet khác số phiếu với nhau.");

                    mniConfirm.Enabled = true;
                    lblPallet2.Text = pallet.PalletCode;
                    lblStepHints.Text = "Xác nhận dồn pallet";
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                Utility.WriteLog(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }
    }
}