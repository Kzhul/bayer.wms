﻿namespace Bayer.WMS.Handheld.Views
{
    partial class MoveMaterialPalletView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblPallet = new System.Windows.Forms.Label();
            this.bdsPalletSummary = new System.Windows.Forms.BindingSource(this.components);
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mnuAnotherPallet = new System.Windows.Forms.MenuItem();
            this.mnuMoveAll = new System.Windows.Forms.MenuItem();
            this.mnuClearPallet = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgPallet = new System.Windows.Forms.DataGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bdsPalletDestination = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPalletDestination = new System.Windows.Forms.DataGrid();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.lblProduct = new System.Windows.Forms.Label();
            this.lblProductLot = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.btnMove = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDestination)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 14;
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(3, 27);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(234, 21);
            // 
            // bdsPalletSummary
            // 
            this.bdsPalletSummary.Position = 0;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 221);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 47);
            this.lblStepHints.Text = "Quét mã Pallet để bắt đầu\r\nQuét mã vị trí để xác nhận";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.MenuItems.Add(this.mnuAnotherPallet);
            this.mniConfirm.MenuItems.Add(this.mnuMoveAll);
            this.mniConfirm.MenuItems.Add(this.mnuClearPallet);
            this.mniConfirm.Text = "Chức năng";
            // 
            // mnuAnotherPallet
            // 
            this.mnuAnotherPallet.Text = "1. Làm pallet khác";
            this.mnuAnotherPallet.Click += new System.EventHandler(this.mnuAnotherPallet_Click);
            // 
            // mnuMoveAll
            // 
            this.mnuMoveAll.Text = "2. San hết pallet chứa hàng";
            this.mnuMoveAll.Click += new System.EventHandler(this.mnuMoveAll_Click);
            // 
            // mnuClearPallet
            // 
            this.mnuClearPallet.Text = "3. Clear pallet";
            this.mnuClearPallet.Click += new System.EventHandler(this.mnuClearPallet_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(0, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 167);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgPallet);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(240, 144);
            this.tabPage1.Text = "1.Pallet chứa hàng";
            // 
            // dtgPallet
            // 
            this.dtgPallet.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPallet.DataSource = this.bdsPalletSummary;
            this.dtgPallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallet.Location = new System.Drawing.Point(0, 0);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.RowHeadersVisible = false;
            this.dtgPallet.Size = new System.Drawing.Size(240, 144);
            this.dtgPallet.TabIndex = 1;
            this.dtgPallet.DoubleClick += new System.EventHandler(this.dtgPallet_DoubleClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgPalletDestination);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(240, 144);
            this.tabPage2.Text = "2.Pallet trống";
            // 
            // dtgPalletDestination
            // 
            this.dtgPalletDestination.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPalletDestination.DataSource = this.bdsPalletDestination;
            this.dtgPalletDestination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPalletDestination.Location = new System.Drawing.Point(0, 0);
            this.dtgPalletDestination.Name = "dtgPalletDestination";
            this.dtgPalletDestination.RowHeadersVisible = false;
            this.dtgPalletDestination.Size = new System.Drawing.Size(240, 144);
            this.dtgPalletDestination.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnMove);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.txtQuantity);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.lblProduct);
            this.tabPage3.Controls.Add(this.lblProductLot);
            this.tabPage3.Location = new System.Drawing.Point(0, 0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(240, 144);
            this.tabPage3.Text = "Chi tiết lô";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(2, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 21);
            this.label1.Text = "Pallet:";
            // 
            // lblProduct
            // 
            this.lblProduct.Location = new System.Drawing.Point(1, 21);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(237, 78);
            this.lblProduct.Text = "Sản phẩm:";
            // 
            // lblProductLot
            // 
            this.lblProductLot.Location = new System.Drawing.Point(3, -3);
            this.lblProductLot.Name = "lblProductLot";
            this.lblProductLot.Size = new System.Drawing.Size(237, 21);
            this.lblProductLot.Text = "Mã lô:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 21);
            this.label4.Text = "SL:";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(34, 120);
            this.txtQuantity.MaxLength = 100;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(113, 21);
            this.txtQuantity.TabIndex = 31;
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantity_KeyPress);
            // 
            // btnMove
            // 
            this.btnMove.Location = new System.Drawing.Point(153, 120);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(84, 21);
            this.btnMove.TabIndex = 29;
            this.btnMove.Text = "San";
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // MovePalletView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.lblPallet);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "MovePalletView";
            this.Text = "San pallet";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MovePalletView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDestination)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsPalletSummary;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGrid dtgPallet;
        private System.Windows.Forms.DataGrid dtgPalletDestination;
        private System.Windows.Forms.BindingSource bdsPalletDestination;
        private System.Windows.Forms.MenuItem mnuAnotherPallet;
        private System.Windows.Forms.MenuItem mnuMoveAll;
        private System.Windows.Forms.MenuItem mnuClearPallet;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblProduct;
        private System.Windows.Forms.Label lblProductLot;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Button btnMove;
    }
}