﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading;
using Bayer.WMS.Delivery.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class SplitView : Form
    {
        #region Param
        protected BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;

        private bool _isProcess = false;
        private Queue<string> _queue;
        private List<string> _ListScannedInPallet;
        private Company _company;
        private Pallet _currentPallet;
        private DataTable _dt;
        private DataTable _dtProductByCompany;
        private DataTable _dtProductByDOImport;
        private DataTable _dtProductByPallet;
        private DataTable _dtCompany;
        private DataTable _dtDO;
        private string _deliveryDate;
        private bool dataChanged = false;
        private string _userInfo = "Soạn hàng theo DO";
        private string _palletInfo = string.Empty;
        private string _doImportCode = string.Empty;
        private int _productID = 0;
        private string _productNameCurrent = string.Empty;
        #endregion

        #region InitForm
        public SplitView()
        {
            try
            {
                InitializeComponent();
                InitDataGridView();
                this.KeyPreview = true;
                _queue = new Queue<string>();
                _stepHints = new List<string> 
                { 
                    "Quét mã DO",
                    "Quét mã sản phẩm/ thùng/ lẻ/ số lô",
                    "Quét mã khách hàng",
                    "Quét mã pallet có hàng để xuất chẳn" + Environment.NewLine + "Quét mã pallet trống để xuất lẻ",
                    "Quét mã sản phẩm/ thùng để soạn hàng. Bấm xác nhận để soạn pallet này cho khách hàng",
                };

                lblStepHints.Text = _stepHints[0];
                _company = new Company();
                _currentPallet = new Pallet();
                _dt = new DataTable();
                _dtProductByCompany = new DataTable();
                _dtProductByDOImport = new DataTable();
                _dtProductByPallet = new DataTable();
                _dtCompany = new DataTable();
                _dtDO = new DataTable();
                _queue = new Queue<string>();
                _ListScannedInPallet = new List<string>();

                _userInfo = "SH_DO: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
                this.Text = _userInfo;

                LoadListDO();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);
           

            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isProcess)
                    QueueProcess(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }
        #endregion

        #region MENU BUTTON
        public void mniClearPallet_Click(object sender, EventArgs e)
        {
            clearPallet();
        }

        public void clearPallet()
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ClearPalletView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[3];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergePallet_Click(object sender, EventArgs e)
        {
            moveMaterialPallet();
        }

        public void moveMaterialPallet()
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveMaterialPalletView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[3];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mergePallet()
        {
            try
            {
                DisposeBarcodeReader();

                //using (var view = new Bayer.WMS.Handheld.Views.MergePalletView())
                //{
                //    view.ShowDialog();
                //}

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[3];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergeCarton_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                //using (var view = new Bayer.WMS.Handheld.Views.MergeCartonView())
                //{
                //    view.ShowDialog();
                //}

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[3];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniExit_Click(object sender, EventArgs e)
        {
            exitForm();
        }

        public void exitForm()
        {
            try
            {
                DisposeBarcodeReader();
                this.Close();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        //Xác nhận xuất lấy luôn pallet này cho KH
        private void menuConfirmPallet_Click(object sender, EventArgs e)
        {
            confirmPallet();
        }

        private void confirmPallet()
        {
            try
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    //if (_currentPallet.Status == "N")
                    {
                        #region Verify Prepared Quantity
                        DataTable dt = Company.VerifyPreparedQuantity(_doImportCode, _company.CompanyCode, _currentPallet.PalletCode);
                        if (dt.Rows.Count > 0)
                        {
                            string mes = string.Empty;

                            if (_dtProductByCompany.Select(String.Format("ProductID = '{0}'", dt.Rows[0]["ProductID"].ToString())).Length == 0)
                            {
                                mes = "Sản phẩm " + dt.Rows[0]["ProductName"].ToString() + " không yêu cầu trong DO";
                            }
                            else if (_dtProductByCompany.Select(String.Format("ProductLot = '{0}'", dt.Rows[0]["ProductLot"].ToString())).Length == 0)
                            {
                                mes = "Số lô " + dt.Rows[0]["ProductLot"].ToString() + " không yêu cầu trong DO";
                            }
                            else
                            {
                                mes =
                                    "Vượt quá số lượng yêu cầu" + Environment.NewLine
                                    + "SL: " + dt.Rows[0]["NextQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                                    + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                                    + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                                    ;
                            }

                            Utility.PlayErrorSound();
                            MessageBox.Show(mes, "Lỗi");
                            return;
                        }
                        #endregion

                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_Status(_currentPallet.PalletCode, _doImportCode, _doImportCode, _company.CompanyCode, "S", method.Name);

                        //Auto set location for prepare area
                        Pallet.SetLocation(_currentPallet.PalletCode, Utility.PrepareLocationCode, "SH");

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        LoadPalletInfo(_currentPallet.PalletCode, false);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                    //else
                    //{
                    //    MessageBox.Show("Trạng thái pallet không đúng, không thể xác nhận.");
                    //}
                }
                else
                {
                    MessageBox.Show("Vui lòng quét mã pallet");
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void confirmFinishSplitPallet()
        {
            try
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    //if (_currentPallet.Status == "N")
                    {
                        #region Verify Prepared Quantity
                        DataTable dt = Company.VerifyPreparedQuantity(_doImportCode, _company.CompanyCode, _currentPallet.PalletCode);
                        if (dt.Rows.Count > 0)
                        {
                            string mes = string.Empty;

                            if (_dtProductByCompany.Select(String.Format("ProductID = '{0}'", dt.Rows[0]["ProductID"].ToString())).Length == 0)
                            {
                                mes = "Sản phẩm " + dt.Rows[0]["ProductName"].ToString() + " không yêu cầu trong DO";
                            }
                            else if (_dtProductByCompany.Select(String.Format("ProductLot = '{0}'", dt.Rows[0]["ProductLot"].ToString())).Length == 0)
                            {
                                mes = "Số lô " + dt.Rows[0]["ProductLot"].ToString() + " không yêu cầu trong DO";
                            }
                            else
                            {
                                mes =
                                    "Vượt quá số lượng yêu cầu" + Environment.NewLine
                                    + "SL: " + dt.Rows[0]["NextQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                                    + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                                    + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                                    ;
                            }

                            Utility.PlayErrorSound();
                            MessageBox.Show(mes, "Lỗi");
                            return;
                        }
                        #endregion

                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_Status(_currentPallet.PalletCode, _doImportCode, _doImportCode, _company.CompanyCode, "U", method.Name);

                        //Auto set location for prepare area
                        Pallet.SetLocation(_currentPallet.PalletCode, Utility.PrepareLocationCode, "SH");

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        LoadPalletInfo(_currentPallet.PalletCode, false);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                    //else
                    //{
                    //    MessageBox.Show("Trạng thái pallet không đúng, không thể xác nhận.");
                    //}
                }
                else
                {
                    MessageBox.Show("Vui lòng quét mã pallet");
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        //Trả lại pallet
        private void menuReturnPallet_Click(object sender, EventArgs e)
        {
            returnPallet();
        }

        private void returnPallet()
        {
            try
            {
                //Check trạng thái
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode) && _currentPallet.Status != "N")
                {
                    //Xác nhận có muốn thực hiện hay ko
                    if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn trả lại pallet: " + _currentPallet.PalletCode + " ?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_Status(_currentPallet.PalletCode, string.Empty, string.Empty, string.Empty, "N", method.Name);

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        LoadPalletInfo(_currentPallet.PalletCode, false);
                        #endregion

                        lblStepHints.Text = _stepHints[3];
                    }
                }
                else
                {
                    MessageBox.Show("Trạng thái pallet không đúng");
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void menuQRCode_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.BarcodeTrackingView())
                {
                    view.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveProductPallet())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
        #endregion

        #region Init
        public void CheckProductByDOImportCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtProductByDOImport.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtProductByDOImport.Rows[e.Row]["PreparedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckCustomerProductCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["PreparedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckPalletCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtProductByPallet.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtProductByPallet.Rows[e.Row]["PreparedQty"].ToString());
            decimal current = decimal.Parse(_dtProductByPallet.Rows[e.Row]["Quantity"].ToString());

            if (current > requestQty)
                e.MeetsCriteria = 3;
            else if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckDOCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtDO.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtDO.Rows[e.Row]["PreparedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckCompanyCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtCompany.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtCompany.Rows[e.Row]["PreparedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            try
            {
                #region dtgPallets
                var dtgCustomerPalletsStyle = new DataGridTableStyle { MappingName = "PalletList" };
                dtgCustomerPalletsStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
                dtgCustomerPalletsStyle.GridColumnStyles.Add(Utility.AddColumn("Mã", "PalletCode", 75, String.Empty, null));
                dtgCustomerPalletsStyle.GridColumnStyles.Add(Utility.AddColumn("TT", "StrStatus", 100, String.Empty, null));
                dtgCustomerPalletsStyle.GridColumnStyles.Add(Utility.AddColumn("Người dùng", "UserFullName", 150, String.Empty, null));
                dtgCustomerPalletsStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng", "CartonQuantity", 40, String.Empty, null));
                dtgCustomerPalletsStyle.GridColumnStyles.Add(Utility.AddColumn("Bao", "BagQuantity", 40, String.Empty, null));
                dtgCustomerPalletsStyle.GridColumnStyles.Add(Utility.AddColumn("Xô", "ShoveQuantity", 40, String.Empty, null));
                dtgCustomerPalletsStyle.GridColumnStyles.Add(Utility.AddColumn("Can", "CanQuantity", 40, String.Empty, null));
                dtgPallets.TableStyles.Add(dtgCustomerPalletsStyle);
                #endregion

                #region dtgCustomerProduct
                var dtgCustomerProductsStyle = new DataGridTableStyle { MappingName = "DetailProduct" };
                dtgCustomerProductsStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, CheckCustomerProductCellEquals));
                dtgCustomerProductsStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCustomerProductCellEquals));
                dtgCustomerProductsStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 40, String.Empty, CheckCustomerProductCellEquals));
                dtgCustomerProductsStyle.GridColumnStyles.Add(Utility.AddColumn("SL_cần_lấy", "NeedPrepareQuantity", 60, String.Empty, CheckCustomerProductCellEquals));
                dtgCustomerProductsStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, CheckCustomerProductCellEquals));
                dtgCustomerProductsStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCustomerProductCellEquals));
                dtgCustomerProductsStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 20, String.Empty, CheckCustomerProductCellEquals));
                dtgCustomerProductsStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCustomerProductCellEquals));//CheckCustomerProductCellEquals
                dtgProduct.TableStyles.Add(dtgCustomerProductsStyle);
                #endregion

                #region dtgPalletDetailProduct
                var dtgPalletDetailProductStyle = new DataGridTableStyle { MappingName = "PalletDetailProduct" };
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 100, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 40, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL_cần_lấy", "NeedPrepareQuantity", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, CheckPalletCellEquals));

                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletInfo.TableStyles.Add(dtgPalletDetailProductStyle);
                #endregion

                #region dtgCustomer
                var dtgDOCustomerStyle = new DataGridTableStyle { MappingName = "CustomerList" };
                dtgDOCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Tên Cty", "CompanyName", 150, String.Empty, CheckCompanyCellEquals));
                dtgDOCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCompanyCellEquals));
                dtgDOCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("SL_DO", "DOQuantity", 50, String.Empty, CheckCompanyCellEquals));
                dtgDOCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Chia", "PreparedQty", 50, String.Empty, CheckCompanyCellEquals));
                dtgDOCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Gợi ý", "SuggestSplit", 50, String.Empty, CheckCompanyCellEquals));
                dtgDOCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "QC", 30, String.Empty, CheckCompanyCellEquals));
                dtgDOCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Mã Cty", "CompanyCode", 80, String.Empty, CheckCompanyCellEquals));
                dtgDOCustomers.TableStyles.Add(dtgDOCustomerStyle);
                #endregion

                #region dtgProducts
                var dtgDOProductStyle = new DataGridTableStyle { MappingName = "ProductList" };
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 100, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL_cần_lấy", "NeedPrepareQuantity", 60, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL(DO)", "DOQuantity", 40, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL_đã_lấy", "Quantity", 40, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 20, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckProductByDOImportCellEquals));
                dtgDOProducts.TableStyles.Add(dtgDOProductStyle);
                #endregion

                #region dtgDO
                var dtgDOStyle = new DataGridTableStyle { MappingName = "DOList" };
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Ngày giao", "DeliveryDate", 80, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Mã DO", "DOImportCode", 100, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("SL_DO", "DOQuantity", 50, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Soạn", "PreparedQty", 50, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Giao", "DeliveredQty", 50, String.Empty, CheckDOCellEquals));
                dtgDO.TableStyles.Add(dtgDOStyle);
                #endregion

                ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ProcessBarcode
        public string currentBarCode = string.Empty;
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                currentBarCode = barcode;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (string.IsNullOrEmpty(_doImportCode))
                {
                    _doImportCode = currentBarCode;
                    this.Text = "SH_DO: " + _doImportCode;
                    LoadProductByDO();
                    LoadListCompany(string.Empty);
                    ShowHideTab();
                    lblStepHints.Text = _stepHints[1];
                }
                else if (Utility.CustomerRegex.IsMatch(barcode))
                {
                    barcode = barcode.Remove(0, 3);
                    ProcessCustomerBarcode(barcode);
                    lblStepHints.Text = _stepHints[3];
                }
                //// step 2: scan export note
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                {
                    ProcessPalletBarcode(barcode);
                    lblStepHints.Text = _stepHints[4];
                }
                //// step 3: scan pallet/carton/product
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", lblCurrentActionInfo.Text, barcode, ex.Message);
                Utility.PlayErrorSound();
                Utility.WriteLog(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessCustomerBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                dataChanged = false;

                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");

                _company = Company.Load(barcode);
                lblCurrentActionInfo.Text = _company.CompanyName;

                LoadPalletByCompany();
                LoadProductByCompany();

                bdsPalletInfo.DataSource = null;
                tpPrepareInfo.SelectedIndex = 1;

                lblStepHints.Text = "Quét mã pallet / thùng / lẻ";
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");

                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");

                //// get quantity details of pallet
                _currentPallet = Pallet.Load(barcode);

                //Validate Current Customer vs Pallet Customer
                if (!string.IsNullOrEmpty(_currentPallet.CompanyCode) && (_currentPallet.CompanyCode != "9999999") && (_currentPallet.CompanyCode != _company.CompanyCode))
                {
                    MessageBox.Show(
                        "Pallet này không thuộc về khách hàng đang soạn."
                        + Environment.NewLine
                        + "Vui lòng chọn đúng pallet."
                    );
                    _currentPallet = null;
                    return;
                }

                LoadPalletInfo(barcode, true);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessProductBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                #region Validate Data
                if (String.IsNullOrEmpty(_doImportCode))
                {
                    throw new Exception("Vui lòng quét mã DO.");

                }
                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");
                if (String.IsNullOrEmpty(_currentPallet.PalletCode))
                    throw new Exception("Vui lòng quét mã pallet.");
                if (_dt.Select(String.Format("CartonBarcode = '{0}'", barcode)).Length > 0)
                    throw new Exception("Mã QR đã được quét, vui lòng kiểm tra lại");
                if (_ListScannedInPallet.Exists(e => e.EndsWith(barcode)))
                    throw new Exception("Mã QR đã được quét, vui lòng kiểm tra lại");

                #endregion

                #region Verify Prepared Quantity
                if (_currentPallet.Status != "N" && !string.IsNullOrEmpty(_currentPallet.CompanyCode))
                {
                    DataTable dt = Company.VerifyPreparedQuantity(_currentPallet.PalletCode, barcode);
                    if (dt.Rows.Count > 0)
                    {
                        string mes = string.Empty;

                        if (_dtProductByCompany.Select(String.Format("ProductID = '{0}'", dt.Rows[0]["ProductID"].ToString())).Length == 0)
                        {
                            mes = "Sản phẩm " + dt.Rows[0]["ProductName"].ToString() + " không yêu cầu trong DO";
                        }
                        else if (_dtProductByCompany.Select(String.Format("ProductLot = '{0}'", dt.Rows[0]["ProductLot"].ToString())).Length == 0)
                        {
                            mes = "Số lô " + dt.Rows[0]["ProductLot"].ToString() + " không yêu cầu trong DO";
                        }
                        else
                        {
                            mes =
                                "Vượt quá số lượng yêu cầu" + Environment.NewLine
                                + "SL: " + dt.Rows[0]["NextQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                                + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                                + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                                ;
                        }

                        Utility.PlayErrorSound();
                        MessageBox.Show(mes, "Lỗi");
                        return;
                    }
                }
                #endregion

                #region InsertData
                if (
                     Utility.CartonBarcode3Regex.IsMatch(barcode)
                    || Utility.CartonBarcode2Regex.IsMatch(barcode)
                    || Utility.CartonBarcodeRegex.IsMatch(barcode)
                    )
                {
                    var method = this.GetType().GetMethod("ProcessBarcode");
                    Pallet.UpdatePallet_MoveProduct(_currentPallet.PalletCode, barcode, "C", method.Name);
                }
                else
                {
                    try
                    {
                        string encryptedBarcode = barcode;
                        string type;
                        barcode = Barcode.Process(encryptedBarcode, false, false, true, out type);

                        //if (_dt.Select(String.Format("ProductBarcode = '{0}'", barcode)).Length > 0)
                        //    throw new Exception("Mã QR đã được quét, vui lòng kiểm tra lại");

                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_MoveProduct(_currentPallet.PalletCode, barcode, "E", method.Name);
                    }
                    catch
                    {
                        throw new Exception("Mã không hợp lệ");
                    }
                }

                _ListScannedInPallet.Add(barcode);
                #endregion

                #region Reload Data
                LoadPalletInfo(_currentPallet.PalletCode, false);
                tpPrepareInfo.SelectedIndex = 2;

                dataChanged = true;
                #endregion
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void LoadPalletByCompany()
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
                listParam.Add(new SqlParameter("ProductID", _productID));
                bdsPallets.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_Pallets_SelectByCompany2", listParam);
                a.TableName = "PalletList";
                bdsPallets.DataSource = a;

                tpPrepareInfo.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadProductByCompany()
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
                bdsProducts.DataSource = null;
                _dtProductByCompany = new DataTable();
                _dtProductByCompany = Utility.LoadDataFromStore("proc_PalletStatuses_SelectByCompany_GroupByBarCode", listParam);
                _dtProductByCompany.TableName = "DetailProduct";
                bdsProducts.DataSource = _dtProductByCompany;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadProductByDO()
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
                bdsDOProduct.DataSource = null;
                _dtProductByDOImport = new DataTable();
                _dtProductByDOImport = Utility.LoadDataFromStore("proc_SplitView_SelectProduct", listParam);
                _dtProductByDOImport.TableName = "ProductList";
                bdsDOProduct.DataSource = _dtProductByDOImport;

                tpPrepareInfo.SelectedIndex = 4;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadPalletInfo(string barcode, bool checkStatus)
        {
            try
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    _ListScannedInPallet = new List<string>();

                    #region bdsPalletInfo
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    listParam.Add(new SqlParameter("Barcode", barcode));
                    listParam.Add(new SqlParameter("Type", "P"));
                    DataTable a = Utility.LoadDataFromStore("proc_PalletStatuses_Select2", listParam);
                    _dt = a;
                    #endregion

                    #region bdsPalletInfo
                    List<SqlParameter> listParam2 = new List<SqlParameter>();
                    listParam2.Add(new SqlParameter("Barcode", barcode));
                    listParam2.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                    listParam2.Add(new SqlParameter("DOImportCode", _doImportCode));
                    bdsPalletInfo.DataSource = null;
                    DataTable b = Utility.LoadDataFromStore("proc_PalletStatuses_SelectByPallet_GroupByBarCode", listParam2);
                    b.TableName = "PalletDetailProduct";
                    bdsPalletInfo.DataSource = b;
                    _dtProductByPallet = b;
                    #endregion

                    #region ThungBaoXoCan
                    try
                    {
                        List<SqlParameter> listParam3 = new List<SqlParameter>();
                        listParam3.Add(new SqlParameter("Barcode", barcode));
                        DataTable c = Utility.LoadDataFromStore("proc_Pallets_Select_SumByPackingType", listParam3);
                        _palletInfo = c.Rows[0]["StrQuantity"].ToString();


                    }
                    catch
                    {

                    }
                    #endregion

                    //Tự động hỏi có muốn xác nhận pallet này cho khách hàng hay không
                    if (checkStatus && _currentPallet != null && _currentPallet.Status == "N")
                    {
                        if (DialogResult.Yes == MessageBox.Show("Bạn có muốn chia pallet: " + _currentPallet.PalletCode + " cho khách hàng " + _company.CompanyName + " ?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                        {
                            confirmPallet();
                        }
                    }

                    //Nếu đây là pallet mới và có SP
                    lblStepHints.Text = _stepHints[3];
                    tpPrepareInfo.SelectedIndex = 2;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadListCompany(string barcode)
        {
            try
            {
                _productID = 0;
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
                listParam.Add(new SqlParameter("Barcode", barcode));
                bdsDOCustomer.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_SplitView_SelectCompany", listParam);
                a.TableName = "CustomerList";
                _dtCompany = a;
                bdsDOCustomer.DataSource = a;

                _deliveryDate = a.Rows[0]["StrDeliveryDate"].ToString();
                try
                {
                    _productID = Convert.ToInt32(a.Rows[0]["ProductID"].ToString());
                    _productNameCurrent = a.Rows[0]["ProductName"].ToString();
                }
                catch
                {
                    //Do nothing
                }

                tpPrepareInfo.SelectedIndex = 3;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadListDO()
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                bdsDO.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_Prepare_SelectDO", listParam);
                a.TableName = "DOList";
                _dtDO = a;
                bdsDO.DataSource = a;

                tpPrepareInfo.SelectedIndex = 5;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ActionOnScreen
        private void dtgCustomer_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = "KH_" + dtgDOCustomers[dtgDOCustomers.CurrentRowIndex, 6].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void dtgDO_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgDO[dtgDO.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void dtgPallets_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgPallets[dtgPallets.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void tpPrepareInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                #region ForReloadData
                if (dataChanged)
                {
                    //Nếu check vào 2 tab tổng của KH, load lại list pallet và sl
                    if (tpPrepareInfo.SelectedIndex <= 1)
                    {
                        dataChanged = false;

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        #endregion
                    }
                }
                #endregion

                if (tpPrepareInfo.SelectedIndex == 0 || tpPrepareInfo.SelectedIndex == 1)
                {
                    if (_company != null && !string.IsNullOrEmpty(_company.CompanyName))
                    {
                        lblCurrentActionInfo.Text = "KH:" + _company.CompanyName;
                    }
                    lblStepHints.Text = _stepHints[3];
                }
                else if (tpPrepareInfo.SelectedIndex == 2)
                {
                    if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                    {
                        lblCurrentActionInfo.Text = _currentPallet.PalletCode + ": " + _palletInfo;
                    }
                    lblStepHints.Text = _stepHints[4];
                }
                else if (tpPrepareInfo.SelectedIndex == 3)
                {
                    lblCurrentActionInfo.Text = _productNameCurrent;
                    lblStepHints.Text = _stepHints[2];
                }
                else if (tpPrepareInfo.SelectedIndex == 4)
                {
                    lblCurrentActionInfo.Text = _productNameCurrent;
                    lblStepHints.Text = _stepHints[1];
                }
                else if (tpPrepareInfo.SelectedIndex == 5)
                {
                    lblCurrentActionInfo.Text = string.Empty;
                    lblStepHints.Text = _stepHints[0];
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        private void SplitView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        private void SplitView_Load(object sender, EventArgs e)
        {
            lblStepHints.Text = _stepHints[1];
            ShowHideTab();
        }

        private void ShowHideTab()
        {
            if (string.IsNullOrEmpty(_doImportCode))
            {
                //tpDO.Show();
                //tpListPallets.Hide();
                //tpPallet.Hide();
                //tpPalletProduct.Hide();
                //tpPrepareInfo.Hide();
                //tpProducts.Hide();
                //tpPrepareInfo.TabPages[5].Show();
                //tpPrepareInfo.Refresh();
            }
            else
            {
                //tpDO.Hide();
                //tpListPallets.Show();
                //tpPallet.Show();
                //tpPalletProduct.Show();
                //tpPrepareInfo.Show();
                //tpProducts.Show();
                //tpPrepareInfo.TabPages[5].Hide();
                tpPrepareInfo.Controls.Remove(this.tpDO);
                tpPrepareInfo.Refresh();
                //this.ResumeLayout(false);
            }
        }

        private void dtgProducts_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgDOProducts[dtgDOProducts.CurrentRowIndex, 8].ToString();
                LoadListCompany(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            packaging();
        }

        private void packaging()
        {
            try
            {
                DisposeBarcodeReader();
                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");
                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");
                if (String.IsNullOrEmpty(_currentPallet.PalletCode))
                    throw new Exception("Vui lòng quét mã pallet.");

                string cartonBarcode = Utility.GenCartonWarehouseBarcode();

                if (Utility.debugMode)
                {
                    MessageBox.Show(cartonBarcode);
                }

                if (!string.IsNullOrEmpty(cartonBarcode))
                {
                    using (var view = new PackagingView())
                    {
                        view.OddQty = 0;// _process.OddQty;
                        view.PrepareCode = "SH" + DateTime.Today.ToString("yyMMdd") + "01";//_process.ReferenceNbr;SH17092701
                        view.Pallet = _currentPallet;
                        view.DOImportCode = _doImportCode;
                        view.CartonBarcode = cartonBarcode;
                        view.CompanyCode = _company.CompanyCode;
                        view.CompanyName = _company.CompanyName;
                        view.Executor = Utility.CurrentUser.LastName + " " + Utility.CurrentUser.FirstName;
                        view.DeliveryDate = DateTime.Today;
                        view.ShowDialog();
                    }
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void mnFinishSplit_Click(object sender, EventArgs e)
        {
            confirmFinishSplitPallet();
        }

        private void btnShowAllCustomer_Click(object sender, EventArgs e)
        {
            LoadListCompany(string.Empty);
        }

        private void mnuPrintPallet_Click(object sender, EventArgs e)
        {
            printPallet();
        }

        private void printPallet()
        {
            try
            {
                DisposeBarcodeReader();
                if (_company != null)
                {
                    using (var view = new PrintPalletLabelView())
                    {
                        view.DOImportCode = _doImportCode;
                        view.CompanyCode = _company.CompanyCode;
                        view.CompanyName = _company.CompanyName;
                        view.DeliveryDate = _deliveryDate;
                        view.Executor = Utility.CurrentUser.LastName + " " + Utility.CurrentUser.FirstName;
                        if (view.ShowDialog() != DialogResult.OK)
                            return;
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ProcessCustomerBarcode(_company.CompanyCode);
                ReInitBarcodeReader();
            }
        }

        private void dtgPallets_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (e.KeyCode == Keys.F1)
                //{
                //    tpPrepareInfo.SelectedIndex = 0;
                //}
                //else if (e.KeyCode == Keys.F2)
                //{
                //    tpPrepareInfo.SelectedIndex = 1;
                //}
                //else if (e.KeyCode == Keys.F3)
                //{
                //    tpPrepareInfo.SelectedIndex = 2;
                //}
                //else if (e.KeyCode == Keys.F4)
                //{
                //    tpPrepareInfo.SelectedIndex = 3;
                //}
                //else if (e.KeyCode == Keys.F5)
                //{
                //    tpPrepareInfo.SelectedIndex = 4;
                //}
                //else if (e.KeyCode == Keys.F6)
                //{
                //    tpPrepareInfo.SelectedIndex = 5;
                //}


                //if (e.KeyCode == Keys.Right)
                //{
                //    tpPrepareInfo.SelectedIndex = 5;
                //}
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
    }
}