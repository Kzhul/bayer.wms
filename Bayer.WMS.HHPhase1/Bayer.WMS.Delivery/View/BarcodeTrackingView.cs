﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class BarcodeTrackingView : Form
    {
        private BarcodeReader _barcodeReader;

        public BarcodeTrackingView()
        {
            InitializeComponent();
            InitDataGridView();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void BarcodeTrackingView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public void InitDataGridView()
        {
            #region dtgCartonDetails

            var dtgStyle = new DataGridTableStyle { MappingName = "Tracking" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã thùng", "CartonBarcode", 100, String.Empty, null));
            
            dtgCartonDetails.TableStyles.Add(dtgStyle);

            #endregion

            #region dtgInfo

            var dtgInfoStyle = new DataGridTableStyle { MappingName = "Info" };

            dtgInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Thông tin", "InfoName", 150, String.Empty, null));
            dtgInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Giá trị", "InfoValue", 100, String.Empty, null));

            dtgInfo.TableStyles.Add(dtgInfoStyle);

            #endregion
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                string type = "E";
                string originalBarCode = barcode;
                try
                {
                    barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);                    
                    barcode = Barcode.Process(barcode, true, true, true, out type);
                    txtBarcode.Text = barcode;
                }
                catch
                {
                    barcode = originalBarCode;
                    txtBarcode.Text = barcode;
                }

                var dt = Barcode.GetBarcodeTracking(barcode, type);
                dt.TableName = "Tracking";
                bdsCartonDetails.DataSource = dt;

                var dtInfo = Barcode.GetBarcodeTrackingInfo(barcode, type);
                dtInfo.TableName = "Info";
                bdsInfo.DataSource = dtInfo;
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }
    }
}