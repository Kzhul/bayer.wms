﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ClearPalletView : Form
    {
        private BarcodeReader _barcodeReader;

        public ClearPalletView()
        {
            InitializeComponent();
            InitDataGridView();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void ClearPalletView_Closed(object sender, EventArgs e)
        {
            lblStepHints.Text = "Quét mã Pallet để clear";
        }

        public void ClearPalletView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {

                Pallet.Clear(lblPallet.Text, lblProductLot.Text);
                string palletCode = lblPallet.Text;
                string productLot = lblProductLot.Text;
                bdsPalletSummary.DataSource = null;
                lblPallet.Text = String.Empty;
                lblProductLot.Text = String.Empty;

                if (string.IsNullOrEmpty(productLot))
                {
                    lblStepHints.Text = "Đã clear pallet " + palletCode + ", số lô: " + productLot;
                }
                else
                {
                    lblStepHints.Text = "Đã clear toàn bộ pallet " + palletCode;
                }                
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));

            dtgPallet.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (Utility.ProductLotRegex.IsMatch(barcode))
                {
                    lblProductLot.Text = barcode;
                }
                else
                {
                    string type;
                    barcode = Barcode.Process(barcode, true, false, false, out type);

                    lblPallet.Text = barcode;

                    var dt = Pallet.LoadRequirePalletStatus(barcode, type);
                    dt.TableName = "PalletSummary";
                    bdsPalletSummary.DataSource = dt;

                    lblStepHints.Text = "Xác nhận để clear pallet" + Environment.NewLine + "Quét mã lô để chỉ clear lô đó khỏi pallet";
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        private void dtgPallet_DoubleClick(object sender, EventArgs e)
        {
            txtBarcode.Text = dtgPallet[dtgPallet.CurrentRowIndex, 2].ToString();
            ProcessBarcode(txtBarcode.Text);
        }
    }
}