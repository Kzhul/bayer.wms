﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Bayer.WMS.Delivery.Models;
using System.IO;

namespace Bayer.WMS.Delivery
{
    static class Program
    {
        public static string AppPath;
        public static string ConnStr;
        public static User _user;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            try
            {
                Application.Run(new MainView());
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Utility.ConnStr))
                {
                    Utility.WriteLogToDB(ex);
                }
                using (var sw = new StreamWriter(Path.Combine(Utility.AppPath, "Error.txt"), true))
                {
                    string message = ex.Message;
                    string stackTrace = ex.StackTrace;
                    string innerException_Message = string.Empty;
                    string innerException_StackTrace = string.Empty;
                    sw.WriteLine(message);
                    sw.WriteLine(stackTrace);
                    if (ex.InnerException != null)
                    {
                        innerException_Message = ex.InnerException.Message;
                        innerException_StackTrace = ex.InnerException.StackTrace;
                        sw.WriteLine(innerException_Message);
                        sw.WriteLine(innerException_StackTrace);
                    }

                }
            }
        }
    }
}