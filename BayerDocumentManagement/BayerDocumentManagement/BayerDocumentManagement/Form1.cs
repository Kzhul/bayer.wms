﻿using Bayer.WMS.Objs;
using Microsoft.Office.Interop.Excel;
using ReadWriteCsv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BayerDocumentManagement
{
    public partial class Form1 : Form
    {
        public List<DocumentEntity> listDocuments;
        public List<DocumentEntity> listViews;
        public DocumentEntity currentDocument;
        string fileName = System.Windows.Forms.Application.StartupPath + "\\SystemData.dll";
        int lastIndex = 0;
        public Form1()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgDocument);

            //Reset control tab index
            foreach (Control item in this.Controls)
            {
                item.TabIndex = 999;
                item.TabStop = false;
            }


            this.KeyPreview = true;

            int index = 1;
            txtDocumentBarCode.TabIndex = index; txtDocumentBarCode.TabStop = true; index++;
            txtDocumentName.TabIndex = index; txtDocumentName.TabStop = true; index++;
            txtPage.TabIndex = index; txtPage.TabStop = true; index++;
            txtDescription.TabIndex = index; txtDescription.TabStop = true; index++;
            btnSave.TabIndex = index; btnSave.TabStop = true; index++;
            txtDocumentCodeSearch.TabIndex = index; txtDocumentCodeSearch.TabStop = true; index++;
            dtpFromDate.TabIndex = index; dtpFromDate.TabStop = true; index++;
            dtpToDate.TabIndex = index; dtpToDate.TabStop = true; index++;
            chkNotDelivery.TabIndex = index; chkNotDelivery.TabStop = true; index++;
            btnView.TabIndex = index; btnView.TabStop = true; index++;
            dtgDocument.TabIndex = index; dtgDocument.TabStop = true; index++;

            txtDocumentBarCode.Focus();
        }

        #region Function

        protected void SetDoubleBuffered(DataGridView dtg)
        {
            try
            {
                Type dgvType = dtg.GetType();
                PropertyInfo pi = dgvType.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
                pi.SetValue(dtg, true, null);

                dtg.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(Utility.dgGrid_RowPostPaint);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        public void LoadView()
        {
            try
            {
                DateTime toDate = dtpToDate.Value;
                toDate = toDate.AddDays(1);


                listViews = listDocuments.Where(a =>
                                    (
                                        !string.IsNullOrEmpty(txtDocumentCodeSearch.Text)
                                        ||
                                        (
                                            chkNotDelivery.Checked == false
                                            || a.IsDelivered == false
                                        )
                                    )
                                    &&
                                    (
                                        string.IsNullOrEmpty(txtDocumentCodeSearch.Text)
                                        ||
                                        (
                                            a.DocumentBarCode.Contains(txtDocumentCodeSearch.Text)
                                            || a.DocumentName.Contains(txtDocumentCodeSearch.Text)
                                        )
                                    )
                                    &&
                                    (
                                        !string.IsNullOrEmpty(txtDocumentCodeSearch.Text)
                                        ||
                                        (
                                            a.ReceivedDate >= dtpFromDate.Value
                                            && a.ReceivedDate <= toDate
                                            ||
                                            a.DeliveryDate >= dtpFromDate.Value
                                            && a.DeliveryDate <= toDate
                                        )
                                    )
                            ).ToList();

                bindingSource.DataSource = listViews;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        public void LoadExcel()
        {
            using (CsvFileReader reader = new CsvFileReader(fileName))
            {
                CsvRow row = new CsvRow();
                int i = 0;
                while (reader.ReadRow(row))
                {
                    string DocumentBarCode = row[0];

                    if (!string.IsNullOrEmpty(DocumentBarCode))
                    {
                        string DocumentName = row[1];
                        int Page = Utility.IntParse(row[2]);
                        string Description = row[3];


                        string StrReceivedDate = row[4];
                        double m_date;
                        double.TryParse(row[4], out m_date);
                        DateTime ReceivedDate = m_date > 0 ? DateTime.FromOADate(m_date) : DateTime.Parse(StrReceivedDate);

                        DateTime? DeliveryDate = null;
                        if (row.Count > 5)
                        {
                            string StrDeliveryDate = row[5];
                            if (!string.IsNullOrEmpty(StrDeliveryDate))
                            {
                                double t_date;
                                double.TryParse(row[5], out t_date);
                                DeliveryDate = m_date > 0 ? DateTime.FromOADate(m_date) : DateTime.Parse(StrReceivedDate);
                            }
                        }

                        listDocuments.Add(
                            new DocumentEntity()
                            {
                                DocumentBarCode = DocumentBarCode,
                                DocumentName = DocumentName,
                                Page = Page,
                                Description = Description,
                                ReceivedDate = ReceivedDate,
                                DeliveryDate = DeliveryDate,
                                ExcelIndex = i
                            }
                        );
                        i++;
                    }
                }
                lastIndex = i;
            }
        }

        public void LoadExcel_OLD()
        {
            Microsoft.Office.Interop.Excel.Application excelApp = null;
            Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
            Microsoft.Office.Interop.Excel.Workbook workbook = null;
            //Microsoft.Office.Interop.Excel.Sheets sheets = null;
            Microsoft.Office.Interop.Excel.Worksheet sheetDetail = null;
            Microsoft.Office.Interop.Excel.Range range = null;

            try
            {
                string fileName = System.Windows.Forms.Application.StartupPath + "\\Data.xlsx";
                string start = "A1";
                string end = String.Empty;

                excelApp = new Microsoft.Office.Interop.Excel.Application();
                excelApp.DisplayAlerts = false;
                workbooks = excelApp.Workbooks;
                workbook = workbooks.Open(fileName, ReadOnly: true, Password: "123456aA@");

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                //// get a range to work with
                range = sheetDetail.get_Range(start, Missing.Value);

                //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                range = range.get_End(XlDirection.xlDown);

                //// specific end column
                end = "K" + sheetDetail.UsedRange.Columns["A:A", Type.Missing].Rows.Count.ToString();

                //// Get the range, then values from start to end
                range = sheetDetail.get_Range(start, end);
                var values = (object[,])range.Value2;
                int count = values.GetLength(0);


                int beginImport = 2;
                for (int i = beginImport; i <= count; i++)
                {
                    string DocumentBarCode = $"{values[i, 1]}";

                    if (!string.IsNullOrEmpty(DocumentBarCode))
                    {
                        string DocumentName = $"{values[i, 2]}";
                        int Page = Utility.IntParse($"{values[i, 3]}");
                        string Description = $"{values[i, 4]}";


                        string StrReceivedDate = $"{values[i, 5]}";
                        double m_date;
                        double.TryParse($"{values[i, 5]}", out m_date);
                        DateTime ReceivedDate = m_date > 0 ? DateTime.FromOADate(m_date) : DateTime.Parse(StrReceivedDate);

                        string StrDeliveryDate = $"{values[i, 6]}";
                        DateTime? DeliveryDate = null;
                        if (!string.IsNullOrEmpty(StrDeliveryDate))
                        {
                            double t_date;
                            double.TryParse($"{values[i, 6]}", out t_date);
                            DeliveryDate = m_date > 0 ? DateTime.FromOADate(m_date) : DateTime.Parse(StrReceivedDate);
                        }

                        listDocuments.Add(
                            new DocumentEntity()
                            {
                                DocumentBarCode = DocumentBarCode,
                                DocumentName = DocumentName,
                                Page = Page,
                                Description = Description,
                                ReceivedDate = ReceivedDate,
                                DeliveryDate = DeliveryDate,
                                ExcelIndex = i
                            }
                        );
                    }
                }

                BindingControl();
                LoadView();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public void BindingControl()
        {
            txtDocumentBarCode.Text = currentDocument.DocumentBarCode;
            txtDocumentName.Text = currentDocument.DocumentName;
            txtPage.Text = currentDocument.Page.ToString();
            txtDescription.Text = currentDocument.Description;
            txtReceivedDate.Text = currentDocument.strReceivedDate;
            txtDeliveryDate.Text = currentDocument.strDeliveryDate;
            chkIsDelivered.Checked = currentDocument.IsDelivered;

            btnDelivery.Visible = !chkIsDelivered.Checked;
        }

        public void ClearControl()
        {
            txtDocumentBarCode.Text = string.Empty;
            txtDocumentName.Text = string.Empty;
            txtPage.Text = "0";
            txtDescription.Text = string.Empty;
            txtReceivedDate.Text = string.Empty;
            txtDeliveryDate.Text = string.Empty;
            chkIsDelivered.Checked = false;

            btnDelivery.Visible = false;
        }

        public bool Save()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtDocumentBarCode.Text))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
                    return false;
                }

                DocumentEntity documentToSave = new DocumentEntity();
                documentToSave.DocumentBarCode = txtDocumentBarCode.Text;
                documentToSave.DocumentName = txtDocumentName.Text;
                documentToSave.Page = Utility.IntParse(txtPage.Value);
                documentToSave.Description = txtDescription.Text;

                //Điền ngày nhập nếu chưa tồn tại record này
                if (!listDocuments.Exists(a => a.DocumentBarCode == txtDocumentBarCode.Text))
                {
                    documentToSave.ExcelIndex = lastIndex;
                    lastIndex = lastIndex + 1;
                    documentToSave.ReceivedDate = DateTime.Now;
                    listDocuments.Add(documentToSave);
                }
                else
                {
                    currentDocument = listDocuments.First(a => a.DocumentBarCode == txtDocumentBarCode.Text);
                    documentToSave.ExcelIndex = currentDocument.ExcelIndex;
                    documentToSave.ReceivedDate = currentDocument.ReceivedDate;
                    documentToSave.DeliveryDate = currentDocument.DeliveryDate;
                }

                ////////////////////////////////SAVE EXCEL////////////////////////////////
                CsvRow row = new CsvRow();
                row.Add(documentToSave.DocumentBarCode);
                row.Add(documentToSave.DocumentName);
                row.Add(documentToSave.Page.ToString());
                row.Add(documentToSave.Description);
                row.Add(documentToSave.ReceivedDate.ToString("yyyy/MM/dd HH:mm:ss"));

                if (documentToSave.DeliveryDate.HasValue)
                {
                    row.Add(documentToSave.DeliveryDate.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                }
                else
                {
                    row.Add(string.Empty);
                }

                //if (documentToSave.ExcelIndex == 0)
                //{
                //    Utility.WriteTxtLine(fileName, Utility.GetRow(row));
                //}
                //else
                {
                    Utility.WriteTxtLine(fileName, documentToSave.ExcelIndex, Utility.GetRow(row));
                }

                currentDocument = documentToSave;
                BindingControl();
                LoadView();

                ClearControl();
                lblStatusBarMessage.Text = "Lưu thành công: " + txtDocumentBarCode.Text;

            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            return false;
        }

        public bool Save_OLD()
        {
            try
            {
                DocumentEntity documentToSave = new DocumentEntity();
                documentToSave.DocumentBarCode = txtDocumentBarCode.Text;
                documentToSave.DocumentName = txtDocumentName.Text;
                documentToSave.Page = Utility.IntParse(txtPage.Value);
                documentToSave.Description = txtDescription.Text;

                //Điền ngày nhập nếu chưa tồn tại record này
                if (!listDocuments.Exists(a => a.DocumentBarCode == txtDocumentBarCode.Text))
                {
                    documentToSave.ExcelIndex = 0;
                    documentToSave.ReceivedDate = DateTime.Now;
                    listDocuments.Add(documentToSave);
                }
                else
                {
                    currentDocument = listDocuments.First(a => a.DocumentBarCode == txtDocumentBarCode.Text);
                    documentToSave.ExcelIndex = currentDocument.ExcelIndex;
                    documentToSave.ReceivedDate = currentDocument.ReceivedDate;
                    documentToSave.DeliveryDate = currentDocument.DeliveryDate;
                }

                ////////////////////////////////SAVE EXCEL////////////////////////////////
                Microsoft.Office.Interop.Excel.Application excelApp = null;
                Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
                Microsoft.Office.Interop.Excel.Workbook workbook = null;
                //Microsoft.Office.Interop.Excel.Sheets sheets = null;
                Microsoft.Office.Interop.Excel.Worksheet sheetDetail = null;
                Microsoft.Office.Interop.Excel.Range range = null;

                //Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                //excelApp.DisplayAlerts = false;
                //_Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
                //_Worksheet sheetDetail = null;
                //Range range = null;
                //Workbooks workbooks = excelApp.Workbooks;

                try
                {
                    string fileName = System.Windows.Forms.Application.StartupPath + "\\Data.xlsx";
                    string start = "A1";
                    string end = String.Empty;

                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.DisplayAlerts = false;
                    workbooks = excelApp.Workbooks;
                    workbook = workbooks.Open(fileName, ReadOnly: false, Password: "123456aA@");

                    sheetDetail = (Worksheet)workbook.ActiveSheet;

                    //// get a range to work with
                    range = sheetDetail.get_Range(start, Missing.Value);

                    //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                    range = range.get_End(XlDirection.xlDown);

                    //// specific end column
                    end = "K" + sheetDetail.UsedRange.Columns["A:A", Type.Missing].Rows.Count.ToString();

                    //// Get the range, then values from start to end
                    range = sheetDetail.get_Range(start, end);
                    var values = (object[,])range.Value2;
                    int count = values.GetLength(0);

                    if (documentToSave.ExcelIndex == 0)
                    {
                        documentToSave.ExcelIndex = count + 1;
                    }

                    sheetDetail.Cells[documentToSave.ExcelIndex, 1] = documentToSave.DocumentBarCode;
                    sheetDetail.Cells[documentToSave.ExcelIndex, 2] = documentToSave.DocumentName;
                    sheetDetail.Cells[documentToSave.ExcelIndex, 3] = documentToSave.Page;
                    sheetDetail.Cells[documentToSave.ExcelIndex, 4] = documentToSave.Description;
                    sheetDetail.Cells[documentToSave.ExcelIndex, 5] = documentToSave.ReceivedDate;

                    if (documentToSave.DeliveryDate.HasValue)
                    {
                        sheetDetail.Cells[documentToSave.ExcelIndex, 6] = documentToSave.DeliveryDate.Value;
                    }

                    //workbook.SaveAs(fileName);
                    workbook.Close();
                    excelApp.Quit();

                    currentDocument = documentToSave;
                    BindingControl();
                    LoadView();

                    lblStatusBarMessage.Text = "Lưu thành công: " + txtDocumentBarCode.Text;
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                finally
                {
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);
                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }

            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            return false;
        }
        #endregion

        private void btnView_Click(object sender, EventArgs e)
        {
            LoadView();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                txtDocumentBarCode.Select();
                listDocuments = new List<DocumentEntity>();
                listViews = new List<DocumentEntity>();
                dtpFromDate.Value = DateTime.Today.AddDays(-7);
                dtpToDate.Value = DateTime.Today;
                currentDocument = new DocumentEntity();

                LoadExcel();
                LoadView();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel();
        }

        private void ExportExcel()
        {
            try
            {
                DocumentEntity documentToSave = new DocumentEntity();
                documentToSave.DocumentBarCode = txtDocumentBarCode.Text;
                documentToSave.DocumentName = txtDocumentName.Text;
                documentToSave.Page = Utility.IntParse(txtPage.Value);
                documentToSave.Description = txtDescription.Text;

                //Điền ngày nhập nếu chưa tồn tại record này
                if (!listDocuments.Exists(a => a.DocumentBarCode == txtDocumentBarCode.Text))
                {
                    documentToSave.ExcelIndex = 0;
                    documentToSave.ReceivedDate = DateTime.Now;
                    listDocuments.Add(documentToSave);
                }
                else
                {
                    currentDocument = listDocuments.First(a => a.DocumentBarCode == txtDocumentBarCode.Text);
                    documentToSave.ExcelIndex = currentDocument.ExcelIndex;
                    documentToSave.ReceivedDate = currentDocument.ReceivedDate;
                    documentToSave.DeliveryDate = currentDocument.DeliveryDate;
                }

                ////////////////////////////////SAVE EXCEL////////////////////////////////
                Microsoft.Office.Interop.Excel.Application excelApp = null;
                Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
                Microsoft.Office.Interop.Excel.Workbook workbook = null;
                //Microsoft.Office.Interop.Excel.Sheets sheets = null;
                Microsoft.Office.Interop.Excel.Worksheet sheetDetail = null;
                Microsoft.Office.Interop.Excel.Range range = null;
                string templatePath = System.Windows.Forms.Application.StartupPath + "\\Data.xlsx";
                string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ReportExcel\";
                try
                {

                    try
                    {
                        System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                        directory.Empty();
                    }
                    catch
                    {
                        //Do Nothing
                    }
                    string end = String.Empty;

                    excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.DisplayAlerts = false;
                    workbooks = excelApp.Workbooks;
                    workbook = workbooks.Open(templatePath, ReadOnly: true);
                    sheetDetail = (Worksheet)workbook.ActiveSheet;

                    int i = 2;
                    foreach (DocumentEntity document in listDocuments)
                    {
                        if (!string.IsNullOrEmpty(document.DocumentBarCode))
                        {
                            sheetDetail.Cells[i, 1] = document.DocumentBarCode;
                            sheetDetail.Cells[i, 2] = document.DocumentName;
                            sheetDetail.Cells[i, 3] = document.Page;
                            sheetDetail.Cells[i, 4] = document.Description;
                            sheetDetail.Cells[i, 5] = document.ReceivedDate;

                            if (document.DeliveryDate.HasValue)
                            {
                                sheetDetail.Cells[i, 6] = document.DeliveryDate.Value;
                            }
                            lblStatusBarMessage.Text = "Đang xuất dòng: " + i.ToString();
                            i++;
                        }
                    }

                    excelApp.DisplayAlerts = false;
                    exportPath += "BaoCaoHoSo" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                    workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                    workbook.Close();
                    excelApp.Quit();

                    lblStatusBarMessage.Text = "Xuất excel thành công";
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                finally
                {
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);
                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();

                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void dtgDocument_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow row in dtgDocument.Rows)
            {
                if (Convert.ToString(row.Cells["colDeliveryDate"].Value) == string.Empty)
                {
                    row.Cells["colDocumentBarCode"].Style.BackColor = Color.Yellow;
                }
            }
        }

        private void btnDelivery_Click(object sender, EventArgs e)
        {
            Delivery();
        }
        private void Delivery()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtDocumentBarCode.Text))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
                    return;
                }

                if (listDocuments.Exists(a => a.DocumentBarCode == txtDocumentBarCode.Text))
                {
                    currentDocument = listDocuments.First(a => a.DocumentBarCode == txtDocumentBarCode.Text);
                    currentDocument.DeliveryDate = DateTime.Now;
                    Save();

                    lblStatusBarMessage.Text = "Giao thành công: " + txtDocumentBarCode.Text;
                }
                else
                {
                    lblStatusBarMessage.Text = "Chưa nhập hồ sơ: " + txtDocumentBarCode.Text;
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {



                Save();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void txtDocumentBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (!string.IsNullOrWhiteSpace(txtDocumentBarCode.Text))
                {
                    try
                    {
                        if (listDocuments.Exists(a => a.DocumentBarCode == txtDocumentBarCode.Text))
                        {
                            currentDocument = listDocuments.First(a => a.DocumentBarCode == txtDocumentBarCode.Text);
                            BindingControl();

                            Delivery();
                        }
                    }
                    catch
                    {
                        //Do nothing
                    }
                }
            }
        }

        private void txtDocumentCodeSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (!string.IsNullOrWhiteSpace(txtDocumentCodeSearch.Text))
                {
                    LoadView();
                }
            }
        }

        private void chkNotDelivery_CheckedChanged(object sender, EventArgs e)
        {
            LoadView();
        }

        private void dtgDocument_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtDocumentBarCode.Text = dtgDocument.Rows[e.RowIndex].Cells[0].Value.ToString();
            try
            {
                if (listDocuments.Exists(a => a.DocumentBarCode == txtDocumentBarCode.Text))
                {
                    currentDocument = listDocuments.First(a => a.DocumentBarCode == txtDocumentBarCode.Text);
                    BindingControl();
                }
            }
            catch
            {
                //Do nothing
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                Delivery();
            }
            else if (e.KeyCode == Keys.F1)
            {
                LoadView();
            }
            else if (e.KeyCode == Keys.S && Control.ModifierKeys == Keys.Control)
            {
                Save();
            }
            else if (e.KeyCode == Keys.P && Control.ModifierKeys == Keys.Control)
            {
                ExportExcel();
            }
        }
    }
}
