﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ConfirmQtyForExportView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsConfirmQty = new System.Windows.Forms.BindingSource(this.components);
            this.dtgConfirmQty = new System.Windows.Forms.DataGrid();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniFeature = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPalletCode = new System.Windows.Forms.Label();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mniDelete = new System.Windows.Forms.MenuItem();
            this.mniEndDelete = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.bdsConfirmQty)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgConfirmQty
            // 
            this.dtgConfirmQty.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgConfirmQty.DataSource = this.bdsConfirmQty;
            this.dtgConfirmQty.Location = new System.Drawing.Point(0, 51);
            this.dtgConfirmQty.Name = "dtgConfirmQty";
            this.dtgConfirmQty.RowHeadersVisible = false;
            this.dtgConfirmQty.Size = new System.Drawing.Size(240, 174);
            this.dtgConfirmQty.TabIndex = 4;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 0;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 228);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 40);
            this.lblStepHints.Text = "Quét mã thùng/đơn vị lẻ";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniFeature);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniFeature
            // 
            this.mniFeature.MenuItems.Add(this.menuItem1);
            this.mniFeature.MenuItems.Add(this.mniDelete);
            this.mniFeature.MenuItems.Add(this.mniEndDelete);
            this.mniFeature.Text = "Chức năng";
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 21);
            this.label1.Text = "Pallet:";
            // 
            // lblPalletCode
            // 
            this.lblPalletCode.Location = new System.Drawing.Point(49, 27);
            this.lblPalletCode.Name = "lblPalletCode";
            this.lblPalletCode.Size = new System.Drawing.Size(188, 21);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Xác nhận";
            this.menuItem1.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniDelete
            // 
            this.mniDelete.Text = "Hủy";
            this.mniDelete.Click += new System.EventHandler(this.mniDelete_Click);
            // 
            // mniEndDelete
            // 
            this.mniEndDelete.Text = "Kết thúc hủy";
            this.mniEndDelete.Click += new System.EventHandler(this.mniEndDelete_Click);
            // 
            // ConfirmQtyForExportView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblPalletCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.dtgConfirmQty);
            this.Menu = this.mainMenu1;
            this.Name = "ConfirmQtyForExportView";
            this.Text = "Xuất hàng trên Pallet";
            this.Load += new System.EventHandler(this.ConfirmQtyForExportView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ConfirmQtyForExportView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsConfirmQty)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dtgConfirmQty;
        private System.Windows.Forms.BindingSource bdsConfirmQty;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniFeature;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPalletCode;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem mniDelete;
        private System.Windows.Forms.MenuItem mniEndDelete;

    }
}