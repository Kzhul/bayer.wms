﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ClearPalletView : Form
    {
        private BarcodeReader _barcodeReader;

        public ClearPalletView()
        {
            InitializeComponent();
            InitDataGridView();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void ClearPalletView_Closed(object sender, EventArgs e)
        {
            lblStepHints.Text = "Quét mã Pallet để clear";
        }

        public void ClearPalletView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            Pallet.Clear(lblPallet.Text, lblProductLot.Text);

            bdsPalletSummary.DataSource = null;
            lblPallet.Text = String.Empty;
            lblProductLot.Text = String.Empty;
            lblStepHints.Text = "Quét mã Pallet để clear";
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));

            dtgPallet.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (Utility.ProductLotRegex.IsMatch(barcode))
                {
                    lblProductLot.Text = barcode;
                }
                else
                {
                    string type;
                    barcode = Barcode.Process(barcode, true, false, false, out type);

                    lblPallet.Text = barcode;

                    var dt = Pallet.LoadRequirePalletStatus(barcode, type);
                    dt.TableName = "PalletSummary";
                    bdsPalletSummary.DataSource = dt;

                    lblStepHints.Text = "Xác nhận để clear pallet" + Environment.NewLine + "Quét mã lô để chỉ clear lô đó khỏi pallet";
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }
    }
}