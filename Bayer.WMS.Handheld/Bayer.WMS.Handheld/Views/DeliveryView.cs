﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    public partial class DeliveryView : BaseForm
    {
        private ProcessDelivery _process;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public DeliveryView()
            : base(Utility.DeliveryView)
        {
            InitializeComponent();

            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Vui lòng quét mã NV",
                "Vui lòng quét mã phiếu giao hàng",
                "Vui lòng quét biển số xe",
                "Vui lòng quét mã pallet"
            };

            lblStepHints.Text = _stepHints[0];
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);

                if (String.IsNullOrEmpty(barcode.Trim()))
                    return;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                else if (Utility.DeliveryRegex.IsMatch(barcode))
                    ProcessDeliveryNoteBarcode(barcode);
                else if (Utility.TruckNoRegex.IsMatch(barcode))
                    ProcessTruckNoBarcode(barcode);
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
                else
                    throw new Exception("Mã không hợp lệ, vui lòng kiểm tra lại dữ liệu.");
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", lblDeliveryCode.Text, barcode, ex.Message);
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), descr);
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessDeliveryNoteBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            lblDeliveryCode.Text = String.Empty;
            lblTruckNo.Text = String.Empty;
            lblCompanyName.Text = String.Empty;

            _process = new ProcessDelivery();
            _process.Delivery = DeliveryNote.LoadDeliveryNote(barcode);
            _process.DeliveredDetails = DeliveryNote.LoadDeliveryNote_Delivered(barcode);

            lblPallet.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredPallet, _process.Delivery.TotalPallet);

            var deliveredRow = _process.DeliveredDetails.Select("PackingType = 'C'").FirstOrDefault();
            _process.Delivery.DeliveredBoxes = deliveredRow == null ? 0 : decimal.Parse(deliveredRow["Quantity"].ToString());
            lblCarton.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredBoxes, _process.Delivery.TotalBoxes);

            deliveredRow = _process.DeliveredDetails.Select("PackingType = 'S'").FirstOrDefault();
            _process.Delivery.DeliveredBuckets = deliveredRow == null ? 0 : decimal.Parse(deliveredRow["Quantity"].ToString());
            lblShove.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredBuckets, _process.Delivery.TotalBuckets);

            deliveredRow = _process.DeliveredDetails.Select("PackingType = 'A'").FirstOrDefault();
            _process.Delivery.DeliveredCans = deliveredRow == null ? 0 : decimal.Parse(deliveredRow["Quantity"].ToString());
            lblCan.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredCans, _process.Delivery.TotalCans);

            deliveredRow = _process.DeliveredDetails.Select("PackingType = 'B'").FirstOrDefault();
            _process.Delivery.DeliveredBags = deliveredRow == null ? 0 : decimal.Parse(deliveredRow["Quantity"].ToString());
            lblBag.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredBags, _process.Delivery.TotalBags);

            var executor = Utility.LoadDeliverOrder_Executor(_process.Delivery.DOImportCode, "P");
            if (executor.Select(String.Format("UserID = {0}", Utility.UserID)).Length > 0)
                throw new Exception("Nhân viên này đã thực hiện soạn hàng nên không thể giao hàng.");

            lblDeliveryCode.Text = _process.Delivery.DeliveryTicketCode;
            lblCompanyName.Text = _process.Delivery.CompanyName;

            _scannedValue.Clear();

            if (_process.IsComplete())
                lblStepHints.Text = "Giao hàng đã hoàn tất";
            else
                lblStepHints.Text = _stepHints[2];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanDeliveryNote, this.GetType().GetMethod("ProcessDeliveryNoteBarcode"), barcode);
        }

        public void ProcessTruckNoBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");
            if (String.IsNullOrEmpty(lblDeliveryCode.Text))
                throw new Exception("Vui lòng quét mã phiếu giao hàng trước.");

            barcode = barcode.Remove(0, 3);
            if (!("," + _process.Delivery.TruckNo + ",").ToLower().Contains(barcode.ToLower()))
                throw new Exception("Xe không đúng với phiếu giao, vui lòng kiểm tra lại dữ liệu.");

            lblTruckNo.Text = barcode;

            lblStepHints.Text = _stepHints[3];

            tabControl1.SelectedIndex = 1;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanTruckNo, this.GetType().GetMethod("ProcessTruckNoBarcode"), String.Format("{0} - {1}", lblDeliveryCode.Text, barcode));
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(lblEmployeeName.Text))
                    throw new Exception("Vui lòng quét mã nhân viên trước.");
                if (String.IsNullOrEmpty(lblDeliveryCode.Text))
                    throw new Exception("Vui lòng quét mã phiếu giao hàng trước.");
                if (String.IsNullOrEmpty(lblTruckNo.Text))
                    throw new Exception("Vui lòng quét mã số xe trước.");

                _process.Process(barcode);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            if (!ShowConfirmDeliveryQty())
                return;

            //// update splitted qty on screen
            _process.UpdateDeliveredQty(lblTruckNo.Text, barcode);

            var row = _process.PalletStatusesSummary.Select("PackingType = 'C'").FirstOrDefault();
            if (row != null)
            {
                _process.Delivery.DeliveredBoxes += decimal.Parse(row["Quantity"].ToString());
                lblCarton.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredBoxes, _process.Delivery.TotalBoxes);
            }

            row = _process.PalletStatusesSummary.Select("PackingType = 'S'").FirstOrDefault();
            if (row != null)
            {
                _process.Delivery.DeliveredBuckets += decimal.Parse(row["Quantity"].ToString());
                lblShove.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredBuckets, _process.Delivery.TotalBuckets);
            }

            row = _process.PalletStatusesSummary.Select("PackingType = 'A'").FirstOrDefault();
            if (row != null)
            {
                _process.Delivery.DeliveredCans += decimal.Parse(row["Quantity"].ToString());
                lblCan.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredCans, _process.Delivery.TotalCans);
            }

            row = _process.PalletStatusesSummary.Select("PackingType = 'B'").FirstOrDefault();
            if (row != null)
            {
                _process.Delivery.DeliveredBags += decimal.Parse(row["Quantity"].ToString());
                lblBag.Text = String.Format("{0:N0} / {1:N0}", _process.Delivery.DeliveredBags, _process.Delivery.TotalBags);
            }

            if (_process.IsComplete())
                lblStepHints.Text = "Giao hàng đã hoàn tất";

            _scannedValue.Add(barcode);

            this.Refresh();
        }

        public bool ShowConfirmDeliveryQty()
        {
            try
            {
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForDeliveryView())
                {
                    view.Process = _process;

                    return (view.ShowDialog() == DialogResult.OK);
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}