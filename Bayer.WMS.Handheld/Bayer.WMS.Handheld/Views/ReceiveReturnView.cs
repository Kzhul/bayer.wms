﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;

namespace Bayer.WMS.Handheld.Views
{
    /// <summary>
    /// step 1: scan employee
    /// step 2: scan export note
    /// step 3: scan pallet
    /// step 4: confirm quantity
    /// step 5: scan end
    /// </summary>
    public partial class ReceiveReturnView : BaseForm
    {
        private ProcessReceiveReturn _process;

        public ReceiveReturnView()
            : base(Utility.ReceiveView)
        {
            InitializeComponent();
            InitDataGridView();

            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã phiếu trả hàng",
                "Quét mã pallet"
            };

            lblStepHints.Text = _stepHints[0];
        }

        protected override void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal exportedQty = decimal.Parse(_dt.Rows[e.Row]["ExportedReturnQty"].ToString());
            decimal receivedQty = decimal.Parse(_dt.Rows[e.Row]["ReceivedReturnQty"].ToString());

            if (receivedQty == 0)
                e.MeetsCriteria = 0;
            else if (receivedQty < exportedQty)
                e.MeetsCriteria = 1;
            else
                e.MeetsCriteria = 2;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ExportReturnList" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL trả", "ExportedReturnQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL nhận", "ReceivedReturnQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "BatchCode", 60, String.Empty, CheckCellEquals));

            dtgReceiveGood.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                else if (Utility.ExportReturnRegex.IsMatch(barcode))
                    ProcessExportReturnNoteBarcode(barcode);
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"),
                    String.Format("{0} - {1}", barcode, ex.Message));

                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessExportReturnNoteBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            lblExportCode.Text = String.Empty;
            bdsReceiveGood.DataSource = null;

            _process = new ProcessReceiveReturn();
            _process.ExportReturnNoteDetails = Utility.LoadExportNote(barcode);
            _process.ExportReturnNoteDetails.TableName = "ExportReturnList";

            var executor = Utility.LoadDeliverOrder_Executor(_dt.Rows[0]["DOImportCode"].ToString(), "E");
            if (executor.Select(String.Format("UserID = {0}", Utility.UserID)).Length > 0)
                throw new Exception("Nhân viên này đã thực hiện xuất hàng nên không thể nhận hàng.");

            lblExportCode.Text = _process.ExportReturnCode;
            bdsReceiveGood.DataSource = _process.ExportReturnNoteDetails;

            _scannedValue.Clear();

            if (IsComplete())
                lblStepHints.Text = "Nhận hàng trả đã hoàn tất";
            else
                lblStepHints.Text = _stepHints[2];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanExportNote, this.GetType().GetMethod("ProcessExportReturnNoteBarcode"), barcode);
        }

        public void ProcessPalletBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");
            if (String.IsNullOrEmpty(lblExportCode.Text))
                throw new Exception("Vui lòng quét mã phiếu trả hàng trước.");

            //// get quantity details of pallet/carton/product
            _process.Process(barcode);

            if (!ShowConfirmReceiveQty())
                return;

            _process.UpdateReceivedReturnQty();

            if (_process.IsComplete())
                lblStepHints.Text = "Nhận hàng trả đã hoàn tất";

            _scannedValue.Add(barcode);

            this.Refresh();
        }

        public bool ShowConfirmReceiveQty()
        {
            try
            {
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForReceiveReturnView())
                {
                    view.Process = _process;

                    return (view.ShowDialog() == DialogResult.OK);
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}