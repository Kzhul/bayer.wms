﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using System.Reflection;
using Bayer.WMS.Handheld.Models;
using System.Threading;
using Bayer.WMS.Handheld.Process.MainProcess;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ExportView : BaseForm
    {
        private ProcessExport _process;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public ExportView()
            : base(Utility.ExportView)
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã phiếu xuất hàng",
                "Quét mã pallet có hàng để xuất chẳn" + Environment.NewLine + "Quét mã pallet trống để xuất lẻ"
            };

            lblStepHints.Text = _stepHints[0];
        }

        protected override void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal remainQty = decimal.Parse(_process.ExportNoteDetails.Rows[e.Row]["RemainQty"].ToString());
            decimal exportedQty = decimal.Parse(_process.ExportNoteDetails.Rows[e.Row]["ExportedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < remainQty)
                e.MeetsCriteria = 1;
            else
                e.MeetsCriteria = 2;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniClearPallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new ClearPalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergePallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new MergePalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergeCarton_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new MergeCartonView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ExportList" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL y.cầu", "RemainQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL xuất", "ExportedQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "BatchCode", 60, String.Empty, CheckCellEquals));

            dtgExportGood.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                //// step 1: scan employee
                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                //// step 2: scan export note
                else if (Utility.ExportRegex.IsMatch(barcode))
                    ProcessExportNoteBarcode(barcode);
                //// step 3: scan pallet/carton/product
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", lblExportCode.Text, barcode, ex.Message);
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), descr);
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessExportNoteBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            lblExportCode.Text = String.Empty;
            bdsExportGood.DataSource = null;

            _process = new ProcessExport();
            _process.ExportNoteDetails = Utility.LoadExportNote(barcode);
            _process.ExportNoteDetails.TableName = "ExportList";

            _process.PalletStatusesDetails = Pallet.LoadPalletStatuses(_process.ReferenceNbr);

            lblExportCode.Text = _process.ReferenceNbr;
            bdsExportGood.DataSource = _process.ExportNoteDetails;

            _scannedValue.Clear();

            if (_process.IsComplete())
                lblStepHints.Text = "Xuất hàng đã hoàn tất";
            else
                lblStepHints.Text = _stepHints[2];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanExportNote, this.GetType().GetMethod("ProcessExportNoteBarcode"), barcode);
        }

        public void ProcessProductBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(lblEmployeeName.Text))
                    throw new Exception("Vui lòng quét mã nhân viên trước.");
                if (String.IsNullOrEmpty(lblExportCode.Text))
                    throw new Exception("Vui lòng quét mã phiếu xuất hàng trước.");

                //// get quantity details of pallet/carton/product
                _process.Process(barcode);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            DataTable summary, details;
            if (!ShowConfirmExportQty(out summary, out details))
                return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                //// update confirm qty on screen
                _process.UpdateExportedQty(summary, details);

                if (_process.IsComplete())
                    lblStepHints.Text = "Xuất hàng đã hoàn tất";

                this.Refresh();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public bool ShowConfirmExportQty(out DataTable summary, out DataTable details)
        {
            try
            {
                summary = new DataTable();
                details = new DataTable();
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForExportView())
                {
                    view.Process = _process;
                    if (view.ShowDialog() == DialogResult.OK)
                    {
                        summary = view.Summary;
                        details = view.Details;
                        return true;
                    }
                    return false;
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}