﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ConfirmQtyForReceiveReturnWithoutDocumentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsConfirmQty = new System.Windows.Forms.BindingSource(this.components);
            this.dtgConfirmQty = new System.Windows.Forms.DataGrid();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblCarton = new System.Windows.Forms.Label();
            this.lblCan = new System.Windows.Forms.Label();
            this.lblShove = new System.Windows.Forms.Label();
            this.lblBag = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEach = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.bdsConfirmQty)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgConfirmQty
            // 
            this.dtgConfirmQty.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgConfirmQty.DataSource = this.bdsConfirmQty;
            this.dtgConfirmQty.Location = new System.Drawing.Point(0, 93);
            this.dtgConfirmQty.Name = "dtgConfirmQty";
            this.dtgConfirmQty.RowHeadersVisible = false;
            this.dtgConfirmQty.Size = new System.Drawing.Size(240, 125);
            this.dtgConfirmQty.TabIndex = 4;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 9;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 8;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 221);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 47);
            this.lblStepHints.Text = "Quét mã thùng/lẻ để x.nhận s.lượng";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 21);
            this.label1.Text = "Thùng:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(115, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 21);
            this.label2.Text = "Xô:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 21);
            this.label4.Text = "Can:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(115, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 21);
            this.label5.Text = "Bao:";
            // 
            // lblCarton
            // 
            this.lblCarton.Location = new System.Drawing.Point(59, 27);
            this.lblCarton.Name = "lblCarton";
            this.lblCarton.Size = new System.Drawing.Size(50, 21);
            this.lblCarton.Text = "0";
            // 
            // lblCan
            // 
            this.lblCan.Location = new System.Drawing.Point(59, 48);
            this.lblCan.Name = "lblCan";
            this.lblCan.Size = new System.Drawing.Size(50, 21);
            this.lblCan.Text = "0";
            // 
            // lblShove
            // 
            this.lblShove.Location = new System.Drawing.Point(156, 27);
            this.lblShove.Name = "lblShove";
            this.lblShove.Size = new System.Drawing.Size(50, 21);
            this.lblShove.Text = "0";
            // 
            // lblBag
            // 
            this.lblBag.Location = new System.Drawing.Point(156, 48);
            this.lblBag.Name = "lblBag";
            this.lblBag.Size = new System.Drawing.Size(50, 21);
            this.lblBag.Text = "0";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 21);
            this.label6.Text = "Lẻ:";
            // 
            // lblEach
            // 
            this.lblEach.Location = new System.Drawing.Point(59, 69);
            this.lblEach.Name = "lblEach";
            this.lblEach.Size = new System.Drawing.Size(50, 21);
            this.lblEach.Text = "0";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Enabled = false;
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // ConfirmQtyForReceiveReturnWithoutDocumentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblEach);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblBag);
            this.Controls.Add(this.lblShove);
            this.Controls.Add(this.lblCan);
            this.Controls.Add(this.lblCarton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.dtgConfirmQty);
            this.Menu = this.mainMenu1;
            this.Name = "ConfirmQtyForReceiveReturnWithoutDocumentView";
            this.Text = "Xác nhận số lượng";
            this.Load += new System.EventHandler(this.ConfirmQtyForReceivewView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ConfirmQtyForReceivewView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsConfirmQty)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dtgConfirmQty;
        private System.Windows.Forms.BindingSource bdsConfirmQty;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblCarton;
        private System.Windows.Forms.Label lblCan;
        private System.Windows.Forms.Label lblShove;
        private System.Windows.Forms.Label lblBag;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblEach;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;

    }
}