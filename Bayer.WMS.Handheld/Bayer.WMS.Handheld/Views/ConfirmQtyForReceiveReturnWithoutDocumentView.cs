﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ConfirmQtyForReceiveReturnWithoutDocumentView : Form
    {
        private ProcessReceiveReturnWithouDocument _process;
        private bool _isComplete;
        private List<string> _scannedValue;
        private BarcodeReader _barcodeReader;

        public ConfirmQtyForReceiveReturnWithoutDocumentView()
        {
            InitializeComponent();
            InitDataGridView();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            _scannedValue = new List<string>();
        }

        public void ConfirmQtyForReceivewView_Load(object sender, EventArgs e)
        {
            lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để xác nhận";
        }

        public void ConfirmQtyForReceivewView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isComplete)
                    throw new Exception("Bạn chưa xác nhận đủ pallet.");

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        public void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ConfirmQty" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lượng", "Quantity", 30, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/Lẻ", "EncryptedBarcode", 100, String.Empty, null));

            dtgConfirmQty.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                string type;
                string packingType;
                int oddQty;
                _process.ProcessCfm(barcode, out type, out packingType, out oddQty, out _isComplete);

                switch (packingType)
                {
                    case "C":
                        if (oddQty == 0)
                            lblCarton.Text = (int.Parse(lblCarton.Text) - 1).ToString();
                        else
                            lblEach.Text = (int.Parse(lblEach.Text) - 1).ToString();
                        break;
                    case "S":
                        lblShove.Text = (int.Parse(lblShove.Text) - 1).ToString();
                        break;
                    case "A":
                        lblCan.Text = (int.Parse(lblCan.Text) - 1).ToString();
                        break;
                    case "B":
                        lblBag.Text = (int.Parse(lblBag.Text) - 1).ToString();
                        break;
                }

                if (_isComplete)
                {
                    mniConfirm.Enabled = true;
                    lblStepHints.Text = "Xác nhận số lượng";
                }
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"),
                    String.Format("{0} - {1}", barcode, ex.Message));

                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public ProcessReceiveReturnWithouDocument Process
        {
            set
            {
                _process = value;
                _process.Temp.TableName = "ConfirmQty";

                bdsConfirmQty.DataSource = _process.Temp;

                var rows = _process.Temp.Select("PackingType = 'C' AND OddQuantity = 0");
                lblCarton.Text = rows.Length.ToString();

                rows = _process.Temp.Select("PackingType = 'S'");
                lblShove.Text = rows.Length.ToString();

                rows = _process.Temp.Select("PackingType = 'A'");
                lblCan.Text = rows.Length.ToString();

                rows = _process.Temp.Select("PackingType = 'B'");
                lblBag.Text = rows.Length.ToString();

                rows = _process.Temp.Select("PackingType = 'C' AND OddQuantity > 0");
                lblEach.Text = rows.Length.ToString();
            }
        }
    }
}