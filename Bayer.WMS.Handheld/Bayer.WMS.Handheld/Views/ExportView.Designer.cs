﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ExportView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mniMergeCarton = new System.Windows.Forms.MenuItem();
            this.mniClearPallet = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtgExportGood = new System.Windows.Forms.DataGrid();
            this.label3 = new System.Windows.Forms.Label();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblExportCode = new System.Windows.Forms.Label();
            this.mniMergePallet = new System.Windows.Forms.MenuItem();
            bdsExportGood = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(bdsExportGood)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.mniClearPallet);
            this.menuItem1.MenuItems.Add(this.mniMergePallet);
            this.menuItem1.MenuItems.Add(this.mniMergeCarton);
            this.menuItem1.Text = "Chức năng";
            // 
            // mniMergeCarton
            // 
            this.mniMergeCarton.Text = "Dồn thùng";
            this.mniMergeCarton.Click += new System.EventHandler(this.mniMergeCarton_Click);
            // 
            // mniClearPallet
            // 
            this.mniClearPallet.Text = "Clear pallet";
            this.mniClearPallet.Click += new System.EventHandler(this.mniClearPallet_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 21);
            this.label1.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 21);
            this.label2.Text = "XH:";
            // 
            // dtgExportGood
            // 
            this.dtgExportGood.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgExportGood.DataSource = bdsExportGood;
            this.dtgExportGood.Location = new System.Drawing.Point(0, 72);
            this.dtgExportGood.Name = "dtgExportGood";
            this.dtgExportGood.RowHeadersVisible = false;
            this.dtgExportGood.Size = new System.Drawing.Size(240, 158);
            this.dtgExportGood.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "NV:";
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã pallet có hàng để xuất nhiều\r\nQuét mã pallet trống để xuất lẻ";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Location = new System.Drawing.Point(34, 27);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(203, 21);
            // 
            // lblExportCode
            // 
            this.lblExportCode.Location = new System.Drawing.Point(34, 48);
            this.lblExportCode.Name = "lblExportCode";
            this.lblExportCode.Size = new System.Drawing.Size(203, 21);
            // 
            // mniMergePallet
            // 
            this.mniMergePallet.Text = "Dồn pallet";
            this.mniMergePallet.Click += new System.EventHandler(this.mniMergePallet_Click);
            // 
            // ExportView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblExportCode);
            this.Controls.Add(this.lblEmployeeName);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgExportGood);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "ExportView";
            this.Text = "Xuất hàng";
            ((System.ComponentModel.ISupportInitialize)(bdsExportGood)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.DataGrid dtgExportGood;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblExportCode;
        private System.Windows.Forms.MenuItem mniMergeCarton;
        private System.Windows.Forms.MenuItem mniClearPallet;
        private System.Windows.Forms.MenuItem mniMergePallet;
        private System.Windows.Forms.BindingSource bdsExportGood;
    }
}