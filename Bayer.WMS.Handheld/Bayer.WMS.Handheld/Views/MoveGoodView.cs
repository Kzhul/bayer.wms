﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.SubProcess;
using Intermec.DataCollection;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    public partial class MoveGoodView : Form
    {
        private int _step = 1;
        private string _status;
        private bool _isProcess = false;
        private DataTable _dtMovedItems;
        private DataTable _dtCurrentItems;
        private List<string> _scannedValue;
        private BarcodeReader _barcodeReader;
        private Queue<string> _queue;

        public MoveGoodView()
        {
            InitializeComponent();
            InitDataGridView();

            _dtMovedItems = new DataTable();
            _dtMovedItems.TableName = "MoveItems";
            _dtMovedItems.Columns.Add("Barcode", typeof(string));
            _dtMovedItems.Columns.Add("EncryptedProductBarcode", typeof(string));
            _dtMovedItems.Columns.Add("Type", typeof(string));
            _dtMovedItems.Columns.Add("ProductLot", typeof(string));
            _dtMovedItems.Columns.Add("ProductID", typeof(int));
            _dtMovedItems.Columns.Add("ProductCode", typeof(string));
            _dtMovedItems.Columns.Add("ProductDescription", typeof(string));
            _dtMovedItems.Columns.Add("ProductQty", typeof(int));

            bindingSource1.DataSource = _dtMovedItems;

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            _scannedValue = new List<string>();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "MoveItems" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lượng", "ProductQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "EncryptedProductBarcode", 100, String.Empty, null));

            dtgMoveItems.TableStyles.Add(dtgStyle);
        }

        private void MoveGoodView_Load(object sender, EventArgs e)
        {

        }

        private void MoveGoodView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void mniDelete_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode("huy");
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniEndDelete_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode("ket_thuc_huy");
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã QR này đã quét, vui lòng quét mã khác.");

                if (barcode == "ket_thuc_huy")
                {
                    _step = 1;

                    lblStepHints.Text = "Quét mã thùng/đơn vị lẻ";
                    lblStepHints.BackColor = Color.LimeGreen;
                    Utility.RecordAuditTrail(null, AuditTrailAction.ScanEndDeletePackaging, this.GetType().GetMethod("ProcessBarcode"), barcode);
                }
                else if (barcode == "huy")
                {
                    _step = 0;

                    lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để hủy";
                    lblStepHints.BackColor = Color.Red;
                    Utility.RecordAuditTrail(null, AuditTrailAction.ScanDeletePackaging, this.GetType().GetMethod("ProcessBarcode"), barcode);
                }
                else
                {
                    string type;
                    barcode = Barcode.Process(barcode, false, true, true, out type);

                    if (_step == 0)
                        ProcessCancelMoveItem(barcode, type);
                    else
                        ProcessMoveItem(barcode, type);
                }
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1}", barcode, ex.Message);
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), descr);
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public void ProcessCancelMoveItem(string barcode, string type)
        {
            if (MessageBox.Show("Bạn có chắc chắn muốn hủy?", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                return;

            string filterExp = String.Format("Barcode = '{0}' AND Type = '{1}'", barcode, type);
            var row = _dtMovedItems.Select(filterExp).FirstOrDefault();
            if (row == null)
                throw new Exception("Mã không hợp lệ, vui lòng kiểm tra lại dữ liệu.");

            _dtMovedItems.Rows.Remove(row);
            _scannedValue.Remove(barcode);

            switch (type)
            {
                case "C":
                    lblCarton.Text = (int.Parse(lblCarton.Text) - 1).ToString();
                    break;
                case "E":
                    lblEach.Text = (int.Parse(lblEach.Text) - 1).ToString();
                    break;
            }

            string action = type == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;
            Utility.RecordAuditTrail(null, action, this.GetType().GetMethod("ProcessCancelMoveItem"), barcode);
        }

        public void ProcessMoveItem(string barcode, string type)
        {
            string filterExp = String.Format("{0} = '{1}'", type == "C" ? "CartonBarcode" : "ProductBarcode", barcode);
            var moveRows = _dtCurrentItems.Select(filterExp);
            if (moveRows.Length == 0)
                throw new Exception("Mã hiện tại không nằm trên pallet, vui lòng kiểm tra lại dữ liệu.");

            int qty = (int)moveRows.Sum(p => int.Parse(p["Qty"].ToString()));

            if (type == "C")
                _dtMovedItems.Rows.Add(moveRows[0]["CartonBarcode"], moveRows[0]["CartonBarcode"], type, moveRows[0]["ProductLot"],
                    moveRows[0]["ProductID"], moveRows[0]["ProductCode"], moveRows[0]["ProductDescription"], qty);
            else
                _dtMovedItems.Rows.Add(moveRows[0]["ProductBarcode"], moveRows[0]["EncryptedProductBarcode"], type, moveRows[0]["ProductLot"],
                    moveRows[0]["ProductID"], moveRows[0]["ProductCode"], moveRows[0]["ProductDescription"], qty);

            _scannedValue.Add(barcode);

            switch (type)
            {
                case "C":
                    lblCarton.Text = (int.Parse(lblCarton.Text) + 1).ToString();
                    break;
                case "E":
                    lblEach.Text = (int.Parse(lblEach.Text) + 1).ToString();
                    break;
            }

            string action = type == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;
            Utility.RecordAuditTrail(null, action, this.GetType().GetMethod("ProcessMoveItem"), barcode);
        }

        public string Status
        {
            set { _status = value; }
        }

        public DataTable MovedItems
        {
            get { return _dtMovedItems; }
        }

        public DataTable CurrentItems
        {
            set { _dtCurrentItems = value; }
        }
    }
}