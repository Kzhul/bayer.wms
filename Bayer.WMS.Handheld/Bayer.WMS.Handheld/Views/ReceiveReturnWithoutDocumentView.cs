﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ReceiveReturnWithoutDocumentView : BaseForm
    {
        private ProcessReceiveReturnWithouDocument _process;

        public ReceiveReturnWithoutDocumentView()
            : base(Utility.ReceiveView)
        {
            InitializeComponent();
            InitDataGridView();

            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã pallet"
            };

            lblStepHints.Text = _stepHints[0];

            _dt = new DataTable();
            _dt.TableName = "ExportReturnList";
            _dt.Columns.Add("BatchCode", typeof(string));
            _dt.Columns.Add("ProductID", typeof(int));
            _dt.Columns.Add("ProductCode", typeof(string));
            _dt.Columns.Add("ProductDescription", typeof(string));
            _dt.Columns.Add("ReturnedQty", typeof(decimal));

            bdsReceiveReturnGood.DataSource = _dt;

            _process = new ProcessReceiveReturnWithouDocument();
            _process.ExportReturnNoteDetails = _dt;
        }

        protected override void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ExportReturnList" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL trả", "ExportedReturnQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "BatchCode", 60, String.Empty, null));

            dtgReceiveReturnGood.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"),
                    String.Format("{0} - {1}", barcode, ex.Message));

                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessPalletBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            //// get quantity details of pallet/carton/product
            _process.Process(barcode);

            if (!ShowConfirmReceiveQty())
                return;

            _process.UpdateReceivedReturnQty();

            if (_process.IsComplete())
                lblStepHints.Text = "Nhận hàng trả đã hoàn tất";

            _scannedValue.Add(barcode);

            this.Refresh();
        }

        public bool ShowConfirmReceiveQty()
        {
            try
            {
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForReceiveReturnWithoutDocumentView())
                {
                    view.Process = _process;

                    return (view.ShowDialog() == DialogResult.OK);
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}