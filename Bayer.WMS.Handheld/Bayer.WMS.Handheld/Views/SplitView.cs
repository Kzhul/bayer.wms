﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    /// <summary>
    /// step 1: scan employee
    /// step 2: scan split note
    /// step 3: scan pallet
    /// step 4: show popup about quantity
    /// </summary>
    public partial class SplitView : BaseForm
    {
        private ProcessSplit _process;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public SplitView()
            : base(Utility.SplitView)
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã phiếu chia hàng",
                "Quét mã pallet n.hàng để chia nguyên" + Environment.NewLine + "Quét mã pallet trống/đã chia để san hàng",
                "Quét mã khách hàng",
            };

            lblStepHints.Text = _stepHints[0];
        }

        protected override void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal quantity = decimal.Parse(_process.SplitNoteDetails.Rows[e.Row]["Quantity"].ToString());
            decimal splittedQty = decimal.Parse(_process.SplitNoteDetails.Rows[e.Row]["SplittedQty"].ToString());

            if (splittedQty == 0)
                e.MeetsCriteria = 0;
            else if (splittedQty < quantity)
                e.MeetsCriteria = 1;
            else
                e.MeetsCriteria = 2;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniClearPallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new ClearPalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergePallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new MergePalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergeCarton_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new MergeCartonView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "SplitNoteList" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL y.cầu", "Quantity", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL đã chia", "SplittedQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "BatchCode", 60, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Tên KH", "CompanyName", 250, String.Empty, CheckCellEquals));

            dtgSplitGood.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                else if (Utility.SplitRegex.IsMatch(barcode))
                    ProcessSplitNoteBarcode(barcode);
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", lblSplitCode.Text, barcode, ex.Message);
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), descr);
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            Cursor.Current = Cursors.WaitCursor;

            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessSplitNoteBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            lblSplitCode.Text = String.Empty;
            bdsSplitGood.DataSource = null;

            _process = new ProcessSplit();
            _process.SplitNoteDetails = Utility.LoadSplitNote(barcode);
            _process.SplitNoteDetails.TableName = "SplitNoteList";

            var executor = Utility.LoadDeliverOrder_Executor(_process.DOImportCode, "R");
            if (executor.Select(String.Format("UserID = {0}", Utility.UserID)).Length > 0)
                throw new Exception("Nhân viên này đã thực hiện nhận hàng nên không thể chia hàng.");

            _process.PalletStatusesDetails = Pallet.LoadPalletStatuses(_process.ReferenceNbr);

            lblSplitCode.Text = _process.ReferenceNbr;
            bdsSplitGood.DataSource = _process.SplitNoteDetails;

            _scannedValue.Clear();

            if (_process.IsComplete())
                lblStepHints.Text = "Chia hàng đã hoàn tất";
            else
                lblStepHints.Text = _stepHints[2];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanSplitNote, this.GetType().GetMethod("ProcessSplitNoteBarcode"), barcode);
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(lblEmployeeName.Text))
                    throw new Exception("Vui lòng quét mã nhân viên trước.");
                if (String.IsNullOrEmpty(lblSplitCode.Text))
                    throw new Exception("Vui lòng quét mã phiếu chia hàng trước.");

                _process.Process(barcode);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            DataTable summary, details;
            if (!ShowConfirmSplitQty(out summary, out details))
                return;

            //// update confirm qty on screen
            _process.UpdateSplittedQty(summary, details);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (_process.IsComplete())
                    lblStepHints.Text = "Chia hàng đã hoàn tất";

                this.Refresh();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public bool ShowConfirmSplitQty(out DataTable summary, out DataTable details)
        {
            try
            {
                summary = new DataTable();
                details = new DataTable();
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForSplitView())
                {
                    view.Process = _process;

                    if (view.ShowDialog() == DialogResult.OK)
                    {
                        summary = view.Summary;
                        details = view.Details;
                        return true;
                    }

                    return false;
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}