﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    /// <summary>
    /// step 1: scan employee
    /// step 2: scan export note
    /// step 3: scan pallet
    /// step 4: confirm quantity
    /// step 5: scan end
    /// </summary>
    public partial class ReceiveView : BaseForm
    {
        private ProcessReceive _process;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public ReceiveView()
            : base(Utility.ReceiveView)
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã phiếu xuất hàng",
                "Quét mã pallet đã xuất để nhận hàng"
            };

            lblStepHints.Text = _stepHints[0];
        }

        protected override void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal exportedQty = decimal.Parse(_process.ExportNoteDetails.Rows[e.Row]["ExportedQty"].ToString());
            decimal receivedQty = decimal.Parse(_process.ExportNoteDetails.Rows[e.Row]["ReceivedQty"].ToString());

            if (receivedQty == 0)
                e.MeetsCriteria = 0;
            else if (receivedQty < exportedQty)
                e.MeetsCriteria = 1;
            else
                e.MeetsCriteria = 2;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void mniMergePallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new MergePalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergeCarton_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new MergeCartonView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ExportList" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL xuất", "ExportedQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL nhận", "ReceivedQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "BatchCode", 60, String.Empty, CheckCellEquals));

            dtgReceiveGood.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                else if (Utility.ExportRegex.IsMatch(barcode))
                    ProcessExportNoteBarcode(barcode);
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", lblExportCode.Text, barcode, ex.Message);
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), descr);
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessExportNoteBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            lblExportCode.Text = String.Empty;
            bdsReceiveGood.DataSource = null;

            _process = new ProcessReceive();
            _process.ExportNoteDetails = Utility.LoadExportNote(barcode);
            _process.ExportNoteDetails.TableName = "ExportList";

            var executor = Utility.LoadDeliverOrder_Executor(_process.DOImportCode, "E");
            if (executor.Select(String.Format("UserID = {0}", Utility.UserID)).Length > 0)
                throw new Exception("Nhân viên này đã thực hiện xuất hàng nên không thể nhận hàng.");

            lblExportCode.Text = _process.ReferenceNbr;
            bdsReceiveGood.DataSource = _process.ExportNoteDetails;

            _scannedValue.Clear();

            if (_process.IsComplete())
                lblStepHints.Text = "Nhận hàng đã hoàn tất";
            else
                lblStepHints.Text = _stepHints[2];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanExportNote, this.GetType().GetMethod("ProcessExportNoteBarcode"), barcode);
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                if (String.IsNullOrEmpty(lblEmployeeName.Text))
                    throw new Exception("Vui lòng quét mã nhân viên thực hiện trước.");
                if (String.IsNullOrEmpty(lblExportCode.Text))
                    throw new Exception("Vui lòng quét mã phiếu xuất hàng trước.");

                //// get quantity details of pallet/carton/product
                _process.Process(barcode);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            if (!ShowConfirmReceiveQty())
                return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                _process.UpdateReceivedQty();

                if (_process.IsComplete())
                    lblStepHints.Text = "Nhận hàng đã hoàn tất";

                _scannedValue.Add(barcode);

                this.Refresh();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public bool ShowConfirmReceiveQty()
        {
            try
            {
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForReceiveView())
                {
                    view.Process = _process;

                    return (view.ShowDialog() == DialogResult.OK);
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}