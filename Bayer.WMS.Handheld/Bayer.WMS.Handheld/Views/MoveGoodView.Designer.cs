﻿namespace Bayer.WMS.Handheld.Views
{
    partial class MoveGoodView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniFeature = new System.Windows.Forms.MenuItem();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniDelete = new System.Windows.Forms.MenuItem();
            this.mniEndDelete = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dtgMoveItems = new System.Windows.Forms.DataGrid();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.lblCarton = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEach = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniFeature);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniFeature
            // 
            this.mniFeature.MenuItems.Add(this.mniConfirm);
            this.mniFeature.MenuItems.Add(this.mniDelete);
            this.mniFeature.MenuItems.Add(this.mniEndDelete);
            this.mniFeature.Text = "Chức năng";
            // 
            // mniConfirm
            // 
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniDelete
            // 
            this.mniDelete.Text = "Hủy";
            this.mniDelete.Click += new System.EventHandler(this.mniDelete_Click);
            // 
            // mniEndDelete
            // 
            this.mniEndDelete.Text = "Kết thúc hủy";
            this.mniEndDelete.Click += new System.EventHandler(this.mniEndDelete_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 21);
            this.label3.Text = "Mã QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(59, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(122, 21);
            this.txtBarcode.TabIndex = 3;
            // 
            // dtgConfirmQty
            // 
            this.dtgMoveItems.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgMoveItems.DataSource = this.bindingSource1;
            this.dtgMoveItems.Location = new System.Drawing.Point(0, 53);
            this.dtgMoveItems.Name = "dtgConfirmQty";
            this.dtgMoveItems.RowHeadersVisible = false;
            this.dtgMoveItems.Size = new System.Drawing.Size(240, 165);
            this.dtgMoveItems.TabIndex = 6;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 221);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 47);
            this.lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để san hàng";
            // 
            // lblCarton
            // 
            this.lblCarton.Location = new System.Drawing.Point(59, 29);
            this.lblCarton.Name = "lblCarton";
            this.lblCarton.Size = new System.Drawing.Size(50, 21);
            this.lblCarton.Text = "0";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 21);
            this.label1.Text = "Thùng:";
            // 
            // lblEach
            // 
            this.lblEach.Location = new System.Drawing.Point(171, 29);
            this.lblEach.Name = "lblEach";
            this.lblEach.Size = new System.Drawing.Size(50, 21);
            this.lblEach.Text = "0";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(115, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 21);
            this.label6.Text = "Lẻ:";
            // 
            // MoveGoodView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblEach);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblCarton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.dtgMoveItems);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.KeyPreview = true;
            this.Menu = this.mainMenu1;
            this.Name = "MoveGoodView";
            this.Text = "Chuyển hàng";
            this.Load += new System.EventHandler(this.MoveGoodView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MoveGoodView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.DataGrid dtgMoveItems;
        private System.Windows.Forms.MenuItem mniFeature;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniDelete;
        private System.Windows.Forms.MenuItem mniEndDelete;
        private System.Windows.Forms.Label lblCarton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblEach;
        private System.Windows.Forms.Label label6;
    }
}