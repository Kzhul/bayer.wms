﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ConfirmQtyForSplitView : Form
    {
        private ProcessSplit _process;
        private string _status;
        /// <summary>
        /// 0: Scan customer for pallet.
        /// 1: Based on pallet, move carton or product on it.
        /// 2: Based on splitted pallet, move carton or product on it
        /// </summary>
        private int _state;
        private DataTable _summary;
        private DataTable _details;
        private List<string> _scannedValue;
        private BarcodeReader _barcodeReader;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public ConfirmQtyForSplitView()
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            _scannedValue = new List<string>();
        }

        public void ConfirmQtyForSplitView_Load(object sender, EventArgs e)
        {
            if (_status == Utility.PalletStatusNew || _status == Utility.PalletStatusReceived)
            {
                _state = 0;
                lblStepHints.Text = "Quét mã khách hàng";
            }
            else
            {
                _state = 1;
                lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để chia hàng";
                lblCustomerName.Text = _process.CompanyName;
            }
        }

        public void ConfirmQtyForSplitView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Bạn có chắc chắn?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                    return;

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        public void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            if (!String.IsNullOrEmpty(_process.CompanyCode))
            {
                decimal requestQty = decimal.Parse(_summary.Rows[e.Row]["RequestQty"].ToString());
                decimal splittedQty = decimal.Parse(_summary.Rows[e.Row]["ProductQty"].ToString());

                if (splittedQty == 0)
                    e.MeetsCriteria = 0;
                else if (splittedQty < requestQty)
                    e.MeetsCriteria = 1;
            else if (splittedQty > requestQty)
                e.MeetsCriteria = 3;
                else
                    e.MeetsCriteria = 2;
            }
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ConfirmQty" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL y.cầu", "RequestQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL chia", "ProductQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));

            dtgConfirmQty.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                bool isMoveItem;
                bool isPreparePromotion;
                _process.ProcessCfm(barcode, ref _state, out isMoveItem, out isPreparePromotion, ref _summary, ref _details);

                if (_state == 0)
                {
                    lblCustomerName.Text = _process.CompanyName;
                    lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để chia hàng" + Environment.NewLine + "Xác nhận số lượng chia hàng";

                    _state = 1;
                }
                else
                {
                    if (isMoveItem)
                        OpenMoveItemPopup();
                    else if (isPreparePromotion)
                    {
                        using (var view = new SplitPromotionView())
                        {
                            if (view.ShowDialog() != DialogResult.OK)
                                return;

                            //_process.ProcessPromotion(view.Qty);
                        }
                    }
                    else
                    {
                        lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để chia hàng" + Environment.NewLine + "Xác nhận số lượng chia hàng";
                        bdsConfirmQty.DataSource = _summary;
                        mniConfirm.Enabled = true;
                    }

                    SetHint();
                }
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", _process.ReferenceNbr, barcode, ex.Message);
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), descr);
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void OpenMoveItemPopup()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();

                using (var view = new MoveGoodView())
                {
                    view.CurrentItems = _details;
                    view.Status = Utility.PalletStatusSplitted;
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    _process.UpdateMoveToNewOrReceivedPallet(ref _summary, ref _details, view.MovedItems);
                }
            }
            finally
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
        }

        public void SetHint()
        {
            int state = _process.ValidateState(_summary);
            if (state == 2)
            {
                lblStepHints.Text = "Đã quá số lượng yêu cầu, quét mã pallet để san bớt hàng";
                lblStepHints.BackColor = Color.Red;

                Utility.PlayErrorSound();
            }
            else if (state == 1)
            {
                lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để chia hàng";
                lblStepHints.BackColor = Color.LimeGreen;
            }
            else
            {
                lblStepHints.Text = "Xác nhận số lượng";
                lblStepHints.BackColor = Color.LimeGreen;
            }
        }

        public ProcessSplit Process
        {
            set
            {
                _process = value;
                _status = _process.PalletStatusesSummary.Rows[0]["Status"].ToString();

                if (_process.PalletStatusesDetails != null)
                {
                    _details = _process.PalletStatusesDetails.Clone();
                    var rows = _process.PalletStatusesDetails.Select(String.Format("PalletCode = '{0}'", _process.CurrentBarcode));
                    foreach (DataRow row in rows)
                    {
                        _details.Rows.Add(row.ItemArray);
                    }
                }

                _summary = _process.PalletStatusesSummary.Copy();
                _summary.TableName = "ConfirmQty";

                //// only set datasource when datatable has value
                if (_summary.Select("ProductQty > 0").Length > 0)
                    bdsConfirmQty.DataSource = _summary;
            }
        }

        public DataTable Summary
        {
            get { return _summary; }
        }

        public DataTable Details
        {
            get { return _details; }
        }
    }
}