﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.IO.Ports;
using Bayer.WMS.Handheld.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class PrintPalletLabelView : Form
    {
        private bool _isPrintPalletLabel;
        private string _companyName;
        private DateTime _deliveryDate;
        private BarcodeReader _barcodeReader;
        private readonly SerialPort _serialPort;

        public PrintPalletLabelView()
        {
            InitializeComponent();
            InitDataGridView();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);

            Parity parity = Parity.None;
            switch (Utility.COM_Parity)
            {
                case "Even":
                    parity = Parity.Even;
                    break;
                case "Mark":
                    parity = Parity.Mark;
                    break;
                case "Odd":
                    parity = Parity.Odd;
                    break;
                case "Space":
                    parity = Parity.Space;
                    break;
                default:
                    break;
            }

            StopBits sb = StopBits.None;
            switch (Utility.COM_StopBits)
            {
                case "1":
                    sb = StopBits.One;
                    break;
                case "1.5":
                    sb = StopBits.OnePointFive;
                    break;
                case "2":
                    sb = StopBits.Two;
                    break;
                default:
                    break;
            }

            _serialPort = new SerialPort(Utility.COM_PortName, Utility.COM_BaudRate, parity, Utility.COM_DataBits, sb);
        }

        public void PrintPalletLabelView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Loại", "StrPackingType", 100, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lượng", "Quantity", 60, "N0", null));

            dtgConfirmQty.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                {
                    var dt = Pallet.LoadPalletStatusGroupByPackingType(barcode, Utility.PalletStatusPrepared);
                    dt.TableName = "PalletSummary";
                    bdsPalletSummary.DataSource = dt;

                    try
                    {
                        Print(barcode, dt);
                    }
                    catch (Exception ex)
                    {
                        Utility.PlayErrorSound();
                        MessageBox.Show(ex.Message, "Lỗi");
                    }

                    _isPrintPalletLabel = true;
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public void Print(string palletCode, DataTable dt)
        {
            try
            {
                if (!_serialPort.IsOpen)
                    _serialPort.Open();

                string template = Utility.PalletLabelTemplate;
                var quantities = new List<string>();
                foreach (DataRow row in dt.Rows)
                {
                    string packginType = row["PackingType"].ToString();
                    switch (packginType)
                    {
                        case "C":
                            packginType = "T";
                            break;
                        case "B":
                            packginType = "B";
                            break;
                        case "S":
                            packginType = "X";
                            break;
                        case "A":
                            packginType = "C";
                            break;
                        default:
                            break;
                    }
                    quantities.Add(String.Format("{0} {1}", row["Quantity"], packginType));
                }

                string text = template.Replace("{PalletCode}", palletCode);
                text = text.Replace("{CompanyName}", _companyName);
                text = text.Replace("{DeliveryDate}", _deliveryDate.ToString("dd-MM-yyyy"));
                text = text.Replace("{Quantity}", String.Join(",", quantities.ToArray()));

                byte[] b = Encoding.UTF8.GetBytes(text);
                _serialPort.Write(b, 0, b.Length);

                Utility.RecordAuditTrail(null, AuditTrailAction.PrintPalletLabel, this.GetType().GetMethod("Print"), palletCode);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();
            }
        }

        public bool IsPrintPalletLabel
        {
            get { return _isPrintPalletLabel; }
        }

        public string CompanyName
        {
            set { _companyName = value; }
        }

        public DateTime DeliveryDate
        {
            set { _deliveryDate = value; }
        }
    }
}