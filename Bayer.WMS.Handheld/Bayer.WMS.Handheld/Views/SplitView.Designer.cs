﻿namespace Bayer.WMS.Handheld.Views
{
    partial class SplitView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsSplitGood = new System.Windows.Forms.BindingSource(this.components);
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mniClearPallet = new System.Windows.Forms.MenuItem();
            this.mniMergePallet = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.dtgSplitGood = new System.Windows.Forms.DataGrid();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblSplitCode = new System.Windows.Forms.Label();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.bdsSplitGood)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.miExit);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.mniClearPallet);
            this.menuItem1.MenuItems.Add(this.mniMergePallet);
            this.menuItem1.MenuItems.Add(this.menuItem2);
            this.menuItem1.Text = "Chức năng";
            // 
            // mniClearPallet
            // 
            this.mniClearPallet.Text = "Clear pallet";
            this.mniClearPallet.Click += new System.EventHandler(this.mniClearPallet_Click);
            // 
            // mniMergePallet
            // 
            this.mniMergePallet.Text = "Dồn pallet";
            this.mniMergePallet.Click += new System.EventHandler(this.mniMergePallet_Click);
            // 
            // miExit
            // 
            this.miExit.Text = "Thoát";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 21);
            this.label1.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(36, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(145, 21);
            this.txtBarcode.TabIndex = 0;
            // 
            // dtgSplitGood
            // 
            this.dtgSplitGood.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgSplitGood.DataSource = this.bdsSplitGood;
            this.dtgSplitGood.Location = new System.Drawing.Point(0, 72);
            this.dtgSplitGood.Name = "dtgSplitGood";
            this.dtgSplitGood.RowHeadersVisible = false;
            this.dtgSplitGood.Size = new System.Drawing.Size(240, 138);
            this.dtgSplitGood.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 21);
            this.label3.Text = "NV:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 21);
            this.label4.Text = "CH:";
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 213);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 55);
            this.lblStepHints.Text = "Quét mã pallet n.hàng để chia nguyên\r\nQuét mã pallet trống/đã chia để san hàng";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Location = new System.Drawing.Point(36, 27);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(203, 21);
            // 
            // lblSplitCode
            // 
            this.lblSplitCode.Location = new System.Drawing.Point(36, 48);
            this.lblSplitCode.Name = "lblSplitCode";
            this.lblSplitCode.Size = new System.Drawing.Size(203, 21);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Dồn thùng";
            this.menuItem2.Click += new System.EventHandler(this.mniMergeCarton_Click);
            // 
            // SplitView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblSplitCode);
            this.Controls.Add(this.lblEmployeeName);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtgSplitGood);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "SplitView";
            this.Text = "Chia hàng";
            ((System.ComponentModel.ISupportInitialize)(this.bdsSplitGood)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bdsSplitGood;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.DataGrid dtgSplitGood;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblSplitCode;
        private System.Windows.Forms.MenuItem mniClearPallet;
        private System.Windows.Forms.MenuItem mniMergePallet;
        private System.Windows.Forms.MenuItem menuItem2;
    }
}

