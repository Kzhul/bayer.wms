﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ClearPalletView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPallet = new System.Windows.Forms.Label();
            this.bdsPalletSummary = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPallet = new System.Windows.Forms.DataGrid();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.lblProductLot = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 21);
            this.label1.Text = "Pallet:";
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(49, 27);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(188, 21);
            // 
            // dtgPallet
            // 
            this.dtgPallet.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPallet.DataSource = this.bdsPalletSummary;
            this.dtgPallet.Location = new System.Drawing.Point(0, 72);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.RowHeadersVisible = false;
            this.dtgPallet.Size = new System.Drawing.Size(240, 143);
            this.dtgPallet.TabIndex = 21;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 218);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 50);
            this.lblStepHints.Text = "Quét mã Pallet để clear\r\nQuét mã lô để chỉ clear lô đó khỏi pallet";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 21);
            this.label2.Text = "Lô:";
            // 
            // lblProductLot
            // 
            this.lblProductLot.Location = new System.Drawing.Point(49, 48);
            this.lblProductLot.Name = "lblProductLot";
            this.lblProductLot.Size = new System.Drawing.Size(188, 21);
            // 
            // ClearPalletView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblProductLot);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.dtgPallet);
            this.Controls.Add(this.lblPallet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "ClearPalletView";
            this.Text = "Clear Pallet";
            this.Closed += new System.EventHandler(this.ClearPalletView_Closed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ClearPalletView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.DataGrid dtgPallet;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsPalletSummary;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblProductLot;
    }
}