﻿namespace Bayer.WMS.Handheld.Views
{
    partial class DeliveryView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblTruckNo = new System.Windows.Forms.Label();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.lblDeliveryCode = new System.Windows.Forms.Label();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.lblPallet = new System.Windows.Forms.Label();
            this.lblEach = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblBag = new System.Windows.Forms.Label();
            this.lblShove = new System.Windows.Forms.Label();
            this.lblCan = new System.Windows.Forms.Label();
            this.lblCarton = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.miExit);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = " ";
            // 
            // miExit
            // 
            this.miExit.Text = "Thoát";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 19;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 21);
            this.label1.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "NV:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 21);
            this.label4.Text = "GH:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 20);
            this.label2.Text = "Xe:";
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 248);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 20);
            this.lblStepHints.Text = "Quét mã NV";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(0, 30);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 215);
            this.tabControl1.TabIndex = 37;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblTruckNo);
            this.tabPage1.Controls.Add(this.lblCompanyName);
            this.tabPage1.Controls.Add(this.lblDeliveryCode);
            this.tabPage1.Controls.Add(this.lblEmployeeName);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(240, 192);
            this.tabPage1.Text = "Thông tin";
            // 
            // lblTruckNo
            // 
            this.lblTruckNo.Location = new System.Drawing.Point(34, 109);
            this.lblTruckNo.Name = "lblTruckNo";
            this.lblTruckNo.Size = new System.Drawing.Size(203, 21);
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Location = new System.Drawing.Point(34, 46);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(203, 63);
            // 
            // lblDeliveryCode
            // 
            this.lblDeliveryCode.Location = new System.Drawing.Point(34, 25);
            this.lblDeliveryCode.Name = "lblDeliveryCode";
            this.lblDeliveryCode.Size = new System.Drawing.Size(203, 21);
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Location = new System.Drawing.Point(34, 4);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(203, 21);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 20);
            this.label5.Text = "KH:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.lblPallet);
            this.tabPage2.Controls.Add(this.lblEach);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.lblBag);
            this.tabPage2.Controls.Add(this.lblShove);
            this.tabPage2.Controls.Add(this.lblCan);
            this.tabPage2.Controls.Add(this.lblCarton);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(232, 189);
            this.tabPage2.Text = "Chi tiết";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(59, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(122, 21);
            this.label12.Text = "0";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(3, 4);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(50, 21);
            this.lblPallet.Text = "Pallet:";
            // 
            // lblEach
            // 
            this.lblEach.Location = new System.Drawing.Point(59, 109);
            this.lblEach.Name = "lblEach";
            this.lblEach.Size = new System.Drawing.Size(122, 21);
            this.lblEach.Text = "0";
            this.lblEach.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(3, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 21);
            this.label6.Text = "Lẻ:";
            // 
            // lblBag
            // 
            this.lblBag.Location = new System.Drawing.Point(59, 46);
            this.lblBag.Name = "lblBag";
            this.lblBag.Size = new System.Drawing.Size(122, 21);
            this.lblBag.Text = "0";
            this.lblBag.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblShove
            // 
            this.lblShove.Location = new System.Drawing.Point(59, 67);
            this.lblShove.Name = "lblShove";
            this.lblShove.Size = new System.Drawing.Size(122, 21);
            this.lblShove.Text = "0";
            this.lblShove.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCan
            // 
            this.lblCan.Location = new System.Drawing.Point(59, 88);
            this.lblCan.Name = "lblCan";
            this.lblCan.Size = new System.Drawing.Size(122, 21);
            this.lblCan.Text = "0";
            this.lblCan.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCarton
            // 
            this.lblCarton.Location = new System.Drawing.Point(59, 25);
            this.lblCarton.Name = "lblCarton";
            this.lblCarton.Size = new System.Drawing.Size(122, 21);
            this.lblCarton.Text = "0";
            this.lblCarton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(3, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 21);
            this.label7.Text = "Bao:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(3, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 21);
            this.label8.Text = "Can:";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(3, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 21);
            this.label9.Text = "Xô:";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(3, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 21);
            this.label10.Text = "Thùng:";
            // 
            // DeliveryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "DeliveryView";
            this.Text = "Giao hàng";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblDeliveryCode;
        private System.Windows.Forms.Label lblTruckNo;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.Label lblEach;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblBag;
        private System.Windows.Forms.Label lblShove;
        private System.Windows.Forms.Label lblCan;
        private System.Windows.Forms.Label lblCarton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}