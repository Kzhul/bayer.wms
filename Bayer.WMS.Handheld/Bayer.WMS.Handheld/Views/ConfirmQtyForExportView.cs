﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ConfirmQtyForExportView : Form
    {
        private ProcessExport _process;
        private string _status;
        private bool _isEmpty;
        /// <summary>
        /// 0: Confirm qty on pallet.
        /// 1: Based on pallet, move carton or product on it.
        /// 2: Based on carton or product, move on pallet.
        /// </summary>
        private int _state;
        private int _step = 1;
        private DataTable _summary;
        private DataTable _details;
        private List<string> _scannedValue;
        private BarcodeReader _barcodeReader;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public ConfirmQtyForExportView()
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            _scannedValue = new List<string>();
        }

        public void ConfirmQtyForExportView_Load(object sender, EventArgs e)
        {
            //// move carton or product on empty pallet or exported pallet
            if (_isEmpty || _status == Utility.PalletStatusExported)
            {
                _state = 1;
                lblStepHints.Text = "Quét mã thùng/đơn vị lẻ";
            }
            else
            {
                _state = 0;
                SetHint();
            }
        }

        public void ConfirmQtyForExportView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Bạn có chắc chắn?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                    return;

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        public void mniDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue("huy");
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniEndDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue("ket_thuc_huy");
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_summary.Rows[e.Row]["RequestQty"].ToString());
            decimal exportedQty = decimal.Parse(_summary.Rows[e.Row]["ProductQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ConfirmQty" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL y.cầu", "RequestQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL xuất", "ProductQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));

            dtgConfirmQty.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            var method = this.GetType().GetMethod("ProcessBarcode");

            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (barcode == "ket_thuc_huy")
                {
                    _step = 1;

                    lblStepHints.Text = "Quét mã thùng/đơn vị lẻ";
                    lblStepHints.BackColor = Color.LimeGreen;
                    Utility.RecordAuditTrail(null, AuditTrailAction.ScanDeleteEnd, this.GetType().GetMethod("ProcessBarcode"), String.Format("{0} - {1} - {2}", _process.ReferenceNbr, lblPalletCode.Text, barcode));

                    SetHint();
                }
                else if (barcode == "huy")
                {
                    _step = 0;

                    lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để hủy";
                    lblStepHints.BackColor = Color.Red;
                    Utility.RecordAuditTrail(null, AuditTrailAction.ScanDeleteBegin, this.GetType().GetMethod("ProcessBarcode"), String.Format("{0} - {1} - {2}", _process.ReferenceNbr, lblPalletCode.Text, barcode));
                }
                else
                {
                    if (_step == 1)
                    {
                        if (_scannedValue.Exists(p => p == barcode))
                            throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                        bool isMoveItem;
                        _process.ProcessCfm(barcode, _state, out isMoveItem, ref _summary, ref _details);

                        if (isMoveItem)
                            OpenMoveItemPopup();
                        else
                            bdsConfirmQty.DataSource = _summary;

                        SetHint();
                    }
                    else
                    {
                        _process.ProcessDelete(barcode, ref _summary, ref _details);
                    }
                }
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", _process.ReferenceNbr, barcode, ex.Message);
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, method, descr);
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void OpenMoveItemPopup()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();

                using (var view = new MoveGoodView())
                {
                    view.CurrentItems = _details;
                    view.Status = Utility.PalletStatusExported;
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    _process.UpdateMoveToNewPallet(ref _summary, ref _details, view.MovedItems);
                }
            }
            finally
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
        }

        public void SetHint()
        {
            int state = _process.ValidateState(_summary);
            if (state == 2)
            {
                lblStepHints.Text = "Đã quá số lượng yêu cầu, quét mã pallet để san bớt hàng";
                lblStepHints.BackColor = Color.Red;

                Utility.PlayErrorSound();
            }
            else if (state == 1)
            {
                lblStepHints.Text = "Quét mã thùng/đơn vị lẻ để xuất đủ hàng";
                lblStepHints.BackColor = Color.LimeGreen;
            }
            else
            {
                lblStepHints.Text = "Xác nhận số lượng";
                lblStepHints.BackColor = Color.LimeGreen;
            }
        }

        public ProcessExport Process
        {
            set
            {
                _process = value;
                _process.PalletStatusesSummary.TableName = "ConfirmQty";
                _status = _process.PalletStatusesSummary.Rows[0]["Status"].ToString();

                if (_status == Utility.PalletStatusExported)
                {
                    _details = _process.PalletStatusesDetails.Clone();
                    var rows = _process.PalletStatusesDetails.Select(String.Format("PalletCode = '{0}'", _process.CurrentBarcode));
                    foreach (DataRow row in rows)
                    {
                        _details.Rows.Add(row.ItemArray);
                    }
                }
                else if (_process.DetailsOnPallet != null)
                {
                    _details = _process.DetailsOnPallet.Copy();
                }

                lblPalletCode.Text = _process.CurrentBarcode;

                _summary = _process.PalletStatusesSummary.Copy();
                _summary.TableName = "ConfirmQty";

                //// only set datasource when datatable has value
                if (_summary.Select("ProductQty > 0").Length > 0)
                {
                    bdsConfirmQty.DataSource = _summary;
                    _isEmpty = false;
                }
                else
                    _isEmpty = true;
            }
        }

        public DataTable Summary
        {
            get { return _summary; }
        }

        public DataTable Details
        {
            get { return _details; }
        }


    }
}