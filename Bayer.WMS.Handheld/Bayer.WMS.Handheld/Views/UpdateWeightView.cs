﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bayer.WMS.Handheld.Views
{
    public partial class UpdateWeightView : Form
    {
        public UpdateWeightView()
        {
            InitializeComponent();
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void mniExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public decimal Weight
        {
            get { return decimal.Parse(txtWeight.Text); }
        }
    }
}