﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Process.MainProcess;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ConfirmQtyForDeliveryView : Form
    {
        private ProcessDelivery _process;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public ConfirmQtyForDeliveryView()
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
        }

        public void ConfirmQtyWithoutScanView_Load(object sender, EventArgs e)
        {
            lblStepHints.Text = "Xác nhận số lượng.";
        }

        public void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Bạn có chắc chắn?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                    return;

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        public void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ConfirmQty" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Loại", "StrPackingType", 100, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lượng", "Quantity", 60, "N0", null));

            dtgConfirmQty.TableStyles.Add(dtgStyle);
        }

        public ProcessDelivery Process
        {
            set
            {
                _process = value;
                _process.PalletStatusesSummary.TableName = "ConfirmQty";

                bdsConfirmQty.DataSource = _process.PalletStatusesSummary;
            }
        }
    }
}