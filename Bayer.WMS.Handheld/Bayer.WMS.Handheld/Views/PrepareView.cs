﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    public partial class PrepareView : BaseForm
    {
        private bool _isPrintPalletLabel = false;
        private ProcessPrepare _process;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public PrepareView()
            : base(Utility.PrepareView)
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã phiếu soạn hàng",
                "Quét mã pallet đã chia để soạn hàng"
            };

            lblStepHints.Text = _stepHints[0];
        }

        protected override void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal quantity = decimal.Parse(_process.PrepareNoteDetails.Rows[e.Row]["Quantity"].ToString());
            decimal preparedQty = decimal.Parse(_process.PrepareNoteDetails.Rows[e.Row]["PreparedQty"].ToString());

            if (preparedQty == 0)
                e.MeetsCriteria = 0;
            else if (preparedQty < quantity)
                e.MeetsCriteria = 1;
            else
                e.MeetsCriteria = 2;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miPackaging_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new PackagingView())
                {
                    view.OddQty = _process.OddQty;
                    view.PrepareCode = _process.ReferenceNbr;
                    view.CompanyName = _process.CompanyName;
                    view.Executor = lblEmployeeName.Text;
                    view.DeliveryDate = _process.DeliveryDate;

                    view.ShowDialog();

                    _process.OddQty = view.OddQty;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogToDB(ex);
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        public void miPrintPallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new PrintPalletLabelView())
                {
                    view.CompanyName = _process.CompanyName;
                    view.DeliveryDate = _process.DeliveryDate;
                    //view.Executor = lblEmployeeName.Text;
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    _isPrintPalletLabel = view.IsPrintPalletLabel;
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        public void mniMergePallet_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new MergePalletView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            try
            {
                if (_process == null)
                {
                    this.Close();
                    return;
                }

                if (_process.OddQty > 0)
                    throw new Exception(String.Format("Số lượng gói lẻ còn lại là {0:N0}, vui lòng thực hiện đóng gói hàng lẻ.", _process.OddQty));

                if (!_isPrintPalletLabel)
                    throw new Exception("Vui lòng in nhãn các pallet đã soạn.");

                DisposeBarcodeReader();
                this.Close();
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("miExit_Click"), ex.Message);

                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PrepareList" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL y.cầu", "Quantity", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL đã soạn", "PreparedQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));

            dtgPrepareGood.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                else if (Utility.PrepareNoteRegex.IsMatch(barcode))
                    ProcessPrepareNoteBarcode(barcode);
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", lblPrepareCode.Text, barcode, ex.Message);
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), descr);
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessPrepareNoteBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            lblPrepareCode.Text = String.Empty;
            lblCompanyName.Text = String.Empty;
            bdsPrepareGood.DataSource = null;

            _process = new ProcessPrepare();
            _process.PrepareNoteDetails = Utility.LoadPrepareNote(barcode);
            _process.PrepareNoteDetails.TableName = "PrepareList";

            var executor = Utility.LoadDeliverOrder_Executor(_process.DOImportCode, "S");
            if (executor.Select(String.Format("UserID = {0}", Utility.UserID)).Length > 0)
                throw new Exception("Nhân viên này đã thực hiện chia hàng nên không thể soạn hàng.");

            int oddQty = Utility.GetOddQty(barcode);
            if (oddQty > 0)
                _isPrintPalletLabel = false;
            else
                _isPrintPalletLabel = true;

            _process.OddQty = oddQty;

            lblPrepareCode.Text = _process.ReferenceNbr;
            lblCompanyName.Text = _process.CompanyName;
            bdsPrepareGood.DataSource = _process.PrepareNoteDetails;

            _scannedValue.Clear();

            if (_process.IsComplete())
                lblStepHints.Text = "Soạn hàng đã hoàn tất";
            else
                lblStepHints.Text = _stepHints[2];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPrepareNote, this.GetType().GetMethod("ProcessPrepareNoteBarcode"), barcode);
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(lblEmployeeName.Text))
                    throw new Exception("Vui lòng quét mã nhân viên trước.");
                if (String.IsNullOrEmpty(lblPrepareCode.Text))
                    throw new Exception("Vui lòng quét mã phiếu soạn hàng trước.");

                tabControl1.SelectedIndex = 1;

                _process.Process(barcode);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            if (!ShowConfirmPrepareQty())
                return;

            _process.UpdatePreparedQty();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (_process.IsComplete())
                {
                    if (_process.OddQty > 0)
                        lblStepHints.Text = "Vui lòng đóng gói lẻ";
                    else
                        lblStepHints.Text = "Soạn hàng đã hoàn tất";
                }

                _scannedValue.Add(barcode);

                this.Refresh();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public bool ShowConfirmPrepareQty()
        {
            try
            {
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForPrepareView())
                {
                    view.Process = _process;

                    return (view.ShowDialog() == DialogResult.OK);
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}