﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Models;
using System.IO.Ports;
using System.Threading;
using System.Globalization;
using System.IO;

namespace Bayer.WMS.Handheld.Views
{
    public partial class PackagingView : Form
    {
        private int _step = 1;
        private int _storeStep;
        private string _storeHints;
        private int _oddQty;
        private string _prepareCode;
        private string _companyName;
        private string _executor;
        private DateTime _deliveryDate;
        private DataTable _dt;
        private BarcodeReader _barcodeReader;
        private readonly SerialPort _serialPort;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public PackagingView()
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);

            _dt = new DataTable();
            _dt.Columns.Add("CartonBarcode", typeof(string));
            _dt.Columns.Add("ProductBarcode", typeof(string));
            _dt.Columns.Add("EncryptedProductBarcode", typeof(string));
            _dt.Columns.Add("ProductLot", typeof(string));
            _dt.Columns.Add("ProductID", typeof(int));
            _dt.Columns.Add("ProductCode", typeof(string));
            _dt.Columns.Add("ProductDescription", typeof(string));

            var dtPerCarton = _dt.Clone();
            dtPerCarton.TableName = "Packaging";
            bdsPackaging.DataSource = dtPerCarton;

            Parity parity = Parity.None;
            switch (Utility.COM_Parity)
            {
                case "Even":
                    parity = Parity.Even;
                    break;
                case "Mark":
                    parity = Parity.Mark;
                    break;
                case "Odd":
                    parity = Parity.Odd;
                    break;
                case "Space":
                    parity = Parity.Space;
                    break;
                default:
                    break;
            }

            StopBits sb = StopBits.None;
            switch (Utility.COM_StopBits)
            {
                case "1":
                    sb = StopBits.One;
                    break;
                case "1.5":
                    sb = StopBits.OnePointFive;
                    break;
                case "2":
                    sb = StopBits.Two;
                    break;
                default:
                    break;
            }

            _serialPort = new SerialPort(Utility.COM_PortName, Utility.COM_BaudRate, parity, Utility.COM_DataBits, sb);
        }

        public void PackagingView_Load(object sender, EventArgs e)
        {
            lblStepHints.Text = "Quét mã đơn vị lẻ để thực hiện đóng gói";
        }

        public void PackagingView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniFinish_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue("ket_thuc");
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue("huy");
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniEndDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue("ket_thuc_huy");
                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "Packaging" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("QR", "EncryptedProductBarcode", 100, String.Empty, null));

            dtgPackaging.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (barcode == "ket_thuc")
                    ProcessPackagingEnd();
                else if (barcode == "ket_thuc_huy")
                    ProcessPackagingDestroyEnd();
                else if (barcode == "huy")
                    ProcessPackaginDestroy();
                else if (Utility.CartonBarcode2Regex.IsMatch(barcode) || Utility.CartonBarcodeRegex.IsMatch(barcode))
                    ProcessCartonBarcode(barcode);
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), String.Format("{0} - {1}", barcode, ex.Message));
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPackagingEnd()
        {
            if (_step > 2)
                throw new Exception("Thực hiện sai bước, vui lòng thực hiện theo hướng dẫn");

            Cursor.Current = Cursors.Default;

            decimal weight;
            using (var view = new UpdateWeightView())
            {
                if (view.ShowDialog() != DialogResult.OK)
                    return;

                weight = view.Weight;
            }

            _step = 2;

            Print(bdsPackaging.DataSource as DataTable, weight);
            lblStepHints.Text = "Quét mã thùng để xác nhận đóng gói";

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanFinishPackaging, this.GetType().GetMethod("ProcessPackagingEnd"), _prepareCode);
        }

        public void ProcessPackaginDestroy()
        {
            _storeStep = _step;
            _storeHints = lblStepHints.Text;
            _step = 0;

            lblStepHints.Text = "Quét mã đơn vị lẻ để hủy";
            lblStepHints.BackColor = Color.Red;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanDeletePackaging, this.GetType().GetMethod("ProcessPackaginDestroy"), _prepareCode);
        }

        public void ProcessPackagingDestroyEnd()
        {
            _step = _storeStep;
            lblStepHints.Text = _storeHints;
            lblStepHints.BackColor = Color.LimeGreen;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEndDeletePackaging, this.GetType().GetMethod("ProcessPackagingDestroyEnd"), _prepareCode);
        }

        public void ProcessCartonBarcode(string barcode)
        {
            if (_step > 3)
                throw new Exception("Thực hiện sai bước, vui lòng thực hiện theo hướng dẫn");

            if (_step == 1)
            {
                var details = Pallet.LoadPalletStatuses(barcode, "C");

                var dtPerCarton = bdsPackaging.DataSource as DataTable;
                foreach (DataRow row in details.Rows)
                {
                    _dt.Rows.Add(String.Empty, row["ProductBarcode"], row["EncryptedProductBarcode"], row["ProductLot"],
                        row["ProductID"], row["ProductCode"], row["ProductDescription"]);
                    dtPerCarton.Rows.Add(String.Empty, row["ProductBarcode"], row["EncryptedProductBarcode"], row["ProductLot"],
                        row["ProductID"], row["ProductCode"], row["ProductDescription"]);
                }

                bdsPackaging.DataSource = dtPerCarton;
                lblQty.Text = (int.Parse(lblQty.Text) + details.Rows.Count).ToString();

                Utility.RecordAuditTrail(null, AuditTrailAction.ScanCarton, this.GetType().GetMethod("ProcessCartonBarcode"), String.Format("{0} - {1}", _prepareCode, barcode));
            }
            else
            {
                _step = 3;

                lblCarton.Text = barcode;
                lblStepHints.Text = "Quét mã pallet để xác nhận vị trí của thùng đã đóng gói";

                Utility.RecordAuditTrail(null, AuditTrailAction.ScanCarton, this.GetType().GetMethod("ProcessCartonBarcode"), String.Format("{0} - {1} - Đóng gói vô thùng", _prepareCode, barcode));
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            if (_step > 4)
                throw new Exception("Thực hiện sai bước, vui lòng thực hiện theo hướng dẫn");
            _step = 4;

            Packaging(barcode, bdsPackaging.DataSource as DataTable);

            _step = 1;

            if (_oddQty == 0)
                lblStepHints.Text = "Đóng thùng lẻ đã hoàn tất, vui lòng thoát màn hình";
            else
                lblStepHints.Text = "Quét mã đơn vị lẻ để thực hiện đóng gói";

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("ProcessPalletBarcode"), String.Format("{0} - {1} - {2}", _prepareCode, lblCarton.Text, barcode));
        }

        public void ProcessProductBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string encryptedBarcode = barcode.Split('|')[0];
                string type;
                barcode = Barcode.Process(encryptedBarcode, false, false, true, out type);

                if (_step > 1)
                    throw new Exception("Thực hiện sai bước, vui lòng thực hiện theo hướng dẫn");
                if (_step == 0)
                {
                    if (MessageBox.Show("Bạn có chắc chắn muốn hủy?", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                        return;

                    var dtPerCarton = bdsPackaging.DataSource as DataTable;
                    var row = dtPerCarton.Select(String.Format("ProductBarcode = '{0}'", barcode)).FirstOrDefault();
                    if (row == null)
                        throw new Exception("Mã không hợp lệ, vui lòng kiểm tra lại dữ liệu.");

                    dtPerCarton.Rows.Remove(row);

                    row = _dt.Select(String.Format("ProductBarcode = '{0}'", barcode)).FirstOrDefault();
                    _dt.Rows.Remove(row);

                    lblQty.Text = (int.Parse(lblQty.Text) - 1).ToString();

                    Utility.RecordAuditTrail(null, AuditTrailAction.ScanDelete, this.GetType().GetMethod("ProcessBarcode"), String.Format("{0} - {1}", _prepareCode, barcode));
                }
                else
                {
                    int packagingQty = int.Parse(lblQty.Text);
                    if (packagingQty >= 10)
                    {
                        Utility.PlayErrorSound();
                        if (MessageBox.Show("Bạn có muốn đóng gói tiếp thùng này hay không", "Cảnh báo",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                        {
                            lblStepHints.Text = "Quét mã kết thúc để in nhãn";
                            return;
                        }
                    }
                    packagingQty++;

                    if (_dt.Select(String.Format("ProductBarcode = '{0}'", barcode)).Length > 0)
                        throw new Exception("Mã đã đóng gói rồi, vui lòng kiểm tra lại dữ liệu.");

                    var dt = Pallet.LoadRequirePalletStatus(barcode, "E", Utility.PalletStatusPrepared);

                    var dtPerCarton = bdsPackaging.DataSource as DataTable;

                    _dt.Rows.Add(String.Empty, barcode, encryptedBarcode, dt.Rows[0]["ProductLot"],
                        dt.Rows[0]["ProductID"], dt.Rows[0]["ProductCode"], dt.Rows[0]["ProductDescription"]);
                    dtPerCarton.Rows.Add(String.Empty, barcode, encryptedBarcode, dt.Rows[0]["ProductLot"],
                        dt.Rows[0]["ProductID"], dt.Rows[0]["ProductCode"], dt.Rows[0]["ProductDescription"]);

                    bdsPackaging.DataSource = dtPerCarton;

                    if (packagingQty >= 10)
                        lblStepHints.Text = "Thùng có thể đầy, quét mã kết thúc để in nhãn";
                    else
                        lblStepHints.Text = "Quét mã đơn vị lẻ để thực hiện đóng gói";

                    lblQty.Text = packagingQty.ToString();

                    Utility.RecordAuditTrail(null, AuditTrailAction.ScanProduct, this.GetType().GetMethod("ProcessBarcode"), String.Format("{0} - {1}", _prepareCode, barcode));
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void Print(DataTable packagingData, decimal weight)
        {
            try
            {
                var products = new List<string>();
                var qties = new Dictionary<string, int>();
                foreach (DataRow row in packagingData.Rows)
                {
                    string productLot = row["ProductLot"].ToString();
                    string productCode = row["ProductCode"].ToString();
                    string productDescription = row["ProductDescription"].ToString();

                    string tmp = String.Format("{0}-{1}-{2}", productLot, productCode, productDescription);
                    if (!products.Exists(p => p == tmp))
                    {
                        products.Add(tmp);
                        qties.Add(tmp, 1);
                    }
                    else
                        qties[tmp] += 1;
                }

                int lastNumber = Utility.GenCartonWarehouseBarcode(_prepareCode, 1, weight);

                if (!_serialPort.IsOpen)
                    _serialPort.Open();

                string text = String.Empty;
                if (products.Count == 1)
                {
                    var productionPlan = ProductionPlan.LoadProductionPlan(products[0].Split('-')[0]);

                    char separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
                    string format = GetNumberFormat(productionPlan.PackageSize);

                    text = Utility.CartonLabelTemplate;
                    text = text.Replace("{FontScale}", productionPlan.ProductDescription.Length <= 25 ? "100" : (Math.Round(((decimal)25 / productionPlan.ProductDescription.Length) * 100)).ToString());
                    text = text.Replace("{CompanyName}", _companyName);
                    text = text.Replace("{DeliveryDate}", _deliveryDate.ToString("dd/MM/yyyy"));
                    text = text.Replace("{ProductName}", productionPlan.ProductDescription);
                    text = text.Replace("{ProductLot}", productionPlan.ProductLot);
                    text = text.Replace("{ManufacturingDate}", productionPlan.MFD.ToString("dd/MM/yyyy"));
                    text = text.Replace("{ExpiryDate}", productionPlan.EXP.ToString("dd/MM/yyyy"));
                    text = text.Replace("{CartonBarcode}", String.Format("{0}C{1:000}", _prepareCode, lastNumber + 1));
                    text = text.Replace("{Weight}", String.Format("{0:N2}", weight));
                    text = text.Replace("{Executor}", _executor);
                    text = text.Replace("{PackageSize}", String.Format(format, productionPlan.PackageSize));
                    text = text.Replace("{UOM}", productionPlan.UOM);
                    text = text.Replace("{ProductPacking}", String.Format("{0:N0}", qties[products[0]]));
                }
                else
                {
                    text = Utility.CartonLabelForMultiProductTemplate;
                    text = text.Replace("{CompanyName}", _companyName);
                    text = text.Replace("{DeliveryDate}", _deliveryDate.ToString("dd/MM/yyyy"));
                    text = text.Replace("{CartonBarcode}", String.Format("{0}C{1:000}", _prepareCode, lastNumber + 1));
                    text = text.Replace("{Weight}", String.Format("{0:N2}", weight));
                    text = text.Replace("{Executor}", _executor);

                    for (int i = 1; i <= 6; i++)
                    {
                        if (products.Count >= i)
                        {
                            string[] temp = products[i - 1].Split('-');

                            text = text.Replace("{ProductLot" + i + "}", temp[0]);
                            text = text.Replace("{ProductName" + i + "}", temp[2]);
                            text = text.Replace("{Qty" + i + "}", String.Format("{0:N0}", qties[products[i - 1]]));
                        }
                        else
                        {
                            text = text.Replace("{ProductLot" + i + "}", String.Empty);
                            text = text.Replace("{ProductName" + i + "}", String.Empty);
                            text = text.Replace("{Qty" + i + "}", String.Empty);
                        }
                    }
                }
                byte[] b = Encoding.UTF8.GetBytes(text);
                _serialPort.Write(b, 0, b.Length);

                try
                {
                    using (var sw = new StreamWriter(Path.Combine(Utility.AppPath, String.Format("{0}C{1:000}", _prepareCode, lastNumber + 1))))
                    {
                        sw.Write(text);
                        sw.Flush();
                        sw.Close();
                    }
                }
                catch
                {

                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();
            }
        }

        public void Packaging(string palletCode, DataTable packagingData)
        {
            var pallet = Pallet.Load(palletCode);
            if (pallet.Status != Utility.PalletStatusPrepared && pallet.Status != Utility.PalletStatusNew)
                throw new Exception("Trạng thái pallet không hợp lệ, chỉ chấp nhận pallet trống hoặc đã soạn.");

            var productBarcodes = new List<string>();

            foreach (DataRow row in packagingData.Rows)
            {
                productBarcodes.Add(row["ProductBarcode"].ToString());
            }

            if (productBarcodes.Count > 0)
            {
                using (var conn = new SqlConnection(Utility.ConnStr))
                {
                    conn.Open();
                    try
                    {
                        var tran = conn.BeginTransaction();
                        Utility.UpdatePalletStatus_Packaging(palletCode, lblCarton.Text,
                            String.Join(",", productBarcodes.ToArray()), this.GetType().GetMethod("Packaging").ReflectedType.FullName, conn, tran);

                        tran.Commit();

                        lblCarton.Text = String.Empty;
                        lblQty.Text = "0";
                        var dtPerCarton = _dt.Clone();
                        dtPerCarton.TableName = "Packaging";
                        bdsPackaging.DataSource = dtPerCarton;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                _oddQty -= productBarcodes.Count;
            }
        }

        public string GetNumberFormat(decimal number)
        {
            char separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
            string format = "{0:N0}";
            if (number.ToString().Contains(separator))
            {
                string tmp = number.ToString().Split(separator)[1];
                while (tmp.EndsWith("0"))
                    tmp = tmp.Substring(0, tmp.Length - 1);
                if (tmp.Length == 1)
                    format = "{0:N1}";
                else if (tmp.Length == 2)
                    format = "{0:N2}";
                else if (tmp.Length == 3)
                    format = "{0:N3}";
                else if (tmp.Length == 4)
                    format = "{0:N4}";
            }

            return format;
        }

        public string PrepareCode
        {
            set { _prepareCode = value; }
        }

        public string CompanyName
        {
            set { _companyName = value; }
        }

        public string Executor
        {
            set { _executor = value; }
        }

        public DateTime DeliveryDate
        {
            set { _deliveryDate = value; }
        }

        public int OddQty
        {
            get { return _oddQty; }
            set { _oddQty = value; }
        }
    }
}