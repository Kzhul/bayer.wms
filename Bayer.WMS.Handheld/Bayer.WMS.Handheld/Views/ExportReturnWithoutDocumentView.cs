﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bayer.WMS.Handheld.Process.MainProcess;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ExportReturnWithoutDocumentView : BaseForm
    {
        private string _palletCode = String.Empty;
        private ProcessExportReturnWithoutDocument _process;

        public ExportReturnWithoutDocumentView()
            : base(Utility.ExportReturnView)
        {
            InitializeComponent();
            InitDataGridView();

            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã pallet"
            };

            lblStepHints.Text = _stepHints[0];

            _dt = new DataTable();
            _dt.TableName = "ExportReturnList";
            _dt.Columns.Add("BatchCode", typeof(string));
            _dt.Columns.Add("ProductID", typeof(int));
            _dt.Columns.Add("ProductCode", typeof(string));
            _dt.Columns.Add("ProductDescription", typeof(string));
            _dt.Columns.Add("ReturnedQty", typeof(decimal));

            bdsExportReturnGood.DataSource = _dt;

            _process = new ProcessExportReturnWithoutDocument();
            _process.ExportReturnNoteDetails = _dt;
        }

        protected override void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ExportReturnList" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL trả", "ExportedReturnQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "BatchCode", 60, String.Empty, null));

            dtgExportReturnGood.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                //// step 1: scan employee
                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                //// step 3: scan pallet/carton/product
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"),
                    String.Format("{0} - {1}", barcode, ex.Message));

                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessProductBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            //// get quantity details of pallet/carton/product
            _process.Process(barcode);

            DataTable summary, details;
            if (!ShowConfirmExportReturnQty(out summary, out details))
                return;

            // update confirm qty on screen
            _process.UpdateExportedReturnQty(summary, details);

            this.Refresh();
        }

        public bool ShowConfirmExportReturnQty(out DataTable summary, out DataTable details)
        {
            try
            {
                summary = new DataTable();
                details = new DataTable();
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForExportReturnWithoutDocumentView())
                {
                    view.Process = _process;

                    if (view.ShowDialog() == DialogResult.OK)
                    {
                        summary = view.Summary;
                        details = view.Details;
                        return true;
                    }
                    return false;
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}