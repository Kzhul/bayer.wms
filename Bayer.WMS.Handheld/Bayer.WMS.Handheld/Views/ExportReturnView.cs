﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Process.MainProcess;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ExportReturnView : BaseForm
    {
        private string _palletCode = String.Empty;
        private ProcessExportReturn _process;

        public ExportReturnView()
            : base(Utility.ExportReturnView)
        {
            InitializeComponent();
            InitDataGridView();

            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã phiếu trả hàng",
                "Quét mã pallet"
            };

            lblStepHints.Text = _stepHints[0];
        }

        protected override void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal remainQty = decimal.Parse(_process.ExportReturnNoteDetails.Rows[e.Row]["RemainQty"].ToString());
            decimal returnedQty = decimal.Parse(_process.ExportReturnNoteDetails.Rows[e.Row]["ExportedReturnQty"].ToString());

            if (returnedQty == 0)
                e.MeetsCriteria = 0;
            else if (returnedQty < remainQty)
                e.MeetsCriteria = 1;
            else
                e.MeetsCriteria = 2;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ExportReturnList" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL y.cầu", "RemainQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL trả", "ExportedReturnQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "BatchCode", 60, String.Empty, CheckCellEquals));

            dtgExportReturnGood.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                //// step 1: scan employee
                if (Utility.EmployeeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                //// step 2: scan export note
                else if (Utility.ExportReturnRegex.IsMatch(barcode))
                    ProcessExportReturnNoteBarcode(barcode);
                //// step 3: scan pallet/carton/product
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"),
                    String.Format("{0} - {1}", barcode, ex.Message));

                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public void ProcessEmployeeBarcode(string barcode)
        {
            barcode = barcode.Remove(0, 3);

            var user = User.LoadUser(barcode);

            Utility.UserID = user.UserID;
            lblEmployeeName.Text = String.Format("{0} {1}", user.LastName, user.FirstName);

            lblStepHints.Text = _stepHints[1];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEmployee, this.GetType().GetMethod("ProcessEmployeeBarcode"), barcode);
        }

        public void ProcessExportReturnNoteBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");

            lblExportCode.Text = String.Empty;
            bdsExportReturnGood.DataSource = null;

            _process = new ProcessExportReturn();
            _process.ExportReturnNoteDetails = Utility.LoadExportReturnNote(barcode);
            _process.ExportReturnNoteDetails.TableName = "ExportReturnList";

            lblExportCode.Text = _process.ReferenceNbr;
            bdsExportReturnGood.DataSource = _process.ExportReturnNoteDetails;

            _scannedValue.Clear();

            if (_process.IsComplete())
                lblStepHints.Text = "Trả hàng đã hoàn tất";
            else
                lblStepHints.Text = _stepHints[2];

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanExportNote, this.GetType().GetMethod("ProcessExportReturnNoteBarcode"), barcode);
        }

        public void ProcessProductBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblEmployeeName.Text))
                throw new Exception("Vui lòng quét mã nhân viên trước.");
            if (String.IsNullOrEmpty(lblExportCode.Text))
                throw new Exception("Vui lòng quét mã phiếu xuất hàng trước.");

            //// get quantity details of pallet/carton/product
            _process.Process(barcode);

            DataTable summary, details;
            if (!ShowConfirmExportReturnQty(out summary, out details))
                return;

            //// update confirm qty on screen
            _process.UpdateExportedReturnQty(summary, details);

            if (_process.IsComplete())
                lblStepHints.Text = "Trả hàng đã hoàn tất";

            if (_process.CurrentType != "P")
                _scannedValue.Add(barcode);

            this.Refresh();
        }

        public bool ShowConfirmExportReturnQty(out DataTable summary, out DataTable details)
        {
            try
            {
                summary = new DataTable();
                details = new DataTable();
                DisposeBarcodeReader();

                //// open popup to show quantity in pallet/carton/product and then confirm quantity
                using (var view = new ConfirmQtyForExportReturnView())
                {
                    view.Process = _process;

                    if (view.ShowDialog() == DialogResult.OK)
                    {
                        summary = view.Summary;
                        details = view.Details;
                        return true;
                    }
                    return false;
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
    }
}