﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.MainProcess;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ConfirmQtyForExportReturnWithoutDocumentView : Form
    {
        private ProcessExportReturnWithoutDocument _process;
        private string _status;
        private bool _isEmpty;
        /// <summary>
        /// 0: Confirm qty on pallet.
        /// 1: Based on pallet, move carton or product on it.
        /// 2: Based on carton or product, move on pallet.
        /// 3: Move carton or product to another pallet.
        /// </summary>
        private int _state;
        private DataTable _summary;
        private DataTable _details;
        private List<string> _scannedValue;
        private BarcodeReader _barcodeReader;

        public ConfirmQtyForExportReturnWithoutDocumentView()
        {
            InitializeComponent();
            InitDataGridView();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            _scannedValue = new List<string>();
        }

        public void ConfirmQtyForExportView_Load(object sender, EventArgs e)
        {
            //// move carton or product on empty pallet or exported pallet
            if (_isEmpty || _status == Utility.PalletStatusExportedReturn)
            {
                _state = 1;
                lblStepHints.Text = "Quét mã thùng/đơn vị lẻ";
            }
            else
            {
                _state = 0;
                lblStepHints.Text = "Xác nhận số lượng";
            }
        }

        public void ConfirmQtyForExportView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Bạn có chắc chắn?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                    return;

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        public void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "ConfirmQty" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL xuất", "ProductQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));

            dtgConfirmQty.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            var method = this.GetType().GetMethod("ProcessBarcode");

            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (_scannedValue.Exists(p => p == barcode))
                    throw new Exception("Mã này đã quét, vui lòng quét mã khác.");

                bool isMoveItem;
                _process.ProcessCfm(barcode, ref _state, out isMoveItem);

                if (isMoveItem)
                    OpenMoveItemPopup();
                else
                    bdsConfirmQty.DataSource = _summary;
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, method, String.Format("{0} - {1}", barcode, ex.Message));

                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public void OpenMoveItemPopup()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();

                using (var view = new MoveGoodView())
                {
                    view.Status = Utility.PalletStatusExportedReturn;
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    var dt = view.MovedItems;

                    foreach (DataRow row in dt.Rows)
                    {
                        string filterExp = String.Format("{0} = '{1}'", row["Type"].ToString() == "C" ? "CartonBarcode" : "ProductBarcode", row["Barcode"]);
                        var movedRow = _details.Select(filterExp).FirstOrDefault();
                        _details.Rows.Remove(movedRow);
                    }

                    _process.UpdateMoveToNewOrExportedReturnPallet(ref _summary, dt);
                }
            }
            finally
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
        }

        public ProcessExportReturnWithoutDocument Process
        {
            set
            {
                _process = value;
                _process.PalletStatusesSummary.TableName = "ConfirmQty";
                _status = _process.PalletStatusesSummary.Rows[0]["Status"].ToString();
                
                if (_process.PalletStatusesDetails != null)
                {
                    _details = _process.PalletStatusesDetails.Clone();
                    var rows = _process.PalletStatusesDetails.Select(String.Format("PalletCode = '{0}'", _process.CurrentBarcode));
                    foreach (DataRow row in rows)
                    {
                        _details.Rows.Add(row.ItemArray);
                    }
                }

                _summary = _process.PalletStatusesSummary.Copy();
                _summary.TableName = "ConfirmQty";

                //// only set datasource when datatable has value
                if (_summary.Select("ProductQty > 0").Length > 0)
                {
                    bdsConfirmQty.DataSource = _summary;
                    _isEmpty = false;
                }
                else
                    _isEmpty = true;
            }
        }

        public DataTable Summary
        {
            get { return _summary; }
        }

        public DataTable Details
        {
            get { return _details; }
        }
    }
}