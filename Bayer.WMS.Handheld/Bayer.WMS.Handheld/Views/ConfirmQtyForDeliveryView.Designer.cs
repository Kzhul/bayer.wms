﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ConfirmQtyForDeliveryView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsConfirmQty = new System.Windows.Forms.BindingSource(this.components);
            this.dtgConfirmQty = new System.Windows.Forms.DataGrid();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.bdsConfirmQty)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgConfirmQty
            // 
            this.dtgConfirmQty.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgConfirmQty.DataSource = this.bdsConfirmQty;
            this.dtgConfirmQty.Location = new System.Drawing.Point(0, 3);
            this.dtgConfirmQty.Name = "dtgConfirmQty";
            this.dtgConfirmQty.RowHeadersVisible = false;
            this.dtgConfirmQty.Size = new System.Drawing.Size(240, 242);
            this.dtgConfirmQty.TabIndex = 4;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 248);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 20);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // ConfirmQtyForDeliveryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.dtgConfirmQty);
            this.Menu = this.mainMenu1;
            this.Name = "ConfirmQtyForDeliveryView";
            this.Text = "Xác nhận số lượng";
            this.Load += new System.EventHandler(this.ConfirmQtyWithoutScanView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bdsConfirmQty)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dtgConfirmQty;
        private System.Windows.Forms.BindingSource bdsConfirmQty;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;

    }
}