﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;
using System.Data.SqlClient;
using System.Threading;

namespace Bayer.WMS.Handheld.Views
{
    public partial class MergeCartonView : Form
    {
        private int _state = 0;
        private string _palletCode;
        private Pallet _pallet;
        private Pallet _prePallet;
        private DataTable _dt;
        private DataTable _currentOnCarton;
        private BarcodeReader _barcodeReader;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public MergeCartonView()
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        private void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                _queue.Enqueue(barcode);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void QueueProcess()
        {
            Thread thread = new Thread(() =>
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string data = _queue.Dequeue();
                    if (InvokeRequired)
                    {
                        Invoke((ThreadStart)delegate
                        {
                            ProcessBarcode(data);
                        });
                    }
                    else
                        ProcessBarcode(data);
                }

                _isProcess = false;
            });

            thread.Start();
        }

        private void MergeCartonView_Load(object sender, EventArgs e)
        {
            lblStepHints.Text = "Quét mã thùng";
        }

        private void MergeCartonView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue(txtBarcode.Text);

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniFinish_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue("ket_thuc");

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue("huy");

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniEndDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _queue.Enqueue("ket_thuc_huy");

                if (!_isProcess)
                    QueueProcess();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "JoinCarton" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "EncryptedProductBarcode", 100, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));

            dtgPackaging.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (barcode == "ket_thuc")
                    ProcessEnd(barcode);
                else if (barcode == "ket_thuc_huy")
                    ProcessDestroyEnd(barcode);
                else if (barcode == "huy")
                    ProcessDestroy(barcode);
                else
                {
                    string type;
                    string encryptedBarcode = barcode.Split('|')[0];
                    barcode = Barcode.Process(barcode, true, true, true, out type);

                    if (type == "P")
                        ProcessPallet(barcode);
                    else if (type == "C")
                        ProcessCartonBarcode(barcode);
                    else
                        ProcessProductBarcode(barcode, encryptedBarcode);
                }
            }
            catch (Exception ex)
            {
                Utility.RecordAuditTrail(null, AuditTrailAction.ScanBarcodeInvalid, this.GetType().GetMethod("ProcessBarcode"), String.Format("{0} - {1}", barcode, ex.Message));
                Utility.PlayErrorSound();
                Utility.WriteLogToDB(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessEnd(string barcode)
        {
            if (_state == 1)
                throw new Exception("Thực hiện sai bước, vui lòng thực hiện theo hướng dẫn");

            if (String.IsNullOrEmpty(_palletCode))
                throw new Exception("Quét mã pallet để xác nhận vị trí thùng");

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();

                try
                {
                    foreach (DataRow row in _dt.Rows)
                    {
                        using (var cmd = new SqlCommand("proc_PalletStatuses_Update_Merge_Carton", conn, tran))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@ProductBarcode", row["ProductBarcode"]));
                            cmd.Parameters.Add(new SqlParameter("@CartonBarcode", lblCarton.Text));
                            cmd.Parameters.Add(new SqlParameter("@PalletCode", _palletCode));

                            cmd.ExecuteNonQuery();
                        }
                    }

                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }

            lblCarton.Text = String.Empty;
            lblQty.Text = "0";
            _dt.Clear();

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanFinishPackaging, this.GetType().GetMethod("ProcessPackagingFinish"), barcode);
        }

        public void ProcessDestroy(string barcode)
        {
            _state = 1;

            lblStepHints.Text = "Quét mã đơn vị lẻ để hủy";
            lblStepHints.BackColor = Color.Red;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanDeletePackaging, this.GetType().GetMethod("ProcessDestroy"), barcode);
        }

        public void ProcessDestroyEnd(string barcode)
        {
            _state = 0;

            lblStepHints.Text = "Quét mã đơn vị lẻ để dồn thùng";
            lblStepHints.BackColor = Color.LimeGreen;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanEndDeletePackaging, this.GetType().GetMethod("ProcessDestroyEnd"), barcode);
        }

        public void ProcessPallet(string barcode)
        {
            var pallet = Pallet.Load(barcode);
            if (_pallet != null && _pallet.Status != pallet.Status || _pallet.ReferenceNbr != pallet.ReferenceNbr)
                throw new Exception("Trạng thái thùng và đơn vị lẻ không đồng nhất nên không thể dồn thùng, vui lòng kiểm tra lại dữ liệu.");

            _palletCode = pallet.PalletCode;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanCarton, this.GetType().GetMethod("ProcessPallet"), barcode);
        }

        public void ProcessCartonBarcode(string barcode)
        {
            _currentOnCarton = Pallet.LoadPalletStatuses(barcode, "C");
            _dt = _currentOnCarton.Clone();
            _dt.TableName = "JoinCarton";

            _pallet = null;
            _palletCode = String.Empty;
            if (_currentOnCarton.Rows.Count > 0)
            {
                string palletCode = _currentOnCarton.Rows[0]["PalletCode"].ToString();
                _pallet = Pallet.Load(palletCode);
                _palletCode = _pallet.PalletCode;
            }

            bdsJoinCarton.DataSource = _dt;
            lblQty.Text = _dt.Rows.Count.ToString();
            lblCarton.Text = barcode;
            lblStepHints.Text = "Quét mã đơn vị lẻ để dồn thùng";

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanCarton, this.GetType().GetMethod("ProcessCartonBarcode"), barcode);
        }

        public void ProcessProductBarcode(string barcode, string encryptedBarcode)
        {
            if (String.IsNullOrEmpty(lblCarton.Text))
                throw new Exception("Vui lòng quét mã thùng trước.");

            if (_state == 1)
            {
                if (MessageBox.Show("Bạn có chắc chắn muốn hủy?", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) != DialogResult.Yes)
                    return;

                var row = _dt.Select(String.Format("ProductBarcode = '{0}'", barcode)).FirstOrDefault();
                if (row == null)
                    throw new Exception("Mã không hợp lệ, vui lòng kiểm tra lại dữ liệu.");

                _dt.Rows.Remove(row);
                lblQty.Text = (int.Parse(lblQty.Text) - 1).ToString();

            }
            else
            {
                var productBarcode = Pallet.LoadRequirePalletStatus(barcode, "E").Rows[0];
                string palletCode = productBarcode["PalletCode"].ToString();
                string status = productBarcode["Status"].ToString();
                string refNbr = productBarcode["ReferenceNbr"] == null ? null : productBarcode["ReferenceNbr"].ToString();
                string productLot = productBarcode["ProductLot"].ToString();

                if (_pallet != null)
                {
                    if (_pallet.Status != status || _pallet.ReferenceNbr != refNbr)
                        throw new Exception("Trạng thái thùng và đơn vị lẻ không đồng nhất nên không thể dồn thùng, vui lòng kiểm tra lại dữ liệu.");
                }
                else
                    _pallet = Pallet.Load(palletCode);

                if (_currentOnCarton.Rows.Count > 0 && _currentOnCarton.Rows[0]["ProductLot"].ToString() != productLot)
                    throw new Exception("Không thể dồn 2 lô khác nhau vào 1 thùng, vui lòng kiểm tra lại dữ liệu.");

                _dt.Rows.Add(_palletCode, lblCarton.Text, barcode, encryptedBarcode, productLot, productBarcode["ProductQty"],
                    productBarcode["ProductID"], productBarcode["ProductCode"], productBarcode["ProductDescription"]);
                _currentOnCarton.Rows.Add(_palletCode, lblCarton.Text, barcode, encryptedBarcode, productLot, productBarcode["ProductQty"],
                    productBarcode["ProductID"], productBarcode["ProductCode"], productBarcode["ProductDescription"]);

                lblQty.Text = (int.Parse(lblQty.Text) + 1).ToString();
            }
        }
    }
}