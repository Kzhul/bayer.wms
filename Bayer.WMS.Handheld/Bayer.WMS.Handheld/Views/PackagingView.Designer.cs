﻿namespace Bayer.WMS.Handheld.Views
{
    partial class PackagingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsPackaging = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPackaging = new System.Windows.Forms.DataGrid();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniFeature = new System.Windows.Forms.MenuItem();
            this.mniFinish = new System.Windows.Forms.MenuItem();
            this.mniDelete = new System.Windows.Forms.MenuItem();
            this.mniEndDelete = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.lblCarton = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackaging)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgPackaging
            // 
            this.dtgPackaging.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPackaging.DataSource = this.bdsPackaging;
            this.dtgPackaging.Location = new System.Drawing.Point(0, 72);
            this.dtgPackaging.Name = "dtgPackaging";
            this.dtgPackaging.RowHeadersVisible = false;
            this.dtgPackaging.Size = new System.Drawing.Size(240, 158);
            this.dtgPackaging.TabIndex = 5;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 11;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(59, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(122, 21);
            this.txtBarcode.TabIndex = 10;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã lẻ để đóng gói hoặc pallet để kết thúc";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 21);
            this.label1.Text = "Thùng:";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniFeature);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniFeature
            // 
            this.mniFeature.MenuItems.Add(this.mniFinish);
            this.mniFeature.MenuItems.Add(this.mniDelete);
            this.mniFeature.MenuItems.Add(this.mniEndDelete);
            this.mniFeature.Text = "Chức năng";
            // 
            // mniFinish
            // 
            this.mniFinish.Text = "Kết thúc";
            this.mniFinish.Click += new System.EventHandler(this.mniFinish_Click);
            // 
            // mniDelete
            // 
            this.mniDelete.Text = "Hủy";
            this.mniDelete.Click += new System.EventHandler(this.mniDelete_Click);
            // 
            // mniEndDelete
            // 
            this.mniEndDelete.Text = "Kết thúc hủy";
            this.mniEndDelete.Click += new System.EventHandler(this.mniEndDelete_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // lblCarton
            // 
            this.lblCarton.Location = new System.Drawing.Point(59, 27);
            this.lblCarton.Name = "lblCarton";
            this.lblCarton.Size = new System.Drawing.Size(178, 21);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 21);
            this.label2.Text = "SL:";
            // 
            // lblQty
            // 
            this.lblQty.Location = new System.Drawing.Point(59, 48);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(178, 21);
            this.lblQty.Text = "0";
            // 
            // PackagingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblQty);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCarton);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.dtgPackaging);
            this.Menu = this.mainMenu1;
            this.Name = "PackagingView";
            this.Text = "Đóng thùng lẻ";
            this.Load += new System.EventHandler(this.PackagingView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.PackagingView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackaging)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dtgPackaging;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.BindingSource bdsPackaging;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniFeature;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.MenuItem mniFinish;
        private System.Windows.Forms.MenuItem mniDelete;
        private System.Windows.Forms.MenuItem mniEndDelete;
        private System.Windows.Forms.Label lblCarton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblQty;
    }
}