﻿namespace Bayer.WMS.Handheld.Views
{
    partial class SplitPromotionView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.nudQty = new System.Windows.Forms.NumericUpDown();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 21);
            this.label3.Text = "Số lượng:";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniExit_Click);
            // 
            // nudQty
            // 
            this.nudQty.Location = new System.Drawing.Point(81, 3);
            this.nudQty.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudQty.Name = "nudQty";
            this.nudQty.Size = new System.Drawing.Size(156, 22);
            this.nudQty.TabIndex = 4;
            // 
            // UpdateWeightView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.nudQty);
            this.Controls.Add(this.label3);
            this.Menu = this.mainMenu1;
            this.Name = "UpdateWeightView";
            this.Text = "Soạn hàng khuyến mãi";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.NumericUpDown nudQty;

    }
}