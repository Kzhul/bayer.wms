﻿namespace Bayer.WMS.Handheld.Views
{
    partial class PrepareView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsPrepareGood = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
            this.colBarcode = new System.Windows.Forms.DataGridTextBoxColumn();
            this.colProductDescription = new System.Windows.Forms.DataGridTextBoxColumn();
            this.colProductLot = new System.Windows.Forms.DataGridTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridTextBoxColumn();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.miFeature = new System.Windows.Forms.MenuItem();
            this.miPackaging = new System.Windows.Forms.MenuItem();
            this.miPrintPallet = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtgPrepareGood = new System.Windows.Forms.DataGrid();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblPrepareCode = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.mniMergePallet = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPrepareGood)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridTableStyle1
            // 
            this.dataGridTableStyle1.GridColumnStyles.Add(this.colBarcode);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.colProductDescription);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.colProductLot);
            this.dataGridTableStyle1.GridColumnStyles.Add(this.colQuantity);
            // 
            // colBarcode
            // 
            this.colBarcode.Format = "";
            this.colBarcode.HeaderText = "Ma Pallet/Thung";
            this.colBarcode.MappingName = "Barcode";
            this.colBarcode.Width = 100;
            // 
            // colProductDescription
            // 
            this.colProductDescription.Format = "";
            this.colProductDescription.HeaderText = "Ten thanh pham";
            this.colProductDescription.MappingName = "ProductDescription";
            this.colProductDescription.Width = 150;
            // 
            // colProductLot
            // 
            this.colProductLot.Format = "";
            this.colProductLot.HeaderText = "Lo thanh pham";
            this.colProductLot.MappingName = "ProductLot";
            this.colProductLot.Width = 70;
            // 
            // colQuantity
            // 
            this.colQuantity.Format = "N0";
            this.colQuantity.HeaderText = "So luong";
            this.colQuantity.MappingName = "Quantity";
            this.colQuantity.Width = 70;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.miFeature);
            this.mainMenu1.MenuItems.Add(this.miExit);
            // 
            // miFeature
            // 
            this.miFeature.MenuItems.Add(this.miPackaging);
            this.miFeature.MenuItems.Add(this.miPrintPallet);
            this.miFeature.MenuItems.Add(this.mniMergePallet);
            this.miFeature.Text = "Chức năng";
            // 
            // miPackaging
            // 
            this.miPackaging.Text = "Đóng gói";
            this.miPackaging.Click += new System.EventHandler(this.miPackaging_Click);
            // 
            // miPrintPallet
            // 
            this.miPrintPallet.Text = "In nhãn pallet";
            this.miPrintPallet.Click += new System.EventHandler(this.miPrintPallet_Click);
            // 
            // miExit
            // 
            this.miExit.Text = "Thoát";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 21);
            this.label1.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "NV:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 21);
            this.label4.Text = "SH:";
            // 
            // dtgPrepareGood
            // 
            this.dtgPrepareGood.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPrepareGood.DataSource = this.bdsPrepareGood;
            this.dtgPrepareGood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPrepareGood.Location = new System.Drawing.Point(0, 0);
            this.dtgPrepareGood.Name = "dtgPrepareGood";
            this.dtgPrepareGood.RowHeadersVisible = false;
            this.dtgPrepareGood.Size = new System.Drawing.Size(232, 162);
            this.dtgPrepareGood.TabIndex = 20;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 221);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 47);
            this.lblStepHints.Text = "Quét mã NV";
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Chức năng";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Location = new System.Drawing.Point(34, 0);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(203, 21);
            // 
            // lblPrepareCode
            // 
            this.lblPrepareCode.Location = new System.Drawing.Point(34, 21);
            this.lblPrepareCode.Name = "lblPrepareCode";
            this.lblPrepareCode.Size = new System.Drawing.Size(203, 21);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(0, 30);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 188);
            this.tabControl1.TabIndex = 24;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblCompanyName);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.lblPrepareCode);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.lblEmployeeName);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(240, 165);
            this.tabPage1.Text = "Thông tin phiếu";
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Location = new System.Drawing.Point(34, 42);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(203, 63);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 21);
            this.label2.Text = "KH:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgPrepareGood);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(232, 162);
            this.tabPage2.Text = "Chia tiết";
            // 
            // mniMergePallet
            // 
            this.mniMergePallet.Text = "Dồn pallet";
            this.mniMergePallet.Click += new System.EventHandler(this.mniMergePallet_Click);
            // 
            // PrepareView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "PrepareView";
            this.Text = "Soạn Hàng";
            ((System.ComponentModel.ISupportInitialize)(this.bdsPrepareGood)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem miFeature;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.DataGridTableStyle dataGridTableStyle1;
        private System.Windows.Forms.DataGridTextBoxColumn colBarcode;
        private System.Windows.Forms.DataGridTextBoxColumn colProductDescription;
        private System.Windows.Forms.DataGridTextBoxColumn colProductLot;
        private System.Windows.Forms.DataGridTextBoxColumn colQuantity;
        private System.Windows.Forms.BindingSource bdsPrepareGood;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGrid dtgPrepareGood;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.MenuItem miPackaging;
        private System.Windows.Forms.MenuItem miPrintPallet;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblPrepareCode;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MenuItem mniMergePallet;
    }
}