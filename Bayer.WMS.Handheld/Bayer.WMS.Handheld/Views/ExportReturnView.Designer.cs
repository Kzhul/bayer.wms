﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ExportReturnView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.bdsExportReturnGood = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtgExportReturnGood = new System.Windows.Forms.DataGrid();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblExportCode = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bdsExportReturnGood)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.miExit);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = " ";
            // 
            // miExit
            // 
            this.miExit.Text = "Thoát";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 21);
            this.label1.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "NV:";
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 248);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 20);
            this.lblStepHints.Text = "Quét mã NV";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 21);
            this.label2.Text = "TH:";
            // 
            // dtgExportReturnGood
            // 
            this.dtgExportReturnGood.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgExportReturnGood.DataSource = this.bdsExportReturnGood;
            this.dtgExportReturnGood.Location = new System.Drawing.Point(0, 72);
            this.dtgExportReturnGood.Name = "dtgExportReturnGood";
            this.dtgExportReturnGood.RowHeadersVisible = false;
            this.dtgExportReturnGood.Size = new System.Drawing.Size(240, 173);
            this.dtgExportReturnGood.TabIndex = 12;
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Location = new System.Drawing.Point(34, 27);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(203, 21);
            // 
            // lblExportCode
            // 
            this.lblExportCode.Location = new System.Drawing.Point(34, 48);
            this.lblExportCode.Name = "lblExportCode";
            this.lblExportCode.Size = new System.Drawing.Size(203, 21);
            // 
            // ExportReturnView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblExportCode);
            this.Controls.Add(this.lblEmployeeName);
            this.Controls.Add(this.dtgExportReturnGood);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "ExportReturnView";
            this.Text = "Xuất trả hàng";
            ((System.ComponentModel.ISupportInitialize)(this.bdsExportReturnGood)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsExportReturnGood;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGrid dtgExportReturnGood;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblExportCode;
    }
}