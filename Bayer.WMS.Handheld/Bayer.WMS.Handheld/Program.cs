﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Media;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;

namespace Bayer.WMS.Handheld
{
    static class Program
    {
        public static string AppPath;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            try
            {
                Application.Run(new MainView());
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Utility.ConnStr))
                {
                    Utility.WriteLogToDB(ex);
                }
                using (var sw = new StreamWriter(Path.Combine(Program.AppPath, "Error.txt"), true))
                {
                    string message = ex.Message;
                    string stackTrace = ex.StackTrace;
                    string innerException_Message = string.Empty;
                    string innerException_StackTrace = string.Empty;
                    sw.WriteLine(message);
                    sw.WriteLine(stackTrace);
                    if (ex.InnerException != null)
                    {
                        innerException_Message = ex.InnerException.Message;
                        innerException_StackTrace = ex.InnerException.StackTrace;
                        sw.WriteLine(innerException_Message);
                        sw.WriteLine(innerException_StackTrace);
                    }
                    
                }
            }
        }

        public static void LoadConfig()
        {
            Program.AppPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().GetName().CodeBase);
            string configPath = Path.Combine(Utility.AppPath, "AppConfig.txt");

            try
            {
                var config = new Dictionary<string, string>();
                using (var sr = new StreamReader(configPath))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] tmp = sr.ReadLine().Split(':');
                        if (tmp.Length == 2)
                            config.Add(tmp[0].Trim(), tmp[1].Trim());
                    }
                }

                ReadConnectionString(config["ConnectionString"]);
                ReadCOMConfig(config["COMConfig"]);
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
        }

        public static void ReadConnectionString(string config)
        {
            #region DEBUG MODE
            //1.Connect to to Home using wifi
            //2.Connect to to Home over the Internet
            //3.Using config

            if (config.Contains("debug"))
            {
                #region //Try to connect to local wifi at home
                //Try to connect to local wifi at home
                try
                {
                    string connStr = String.Format("Server={0};Database={1};Uid={2};Pwd={3};", "192.168.100.4,1433", "Bayer.WMS", "sa", "123456aA@");
                    Utility.ConnStr = connStr;
                    using (var sqlConn = new SqlConnection(Utility.ConnStr))
                    {
                        sqlConn.Open();
                    }

                    MessageBox.Show("Connected to Local Home !");
                }
                #endregion
                #region //Try to connect over internet to home
                catch (Exception ex)
                {
                    try
                    {
                        string connStr = String.Format("Server={0};Database={1};Uid={2};Pwd={3};", "syvn7154.dynu.com,1433", "Bayer.WMS", "sa", "123456aA@");
                        Utility.ConnStr = connStr;
                        using (var sqlConn = new SqlConnection(Utility.ConnStr))
                        {
                            sqlConn.Open();
                        }

                        MessageBox.Show("Connected to Home over Internet !");
                    }
                #endregion
                    #region Try To Connect using config
                    catch (Exception exInternet)
                    {
                        try
                        {
                            string[] connStrComponent = config.Split(';');
                            string server = connStrComponent.FirstOrDefault(p => p.StartsWith("Server")).Replace("Server=", String.Empty).Trim();
                            string database = connStrComponent.FirstOrDefault(p => p.StartsWith("Database")).Replace("Database=", String.Empty).Trim();
                            string userid = connStrComponent.FirstOrDefault(p => p.StartsWith("Uid")).Replace("Uid=", String.Empty).Trim();
                            string pwd = connStrComponent.FirstOrDefault(p => p.StartsWith("Pwd")).Replace("Pwd=", String.Empty).Trim();

                            string connStr = String.Format("Server={0};Database={1};Uid={2};Pwd={3};", server, database, (userid), (pwd));
                            Utility.ConnStr = connStr;
                            using (var sqlConn = new SqlConnection(Utility.ConnStr))
                            {
                                sqlConn.Open();
                            }

                            MessageBox.Show("Connected to DB using config file !");
                        }
                        catch (Exception exConfig)
                        {
                            throw new Exception("Debug mode không thành công. Xem lại config.");
                        }
                    }
                }
                    #endregion

            }
            #endregion
            else
            {
                string[] connStrComponent = config.Split(';');
                string server = connStrComponent.FirstOrDefault(p => p.StartsWith("Server")).Replace("Server=", String.Empty).Trim();
                string database = connStrComponent.FirstOrDefault(p => p.StartsWith("Database")).Replace("Database=", String.Empty).Trim();
                string userid = connStrComponent.FirstOrDefault(p => p.StartsWith("Uid")).Replace("Uid=", String.Empty).Trim();
                string pwd = connStrComponent.FirstOrDefault(p => p.StartsWith("Pwd")).Replace("Pwd=", String.Empty).Trim();

                //string connStr = String.Format("Server={0};Database={1};Uid={2};pwd={3};",
                //    server, database, AESDecrypt(userid), AESDecrypt(pwd));
                Utility.ConnStr = config;
            }
        }

        public static void ReadCOMConfig(string config)
        {
            string[] comConfigComponent = config.Split(';');
            Utility.COM_PortName = comConfigComponent.FirstOrDefault(p => p.StartsWith("PortName")).Replace("PortName=", String.Empty).Trim();
            Utility.COM_BaudRate = int.Parse(comConfigComponent.FirstOrDefault(p => p.StartsWith("BaudRate")).Replace("BaudRate=", String.Empty).Trim());
            Utility.COM_Parity = comConfigComponent.FirstOrDefault(p => p.StartsWith("Parity")).Replace("Parity=", String.Empty).Trim();
            Utility.COM_DataBits = int.Parse(comConfigComponent.FirstOrDefault(p => p.StartsWith("DataBits")).Replace("DataBits=", String.Empty).Trim());
            Utility.COM_StopBits = comConfigComponent.FirstOrDefault(p => p.StartsWith("StopBits")).Replace("StopBits=", String.Empty).Trim();
        }
    }
}