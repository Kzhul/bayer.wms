﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bayer.WMS.Handheld.Views;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using Intermec.DataCollection;
using System.IO.Ports;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld
{
    public partial class MainView : Form
    {
        private BarcodeReader _barcodeReader;

        public MainView()
        {
            InitializeComponent();
            Utility.AppPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().GetName().CodeBase);

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void MainView_Load(object sender, EventArgs e)
        {
            try
            {
                LoadConfig();
                LoadCartonLabelTemplate();
                LoadCartonLabelForMultiProductTemplate();
                LoadPalletLabelTemplate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            //string barcode = bre.strDataBuffer;
            //if (barcode == "XH")
            //    btnExportGood_Click(btnExportGood, new EventArgs());
            //else if (barcode == "NH")
            //    btnReceiveGood_Click(btnReceiveGood, new EventArgs());
            //else if (barcode == "CH")
            //    btnSplitGood_Click(btnSplitGood, new EventArgs());
            //else if (barcode == "SH")
            //    btnPrepareGood_Click(btnPrepareGood, new EventArgs());
            //else if (barcode == "GH")
            //    btnDeliveryGood_Click(btnDeliveryGood, new EventArgs());
            //else if (barcode == "TH")
            //    btnExportReturn_Click(btnExportReturn, new EventArgs());
            //else if (barcode == "NT")
            //    btnReceiveReturn_Click(btnReceiveReturn, new EventArgs());
        }

        public void ReadConnectionString(string config)
        {
            #region DEBUG MODE
            //1.Connect to to Home using wifi
            //2.Connect to to Home over the Internet
            //3.Using config

            if (config.Contains("debug"))
            {
                #region //Try to connect to local wifi at home
                //Try to connect to local wifi at home
                try
                {
                    string connStr = String.Format("Server={0};Database={1};Uid={2};Pwd={3};", "192.168.100.4,1433", "Bayer.WMS", "sa", "123456aA@");
                    Utility.ConnStr = connStr;
                    using (var sqlConn = new SqlConnection(Utility.ConnStr))
                    {
                        sqlConn.Open();
                    }

                    MessageBox.Show("Connected to Local Home !");
                }
                #endregion
                #region //Try to connect over internet to home
                catch (Exception ex)
                {
                    try
                    {
                        string connStr = String.Format("Server={0};Database={1};Uid={2};Pwd={3};", "syvn7154.dynu.com,1433", "Bayer.WMS", "sa", "123456aA@");
                        Utility.ConnStr = connStr;
                        using (var sqlConn = new SqlConnection(Utility.ConnStr))
                        {
                            sqlConn.Open();
                        }

                        MessageBox.Show("Connected to Home over Internet !");
                    }
                #endregion
                    #region Try To Connect using config
                    catch (Exception exInternet)
                    {
                        try
                        {
                            string[] connStrComponent = config.Split(';');
                            string server = connStrComponent.FirstOrDefault(p => p.StartsWith("Server")).Replace("Server=", String.Empty).Trim();
                            string database = connStrComponent.FirstOrDefault(p => p.StartsWith("Database")).Replace("Database=", String.Empty).Trim();
                            string userid = connStrComponent.FirstOrDefault(p => p.StartsWith("Uid")).Replace("Uid=", String.Empty).Trim();
                            string pwd = connStrComponent.FirstOrDefault(p => p.StartsWith("Pwd")).Replace("Pwd=", String.Empty).Trim();

                            string connStr = String.Format("Server={0};Database={1};Uid={2};Pwd={3};", server, database, (userid), (pwd));
                            Utility.ConnStr = connStr;
                            using (var sqlConn = new SqlConnection(Utility.ConnStr))
                            {
                                sqlConn.Open();
                            }

                            MessageBox.Show("Connected to DB using config file !");
                        }
                        catch (Exception exConfig)
                        {
                            throw new Exception("Debug mode không thành công. Xem lại config.");
                        }
                    }
                }
                    #endregion

            }
            #endregion
            else
            {
                string[] connStrComponent = config.Split(';');
                string server = connStrComponent.FirstOrDefault(p => p.StartsWith("Server")).Replace("Server=", String.Empty).Trim();
                string database = connStrComponent.FirstOrDefault(p => p.StartsWith("Database")).Replace("Database=", String.Empty).Trim();
                string userid = connStrComponent.FirstOrDefault(p => p.StartsWith("Uid")).Replace("Uid=", String.Empty).Trim();
                string pwd = connStrComponent.FirstOrDefault(p => p.StartsWith("Pwd")).Replace("Pwd=", String.Empty).Trim();
                string connectionTimeout = connStrComponent.FirstOrDefault(p => p.StartsWith("Connection Timeout")).Replace("Connection Timeout=", String.Empty).Trim();

                //string connStr = String.Format("Server={0};Database={1};Uid={2};pwd={3};",
                //    server, database, AESDecrypt(userid), AESDecrypt(pwd));
                Utility.ConnStr = config;

                using (var conn = new SqlConnection(Utility.ConnStr))
                {
                    conn.Open();
                    conn.Close();
                }

            }
        }

        public void ReadCOMConfig(string config)
        {
            string[] comConfigComponent = config.Split(';');
            Utility.COM_PortName = comConfigComponent.FirstOrDefault(p => p.StartsWith("PortName")).Replace("PortName=", String.Empty).Trim();
            Utility.COM_BaudRate = int.Parse(comConfigComponent.FirstOrDefault(p => p.StartsWith("BaudRate")).Replace("BaudRate=", String.Empty).Trim());
            Utility.COM_Parity = comConfigComponent.FirstOrDefault(p => p.StartsWith("Parity")).Replace("Parity=", String.Empty).Trim();
            Utility.COM_DataBits = int.Parse(comConfigComponent.FirstOrDefault(p => p.StartsWith("DataBits")).Replace("DataBits=", String.Empty).Trim());
            Utility.COM_StopBits = comConfigComponent.FirstOrDefault(p => p.StartsWith("StopBits")).Replace("StopBits=", String.Empty).Trim();
        }

        public void LoadConfig()
        {
            string configPath = Path.Combine(Utility.AppPath, "AppConfig.txt");

            try
            {
                var config = new Dictionary<string, string>();
                using (var sr = new StreamReader(configPath))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] tmp = sr.ReadLine().Split(':');
                        if (tmp.Length == 2)
                            config.Add(tmp[0].Trim(), tmp[1].Trim());
                    }
                }

                ReadConnectionString(config["ConnectionString"]);
                ReadCOMConfig(config["COMConfig"]);
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
            catch (SqlException)
            {
                throw new Exception("Không thể kết nối đến hệ thống.");
            }
        }

        public void LoadCartonLabelTemplate()
        {
            string templatePath = Path.Combine(Utility.AppPath, @"Template\CartonLabelTemplate.txt");

            try
            {
                using (var sr = new StreamReader(templatePath))
                {
                    Utility.CartonLabelTemplate = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
        }

        public void LoadCartonLabelForMultiProductTemplate()
        {
            string templatePath = Path.Combine(Utility.AppPath, @"Template\CartonLabelForMultiProductTemplate.txt");

            try
            {
                using (var sr = new StreamReader(templatePath))
                {
                    Utility.CartonLabelForMultiProductTemplate = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
        }

        public void LoadPalletLabelTemplate()
        {
            string templatePath = Path.Combine(Utility.AppPath, @"Template\PalletLabelTemplate.txt");

            try
            {
                using (var sr = new StreamReader(templatePath))
                {
                    Utility.PalletLabelTemplate = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file template.");
            }
        }

        private void mniExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mniExport_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new ExportView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniReceiveReturn_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new ReceiveReturnView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniReceiveReturnWithoutDocument_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new ReceiveReturnWithoutDocumentView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniJoinCarton_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new MergeCartonView())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniReceive_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new ReceiveView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniSplit_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new SplitView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniPrepare_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new PrepareView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniDelivery_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new DeliveryView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniExportReturn_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new ExportReturnView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mnExportReturnWithoutDocument_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new ExportReturnWithoutDocumentView())
                {
                    view.ShowDialog();
                    view.DisposeBarcodeReader();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }

        private void mniBarcodeTracking_Click(object sender, EventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();

            try
            {
                using (var view = new BarcodeTrackingView())
                {
                    view.ShowDialog();
                    view.Dispose();
                }
            }
            finally
            {
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
        }
    }
}