﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Bayer.WMS.Handheld.Models;

namespace Bayer.WMS.Handheld.Process
{
    public class Process
    {
        protected string _doImportCode;
        protected string _referenceNbr;
        protected string _currentBarcode;
        protected string _currentType;
        protected DataTable _dtPalletStatusesSummary;
        protected DataTable _dtPalletStatusesDetails;
        protected Pallet _destPallet;

        public string CurrentBarcode
        {
            get { return _currentBarcode; }
        }

        public string CurrentType
        {
            get { return _currentType; }
        }

        public string DOImportCode
        {
            get { return _doImportCode; }
        }

        public string ReferenceNbr
        {
            get { return _referenceNbr; }
        }

        public DataTable PalletStatusesSummary
        {
            get { return _dtPalletStatusesSummary; }
            set { _dtPalletStatusesSummary = value; }
        }

        public DataTable PalletStatusesDetails
        {
            get { return _dtPalletStatusesDetails; }
            set { _dtPalletStatusesDetails = value; }
        }

        public Pallet DestPallet
        {
            get { return _destPallet; }
        }
    }
}
