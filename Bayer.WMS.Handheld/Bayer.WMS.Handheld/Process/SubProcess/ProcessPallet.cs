﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Bayer.WMS.Handheld.Models;

namespace Bayer.WMS.Handheld.Process.SubProcess
{
    public class ProcessPallet
    {
        public DataTable Process_PalletStatuses_ForReceiveReturn(string palletCode)
        {
            var dt = Pallet.LoadRequirePalletStatusGroupByBarcode(palletCode, Utility.PalletStatusExportedReturn);
            return dt;
        }

        public Pallet Process_NewPallet(string palletCode)
        {
            var pallet = Pallet.Load(palletCode);

            if (pallet.Status != Utility.PalletStatusNew)
                throw new Exception("Trạng thái pallet không hợp lệ, vui lòng kiểm tra lại dữ liệu");

            return pallet;
        }

        public Pallet Process_NewOrExportedPallet(string palletCode)
        {
            var pallet = Pallet.Load(palletCode);

            if (pallet.Status != Utility.PalletStatusNew && pallet.Status != Utility.PalletStatusExported)
                throw new Exception("Trạng thái pallet không hợp lệ, vui lòng kiểm tra lại dữ liệu");

            return pallet;
        }

        public Pallet Process_NewOrReceivedPallet(string palletCode)
        {
            var pallet = Pallet.Load(palletCode);

            if (pallet.Status != Utility.PalletStatusNew && pallet.Status != Utility.PalletStatusReceived)
                throw new Exception("Trạng thái pallet không hợp lệ, vui lòng kiểm tra lại dữ liệu");

            return pallet;
        }

        public Pallet Process_NewOrExportedReturnPallet(string palletCode)
        {
            var pallet = Pallet.Load(palletCode);

            if (pallet.Status != Utility.PalletStatusNew && pallet.Status != Utility.PalletStatusExportedReturn)
                throw new Exception("Trạng thái pallet không hợp lệ, vui lòng kiểm tra lại dữ liệu");

            return pallet;
        }
    }
}
