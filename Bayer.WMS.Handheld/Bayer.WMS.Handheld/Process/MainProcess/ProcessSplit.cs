﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Bayer.WMS.Handheld.Process.SubProcess;
using System.Data;
using Bayer.WMS.Handheld.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessSplit : Process
    {
        private readonly ProcessPallet _processPallet;

        private string _companyCode;
        private string _companyName;
        private Dictionary<string, string> _movedItemsDic;
        private DataTable _dtSplitNoteDetails;
        private Product _promotion;

        public ProcessSplit()
        {
            _processPallet = new ProcessPallet();
        }

        private void ValidateExistOnDocument(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (_dtSplitNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"])).Length == 0)
                    throw new Exception("Số lô không đúng với phiếu xuất hàng, vui lòng kiểm tra lại dữ liệu.");
            }
        }

        private void ValidateExistOnScan(string barcode, string type, DataTable details)
        {
            if (details != null)
            {
                string filterExp = String.Format("{0} = '{1}'", type == "C" ? "CartonBarcode" : "ProductBarcode", barcode);
                if (details.Select(filterExp).Length > 0)
                {
                    if (type == "C")
                        throw new Exception("Đã quét mã sản phẩm trong thùng này nên không thể quét ược, vui lòng kiểm tra lại dữ liệu.");
                    if (type == "E")
                        throw new Exception("Đã quét mã thùng chứa sản phẩm này nên không thể quét được, vui lòng kiểm tra lại dữ liệu.");
                }
            }
        }

        private void ValidateSplitForCustomer(DataTable dt, string comapnyCode)
        {
            foreach (DataRow row in dt.Rows)
            {
                string filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}' AND CompanyCode = '{2}'", row["ProductID"], row["ProductLot"], comapnyCode);
                var requestRow = _dtSplitNoteDetails.Select(filterExp).FirstOrDefault();

                if (requestRow == null)
                    throw new Exception(String.Format("Sản phẩm '{0}' không được chia cho khách hàng hiện tại", row["ProductDescription"]));
            }
        }

        private void LoadPalletStatusDetails(string barcode, string type, ref DataTable details)
        {
            var dtMoveDetails = Pallet.LoadPalletStatuses(barcode, type);
            foreach (DataRow row in dtMoveDetails.Rows)
                details.Rows.Add(row.ItemArray);
        }

        public int ValidateState(DataTable dt)
        {
            var products = new List<Product>();
            foreach (DataRow row in dt.Rows)
            {
                int splittedQty = 0;
                string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}' AND Status = 'S'", row["ProductID"], row["ProductLot"]);
                var oldRow = _dtPalletStatusesSummary.Select(filterExp).FirstOrDefault();
                if (oldRow != null)
                    splittedQty = int.Parse(row["ProductQty"].ToString());

                int productID = int.Parse(row["ProductID"].ToString());
                string productLot = row["ProductLot"].ToString();
                int qty = int.Parse(row["ProductQty"].ToString()) - splittedQty;

                var product = products.FirstOrDefault(p => p.ProductID == productID && p.ProductLot == productLot);
                if (product == null)
                    products.Add(new Product { ProductID = productID, ProductLot = productLot, Quantity = qty });
                else
                    product.Quantity += qty;
            }

            int state = 0;
            foreach (var item in products)
            {
                string filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}' AND CompanyCode = '{2}'", item.ProductID, item.ProductLot, _companyCode);
                var row = _dtSplitNoteDetails.Select(filterExp).FirstOrDefault();
                if (decimal.Parse(row["Quantity"].ToString()) > decimal.Parse(row["SplittedQty"].ToString()) + item.Quantity)
                {
                    state = 1;
                    break;
                }
                else if (decimal.Parse(row["Quantity"].ToString()) < decimal.Parse(row["SplittedQty"].ToString()) + item.Quantity)
                {
                    state = 2;
                    break;
                }
            }

            return state;
        }

        public bool IsComplete()
        {
            return _dtSplitNoteDetails.Select("Quantity <> SplittedQty").Length == 0;
        }

        public void Process(string barcode)
        {
            _currentBarcode = Barcode.Process(barcode, true, false, false, out _currentType);

            _dtPalletStatusesSummary = Pallet.LoadOptionPalletStatus(_currentBarcode, Utility.PalletStatusNew, Utility.PalletStatusReceived, Utility.PalletStatusSplitted);
            _dtPalletStatusesSummary.Columns.Add("RequestQty", typeof(decimal));

            _movedItemsDic = new Dictionary<string, string>();

            //// check pallet
            ////    - Is empty
            ////    - Splitted but same export document
            string status = _dtPalletStatusesSummary.Rows[0]["Status"].ToString();
            if (status == Utility.PalletStatusSplitted && _dtPalletStatusesSummary.Rows[0]["ReferenceNbr"].ToString() != _referenceNbr)
                throw new Exception("Pallet đã chia cho phiếu khác, vui lòng kiểm tra lại dữ liệu.");

            _companyCode = String.Empty;
            if (status == Utility.PalletStatusSplitted)
            {
                _companyCode = _dtPalletStatusesSummary.Rows[0]["CompanyCode"].ToString();
                var row = _dtSplitNoteDetails.Select(String.Format("CompanyCode = '{0}'", _companyCode)).FirstOrDefault();
                _companyName = row["CompanyName"].ToString();
            }

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("Process"), String.Format("{0} - {1}", _referenceNbr, _currentBarcode));

            if (_dtPalletStatusesSummary.Rows[0]["ProductQty"].ToString() != "0" && !String.IsNullOrEmpty(_companyCode))
            {
                ValidateExistOnDocument(_dtPalletStatusesSummary);

                foreach (DataRow row in _dtPalletStatusesSummary.Rows)
                {
                    string filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}' AND CompanyCode = '{2}'", row["ProductID"], row["ProductLot"], _companyCode);
                    var requestRow = _dtSplitNoteDetails.Select(filterExp).FirstOrDefault();

                    row["RequestQty"] = requestRow["Quantity"];
                }
            }
        }

        public void ProcessCfm(string barcode, ref int state, out bool isMoveItem, out bool isPreparePromotion, ref DataTable summary, ref DataTable details)
        {
            var method = this.GetType().GetMethod("ProcessCfm");

            isMoveItem = false;
            isPreparePromotion = false;
            string type = String.Empty;
            switch (state)
            {
                case 0:
                    if (Utility.CustomerRegex.IsMatch(barcode))
                    {
                        string companyCode = barcode.Remove(0, 3);
                        var tmp = _dtSplitNoteDetails.Select(String.Format("CompanyCode = '{0}'", companyCode)).FirstOrDefault();

                        if (summary.Rows[0]["ProductQty"].ToString() != "0")
                        {
                            ValidateSplitForCustomer(summary, companyCode);

                            foreach (DataRow row in summary.Rows)
                            {
                                string filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}' AND CompanyCode = '{2}'", row["ProductID"], row["ProductLot"], companyCode);
                                var requestRow = _dtSplitNoteDetails.Select(filterExp).FirstOrDefault();

                                row["RequestQty"] = requestRow["Quantity"];
                            }
                        }

                        _companyCode = companyCode;
                        _companyName = tmp["CompanyName"].ToString();

                        Utility.RecordAuditTrail(null, AuditTrailAction.ScanCustomer, this.GetType().GetMethod("ProcessCfm"), String.Format("{0} - {1} - {2}", _referenceNbr, _currentBarcode, barcode));
                    }
                    else
                        throw new Exception("Mã không hợp lệ, vui lòng kiểm tra lại dữ liệu.");
                    break;
                case 1:
                    //// Split promotion
                    if (Utility.ProductRegex.IsMatch(barcode))
                    {
                        var product = Product.Load(barcode.Remove(0, 3));
                        if (product.CategoryID != 4)
                            throw new Exception("Chỉ chấp nhận quét mã sản phẩm KM, vui lòng kiểm tra lại dữ liệu");
                        if (_dtSplitNoteDetails.Select(String.Format("ProductID = {0}", product.ProductID)).Length == 0)
                            throw new Exception("Không tìm thấy sản phẩm KM trong phiếu, vui lòng kiểm tra lại dữ liệu");

                        isPreparePromotion = true;
                    }
                    else
                    {
                        //// Allow pallet, carton and product
                        barcode = Barcode.Process(barcode, true, true, true, out type);

                        if (type == "P")
                        {
                            if (barcode == _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString())
                                throw new Exception("Thao tác không đúng, không thể chuyển hàng lên cùng pallet");

                            //// Only allow pallet which status is new or received
                            _destPallet = _processPallet.Process_NewOrReceivedPallet(barcode);
                            if (_destPallet.Status == Utility.PalletStatusReceived && _destPallet.DOImportCode != _doImportCode)
                                throw new Exception("Pallet đã xuất cho phiếu khác, vui lòng kiểm tra lại dữ liệu.");

                            isMoveItem = true;

                            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("ProcessCfm"), String.Format("{0} - {1} - {2} - San hàng", _referenceNbr, _currentBarcode, barcode));
                        }
                        else
                        {
                            //// Move on current pallet
                            ProcessMoveToCurrentPallet(barcode, type, ref summary, ref details);

                            string action = type == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;
                            Utility.RecordAuditTrail(null, action, this.GetType().GetMethod("ProcessCfm"), String.Format("{0} - {1} - {2}", _referenceNbr, _currentBarcode, barcode));
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Move carton or product from another pallet to current pallet
        /// </summary>
        public void ProcessMoveToCurrentPallet(string barcode, string type, ref DataTable summary, ref DataTable details)
        {
            ValidateExistOnScan(barcode, type, details);

            //// Only allow carton or product on pallet which status is new
            var dtMoveSummary = Pallet.LoadRequirePalletStatus(barcode, type, Utility.PalletStatusReceived);

            //// Validate something here
            ValidateSplitForCustomer(dtMoveSummary, _companyCode);

            string currentPalletCode = summary.Rows[0]["PalletCode"].ToString();
            foreach (DataRow row in dtMoveSummary.Rows)
            {
                string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}'", row["ProductID"], row["ProductLot"]);
                var actualRow = summary.Select(filterExp).FirstOrDefault();

                if (actualRow == null)
                {
                    filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}' AND CompanyCode = '{2}'", row["ProductID"], row["ProductLot"], _companyCode);
                    var requestRow = _dtSplitNoteDetails.Select(filterExp).FirstOrDefault();

                    summary.Rows.Add(currentPalletCode, String.Empty, _referenceNbr, _companyCode, row["ProductLot"], row["ProductQty"],
                        type == "C" ? 1 : 0, row["ProductID"], row["ProductCode"], row["ProductDescription"], type, requestRow["Quantity"]);
                }
                else
                    actualRow["ProductQty"] = int.Parse(actualRow["ProductQty"].ToString()) + int.Parse(row["ProductQty"].ToString());
            }

            LoadPalletStatusDetails(barcode, type, ref details);
            _movedItemsDic.Add(barcode, type);

            if (summary.Rows[0]["ProductQty"].ToString() == "0")
                summary.Rows.RemoveAt(0);
        }

        //public void ProcessPromotion(int qty)
        //{
        //    string filterExp = String.Format("ProductID = {0} AND Status <> 'S'", _promotion.ProductID);
        //    var actualRow = _dtPalletStatusesSummary.Select(filterExp).FirstOrDefault();

        //    string currentPalletCode = _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString();
        //    if (actualRow == null)
        //        _dtPalletStatusesSummary.Rows.Add(currentPalletCode, String.Empty, _splitCode, _companyCode, String.Empty, qty,
        //            0, _promotion.ProductID, _promotion.ProductCode, _promotion.ProductDescription, "K");
        //    else
        //        actualRow["ProductQty"] = int.Parse(actualRow["ProductQty"].ToString()) + qty;

        //    bool isReload = false;
        //    if (!_productIDList.Contains(_promotion.ProductID.ToString()))
        //    {
        //        isReload = true;
        //        _productIDList.Add(_promotion.ProductID.ToString());
        //    }

        //    _movedItemsDic.Add(_promotion.ProductID + "_" + _promotion.ProductCode, qty.ToString());

        //    if (_dtPalletStatusesSummary.Rows[0]["ProductQty"].ToString() == "0")
        //        _dtPalletStatusesSummary.Rows.RemoveAt(0);
        //}

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdateSplittedQty(DataTable summary, DataTable details)
        {
            var method = this.GetType().GetMethod("UpdateSplittedQty");

            //// save to database
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();

                try
                {
                    Dictionary<DataRow, decimal> rows = new Dictionary<DataRow, decimal>();

                    foreach (DataRow row in summary.Rows)
                    {
                        string filerExp = String.Format("ProductID = {0} AND ProductLot = '{1}' AND Status = 'S'", row["ProductID"], row["ProductLot"]);

                        decimal oldSplittedQty = 0;
                        var oldRow = _dtPalletStatusesSummary.Select(filerExp).FirstOrDefault();
                        if (oldRow != null)
                            oldSplittedQty = decimal.Parse(oldRow["ProductQty"].ToString());

                        //// update on screen
                        filerExp = String.Format("ProductID = {0} AND BatchCode = '{1}' AND CompanyCode = '{2}'", row["ProductID"], row["ProductLot"], _companyCode);
                        var selectedRow = _dtSplitNoteDetails.Select(filerExp).FirstOrDefault();
                        decimal quantity = decimal.Parse(selectedRow["Quantity"].ToString());
                        decimal splittedQty = decimal.Parse(selectedRow["SplittedQty"].ToString());
                        decimal productQty = decimal.Parse(row["ProductQty"].ToString());
                        if (quantity < splittedQty + productQty - oldSplittedQty)
                            throw new Exception(String.Format("Số lượng lô {0} vượt quá số lượng phiếu chia cho KH {2}", row["ProductLot"], _companyName));

                        splittedQty = splittedQty + productQty - oldSplittedQty;
                        rows.Add(selectedRow, splittedQty);

                        //// update splitted qty of split note
                        using (var cmd = new SqlCommand("proc_SplitNote_Update_SplittedQty", conn, tran))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@SplitCode", selectedRow["SplitCode"]));
                            cmd.Parameters.Add(new SqlParameter("@DOImportCode", selectedRow["DOImportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@CompanyCode", selectedRow["CompanyCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ProductID", selectedRow["ProductID"]));
                            cmd.Parameters.Add(new SqlParameter("@BatchCode", selectedRow["BatchCode"]));
                            cmd.Parameters.Add(new SqlParameter("@SplittedQty", splittedQty));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method.ReflectedType.FullName));

                            cmd.ExecuteNonQuery();
                        }
                    }

                    string palletCode = _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString();

                    //// Move product to this pallet
                    foreach (var item in _movedItemsDic)
                    {
                        if (item.Value == "C" || item.Value == "E")
                            Pallet.UpdatePallet_MoveProduct(palletCode, item.Key, item.Value, method.ReflectedType.FullName, conn, tran);
                        else
                        {
                            string[] tmp = item.Key.Split('_');
                            Pallet.UpdatePallet_MovePromotion(palletCode, int.Parse(tmp[0]), tmp[1], _referenceNbr, _companyCode, int.Parse(item.Value),
                                method.ReflectedType.FullName, conn, tran);
                        }
                    }

                    //// Update status of pallet to prevent duplicate process
                    Pallet.UpdatePallet_Status(palletCode, _doImportCode, _referenceNbr, _companyCode, Utility.PalletStatusSplitted, method.ReflectedType.FullName, conn, tran);

                    foreach (var row in rows)
                    {
                        row.Key["SplittedQty"] = row.Value;
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }

            foreach (DataRow row in details.Rows)
            {
                _dtPalletStatusesDetails.Rows.Add(row.ItemArray);
            }
        }

        /// <summary>
        /// Move product from current pallet to another pallet which status is new or received.
        /// Data will be save immediately to database
        /// </summary>
        public void UpdateMoveToNewOrReceivedPallet(ref DataTable summary, ref DataTable details, DataTable moveItems)
        {
            var method = this.GetType().GetMethod("UpdateMoveToNewOrReceivedPallet");
            var backup = summary.Copy();

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();

                try
                {
                    string referenceNbr = String.Empty;
                    if (moveItems.Rows.Count > 0)
                    {
                        string barcode = moveItems.Rows[0]["Barcode"].ToString();
                        string type = moveItems.Rows[0]["Type"].ToString();

                        var dtMoveSummary = Pallet.LoadRequirePalletStatus(barcode, type, Utility.PalletStatusReceived);
                        referenceNbr = dtMoveSummary.Rows[0]["ReferenceNbr"].ToString();
                    }

                    foreach (DataRow row in moveItems.Rows)
                    {
                        string barcode = row["Barcode"].ToString();
                        string type = row["Type"].ToString();

                        //// move carton or product to another pallet
                        Pallet.UpdatePallet_MoveProduct(_destPallet.PalletCode, barcode, type, method.ReflectedType.FullName, conn, tran);
                        Pallet.UpdatePallet_Status(_destPallet.PalletCode, _doImportCode, referenceNbr, null, Utility.PalletStatusReceived, method.ReflectedType.FullName, conn, tran);
                    }

                    //// update on screen
                    foreach (DataRow row in moveItems.Rows)
                    {
                        string barcode = row["Barcode"].ToString();
                        string type = row["Type"].ToString();

                        string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}'", row["ProductID"], row["ProductLot"]);
                        var selectedRow = summary.Select(filterExp).FirstOrDefault();

                        selectedRow["ProductQty"] = int.Parse(selectedRow["ProductQty"].ToString()) - int.Parse(row["ProductQty"].ToString());

                        filterExp = String.Format("{0} = '{1}'", type == "C" ? "CartonBarcode" : "ProductBarcode", barcode);
                        var movedRows = details.Select(filterExp);

                        foreach (DataRow movedRow in movedRows)
                        {
                            details.Rows.Remove(movedRow);
                        }
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    summary = backup;

                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public string CompanyCode
        {
            get { return _companyCode; }
        }

        public string CompanyName
        {
            get { return _companyName; }
        }

        public DataTable SplitNoteDetails
        {
            get { return _dtSplitNoteDetails; }
            set
            {
                _dtSplitNoteDetails = value;
                _doImportCode = _dtSplitNoteDetails.Rows[0]["DOImportCode"].ToString();
                _referenceNbr = _dtSplitNoteDetails.Rows[0]["SplitCode"].ToString();
            }
        }
    }
}
