﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Bayer.WMS.Handheld.Process.SubProcess;
using Bayer.WMS.Handheld.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessPrepare : Process
    {
        private readonly ProcessPallet _processPallet;

        private string _companyCode;
        private string _companyName;
        private int _oddQty;
        private Pallet _pallet;
        private DateTime _deliveryDate;
        private DataTable _dtPrepareNoteDetails;
        private DataTable _dtTemp;

        public ProcessPrepare()
        {
            _processPallet = new ProcessPallet();
        }

        private void ValidateExistOnDocument(Pallet pallet)
        {
            if (pallet.CompanyCode != _companyCode)
                throw new Exception("Pallet này không được chia cho khách hàng hiện tại, vui lòng kiểm tra lại dữ liệu.");
        }

        public bool IsComplete()
        {
            return _dtPrepareNoteDetails.Select("Quantity <> PreparedQty").Length == 0;
        }

        public void Process(string barcode)
        {
            _pallet = Pallet.Load(barcode);

            ValidateExistOnDocument(_pallet);

            _dtPalletStatusesSummary = Pallet.LoadRequirePalletStatusGroupByBarcode(barcode, Utility.PalletStatusSplitted);
            _dtTemp = _dtPalletStatusesSummary.Copy();

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("Process"), String.Format("{0} - {1} - {2}", _referenceNbr, _pallet.PalletCode, barcode));
        }

        public void ProcessCfm(string barcode, out string type, out string packingType, out int oddQty, out bool isComplete)
        {
            barcode = Barcode.Process(barcode, false, true, true, out type);

            var row = _dtTemp.Select(String.Format("Barcode = '{0}'", barcode)).FirstOrDefault();
            if (row == null)
                throw new Exception("Không tìm thấy mã trên pallet đang nhận.");

            packingType = row["PackingType"].ToString();
            oddQty = int.Parse(row["OddQuantity"].ToString());

            _oddQty += oddQty;
            _dtTemp.Rows.Remove(row);

            isComplete = _dtTemp.Select("PackingType = 'C'").FirstOrDefault() == null;

            string action = type == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;
            Utility.RecordAuditTrail(null, action, this.GetType().GetMethod("ProcessCfm"), String.Format("{0} - {1} - {2}", _referenceNbr, _pallet.PalletCode, barcode));
        }

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdatePreparedQty()
        {
            var method = this.GetType().GetMethod("UpdatePreparedQty");

            //// save to database
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();
                try
                {
                    var productList = new List<Product>();

                    foreach (DataRow row in _dtPalletStatusesSummary.Rows)
                    {
                        int productID = int.Parse(row["ProductID"].ToString());
                        string productLot = row["ProductLot"].ToString();
                        decimal quantity = decimal.Parse(row["Quantity"].ToString());

                        var product = productList.FirstOrDefault(p => p.ProductID == productID && p.ProductLot == productLot);
                        if (product == null)
                            productList.Add(new Product { ProductID = productID, ProductLot = productLot, Quantity = quantity });
                        else
                            product.Quantity += quantity;
                    }

                    foreach (var product in productList)
                    {
                        //// update on screen
                        var selectedRow = _dtPrepareNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", product.ProductID, product.ProductLot)).FirstOrDefault();

                        selectedRow["PreparedQty"] = decimal.Parse(selectedRow["PreparedQty"].ToString()) + product.Quantity;

                        //// update confirm qty of delivery note
                        using (var cmd = new SqlCommand("proc_PrepareNote_Update_PreparedQty", conn, tran))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@PrepareCode", selectedRow["PrepareCode"]));
                            cmd.Parameters.Add(new SqlParameter("@DOImportCode", selectedRow["DOImportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ProductID", selectedRow["ProductID"]));
                            cmd.Parameters.Add(new SqlParameter("@BatchCode", selectedRow["BatchCode"]));
                            cmd.Parameters.Add(new SqlParameter("@PreparedQty", selectedRow["PreparedQty"]));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method.ReflectedType.FullName));

                            cmd.ExecuteNonQuery();
                        }
                    }

                    //// update status of pallet to prevent duplicate process
                    string palletCode = _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString();
                    Pallet.UpdatePallet_Status(palletCode, _doImportCode, _referenceNbr, _companyCode, Utility.PalletStatusPrepared, method.ReflectedType.FullName, conn, tran);

                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public string CompanyCode
        {
            get { return _companyCode; }
        }

        public string CompanyName
        {
            get { return _companyName; }
        }

        public DateTime DeliveryDate
        {
            get { return _deliveryDate; }
        }

        public int OddQty
        {
            get { return _oddQty; }
            set { _oddQty = value; }
        }

        public DataTable PrepareNoteDetails
        {
            get { return _dtPrepareNoteDetails; }
            set
            {
                _dtPrepareNoteDetails = value;
                _doImportCode = _dtPrepareNoteDetails.Rows[0]["DOImportCode"].ToString();
                _referenceNbr = _dtPrepareNoteDetails.Rows[0]["PrepareCode"].ToString();
                _companyCode = _dtPrepareNoteDetails.Rows[0]["CompanyCode"].ToString();
                _companyName = _dtPrepareNoteDetails.Rows[0]["CompanyName"].ToString();
                _deliveryDate = DateTime.Parse(_dtPrepareNoteDetails.Rows[0]["DeliveryDate"].ToString());
            }
        }

        public DataTable Temp
        {
            get { return _dtTemp; }
        }
    }
}
