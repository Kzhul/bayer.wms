﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Process;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.SubProcess;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessExportReturnWithoutDocument
    {
        private readonly ProcessPallet _processPallet;

        private string _currentBarcode;
        private string _currentType;
        private Dictionary<string, string> _movedItemsDic;
        private DataTable _dtExportReturnNoteDetails;
        private DataTable _dtPalletStatusesSummary;
        private DataTable _dtPalletStatusesDetails;
        private Pallet _destPallet;

        public ProcessExportReturnWithoutDocument()
        {
            _processPallet = new ProcessPallet();
        }

        public void Process(string barcode)
        {
            _currentBarcode = Barcode.Process(barcode, true, false, false, out _currentType);

            _dtPalletStatusesSummary = Pallet.LoadOptionPalletStatus(_currentBarcode);
            _movedItemsDic = new Dictionary<string, string>();

            //// check pallet
            ////    - Is empty
            ////    - Has product but not yet exported
            ////    - Exported but same export document
            string status = _dtPalletStatusesSummary.Rows[0]["Status"].ToString();
            if (status == Utility.PalletStatusExportedReturn && _dtPalletStatusesSummary.Rows[0]["ReferenceNbr"] != null)
                throw new Exception("Pallet đã xuất cho phiếu khác, vui lòng kiểm tra lại dữ liệu.");

            string action = _currentType == "P" ? AuditTrailAction.ScanPallet : _currentType == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;
            Utility.RecordAuditTrail(null, action, this.GetType().GetMethod("Process"), _currentBarcode);
        }

        public void ProcessCfm(string barcode, ref int state, out bool isMoveItem)
        {
            var method = this.GetType().GetMethod("ProcessCfm");

            isMoveItem = false;
            string type = String.Empty;
            switch (state)
            {
                //case 0:
                //    //// Allow pallet
                //    barcode = Barcode.Process(barcode, true, false, false, out type);

                //    //// Only allow pallet which status is new
                //    _destPallet = _processPallet.Process_NewOrExportedReturnPallet(barcode);
                //    isMoveItem = true;
                //    break;
                case 1:
                    //// Allow pallet, carton and product
                    barcode = Barcode.Process(barcode, true, true, true, out type);

                    //// Move on current pallet
                    ProcessMoveToCurrentPallet(barcode, type);
                    break;
                //case 2:
                //    //// Allow pallet
                //    barcode = Barcode.Process(barcode, true, false, false, out type);

                //    //// Move to another pallet
                //    ProcessMoveToAnotherPallet(barcode, type);
                //    break;
            }

            string action = type == "P" ? AuditTrailAction.ScanPallet : type == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;
            Utility.RecordAuditTrail(null, action, this.GetType().GetMethod("ProcessCfm"), barcode);
        }

        /// <summary>
        /// Move carton or product from another pallet to current pallet
        /// </summary>
        public void ProcessMoveToCurrentPallet(string barcode, string type)
        {
            //// Only allow carton or product on pallet which status is new
            var dtMoveSummary = Pallet.LoadRequirePalletStatus(barcode, type, Utility.PalletStatusReceived, Utility.PalletStatusSplitted, Utility.PalletStatusPrepared);

            //// update on screen
            string currentPalletCode = _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString();
            foreach (DataRow row in dtMoveSummary.Rows)
            {
                string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}' AND Status <> 'X'", row["ProductID"], row["ProductLot"]);
                var actualRow = _dtPalletStatusesSummary.Select(filterExp).FirstOrDefault();

                if (actualRow == null)
                    _dtPalletStatusesSummary.Rows.Add(currentPalletCode, row["Status"], row["ReferenceNbr"], row["CompanyCode"], row["ProductLot"], row["ProductQty"],
                        type == "C" ? 1 : 0, row["ProductID"], row["ProductCode"], row["ProductDescription"], type);
                else
                    actualRow["ProductQty"] = int.Parse(actualRow["ProductQty"].ToString()) + int.Parse(row["ProductQty"].ToString());
            }

            _movedItemsDic.Add(barcode, type);

            if (_dtPalletStatusesSummary.Rows[0]["ProductQty"].ToString() == "0")
                _dtPalletStatusesSummary.Rows.RemoveAt(0);
        }

        /// <summary>
        /// Move carton or product to another pallet
        /// </summary>
        public void ProcessMoveToAnotherPallet(Pallet destPallet)
        {
            foreach (DataRow row in _dtPalletStatusesSummary.Rows)
            {
                row["PalletCode"] = destPallet.PalletCode;
            }
        }

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdateExportedReturnQty(DataTable summary, DataTable details)
        {
            var method = this.GetType().GetMethod("UpdateExportedReturnQty");

            //// save to database
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();
                try
                {
                    foreach (DataRow row in _dtPalletStatusesSummary.Rows)
                    {
                        string filerExp = String.Format("ProductID = {0} AND ProductLot = '{1}' AND Status = 'E'", row["ProductID"], row["ProductLot"]);

                        decimal oldExportedReturnQty = 0;
                        var oldRow = _dtPalletStatusesSummary.Select(filerExp).FirstOrDefault();
                        if (oldRow != null)
                            oldExportedReturnQty = decimal.Parse(oldRow["ProductQty"].ToString());

                        //// update on screen
                        var selectedRow = _dtExportReturnNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"])).FirstOrDefault();
                        if (selectedRow != null)
                        {
                            decimal returnedQty = decimal.Parse(selectedRow["ReturnedQty"].ToString());
                            decimal productQty = decimal.Parse(row["ProductQty"].ToString());

                            selectedRow["ReturnedQty"] = returnedQty + productQty - oldExportedReturnQty;
                        }
                        else
                            _dtExportReturnNoteDetails.Rows.Add(row["ProductLot"], row["ProductID"], row["ProductCode"], row["ProductDescription"], row["ProductQty"].ToString());
                    }

                    string palletCode = _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString();

                    //// Move product to this pallet
                    foreach (var item in _movedItemsDic)
                    {
                        //// update export qty
                        //// update confirm qty of delivery note
                        using (var cmd = new SqlCommand("proc_DeliveryOrder_Update_ReturnQty", conn, tran))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@Barcode", item.Key));
                            cmd.Parameters.Add(new SqlParameter("@Type", item.Value));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method.ReflectedType.FullName));

                            cmd.ExecuteNonQuery();
                        }

                        Pallet.UpdatePallet_MoveProduct(palletCode, item.Key, item.Value, method.ReflectedType.FullName, conn, tran);
                    }

                    //// Update status of pallet to prevent duplicate process
                    Pallet.UpdatePallet_Status(palletCode, null, null, null, Utility.PalletStatusExportedReturn, method.ReflectedType.FullName, conn, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// Move product from current pallet to another pallet which status is new or received.
        /// Data will be save immediately to database
        /// </summary>
        public void UpdateMoveToNewOrExportedReturnPallet(ref DataTable summary, DataTable moveItems)
        {
            var method = this.GetType().GetMethod("UpdateMoveToNewOrReceivedPallet");

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();

                try
                {
                    string status = String.Empty;
                    string referenceNbr = String.Empty;
                    string companyCode = String.Empty;
                    if (moveItems.Rows.Count > 0)
                    {
                        string barcode = moveItems.Rows[0]["Barcode"].ToString();
                        string type = moveItems.Rows[0]["Type"].ToString();

                        var dtMoveSummary = Pallet.LoadRequirePalletStatus(barcode, type, Utility.PalletStatusReceived, Utility.PalletStatusSplitted, Utility.PalletStatusPrepared);
                        status = dtMoveSummary.Rows[0]["Status"].ToString();
                        referenceNbr = dtMoveSummary.Rows[0]["ReferenceNbr"].ToString();
                        companyCode = dtMoveSummary.Rows[0]["CompanyCode"].ToString();
                    }

                    foreach (DataRow row in moveItems.Rows)
                    {
                        string barcode = row["Barcode"].ToString();
                        string type = row["Type"].ToString();

                        //// move carton or product to another pallet
                        Pallet.UpdatePallet_MoveProduct(_destPallet.PalletCode, barcode, type, method.ReflectedType.FullName, conn, tran);
                        Pallet.UpdatePallet_Status(_destPallet.PalletCode, null, referenceNbr, companyCode, status, method.ReflectedType.FullName, conn, tran);
                    }

                    //// update on screen
                    foreach (DataRow row in moveItems.Rows)
                    {
                        string barcode = row["Barcode"].ToString();
                        string type = row["Type"].ToString();

                        string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}'", row["ProductID"], row["ProductLot"]);
                        var selectedRow = summary.Select(filterExp).FirstOrDefault();

                        selectedRow["ProductQty"] = int.Parse(selectedRow["ProductQty"].ToString()) - int.Parse(row["ProductQty"].ToString());
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public string CurrentBarcode
        {
            get { return _currentBarcode; }
        }

        public string CurrentType
        {
            get { return _currentType; }
        }

        public DataTable ExportReturnNoteDetails
        {
            get { return _dtExportReturnNoteDetails; }
            set { _dtExportReturnNoteDetails = value; }
        }

        public DataTable PalletStatusesSummary
        {
            get { return _dtPalletStatusesSummary; }
            set { _dtPalletStatusesSummary = value; }
        }

        public DataTable PalletStatusesDetails
        {
            get { return _dtPalletStatusesDetails; }
            set { _dtPalletStatusesDetails = value; }
        }
    }
}
