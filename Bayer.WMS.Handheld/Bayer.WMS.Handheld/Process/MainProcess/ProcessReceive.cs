﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Bayer.WMS.Handheld.Process.SubProcess;
using Bayer.WMS.Handheld.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessReceive : Process
    {
        private readonly ProcessPallet _processPallet;

        private Pallet _pallet;
        private DataTable _dtExportNoteDetails;
        private DataTable _dtTemp;

        public ProcessReceive()
        {
            _processPallet = new ProcessPallet();
        }

        private void ValidateExistOnDocument(Pallet pallet)
        {
            if (pallet.ReferenceNbr != _referenceNbr)
                throw new Exception("Pallet này không được xuất cho phiếu này, vui lòng kiểm tra lại dữ liệu.");
        }

        public bool IsComplete()
        {
            return _dtExportNoteDetails.Select("ReceivedQty <> ExportedQty").Length == 0;
        }

        public void Process(string barcode)
        {
            _pallet = Pallet.Load(barcode);

            ValidateExistOnDocument(_pallet);

            _dtPalletStatusesSummary = Pallet.LoadRequirePalletStatusGroupByBarcode(barcode, Utility.PalletStatusExported);
            _dtTemp = _dtPalletStatusesSummary.Copy();

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("Process"), String.Format("{0} - {1}", _referenceNbr, barcode));
        }

        public void ProcessCfm(string barcode, out string type, out string packingType, out int oddQty, out bool isComplete)
        {
            barcode = Barcode.Process(barcode, false, true, true, out type);

            var row = _dtTemp.Select(String.Format("Barcode = '{0}'", barcode)).FirstOrDefault();
            if (row == null)
                throw new Exception("Không tìm thấy mã trên pallet đang nhận.");

            packingType = row["PackingType"].ToString();
            oddQty = int.Parse(row["OddQuantity"].ToString());

            _dtTemp.Rows.Remove(row);

            isComplete = _dtTemp.Select("PackingType = 'C'").FirstOrDefault() == null;

            string scan = packingType == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;

            Utility.RecordAuditTrail(null, scan, this.GetType().GetMethod("ProcessCfm"), String.Format("{0} - {1} - {2}", _referenceNbr, _pallet.PalletCode, barcode));
        }

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdateReceivedQty()
        {
            var method = this.GetType().GetMethod("UpdateReceivedQty");

            //// save to database
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();
                try
                {
                    var productList = new List<Product>();

                    foreach (DataRow row in _dtPalletStatusesSummary.Rows)
                    {
                        int productID = int.Parse(row["ProductID"].ToString());
                        string productLot = row["ProductLot"].ToString();
                        decimal quantity = decimal.Parse(row["Quantity"].ToString());

                        var product = productList.FirstOrDefault(p => p.ProductID == productID && p.ProductLot == productLot);
                        if (product == null)
                            productList.Add(new Product { ProductID = productID, ProductLot = productLot, Quantity = quantity });
                        else
                            product.Quantity += quantity;
                    }

                    foreach (var product in productList)
                    {
                        //// update on screen
                        var selectedRow = _dtExportNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", product.ProductID, product.ProductLot)).FirstOrDefault();

                        selectedRow["ReceivedQty"] = decimal.Parse(selectedRow["ReceivedQty"].ToString()) + product.Quantity;

                        //// update confirm qty of delivery note
                        using (var cmd = new SqlCommand("proc_DeliveryNote_Update_ReceivedQty", conn, tran))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@ExportCode", selectedRow["ExportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@DOImportCode", selectedRow["DOImportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ProductID", selectedRow["ProductID"]));
                            cmd.Parameters.Add(new SqlParameter("@BatchCode", selectedRow["BatchCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ReceivedQty", selectedRow["ReceivedQty"]));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method.ReflectedType.FullName));

                            cmd.ExecuteNonQuery();
                        }
                    }

                    //// update status of pallet to prevent duplicate process
                    string palletCode = _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString();
                    Pallet.UpdatePallet_Status(palletCode, _doImportCode, _referenceNbr, null, Utility.PalletStatusReceived, method.ReflectedType.FullName, conn, tran);

                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public DataTable ExportNoteDetails
        {
            get { return _dtExportNoteDetails; }
            set
            {
                _dtExportNoteDetails = value;
                _doImportCode = _dtExportNoteDetails.Rows[0]["DOImportCode"].ToString();
                _referenceNbr = _dtExportNoteDetails.Rows[0]["ExportCode"].ToString();
            }
        }


        public DataTable Temp
        {
            get { return _dtTemp; }
        }
    }
}
