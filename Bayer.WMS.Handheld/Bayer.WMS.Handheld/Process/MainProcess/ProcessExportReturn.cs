﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Process;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.SubProcess;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessExportReturn : Process
    {
        private readonly ProcessPallet _processPallet;

        private string _companyCode;
        private Dictionary<string, string> _movedItemsDic;
        private DataTable _dtExportReturnNoteDetails;
        private DataTable _dtDetailsOnPallet;
        private Pallet _destPallet;

        public ProcessExportReturn()
        {
            _processPallet = new ProcessPallet();
        }

        private void ValidateExistOnDocument(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (_dtExportReturnNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"])).Length == 0)
                    throw new Exception("Số lô không đúng với phiếu xuất hàng, vui lòng kiểm tra lại dữ liệu.");
            }
        }

        private void ValidateExistOnScan(string barcode, string type, DataTable details)
        {
            if (details != null)
            {
                string filterExp = String.Format("{0} = '{1}'", type == "C" ? "CartonBarcode" : "ProductBarcode", barcode);
                if (details.Select(filterExp).Length > 0)
                {
                    if (type == "C")
                        throw new Exception("Đã quét mã sản phẩm trong thùng này nên không thể quét được, vui lòng kiểm tra lại dữ liệu.");
                    if (type == "E")
                        throw new Exception("Đã quét mã thùng chứa sản phẩm này nên không thể quét được, vui lòng kiểm tra lại dữ liệu.");
                }
            }
        }

        private void LoadPalletStatusDetails(string barcode, string type, ref DataTable details)
        {
            var dtMoveDetails = Pallet.LoadPalletStatuses(barcode, type);
            foreach (DataRow row in dtMoveDetails.Rows)
            {
                details.Rows.Add(row.ItemArray);
            }
        }

        public bool IsComplete()
        {
            return _dtExportReturnNoteDetails.Select("RemainQty <> ExportedReturnQty").Length == 0;
        }

        public int ValidateState(DataTable dt)
        {
            var products = new List<Product>();
            foreach (DataRow row in dt.Rows)
            {
                int exportedReturnQty = 0;
                string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}' AND Status = 'X'", row["ProductID"], row["ProductLot"]);
                var oldRow = _dtPalletStatusesSummary.Select(filterExp).FirstOrDefault();
                if (oldRow != null)
                    exportedReturnQty = int.Parse(row["ProductQty"].ToString());

                int productID = int.Parse(row["ProductID"].ToString());
                string productLot = row["ProductLot"].ToString();
                int qty = int.Parse(row["ProductQty"].ToString()) - exportedReturnQty;

                var product = products.FirstOrDefault(p => p.ProductID == productID && p.ProductLot == productLot);
                if (product == null)
                    products.Add(new Product { ProductID = productID, ProductLot = productLot, Quantity = qty });
                else
                    product.Quantity += qty;
            }

            int state = 0;
            foreach (var item in products)
            {
                var row = _dtExportReturnNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", item.ProductID, item.ProductLot)).FirstOrDefault();
                if (decimal.Parse(row["RemainQty"].ToString()) > decimal.Parse(row["ExportedQty"].ToString()) + item.Quantity)
                {
                    state = 1;
                    break;
                }
                else if (decimal.Parse(row["RemainQty"].ToString()) < decimal.Parse(row["ExportedQty"].ToString()) + item.Quantity)
                {
                    state = 2;
                    break;
                }
            }

            return state;
        }

        public void Process(string barcode)
        {
            _currentBarcode = Barcode.Process(barcode, true, false, false, out _currentType);

            _dtPalletStatusesSummary = Pallet.LoadOptionPalletStatus(_currentBarcode, Utility.PalletStatusNew, Utility.PalletStatusExported);
            _dtPalletStatusesSummary.Columns.Add("RequestQty", typeof(decimal));
            _dtDetailsOnPallet = null;

            //// check pallet
            ////    - Is empty
            ////    - Has product but not yet exported
            ////    - Exported but same export document
            string status = _dtPalletStatusesSummary.Rows[0]["Status"].ToString();
            if (status == Utility.PalletStatusExportedReturn && _dtPalletStatusesSummary.Rows[0]["ReferenceNbr"].ToString() != _referenceNbr)
                throw new Exception("Pallet đã xuất cho phiếu khác, vui lòng kiểm tra lại dữ liệu.");

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("Process"), _currentBarcode);

            if (_dtPalletStatusesSummary.Rows[0]["ProductQty"].ToString() != "0")
            {
                ValidateExistOnDocument(_dtPalletStatusesSummary);

                foreach (DataRow row in _dtPalletStatusesSummary.Rows)
                {
                    string filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"]);
                    var requestRow = _dtExportReturnNoteDetails.Select(filterExp).FirstOrDefault();

                    row["RequestQty"] = requestRow["Quantity"];
                }

                //// Pallet has product and not yet exported
                if (status != Utility.PalletStatusNew && status != Utility.PalletStatusExportedReturn)
                    _dtDetailsOnPallet = Pallet.LoadPalletStatuses(_currentBarcode, _currentType);
            }
        }

        public void ProcessCfm(string barcode, int state, out bool isMoveItem, ref DataTable summary, ref DataTable details)
        {
            var method = this.GetType().GetMethod("ProcessCfm");

            isMoveItem = false;
            string type = String.Empty;
            barcode = Barcode.Process(barcode, true, true, true, out type);

            if (type == "P")
            {
                //// Only allow pallet which status is new
                _destPallet = _processPallet.Process_NewPallet(barcode);
                isMoveItem = true;
            }
            else
            {
                //// Move on current pallet
                ProcessMoveToCurrentPallet(barcode, type, ref summary, ref details);
            }

            string action = type == "P" ? AuditTrailAction.ScanPallet : type == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;
            Utility.RecordAuditTrail(null, action, this.GetType().GetMethod("ProcessCfm"), barcode);
        }

        /// <summary>
        /// Move carton or product from another pallet to current pallet
        /// </summary>
        public void ProcessMoveToCurrentPallet(string barcode, string type, ref DataTable summary, ref DataTable details)
        {
            ValidateExistOnScan(barcode, type, details);

            //// Only allow carton or product on pallet which status is new
            var dtMoveSummary = Pallet.LoadRequirePalletStatus(barcode, type, Utility.PalletStatusReceived, Utility.PalletStatusSplitted, Utility.PalletStatusPrepared);

            ValidateExistOnDocument(dtMoveSummary);

            string doImportCode = dtMoveSummary.Rows[0]["DOImportCode"].ToString();
            string status = dtMoveSummary.Rows[0]["Status"].ToString();

            if (doImportCode != _doImportCode)
                throw new Exception("Xuất trả không đúng nội dung phiếu, vui lòng kiểm tra lại");

            if (status == Utility.PalletStatusSplitted || status == Utility.PalletStatusPrepared)
            {
                string companyCode = dtMoveSummary.Rows[0]["CompanyCode"].ToString();
                if (companyCode != _companyCode)
                    throw new Exception("Xuất trả không đúng nội dung phiếu, vui lòng kiểm tra lại");
            }

            //// update on screen
            string currentPalletCode = summary.Rows[0]["PalletCode"].ToString();
            foreach (DataRow row in dtMoveSummary.Rows)
            {
                string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}'", row["ProductID"], row["ProductLot"]);
                var actualRow = summary.Select(filterExp).FirstOrDefault();

                if (actualRow == null)
                {
                    filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"]);
                    var requestRow = _dtExportReturnNoteDetails.Select(filterExp).FirstOrDefault();

                    summary.Rows.Add(currentPalletCode, String.Empty, _referenceNbr, String.Empty, row["ProductLot"], row["ProductQty"],
                        type == "C" ? 1 : 0, row["ProductID"], row["ProductCode"], row["ProductDescription"], type, requestRow["Quantity"]);
                }
                else
                    actualRow["ProductQty"] = int.Parse(actualRow["ProductQty"].ToString()) + int.Parse(row["ProductQty"].ToString());
            }

            LoadPalletStatusDetails(barcode, type, ref details);
            _movedItemsDic.Add(barcode, type);

            if (summary.Rows[0]["ProductQty"].ToString() == "0")
                summary.Rows.RemoveAt(0);
        }

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdateExportedReturnQty(DataTable summary, DataTable details)
        {
            var method = this.GetType().GetMethod("UpdateExportedReturnQty");

            //// save to database
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();
                try
                {
                    foreach (DataRow row in _dtPalletStatusesSummary.Rows)
                    {
                        string filerExp = String.Format("ProductID = {0} AND ProductLot = '{1}' AND Status = 'E'", row["ProductID"], row["ProductLot"]);

                        decimal oldExportedReturnQty = 0;
                        var oldRow = _dtPalletStatusesSummary.Select(filerExp).FirstOrDefault();
                        if (oldRow != null)
                            oldExportedReturnQty = decimal.Parse(oldRow["ProductQty"].ToString());

                        //// update on screen
                        filerExp = String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"]);
                        var selectedRow = _dtExportReturnNoteDetails.Select(filerExp).FirstOrDefault();
                        decimal remainQty = decimal.Parse(selectedRow["RemainQty"].ToString());
                        decimal exportedReturnQty = decimal.Parse(selectedRow["ExportedReturnQty"].ToString());
                        decimal productQty = decimal.Parse(row["ProductQty"].ToString());
                        if (remainQty < exportedReturnQty + productQty - oldExportedReturnQty)
                            throw new Exception(String.Format("Số lượng lô {0} của mã {1} vượt quá số lượng phiếu trả", row["ProductLot"], row["ProductCode"]));

                        selectedRow["ExportedReturnQty"] = exportedReturnQty + productQty - oldExportedReturnQty;

                        //// update export qty
                        //// update confirm qty of delivery note
                        using (var cmd = new SqlCommand("proc_ReturnDO_Update_ExportReturnQty", conn, tran))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@ReturnDOCode", selectedRow["ReturnDOCode"]));
                            cmd.Parameters.Add(new SqlParameter("@DOImportCode", selectedRow["DOImportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ProductID", selectedRow["ProductID"]));
                            cmd.Parameters.Add(new SqlParameter("@BatchCode", selectedRow["BatchCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ExportedReturnQty", selectedRow["ExportedReturnQty"]));
                            cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", row["ReferenceNbr"]));
                            cmd.Parameters.Add(new SqlParameter("@CompanyCode", row["CompanyCode"]));
                            cmd.Parameters.Add(new SqlParameter("@Status", row["Status"]));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method.ReflectedType.FullName));

                            cmd.ExecuteNonQuery();
                        }
                    }

                    string palletCode = _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString();

                    //// Move product to this pallet if barcode is carton or product
                    if (_currentType == "C" || _currentType == "E")
                        Pallet.UpdatePallet_MoveProduct(palletCode, _currentBarcode, _currentType, method.ReflectedType.FullName, conn, tran);

                    //// Move product to this pallet
                    foreach (var item in _movedItemsDic)
                    {
                        Pallet.UpdatePallet_MoveProduct(palletCode, item.Key, item.Value, method.ReflectedType.FullName, conn, tran);
                    }

                    //// Update status of pallet to prevent duplicate process
                    Pallet.UpdatePallet_Status(palletCode, _doImportCode, _referenceNbr, null, Utility.PalletStatusExportedReturn, method.ReflectedType.FullName, conn, tran);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }

            foreach (DataRow row in details.Rows)
            {
                _dtPalletStatusesDetails.Rows.Add(row.ItemArray);
            }
        }

        /// <summary>
        /// Move product from current pallet to another pallet which status is new or received.
        /// Data will be save immediately to database
        /// </summary>
        public void UpdateMoveToNewOrExportedReturnPallet(ref DataTable summary, DataTable moveItems)
        {
            var method = this.GetType().GetMethod("UpdateMoveToNewOrReceivedPallet");

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();

                try
                {
                    string status = String.Empty;
                    string referenceNbr = String.Empty;
                    string companyCode = String.Empty;
                    if (moveItems.Rows.Count > 0)
                    {
                        string barcode = moveItems.Rows[0]["Barcode"].ToString();
                        string type = moveItems.Rows[0]["Type"].ToString();

                        var dtMoveSummary = Pallet.LoadRequirePalletStatus(barcode, type, Utility.PalletStatusReceived, Utility.PalletStatusSplitted, Utility.PalletStatusPrepared);
                        status = dtMoveSummary.Rows[0]["Status"].ToString();
                        referenceNbr = dtMoveSummary.Rows[0]["ReferenceNbr"].ToString();
                        companyCode = dtMoveSummary.Rows[0]["CompanyCode"].ToString();
                    }

                    foreach (DataRow row in moveItems.Rows)
                    {
                        string barcode = row["Barcode"].ToString();
                        string type = row["Type"].ToString();

                        //// move carton or product to another pallet
                        Pallet.UpdatePallet_MoveProduct(_destPallet.PalletCode, barcode, type, method.ReflectedType.FullName, conn, tran);
                        Pallet.UpdatePallet_Status(_destPallet.PalletCode, _doImportCode, referenceNbr, companyCode, status, method.ReflectedType.FullName, conn, tran);
                    }

                    //// update on screen
                    foreach (DataRow row in moveItems.Rows)
                    {
                        string barcode = row["Barcode"].ToString();
                        string type = row["Type"].ToString();

                        string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}'", row["ProductID"], row["ProductLot"]);
                        var selectedRow = summary.Select(filterExp).FirstOrDefault();

                        selectedRow["ProductQty"] = int.Parse(selectedRow["ProductQty"].ToString()) - int.Parse(row["ProductQty"].ToString());
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public DataTable ExportReturnNoteDetails
        {
            get { return _dtExportReturnNoteDetails; }
            set
            {
                _dtExportReturnNoteDetails = value;
                _doImportCode = _dtExportReturnNoteDetails.Rows[0]["DOImportCode"].ToString();
                _referenceNbr = _dtExportReturnNoteDetails.Rows[0]["ReturnDOCode"].ToString();
                _companyCode = _dtExportReturnNoteDetails.Rows[0]["CompanyCode"].ToString();
            }
        }

        public DataTable DetailsOnPallet
        {
            get { return _dtDetailsOnPallet; }
        }
    }
}
