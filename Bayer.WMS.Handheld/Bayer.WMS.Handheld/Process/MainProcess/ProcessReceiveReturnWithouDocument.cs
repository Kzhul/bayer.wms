﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Bayer.WMS.Handheld.Process.SubProcess;
using Bayer.WMS.Handheld.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessReceiveReturnWithouDocument
    {
        private readonly ProcessPallet _processPallet;

        private DataTable _dtExportReturnNoteDetails;
        private DataTable _dtPalletStatuses;
        private DataTable _dtTemp;

        public ProcessReceiveReturnWithouDocument()
        {
            _processPallet = new ProcessPallet();
        }

        public bool IsComplete()
        {
            return _dtExportReturnNoteDetails.Rows.Count == _dtExportReturnNoteDetails.Select("IsComplete = 1").Length;
        }

        public void Process(string barcode)
        {
            _dtPalletStatuses = Pallet.LoadRequirePalletStatusGroupByBarcode(barcode, Utility.PalletStatusExportedReturn);
            _dtTemp = _dtPalletStatuses.Copy();

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("Process"), barcode);
        }

        public void ProcessCfm(string barcode, out string type, out string packingType, out int oddQty, out bool isComplete)
        {
            barcode = Barcode.Process(barcode, false, true, true, out type);

            var row = _dtTemp.Select(String.Format("Barcode = '{0}'", barcode)).FirstOrDefault();
            if (row == null)
                throw new Exception("Không tìm thấy mã trên pallet đang nhận.");

            packingType = row["PackingType"].ToString();
            oddQty = int.Parse(row["OddQuantity"].ToString());

            _dtTemp.Rows.Remove(row);

            isComplete = _dtTemp.Rows.Count == 0;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("ProcessCfm"), barcode);
        }

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdateReceivedReturnQty()
        {
            var method = this.GetType().GetMethod("UpdateReceivedReturnQty");

            //// save to database
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();
                try
                {
                    var productList = new List<Product>();

                    foreach (DataRow row in _dtPalletStatuses.Rows)
                    {
                        int productID = int.Parse(row["ProductID"].ToString());
                        string productLot = row["ProductLot"].ToString();
                        decimal quantity = decimal.Parse(row["Quantity"].ToString());

                        var product = productList.FirstOrDefault(p => p.ProductID == productID && p.ProductLot == productLot);
                        if (product == null)
                            productList.Add(new Product { ProductID = productID, ProductLot = productLot, Quantity = quantity });
                        else
                            product.Quantity += quantity;
                    }

                    foreach (var product in productList)
                    {
                        //// update on screen
                        var selectedRow = _dtExportReturnNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", product.ProductID, product.ProductLot)).FirstOrDefault();
                        if (selectedRow != null)
                        {
                            decimal returnedQty = decimal.Parse(selectedRow["ReturnedQty"].ToString());

                            selectedRow["ReturnedQty"] = returnedQty + product.Quantity;
                        }
                        else
                            _dtExportReturnNoteDetails.Rows.Add(product.ProductLot, product.ProductID, product.ProductCode, product.ProductDescription, product.Quantity);
                    }

                    //// update status of pallet to prevent duplicate process
                    string palletCode = _dtPalletStatuses.Rows[0]["PalletCode"].ToString();
                    Pallet.UpdatePallet_Status(palletCode, null, null, null, Utility.PalletStatusNew, method.ReflectedType.FullName, conn, tran);

                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public DataTable ExportReturnNoteDetails
        {
            set { _dtExportReturnNoteDetails = value; }
        }

        public DataTable PalletStatuses
        {
            set { _dtPalletStatuses = value; }
        }

        public DataTable Temp
        {
            get { return _dtTemp; }
        }
    }
}
