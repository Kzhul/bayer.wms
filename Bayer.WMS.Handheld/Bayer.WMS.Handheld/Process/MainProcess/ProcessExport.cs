﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Bayer.WMS.Handheld.Process;
using Bayer.WMS.Handheld.Models;
using Bayer.WMS.Handheld.Process.SubProcess;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessExport : Process
    {
        private readonly ProcessPallet _processPallet;

        private Dictionary<string, string> _movedItemsDic;
        private DataTable _dtExportNoteDetails;
        private DataTable _dtDetailsOnPallet;

        public ProcessExport()
        {
            _processPallet = new ProcessPallet();
        }

        private void ValidateExistOnDocument(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (_dtExportNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"])).Length == 0)
                    throw new Exception("Số lô không đúng với phiếu xuất hàng, vui lòng kiểm tra lại dữ liệu.");
            }
        }

        private void ValidateExistOnScan(string barcode, string type, DataTable details)
        {
            if (details != null)
            {
                string filterExp = String.Format("{0} = '{1}'", type == "C" ? "CartonBarcode" : "ProductBarcode", barcode);
                if (details.Select(filterExp).Length > 0)
                {
                    if (type == "C")
                        throw new Exception("Đã quét mã sản phẩm trong thùng này nên không thể quét được, vui lòng kiểm tra lại dữ liệu.");
                    if (type == "E")
                        throw new Exception("Đã quét mã thùng chứa sản phẩm này nên không thể quét được, vui lòng kiểm tra lại dữ liệu.");
                }
            }
        }

        private void LoadPalletStatusDetails(string barcode, string type, ref DataTable details)
        {
            var dtMoveDetails = Pallet.LoadPalletStatuses(barcode, type);
            foreach (DataRow row in dtMoveDetails.Rows)
                details.Rows.Add(row.ItemArray);
        }

        public int ValidateState(DataTable dt)
        {
            var products = new List<Product>();
            foreach (DataRow row in dt.Rows)
            {
                int exportedQty = 0;
                string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}' AND Status = 'E'", row["ProductID"], row["ProductLot"]);
                var oldRow = _dtPalletStatusesSummary.Select(filterExp).FirstOrDefault();
                if (oldRow != null)
                    exportedQty = int.Parse(row["ProductQty"].ToString());

                int productID = int.Parse(row["ProductID"].ToString());
                string productLot = row["ProductLot"].ToString();
                int qty = int.Parse(row["ProductQty"].ToString()) - exportedQty;

                var product = products.FirstOrDefault(p => p.ProductID == productID && p.ProductLot == productLot);
                if (product == null)
                    products.Add(new Product { ProductID = productID, ProductLot = productLot, Quantity = qty });
                else
                    product.Quantity += qty;
            }

            int state = 0;
            foreach (var item in products)
            {
                var row = _dtExportNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", item.ProductID, item.ProductLot)).FirstOrDefault();
                if (decimal.Parse(row["RemainQty"].ToString()) > decimal.Parse(row["ExportedQty"].ToString()) + item.Quantity)
                {
                    state = 1;
                    break;
                }
                else if (decimal.Parse(row["RemainQty"].ToString()) < decimal.Parse(row["ExportedQty"].ToString()) + item.Quantity)
                {
                    state = 2;
                    break;
                }
            }

            return state;
        }

        public bool IsComplete()
        {
            return _dtExportNoteDetails.Select("RemainQty <> ExportedQty").Length == 0;
        }

        public void Process(string barcode)
        {
            _currentBarcode = Barcode.Process(barcode, true, false, false, out _currentType);

            _dtPalletStatusesSummary = Pallet.LoadOptionPalletStatus(_currentBarcode, Utility.PalletStatusNew, Utility.PalletStatusExported);
            _dtPalletStatusesSummary.Columns.Add("RequestQty", typeof(decimal));
            _dtDetailsOnPallet = null;

            _movedItemsDic = new Dictionary<string, string>();

            //// check pallet
            ////    - Is empty
            ////    - Has product but not yet exported
            ////    - Exported but same export document
            string status = _dtPalletStatusesSummary.Rows[0]["Status"].ToString();
            if (status == Utility.PalletStatusExported && _dtPalletStatusesSummary.Rows[0]["ReferenceNbr"].ToString() != _referenceNbr)
                throw new Exception("Pallet đã xuất cho phiếu khác, vui lòng kiểm tra lại dữ liệu.");

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("Process"), String.Format("{0} - {1}", _referenceNbr, _currentBarcode));

            //// If pallet has product
            if (_dtPalletStatusesSummary.Rows[0]["ProductQty"].ToString() != "0")
            {
                ValidateExistOnDocument(_dtPalletStatusesSummary);

                foreach (DataRow row in _dtPalletStatusesSummary.Rows)
                {
                    string filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"]);
                    var requestRow = _dtExportNoteDetails.Select(filterExp).FirstOrDefault();

                    row["RequestQty"] = requestRow["Quantity"];
                }
            }

            //// Pallet has product and not yet exported
            if (status == Utility.PalletStatusNew)
                _dtDetailsOnPallet = Pallet.LoadPalletStatuses(_currentBarcode, _currentType);
        }

        public void ProcessCfm(string barcode, int state, out bool isMoveItem, ref DataTable summary, ref DataTable details)
        {
            var method = this.GetType().GetMethod("ProcessCfm");

            isMoveItem = false;
            string type = String.Empty;
            //// If users want to move carton or product on current pallet, they will scan qr carton or product
            //// but if they want to split product in carton to ensure quantity, they will scan qr pallet
            //// Allow pallet, carton and product
            barcode = Barcode.Process(barcode, true, true, true, out type);

            if (type == "P")
            {
                //// determine pallet to be moved on
                //// Only allow pallet which status is new
                _destPallet = _processPallet.Process_NewPallet(barcode);
                isMoveItem = true;

                Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("ProcessCfm"), String.Format("{0} - {1} - San hàng", _referenceNbr, barcode));
            }
            else
            {
                //// Move on current pallet
                ProcessMoveToCurrentPallet(barcode, type, ref summary, ref details);

                string action = type == "C" ? AuditTrailAction.ScanCarton : AuditTrailAction.ScanProduct;
                Utility.RecordAuditTrail(null, action, this.GetType().GetMethod("ProcessCfm"), String.Format("{0} - {1} - {2}", _referenceNbr, _currentBarcode, barcode));
            }
        }

        public void ProcessDelete(string barcode, ref DataTable summary, ref DataTable details)
        {
            string type = String.Empty;
            barcode = Barcode.Process(barcode, false, true, true, out type);

            string filterExp = String.Format("{0} = '{1}'", type == "C" ? "CartonBarcode" : "ProductBarcode", barcode);
            var rows = details.Select(filterExp);
            if (rows.Length == 0)
                throw new Exception("Mã không hợp lệ, vui lòng kiểm tra lại dữ liệu.");

            filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}'", rows[0]["ProductID"], rows[0]["ProductLot"]);

            int qty = rows.Sum(p => int.Parse(p["Qty"].ToString()));
            foreach (DataRow row in rows)
            {
                details.Rows.Remove(row);
            }

            var summaryRow = summary.Select(filterExp).FirstOrDefault();
            summaryRow["ProductQty"] = int.Parse(summaryRow["ProductQty"].ToString()) - qty;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanDelete, this.GetType().GetMethod("ProcessDelete"), String.Format("{0} - {1} - {2}", _referenceNbr, _currentBarcode, barcode));
        }

        /// <summary>
        /// Move carton or product from another pallet to current pallet
        /// </summary>
        public void ProcessMoveToCurrentPallet(string barcode, string type, ref DataTable summary, ref DataTable details)
        {
            ValidateExistOnScan(barcode, type, details);

            //// Only allow carton or product on pallet which status is new
            var dtMoveSummary = Pallet.LoadRequirePalletStatus(barcode, type, Utility.PalletStatusNew);

            ValidateExistOnDocument(dtMoveSummary);

            //// update on screen
            string currentPalletCode = summary.Rows[0]["PalletCode"].ToString();
            foreach (DataRow row in dtMoveSummary.Rows)
            {
                string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}'", row["ProductID"], row["ProductLot"]);
                var actualRow = summary.Select(filterExp).FirstOrDefault();

                if (actualRow == null)
                {
                    filterExp = String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"]);
                    var requestRow = _dtExportNoteDetails.Select(filterExp).FirstOrDefault();

                    summary.Rows.Add(currentPalletCode, String.Empty, _referenceNbr, String.Empty, row["ProductLot"], row["ProductQty"],
                        type == "C" ? 1 : 0, row["ProductID"], row["ProductCode"], row["ProductDescription"], type, requestRow["Quantity"]);
                }
                else
                    actualRow["ProductQty"] = int.Parse(actualRow["ProductQty"].ToString()) + int.Parse(row["ProductQty"].ToString());
            }

            LoadPalletStatusDetails(barcode, type, ref details);
            _movedItemsDic.Add(barcode, type);

            if (summary.Rows[0]["ProductQty"].ToString() == "0")
                summary.Rows.RemoveAt(0);
        }

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdateExportedQty(DataTable summary, DataTable details)
        {
            var method = this.GetType().GetMethod("UpdateExportedQty");

            //// save to database
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();

                try
                {
                    Dictionary<DataRow, decimal> rows = new Dictionary<DataRow, decimal>();

                    foreach (DataRow row in summary.Rows)
                    {
                        string filerExp = String.Format("ProductID = {0} AND ProductLot = '{1}' AND Status = 'E'", row["ProductID"], row["ProductLot"]);

                        decimal oldExportedQty = 0;
                        var oldRow = _dtPalletStatusesSummary.Select(filerExp).FirstOrDefault();
                        if (oldRow != null)
                            oldExportedQty = decimal.Parse(oldRow["ProductQty"].ToString());

                        //// update on screen
                        filerExp = String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"]);
                        var selectedRow = _dtExportNoteDetails.Select(filerExp).FirstOrDefault();
                        decimal remainQty = decimal.Parse(selectedRow["RemainQty"].ToString());
                        decimal exportedQty = decimal.Parse(selectedRow["ExportedQty"].ToString());
                        decimal productQty = decimal.Parse(row["ProductQty"].ToString());
                        if (remainQty < exportedQty + productQty - oldExportedQty)
                            throw new Exception(String.Format("Số lượng lô {0} của mã {1} vượt quá số lượng phiếu xuất", row["ProductLot"], row["ProductCode"]));

                        exportedQty = exportedQty + productQty - oldExportedQty;
                        rows.Add(selectedRow, exportedQty);
                        //selectedRow["ExportedQty"] = exportedQty + productQty - oldExportedQty;

                        //// update export qty
                        //// update confirm qty of delivery note
                        using (var cmd = new SqlCommand("proc_DeliveryNote_Update_ExportQty", conn, tran))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@ExportCode", selectedRow["ExportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@DOImportCode", selectedRow["DOImportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ProductID", selectedRow["ProductID"]));
                            cmd.Parameters.Add(new SqlParameter("@BatchCode", selectedRow["BatchCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ExportedQty", exportedQty));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method.ReflectedType.FullName));

                            cmd.ExecuteNonQuery();
                        }
                    }

                    string palletCode = _dtPalletStatusesSummary.Rows[0]["PalletCode"].ToString();

                    //// Move product to this pallet
                    foreach (var item in _movedItemsDic)
                    {
                        Pallet.UpdatePallet_MoveProduct(palletCode, item.Key, item.Value, method.ReflectedType.FullName, conn, tran);
                    }

                    //// Update status of pallet to prevent duplicate process
                    Pallet.UpdatePallet_Status(palletCode, _doImportCode, _referenceNbr, null, Utility.PalletStatusExported, method.ReflectedType.FullName, conn, tran);

                    foreach (var row in rows)
                    {
                        row.Key["ExportedQty"] = row.Value;
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }

            foreach (DataRow row in details.Rows)
            {
                _dtPalletStatusesDetails.Rows.Add(row.ItemArray);
            }
        }

        /// <summary>
        /// Move product from current pallet to another pallet which status is new.
        /// Data will be save immediately to database
        /// </summary>
        public void UpdateMoveToNewPallet(ref DataTable summary, ref DataTable details, DataTable moveItems)
        {
            var method = this.GetType().GetMethod("UpdateMoveToNewPallet");
            var backup = summary.Copy();

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();

                try
                {
                    foreach (DataRow row in moveItems.Rows)
                    {
                        string barcode = row["Barcode"].ToString();
                        string type = row["Type"].ToString();

                        //// move carton or product to another pallet
                        Pallet.UpdatePallet_MoveProduct(_destPallet.PalletCode, barcode, type, method.ReflectedType.FullName, conn, tran);
                    }

                    //// update on screen
                    foreach (DataRow row in moveItems.Rows)
                    {
                        string barcode = row["Barcode"].ToString();
                        string type = row["Type"].ToString();

                        string filterExp = String.Format("ProductID = {0} AND ProductLot = '{1}'", row["ProductID"], row["ProductLot"]);
                        var selectedRow = summary.Select(filterExp).FirstOrDefault();

                        selectedRow["ProductQty"] = int.Parse(selectedRow["ProductQty"].ToString()) - int.Parse(row["ProductQty"].ToString());

                        filterExp = String.Format("{0} = '{1}'", type == "C" ? "CartonBarcode" : "ProductBarcode", barcode);
                        var movedRows = details.Select(filterExp);

                        foreach (DataRow movedRow in movedRows)
                        {
                            details.Rows.Remove(movedRow);
                        }
                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    summary = backup;

                    tran.Rollback();
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public DataTable ExportNoteDetails
        {
            get { return _dtExportNoteDetails; }
            set
            {
                _dtExportNoteDetails = value;
                _doImportCode = _dtExportNoteDetails.Rows[0]["DOImportCode"].ToString();
                _referenceNbr = _dtExportNoteDetails.Rows[0]["ExportCode"].ToString();
            }
        }

        public DataTable DetailsOnPallet
        {
            get { return _dtDetailsOnPallet; }
        }
    }
}
