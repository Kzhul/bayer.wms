﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Bayer.WMS.Handheld.Process.SubProcess;
using Bayer.WMS.Handheld.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessDelivery : Process
    {
        private readonly ProcessPallet _processPallet;

        private Pallet _pallet;
        private DeliveryNote _delivery;
        private DataTable _dtDeliveredDetails;
        private DataTable _dtTemp;

        public ProcessDelivery()
        {
            _processPallet = new ProcessPallet();
        }

        private void ValidateExistOnDocument(Pallet pallet)
        {
            if (pallet.CompanyCode != _delivery.CompanyCode)
                throw new Exception("Pallet này không được chia cho khách hàng hiện tại, vui lòng kiểm tra lại dữ liệu.");
        }

        public bool IsComplete()
        {
            if (_delivery.TotalPallet == _delivery.DeliveredPallet && _delivery.TotalBags == _delivery.DeliveredBags
                && _delivery.TotalBoxes == _delivery.DeliveredBoxes && _delivery.TotalBuckets == _delivery.DeliveredBuckets
                && _delivery.TotalCans == _delivery.DeliveredCans)
                return true;

            return false;
        }

        public void Process(string barcode)
        {
            _pallet = Pallet.Load(barcode);

            ValidateExistOnDocument(_pallet);

            _dtPalletStatusesSummary = Pallet.LoadPalletStatusGroupByPackingType(barcode, Utility.PalletStatusPrepared);

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("Process"), String.Format("{0} - {1}", _referenceNbr, barcode));
        }

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdateDeliveredQty(string truckNo, string palletCode)
        {
            string method = this.GetType().GetMethod("UpdateDeliveredQty").ReflectedType.FullName;

            Utility.DeliveryPalletStatus(_delivery.DeliveryTicketCode, _delivery.CompanyCode, _delivery.CompanyName, truckNo,
                _delivery.DeliveryDate, palletCode, Utility.UserID, Utility.SitemapID, method);
        }

        public DeliveryNote Delivery
        {
            get { return _delivery; }
            set { _delivery = value; }
        }

        public DataTable DeliveredDetails
        {
            get { return _dtDeliveredDetails; }
            set { _dtDeliveredDetails = value; }
        }

        public DataTable Temp
        {
            get { return _dtTemp; }
        }
    }
}
