﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Bayer.WMS.Handheld.Process.SubProcess;
using Bayer.WMS.Handheld.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Process.MainProcess
{
    public class ProcessReceiveReturn
    {
        private readonly ProcessPallet _processPallet;

        private string _doImportCode;
        private string _exportReturnCode;
        private DataTable _dtExportReturnNoteDetails;
        private DataTable _dtPalletStatuses;
        private DataTable _dtTemp;

        public ProcessReceiveReturn()
        {
            _processPallet = new ProcessPallet();
        }

        private void ValidateExistOnDocument(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (_dtExportReturnNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", row["ProductID"], row["ProductLot"])).Length == 0)
                    throw new Exception("Số lô không đúng với phiếu xuất hàng, vui lòng kiểm tra lại dữ liệu.");
            }
        }

        public bool IsComplete()
        {
            return _dtExportReturnNoteDetails.Select("ReceivedReturnQty <> ExportedReturnQty").Length == 0;
        }

        public void Process(string barcode)
        {
            _dtPalletStatuses = Pallet.LoadRequirePalletStatusGroupByBarcode(barcode, Utility.PalletStatusExportedReturn);
            _dtTemp = _dtPalletStatuses.Copy();

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("Process"), barcode);

            ValidateExistOnDocument(_dtPalletStatuses);
        }

        public void ProcessCfm(string barcode, out string type, out string packingType, out int oddQty, out bool isComplete)
        {
            barcode = Barcode.Process(barcode, false, true, true, out type);

            var row = _dtTemp.Select(String.Format("Barcode = '{0}'", barcode)).FirstOrDefault();
            if (row == null)
                throw new Exception("Không tìm thấy mã trên pallet đang nhận.");

            packingType = row["PackingType"].ToString();
            oddQty = int.Parse(row["OddQuantity"].ToString());

            _dtTemp.Rows.Remove(row);

            isComplete = _dtTemp.Rows.Count == 0;

            Utility.RecordAuditTrail(null, AuditTrailAction.ScanPallet, this.GetType().GetMethod("ProcessCfm"), barcode);
        }

        /// <summary>
        /// Save exported qty to database.
        /// Update pallet status to export.
        /// Move carton or product to another pallet.
        /// </summary>
        public void UpdateReceivedReturnQty()
        {
            var method = this.GetType().GetMethod("UpdateReceivedQty");

            //// save to database
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                var tran = conn.BeginTransaction();
                try
                {
                    var productList = new List<Product>();

                    foreach (DataRow row in _dtPalletStatuses.Rows)
                    {
                        int productID = int.Parse(row["ProductID"].ToString());
                        string productLot = row["ProductLot"].ToString();
                        decimal quantity = decimal.Parse(row["Quantity"].ToString());

                        var product = productList.FirstOrDefault(p => p.ProductID == productID && p.ProductLot == productLot);
                        if (product == null)
                            productList.Add(new Product { ProductID = productID, ProductLot = productLot, Quantity = quantity });
                        else
                            product.Quantity += quantity;
                    }

                    foreach (var product in productList)
                    {
                        //// update on screen
                        var selectedRow = _dtExportReturnNoteDetails.Select(String.Format("ProductID = {0} AND BatchCode = '{1}'", product.ProductID, product.ProductLot)).FirstOrDefault();

                        selectedRow["ReceivedReturnQty"] = decimal.Parse(selectedRow["ReceivedReturnQty"].ToString()) + product.Quantity;

                        //// update confirm qty of delivery note
                        using (var cmd = new SqlCommand("proc_ReturnDOHeaders_Update_ReceiveReturnQty", conn, tran))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add(new SqlParameter("@ExportCode", selectedRow["ExportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@DOImportCode", selectedRow["DOImportCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ProductID", selectedRow["ProductID"]));
                            cmd.Parameters.Add(new SqlParameter("@BatchCode", selectedRow["BatchCode"]));
                            cmd.Parameters.Add(new SqlParameter("@ReceivedReturnQty", selectedRow["ReceivedReturnQty"]));
                            cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                            cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                            cmd.Parameters.Add(new SqlParameter("@Method", method.ReflectedType.FullName));

                            cmd.ExecuteNonQuery();
                        }
                    }

                    //// update status of pallet to prevent duplicate process
                    string palletCode = _dtPalletStatuses.Rows[0]["PalletCode"].ToString();
                    Pallet.UpdatePallet_Status(palletCode, _doImportCode, _exportReturnCode, null, Utility.PalletStatusNew, method.ReflectedType.FullName, conn, tran);

                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public string DOImportCode
        {
            get { return _doImportCode; }
        }

        public string ExportReturnCode
        {
            get { return _exportReturnCode; }
        }

        public DataTable ExportReturnNoteDetails
        {
            get { return _dtExportReturnNoteDetails; }
            set
            {
                _dtExportReturnNoteDetails = value;
                _doImportCode = _dtExportReturnNoteDetails.Rows[0]["DOImportCode"].ToString();
                _exportReturnCode = _dtExportReturnNoteDetails.Rows[0]["ReturnDOCode"].ToString();
            }
        }

        public DataTable PalletStatuses
        {
            set { _dtPalletStatuses = value; }
        }

        public DataTable Temp
        {
            get { return _dtTemp; }
        }
    }
}
