﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Bayer.WMS.Handheld.Models
{
    public class Product
    {
        public int ProductID { get; set; }

        public string ProductCode { get; set; }

        public string ProductDescription { get; set; }

        public string ProductLot { get; set; }

        public decimal Quantity { get; set; }

        public int CategoryID { get; set; }

        public static Product Load(string productCode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Products_Select", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ProductCode", productCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã pallet không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return new Product
            {
                ProductID = int.Parse(dt.Rows[0]["ProductID"].ToString()),
                ProductCode = dt.Rows[0]["ProductCode"].ToString(),
                ProductDescription = dt.Rows[0]["Description"].ToString(),
                CategoryID = int.Parse(dt.Rows[0]["CategoryID"].ToString())
            };
        }
    }
}
