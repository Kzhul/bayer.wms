﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Models
{
    public class ProductionPlan
    {
        public string ProductLot { get; set; }

        public int ProductID { get; set; }

        public string ProductCode { get; set; }

        public string ProductDescription { get; set; }

        public decimal PackageSize { get; set; }

        public string UOM { get; set; }

        public decimal Quantity { get; set; }

        public DateTime MFD { get; set; }

        public DateTime EXP { get; set; }

        public static ProductionPlan LoadProductionPlan(string productLot)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_ProductionPlans_SelectByProductLot", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ProductLot", productLot));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã lô không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return new ProductionPlan
            {
                ProductLot = dt.Rows[0]["ProductLot"].ToString(),
                ProductID = int.Parse(dt.Rows[0]["ProductID"].ToString()),
                ProductCode = dt.Rows[0]["ProductCode"].ToString(),
                ProductDescription = dt.Rows[0]["ProductDescription"].ToString(),
                MFD = DateTime.Parse(dt.Rows[0]["ManufacturingDate"].ToString()),
                EXP = DateTime.Parse(dt.Rows[0]["ExpiryDate"].ToString()),
                PackageSize = decimal.Parse(dt.Rows[0]["PackageSize"].ToString()),
                UOM = dt.Rows[0]["UOM"].ToString()
            };
        }
    }
}
