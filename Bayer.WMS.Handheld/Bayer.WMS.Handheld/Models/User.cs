﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Models
{
    public class User
    {
        public int UserID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public static User LoadUser(string username)
        {
            var dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Users_Select", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Username", username));
                        cmd.Parameters.Add(new SqlParameter("@Status", "A"));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Không tìm thấy mã nhân viên, vui lòng kiểm tra lại.");

            return new User
            {
                UserID = int.Parse(dt.Rows[0]["UserID"].ToString()),
                FirstName = dt.Rows[0]["FirstName"].ToString(),
                LastName = dt.Rows[0]["LastName"].ToString()
            };
        }
    }
}
