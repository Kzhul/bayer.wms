﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Bayer.WMS.Handheld.Models
{
    public class AuditTrailAction
    {
        public const string Insert = "INS";
        public const string Update = "UPD";
        public const string Delete = "DEL";
        public const string ScanEmployee = "SEM";
        public const string ScanProductLot = "SPL";
        public const string ScanCustomer = "SCM";
        public const string ScanPallet = "SPT";
        public const string ScanCarton = "SCT";
        public const string ScanProduct = "SPD";
        public const string ScanDelete = "SDEL";
        public const string ScanDeleteBegin = "SDELB";
        public const string ScanDeleteEnd = "SDELE";
        public const string ScanEnd = "SED";
        public const string ScanDetroyLabel = "SDL";
        public const string ScanDetroyEnd = "SDE";
        public const string ScanBarcodeInvalid = "SBI";
        public const string ScanExportNote = "SEN";
        public const string ScanSplitNote = "SSN";
        public const string ScanPrepareNote = "SPN";
        public const string ScanDeliveryNote = "SDN";
        public const string ScanTruckNo = "STN";
        public const string ScanFinishPackaging = "SFP";
        public const string ScanDeletePackaging = "SDP";
        public const string ScanEndDeletePackaging = "SEDP";
        public const string PrintCartonLabel = "PCL";
        public const string PrintPalletLabel = "PPL";
    }
}
