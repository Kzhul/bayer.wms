﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Models
{
    public class DeliveryNote
    {
        public string DeliveryTicketCode { get; set; }

        public DateTime DeliveryDate { get; set; }

        public string CompanyCode { get; set; }

        public string CompanyName { get; set; }

        public string TruckNo { get; set; }

        public string DOImportCode { get; set; }

        public decimal TotalPallet { get; set; }

        public decimal DeliveredPallet { get; set; }

        public decimal TotalBoxes { get; set; }

        public decimal DeliveredBoxes { get; set; }

        public decimal TotalBags { get; set; }

        public decimal DeliveredBags { get; set; }

        public decimal TotalBuckets { get; set; }

        public decimal DeliveredBuckets { get; set; }

        public decimal TotalCans { get; set; }

        public decimal DeliveredCans { get; set; }

        public static DeliveryNote LoadDeliveryNote(string deliveryCode)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryTicket_Select", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DeliveryTicketCode", deliveryCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Phiếu giao hàng không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return new DeliveryNote
                {
                    DeliveryTicketCode = dt.Rows[0]["DeliveryTicketCode"].ToString(),
                    DeliveryDate = DateTime.Parse(dt.Rows[0]["DeliveryDate"].ToString()),
                    CompanyCode = dt.Rows[0]["CompanyCode"].ToString(),
                    CompanyName = dt.Rows[0]["CompanyName"].ToString(),
                    TruckNo = dt.Rows[0]["TruckNo"].ToString(),
                    DOImportCode = dt.Rows[0]["DOImportCode"].ToString(),
                    TotalBoxes = decimal.Parse(dt.Rows[0]["TotalBoxes"].ToString()),
                    TotalBags = decimal.Parse(dt.Rows[0]["TotalBags"].ToString()),
                    TotalBuckets = decimal.Parse(dt.Rows[0]["TotalBuckets"].ToString()),
                    TotalCans = decimal.Parse(dt.Rows[0]["TotalCans"].ToString()),
                };
        }

        public static DataTable LoadDeliveryNote_Delivered(string deliveryCode)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryTicket_Select_Delivered", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DeliveryTicketCode", deliveryCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            return dt;
        }
    }
}
