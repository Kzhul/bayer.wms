﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Models
{
    public class Pallet
    {
        public string PalletCode { get; set; }

        public string DOImportCode { get; set; }

        public string ReferenceNbr { get; set; }

        public string CompanyCode { get; set; }

        public string Status { get; set; }

        public int ProductQty { get; set; }

        public static void Clear(string palletCode, string productLot)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Clear", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@ProductLot", productLot));

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static void Merge(string palletCode1, string palletCode2)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Merge", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode1", palletCode1));
                        cmd.Parameters.Add(new SqlParameter("@PalletCode2", palletCode2));

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static Pallet Load(string palletCode)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Select", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã pallet không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return new Pallet
            {
                PalletCode = dt.Rows[0]["PalletCode"].ToString(),
                DOImportCode = dt.Rows[0]["DOImportCode"] == null ? null : dt.Rows[0]["DOImportCode"].ToString(),
                ReferenceNbr = dt.Rows[0]["ReferenceNbr"] == null ? null : dt.Rows[0]["ReferenceNbr"].ToString(),
                CompanyCode = dt.Rows[0]["CompanyCode"] == null ? null : dt.Rows[0]["CompanyCode"].ToString(),
                Status = dt.Rows[0]["Status"].ToString()
            };
        }

        public static DataTable LoadPalletStatuses(string barcode, string type)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select_Details", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadPalletStatuses(string referenceNbr)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select_ByRefereneNbr", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadOptionPalletStatus(string palletCode, params string[] statuses)
        {
            DataTable dt = new DataTable();
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_Pallets_Select_With_PalletStatuses", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã pallet không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            if (statuses.Length > 0)
            {
                foreach (string stt in statuses)
                {
                    if (dt.Select(String.Format("Status = '{0}'", stt)).Length > 0)
                        return dt;
                }

                throw new Exception("Trạng thái pallet không hợp lệ, pallet này đã được sử dụng vào giai đoạn khác, vui lòng kiểm tra lại dữ liệu.");
            }

            return dt;
        }

        public static DataTable LoadRequirePalletStatus(string barcode, string type, params string[] statuses)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            if (statuses.Length > 0)
            {
                foreach (string stt in statuses)
                {
                    if (dt.Select(String.Format("Status = '{0}'", stt)).Length > 0)
                        return dt;
                }

                throw new Exception("Trạng thái pallet không hợp lệ, pallet này đã được sử dụng vào giai đoạn khác, vui lòng kiểm tra lại dữ liệu.");
            }

            return dt;
        }

        public static DataTable LoadRequirePalletStatusGroupByBarcode(string palletCode, params string[] statuses)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select_GroupByBarcode", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã QR không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            if (statuses.Length > 0)
            {
                foreach (string stt in statuses)
                {
                    if (dt.Select(String.Format("Status = '{0}'", stt)).Length > 0)
                        return dt;
                }

                throw new Exception("Trạng thái pallet không hợp lệ, pallet này đã được sử dụng vào giai đoạn khác, vui lòng kiểm tra lại dữ liệu.");
            }

            return dt;
        }

        public static DataTable LoadPalletStatusGroupByPackingType(string palletCode, params string[] statuses)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Select_GroupByPackingType", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Mã QR không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            if (statuses.Length > 0)
            {
                foreach (string stt in statuses)
                {
                    if (dt.Select(String.Format("Status = '{0}'", stt)).Length > 0)
                        return dt;
                }

                throw new Exception("Trạng thái pallet không hợp lệ, pallet này đã được sử dụng vào giai đoạn khác, vui lòng kiểm tra lại dữ liệu.");
            }

            return dt;
        }

        public static void UpdatePallet_MovePromotion(string palletCode, int productID, string productCode, string referenceNbr, string companyCode, int qty, string method, SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MovePromotion", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                cmd.Parameters.Add(new SqlParameter("@ProductID", productID));
                cmd.Parameters.Add(new SqlParameter("@ProductCode", productCode));
                cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));
                cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                cmd.Parameters.Add(new SqlParameter("@Qty", qty));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Move product from another pallet to this pallet
        /// </summary>
        /// <param name="palletCode">destination pallet</param>
        public static void UpdatePallet_MoveProduct(string palletCode, string barcode, string type, string method, SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MoveProduct", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                cmd.Parameters.Add(new SqlParameter("@Type", type));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Move product from another pallet to this pallet
        /// </summary>
        /// <param name="palletCode">destination pallet</param>
        public static void UpdatePallet_MoveProduct(string palletCode, string barcode, string type, string method)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Update_MoveProduct", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static void UpdatePallet_Status(string palletCode, string doImportCode, string referenceNbr, string companyCode, string status, string method,
            SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_Pallets_Update_Status", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                cmd.Parameters.Add(new SqlParameter("@ReferenceNbr", referenceNbr));
                cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                cmd.Parameters.Add(new SqlParameter("@Status", status));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdatePalletStatus(string barcode, string type, string newPalletCode, string method,
            SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_PalletStatuses_Update", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                cmd.Parameters.Add(new SqlParameter("@Type", type));
                cmd.Parameters.Add(new SqlParameter("@NewPalletCode", newPalletCode));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }
    }
}
