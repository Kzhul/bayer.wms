﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Models
{
    public class Barcode
    {
        public static string Process(string barcode, bool isAcceptPallet, bool isAcceptCarton, bool isAcceptProduct, out string type)
        {
            type = String.Empty;
            if (isAcceptPallet && Utility.PalletBarcodeRegex.IsMatch(barcode))
                type = "P";
            else if (isAcceptCarton
                && (Utility.CartonBarcodeRegex.IsMatch(barcode) 
                    || Utility.CartonBarcode2Regex.IsMatch(barcode)
                    || Utility.CartonBarcodeTempRegex.IsMatch(barcode)))
                type = "C";
            else
            {
                try
                {
                    //barcode = Utility.ProcessProductBarcode(barcode.Split('|')[0].Remove(0, 2));
                    if (isAcceptProduct && Utility.ProductCartonRegex.IsMatch(barcode))
                        type = "E";
                    else
                        throw new Exception();
                }
                catch
                {
                    throw new Exception("Mã không hợp lệ, vui lòng kiểm tra lại dữ liệu.");
                }
            }

            return barcode;
        }

        public static DataTable GetBarcodeTracking(string barcode, string type)
        {
            var dt = new DataTable();

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_Barcode_Tracking", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable GetBarcodeTrackingInfo(string barcode, string type)
        {
            var dt = new DataTable();

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_Barcode_TrackingInfo", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }
    }
}
