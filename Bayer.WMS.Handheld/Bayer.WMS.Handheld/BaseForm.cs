﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Handheld.Models;

namespace Bayer.WMS.Handheld
{
    public partial class BaseForm : Form
    {
        protected BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        protected DataTable _dt;

        public BaseForm()
        {
            InitializeComponent();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            _scannedValue = new List<string>();
        }

        public BaseForm(int sitemapID)
        {
            InitializeComponent();

            Utility.SitemapID = sitemapID;

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            _scannedValue = new List<string>();
        }

        public void BaseForm_Closing(object sender, CancelEventArgs e)
        {
            DisposeBarcodeReader();
        }

        protected virtual void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
        }

        public virtual void DisposeBarcodeReader()
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public virtual void ReInitBarcodeReader()
        {
            _barcodeReader = Utility.ReInitBarcodeReader();
        }

        public virtual bool IsComplete()
        {
            return _dt.Rows.Count == _dt.Select("IsComplete = 1").Length;
        }
    }
}