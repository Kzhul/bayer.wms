﻿namespace Bayer.WMS.Handheld
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.mniExport = new System.Windows.Forms.MenuItem();
            this.mniReceiveReturn = new System.Windows.Forms.MenuItem();
            this.mniReceiveReturnWithoutDocument = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mniReceive = new System.Windows.Forms.MenuItem();
            this.mniSplit = new System.Windows.Forms.MenuItem();
            this.mniPrepare = new System.Windows.Forms.MenuItem();
            this.mniDelivery = new System.Windows.Forms.MenuItem();
            this.mniExportReturn = new System.Windows.Forms.MenuItem();
            this.mnExportReturnWithoutDocument = new System.Windows.Forms.MenuItem();
            this.mniBarcodeTracking = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mniJoinCarton = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.menuItem2);
            this.menuItem1.MenuItems.Add(this.menuItem3);
            this.menuItem1.MenuItems.Add(this.mniBarcodeTracking);
            this.menuItem1.Text = "Menu";
            // 
            // menuItem2
            // 
            this.menuItem2.MenuItems.Add(this.mniExport);
            this.menuItem2.MenuItems.Add(this.mniReceiveReturn);
            this.menuItem2.MenuItems.Add(this.mniReceiveReturnWithoutDocument);
            this.menuItem2.MenuItems.Add(this.mniJoinCarton);
            this.menuItem2.Text = "Bayer";
            // 
            // mniExport
            // 
            this.mniExport.Text = "Xuất hàng";
            this.mniExport.Click += new System.EventHandler(this.mniExport_Click);
            // 
            // mniReceiveReturn
            // 
            this.mniReceiveReturn.Text = "Nhận hàng trả";
            this.mniReceiveReturn.Click += new System.EventHandler(this.mniReceiveReturn_Click);
            // 
            // mniReceiveReturnWithoutDocument
            // 
            this.mniReceiveReturnWithoutDocument.Text = "Nhận hàng trả không phiếu";
            this.mniReceiveReturnWithoutDocument.Click += new System.EventHandler(this.mniReceiveReturnWithoutDocument_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.MenuItems.Add(this.mniReceive);
            this.menuItem3.MenuItems.Add(this.mniSplit);
            this.menuItem3.MenuItems.Add(this.mniPrepare);
            this.menuItem3.MenuItems.Add(this.mniDelivery);
            this.menuItem3.MenuItems.Add(this.mniExportReturn);
            this.menuItem3.MenuItems.Add(this.mnExportReturnWithoutDocument);
            this.menuItem3.Text = "GRSC";
            // 
            // mniReceive
            // 
            this.mniReceive.Text = "Nhận hàng";
            this.mniReceive.Click += new System.EventHandler(this.mniReceive_Click);
            // 
            // mniSplit
            // 
            this.mniSplit.Text = "Chia hàng";
            this.mniSplit.Click += new System.EventHandler(this.mniSplit_Click);
            // 
            // mniPrepare
            // 
            this.mniPrepare.Text = "Soạn hàng";
            this.mniPrepare.Click += new System.EventHandler(this.mniPrepare_Click);
            // 
            // mniDelivery
            // 
            this.mniDelivery.Text = "Giao hàng";
            this.mniDelivery.Click += new System.EventHandler(this.mniDelivery_Click);
            // 
            // mniExportReturn
            // 
            this.mniExportReturn.Text = "Trả hàng";
            this.mniExportReturn.Click += new System.EventHandler(this.mniExportReturn_Click);
            // 
            // mnExportReturnWithoutDocument
            // 
            this.mnExportReturnWithoutDocument.Text = "Trả hàng không phiếu";
            this.mnExportReturnWithoutDocument.Click += new System.EventHandler(this.mnExportReturnWithoutDocument_Click);
            // 
            // mniBarcodeTracking
            // 
            this.mniBarcodeTracking.Text = "Tuy vết mã QR";
            this.mniBarcodeTracking.Click += new System.EventHandler(this.mniBarcodeTracking_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniExit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(60, 60);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 120);
            // 
            // mniJoinCarton
            // 
            this.mniJoinCarton.Text = "Dồn thùng lẻ";
            this.mniJoinCarton.Click += new System.EventHandler(this.mniJoinCarton_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.pictureBox1);
            this.Menu = this.mainMenu1;
            this.Name = "MainView";
            this.Text = "Bayer";
            this.Load += new System.EventHandler(this.MainView_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem mniExport;
        private System.Windows.Forms.MenuItem mniReceiveReturn;
        private System.Windows.Forms.MenuItem mniReceive;
        private System.Windows.Forms.MenuItem mniSplit;
        private System.Windows.Forms.MenuItem mniPrepare;
        private System.Windows.Forms.MenuItem mniExportReturn;
        private System.Windows.Forms.MenuItem mniDelivery;
        private System.Windows.Forms.MenuItem mniBarcodeTracking;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuItem mnExportReturnWithoutDocument;
        private System.Windows.Forms.MenuItem mniReceiveReturnWithoutDocument;
        private System.Windows.Forms.MenuItem mniJoinCarton;
    }
}