﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;
using System.Media;
using Bayer.WMS.Handheld.Models;
using Intermec.DataCollection;
using System.IO.Ports;
using System.Reflection;
using System.Threading;

namespace Bayer.WMS.Handheld
{
    public static class Utility
    {
        public const string PalletStatusNew = "N";
        public const string PalletStatusExported = "E";
        public const string PalletStatusReceived = "R";
        public const string PalletStatusSplitted = "S";
        public const string PalletStatusPrepared = "P";
        public const string PalletStatusExportedReturn = "X";

        public const int ExportView = -1;
        public const int ReceiveView = -2;
        public const int SplitView = -3;
        public const int PrepareView = -4;
        public const int DeliveryView = -5;
        public const int ExportReturnView = -6;
        public const int ReceiveReturnView = -7;

        public static string ConnStr;
        public static string AppPath;
        public static string CartonLabelTemplate;
        public static string CartonLabelForMultiProductTemplate;
        public static string PalletLabelTemplate;
        public static string COM_PortName;
        public static int COM_BaudRate;
        public static string COM_Parity;
        public static int COM_DataBits;
        public static string COM_StopBits;
        public static int SitemapID;
        public static int UserID;
        public static byte[] bytes = Encoding.ASCII.GetBytes("BayerWMS");
        public static Regex ExportRegex = new Regex(@"^XH\d{8}$", RegexOptions.IgnoreCase);
        public static Regex ExportReturnRegex = new Regex(@"^RD\d{8}$", RegexOptions.IgnoreCase);
        public static Regex SplitRegex = new Regex(@"^CH\d{8}$", RegexOptions.IgnoreCase);
        public static Regex PrepareNoteRegex = new Regex(@"^SH\d{8}$", RegexOptions.IgnoreCase);
        public static Regex DeliveryRegex = new Regex(@"^GH\d{8}$", RegexOptions.IgnoreCase);
        public static Regex PalletBarcodeRegex = new Regex(@"^P[A-Z][01]\d\d{2}\d{5}$", RegexOptions.IgnoreCase);
        public static Regex CartonBarcodeRegex = new Regex(@"^\d{6}\w\d{2}C\d{3}$", RegexOptions.IgnoreCase);
        public static Regex CartonBarcode2Regex = new Regex(@"^SH\d{8}C\d{3}$", RegexOptions.IgnoreCase);
        public static Regex CartonBarcodeTempRegex = new Regex(@"^DO\d{8}C\d{3}$", RegexOptions.IgnoreCase);
        public static Regex ProductCartonRegex = new Regex(@"^\d{6}\w\d{2}\d{5}", RegexOptions.IgnoreCase);
        public static Regex TruckNoRegex = new Regex(@"^SX_.*$", RegexOptions.IgnoreCase);
        public static Regex EmployeeRegex = new Regex(@"^NV_", RegexOptions.IgnoreCase);
        public static Regex CustomerRegex = new Regex(@"^KH_", RegexOptions.IgnoreCase);
        public static Regex ProductRegex = new Regex(@"^SP_", RegexOptions.IgnoreCase);
        public static Regex ProductLotRegex = new Regex(@"^\d{6}\w\d{2}", RegexOptions.IgnoreCase);

        public static void PlayErrorSound()
        {
            string path = Path.Combine(AppPath, @"Resources\Error.wav");
            var player = new SoundPlayer(path);
            player.Play();
            System.Threading.Thread.Sleep(2000);
            player.Stop();
        }

        public static void RecordAuditTrail(string data, string action, MethodInfo method, string description)
        {
            Thread thread = new Thread(() =>
                {
                    try
                    {
                        using (var conn = new SqlConnection(Utility.ConnStr))
                        {
                            conn.Open();

                            try
                            {
                                using (var cmd = new SqlCommand("proc_AuditTrails_Insert", conn))
                                {
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = Utility.UserID });
                                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@SitemapID", SqlDbType = SqlDbType.Int, Value = Utility.SitemapID });
                                    //cmd.Parameters.Add(new SqlParameter { ParameterName = "@Data", SqlDbType = SqlDbType.Xml, Value = data });
                                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@Action", SqlDbType = SqlDbType.VarChar, Value = action });
                                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@Method", SqlDbType = SqlDbType.VarChar, Value = method.ReflectedType.FullName });
                                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@Date", SqlDbType = SqlDbType.DateTime, Value = DateTime.Today });
                                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@DateTime", SqlDbType = SqlDbType.DateTime, Value = DateTime.Now });
                                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@Description", SqlDbType = SqlDbType.NVarChar, Value = description });
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            finally
                            {
                                conn.Close();
                            }
                        }
                    }
                    catch
                    {
                    }
                });

            thread.Start();
        }

        public static ColumnStyle AddColumn(string caption, string mappingName, int width, string format, CheckCellEventHandler eventHandler)
        {
            var column = new ColumnStyle(1) { Width = 60, HeaderText = "SL y.cầu", MappingName = "RemainQty", Format = "N0" };
            column.HeaderText = caption;
            column.MappingName = mappingName;
            column.Width = width;

            if (!String.IsNullOrEmpty(format))
                column.Format = format;

            if (eventHandler != null)
                column.CheckCellEquals += eventHandler;

            return column;
        }

        public static DataTable LoadDeliveryOrder(string customerCode, DateTime deliveryDate)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryOrders_Select_ForCustomer", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", customerCode));
                        cmd.Parameters.Add(new SqlParameter("@DeliveryDate", deliveryDate));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Phiếu giao cho KH không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return dt;
        }

        public static DataTable LoadDeliverOrder_Executor(string doImportCode, string type)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryOrders_Select_Executor", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DOImportCode", doImportCode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            return dt;
        }

        public static DataTable LoadExportNote(string exportCode)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_DeliveryNote_Select", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ExportCode", exportCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Phiếu xuất hàng không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return dt;
        }

        public static DataTable LoadExportReturnNote(string exportReturnCode)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_ReturnDO_Select", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ReturnDOCode", exportReturnCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Phiếu trả hàng không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return dt;
        }

        public static DataTable LoadSplitNote(string splitCode)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_SplitNote_Select", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@SplitCode", splitCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Phiếu chia hàng không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return dt;
        }

        public static DataTable LoadPrepareNote(string prepareCode)
        {
            DataTable dt = new DataTable();
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();

                try
                {
                    using (var cmd = new SqlCommand("proc_PrepareNote_Select", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@PrepareNote", prepareCode));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }

            if (dt.Rows.Count == 0)
                throw new Exception("Phiếu soạn hàng không tồn tại, vui lòng kiểm tra lại dữ liệu.");

            return dt;
        }

        public static void UpdatePalletStatus_Packaging(string palletCode, string cartonBarcode, string productBarcodeList, string method, SqlConnection conn, SqlTransaction tran)
        {
            using (var cmd = new SqlCommand("proc_PalletStatuses_Update_WHPackaging", conn, tran))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                cmd.Parameters.Add(new SqlParameter("@CartonBarcode", cartonBarcode));
                cmd.Parameters.Add(new SqlParameter("@ProductBarcodeList", productBarcodeList));
                cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                cmd.Parameters.Add(new SqlParameter("@Method", method));

                cmd.ExecuteNonQuery();
            }
        }

        public static void DeliveryPalletStatus(string deliveryCode, string companyCode, string companyName, string truckNo, DateTime deliveryDate,
            string palletCode, int userID, int sitemapID, string method)
        {
            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_PalletStatuses_Update_Delivery", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@DeliveryTicketCode", deliveryCode));
                        cmd.Parameters.Add(new SqlParameter("@CompanyCode", companyCode));
                        cmd.Parameters.Add(new SqlParameter("@CompanyName", companyName));
                        cmd.Parameters.Add(new SqlParameter("@TruckNo", truckNo));
                        cmd.Parameters.Add(new SqlParameter("@ActualDeliveryDate", DateTime.Today));
                        cmd.Parameters.Add(new SqlParameter("@DeliveryDate", deliveryDate));
                        cmd.Parameters.Add(new SqlParameter("@PalletCode", palletCode));
                        cmd.Parameters.Add(new SqlParameter("@UserID", userID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", sitemapID));
                        cmd.Parameters.Add(new SqlParameter("@Method", method));

                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static int GetOddQty(string prepareCode)
        {
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_PrepareNote_Select_OddQty", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PrepareCode", prepareCode));
                        cmd.Parameters.Add(new SqlParameter("@OddQty", 0));
                        cmd.Parameters["@OddQty"].Direction = ParameterDirection.Output;
                        cmd.Parameters["@OddQty"].DbType = DbType.Int32;

                        cmd.ExecuteNonQuery();

                        return int.Parse(cmd.Parameters["@OddQty"].Value.ToString());
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }
        }

        public static int GenCartonWarehouseBarcode(string prepareCode, int qty, decimal weight)
        {
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_CartonWarehouseBarcodes_Gen", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@PrepareCode", prepareCode));
                        cmd.Parameters.Add(new SqlParameter("@Quantity", qty));
                        cmd.Parameters.Add(new SqlParameter("@Weight", weight));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", Utility.SitemapID));
                        cmd.Parameters.Add(new SqlParameter("@LastNumber", 0));
                        cmd.Parameters["@LastNumber"].Direction = ParameterDirection.Output;
                        cmd.Parameters["@LastNumber"].DbType = DbType.Int32;

                        cmd.ExecuteNonQuery();

                        return int.Parse(cmd.Parameters["@LastNumber"].Value.ToString());
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }
        }

        public static string AESEncrypt(string planString)
        {
            if (String.IsNullOrEmpty(planString))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);

            var writer = new StreamWriter(cryptoStream);
            writer.Write(planString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();

            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        public static string AESDecrypt(string cipherText)
        {
            if (String.IsNullOrEmpty(cipherText))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream(Convert.FromBase64String(cipherText));
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
            var reader = new StreamReader(cryptoStream);

            return reader.ReadToEnd();
            //return cipherText;
        }

        public static string MD5Encrypt(string planString)
        {
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] hashedBytes;
            UTF8Encoding encoder = new UTF8Encoding();
            hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(planString));

            string result = String.Empty;
            foreach (Byte b in hashedBytes)
                result += b.ToString("x2");
            return result;
        }

        public static string ProcessProductBarcode(string barcode)
        {
            barcode = Utility.AESDecrypt(barcode);
            return barcode;
        }

        public static BarcodeReader InitBarcodeReader(BarcodeReadEventHandler handler)
        {
            try
            {
                var barcodeReader = new BarcodeReader();
                barcodeReader.ThreadedRead(true);
                barcodeReader.BarcodeRead += handler;

                return barcodeReader;
            }
            catch (Exception ex)
            {
                //Write Log
            }
            return null;
        }

        public static BarcodeReader ReInitBarcodeReader()
        {
            try
            {
                var barcodeReader = new BarcodeReader();
                barcodeReader.ThreadedRead(true);

                return barcodeReader;
            }
            catch (Exception ex)
            {
                //Write Log
            }
            return null;
        }

        public static void WriteLogToDB(Exception ex)
        {
            if (ex == null)
                return;

            string message = ex.Message;
            string stackTrace = ex.StackTrace;
            string innerException_Message = string.Empty;
            string innerException_StackTrace = string.Empty;

            if (ex.InnerException != null)
            {
                innerException_Message = ex.InnerException.Message;
                innerException_StackTrace = ex.InnerException.StackTrace;
            }
            using (var sqlConn = new SqlConnection(Utility.ConnStr))
            {
                sqlConn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_WriteLog", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@Message", message));
                        cmd.Parameters.Add(new SqlParameter("@StackTrace", stackTrace));
                        cmd.Parameters.Add(new SqlParameter("@InnerException_Message", innerException_Message));
                        cmd.Parameters.Add(new SqlParameter("@InnerException_StackTrace", innerException_StackTrace));
                        cmd.ExecuteNonQuery();
                    }
                }
                finally
                {
                    sqlConn.Close();
                }
            }
        }
    }
}
