DECLARE @_DOImportCode NVARCHAR(20) = 'DO18053003'
DECLARE @_CompanyCode NVARCHAR(20) = 'DO18053003'

SELECT DISTINCT
	DH.DOImportCode
	, DH.CompanyCode
	, DH.BatchCode
	, DH.ProductID
	, PD.ProductCode	
	, PD.[UOM]
	, ProductDescription = PD.Description 
	, DH.DOQuantity
	, DH.PreparedQty
	, DH.[DeliveredQty]
	, CartonQty = ISNULL(tmp.CartonQty,0)
	, [PackageSize] = CONVERT(INT,ISNULL(PPS.Quantity,0))
	, OddQty = ISNULL(tmp.OddQty,0)
	, ProductQty = ISNULL(tmp.ProductQty,0)
FROM 
	DeliveryOrderSum DH WITH (NOLOCK) 
	LEFT JOIN (
		SELECT DISTINCT 
			DH.DOImportCode
			, DH.CompanyCode
			, DH.ProductID
			, DH.BatchCode
			, CartonQty = COUNT(DISTINCT CASE WHEN ps.CartonBarcode != '' THEN ps.CartonBarcode ELSE NULL END)
			, OddQty = SUM(CASE WHEN ps.CartonBarcode = '' THEN 1 ELSE 0 END)
			, ProductQty = SUM(ISNULL(ps.Qty, 1))
		FROM
			DeliveryOrderSum DH WITH (NOLOCK)
			JOIN Pallets P WITH (NOLOCK)
				ON DH.DOImportCode = P.DOImportCode
				AND DH.CompanyCode = P.CompanyCode
			JOIN dbo.PalletStatuses ps WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
				AND DH.ProductID = PS.ProductID
				AND DH.BatchCode = PS.ProductLot
		WHERE
			DH.DOImportCode = @_DOImportCode
		GROUP BY
			DH.DOImportCode
			, DH.CompanyCode
			, DH.ProductID
			, DH.BatchCode

		UNION

		SELECT DISTINCT 
			DH.DOImportCode
			, DH.CompanyCode
			, DH.ProductID
			, DH.BatchCode
			, CartonQty = COUNT(DISTINCT CASE WHEN ps.CartonBarcode != '' THEN ps.CartonBarcode ELSE NULL END)
			, OddQty = SUM(CASE WHEN ps.CartonBarcode = '' THEN 1 ELSE 0 END)
			, ProductQty = COUNT(DISTINCT PS.[ProductBarcode])
		FROM
			DeliveryOrderSum DH WITH (NOLOCK)
			JOIN dbo.DeliveryHistories ps WITH (NOLOCK)
				ON DH.DOImportCode = ps.DeliveryTicketCode
				AND DH.CompanyCode = PS.CompanyCode
				AND DH.ProductID = PS.ProductID
				AND DH.BatchCode = PS.ProductLot
		WHERE
			DH.DOImportCode = 'DO18080301'
		GROUP BY
			DH.DOImportCode
			, DH.CompanyCode
			, DH.ProductID
			, DH.BatchCode
	)tmp
		ON DH.DOImportCode = tmp.DOImportCode
		AND DH.CompanyCode = tmp.CompanyCode
		AND DH.ProductID = tmp.ProductID
		AND DH.BatchCode = tmp.BatchCode
	LEFT JOIN Products PD WITH (NOLOCK)
		ON DH.ProductID = PD.ProductID
	LEFT JOIN [dbo].[ProductPackings] PPS
		ON PD.ProductID = PPS.ProductID
		AND PPS.Type != 'P'
WHERE
	DH.DOImportCode = @_DOImportCode