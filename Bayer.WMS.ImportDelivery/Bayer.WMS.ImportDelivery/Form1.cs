﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.ImportDelivery
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            string fileName;
            using (var ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;

                fileName = ofd.FileName;
            }

            var list = new List<string>();
            using (var sr = new StreamReader(fileName))
            {
                while (!sr.EndOfStream)
                {
                    list.Add(sr.ReadLine());
                }

                sr.Close();
            }

            var thread = new Thread(() => Process(list));
            thread.Start();
        }

        private void Process(List<string> list)
        {
            Invoke((MethodInvoker)delegate
            {
                btnImport.Enabled = false;
                lblCurrent.Text = $"0 / {list.Count}";
                progressBar1.Step = 1;
                progressBar1.Maximum = list.Count;
            });

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["BayerWMSContext"].ConnectionString))
            {
                conn.Open();

                try
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        string[] tmp = list[i].ToUpper().Replace("-", "_").Replace("BAH_", "BAH-").Split('_');
                        using (var cmd = new SqlCommand("proc_DeliveryHistories_Insert_FromOffline", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter { ParameterName = "@Username", SqlDbType = SqlDbType.VarChar, Value = tmp[0] });
                            cmd.Parameters.Add(new SqlParameter { ParameterName = "@CompanyCode", SqlDbType = SqlDbType.VarChar, Value = tmp[1] });
                            cmd.Parameters.Add(new SqlParameter { ParameterName = "@Barcode", SqlDbType = SqlDbType.VarChar, Value = tmp[2] });
                            cmd.Parameters.Add(new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.VarChar, Value = tmp[3] });

                            if (tmp.Count() >= 6)
                            {
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = tmp[4] });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@DeliveryDate", SqlDbType = SqlDbType.DateTime, Value = DateTime.Parse(tmp[5]) });
                            }
                            else if (tmp.Count() == 4)
                            {
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = string.Empty });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@DeliveryDate", SqlDbType = SqlDbType.DateTime, Value = DateTime.Parse("2017-11-30") });
                            }
                            else if (tmp.Count() == 5)
                            {
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = string.Empty });
                                cmd.Parameters.Add(new SqlParameter { ParameterName = "@DeliveryDate", SqlDbType = SqlDbType.DateTime, Value = DateTime.Parse(tmp[4]) });
                            }
                            cmd.ExecuteNonQuery();
                        }

                        Invoke((MethodInvoker)delegate
                        {
                            lblCurrent.Text = $"{i + 1} / {list.Count}";
                            progressBar1.PerformStep();
                        });
                    }
                }
                finally
                {
                    conn.Close();
                }
            }

            Invoke((MethodInvoker)delegate
            {
                btnImport.Enabled = true;
                MessageBox.Show("Hoàn thành");
            });
        }
    }
}
