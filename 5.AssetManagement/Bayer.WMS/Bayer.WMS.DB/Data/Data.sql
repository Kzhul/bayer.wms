﻿DELETE FROM [dbo].[Sitemaps]
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (1, NULL, N'Thiết lập hệ thống', NULL, NULL, NULL, 0, N'System_icon_16', NULL, NULL, NULL, NULL, NULL, N'M')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (2, NULL, N'Kho', NULL, NULL, NULL, 2, N'Warehouse_icon_16', NULL, NULL, NULL, NULL, NULL, N'M')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (3, 1, N'Dữ liệu nền', NULL, NULL, NULL, 0, N'MDM_icon_16', NULL, NULL, NULL, NULL, NULL, N'M')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (4, 3, N'Người sử dụng', N'UserMaintView', N'IUserMaintPresenter', N'Bayer.WMS.Cof', 0, N'User_icon_16', 1, 1, 1, 0, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (5, 3, N'Vai trò', N'RoleMaintView', N'IRoleMaintPresenter', N'Bayer.WMS.Cof', 1, N'User_icon_16', 1, 1, 1, 0, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (6, NULL, N'Sản xuất', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, N'M')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (7, 6, N'Đóng gói', N'PackagingView', N'IPackagingPresenter', N'Bayer.WMS.Prd', 2, N'Packaging_icon_16', NULL, NULL, NULL, NULL, NULL, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (8, 3, N'Sản phẩm', N'ProductMaintView', N'IProductMaintPresenter', N'Bayer.WMS.Inv', 3, NULL, 1, 1, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (9, 3, N'Quy cách đóng gói', N'ProductPackingMaintView', N'IProductPackingMaintPresenter', N'Bayer.WMS.Inv', 4, NULL, 1, 0, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (10, 6, N'Kế hoạch sản xuất', N'ProductionPlanView', N'IProductionPlanPresenter', N'Bayer.WMS.Prd', 1, NULL, 1, 0, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (11, 1, N'Thiết lập', N'ConfigView', N'IConfigPresenter', N'Bayer.WMS.Cof', 1, N'System_icon_16', 1, 0, 1, 0, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (12, 2, N'Import DO', N'DeliveryOrderView', N'IDeliveryOrderPresenter', N'Bayer.WMS.Inv', 4, N'Warehouse_icon_16', 1, 1, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (13, 2, N'Phiếu xuất hàng', N'DeliveryNoteView', N'IDeliveryNotePresenter', N'Bayer.WMS.Inv', 5, N'Warehouse_icon_16', 1, 0, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (14, 2, N'Phiếu chia hàng', N'SplitNoteView', N'ISplitNotePresenter', N'Bayer.WMS.Inv', 6, N'Warehouse_icon_16', 1, 0, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (15, 6, N'Thiết bị/phòng đóng gói', N'PackagingDeviceMaintView', N'IDeviceMaintPresenter', N'Bayer.WMS.Prd', 0, NULL, 1, 1, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (16, 2, N'Pallet', N'PalletMaintView', N'IPalletMaintPresenter', N'Bayer.WMS.Inv', 2, NULL, 1, 0, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (17, 2, N'Phiếu soạn hàng', N'PrepareNoteView', N'IPrepareNotePresenter', N'Bayer.WMS.Inv', 7, N'Warehouse_icon_16', 1, 0, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (18, 2, N'Phiếu giao hàng', N'DeliveryTicketView', N'IDeliveryTicketPresenter', N'Bayer.WMS.Inv', 8, N'Warehouse_icon_16', 1, 1, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (19, 10, N'Import KHSX', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (20, 10, N'Tạo mã QR', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (21, 12, N'ImportDO', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (22, 12, N'Tạo phiếu xuất hàng', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (23, 12, N'Tạo phiếu chia hàng', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (24, 12, N'Tạo phiếu soạn hàng', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (25, 2, N'Báo cáo DO', N'ReportDOView', N'IReportDOPresenter', N'Bayer.WMS.Inv', 11, N'Warehouse_icon_16', 1, 0, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (26, 10, N'Hold KHSX', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (27, 10, N'Export mã QR', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (28, NULL, N'Báo cáo', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, N'M')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (29, 28, N'Audit Trail', N'AuditTrailView', N'IAuditTrailPresenter', N'Bayer.WMS.Cof', 0, NULL, 0, 0, 0, 0, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (30, 2, N'Báo cáo chi tiết giao hàng', N'ReportDeliveryBarcodeHistoryView', N'IReportDeliveryBarcodeHistoryPresenter', N'Bayer.WMS.Inv', 10, N'Warehouse_icon_16', 1, 0, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (31, 28, N'Truy vết barcode', N'BarcodeTrackingView', N'IBarcodeTrackingPresenter', N'Bayer.WMS.Inv', 8, N'Warehouse_icon_16', 1, 0, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (32, 6, N'Kiểm tra đóng gói', N'PackagingReportView', N'IPackagingReportPresenter', N'Bayer.WMS.Prd', 3, NULL, 0, 0, 0, 0, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (33, 2, N'Phiếu trả hàng', N'ReturnDOView', N'IReturnDOPresenter', N'Bayer.WMS.Inv', 12, N'Warehouse_icon_16', 1, 1, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (34, 10, N'Đổi trạng thái', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (35, 2, N'Phiếu nhập hàng', N'StockReceivingView', N'IStockReceivingPresenter', N'Bayer.WMS.Inv', 10, N'Warehouse_icon_16', 1, 1, 1, 1, 1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (36, 6, N'Ngân hàng barcode', N'BarcodeBankMaintView', N'IBarcodeBankMaintPresenter', N'Bayer.WMS.Prd', 4, NULL, NULL, NULL, NULL, NULL, NULL, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (37, 2, N'Danh sách công ty', N'CompanyView', N'ICompanyPresenter', N'Bayer.WMS.Cof', 3, NULL, 1, 1, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (38, 2, N'Kho', N'WarehouseMaintView', N'IWarehouseMaintPresenter', N'Bayer.WMS.Inv', 0, NULL, 1, 1, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (39, 2, N'Khu vực', N'ZoneMaintView', N'IZoneMaintPresenter', N'Bayer.WMS.Inv', 1, NULL, 1, 1, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (40,	28,	N'Báo cáo chi tiết kho'	,'ReportLocationView'	,'IReportLocationPresenter',	'Bayer.WMS.Inv'	,7	,'Warehouse_icon_16'	,1	,0,	1,	1,	1, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (41, 2, N'Yêu cầu nguyên liệu', N'RequestMaterialView', N'IRequestMaterialPresenter', N'Bayer.WMS.Inv', 1, NULL, 1, 1, 1, 1, 0, N'V')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (42, 41, N'Tự động lấy hàng', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'F')
GO
INSERT [dbo].[Sitemaps] ([SitemapID], [ParentID], [Description], [ClassName], [InterfaceName], [AssemblyName], [Order], [Icon], [EnableRefresh], [EnableInsert], [EnableSave], [EnableDelete], [EnablePrint], [Type]) VALUES (43, 28, N'Danh sách pallet thành phẩm', N'PalletTrackingView', N'IPalletTrackingPresenter', N'Bayer.WMS.Inv', 8, N'Warehouse_icon_16', 1, 0, 1, 1, 0, N'V')
GO

DELETE FROM [dbo].[Users] WHERE UserID = 1
GO

SET IDENTITY_INSERT [dbo].[Users] ON 

GO
INSERT [dbo].[Users] ([UserID], [Username], [Password], [FirstName], [LastName], [Email], [SitemapID], [Status], [PasswordChangeOnNextLogin], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (1, N'Admin', 'c030437f6e8e94d244bc602606df5235', N'Admin', N'Admin', NULL, NULL, N'A', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO

DELETE FROM [dbo].[Categories] WHERE [CategoryID] IN (1, 2, 3, 4)
GO

SET IDENTITY_INSERT [dbo].[Categories] ON 

GO
INSERT [dbo].[Categories] ([CategoryID], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (1, N'Aqua', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Categories] ([CategoryID], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (2, N'Livestock', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Categories] ([CategoryID], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (3, N'CAP', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Categories] ([CategoryID], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (4, N'P&I', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO

DELETE FROM [dbo].[UOM] WHERE [UOM] IN ('BAO', 'CAN', 'CHAI', 'GOI', 'HOP', 'HU', 'XO')
GO

INSERT [dbo].[UOM] ([UOM], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (N'BAO', N'Bao', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UOM] ([UOM], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (N'CAN', N'Can', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UOM] ([UOM], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (N'CHAI', N'Chai', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UOM] ([UOM], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (N'GOI', N'Gói', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UOM] ([UOM], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (N'HOP', N'Hộp', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UOM] ([UOM], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (N'HU', N'Hũ', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[UOM] ([UOM], [Description], [IsDeleted], [CreatedBy], [CreatedBySitemapID], [CreatedDateTime], [UpdatedBy], [UpdatedBySitemapID], [UpdatedDateTime]) VALUES (N'XO', N'Xô', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO