﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnStrPackagingType]
(
	@PackagingType NVARCHAR(1)
)
RETURNS NVARCHAR(1)
AS
BEGIN
	RETURN CASE WHEN @PackagingType = 'C' THEN 'T'
				WHEN @PackagingType = 'S' THEN ' X'
				WHEN @PackagingType = 'B' THEN ' B'
				WHEN @PackagingType = 'A' THEN ' C'
				ELSE ''
				END
END