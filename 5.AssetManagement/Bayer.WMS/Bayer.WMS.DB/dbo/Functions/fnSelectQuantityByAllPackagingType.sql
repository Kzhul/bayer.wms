﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnSelectQuantityByAllPackagingType]
(
	@PalletCode NVARCHAR(50)
)
RETURNS NVARCHAR(50)
AS
BEGIN
	RETURN 
		(
			SELECT TOP 1
				OddQuantity = CASE 
								WHEN ISNULL(tmpAll.OddQuantity, 0) > 0 ----hàng nguyên liệu
									THEN CONVERT(NVARCHAR(20),ISNULL(tmpAll.OddQuantity, 0))
								ELSE ----hàng thành phẩm
									 CASE WHEN ISNULL(tmp.PackingType,tmpAll.PackingType) = 'C'----hàng thùng
										 THEN 
											 CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
											  + ' T / ' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
										 ELSE----hàng Xô, Bao, Can
											 CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
											 + CASE WHEN tmp.PackingType = 'S' THEN ' X'
													WHEN tmp.PackingType = 'B' THEN ' B'
													WHEN tmp.PackingType = 'A' THEN ' C'
												END
									     END
								END
			FROM
				Pallets P WITH (NOLOCK)
				LEFT JOIN 
				(
					SELECT
						ps.PalletCode
						, P.PackingType
						, OddQuantity = COUNT(DISTINCT ProductBarcode)
					FROM
						dbo.PalletStatuses ps WITH (NOLOCK)
						JOIN Products P ON ps.ProductID = P.ProductID
					WHERE
						ps.PalletCode = @PalletCode
						AND (ps.CartonBarcode IS NULL OR CartonBarcode = '')
						AND (ps.ProductBarcode IS NOT NULL AND ProductBarcode != '')
					GROUP BY
						ps.PalletCode
						, P.PackingType
				) tmp
					ON P.PalletCode = tmp.PalletCode
				LEFT JOIN 
				(
					SELECT
						ps.PalletCode
						, P.PackingType
						, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
					FROM
						dbo.PalletStatuses ps WITH (NOLOCK)
						JOIN Products P ON ps.ProductID = P.ProductID
					WHERE
						ps.PalletCode = @PalletCode
						AND CartonBarcode IS NOT NULL 
						AND CartonBarcode != ''
					GROUP BY
						ps.PalletCode
						, P.PackingType
				) tmpC
					ON P.PalletCode = tmpC.PalletCode
				LEFT JOIN 
				(
					SELECT
						ps.PalletCode
						, P.PackingType
						, OddQuantity = SUM(PS.Qty)
					FROM
						dbo.PalletStatuses ps WITH (NOLOCK)
						JOIN Products P ON ps.ProductID = P.ProductID
					WHERE
						ps.PalletCode = @PalletCode
						AND PS.Qty >= 1
						AND (ps.CartonBarcode IS NULL OR ps.CartonBarcode = '')
						AND (ps.ProductBarcode IS NULL OR ps.ProductBarcode = '')
					GROUP BY
						ps.PalletCode
						, P.PackingType
				) tmpAll
					ON P.PalletCode = tmpAll.PalletCode
				WHERE
					P.PalletCode = @PalletCode
		)
END