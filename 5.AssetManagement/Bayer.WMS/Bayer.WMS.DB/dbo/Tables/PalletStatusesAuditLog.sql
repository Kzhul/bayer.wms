﻿CREATE TABLE [dbo].[PalletStatusesAuditLog] (
    [PalletCode]              VARCHAR (25)  NOT NULL,
    [CartonBarcode]           VARCHAR (50)  NOT NULL,
    [ProductBarcode]          VARCHAR (255) NOT NULL,
    [EncryptedProductBarcode] VARCHAR (255) NULL,
    [ProductID]               INT           NULL,
    [ProductLot]              VARCHAR (25)  NOT NULL,
    [Qty]                     INT           NULL,
    [Qty_Old]                 INT           NULL,
    [ReasonID]                NVARCHAR (20) NULL,
    [CreatedBy]               INT           NULL,
    [CreatedBySitemapID]      INT           NULL,
    [CreatedDateTime]         DATETIME      NULL,
    [UpdatedBy]               INT           NULL,
    [UpdatedBySitemapID]      INT           NULL,
    [UpdatedDateTime]         DATETIME      NULL,
    [RowVersion]              ROWVERSION    NULL,
    CONSTRAINT [PK_PalletStatusesAuditLog] PRIMARY KEY CLUSTERED ([PalletCode] ASC, [CartonBarcode] ASC, [ProductBarcode] ASC, [ProductLot] ASC)
);

