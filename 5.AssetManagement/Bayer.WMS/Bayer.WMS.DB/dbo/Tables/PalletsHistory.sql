﻿CREATE TABLE [dbo].[PalletsHistory] (
    [ID]                 INT           IDENTITY (1, 1) NOT NULL,
    [PalletCode]         VARCHAR (255) NOT NULL,
    [Type]               CHAR (2)      NULL,
    [StartDate]          DATE          NULL,
    [UsageTime]          INT           NULL,
    [ExpiryDate]         DATE          NULL,
    [Sequence]           INT           NULL,
    [DOImportCode]       VARCHAR (255) NULL,
    [ReferenceNbr]       VARCHAR (255) NULL,
    [CompanyCode]        VARCHAR (255) NULL,
    [CapacityStatus]     CHAR (1)      NULL,
    [Status]             CHAR (1)      NULL,
    [IsDeleted]          BIT           NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_PalletsHistory] PRIMARY KEY CLUSTERED ([ID] ASC)
);



