﻿CREATE TABLE [dbo].[PrepareNoteDetailSplits] (
    [PalletCode]         VARCHAR (255) NOT NULL,
    [DOImportCode]       VARCHAR (255) NOT NULL,
    [ReferenceNbr]       VARCHAR (255) NOT NULL,
    [Status]             CHAR (1)      NULL,
    [IsDeleted]          BIT           NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    [LocationCode]       VARCHAR (255) NULL,
    [LocationSuggestion] VARCHAR (255) NULL,
    [Preparer]           INT           NULL,
    [PrepareDate]        DATETIME      NULL,
    [DeliveryMan]        INT           NULL,
    [DeliveryDate]       DATETIME      NULL,
    CONSTRAINT [PK_PrepareNoteDetailSplits] PRIMARY KEY CLUSTERED ([PalletCode] ASC, [DOImportCode] ASC, [ReferenceNbr] ASC)
);

