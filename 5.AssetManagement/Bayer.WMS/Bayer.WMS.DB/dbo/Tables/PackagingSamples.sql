﻿CREATE TABLE [dbo].[PackagingSamples] (
    [Barcode]            VARCHAR (255) NOT NULL,
    [EncryptedBarcode]   VARCHAR (255) NULL,
    [Type]               CHAR (1)      NULL,
    [ProductLot]         VARCHAR (255) NULL,
    [ProductID]          INT           NULL,
    [Date]               DATETIME      NULL,
    [UserID]             INT           NULL,
    [Device]             VARCHAR (255) NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_PackagingSamples] PRIMARY KEY CLUSTERED ([Barcode] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_PackagingSamples_ProductLot]
    ON [dbo].[PackagingSamples]([ProductLot] ASC)
    INCLUDE([Barcode]);

