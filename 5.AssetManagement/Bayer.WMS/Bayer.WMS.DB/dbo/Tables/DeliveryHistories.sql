﻿CREATE TABLE [dbo].[DeliveryHistories] (
    [ProductID]               INT            NULL,
    [ProductLot]              VARCHAR (255)  NULL,
    [ProductBarcode]          VARCHAR (255)  NOT NULL,
    [EncryptedProductBarcode] VARCHAR (255)  NULL,
    [CartonBarcode]           VARCHAR (255)  NULL,
    [DeliveryTicketCode]      VARCHAR (255)  NULL,
    [CompanyCode]             VARCHAR (255)  NULL,
    [CompanyName]             NVARCHAR (255) NULL,
    [TruckNo]                 VARCHAR (255)  NULL,
    [ActualDeliveryDate]      DATE           NULL,
    [DeliveryDate]            DATE           NULL,
    [CreatedBy]               INT            NULL,
    [CreatedBySitemapID]      INT            NULL,
    [CreatedDateTime]         DATETIME       NULL,
    [UpdatedBy]               INT            NULL,
    [UpdatedBySitemapID]      INT            NULL,
    [UpdatedDateTime]         DATETIME       NULL,
    [RowVersion]              ROWVERSION     NULL,
    [PalletCode]              VARCHAR (50)   NULL,
    CONSTRAINT [PK_DeliveryHistories] PRIMARY KEY CLUSTERED ([ProductBarcode] ASC)
);









