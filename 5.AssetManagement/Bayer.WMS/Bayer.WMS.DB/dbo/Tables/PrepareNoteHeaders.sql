﻿CREATE TABLE [dbo].[PrepareNoteHeaders] (
    [PrepareCode]        NVARCHAR (30)  NOT NULL,
    [PrepareDate]        DATE           NULL,
    [DOImportCode]       NVARCHAR (30)  NOT NULL,
    [Description]        NVARCHAR (250) NULL,
    [DeliveryDate]       DATE           NULL,
    [CompanyCode]        NVARCHAR (30)  NULL,
    [CompanyName]        NVARCHAR (250) NULL,
    [Province]           NVARCHAR (250) NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    [UserCreateFullName] NVARCHAR (250) NULL,
    [UserWareHouse]      NVARCHAR (250) NULL,
    [UserReview]         NVARCHAR (250) NULL,
    CONSTRAINT [PK_PrepareNoteHeaders_1] PRIMARY KEY CLUSTERED ([PrepareCode] ASC)
);







