﻿CREATE TABLE [dbo].[ProductReturnDetails] (
    [ProductReturnCode]  NVARCHAR (50)   NOT NULL,
    [LineNbr]            INT             NOT NULL,
    [ProductID]          INT             NULL,
    [BatchCode]          NVARCHAR (50)   NULL,
    [ReturnQty]          DECIMAL (18, 3) NULL,
    [DeliveredQty]       DECIMAL (18, 3) NULL,
    [Description]        NVARCHAR (255)  NULL,
    [Status]             NVARCHAR (1)    NULL,
    [IsDeleted]          INT             NULL,
    [CreatedBy]          INT             NULL,
    [CreatedBySitemapID] INT             NULL,
    [CreatedDateTime]    DATETIME        NULL,
    [UpdatedBy]          INT             NULL,
    [UpdatedBySitemapID] INT             NULL,
    [UpdatedDateTime]    DATETIME        NULL,
    [RowVersion]         ROWVERSION      NULL,
    CONSTRAINT [PK_ProductReturnDetails] PRIMARY KEY CLUSTERED ([ProductReturnCode] ASC, [LineNbr] ASC)
);

