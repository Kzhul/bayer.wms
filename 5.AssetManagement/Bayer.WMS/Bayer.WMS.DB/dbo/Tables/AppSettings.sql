﻿CREATE TABLE [dbo].[AppSettings] (
    [SettingID]    VARCHAR (255) NOT NULL,
    [SettingValue] VARCHAR (255) NULL,
    CONSTRAINT [PK_AppSettings] PRIMARY KEY CLUSTERED ([SettingID] ASC)
);

