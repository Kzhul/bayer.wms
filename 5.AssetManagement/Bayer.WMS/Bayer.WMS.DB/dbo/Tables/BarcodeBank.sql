﻿CREATE TABLE [dbo].[BarcodeBank] (
    [Barcode]            VARCHAR (255) NOT NULL,
    [Year]               INT           NULL,
    [Sequence]           INT           NULL,
    [IsExport]           BIT           NULL,
	[CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    PRIMARY KEY CLUSTERED ([Barcode] ASC)
);


