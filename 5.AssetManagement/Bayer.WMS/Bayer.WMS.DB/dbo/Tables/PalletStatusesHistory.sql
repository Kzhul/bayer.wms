﻿CREATE TABLE [dbo].[PalletStatusesHistory] (
    [ID]                      INT           IDENTITY (1, 1) NOT NULL,
    [PalletCode]              VARCHAR (255) NULL,
    [CartonBarcode]           VARCHAR (255) NULL,
    [ProductBarcode]          VARCHAR (255) NOT NULL,
    [EncryptedProductBarcode] VARCHAR (255) NULL,
    [ProductID]               INT           NULL,
    [ProductLot]              VARCHAR (255) NULL,
    [Qty]                     INT           NULL,
    [CreatedBy]               INT           NULL,
    [CreatedBySitemapID]      INT           NULL,
    [CreatedDateTime]         DATETIME      NULL,
    [UpdatedBy]               INT           NULL,
    [UpdatedBySitemapID]      INT           NULL,
    [UpdatedDateTime]         DATETIME      NULL,
    [RowVersion]              ROWVERSION    NULL,
    [DOImportCode]            VARCHAR (255) NULL,
    [ReferenceNbr]            VARCHAR (255) NULL,
    [CompanyCode]             VARCHAR (255) NULL,
    CONSTRAINT [PK_PalletStatusesHistory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

