﻿CREATE TABLE [dbo].[ProductionPlanImportLogs] (
    [ID]                 INT            IDENTITY (1, 1) NOT NULL,
    [UserID]             INT            NULL,
    [Note]               NVARCHAR (255) NULL,
    [FromDate]           DATE           NULL,
    [ToDate]             DATE           NULL,
    [FileName]           NVARCHAR (255) NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_ProductionPlanImportLogs] PRIMARY KEY CLUSTERED ([ID] ASC)
);

