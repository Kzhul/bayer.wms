﻿CREATE TABLE [dbo].[DeliveryTicketHeaders] (
    [DeliveryTicketCode]   NVARCHAR (30)   NOT NULL,
    [Description]          NVARCHAR (250)  NULL,
    [DeliveryDate]         DATE            NULL,
    [DeliveryAddress]      NVARCHAR (250)  NULL,
    [CompanyCode]          NVARCHAR (30)   NULL,
    [CompanyName]          NVARCHAR (250)  NULL,
    [Province]             NVARCHAR (250)  NULL,
    [UserCreateFullName]   NVARCHAR (250)  NULL,
    [UserWareHouse]        NVARCHAR (250)  NULL,
    [UserReview]           NVARCHAR (250)  NULL,
    [ListOrderNumbers]     NVARCHAR (250)  NULL,
    [ListFreeItems]        NVARCHAR (250)  NULL,
    [FreeItemsWeight]      NVARCHAR (250)  NULL,
    [TotalBoxes]           DECIMAL (18, 2) NULL,
    [TotalBags]            DECIMAL (18, 2) NULL,
    [TotalBuckets]         DECIMAL (18, 2) NULL,
    [TotalCans]            DECIMAL (18, 2) NULL,
    [GrossWeight]          DECIMAL (18, 2) NULL,
    [TruckNo]              NVARCHAR (250)  NULL,
    [Driver]               NVARCHAR (250)  NULL,
    [DriverIdentification] NVARCHAR (250)  NULL,
    [ReceiptBy]            NVARCHAR (250)  NULL,
    [DeliveverSign]        NVARCHAR (250)  NULL,
    [DriverSign]           NVARCHAR (250)  NULL,
    [Status]               CHAR (1)        NULL,
    [IsDeleted]            BIT             NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    CONSTRAINT [PK_DeliveryTicketHeaders_1] PRIMARY KEY CLUSTERED ([DeliveryTicketCode] ASC)
);





