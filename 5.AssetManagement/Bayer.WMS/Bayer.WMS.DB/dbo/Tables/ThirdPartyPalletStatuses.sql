﻿CREATE TABLE [dbo].[ThirdPartyPalletStatuses] (
    [PalletCode]              VARCHAR (25)  NOT NULL,
    [CartonBarcode]           VARCHAR (50)  NOT NULL,
    [ProductBarcode]          VARCHAR (255) NOT NULL,
    [EncryptedProductBarcode] VARCHAR (255) NULL,
    [ProductID]               INT           NULL,
    [ProductLot]              VARCHAR (25)  NOT NULL,
    [Qty]                     INT           NULL,
    [CreatedBy]               INT           NULL,
    [CreatedBySitemapID]      INT           NULL,
    [CreatedDateTime]         DATETIME      NULL,
    [UpdatedBy]               INT           NULL,
    [UpdatedBySitemapID]      INT           NULL,
    [UpdatedDateTime]         DATETIME      NULL,
    [RowVersion]              ROWVERSION    NULL,
    [Received]                INT           NULL,
    [ReceivedDate]            DATETIME      NULL,
    [WarehouseConfirmID]      INT           NULL,
    [WarehouseConfirmdate]    DATETIME      NULL,
    [DeliveryDate]            DATE          NULL,
    [DOImportCode]            VARCHAR (25)  NULL,
    CONSTRAINT [PK_ThirdPartyPalletStatuses] PRIMARY KEY CLUSTERED ([PalletCode] ASC, [CartonBarcode] ASC, [ProductBarcode] ASC, [ProductLot] ASC)
);

