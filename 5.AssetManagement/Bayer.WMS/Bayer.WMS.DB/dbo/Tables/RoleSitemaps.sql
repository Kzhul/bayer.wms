﻿CREATE TABLE [dbo].[RoleSitemaps] (
    [RoleID]             INT        NOT NULL,
    [SitemapID]          INT        NOT NULL,
    [AccessRights]       CHAR (1)   NOT NULL,
    [CreatedBy]          INT        NULL,
    [CreatedBySitemapID] INT        NULL,
    [CreatedDateTime]    DATETIME   NULL,
    [UpdatedBy]          INT        NULL,
    [UpdatedBySitemapID] INT        NULL,
    [UpdatedDateTime]    DATETIME   NULL,
    [RowVersion]         ROWVERSION NULL,
    CONSTRAINT [PK_RoleSiteMaps] PRIMARY KEY CLUSTERED ([RoleID] ASC, [SitemapID] ASC)
);



