﻿CREATE TABLE [dbo].[Zones] (
    [ZoneCode]           VARCHAR (255)  NOT NULL,
    [ZoneName]           NVARCHAR (255) NULL,
    [Description]        NVARCHAR (255) NULL,
    [WarehouseID]        INT            NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_Zones] PRIMARY KEY CLUSTERED ([ZoneCode] ASC),
    CONSTRAINT [UK_Zones_ZoneCode] UNIQUE NONCLUSTERED ([ZoneCode] ASC)
);







