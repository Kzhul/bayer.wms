﻿CREATE TABLE [dbo].[RequestMaterialLineSplits] (
    [DocumentNbr]                  VARCHAR (255)   NOT NULL,
    [LineNbr]                      INT             NOT NULL,
    [ProductID]                    INT             NULL,
    [PalletCode]                   VARCHAR (255)   NOT NULL,
    [ProductLot]                   VARCHAR (255)   NOT NULL,
    [Quantity]                     DECIMAL (18, 2) NULL,
    [Note]                         NVARCHAR (255)  NULL,
    [Driver]                       INT             NULL,
    [DriverExportDate]             DATETIME        NULL,
    [WarehouseKeeper]              INT             NULL,
    [WarehouseVerifyDate]          DATETIME        NULL,
    [ActualQuantity]               DECIMAL (18, 2) NULL,
    [DriverToProduction]           INT             NULL,
    [DriverToProductionDate]       DATETIME        NULL,
    [Status]                       CHAR (1)        NULL,
    [CreatedBy]                    INT             NULL,
    [CreatedBySitemapID]           INT             NULL,
    [CreatedDateTime]              DATETIME        NULL,
    [UpdatedBy]                    INT             NULL,
    [UpdatedBySitemapID]           INT             NULL,
    [UpdatedDateTime]              DATETIME        NULL,
    [RowVersion]                   ROWVERSION      NULL,
    [ExpiredDate]                  DATETIME        NULL,
    [WarehouseKeeperAllowDropID]   INT             NULL,
    [WarehouseVerifyAllowDropTime] DATETIME        NULL,
    CONSTRAINT [PK_RequestMaterialLineSplits] PRIMARY KEY CLUSTERED ([DocumentNbr] ASC, [LineNbr] ASC, [PalletCode] ASC, [ProductLot] ASC)
);







