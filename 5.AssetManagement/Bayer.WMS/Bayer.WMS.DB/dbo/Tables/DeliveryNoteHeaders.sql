﻿CREATE TABLE [dbo].[DeliveryNoteHeaders] (
    [ExportCode]         VARCHAR (255)  NOT NULL,
    [ExportDate]         DATE           NULL,
    [DOImportCode]       NVARCHAR (50)  NULL,
    [Description]        NVARCHAR (255) NULL,
    [DeliveryDate]       DATE           NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NOT NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_DeliveryNoteHeaders] PRIMARY KEY CLUSTERED ([ExportCode] ASC)
);











