﻿CREATE TABLE [dbo].[Devices] (
    [DeviceCode]         VARCHAR (255)  NOT NULL,
    [Description]        NVARCHAR (255) NULL,
    [PalletType]         VARCHAR (255)  NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_Devices] PRIMARY KEY CLUSTERED ([DeviceCode] ASC)
);

