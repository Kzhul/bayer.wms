﻿CREATE TABLE [dbo].[AuditTrails] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [UserID]      INT            NULL,
    [SitemapID]   INT            NULL,
    [TableName]   VARCHAR (255)  NULL,
    [Data]        XML            NULL,
    [Action]      VARCHAR (255)  NULL,
    [Method]      VARCHAR (255)  NULL,
    [Date]        DATE           NULL,
    [DateTime]    DATETIME       NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AuditTrails] PRIMARY KEY CLUSTERED ([ID] ASC)
);



