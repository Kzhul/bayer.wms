﻿CREATE TABLE [dbo].[DeliveryOrderDetails] (
    [DOImportCode]         VARCHAR (255)   NOT NULL,
    [LineNbr]              INT             NOT NULL,
    [Delivery]             VARCHAR (255)   NULL,
    [ProductID]            INT             NULL,
    [ProductCode]          VARCHAR (255)   NULL,
    [ProductName]          NVARCHAR (255)  NULL,
    [BatchCode]            VARCHAR (255)   NULL,
    [BatchCodeDistributor] VARCHAR (255)   NULL,
    [Quantity]             DECIMAL (18, 2) NULL,
    [DOQuantity]           DECIMAL (18, 2) NULL,
    [PreparedQty]          DECIMAL (18, 2) NULL,
    [DeliveredQty]         DECIMAL (18, 2) NULL,
    [RequireReturnQty]     DECIMAL (18, 2) NULL,
    [CompanyCode]          VARCHAR (255)   NULL,
    [CompanyName]          NVARCHAR (255)  NULL,
    [ProvinceSoldTo]       NVARCHAR (255)  NULL,
    [ProvinceShipTo]       NVARCHAR (255)  NULL,
    [DeliveryDate]         DATE            NULL,
    [Status]               CHAR (1)        NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    [ChangeReason]         NVARCHAR (500)  NULL,
    CONSTRAINT [PK_DeliveryOrderDetails_1] PRIMARY KEY CLUSTERED ([DOImportCode] ASC, [LineNbr] ASC)
);























