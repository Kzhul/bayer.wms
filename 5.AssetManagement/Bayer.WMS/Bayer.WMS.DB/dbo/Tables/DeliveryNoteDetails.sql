﻿CREATE TABLE [dbo].[DeliveryNoteDetails] (
    [ExportCode]           NVARCHAR (30)   NOT NULL,
    [DOCode]               NVARCHAR (30)   NOT NULL,
    [ProductID]            INT             NOT NULL,
    [BatchCode]            NVARCHAR (30)   NOT NULL,
    [BatchCodeDistributor] NVARCHAR (255)  NULL,
    [Quantity]             DECIMAL (18, 2) NULL,
    [PackQuantity]         DECIMAL (18, 2) NULL,
    [PackType]             NVARCHAR (10)   NULL,
    [ProductQuantity]      DECIMAL (18, 2) NULL,
    [Shipper1]             DECIMAL (18, 2) NULL,
    [Receiver1]            INT             NULL,
    [Shipper2]             DECIMAL (18, 2) NULL,
    [Receiver2]            INT             NULL,
    [Shipper3]             DECIMAL (18, 2) NULL,
    [Receiver3]            INT             NULL,
    [ExportedQty]          DECIMAL (18, 2) NULL,
    [ConfirmQty]           DECIMAL (18, 2) NULL,
    [ReceivedQty]          DECIMAL (18, 2) NULL,
    [RequireReturnQty]     DECIMAL (18, 2) NULL,
    [ExportReturnedQty]    DECIMAL (18, 2) NULL,
    [ReceiveReturnedQty]   DECIMAL (18, 2) NULL,
    [Status]               CHAR (1)        NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    [LastQuantity]         DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_DeliveryNoteDetails] PRIMARY KEY CLUSTERED ([ExportCode] ASC, [DOCode] ASC, [ProductID] ASC, [BatchCode] ASC)
);



























