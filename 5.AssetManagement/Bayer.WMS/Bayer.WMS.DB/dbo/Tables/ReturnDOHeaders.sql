﻿CREATE TABLE [dbo].[ReturnDOHeaders] (
    [ReturnDOCode]       NVARCHAR (50)  NOT NULL,
    [Delivery]           NVARCHAR (50)  NULL,
    [DOImportCode]       NVARCHAR (50)  NULL,
    [ReturnDate]         DATE           NOT NULL,
    [ReturnType]         NVARCHAR (50)  NULL,
    [Description]        NVARCHAR (255) NULL,
    [Status]             CHAR (1)       NOT NULL,
    [IsDeleted]          BIT            NOT NULL,
    [CreatedBy]          INT            NOT NULL,
    [CreatedBySitemapID] INT            NOT NULL,
    [CreatedDateTime]    DATETIME       NOT NULL,
    [UpdatedBy]          INT            NOT NULL,
    [UpdatedBySitemapID] INT            NOT NULL,
    [UpdatedDateTime]    DATETIME       NOT NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_ReturnDOHeaders] PRIMARY KEY CLUSTERED ([ReturnDOCode] ASC)
);

