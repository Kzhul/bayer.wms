﻿CREATE TABLE [dbo].[SplitNoteHeaders] (
    [SplitCode]          VARCHAR (255)  NOT NULL,
    [SplitDate]          DATE           NULL,
    [DOImportCode]       VARCHAR (255)  NOT NULL,
    [Description]        NVARCHAR (255) NULL,
    [DeliveryDate]       DATE           NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_SplitNoteHeaders_1] PRIMARY KEY CLUSTERED ([SplitCode] ASC)
);











