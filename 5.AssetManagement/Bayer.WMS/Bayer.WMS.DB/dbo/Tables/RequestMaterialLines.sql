﻿CREATE TABLE [dbo].[RequestMaterialLines] (
    [DocumentNbr]        VARCHAR (255)   NOT NULL,
    [LineNbr]            INT             NOT NULL,
    [ProductID]          INT             NULL,
    [RequestQty]         DECIMAL (18, 2) NULL,
    [BookedQty]          DECIMAL (18, 2) NULL,
    [PickedQty]          DECIMAL (18, 2) NULL,
    [TransferedQty]      DECIMAL (18, 2) NULL,
    [UOM]                VARCHAR (255)   NULL,
    [Note]               NVARCHAR (255)  NULL,
    [Status]             CHAR (1)        NULL,
    [CreatedBy]          INT             NULL,
    [CreatedBySitemapID] INT             NULL,
    [CreatedDateTime]    DATETIME        NULL,
    [UpdatedBy]          INT             NULL,
    [UpdatedBySitemapID] INT             NULL,
    [UpdatedDateTime]    DATETIME        NULL,
    [RowVersion]         ROWVERSION      NULL,
    CONSTRAINT [PK_RequestMaterialLines] PRIMARY KEY CLUSTERED ([DocumentNbr] ASC, [LineNbr] ASC)
);





