﻿CREATE TABLE [dbo].[ProductionPlans] (
    [ProductLot]                VARCHAR (255)   NOT NULL,
    [Device]                    VARCHAR (255)   NULL,
    [PONumber]                  VARCHAR (255)   NULL,
    [ProductID]                 INT             NULL,
    [Quantity]                  DECIMAL (18, 2) NULL,
    [PlanDate]                  DATE            NULL,
    [ManufacturingDate]         DATE            NULL,
    [ExpiryDate]                DATE            NULL,
    [PackageQuantity]           DECIMAL (19, 4) NULL,
    [PackageSize]               DECIMAL (19, 4) NULL,
    [UOM]                       VARCHAR (255)   NULL,
    [Sequence]                  INT             NULL,
    [Note]                      NVARCHAR (MAX)  NULL,
    [IsGenProductBarcode]       BIT             NULL,
    [IsGenCartonBarcode]        BIT             NULL,
    [CurrentLabelCartonBarcode] INT             NULL,
    [MaxLabelCartonBarcode]     INT             NULL,
    [PackagingStartTime]        DATETIME        NULL,
    [PackagingEndTime]          DATETIME        NULL,
    [Status]                    CHAR (1)        NULL,
    [CreatedBy]                 INT             NULL,
    [CreatedBySitemapID]        INT             NULL,
    [CreatedDateTime]           DATETIME        NULL,
    [UpdatedBy]                 INT             NULL,
    [UpdatedBySitemapID]        INT             NULL,
    [UpdatedDateTime]           DATETIME        NULL,
    [RowVersion]                ROWVERSION      NULL,
    CONSTRAINT [PK_ProductionPlans] PRIMARY KEY CLUSTERED ([ProductLot] ASC)
);









