﻿CREATE TABLE [dbo].[BarcodeBankManager] (
    [ID]                 INT            IDENTITY (1, 1) NOT NULL,
    [UserID]             INT            NULL,
    [ProductCode]        NVARCHAR (50)  NULL,
    [Description]        NVARCHAR (250) NULL,
    [FileName]           NVARCHAR (550) NULL,
    [Year]               INT            NULL,
    [Quantity]           INT            NULL,
    [SequenceFrom]       INT            NULL,
    [SequenceTo]         INT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK__BarcodeB__3214EC271772227A] PRIMARY KEY CLUSTERED ([ID] ASC)
);



