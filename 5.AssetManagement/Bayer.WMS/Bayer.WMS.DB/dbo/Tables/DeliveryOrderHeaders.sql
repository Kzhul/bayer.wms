﻿CREATE TABLE [dbo].[DeliveryOrderHeaders] (
    [DOImportCode]       VARCHAR (255)  NOT NULL,
    [DeliveryDate]       DATE           NULL,
    [Description]        NVARCHAR (255) NULL,
    [Status]             CHAR (1)       NOT NULL,
    [IsDeleted]          BIT            NOT NULL,
    [CreatedBy]          INT            NOT NULL,
    [CreatedBySitemapID] INT            NOT NULL,
    [CreatedDateTime]    DATETIME       NOT NULL,
    [UpdatedBy]          INT            NOT NULL,
    [UpdatedBySitemapID] INT            NOT NULL,
    [UpdatedDateTime]    DATETIME       NOT NULL,
    [RowVersion]         ROWVERSION     NULL,
    [IsExportFull]       INT            NULL,
    [IsSplitFull]        INT            NULL,
    [IsPrepareFull]      INT            NULL,
    [IsDeliveryFull]     INT            NULL,
    CONSTRAINT [PK_DeliveryOrderHeaders] PRIMARY KEY CLUSTERED ([DOImportCode] ASC)
);









