﻿CREATE TABLE [dbo].[_BarCodeTest] (
    [Barcode] NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK__BarCodeTest] PRIMARY KEY CLUSTERED ([Barcode] ASC)
);

