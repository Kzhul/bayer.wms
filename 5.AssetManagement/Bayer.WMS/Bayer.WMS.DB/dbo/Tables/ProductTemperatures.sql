﻿CREATE TABLE [dbo].[ProductTemperatures] (
    [ProductID]          INT           NOT NULL,
    [RefValue]           VARCHAR (255) NOT NULL,
    [GroupCode]          VARCHAR (255) NOT NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_ProductTemperatures] PRIMARY KEY CLUSTERED ([ProductID] ASC, [RefValue] ASC, [GroupCode] ASC)
);

