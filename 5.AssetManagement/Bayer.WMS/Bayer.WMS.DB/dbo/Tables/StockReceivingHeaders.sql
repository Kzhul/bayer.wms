﻿CREATE TABLE [dbo].[StockReceivingHeaders] (
    [StockReceivingCode]     NVARCHAR (50)  NOT NULL,
    [DeliveryDate]           DATE           NULL,
    [CompanyName]            NVARCHAR (255) NULL,
    [TruckNo]                NVARCHAR (255) NULL,
    [Driver]                 NVARCHAR (255) NULL,
    [Description]            NVARCHAR (255) NULL,
    [MaterialType]           NVARCHAR (1)   NULL,
    [VerifyStatus]           NVARCHAR (1)   NULL,
    [WMApproveImportDate]    DATETIME       NULL,
    [WarehouseManagerUserID] INT            NULL,
    [ImportStatus]           NVARCHAR (1)   NULL,
    [IsDeleted]              BIT            NULL,
    [CreatedBy]              INT            NULL,
    [CreatedBySitemapID]     INT            NULL,
    [CreatedDateTime]        DATETIME       NULL,
    [UpdatedBy]              INT            NULL,
    [UpdatedBySitemapID]     INT            NULL,
    [UpdatedDateTime]        DATETIME       NULL,
    [RowVersion]             ROWVERSION     NULL,
    CONSTRAINT [PK_StockReceivingHeaders] PRIMARY KEY CLUSTERED ([StockReceivingCode] ASC)
);





