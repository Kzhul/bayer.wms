﻿CREATE TABLE [dbo].[CartonBarcodes] (
    [Barcode]            VARCHAR (255) NOT NULL,
    [ProductLot]         VARCHAR (255) NULL,
    [PalletCode]         VARCHAR (255) NULL,
    [Status]             CHAR (1)      NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_CartonBarcodes] PRIMARY KEY CLUSTERED ([Barcode] ASC)
);






GO
CREATE NONCLUSTERED INDEX [IX_CartonBarcodes_ProductLot]
    ON [dbo].[CartonBarcodes]([ProductLot] ASC)
    INCLUDE([Barcode]);

