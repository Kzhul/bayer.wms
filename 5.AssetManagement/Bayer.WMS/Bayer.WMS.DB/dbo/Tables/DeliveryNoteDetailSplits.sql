﻿CREATE TABLE [dbo].[DeliveryNoteDetailSplits] (
    [PalletCode]           VARCHAR (255) NOT NULL,
    [DOImportCode]         VARCHAR (255) NOT NULL,
    [ReferenceNbr]         VARCHAR (255) NOT NULL,
    [Status]               CHAR (1)      NULL,
    [IsDeleted]            BIT           NULL,
    [CreatedBy]            INT           NULL,
    [CreatedBySitemapID]   INT           NULL,
    [CreatedDateTime]      DATETIME      NULL,
    [UpdatedBy]            INT           NULL,
    [UpdatedBySitemapID]   INT           NULL,
    [UpdatedDateTime]      DATETIME      NULL,
    [RowVersion]           ROWVERSION    NULL,
    [LocationCode]         VARCHAR (255) NULL,
    [LocationSuggestion]   VARCHAR (255) NULL,
    [WarehouseKeeper]      INT           NULL,
    [WarehouseVerifyDate]  DATETIME      NULL,
    [Driver]               INT           NULL,
    [LocationPutDate]      DATETIME      NULL,
    [Received]             INT           NULL,
    [ReceivedDate]         DATETIME      NULL,
    [WarehouseConfirmID]   INT           NULL,
    [WarehouseConfirmdate] DATETIME      NULL,
    CONSTRAINT [PK_DeliveryNoteDetailSplits] PRIMARY KEY CLUSTERED ([PalletCode] ASC, [DOImportCode] ASC, [ReferenceNbr] ASC)
);



