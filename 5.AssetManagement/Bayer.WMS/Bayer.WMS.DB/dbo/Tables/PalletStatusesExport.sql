﻿CREATE TABLE [dbo].[PalletStatusesExport] (
    [PalletCode]              VARCHAR (25)  NOT NULL,
    [CartonBarcode]           VARCHAR (50)  NOT NULL,
    [ProductBarcode]          VARCHAR (255) NOT NULL,
    [EncryptedProductBarcode] VARCHAR (255) NULL,
    [ProductID]               INT           NULL,
    [ProductLot]              VARCHAR (25)  NOT NULL,
    [Qty]                     INT           NULL,
    [Received]                BIT           NULL,
    [CreatedBy]               INT           NULL,
    [CreatedBySitemapID]      INT           NULL,
    [CreatedDateTime]         DATETIME      NULL,
    [UpdatedBy]               INT           NULL,
    [UpdatedBySitemapID]      INT           NULL,
    [UpdatedDateTime]         DATETIME      NULL,
    [RowVersion]              ROWVERSION    NULL,
    [DOImportCode]            VARCHAR (255) NULL,
    [ReferenceNbr]            VARCHAR (255) NULL,
    [CompanyCode]             VARCHAR (255) NULL,
    CONSTRAINT [PK_PalletStatusesExport] PRIMARY KEY CLUSTERED ([PalletCode] ASC, [CartonBarcode] ASC, [ProductBarcode] ASC, [ProductLot] ASC)
);



