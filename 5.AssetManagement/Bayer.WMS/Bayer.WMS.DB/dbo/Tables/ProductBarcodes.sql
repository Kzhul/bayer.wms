﻿CREATE TABLE [dbo].[ProductBarcodes] (
    [Barcode]            VARCHAR (255) NOT NULL,
    [EncryptedBarcode]   VARCHAR (255) NULL,
    [ProductLot]         VARCHAR (255) NULL,
    [ProductID]          INT           NULL,
    [CartonBarcode]      VARCHAR (255) NULL,
    [PalletCode]         VARCHAR (255) NULL,
    [Status]             CHAR (1)      NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_ProductBarcodes] PRIMARY KEY CLUSTERED ([Barcode] ASC)
);








GO
CREATE NONCLUSTERED INDEX [IX_ProductBarcodes_ProductLot]
    ON [dbo].[ProductBarcodes]([ProductLot] ASC)
    INCLUDE([Barcode], [ProductID]);

