﻿CREATE TABLE [dbo].[PalletStatuses] (
    [PalletCode]              VARCHAR (25)  NOT NULL,
    [CartonBarcode]           VARCHAR (50)  NOT NULL,
    [ProductBarcode]          VARCHAR (255) NOT NULL,
    [EncryptedProductBarcode] VARCHAR (255) NULL,
    [ProductID]               INT           NULL,
    [ProductLot]              VARCHAR (25)  NOT NULL,
    [Qty]                     INT           NULL,
    [CreatedBy]               INT           NULL,
    [CreatedBySitemapID]      INT           NULL,
    [CreatedDateTime]         DATETIME      NULL,
    [UpdatedBy]               INT           NULL,
    [UpdatedBySitemapID]      INT           NULL,
    [UpdatedDateTime]         DATETIME      NULL,
    [RowVersion]              ROWVERSION    NULL,
    CONSTRAINT [PK_PalletStatuses] PRIMARY KEY CLUSTERED ([PalletCode] ASC, [CartonBarcode] ASC, [ProductBarcode] ASC, [ProductLot] ASC)
);






GO
CREATE NONCLUSTERED INDEX [IX_PalletStatuses_PalletCode]
    ON [dbo].[PalletStatuses]([PalletCode] ASC)
    INCLUDE([CartonBarcode], [ProductBarcode], [EncryptedProductBarcode], [ProductID]);


GO
CREATE NONCLUSTERED INDEX [IX_PalletStatuses_CartonBarcode]
    ON [dbo].[PalletStatuses]([CartonBarcode] ASC)
    INCLUDE([PalletCode], [ProductBarcode], [EncryptedProductBarcode], [ProductID], [ProductLot]);

