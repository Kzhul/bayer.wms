﻿CREATE TABLE [dbo].[Sitemaps] (
    [SitemapID]     INT            NOT NULL,
    [ParentID]      INT            NULL,
    [Description]   NVARCHAR (255) NULL,
    [ClassName]     VARCHAR (255)  NULL,
    [InterfaceName] VARCHAR (255)  NULL,
    [AssemblyName]  VARCHAR (255)  NULL,
    [Order]         INT            NULL,
    [Icon]          VARCHAR (255)  NULL,
    [EnableRefresh] BIT            NULL,
    [EnableInsert]  BIT            NULL,
    [EnableSave]    BIT            NULL,
    [EnableDelete]  BIT            NULL,
    [EnablePrint]   BIT            NULL,
    [Type]          VARCHAR (1)    NULL,
    CONSTRAINT [PK_Sitemaps] PRIMARY KEY CLUSTERED ([SitemapID] ASC)
);





