﻿CREATE TABLE [dbo].[RequestMaterials] (
    [DocumentNbr]        VARCHAR (255)  NOT NULL,
    [Note]               NVARCHAR (255) NULL,
    [Date]               DATE           NULL,
    [Status]             CHAR (1)       NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_RequestMaterials] PRIMARY KEY CLUSTERED ([DocumentNbr] ASC)
);

