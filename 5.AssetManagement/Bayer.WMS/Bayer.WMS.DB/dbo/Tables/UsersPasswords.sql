﻿CREATE TABLE [dbo].[UsersPasswords] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [UserID]      INT           NULL,
    [Password]    VARCHAR (255) NULL,
    [LastUpdated] DATETIME      NULL,
    CONSTRAINT [PK_UsersPasswords] PRIMARY KEY CLUSTERED ([ID] ASC)
);

