﻿CREATE TABLE [dbo].[ZoneTemperatures] (
    [ZoneCode]           VARCHAR (255) NOT NULL,
    [RefValue]           VARCHAR (255) NOT NULL,
    [GroupCode]          VARCHAR (255) NOT NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_ZoneTemperatures] PRIMARY KEY CLUSTERED ([ZoneCode] ASC, [RefValue] ASC, [GroupCode] ASC)
);







