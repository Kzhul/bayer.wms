﻿CREATE TABLE [dbo].[Roles] (
    [RoleID]             INT            IDENTITY (1, 1) NOT NULL,
    [RoleName]           NVARCHAR (255) NULL,
    [Description]        NVARCHAR (255) NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleID] ASC)
);




GO
CREATE NONCLUSTERED INDEX [UK_Roles_RoleName]
    ON [dbo].[Roles]([RoleName] ASC);

