﻿CREATE TABLE [dbo].[ZoneLines] (
    [ZoneCode]           VARCHAR (255)  NOT NULL,
    [LineCode]           VARCHAR (255)  NOT NULL,
    [LineName]           NVARCHAR (50)  NULL,
    [Description]        NVARCHAR (500) NULL,
    [Length]             INT            NULL,
    [Level]              INT            NULL,
    [Type]               CHAR (1)       NULL,
    [Order]              INT            NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_ZoneLine] PRIMARY KEY CLUSTERED ([ZoneCode] ASC, [LineCode] ASC)
);









