﻿CREATE TABLE [dbo].[ProductPackings] (
    [ProductPackingCode] VARCHAR (255)   NOT NULL,
    [ProductID]          INT             NOT NULL,
    [Type]               CHAR (1)        NULL,
    [Quantity]           DECIMAL (18, 2) NULL,
    [Size]               NVARCHAR (255)  NULL,
    [Weight]             NVARCHAR (255)  NULL,
    [Note]               NVARCHAR (255)  NULL,
    [CreatedBy]          INT             NULL,
    [CreatedBySitemapID] INT             NULL,
    [CreatedDateTime]    DATETIME        NULL,
    [UpdatedBy]          INT             NULL,
    [UpdatedBySitemapID] INT             NULL,
    [UpdatedDateTime]    DATETIME        NULL,
    [RowVersion]         ROWVERSION      NULL,
    CONSTRAINT [PK_ProductPackings] PRIMARY KEY CLUSTERED ([ProductPackingCode] ASC, [ProductID] ASC)
);





