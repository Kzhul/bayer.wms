﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_SplitNoteHeader]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method
DECLARE @_ExportCode NVARCHAR(50) = (SELECT TOP 1 SplitCode FROM SplitNoteHeaders WHERE DOImportCode = @_DOImportCode)
DECLARE @_DeliveryDate DATE = (SELECT TOP 1 DeliveryDate FROM [DeliveryOrderSum] WHERE DOImportCode = @_DOImportCode)
--------------SOURCE-------------------
IF NOT EXISTS (SELECT * FROM SplitNoteHeaders WHERE DOImportCode = @_DOImportCode)
BEGIN
	INSERT INTO [dbo].[SplitNoteHeaders]
           ([SplitCode]
           ,[SplitDate]
           ,[DOImportCode]
           ,[Description]
           ,[DeliveryDate]
           ,[Status]
           ,[IsDeleted]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
     VALUES
           (
		   dbo.[fnTicketNextNBR]('CH')
           ,GETDATE()
           ,@_DOImportCode
           ,''
           ,@_DeliveryDate
           ,'N'
           ,0
			,@_UserID--SOURCE.[CreatedBy]
			,@_SitemapID--SOURCE.[CreatedBySitemapID]
			,@_SyncDate--SOURCE.[CreatedDateTime]
			,@_UserID--SOURCE.[UpdatedBy]
			,@_SitemapID--[UpdatedBySitemapID]
			,@_SyncDate--[UpdatedDateTime]
			)
END
END