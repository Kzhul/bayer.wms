﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_SelectDocuments]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @DateToGet DATE = CONVERT(DATE, DATEADD(dd,-30,GETDATE()))

    --Lấy nguyên liệu theo mã phiếu yêu cầu
	SELECT DISTINCT 
		r.DocumentNbr
		,ExportDate = FORMAT(R.Date, 'dd/MM/yyyy')
		,RequestQty = SUM(RM.RequestQty)
		,ExportedQty = SUM(RM.TransferedQty)
		,R.Date
	FROM
		RequestMaterialLines RM WITH (NOLOCK) 
		LEFT JOIN RequestMaterials R WITH (NOLOCK) 
			ON RM.DocumentNbr = R.DocumentNbr
		LEFT JOIN Products PD WITH (NOLOCK) 
			ON RM.ProductID = PD.ProductID
	WHERE
		R.[Date] >= @DateToGet
	GROUP BY
		r.DocumentNbr
		,R.Date
	ORDER BY R.Date DESC
END