﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectIncludeLocation]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type

	--CLEAR LOCATION OF THIS PALLET
	--UPDATE Pallets SET LocationCode = NULL, LocationPutDate = NULL
	--FROM 
	--	Pallets P
	--	JOIN PalletStatuses PS
	--		ON P.PalletCode = PS.PalletCode
	--WHERE
	--	PS.PalletCode = @Barcode
	--	OR PS.ProductBarcode = @Barcode
	--	OR PS.CartonBarcode = @Barcode

	IF @_Type = 'P'
		SELECT
			pl.PalletCode
			, pl.DOImportCode
			, pl.Status
			, pl.ReferenceNbr
			, pl.CompanyCode
			, tmp.ProductLot
			, tmp.ProductQty
			, tmp.CartonQty
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
			, PL.LocationCode
			, LocationName = ISNULL(ZL.LocationName,'')
		FROM
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonQty = COUNT(DISTINCT ps.CartonBarcode)
					, ProductQty = SUM(ISNULL(ps.Qty, 1))
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			)tmp
			JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = tmp.PalletCode
			LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = tmp.ProductID
													  AND p.IsDeleted = 0
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON pl.LocationCode = ZL.LocationCode
	ELSE IF @_Type = 'C'
		SELECT
			pl.PalletCode
			, pl.DOImportCode
			, pl.Status
			, pl.ReferenceNbr
			, pl.CompanyCode
			, tmp.ProductLot
			, tmp.ProductQty
			, tmp.CartonQty
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
			, PL.LocationCode
			, LocationName = ISNULL(ZL.LocationName,'')
		FROM
			(
				SELECT
					ps.PalletCode
					, ps.CartonBarcode
					, ps.ProductID
					, ps.ProductLot
					, CartonQty = 1
					, ProductQty = COUNT(1)
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					ps.CartonBarcode = @_Barcode
				GROUP BY
					ps.PalletCode
					, ps.CartonBarcode
					, ps.ProductID
					, ps.ProductLot
			)tmp
			JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = tmp.PalletCode
			LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = tmp.ProductID
													  AND p.IsDeleted = 0
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON pl.LocationCode = ZL.LocationCode
	ELSE IF @_Type = 'E'
		SELECT
			pl.PalletCode
			, pl.DOImportCode
			, pl.Status
			, pl.ReferenceNbr
			, pl.CompanyCode
			, ps.ProductLot
			, ProductQty = 1
			, CartonQty = 0
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
			, PL.LocationCode
			, LocationName = ISNULL(ZL.LocationName,'')
		FROM
			dbo.PalletStatuses ps WITH (NOLOCK)
			JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
													  AND p.IsDeleted = 0
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON pl.LocationCode = ZL.LocationCode
		WHERE
			ps.ProductBarcode = @_Barcode
			OR ps.EncryptedProductBarcode = @_Barcode
END