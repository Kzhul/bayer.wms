﻿CREATE PROCEDURE [dbo].[proc_WriteLog]
	@Message NVARCHAR(MAX)
	, @StackTrace NVARCHAR(MAX)
	, @InnerException_Message NVARCHAR(MAX)
	, @InnerException_StackTrace NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

INSERT INTO [dbo].[_Log]
           ([Message]
           ,[StackTrace]
           ,[InnerException_Message]
           ,[InnerException_StackTrace])
     VALUES
           (@Message
           ,@StackTrace
           ,@InnerException_Message
           ,@InnerException_StackTrace)
END