﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Select_ByRefereneNbr]
	@ReferenceNbr VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_ReferenceNbr VARCHAR(255) = @ReferenceNbr

    SELECT
		ps.PalletCode
		, ps.CartonBarcode
		, ps.ProductBarcode
		, ps.EncryptedProductBarcode
		, ps.ProductLot
		, Qty = ISNULL(ps.Qty, 1)
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
	FROM
		dbo.Pallets pl WITH (NOLOCK)
		JOIN dbo.PalletStatuses ps WITH (NOLOCK) ON ps.PalletCode = pl.PalletCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
												  AND p.IsDeleted = 0
	WHERE
		pl.ReferenceNbr = @_ReferenceNbr
END