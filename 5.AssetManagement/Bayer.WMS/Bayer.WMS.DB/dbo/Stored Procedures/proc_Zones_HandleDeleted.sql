﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.proc_Zones_HandleDeleted
	@ZoneCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_ZoneCode VARCHAR(255) = @ZoneCode

	UPDATE dbo.ZoneLines
	SET
		IsDeleted = 1
	WHERE
		ZoneCode = @_ZoneCode

	UPDATE dbo.ZoneLocations
	SET
		IsDeleted = 1
	WHERE
		ZoneCode = @_ZoneCode
END