﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatusesExport_Delete]
	@PalletCode VARCHAR(255)
	, @UserID INT
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_Date DATETIME = GETDATE()

	DELETE FROM [dbo].[PalletStatusesExport]
	WHERE
		ProductBarcode IN (
			SELECT DISTINCT ProductBarcode FROM dbo.PalletStatuses
			WHERE PalletCode = @_PalletCode
		)


END