﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PrepareNote_Select]
	@PrepareNote VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PrepareNote VARCHAR(255) = @PrepareNote

	SELECT
		pnh.PrepareCode
		, pnh.DOImportCode
		, pnh.CompanyCode
		, pnh.CompanyName
		, pnh.Description
		, pnh.DeliveryDate
		, pnd.BatchCode
		, pnd.Quantity
		, pnd.PackQuantity
		, pnd.ProductQuantity
		, PreparedQty = ISNULL(pnd.PreparedQty, 0) - ISNULL(pnd.ReturnedQty, 0)
		, ReturnedQty = ISNULL(pnd.ReturnedQty, 0)
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
	FROM
		dbo.PrepareNoteHeaders pnh WITH (NOLOCK)
		JOIN dbo.PrepareNoteDetails pnd WITH (NOLOCK) ON pnd.PrepareCode = pnh.PrepareCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = pnd.ProductID
	WHERE
		pnh.PrepareCode = @_PrepareNote
	ORDER BY
		p.Description
END