﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceiving_SelectDocument]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @DateToGet DATE = CONVERT(DATE, DATEADD(dd,-30,GETDATE()))

--PRINT (@DateToGet)
	SELECT DISTINCT 
		DND.StockReceivingCode AS StockReceivingCode
		,DND.DeliveryDate AS DeliveryDate
		,[QuantityPlanning] = SUM([QuantityPlanning])
		,[QuantityReceived] = SUM([QuantityReceived])
	FROM 
		StockReceivingHeaders DND WITH (NOLOCK)
		JOIN [dbo].StockReceivingDetails DH WITH (NOLOCK)
			ON DND.StockReceivingCode = DH.StockReceivingCode
	WHERE 
		DH.DeliveryDate >= @DateToGet
	GROUP BY
		DND.StockReceivingCode
		,DND.DeliveryDate
	ORDER BY DND.DeliveryDate DESC
END