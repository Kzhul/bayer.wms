﻿CREATE PROCEDURE [dbo].[proc_Bay4SData_ReportTotal]
	
AS
BEGIN
	SET NOCOUNT ON;

    
SELECT DISTINCT 
	B.ProductCode
	,B.ProductName
	,B.BatchCode
	,B.BatchCodeDistributor
	,B.UOM
	,B.Unrestricted
	,B.InQualityInsp
	,B.Blocked
	,B.ManufacturingDate
	,B.ExpiredDate
	,TB.PackSize
	,TB.Quantity
	, StrManufacturingDate = FORMAT(B.ManufacturingDate, 'dd/MM/yyyy')
	, StrExpiredDate = FORMAT(B.ExpiredDate, 'dd/MM/yyyy')
	, SumQuantity = CONVERT(INT, ISNULL(B.Unrestricted, 0) + ISNULL(B.InQualityInsp, 0) + ISNULL(B.Blocked, 0))
	, DifQuantity = CONVERT(INT, ISNULL(B.Unrestricted, 0) + ISNULL(B.InQualityInsp, 0) + ISNULL(B.Blocked, 0) - ISNULL(TB.Quantity, 0))
FROM
[dbo].[Bay4SData] B WITH (NOLOCK)
LEFT JOIN 
(
	SELECT DISTINCT
		   PS.ProductLot
		 , PR.ProductCode
		 , PR.UOM
		 , CONVERT(INT,PPS.Quantity) AS PackSize
		 , SUM(ISNULL(PS.Qty,1)) AS Quantity
	FROM 		
		[dbo].[Pallets] P WITH (NOLOCK)
		LEFT JOIN [dbo].[ZoneLocations] L WITH (NOLOCK)
			ON P.LocationCode = L.LocationCode
			AND P.LocationCode IS NOT NULL
			AND P.LocationCode != ''
		LEFT JOIN [dbo].[ZoneLines] ZL WITH (NOLOCK)
			ON ZL.ZoneCode = L.ZoneCode
			AND ZL.LineCode = L.LineCode
		LEFT JOIN Zones Z WITH (NOLOCK)
			ON Z.ZoneCode = ZL.ZoneCode
		LEFT JOIN PalletStatuses PS WITH (NOLOCK)
			ON P.PalletCode = PS.PalletCode	
		LEFT JOIN Products PR WITH (NOLOCK)
			 ON PS.ProductID = PR.ProductID
		LEFT JOIN ProductionPlans pp WITH (NOLOCK)
			ON PS.ProductLot = PP.ProductLot
		LEFT JOIN [dbo].[ProductPackings] PPS WITH (NOLOCK)
			ON PR.ProductID = PPS.ProductID
			AND PPS.Type != 'P'
		LEFT JOIN Users R WITH (NOLOCK)
			 ON P.DriverReceived = R.UserID
		LEFT JOIN Users W WITH (NOLOCK)
			 ON P.WarehouseKeeper = W.UserID
		LEFT JOIN Users D WITH (NOLOCK)
			 ON P.Driver = D.UserID
	WHERE
		P.PalletCode IS NOT NULL
		AND PS.ProductLot IS NOT NULL
		AND P.PalletCode NOT LIKE '%99999'
	GROUP BY
		PS.ProductLot
		, PR.ProductCode
		, PR.UOM
		, PPS.Quantity-- AS PackSize
) AS TB
ON B.ProductCode = TB.ProductCode
AND B.BatchCode = TB.ProductLot
END