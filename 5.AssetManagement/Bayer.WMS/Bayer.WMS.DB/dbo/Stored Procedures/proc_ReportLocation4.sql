﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ReportLocation4]
	@Product NVARCHAR(255),
	@ProductLot NVARCHAR(255),
	@User NVARCHAR(255),
	@Warehouse NVARCHAR(255),
	@Zone NVARCHAR(255),
	@Line NVARCHAR(255),
	@FromDate Date,
	@ToDate Date,
	@SearchDate INT
AS
BEGIN
	SET NOCOUNT ON
	DECLARE 
	@_Product NVARCHAR(255) = @Product,
	@_ProductLot NVARCHAR(255) = @ProductLot,
	@_User NVARCHAR(255) = @User,
	@_Warehouse NVARCHAR(255) = @Warehouse,
	@_Zone NVARCHAR(255) = @Zone,
	@_Line NVARCHAR(255) = @Line,
	@_FromDate Date = @FromDate,
	@_ToDate Date = @ToDate,
	@_SearchDate INT = @SearchDate

SELECT DISTINCT 
	* 
FROM
(
	SELECT DISTINCT
		 WH.WarehouseCode
		 , WH.Description AS WarehouseName
		 , Z.ZoneName
		 , ZL.LineName
		 , L.LocationCode
		 , L.LocationName
		 , P.PalletCode
		 , PS.ProductLot
		 , PR.ProductCode
		 , PR.Description AS ProductName
		 , PR.UOM
		 , CONVERT(INT,PPS.Quantity) AS PackSize
		 , COUNT(DISTINCT CASE WHEN PS.CartonBarcode = '' THEN 0 ELSE 1 END) AS CartonQuantity
		 , SUM(ISNULL(PS.Qty,1)) AS Quantity	 
		 , ManufacturingDate = FORMAT(PP.ManufacturingDate, 'dd/MM/yyyy')
		 , ExpiryDate = FORMAT(PP.ExpiryDate, 'dd/MM/yyyy')
		 , StrDriverReceivedDate = FORMAT(P.DriverReceivedDate, 'dd/MM/yyyy HH:mm:ss')
		 , StrDriverReceived = ISNULL(R.LastName + ' ', '') + ISNULL(R.FirstName, '')
		 , StrLocationPutDate = FORMAT(P.LocationPutDate, 'dd/MM/yyyy HH:mm:ss')
		 , StrDriver = ISNULL(D.LastName + ' ', '') + ISNULL(D.FirstName, '')
		 , StrWarehouseVerifyDate = FORMAT(P.WarehouseVerifyDate, 'dd/MM/yyyy HH:mm:ss')
		 , StrWarehouseKeeper = ISNULL(W.LastName + ' ', '') + ISNULL(W.FirstName, '')	 
		 , Description = PR.Description
		 , WarehouseVerifyNote
	FROM 		
		[dbo].[Pallets] P WITH (NOLOCK)
		LEFT JOIN [dbo].[ZoneLocations] L WITH (NOLOCK)
			ON P.LocationCode = L.LocationCode
			AND P.LocationCode IS NOT NULL
			AND P.LocationCode != ''
		LEFT JOIN [dbo].[ZoneLines] ZL WITH (NOLOCK)
			ON ZL.ZoneCode = L.ZoneCode
			AND ZL.LineCode = L.LineCode
		LEFT JOIN Zones Z WITH (NOLOCK)
			ON Z.ZoneCode = ZL.ZoneCode
		LEFT JOIN Warehouses WH WITH (NOLOCK)
			ON Z.WarehouseID = WH.WarehouseID
		LEFT JOIN PalletStatuses PS WITH (NOLOCK)
			ON P.PalletCode = PS.PalletCode	
		LEFT JOIN Products PR WITH (NOLOCK)
			 ON PS.ProductID = PR.ProductID
		LEFT JOIN ProductionPlans pp WITH (NOLOCK)
			ON PS.ProductLot = PP.ProductLot
		LEFT JOIN [dbo].[ProductPackings] PPS WITH (NOLOCK)
			ON PR.ProductID = PPS.ProductID
			AND PPS.Type != 'P'
		LEFT JOIN Users R WITH (NOLOCK)
			 ON P.DriverReceived = R.UserID
		LEFT JOIN Users W WITH (NOLOCK)
			 ON P.WarehouseKeeper = W.UserID
		LEFT JOIN Users D WITH (NOLOCK)
			 ON P.Driver = D.UserID
	WHERE
		P.PalletCode IS NOT NULL
		AND PS.ProductLot IS NOT NULL
		AND P.PalletCode NOT LIKE '%99999'
		--AND (P.WarehouseKeeper = 84 OR P.WarehouseKeeper = 86)
		--AND P.UpdatedDateTime >= '2018-07-14'
		AND (
			@_SearchDate = 0
			OR (@_SearchDate = 1 AND P.WarehouseVerifyDate >= @_FromDate AND Convert(DATE,P.WarehouseVerifyDate) <= @_ToDate)
			--OR (P.DriverReceivedDate >= @_FromDate AND P.DriverReceivedDate <= @_ToDate)
			OR (@_SearchDate = 2 AND P.LocationPutDate >= @_FromDate AND Convert(DATE,P.LocationPutDate) <= @_ToDate)
		)
	GROUP BY
		 WH.WarehouseCode
		 , WH.Description
		 , Z.ZoneName
		 , ZL.LineName
		 , L.LocationCode
		 , L.LocationName
		 , P.PalletCode
		 , PS.ProductLot
		 , PR.ProductCode
		 , PR.Description-- AS ProductName
		 , PR.UOM
		 , PPS.Quantity-- AS PackSize
		 , PP.ManufacturingDate
		 , PP.ExpiryDate
		 , P.LocationPutDate
		 , P.[WarehouseVerifyDate]
		 , P.[DriverReceivedDate]
		 , R.FirstName
		 , R.LastName
		 , D.FirstName
		 , D.LastName
		 , W.FirstName
		 , W.LastName
		 , WarehouseVerifyNote
) AS TB
WHERE
	(@_Product = '' OR TB.ProductName LIKE '%' + @_Product + '%' OR TB.ProductCode LIKE '%' + @_Product + '%')
	AND (@_Warehouse = '' OR TB.WarehouseName LIKE '%' + @_Warehouse + '%')
	AND (@_Zone = '' OR TB.ZoneName LIKE '%' + @_Zone + '%')
	AND (@_Line = '' OR TB.LineName LIKE '%' + @_Line + '%')
	AND (@_ProductLot = '' OR TB.ProductLot LIKE '%' + @_ProductLot + '%')
	AND (@_User = '' OR 
			(TB.StrWarehouseKeeper LIKE N'%' + @_User + '%')
			OR (TB.StrDriver LIKE N'%' + @_User + '%')
		)
ORDER BY 
	ZoneName
	 , LineName
	 , LocationName
END