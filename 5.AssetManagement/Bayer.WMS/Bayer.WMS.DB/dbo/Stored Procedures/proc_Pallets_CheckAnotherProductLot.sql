﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_CheckAnotherProductLot]
	@PalletCode VARCHAR(255)
	, @ProductLot VARCHAR(255)
AS
BEGIN
	--Bộ phận đóng gói sử dụng store này khi đóng gói

	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_ProductLot VARCHAR(255) = NULLIF(@ProductLot, '')
	
	SELECT DISTINCT
		PS.*
		,P.ProductCode
		,P.Description AS ProductName
	FROM 
		dbo.PalletStatuses PS WITH (NOLOCK)
		JOIN Products P WITH (NOLOCK)
			ON PS.ProductID = P.ProductID
	WHERE 
		PalletCode = @_PalletCode
		AND ProductLot != @_ProductLot
END