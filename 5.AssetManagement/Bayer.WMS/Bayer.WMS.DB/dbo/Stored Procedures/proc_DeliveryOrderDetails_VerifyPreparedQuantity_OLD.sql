﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity]
--EXEC [proc_DeliveryOrderDetails_VerifyPreparedQuantity] '1100189', '170901F08C009'
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_VerifyPreparedQuantity_OLD]
	@CompanyCode VARCHAR(255)
	,@BarCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_BarCode VARCHAR(255) = @BarCode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode

	IF NOT EXISTS(
		SELECT DISTINCT
			ps.ProductBarcode
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			LEFT JOIN Pallets p WITH (NOLOCK)
				ON ps.PalletCode = p.PalletCode
		WHERE
			CompanyCode = @_CompanyCode
			AND (
				CartonBarcode = @_BarCode 
				OR ProductBarcode = @_BarCode 
				OR EncryptedProductBarcode = @_BarCode
				OR PS.PalletCode = @_BarCode 
			)
	)
	BEGIN
		DECLARE @_ProductLot VARCHAR(255) = ISNULL((SELECT TOP 1 ProductLot 
												FROM PalletStatuses WITH (NOLOCK) 
												WHERE 
													CartonBarcode = @_BarCode 
													OR ProductBarcode = @_BarCode 
													OR EncryptedProductBarcode = @_BarCode
													OR PalletCode = @_BarCode 
											),'')
		DECLARE @_ProductID VARCHAR(255) = ISNULL((SELECT TOP 1 ProductID 
												FROM PalletStatuses WITH (NOLOCK) 
												WHERE 
													CartonBarcode = @_BarCode 
													OR ProductBarcode = @_BarCode 
													OR EncryptedProductBarcode = @_BarCode
													OR PalletCode = @_BarCode 
											),0)
		DECLARE @_NextQuantity VARCHAR(255) = ISNULL((SELECT DISTINCT
													COUNT(DISTINCT ProductBarcode)	
												FROM PalletStatuses WITH (NOLOCK) 
												WHERE 
													CartonBarcode = @_BarCode 
													OR ProductBarcode = @_BarCode 
													OR EncryptedProductBarcode = @_BarCode
													OR PalletCode = @_BarCode 
											),0)
		DECLARE @_PreparedQuantity INT = ISNULL((
			SELECT DISTINCT
				SUM(ps.PreparedQty)	
			FROM 
				[dbo].[DeliveryOrderSum] PS WITH (NOLOCK)
			WHERE
				CompanyCode = @_CompanyCode
				AND (PS.BatchCode = @_ProductLot OR @_ProductLot = '')
				AND PS.ProductID = @_ProductID
				AND PS.[DeliveryDate] >= CONVERT(DATE,GETDATE())
				AND PS.ProductID != 0
			GROUP BY 
				PS.CompanyCode 
				,PS.BatchCode
				,PS.ProductID
		),0)

		DECLARE @_DOQuantity INT = ISNULL((
			SELECT DISTINCT
				SUM(ps.DOQuantity)	
			FROM 
				[dbo].[DeliveryOrderSum] PS WITH (NOLOCK)
			WHERE
				CompanyCode = @_CompanyCode
				AND (PS.BatchCode = @_ProductLot OR @_ProductLot = '')
				AND PS.ProductID = @_ProductID
				AND PS.[DeliveryDate] >= CONVERT(DATE,GETDATE())
				AND PS.ProductID != 0
			GROUP BY 
				PS.CompanyCode 
				,PS.BatchCode
				,PS.ProductID
		),0)

	
		SELECT TOP 3
			CompanyCode = @_CompanyCode--DO.CompanyCode
			, C.CompanyName
			, DOImportCode = ''
			, ProductLot = @_ProductLot
			, ProductCode = P.ProductCode
			, ProductName = P.Description
			, DOQuantity = @_DOQuantity
			, PreparedQuantity = @_PreparedQuantity + @_NextQuantity
			, P.ProductID
			, NextQuantity = @_NextQuantity
		FROM 
			Companies C WITH (NOLOCK)
			,Products P WITH (NOLOCK)
		WHERE
			C.CompanyCode = @_CompanyCode
			AND P.ProductID = @_ProductID
			AND (
					((@_PreparedQuantity + @_NextQuantity) > @_DOQuantity)
					OR @_DOQuantity = 0
				)
	END
END