﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.proc_DeliveryHistories_Insert_Manual
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
	, @CompanyCode VARCHAR(255)
	, @CompanyName NVARCHAR(255)
	, @UserID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_CompanyName NVARCHAR(255) = @CompanyName
	DECLARE @_DeliveryDate DATETIME = GETDATE()
	DECLARE @_UserID INT = @UserID
	DECLARE @_Date DATETIME = GETDATE()

    INSERT INTO dbo.DeliveryHistories
	(
		ProductID
		, ProductLot
		, ProductBarcode
	    , EncryptedProductBarcode
	    , CartonBarcode
	    , CompanyCode
	    , CompanyName
	    , DeliveryDate
	    , CreatedBy
	    , CreatedDateTime
	    , UpdatedBy
	    , UpdatedDateTime
	)
	SELECT
		ps.ProductID
		, ps.ProductLot
		, ps.ProductBarcode
		, ps.EncryptedProductBarcode
		, ps.CartonBarcode
		, @_CompanyCode
		, @_CompanyName
		, @_DeliveryDate
		, @_UserID
		, @_Date
		, @_UserID
		, @_Date
	FROM
		dbo.PalletStatuses ps
	WHERE
		(ps.CartonBarcode = @_Barcode AND @_Type = 'C')
		OR (ps.ProductBarcode = @_Barcode AND @_Type = 'E')

	DELETE FROM dbo.PalletStatuses WHERE (CartonBarcode = @_Barcode AND @_Type = 'C') OR (ProductBarcode = @_Barcode AND @_Type = 'E')
END