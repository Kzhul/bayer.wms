﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryHistories_Insert_FromOffline]
	@Username VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @Barcode VARCHAR(255)
	, @Type CHAR(1)
	, @ProductLot VARCHAR(255)
	, @DeliveryDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_UserID INT
    DECLARE @_Username VARCHAR(255) = @Username
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type
	DECLARE @_ProductLot VARCHAR(255) = @ProductLot
	DECLARE @_DeliveryDate DATETIME = @DeliveryDate

	SELECT @_UserID = ISNULL((SELECT UserID FROM dbo.Users WITH (NOLOCK) WHERE Username = @_Username), 0)
	IF NOT EXISTS(
		SELECT TOP 1 *
		FROM
			dbo.DeliveryHistories ps
		WHERE
			ps.ProductLot = @_ProductLot
			AND (
					--(PalletCode = @_Barcode AND @_Type = 'P') 
					--OR 
					(CartonBarcode = @_Barcode AND @_Type = 'C') 
					OR (ProductBarcode = @_Barcode AND @_Type = 'E')
					OR (EncryptedProductBarcode = @_Barcode AND @_Type = 'E')
				)
	)
	INSERT INTO dbo.DeliveryHistories
	(
		ProductID
		, ProductLot
		, ProductBarcode
	    , EncryptedProductBarcode
	    , CartonBarcode
	    , CompanyCode
		, DeliveryDate
	    , CreatedBy
	    , CreatedDateTime
	    , UpdatedBy
	    , UpdatedDateTime
	)
	SELECT
		ps.ProductID
		, ps.ProductLot
		, ps.ProductBarcode
		, ps.EncryptedProductBarcode
		, ps.CartonBarcode
		, @_CompanyCode
		, @_DeliveryDate
		, @_UserID
		, GETDATE()
		, @_UserID
		, GETDATE()
	FROM
		dbo.PalletStatuses ps
	WHERE
		ps.ProductLot = @_ProductLot
		AND (
				--(PalletCode = @_Barcode AND @_Type = 'P') 
				--OR 
				(CartonBarcode = @_Barcode AND @_Type = 'C') 
				OR (ProductBarcode = @_Barcode AND @_Type = 'E')
				OR (EncryptedProductBarcode = @_Barcode AND @_Type = 'E')
			)

	DELETE FROM dbo.PalletStatuses 
	WHERE
		ProductLot = @_ProductLot
		AND	(
				--(PalletCode = @_Barcode AND @_Type = 'P') 
				--OR 
				(CartonBarcode = @_Barcode AND @_Type = 'C') 
				OR (ProductBarcode = @_Barcode AND @_Type = 'E')
				OR (EncryptedProductBarcode = @_Barcode AND @_Type = 'E')
			)
END