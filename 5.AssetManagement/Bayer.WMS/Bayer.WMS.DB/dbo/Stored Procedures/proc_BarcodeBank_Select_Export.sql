﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_BarcodeBank_Select_Export]
	@Quantity INT,
	@FromSequence INT OUTPUT,
	@ToSequence INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Quantity INT = @Quantity
	DECLARE @_MinYear INT = YEAR(GETDATE()) - 2000-- (SELECT MIN(Year) FROM dbo.BarcodeBank WITH (NOLOCK) WHERE IsExport = 0)
	DECLARE @_MinSequence INT = (SELECT MIN(Sequence) FROM dbo.BarcodeBank WITH (NOLOCK) WHERE IsExport = 0 AND Year = @_MinYear)
	DECLARE @_MaxSequence INT = (SELECT MAX(Sequence) FROM dbo.BarcodeBank WITH (NOLOCK) WHERE IsExport = 0 AND Year = @_MinYear)
	DECLARE @_Rest INT = @_MaxSequence - @_MinSequence + 1

	SET @FromSequence = @_MinSequence
	SET @ToSequence = @_MinSequence + @_Quantity - 1

	IF @_Rest >= @_Quantity
		BEGIN
			SELECT
				Barcode
				, Sequence
			FROM
				dbo.BarcodeBank bb WITH (NOLOCK)
			WHERE
				bb.IsExport = 0
				AND bb.Year = @_MinYear
				AND bb.Sequence BETWEEN @_MinSequence AND @_MinSequence + @_Quantity - 1
		END
	ELSE
		BEGIN
			SELECT
				Barcode
				, Sequence
			FROM
				dbo.BarcodeBank bb WITH (NOLOCK)
			WHERE
				bb.IsExport = 0
				AND bb.Year = @_MinYear
				AND bb.Sequence BETWEEN @_MinSequence AND @_MinSequence + @_Quantity - 1

			UNION ALL

			SELECT
				Barcode
				, Sequence
			FROM
				dbo.BarcodeBank bb WITH (NOLOCK)
			WHERE
				bb.IsExport = 0
				AND bb.Year = @_MinYear + 1
				AND bb.Sequence BETWEEN 1 AND @_Quantity - @_Rest
		END
END