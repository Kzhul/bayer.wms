﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_Generate]
	@PrepareCode VARCHAR(255)
	, @Type VARCHAR(255)
	, @UsageTime INT
	, @StartDate DATE
	, @ExpiryDate DATE
	, @Qty INT
	, @CreateBy INT
	, @CreatedBySitemapID INT
	, @UpdatedBy INT
	, @UpdatedBySitemapID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PrepareCode VARCHAR(255) = @PrepareCode
    DECLARE @_Type VARCHAR(255) = @Type
	DECLARE @_UsageTime INT = @UsageTime
	DECLARE @_StartDate DATE = @StartDate
	DECLARE @_ExpiryDate DATE = @ExpiryDate
	DECLARE @_Qty INT = @Qty
	DECLARE @_CreateBy INT = @CreateBy
	DECLARE @_CreatedBySitemapID INT = @CreatedBySitemapID
	DECLARE @_UpdatedBy INT = @UpdatedBy
	DECLARE @_UpdatedBySitemapID INT = @UpdatedBySitemapID
	DECLARE @_MaxSequence INT

	SET @_MaxSequence = ISNULL((SELECT MAX(Sequence) FROM dbo.Pallets WITH (NOLOCK) WHERE Type = @_Type), 0)

	DECLARE @_i INT = 1
	WHILE @_i <= @_Qty
		BEGIN
			INSERT INTO dbo.Pallets
			(
				PalletCode
			    , Type
			    , StartDate
			    , UsageTime
			    , ExpiryDate
			    , Sequence
				, CapacityStatus
				, Status
			    , IsDeleted
			    , CreatedBy
			    , CreatedBySitemapID
			    , CreatedDateTime
			    , UpdatedBy
			    , UpdatedBySitemapID
			    , UpdatedDateTime
			)
			VALUES
			(
				@_PrepareCode + RIGHT('000000' + CONVERT(VARCHAR(20), @_MaxSequence + @_i), 7)
				, @_Type
				, @_StartDate
				, @_UsageTime
				, @_ExpiryDate
				, @_MaxSequence + @_i
				, 'E'
				, 'N'
				, 0
				, @_CreateBy
				, @_CreatedBySitemapID
				, GETDATE()
				, @_UpdatedBy
				, @_CreatedBySitemapID
				, GETDATE()
			)
			SET @_i = @_i + 1
		END
END