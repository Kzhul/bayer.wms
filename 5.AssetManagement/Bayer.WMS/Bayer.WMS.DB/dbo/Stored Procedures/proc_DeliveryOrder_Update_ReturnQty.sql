﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryOrder_Update_ReturnQty]
	@Barcode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

	SELECT
		pl.PalletCode
		, pl.Status
		, pl.ReferenceNbr
		, pl.CompanyCode
		, ps.ProductID
		, ps.ProductLot
		, ReturnQty = COUNT(ps.ProductBarcode)
	INTO #tmp
	FROM
		dbo.Pallets pl WITH (NOLOCK)
		JOIN dbo.PalletStatuses ps WITH (NOLOCK) ON ps.PalletCode = pl.PalletCode
	WHERE
		pl.PalletCode = @_Barcode
	GROUP BY
		pl.PalletCode
		, pl.Status
		, pl.ReferenceNbr
		, pl.CompanyCode
		, ps.ProductID
		, ps.ProductLot

	DECLARE @_Status CHAR(1)

	SELECT TOP 1 @_Status = Status FROM #tmp

	IF @_Status = 'E'
		UPDATE dnd
		SET
			dnd.ExportReturnedQty = ISNULL(dnd.ExportReturnedQty, 0) + #tmp.ReturnQty
		FROM
			dbo.DeliveryNoteDetails dnd
			JOIN #tmp ON #tmp.ReferenceNbr = dnd.ExportCode
						 AND #tmp.ProductID = dnd.ProductID
						 AND #tmp.ProductLot = dnd.BatchCode
	ELSE IF @_Status = 'R'
		BEGIN
			UPDATE dnd
			SET
				dnd.ReceiveReturnedQty = ISNULL(dnd.ReceiveReturnedQty, 0) + #tmp.ReturnQty
				, dnd.Status =	CASE
									WHEN dnd.Quantity = ISNULL(dnd.ExportedQty, 0) - ISNULL(dnd.ExportReturnedQty, 0) - #tmp.ReturnQty
										THEN 'C'
									ELSE 'I'
								END
			FROM
				dbo.DeliveryNoteDetails dnd
				JOIN #tmp ON #tmp.ReferenceNbr = dnd.ExportCode
							 AND #tmp.ProductID = dnd.ProductID
							 AND #tmp.ProductLot = dnd.BatchCode

			IF EXISTS (SELECT 1 
						FROM
							dbo.DeliveryNoteDetails dnd
							JOIN #tmp ON #tmp.ReferenceNbr = dnd.ExportCode
										 AND #tmp.ProductID = dnd.ProductID
										 AND #tmp.ProductLot = dnd.BatchCode
						WHERE
							dnd.Status != 'C')
				UPDATE dbo.DeliveryNoteHeaders
				SET
					Status = 'I'
				WHERE
					EXISTS (SELECT * FROM #tmp WHERE ReferenceNbr = ExportCode)
		END
	ELSE IF @_Status = 'S'
		BEGIN
			UPDATE snd
			SET
				snd.ReturnedQty = ISNULL(snd.ReturnedQty, 0) + #tmp.ReturnQty
				, snd.Status =	CASE
									WHEN snd.Quantity = ISNULL(snd.ReturnedQty, 0) - ISNULL(snd.ReturnedQty, 0) - #tmp.ReturnQty
										THEN 'C'
									ELSE 'I'
								END
			FROM
				dbo.SplitNoteDetails snd
				JOIN #tmp ON #tmp.ReferenceNbr = snd.SplitCode
							 AND #tmp.ProductID = snd.ProductID
							 AND #tmp.ProductLot = snd.BatchCode
							 AND #tmp.CompanyCode = snd.CompanyCode

			IF EXISTS (SELECT 1 
						FROM
							dbo.SplitNoteDetails snd
							JOIN #tmp ON #tmp.ReferenceNbr = snd.SplitCode
										 AND #tmp.ProductID = snd.ProductID
										 AND #tmp.ProductLot = snd.BatchCode
										 AND #tmp.CompanyCode = snd.CompanyCode
						WHERE
							snd.Status != 'C')
				UPDATE dbo.SplitNoteHeaders
				SET
					Status = 'I'
				WHERE
					EXISTS (SELECT * FROM #tmp WHERE ReferenceNbr = SplitCode)
		END
	ELSE IF @_Status = 'P'
		BEGIN
			UPDATE pnd
			SET
				pnd.ReturnedQty = ISNULL(pnd.ReturnedQty, 0) + #tmp.ReturnQty
				, pnd.Status =	CASE
									WHEN pnd.Quantity = ISNULL(pnd.ReturnedQty, 0) - ISNULL(pnd.ReturnedQty, 0) - #tmp.ReturnQty
										THEN 'C'
									ELSE 'I'
								END
			FROM
				dbo.PrepareNoteDetails pnd
				JOIN #tmp ON #tmp.ReferenceNbr = pnd.PrepareCode
							 AND #tmp.ProductID = pnd.ProductID
							 AND #tmp.ProductLot = pnd.BatchCode

			IF EXISTS (SELECT 1 
						FROM
							dbo.PrepareNoteDetails pnd
							JOIN #tmp ON #tmp.ReferenceNbr = pnd.PrepareCode
										 AND #tmp.ProductID = pnd.ProductID
										 AND #tmp.ProductLot = pnd.BatchCode
						WHERE
							pnd.Status != 'C')
				UPDATE dbo.PrepareNoteHeaders
				SET
					Status = 'I'
				WHERE
					EXISTS (SELECT * FROM #tmp WHERE ReferenceNbr = PrepareCode)
		END
END