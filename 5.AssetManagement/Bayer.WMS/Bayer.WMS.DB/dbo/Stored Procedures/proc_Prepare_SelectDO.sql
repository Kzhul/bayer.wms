﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Prepare_SelectDO]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @DateToGet DATE = CONVERT(DATE, DATEADD(dd,-5,GETDATE()))

	--PRINT (@DateToGet)
	SELECT DISTINCT 
		DOImportCode
		,DeliveryDate = FORMAT(DeliveryDate, 'dd/MM/yyyy')
		,DOQuantity = SUM(DOQuantity)
		,ExportedQty = SUM(ExportedQty)
		,ReceivedQty = SUM(ReceivedQty)
		,PreparedQty = SUM(PreparedQty)
		,DeliveredQty = SUM(DeliveredQty)
	FROM 
		DeliveryOrderSum WITH (NOLOCK)
	WHERE 
		DeliveryDate >= @DateToGet
	GROUP BY
		DOImportCode
		,DeliveryDate
	ORDER BY DOImportCode DESC
END