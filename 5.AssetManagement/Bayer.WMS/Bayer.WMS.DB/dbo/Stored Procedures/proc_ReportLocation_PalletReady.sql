﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--[proc_ReportLocation_PalletReady] 'N'
--[proc_ReportLocation_PalletReady] 'BM'
CREATE PROCEDURE [dbo].[proc_ReportLocation_PalletReady]
	@Type VARCHAR(5)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_Type VARCHAR(5) = @Type
	--Lấy pallet mà thủ kho đã xác nhận, rớt hàng thành phẩn để xuất kho
	IF(@Type = 'DP')
		BEGIN
		SELECT DISTINCT
			 P.PalletCode
			 , [ManufacturingDate] = FORMAT(PP.[ManufacturingDate],'dd/MM/yyyy')
			 , [ExpiryDate] = FORMAT(PP.[ExpiryDate],'dd/MM/yyyy')
			 , PP.ProductID
			 , PD.ProductCode
			 , PD.Description AS ProductName
			 , PP.ProductLot
			 , PP.Note
			 , PP.[PackageSize]
			 , PP.[UOM]
			 , CartonQuantity =  dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'C')
			 , COUNT(DISTINCT PS.ProductBarCode) AS Quantity
			 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
			 , P.LocationCode
			 , LocationSuggestion = 'TE1_T1_T11-01'
			 , ZL.LocationName
			 --, LocationSuggestionName = ZLS.LocationName
			 , P.WarehouseKeeper
			 , P.WarehouseVerifyDate
		INTO #Z
		FROM 
			
			[ProductionPlans] PP WITH (NOLOCK)
			JOIN PalletStatuses PS WITH (NOLOCK)
				ON PP.ProductLot = PS.ProductLot
			JOIN Pallets P WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
			JOIN Products PD WITH (NOLOCK)
				ON PP.ProductID = PD.ProductID
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON P.LocationCode = ZL.LocationCode
			JOIN [DeliveryNoteDetailSplits] DS WITH (NOLOCK) 
				ON PS.PalletCode = ds.PalletCode
		WHERE 
			DS.[Status] = 'B'
			AND DS.[WarehouseConfirmID] > 0
			AND DS.IsDeleted = 0
		GROUP BY
			P.PalletCode
			 , PP.[ManufacturingDate]
			 , PP.[ExpiryDate]
			 , PP.[PackageSize]
			 , PP.[UOM]
			 , PP.ProductID
			 , PD.ProductCode
			 , PD.Description
			 , PP.ProductLot
			 , PP.Note
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
			 , P.WarehouseKeeper
			 , P.WarehouseVerifyDate
	
		SELECT DISTINCT TOP 50
			LocationSuggestionName = ZLS.LocationName
			,B.*		
			,ROW_NUMBER() OVER(ORDER BY B.ProductName
										,B.ProductLot ASC) AS IDS
			,ZLS.LengthID
			,ZLS.LevelID
			,ZLS.LineCode
		FROM 
			#Z AS B
			LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
				ON B.LocationCode = ZLS.LocationCode
		ORDER BY
			ZLS.LengthID DESC
			,ZLS.LevelID DESC
			,ZLS.LineCode
	END
	--Lấy pallet mà thủ kho đã xác nhận, đưa hàng lên kệ
	--Chỉ lấy vị trí từ kho 1
	ELSE IF(@Type = 'C')
		BEGIN
		SELECT DISTINCT TOP 50
			Z.LocationCode
			,ROW_NUMBER() OVER(ORDER BY Z.LocationCode) AS ID
		INTO #A
		FROM
			ZoneLocations Z WITH (NOLOCK)
			JOIN ZoneLines ZL WITH (NOLOCK)
				ON Z.ZoneCode = ZL.ZoneCode
				AND Z.LineCode = ZL.LineCode
			JOIN Zones ZZ WITH (NOLOCK)
				ON Z.ZoneCode = ZZ.ZoneCode
			LEFT JOIN Pallets P WITH (NOLOCK)
				ON Z.LocationCode = P.LocationCode
		WHERE
			ZL.[Type] = 'L'
			AND ZZ.ZoneCode = 'ZW1'
			--AND (P.LocationCode IS NULL or P.LocationCode = '')
			AND (Z.CurrentPalletCode IS NULL or Z.CurrentPalletCode = '')
		

		SELECT DISTINCT TOP 50
			 P.PalletCode
			 , [ManufacturingDate] = FORMAT(PP.[ManufacturingDate],'dd/MM/yyyy')
			 , [ExpiryDate] = FORMAT(PP.[ExpiryDate],'dd/MM/yyyy')
			 , PP.ProductID
			 , PD.ProductCode
			 , PD.Description AS ProductName
			 , PP.ProductLot
			 , PP.Note
			 , PP.[PackageSize]
			 , PP.[UOM]
			 , CartonQuantity =  dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'C')
			 , COUNT(DISTINCT PS.ProductBarCode) AS Quantity
			 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
			 --, LocationSuggestionName = ZLS.LocationName
			 , P.WarehouseKeeper
			 , P.WarehouseVerifyDate
		INTO #B
		FROM 
			[ProductionPlans] PP WITH (NOLOCK)
			JOIN PalletStatuses PS WITH (NOLOCK)
				ON PP.ProductLot = PS.ProductLot
			JOIN Pallets P WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
			JOIN Products PD WITH (NOLOCK)
				ON PP.ProductID = PD.ProductID
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON P.LocationCode = ZL.LocationCode
			--LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
			--	ON P.LocationSuggestion = ZLS.LocationCode
		WHERE 
			PP.[Status] = 'C'
			AND P.Status = 'N'
			--AND ISNULL(P.WarehouseKeeper,0) != 0  --Thủ kho đã xác nhận
			AND P.PalletCode NOT LIKE '%99999'
			--AND (P.LocationCode IS NULL or P.LocationCode = '')
			AND P.WarehouseKeeper > 0
			AND ISNULL(P.Driver,0) = 0
		GROUP BY
			P.PalletCode
			 , PP.[ManufacturingDate]
			 , PP.[ExpiryDate]
			 , PP.[PackageSize]
			 , PP.[UOM]
			 , PP.ProductID
			 , PD.ProductCode
			 , PD.Description
			 , PP.ProductLot
			 , PP.Note
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
			 , P.WarehouseKeeper
			 , P.WarehouseVerifyDate

		--Set pallet location suggest
		UPDATE Pallets
		SET LocationSuggestion = A.LocationCode
		FROM
			#B AS B
			JOIN #A AS A
				ON B.ID= A.ID
		WHERE
			PAllets.PalletCode = B.PalletCode
	
		SELECT DISTINCT TOP 50
			LocationSuggestionName = ZLS.LocationName
			,B.*		
			,ROW_NUMBER() OVER(ORDER BY 
				B.WarehouseVerifyDate DESC
				,B.ProductName
				,B.ProductLot
			) AS IDS
		FROM 
			#B AS B
			LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
				ON B.LocationSuggestion = ZLS.LocationCode
		ORDER BY
			--Điều kiện lưu trữ
			B.WarehouseVerifyDate DESC
			,B.ProductName
			,B.ProductLot
	END
	--Lấy pallet đã sản xuất xong chuẩn bị mang về kho
	ELSE IF(@Type = 'N')
		BEGIN
		SELECT DISTINCT --TOP 1000
			Z.LocationCode
			,ROW_NUMBER() OVER(ORDER BY Z.LocationCode) AS ID
		INTO #C
		FROM
			ZoneLocations Z
			LEFT JOIN Pallets P
				ON Z.LocationCode = P.LocationCode
		WHERE
			(P.LocationCode IS NULL or P.LocationCode = '')
			AND (Z.CurrentPalletCode IS NULL or Z.CurrentPalletCode = '')
		

		SELECT DISTINCT
			 P.PalletCode
			 , [ManufacturingDate] = FORMAT(PP.[ManufacturingDate],'dd/MM/yyyy')
			 , [ExpiryDate] = FORMAT(PP.[ExpiryDate],'dd/MM/yyyy')
			 , PP.ProductID
			 , PD.ProductCode
			 , PD.Description AS ProductName
			 , PP.ProductLot
			 , PP.Note
			 , PP.[PackageSize]
			 , PP.[UOM]
			 , CartonQuantity =  dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'C')
			 , COUNT(DISTINCT PS.ProductBarCode) AS Quantity
			 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
			 , PP.PackagingStartTime
			 , PP.PackagingEndTime
		INTO #D
		FROM 
			[ProductionPlans] PP WITH (NOLOCK)
			JOIN PalletStatuses PS WITH (NOLOCK)
				ON PP.ProductLot = PS.ProductLot
			JOIN Pallets P WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
			JOIN Products PD WITH (NOLOCK)
				ON PP.ProductID = PD.ProductID
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON P.LocationCode = ZL.LocationCode
			--LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
			--	ON P.LocationSuggestion = ZLS.LocationCode
		WHERE 
			PP.[Status] = 'C'
			AND P.Status = 'N'
			AND ISNULL(P.WarehouseKeeper,0) = 0  --Thủ kho chưa xác nhận
			AND P.PalletCode NOT LIKE '%99999'
			--AND (P.LocationCode IS NULL or P.LocationCode = '')
		GROUP BY
			P.PalletCode
			 , PP.[ManufacturingDate]
			 , PP.PackagingStartTime
			 , PP.PackagingEndTime
			 , PP.[ExpiryDate]
			 , PP.[PackageSize]
			 , PP.[UOM]
			 , PP.ProductID
			 , PD.ProductCode
			 , PD.Description
			 , PP.ProductLot
			 , PP.Note
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
			 --, ZLS.LocationName

		--Set pallet location suggest
		UPDATE Pallets
		SET LocationSuggestion = A.LocationCode
		FROM
			#D AS B
			JOIN #C AS A
				ON B.ID= A.ID
		WHERE
			PAllets.PalletCode = B.PalletCode
	
		SELECT DISTINCT TOP 50
			LocationSuggestionName = ZLS.LocationName
			,B.*		
			,ROW_NUMBER() OVER(ORDER BY B.PackagingEndTime DESC
										,B.ProductName ASC
										,B.ProductLot ASC) AS IDS
		FROM 
			#D AS B
			LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
				ON B.LocationSuggestion = ZLS.LocationCode
		ORDER BY
			--Điều kiện lưu trữ
			B.PackagingEndTime DESC
			,B.ProductName ASC
			,B.ProductLot ASC
	END
	--Lấy pallet nguyên liệu thủ kho đã xác nhận và đưa lên kệ
	ELSE IF(@Type = 'B')
	BEGIN
		--Lấy danh sách vị trí trống
		SELECT DISTINCT --TOP 1000
			Z.LocationCode
			,ROW_NUMBER() OVER(ORDER BY Z.LocationCode) AS ID
		INTO #E
		FROM
			ZoneLocations Z
			LEFT JOIN Pallets P
				ON Z.LocationCode = P.LocationCode
		WHERE
			(P.LocationCode IS NULL or P.LocationCode = '')
			AND (Z.CurrentPalletCode IS NULL or Z.CurrentPalletCode = '')
		
		--Danh sách pallet mà thủ kho chưa xác nhận
		SELECT DISTINCT
			 P.PalletCode
			 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
		INTO #F
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			JOIN Pallets P WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
			JOIN Products PD WITH (NOLOCK)
				ON PS.ProductID = PD.ProductID
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON P.LocationCode = ZL.LocationCode
			JOIN [StockReceivingDetailSplits] SRP WITH (NOLOCK)
				ON P.PalletCode = SRP.PalletCode
		WHERE 
			ISNULL(SRP.[ForkliftID],0) = 0  --Xe nâng chưa chất hàng
			AND P.PalletCode NOT LIKE '%99999'

		--Set pallet location suggest
		UPDATE Pallets
		SET LocationSuggestion = A.LocationCode
		FROM
			#F AS B
			JOIN #E AS A
				ON B.ID= A.ID
		WHERE
			PAllets.PalletCode = B.PalletCode

		--Danh sách pallet mà thủ kho chưa xác nhận
		SELECT DISTINCT TOP 50
			 P.PalletCode
			 , PS.ProductID
			 , PD.ProductCode
			 , PD.Description AS ProductName
			 , SRP.ProductLot
			 , CartonQuantity = ''
			 , Quantity = PS.Qty
			 --, IDS = 0
			 --, ROW_NUMBER() OVER(ORDER BY SRH.[WMApproveImportDate] DESC
				--						,PD.Description ASC
				--						,SRP.ProductLot ASC) AS IDS
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
			 , LocationSuggestionName = ZLS.LocationName
			 , P.Status
			 , P.ReferenceNbr
			 , SRH.[WMApproveImportDate]
			 , FORMAT(SRH.[WMApproveImportDate],'dd/MM HH:mm') AS StrTime
		INTO #G
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			JOIN Pallets P WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
			JOIN Products PD WITH (NOLOCK)
				ON PS.ProductID = PD.ProductID
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON P.LocationCode = ZL.LocationCode
			LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
				ON P.LocationSuggestion = ZLS.LocationCode
			JOIN [StockReceivingDetailSplits] SRP WITH (NOLOCK)
				ON P.PalletCode = SRP.PalletCode
			JOIN StockReceivingHeaders SRH WITH (NOLOCK)
				ON SRP.StockReceivingCode = SRH.StockReceivingCode				
		WHERE 
			ISNULL(SRP.[ForkliftID],0) = 0  --Xe nâng chưa chất hàng
			AND SRH.WarehouseManagerUserID > 0 --Thủ kho đã bấm nút duyệt chất hàng lên kệ
			AND P.PalletCode NOT LIKE '%99999'


		SELECT DISTINCT 
			*
			, ROW_NUMBER() OVER(ORDER BY PD.[WMApproveImportDate] DESC
										,PD.ProductName ASC
										,PD.ProductLot ASC) AS IDS
		FROM #G AS PD
		ORDER BY PD.[WMApproveImportDate] DESC
				,PD.ProductName ASC
				,PD.ProductLot ASC
	END
	--Lấy pallet nguyên liệu từ trên kệ để chuyển xuống sản xuất
	ELSE IF(@Type = 'BM')
	BEGIN
		Update Pallets
		SET LocationSuggestion = 'TO2_T1_T11-01'
		FROM 
			Pallets P WITH (NOLOCK)
			JOIN [RequestMaterialLineSplits] S WITH (NOLOCK)
				ON P.PalletCode = S.PalletCode
		WHERE 
			P.Status = 'B'

		SELECT DISTINCT
			 P.PalletCode
			 , PS.ProductID
			 , PD.ProductCode
			 , PD.Description AS ProductName
			 , PS.ProductLot
			 , COUNT(DISTINCT PS.CartonBarcode) AS CartonQuantity
			 , COUNT(DISTINCT PS.ProductBarCode) AS Quantity
			 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS IDS
			 , P.LocationCode
			 , ZL.LocationName
			 --Suggest là vị trí tạm của kho 2: TO2
			 , LocationSuggestion = P.LocationCode--'TO2_T1_T11-01'
			 , LocationSuggestionName = ZL.LocationName--'A1-01'
			 , FORMAT(S.WarehouseVerifyAllowDropTime,'dd/MM HH:mm') AS StrTime
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			JOIN Pallets P WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
			JOIN Products PD WITH (NOLOCK)
				ON PS.ProductID = PD.ProductID
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON P.LocationCode = ZL.LocationCode
			JOIN [RequestMaterialLineSplits] S WITH (NOLOCK)
				ON P.PalletCode = S.PalletCode
		WHERE 
			P.Status = 'B'
		GROUP BY
			P.PalletCode
			 , PS.ProductID
			 , PD.ProductCode
			 , PD.Description
			 , PS.ProductLot
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
			 , S.WarehouseVerifyAllowDropTime
	END
	--Lấy pallet nguyên liệu từ trên kệ để chuyển xuống sản xuất
	ELSE IF(@Type = 'BM')
	BEGIN
		Update Pallets
		SET LocationSuggestion = 'TO2_T1_T11-01'
		FROM 
			Pallets P WITH (NOLOCK)
			JOIN [RequestMaterialLineSplits] S WITH (NOLOCK)
				ON P.PalletCode = S.PalletCode
		WHERE 
			P.Status = 'B'

		SELECT DISTINCT
			 P.PalletCode
			 , PS.ProductID
			 , PD.ProductCode
			 , PD.Description AS ProductName
			 , PS.ProductLot
			 , 0 AS CartonQuantity
			 , SUM(ISNULL(PS.Qty,1)) AS Quantity
			 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
			 , P.LocationCode
			 , ZL.LocationName
			 --Suggest là vị trí tạm của kho 2: TO2
			 , LocationSuggestion = 'TO2_T1_T11-01'
			 , LocationSuggestionName = 'A1-01'
			 , FORMAT(S.[WarehouseVerifyAllowDropTime],'dd/MM HH:mm') AS StrTime
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			JOIN Pallets P WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
			JOIN Products PD WITH (NOLOCK)
				ON PS.ProductID = PD.ProductID
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON P.LocationCode = ZL.LocationCode
			JOIN [RequestMaterialLineSplits] S WITH (NOLOCK)
				ON P.PalletCode = S.PalletCode
		WHERE 
			P.Status = 'B'
		GROUP BY
			P.PalletCode
			 , PS.ProductID
			 , PD.ProductCode
			 , PD.Description
			 , PS.ProductLot
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
			 , S.[WarehouseVerifyAllowDropTime]
	END
	--Lấy pallet nguyên liệu thủ kho đã xác nhận đưa xuống SX
	ELSE IF(@Type = 'MM')
	BEGIN
		Update Pallets
		SET LocationSuggestion = 'TO2_T1_T11-01'
		FROM Pallets P WITH (NOLOCK)
			JOIN [RequestMaterialLineSplits] S WITH (NOLOCK)
				ON P.PalletCode = S.PalletCode
		WHERE 
			P.Status = 'P'
			OR P.Status = 'C'

		SELECT DISTINCT
			 P.PalletCode
			 , PS.ProductID
			 , PD.ProductCode
			 , PD.Description AS ProductName
			 , PS.ProductLot
			 , 0 AS CartonQuantity
			 , SUM(ISNULL(PS.Qty,1)) AS Quantity
			 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
			 , P.LocationCode
			 , ZL.LocationName
			 --Suggest là vị trí tạm của kho SX: TO2
			 , LocationSuggestion = 'TO2_T1_T11-01'
			 , LocationSuggestionName = 'A1-01'
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			JOIN Pallets P WITH (NOLOCK)
				ON PS.PalletCode = P.PalletCode
			JOIN Products PD WITH (NOLOCK)
				ON PS.ProductID = PD.ProductID
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON P.LocationCode = ZL.LocationCode
			JOIN [RequestMaterialLineSplits] S WITH (NOLOCK)
				ON P.PalletCode = S.PalletCode
		WHERE 
			P.Status = 'P'
			OR P.Status = 'C'
		GROUP BY
			P.PalletCode
			 , PS.ProductID
			 , PD.ProductCode
			 , PD.Description
			 , PS.ProductLot
			 , P.LocationCode
			 , P.LocationSuggestion
			 , ZL.LocationName
	END
END