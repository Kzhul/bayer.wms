﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_AuditMaterial2]
	@PalletCode VARCHAR(255)
	, @UserID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_Date DATETIME = GETDATE()

	UPDATE dbo.Pallets
	SET
		UpdatedBy = @_UserID
		, UpdatedBySitemapID = 0
		, UpdatedDateTime = @_Date
		, WarehouseVerifyNote = 'OK'
	WHERE
		PalletCode = @_PalletCode
END