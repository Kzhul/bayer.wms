﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_UpdateVerifyStatus]
	@PalletCode VARCHAR(255)
	, @WarehouseVerifyStatus NVARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_Status NVARCHAR(255) = @WarehouseVerifyStatus
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()

    UPDATE dbo.Pallets
	SET
		WarehouseVerifyStatus = @_Status
		, [WarehouseVerifyNote] = CASE WHEN @_Status = 'OK' THEN 'OK'
										WHEN @_Status = 'WrongLotQuantity' THEN N'Sai số lô, số lượng'
										WHEN @_Status = 'WrongUpdateLater' THEN N'Cần xem lại sau'
										WHEN @_Status = 'MustPrintPallet' THEN N'Cần dán lại nhãn pallet'
										ELSE NULL END
		, WarehouseKeeper = @_UserID
		, WarehouseVerifyDate = @_Date
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode	
END