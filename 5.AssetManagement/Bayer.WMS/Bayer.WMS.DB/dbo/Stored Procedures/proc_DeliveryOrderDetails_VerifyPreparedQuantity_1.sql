﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity]
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_VerifyPreparedQuantity]
	@CompanyCode VARCHAR(255)
	,@BarCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_BarCode VARCHAR(255) = @BarCode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_ProductLot VARCHAR(255) = (SELECT TOP 1 ProductLot FROM PalletStatuses WITH (NOLOCK) WHERE CartonBarcode = @_BarCode OR ProductBarcode = @_BarCode)
	DECLARE @_ProductID VARCHAR(255) = (SELECT TOP 1 ProductID FROM PalletStatuses WITH (NOLOCK) WHERE CartonBarcode = @_BarCode OR ProductBarcode = @_BarCode)
	DECLARE @_PreparedQuantity INT = (
		SELECT DISTINCT
			COUNT(DISTINCT ps.ProductBarcode)	
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			LEFT JOIN Pallets p WITH (NOLOCK)
				ON ps.PalletCode = p.PalletCode
		WHERE
			CompanyCode = @_CompanyCode
			AND PS.ProductLot = @_ProductLot
			AND PS.ProductID = @_ProductID
		GROUP BY 
			P.CompanyCode 
			,PS.ProductLot
			,PS.ProductID
	)

	DECLARE @_DOQuantity INT = (
		SELECT DISTINCT
			SUM(ps.Quantity)	
		FROM 
			DeliveryOrderDetails PS WITH (NOLOCK)
		WHERE
			CompanyCode = @_CompanyCode
			AND PS.BatchCode = @_ProductLot
			AND PS.ProductID = @_ProductID
		GROUP BY 
			PS.CompanyCode 
			,PS.BatchCode
			,PS.ProductID
	)

	SELECT TOP 1
		DO.CompanyCode
		, C.CompanyName
		, DO.DOImportCode
		, ProductLot = @_ProductLot
		, ProductCode = P.ProductCode
		, ProductName = P.Description
		, DOQuantity = @_DOQuantity
		, PreparedQuantity = @_PreparedQuantity
	FROM 
		DeliveryOrderDetails DO WITH (NOLOCK)
		LEFT JOIN Companies C WITH (NOLOCK)
			ON DO.CompanyCode = C.CompanyCode
		LEFT JOIN Products P WITH (NOLOCK)
			ON DO.ProductID = P.ProductID
	WHERE
		DO.CompanyCode = @_CompanyCode
		AND DO.BatchCode = @_ProductLot
		AND DO.ProductID = @_ProductID
		AND DATEDIFF(DD,DO.DeliveryDate,GETDATE()) >= 0
		AND @_PreparedQuantity >= @_DOQuantity
END