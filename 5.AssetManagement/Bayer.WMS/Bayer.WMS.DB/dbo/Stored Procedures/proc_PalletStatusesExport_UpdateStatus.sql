﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatusesExport_UpdateStatus]
	@PalletCode VARCHAR(255)
	, @UserID INT
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_Date DATETIME = GETDATE()

	Update [dbo].[PalletStatusesExport]
	SET Received = 1
		,UpdatedDateTime = @_Date
		,UpdatedBy = @_UserID
	WHERE
		PalletCode = @_PalletCode
END