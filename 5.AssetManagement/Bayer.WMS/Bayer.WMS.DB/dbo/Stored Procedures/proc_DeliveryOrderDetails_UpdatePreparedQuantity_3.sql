﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity]
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_UpdatePreparedQuantity]
	@PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_CompanyCode VARCHAR(255) = ISNULL((SELECT TOP 1 CompanyCode FROM Pallets WITH (NOLOCK) WHERE PalletCode = @_PalletCode),'')
	DECLARE @_DOImportCode VARCHAR(255) = ISNULL((SELECT TOP 1 DOImportCode FROM Pallets WITH (NOLOCK) WHERE PalletCode = @_PalletCode),'')
	IF(@_CompanyCode != '')
	BEGIN
		SELECT DISTINCT
			P.CompanyCode 
			,PS.ProductLot
			,PS.ProductID
			,PreparedQuantity = ISNULL(COUNT(DISTINCT ps.ProductBarcode),0)	
		INTO #A
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			LEFT JOIN Pallets p WITH (NOLOCK)
				ON ps.PalletCode = p.PalletCode
		WHERE
			P.CompanyCode = @_CompanyCode
			AND P.DOImportCode = @_DOImportCode
		GROUP BY 
			P.CompanyCode 
			,PS.ProductLot
			,PS.ProductID

		IF EXISTS (SELECT * FROM #A)
		BEGIN
			UPDATE DeliveryOrderSum
			SET [PreparedQty] = ISNULL(A.PreparedQuantity,0) + ISNULL(DO.DeliveredQty,0)
			FROM
				DeliveryOrderSum DO
				LEFT JOIN #A AS A
					ON DO.BatchCode = A.ProductLot
					AND DO.CompanyCode = A.CompanyCode
					AND DO.ProductID = A.ProductID
			WHERE
				DO.CompanyCode = @_CompanyCode
				AND DO.DOImportCode = @_DOImportCode
		END
	END
END