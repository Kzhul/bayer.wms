﻿CREATE PROCEDURE [dbo].[proc_Barcode_Insert]
	@BarCode NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO [dbo].[_BarCodeTest]
           ([Barcode])
	VALUES
		   (@BarCode)
END