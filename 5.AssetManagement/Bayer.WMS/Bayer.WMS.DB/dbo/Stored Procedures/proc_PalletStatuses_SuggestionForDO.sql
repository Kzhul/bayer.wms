﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_PalletStatuses_SuggestionForDO] 'DO17120902'
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SuggestionForDO]
	@DOImportCode VARCHAR(255)
	,@Search NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_Search NVARCHAR(255) = @Search

	SELECT DISTINCT
		DO.ProductID
		,P.ProductCode
		,P.Description AS ProductName
		,DO.BatchCode	
		,DH.ExportDate AS DeliveryDate
		,DOQuantity = SUM(DO.Quantity)
	INTO #DO
	FROM 
		DeliveryNoteDetails DO WITH(NOLOCK)
		LEFT JOIN DeliveryNoteHeaders DH WITH(NOLOCK)
			ON DO.DOCode = DH.DOImportCode
		LEFT JOIN Products P WITH(NOLOCK)
			ON DO.ProductID = P.ProductID
	WHERE 
		DO.DOCode = @_DOImportCode
	GROUP BY
		DO.ProductID
		,P.ProductCode
		,P.Description
		,DO.BatchCode	
		,DH.ExportDate

	CREATE CLUSTERED INDEX #idx_DO ON #DO(ProductID, ProductCode, BatchCode, DeliveryDate)  

	SELECT DISTINCT
		DO.ProductID
		,DO.BatchCode	
	INTO #A
	FROM 
		#DO AS DO

	CREATE CLUSTERED INDEX #idx_A ON #A(ProductID, BatchCode)  
	SELECT DISTINCT 
		TB.ProductID
		,ProductLot = TB.BatchCode
		,PalletCode = ISNULL(PS.PalletCode,'')
		,LocationCode = ISNULL(P.LocationCode,'')
		,COUNT(DISTINCT PS.ProductBarcode) AS Quantity
	INTO #SuggestTB
	FROM
		#A AS TB
		LEFT JOIN PalletStatuses PS WITH(NOLOCK)
			ON TB.ProductID = PS.ProductID
			AND TB.BatchCode = PS.ProductLot
		LEFT JOIN Pallets P WITH(NOLOCK)
			ON PS.PalletCode = P.PalletCode
	GROUP BY
		TB.ProductID
		,TB.BatchCode
		,PS.PalletCode
		,P.LocationCode
	CREATE CLUSTERED INDEX #idx_SuggestTB ON #SuggestTB(ProductID, ProductLot, PalletCode)  

	SELECT DISTINCT
		DO.ProductCode
		,DO.ProductName
		,TB.ProductLot
		,DO.DeliveryDate	
		,TB.PalletCode
		,TB.LocationCode
		,DO.DOQuantity
		,TB.Quantity AS PalletQuantity
		,ZL.LocationName
		,ZLL.LineName
		,Z.ZoneName
		,ZL.LengthID
		,ZL.LevelID
	FROM
		#DO AS DO
		LEFT JOIN #SuggestTB TB
			ON DO.BatchCode = TB.ProductLot
			AND DO.ProductID = TB.ProductID
		LEFT JOIN ZoneLocations ZL WITH (NOLOCK)
			ON ZL.LocationCode = TB.LocationCode
		LEFT JOIN ZoneLines ZLL WITH (NOLOCK)
			ON ZL.LineCode = ZLL.LineCode
		LEFT JOIN Zones Z WITH (NOLOCK)
			ON Z.ZoneCode = ZL.ZoneCode
	WHERE
		TB.PalletCode NOT IN (
			SELECT DISTINCT 
				DD.PalletCode
			FROM 
				DeliveryNoteDetailSplits DD
			WHERE
				DD.DOImportCode = @_DOImportCode
				AND DD.IsDeleted = 0
		)
		AND 
		(
			(@_Search = '' OR TB.ProductLot LIKE '%' + @_Search+ '%')
			OR (@_Search = '' OR DO.ProductName LIKE N'%' + @_Search+ '%')
			OR (@_Search = '' OR TB.LocationCode LIKE '%' + @_Search+ '%')
			OR (@_Search = '' OR ZL.LocationName LIKE '%' + @_Search+ '%')
			OR (@_Search = '' OR DO.ProductCode LIKE '%' + @_Search+ '%')
			OR (@_Search = '' OR TB.PalletCode LIKE '%' + @_Search+ '%')
		)
	ORDER BY 
		DO.ProductName
		,TB.ProductLot
END