﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_MoveMaterial]
	@PalletSource VARCHAR(255)
	, @PalletDestination VARCHAR(255)
	, @ProductLot VARCHAR(255)
	, @Quantity INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletSource
    DECLARE @_PalletDestination VARCHAR(255) = @PalletDestination
	DECLARE @_ProductLot VARCHAR(255) = @ProductLot
	DECLARE @_Quantity INT = @Quantity

--Tăng cho pallet đích
	IF NOT EXISTS (
		SELECT * 
		FROM 
			dbo.PalletStatuses
		WHERE
			PalletCode = @PalletDestination
			AND ProductLot = @_ProductLot
	)
	BEGIN
		INSERT INTO [dbo].[PalletStatuses]
           ([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
     SELECT
		[PalletCode] = @_PalletDestination
        ,[CartonBarcode] = ''
        ,[ProductBarcode] = ''
        ,[EncryptedProductBarcode] = NULL
        ,[ProductID] = (
							SELECT TOP 1 ProductID 
							FROM PalletStatuses WITH (NOLOCK)
							WHERE PalletCode = @PalletSource
								AND ProductLot = @_ProductLot
						)
        ,[ProductLot] = @ProductLot
        ,[Qty] = @_Quantity
        ,[CreatedBy] = 1
        ,[CreatedBySitemapID] = 1
        ,[CreatedDateTime] = GETDATE()
        ,[UpdatedBy] = 1
        ,[UpdatedBySitemapID] = 1
        ,[UpdatedDateTime] = GETDATE()
	END
	ELSE
	BEGIN
		UPDATE dbo.PalletStatuses
		SET
			Qty = ISNULL(Qty,0) + @Quantity
		WHERE
			PalletCode = @PalletDestination
			AND ProductLot = @_ProductLot
	END

--Giảm pallet nguồn
	UPDATE dbo.PalletStatuses
	SET
		Qty = ISNULL(Qty,0) - @Quantity
	WHERE
		PalletCode = @PalletSource
		AND ProductLot = @_ProductLot

	DELETE FROM dbo.PalletStatuses
	WHERE
		PalletCode = @PalletSource
		AND ProductLot = @_ProductLot
		AND ISNULL(Qty,0) = 0
END