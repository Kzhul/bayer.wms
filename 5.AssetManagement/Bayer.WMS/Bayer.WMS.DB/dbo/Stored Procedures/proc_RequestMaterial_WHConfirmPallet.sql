﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_WHConfirmPallet]
	@DocumentNbr NVARCHAR(50)
	,@PalletCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_PalletCode NVARCHAR(50) = @PalletCode

	UPDATE Pallets 
	SET 
		[Status] = 'C' --Warehouse confirm
		, ReferenceNbr = @_DocumentNbr
	WHERE
		PalletCode = @_PalletCode

	UPDATE [RequestMaterialLineSplits]
	SET [Status] = 'C'
	WHERE
		PalletCode = @_PalletCode
		AND DocumentNbr = @_DocumentNbr
		
	UPDATE [dbo].[RequestMaterialLines]
	SET 
		TransferedQty = 
		(
			SELECT
				SUM(RS.[Quantity])
			FROM
				[RequestMaterialLines] RML
				JOIN [dbo].[RequestMaterialLineSplits] RS
					ON RML.DocumentNbr = RS.DocumentNbr
					AND RML.ProductID = RS.ProductID
			WHERE
				RML.DocumentNbr = @_DocumentNbr
				AND RS.[Status] = 'C'
				AND [RequestMaterialLines].ProductID = RML.ProductID				
		)
	WHERE
		DocumentNbr = @_DocumentNbr

END