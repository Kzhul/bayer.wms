﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ReturnDO_Update_ExportReturnQty]
	@ReturnDOCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
	, @ProductID INT
	, @BatchCode VARCHAR(255)
	, @ExportedReturnQty DECIMAL(18, 2)
	, @ReferenceNbr VARCHAR(255) = NULL
	, @CompanyCode VARCHAR(255) = NULL
	, @Status CHAR(1) = NULL
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @_ReturnDOCode VARCHAR(255) = @ReturnDOCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_BatchCode VARCHAR(255) = @BatchCode
	DECLARE @_ExportedReturnQty DECIMAL(18, 2) = @ExportedReturnQty
	DECLARE @_ReferenceNbr VARCHAR(255) = @ReferenceNbr
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_Status CHAR(1) = @Status
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

    UPDATE dbo.ReturnDODetails
	SET
		ExportedReturnQty = @_ExportedReturnQty
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		ReturnDOCode = @_ReturnDOCode
		AND ProductID = @_ProductID
		AND BatchCode = @_BatchCode
	
	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ReturnDODetail">' 
		+ (SELECT * FROM dbo.ReturnDODetails dnd WHERE dnd.ReturnDOCode = @_ReturnDOCode AND dnd.ProductID = @_ProductID AND dnd.BatchCode = @_BatchCode FOR XML PATH(''))
		+ '</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'ReturnDODetails', @_Data, 'UPD', @_Method, @_Date, @_Date

	IF @_Status = 'E'
		UPDATE dnd
		SET
			dnd.ExportReturnedQty = ISNULL(dnd.ExportReturnedQty, 0) + @_ExportedReturnQty
		FROM
			dbo.DeliveryNoteDetails dnd
		WHERE
			dnd.ExportCode = @_ReferenceNbr
			AND dnd.ProductID = @_ProductID
			AND dnd.BatchCode = @_BatchCode
	ELSE IF @_Status = 'R'
		UPDATE dnd
		SET
			dnd.ReceiveReturnedQty = ISNULL(dnd.ReceiveReturnedQty, 0) + @_ExportedReturnQty
		FROM
			dbo.DeliveryNoteDetails dnd
		WHERE
			dnd.ExportCode = @_ReferenceNbr
			AND dnd.ProductID = @_ProductID
			AND dnd.BatchCode = @_BatchCode
	ELSE IF @_Status = 'S'
		UPDATE snd
		SET
			snd.ReturnedQty = ISNULL(snd.ReturnedQty, 0) + @_ExportedReturnQty
		FROM
			dbo.SplitNoteDetails snd
		WHERE
			snd.SplitCode = @_ReferenceNbr
			AND snd.CompanyCode = @_CompanyCode
			AND snd.ProductID = @_ProductID
			AND snd.BatchCode = @_BatchCode
	ELSE IF @_Status = 'P'
		UPDATE pnd
		SET
			pnd.ReturnedQty = ISNULL(pnd.ReturnedQty, 0) + @_ExportedReturnQty
		FROM
			dbo.PrepareNoteDetails pnd
		WHERE
			pnd.PrepareCode = @_ReferenceNbr
			AND pnd.ProductID = @_ProductID
			AND pnd.BatchCode = @_BatchCode
END