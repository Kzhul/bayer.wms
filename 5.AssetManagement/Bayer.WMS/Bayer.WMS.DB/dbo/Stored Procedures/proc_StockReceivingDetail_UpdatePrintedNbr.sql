﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceivingDetail_UpdatePrintedNbr]
	@DocumentNbr NVARCHAR(50)
	,@BatchCode NVARCHAR(50)
	,@BatchCodePrintedNbr INT
	,@UserID INT
	,@MethodID NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_BatchCode NVARCHAR(50) = @BatchCode
	DECLARE @_BatchCodePrintedNbr INT = @BatchCodePrintedNbr
	DECLARE @_UserID INT = @UserID
	DECLARE @_MethodID NVARCHAR(250) = @MethodID
	DECLARE @_ActionDate DATETIME = GETDATE()
	
	UPDATE [dbo].[StockReceivingDetails]
	SET 
		BatchCodePrintedNbr = ISNULL(@_BatchCodePrintedNbr,0)
		,[UpdatedBy] = @_UserID
		,[UpdatedDateTime] = @_ActionDate
	WHERE
		[StockReceivingCode] = @_DocumentNbr
		AND BatchCode = @_BatchCode
END