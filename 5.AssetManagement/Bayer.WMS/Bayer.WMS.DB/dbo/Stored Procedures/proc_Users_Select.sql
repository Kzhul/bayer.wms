﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Users_Select]
	@Username VARCHAR(255)
	, @Status CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Username VARCHAR(255) = @Username
	DECLARE @_Status CHAR(1) = @Status

    SELECT
		*
	FROM
		dbo.Users u WITH (NOLOCK)
	WHERE
		u.Username = @_Username
		AND u.Status = @_Status
		AND u.IsDeleted = 0
END