﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_ReportProcess]
	@DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	SELECT DISTINCT 
		DO.* 
		,P.ProductCode
		,C.CompanyName 
		,P.Description AS ProductName 
		,CONVERT(INT,PPS.Quantity) AS PackSize
		,NeedPrepareQty = [DOQuantity] - [PreparedQty]
		,NeedDeliveryQty = [DOQuantity] - [DeliveredQty]
	FROM 
		DeliveryOrderSum DO WITH (NOLOCK)
		LEFT JOIN Products P WITH (NOLOCK)
			ON DO.ProductID = P.ProductID
		LEFT JOIN [dbo].[ProductPackings] PPS WITH (NOLOCK)
			ON P.ProductID = PPS.ProductID
			AND PPS.Type != 'P'
		LEFT JOIN Companies C WITH (NOLOCK)
			ON DO.CompanyCode = C.CompanyCode
	WHERE 
		DOImportCode = @_DOImportCode
		AND DO.DOQuantity > 0
	ORDER BY 
		CompanyName
		,P.Description
		,BatchCode
END