﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_Print]
	@PalletCode VARCHAR(255)
	,@UpdatedBy INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UpdatedBy VARCHAR(255) = @UpdatedBy

	UPDATE
		dbo.Pallets
	SET
		PrintDate = GETDATE()
		, UserPrint = @_UpdatedBy
		, PrintedTime = ISNULL(PrintedTime,0) + 1
	WHERE
		PalletCode = @_PalletCode
END