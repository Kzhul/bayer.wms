﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_SplitNote_Select]
	@SplitCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_SplitNote VARCHAR(255) = @SplitCode

	SELECT
		tmp.SplitCode
		, tmp.DOImportCode
		, tmp.Description
		, tmp.DeliveryDate
		, tmp.BatchCode
		, tmp.Quantity
		, tmp.PackQuantity
		, tmp.ProductQuantity
		, tmp.CompanyCode
		, tmp.CompanyName
		, tmp.SplittedQty
		, tmp.ReturnedQty
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
	FROM
		(
			SELECT
				snh.SplitCode
				, snh.DOImportCode
				, snh.Description
				, snh.DeliveryDate
				, snd.BatchCode
				, snd.ProductID
				, Quantity = SUM(snd.Quantity)
				, PackQuantity = SUM(snd.PackQuantity)
				, ProductQuantity = SUM(snd.ProductQuantity)
				, snd.CompanyCode
				, snd.CompanyName
				, SplittedQty = SUM(ISNULL(snd.SplittedQty, 0) - ISNULL(snd.ReturnedQty, 0))
				, ReturnedQty = SUM(ISNULL(snd.ReturnedQty, 0))
			FROM
				dbo.SplitNoteHeaders snh WITH (NOLOCK)
				JOIN dbo.SplitNoteDetails snd WITH (NOLOCK) ON snd.SplitCode = snh.SplitCode
			WHERE
				snh.SplitCode = @_SplitNote
			GROUP BY
				snh.SplitCode
				, snh.DOImportCode
				, snh.Description
				, snh.DeliveryDate
				, snd.BatchCode
				, snd.ProductID
				, snd.CompanyCode
				, snd.CompanyName
		)tmp
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = tmp.ProductID
	ORDER BY
		p.Description
END