﻿CREATE PROCEDURE [dbo].[proc_ProductReturn_VerifyPreparedQuantity]
	@DOImportCode VARCHAR(255)
	,@BarCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_BarCode VARCHAR(255) = @BarCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	SELECT DISTINCT 
		ProductID
		,ProductLot
		,Qty = SUM(ISNULL(Qty,1))
	INTO #PS
	FROM 
		PalletStatuses PS WITH (NOLOCK) 
		JOIN Pallets P WITH (NOLOCK) 
			ON PS.PalletCode = P.PalletCode
	WHERE 
		(
			PS.CartonBarcode = @_BarCode 
			OR PS.ProductBarcode = @_BarCode 
			OR PS.EncryptedProductBarcode = @_BarCode
			OR PS.PalletCode = @_BarCode 
		)
	GROUP BY
		ProductID
		,ProductLot

	SELECT DISTINCT 
		DO.ProductID
		--,ProductLot = DO.BatchCode
		,ReturnQty = SUM(DO.ReturnQty)
		,DeliveredQty = SUM(DO.DeliveredQty)
		,NeedQty = ISNULL(SUM(DO.ReturnQty),0) - ISNULL(SUM(DO.DeliveredQty),0)
		,NeedQtyExcludePallet = ISNULL(SUM(DO.ReturnQty),0) - ISNULL(SUM(DO.DeliveredQty),0) + ISNULL(PS.Qty,0)
	INTO #DO
	FROM 
		ProductReturnDetails DO WITH (NOLOCK) 
		LEFT JOIN 
		(
			SELECT DISTINCT 
				ProductID
				,ProductLot
				,Qty = SUM(ISNULL(Qty,1))
			FROM 
				PalletStatuses PS WITH (NOLOCK) 
				JOIN Pallets P WITH (NOLOCK) 
					ON PS.PalletCode = P.PalletCode
			WHERE 
				(
					PS.CartonBarcode = @_BarCode 
					OR PS.ProductBarcode = @_BarCode 
					OR PS.EncryptedProductBarcode = @_BarCode
					OR PS.PalletCode = @_BarCode 
				)
				AND P.DOImportCode = @DOImportCode
			GROUP BY
				ProductID
				,ProductLot
		) AS PS
		ON DO.ProductID = PS.ProductID
			AND DO.BatchCode = PS.ProductLot
	WHERE 
		ProductReturnCode = @DOImportCode
	GROUP BY
		DO.ProductID
		,ISNULL(PS.Qty,0)

	--SELECT DISTINCT
	--	PS.ProductLot
	--	, ProductCode = P.ProductCode
	--	, ProductName = P.Description
	--	, DOQuantity = DO.ReturnQty
	--	, PreparedQuantity = DO.DeliveredQty + PS.Qty
	--	, P.ProductID
	--	, NextQuantity = PS.Qty
	--	, DO.NeedQtyExcludePallet
	--FROM 
	--	#PS PS
	--	LEFT JOIN #DO DO
	--		ON PS.ProductID = DO.ProductID
	--		--AND PS.ProductLot = DO.ProductLot
	--	LEFT JOIN Products P WITH (NOLOCK)
	--		ON PS.ProductID = P.ProductID
	--WHERE
	--	PS.Qty > DO.NeedQtyExcludePallet
END