﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallet_ConfirmMoveMaterial]
	@PalletCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_DocumentNbr NVARCHAR(50) = (SELECT TOP 1 ReferenceNbr FROM Pallets WITH (NOLOCK) WHERE PalletCode = @PalletCode)
	DECLARE @_OldPalletCode VARCHAR(255)
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'ConfirmMoveMaterial: ' + @_PalletCode

	--1.Update [Pallets] set trạng thái về P, Đã mang hàng xuống, đợi thủ kho duyệt, hoặc mang thẳng xuống SX
	UPDATE Pallets
	SET 
		[Status] = 'N'
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
		, DOImportCode = NULL
		, ReferenceNbr = NULL
		, CompanyCode = NULL		
		, [LocationCode] = NULL
		, [LocationSuggestion] = NULL
		, [BatchCode] = NULL
		, [BatchCodeDistributor] = NULL
		, [ImportStatus] = NULL
		, [Weigh] = NULL
		, [CartonNumber] = NULL
	WHERE
		PalletCode = @_PalletCode

	
	--Update AUDIT REASON and OLD QUANTITY HERE
	INSERT INTO [dbo].[PalletStatusesAuditLog]
           ([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[Qty_Old]
           ,[ReasonID]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
	 SELECT DISTINCT
		[PalletCode]
        ,[CartonBarcode]
        ,[ProductBarcode]
        ,[EncryptedProductBarcode]
        ,[ProductID] 
        ,[ProductLot]
        ,[Qty] 
		,[Qty_Old] = 0
		,[ReasonID] = '-1'
        ,[CreatedBy] = @_UserID
        ,[CreatedBySitemapID] = 1
        ,[CreatedDateTime] = GETDATE()
        ,[UpdatedBy] = @_UserID
        ,[UpdatedBySitemapID] = 1
        ,[UpdatedDateTime] = GETDATE()
	FROM
		dbo.PalletStatuses
	WHERE
		PalletCode = @_PalletCode

	DELETE FROM
		dbo.PalletStatuses
	WHERE
		PalletCode = @_PalletCode

--2.Update [RequestMaterialLineSplits] SET trạng thái là P
	UPDATE RequestMaterialLineSplits
	SET 
		[Status] = 'C'
		, WarehouseKeeper = @_UserID
		, WarehouseVerifyDate = @_Date
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode
		AND DocumentNbr = @_DocumentNbr

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PalletStatus">' + 
			(SELECT * FROM dbo.Pallets ps WHERE PalletCode = @_PalletCode FOR XML PATH('PalletStatus')) + 
		'</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'UPD', @_Method, @_Date, @_Date
END