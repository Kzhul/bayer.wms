﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_DeliveryNoteDetail]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN

DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method
DECLARE @_ExportCode NVARCHAR(50) = (SELECT TOP 1 ExportCode FROM DeliveryNoteHeaders WHERE DOImportCode = @_DOImportCode)
--------------SOURCE-------------------
SELECT DISTINCT
	DOImportCode
	,ProductID
	,BatchCode
	,Quantity = SUM(DOQuantity)
INTO #SOURCE
FROM
	DeliveryOrderSum 
WHERE
	DOImportCode = @DOImportCode
GROUP BY 
	DOImportCode
	,ProductID
	,BatchCode


--Synchronize the target table with
--refreshed data from source table
;
WITH TARGET AS 
(
    SELECT * 
    FROM dbo.DeliveryNoteDetails
    WHERE 
		DOCode = @_DOImportCode
)
MERGE INTO TARGET
USING #SOURCE AS SOURCE 
ON 
	TARGET.[DOCode] = SOURCE.[DOImportCode]
	AND TARGET.[ProductID] = SOURCE.[ProductID]
	AND TARGET.[BatchCode] = SOURCE.[BatchCode]
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND 
		(
			TARGET.[Quantity] <> SOURCE.[Quantity]
		)
	THEN
	UPDATE SET 
		TARGET.[LastQuantity] = TARGET.[Quantity]
		,TARGET.[Quantity] = SOURCE.[Quantity]
		,TARGET.[PackQuantity] = dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 1)
		,TARGET.[PackType] = [dbo].[fnPackagingType] (SOURCE.[ProductID])
		,TARGET.[ProductQuantity] = dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 0)
		,TARGET.[UpdatedBy] = @_UserID
		,TARGET.[UpdatedBySitemapID] = @_SitemapID
		,TARGET.[UpdatedDateTime] = @_SyncDate
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET 
	THEN 
		INSERT (
			[ExportCode]
			,[DOCode]
			,[ProductID]
			,[BatchCode]
			,[BatchCodeDistributor]
			,[Quantity]
			,[PackQuantity]
			,[PackType]
			,[ProductQuantity]
			,[Shipper1]
			,[Receiver1]
			,[Shipper2]
			,[Receiver2]
			,[Shipper3]
			,[Receiver3]
			,[ExportedQty]
			,[ConfirmQty]
			,[ReceivedQty]
			,[RequireReturnQty]
			,[ExportReturnedQty]
			,[ReceiveReturnedQty]
			,[Status]
			,[CreatedBy]
			,[CreatedBySitemapID]
			,[CreatedDateTime]
			,[UpdatedBy]
			,[UpdatedBySitemapID]
			,[UpdatedDateTime]
			--,[RowVersion]
			,[LastQuantity]
			) 
		VALUES (
			@_ExportCode--SOURCE.[ExportCode]
			,SOURCE.[DOImportCode]
			,SOURCE.[ProductID]
			,SOURCE.[BatchCode]
			,''--SOURCE.[BatchCodeDistributor]
			,SOURCE.[Quantity]
			,dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 1)
			,[dbo].[fnPackagingType] (SOURCE.[ProductID])
			,dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 0)
			,NULL--SOURCE.[Shipper1]
			,NULL--SOURCE.[Receiver1]
			,NULL--SOURCE.[Shipper2]
			,NULL--SOURCE.[Receiver2]
			,NULL--SOURCE.[Shipper3]
			,NULL--SOURCE.[Receiver3]
			,0--SOURCE.[ExportedQty]
			,0--SOURCE.[ConfirmQty]
			,0--SOURCE.[ReceivedQty]
			,0--SOURCE.[RequireReturnQty]
			,0--[ExportReturnedQty]
			,0--[ReceiveReturnedQty]
			,'N'--SOURCE.[Status]
			,@_UserID--SOURCE.[CreatedBy]
			,@_SitemapID--SOURCE.[CreatedBySitemapID]
			,@_SyncDate--SOURCE.[CreatedDateTime]
			,@_UserID--SOURCE.[UpdatedBy]
			,@_SitemapID--[UpdatedBySitemapID]
			,@_SyncDate--[UpdatedDateTime]
			--,SOURCE.[RowVersion]
			,0--[LastQuantity]
			)
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table
WHEN NOT MATCHED BY SOURCE 
	THEN 
		UPDATE SET 
		TARGET.[LastQuantity] = TARGET.[Quantity]
		,TARGET.[Quantity] = 0
		,TARGET.[PackQuantity] = 0
		,TARGET.[ProductQuantity] = 0
		,TARGET.[UpdatedBy] = @_UserID
		,TARGET.[UpdatedBySitemapID] = @_SitemapID
		,TARGET.[UpdatedDateTime] = @_SyncDate
		;
SELECT @@ROWCOUNT;

--DROP TABLE #SOURCE
DROP TABLE #SOURCE
END