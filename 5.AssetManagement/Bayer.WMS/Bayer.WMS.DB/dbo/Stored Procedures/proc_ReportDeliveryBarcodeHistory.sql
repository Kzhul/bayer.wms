﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_ReportDeliveryBarcodeHistory] '', NULL, NULL
CREATE PROCEDURE [dbo].[proc_ReportDeliveryBarcodeHistory]
	@CompanyCode NVARCHAR(255)
	,@FromDate Date
	,@ToDate Date
AS
BEGIN
	SET NOCOUNT ON

--Ngày giao:
--Khách hàng:

--Sản phẩm	Số lô	Số lượng	Mã QR sản phẩm	Mã QR thùng
DECLARE @_FromDate DATE = @FromDate
DECLARE @_ToDate DATE  = @ToDate
DECLARE @_CompanyCode NVARCHAR(50) = @CompanyCode

SELECT DISTINCT 
	P.ProductID
	,P.ProductCode
	,DH.ProductLot
	,DH.[CompanyCode]
	,DH.[DeliveryDate]
	,Quantity = COUNT(DH.[ProductBarcode])
INTO #A
FROM 
	DeliveryHistories DH WITH (NOLOCK)
	LEFT JOIN Products P WITH (NOLOCK)
		ON DH.ProductID = P.ProductID
WHERE
	(@_CompanyCode = '' OR DH.CompanyCode = @_CompanyCode)
	AND (@_FromDate IS NULL OR DH.DeliveryDate >= @_FromDate)
	AND (@_ToDate IS NULL OR DH.DeliveryDate <= @_ToDate)
GROUP BY 
	P.ProductID
	,P.ProductCode
	,DH.ProductLot
	,DH.[CompanyCode]
	,DH.[DeliveryDate]

SELECT DISTINCT 
	P.ProductID
	,P.ProductCode
	,P.Description
	,DH.ProductLot
	,Quantity = A.Quantity
	,DH.[ProductBarcode]
	,DH.[CartonBarcode]
	,DH.[CompanyCode]
	,DH.[CompanyName]
	,DH.[DeliveryDate]
	,DH.[DeliveryTicketCode]
FROM 
	DeliveryHistories DH WITH (NOLOCK)
	LEFT JOIN Products P WITH (NOLOCK)
		ON DH.ProductID = P.ProductID
	LEFT JOIN #A AS A
		ON A.ProductID = DH.ProductID
		AND A.ProductLot = DH.ProductLot
		AND A.[CompanyCode] = DH.[CompanyCode]
		AND A.[DeliveryDate] = DH.[DeliveryDate]
WHERE
	(@_CompanyCode = '' OR DH.CompanyCode = @_CompanyCode)
	AND (@_FromDate IS NULL OR DH.DeliveryDate >= @_FromDate)
	AND (@_ToDate IS NULL OR DH.DeliveryDate <= @_ToDate)
END