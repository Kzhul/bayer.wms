﻿CREATE PROCEDURE [dbo].[proc_DeliveryTicket_SelectNumberOfCarton]
	@DeliveryTicketCode NVARCHAR(50)
	,@CompanyCode NVARCHAR(50)
AS
BEGIN
DECLARE @_DeliveryTicketCode NVARCHAR(50) = @DeliveryTicketCode--'DO18052503'
DECLARE @_CompanyCode NVARCHAR(50) = @CompanyCode--'2745613'

SELECT 
	CONVERT(NVARCHAR(50), COUNT(DISTINCT CartonBarcode))
FROM
(
	SELECT DISTINCT 
		PS.CartonBarcode
	FROM 
		Pallets P WITH (NOLOCK)
		JOIN PalletStatuses PS WITH (NOLOCK)
			ON p.PalletCode = PS.PalletCode
		JOIN PrepareNoteHeaders PN WITH (NOLOCK)
			ON P.DOImportCode = PN.DOImportCode
		JOIN DeliveryTicketDetails DT WITH (NOLOCK)
			ON DT.PrepareCode = PN.PrepareCode
	WHERE 
		DT.DeliveryTicketCode = @_DeliveryTicketCode
		AND P.CompanyCode = @_CompanyCode
		AND PS.CartonBarcode != ''

	UNION

	SELECT DISTINCT 
		PS.CartonBarcode
	FROM 
		DeliveryHistories PS WITH (NOLOCK)
		JOIN PrepareNoteHeaders PN WITH (NOLOCK)
			ON PS.DeliveryTicketCode = PN.DOImportCode
		JOIN DeliveryTicketDetails DT WITH (NOLOCK)
			ON DT.PrepareCode = PN.PrepareCode
	WHERE 
		DT.DeliveryTicketCode = @_DeliveryTicketCode
		AND PS.CompanyCode = @_CompanyCode
		AND PS.CartonBarcode != ''
) AS TB
END