﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_SelectProduct]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ASC)
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,RequestQty = ISNULL(RL.RequestQty,0)
	,NeedQuantity = ISNULL(RL.RequestQty,0) -  ISNULL(RL.[PickedQty],0)
	,[PickedQty] = ISNULL(RL.[PickedQty],0)
FROM
	RequestMaterialLines RL WITH (NOLOCK) 
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RL.ProductID = PD.ProductID
WHERE
	RL.DocumentNbr = @_DocumentNbr
ORDER BY
	PD.Description ASC
END