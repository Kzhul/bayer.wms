﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SelectByDOImport]
	@DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

IF(@_DOImportCode = '')
BEGIN
	SELECT DISTINCT
		P.PalletCode
		,[StrStatus] = dbo.fnPalletStatus(P.[Status])
		,C.CompanyName
		,C.CompanyCode		
		,UserFullName = U.LastName + ' ' + U.FirstName
		,StrDeliveryDate = FORMAT(P.UpdatedDateTime, 'dd/MM/yyyy')
		, P.DOImportCode
		, tmp1.ProductLot
		, pd.ProductCode
		, pd.Description
		, ProductDescription = pd.Description
		, pd.PackingType
		, tmp1.Quantity
	FROM 
		Pallets P WITH (NOLOCK)
		LEFT JOIN Users U WITH (NOLOCK)
			ON P.UpdatedBy = U.UserID
		LEFT JOIN Companies C WITH (NOLOCK)
			ON P.CompanyCode = C.CompanyCode
		JOIN 
		(
			SELECT
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
				, Quantity = COUNT(DISTINCT ps.ProductBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.DOImportCode != '' OR pl.CompanyCode != ''
			GROUP BY
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
		) tmp1
			ON P.PalletCode = tmp1.PalletCode
		LEFT JOIN dbo.Products pd WITH (NOLOCK) 
			ON pd.ProductID = tmp1.ProductID
			AND pd.IsDeleted = 0
	WHERE 
		P.DOImportCode != '' OR P.CompanyCode != ''
END
ELSE 
	BEGIN
		SELECT DISTINCT
			P.PalletCode
			,[StrStatus] = dbo.fnPalletStatus(P.[Status])
			,C.CompanyName
			,C.CompanyCode		
			,UserFullName = U.LastName + ' ' + U.FirstName
			,StrDeliveryDate = FORMAT(P.UpdatedDateTime, 'dd/MM/yyyy')
			, P.DOImportCode
			, tmp1.ProductLot
			, pd.ProductCode
			, pd.Description
			, ProductDescription = pd.Description
			, pd.PackingType
			, tmp1.Quantity
		FROM 
			Pallets P WITH (NOLOCK)
			LEFT JOIN Users U WITH (NOLOCK)
				ON P.UpdatedBy = U.UserID
			LEFT JOIN Companies C WITH (NOLOCK)
				ON P.CompanyCode = C.CompanyCode
			JOIN 
			(
				SELECT
					pl.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, Quantity = COUNT(DISTINCT ps.ProductBarcode)
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
					JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
				WHERE
					pl.DOImportCode = @_DOImportCode
				GROUP BY
					pl.PalletCode
					, ps.ProductID
					, ps.ProductLot
			) tmp1
				ON P.PalletCode = tmp1.PalletCode
			LEFT JOIN dbo.Products pd WITH (NOLOCK) 
				ON pd.ProductID = tmp1.ProductID
				AND pd.IsDeleted = 0
		WHERE 
			P.DOImportCode = @_DOImportCode
		UNION 

		SELECT DISTINCT
			DH.PalletCode
			,[StrStatus] = N'Đã giao'
			,C.CompanyName
			,C.CompanyCode
			,UserFullName = U.LastName + ' ' + U.FirstName
			,StrDeliveryDate = FORMAT(DH.ActualDeliveryDate, 'dd/MM/yyyy')
			, DOImportCode = DH.DeliveryTicketCode
			, ProductLot = ''
			, ProductCode = ''
			, Description = ''
			, ProductDescription = ''
			, PackingType = ''
			, Quantity = 0
		FROM 
			DeliveryHistories DH WITH (NOLOCK)
			LEFT JOIN Users U WITH (NOLOCK)
				ON DH.UpdatedBy = U.UserID
			LEFT JOIN Companies C WITH (NOLOCK)
				ON DH.CompanyCode = C.CompanyCode
		WHERE 
			DH.DeliveryTicketCode = @_DOImportCode
	END



END