﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReturn_Select_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

    --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ASC)
	,RM.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,RM.Status
	,ReturnQty = ISNULL(RM.ReturnQty,0.000)
	,ReceivedQty = ISNULL(RM.ReceivedQty,0.000)
	,PD.UOM
	,ProductLot = RM.BatchCode
--INTO #TBPick
FROM
	StockReturnDetails RM WITH (NOLOCK) 
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RM.ProductID = PD.ProductID
WHERE
	RM.StockReturnCode = @_DocumentNbr
	AND RM.Status = 'N'
ORDER BY PD.Description
END