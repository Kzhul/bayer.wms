﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_AuditSelect]
	@PalletCode NVARCHAR(50)
	,@ProductLot NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode NVARCHAR(50) = @PalletCode
	DECLARE @_ProductLot NVARCHAR(50) = @ProductLot


IF(@_ProductLot != '')
BEGIN
	--nếu có số lô, vẩn hiển thị nếu như quantity = 0
	SELECT 
		ID = ROW_NUMBER() OVER ( ORDER BY PD.Description, srd.ExpiredDate ASC)
		,PalletCode = @PalletCode
		,LocationCode = ISNULL(P.LocationCode,'')
		,PD.ProductID
		,PD.ProductCode
		,PD.Description AS ProductName
		,ProductLot = srd.BatchCode
		,P.Status
		,srd.DeliveryDate
		,srd.ExpiredDate
		,ISNULL(PS.Qty, 0) AS PalletQuantity
		, P.LocationCode
		, LocationName = ISNULL(ZL.LocationName,'')
	FROM
		PalletStatuses PS WITH (NOLOCK) 
		LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
			ON srd.ProductID = ps.ProductID
			AND srd.BatchCode = ps.ProductLot
			AND PS.PalletCode = @_PalletCode
		LEFT JOIN Pallets P WITH (NOLOCK) 
			ON @_PalletCode = P.PalletCode
		LEFT JOIN Products PD WITH (NOLOCK) 
			ON PS.ProductID = PD.ProductID
		LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
			ON p.LocationCode = ZL.LocationCode
	WHERE
		(PS.ProductLot = @ProductLot OR @ProductLot = '')
		AND PS.PalletCode = @_PalletCode
	ORDER BY
		PD.Description
		,srd.ExpiredDate ASC
END
ELSE
BEGIN
	--nếu không có số lô, chỉ hiển thị nếu như quantity > 0
	SELECT 
		ID = ROW_NUMBER() OVER ( ORDER BY PD.Description, srd.ExpiredDate ASC)
		,PalletCode = @PalletCode
		,LocationCode = ISNULL(P.LocationCode,'')
		,PD.ProductID
		,PD.ProductCode
		,PD.Description AS ProductName
		,ProductLot = PS.ProductLot
		,P.Status
		,srd.DeliveryDate
		,srd.ExpiredDate
		,ISNULL(SUM(ISNULL(PS.Qty, 0)),0) AS PalletQuantity
		, P.LocationCode
		, LocationName = ISNULL(ZL.LocationName,'')
	FROM
		Pallets P WITH (NOLOCK) 
		LEFT JOIN PalletStatuses PS WITH (NOLOCK) 
			ON PS.PalletCode = @_PalletCode
		LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
			ON srd.ProductID = ps.ProductID
			AND srd.BatchCode = ps.ProductLot
		LEFT JOIN Products PD WITH (NOLOCK) 
			ON PS.ProductID = PD.ProductID
		LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
			ON p.LocationCode = ZL.LocationCode
	WHERE
		(srd.BatchCode = @ProductLot OR @ProductLot = '')
		AND P.PalletCode = @_PalletCode
	GROUP BY
		PS.PalletCode
		,PD.ProductID
		,PS.ProductLot
		,P.Status
		,P.CompanyCode
		,srd.DeliveryDate
		,srd.ExpiredDate
		,PD.ProductID
		,PD.ProductCode
		,PD.Description
		,P.LocationCode
		,ISNULL(ZL.LocationName,'')
	ORDER BY
		PD.Description
		,srd.ExpiredDate ASC
END


END