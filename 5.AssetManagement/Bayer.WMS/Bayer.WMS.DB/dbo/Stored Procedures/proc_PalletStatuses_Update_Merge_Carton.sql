﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_Merge_Carton]
	@ProductBarcode VARCHAR(255)
	, @CartonBarcode VARCHAR(255)
	, @PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_ProductBarcode VARCHAR(255) = @ProductBarcode
	DECLARE @_CartonBarcode VARCHAR(255) = @CartonBarcode
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode

    UPDATE dbo.PalletStatuses SET PalletCode = @_PalletCode, CartonBarcode = @_CartonBarcode WHERE ProductBarcode = @_ProductBarcode
END