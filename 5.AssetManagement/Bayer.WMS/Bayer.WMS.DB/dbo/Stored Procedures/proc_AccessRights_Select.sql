﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.[proc_AccessRights_Select]
	@UserID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_UserID INT = @UserID

	SELECT
		sm.*
		, AccessRights = CASE WHEN @_UserID = 1 THEN 3 ELSE 0 END
	INTO #tmpSitemap
	FROM
		dbo.Sitemaps sm WITH (NOLOCK)

	IF @_UserID != 1
		BEGIN
		SELECT
			RowNumber = ROW_NUMBER() OVER(ORDER BY ur.RoleID)
			, ur.RoleID
		INTO #tmpRole
		FROM
			dbo.UsersRoles ur WITH (NOLOCK)
		WHERE
			ur.UserID = @_UserID
	
		DECLARE @_i INT = 1
		DECLARE @_max INT = (SELECT MAX(RowNumber) FROM #tmpRole)
		DECLARE @_RoleID INT
		CREATE TABLE #tmpAccessRight
		(
			SitemapID INT
			, ParentID INT
			, AccessRights INT
		)

		WHILE @_i <= @_max
			BEGIN
				DELETE FROM #tmpAccessRight

				SELECT @_RoleID = RoleID FROM #tmpRole WHERE RowNumber = @_i

				INSERT INTO #tmpAccessRight
				SELECT
					sm.SitemapID
					, sm.ParentID
					, AccessRights =	CASE rsm.AccessRights
											WHEN 'N' THEN 0
											WHEN 'R' THEN 1
											WHEN 'W' THEN 2
											WHEN 'M' THEN 3
											WHEN 'I' THEN -1
										END
				FROM
					dbo.RoleSitemaps rsm WITH (NOLOCK)
					JOIN dbo.Sitemaps sm WITH (NOLOCK) ON sm.SitemapID = rsm.SitemapID
				WHERE
					rsm.RoleID = @_RoleID
			
				WHILE EXISTS (SELECT 1 FROM #tmpAccessRight WHERE AccessRights = -1)
					BEGIN
						UPDATE ar
						SET
							ar.AccessRights = arp.AccessRights
						FROM
							#tmpAccessRight ar
							JOIN #tmpAccessRight arp ON arp.SitemapID = ar.ParentID
						WHERE
							ar.AccessRights = -1 
					END
			
				UPDATE sm
				SET
					sm.AccessRights = CASE WHEN sm.AccessRights > ar.AccessRights THEN sm.AccessRights ELSE ar.AccessRights END
				FROM
					#tmpSitemap sm
					JOIN #tmpAccessRight ar ON ar.SitemapID = sm.SitemapID

				SET @_i = @_i + 1
			END
		END

	SELECT
		SitemapID
		, ParentID
		, Description
		, ClassName
		, InterfaceName
		, AssemblyName
		, [Order]
		, Icon
		, EnableRefresh
		, EnableInsert
		, EnableSave
		, EnableDelete
		, EnablePrint
		, Type
		, AccessRights =	CASE AccessRights
								WHEN 0 THEN 'N'
								WHEN 1 THEN 'R'
								WHEN 2 THEN 'W'
								WHEN 3 THEN 'M'
							END
	FROM 
		#tmpSitemap
END