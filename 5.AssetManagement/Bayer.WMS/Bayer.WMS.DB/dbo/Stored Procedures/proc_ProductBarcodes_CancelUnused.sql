﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.proc_ProductBarcodes_CancelUnused
	@ProductLot VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_ProductLot VARCHAR(255) = @ProductLot

	UPDATE pb
	SET
		pb.Status = 'X'
	FROM
		dbo.ProductBarcodes pb WITH (NOLOCK)
		LEFT JOIN dbo.PackagingLogs pl WITH (NOLOCK) ON pl.Barcode = pb.Barcode
	WHERE
		pb.ProductLot = @_ProductLot
		AND pl.Barcode IS NULL
END