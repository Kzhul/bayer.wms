﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PrepareNote_Select_OddQty]
	@PrepareCode VARCHAR(255)
	, @OddQty INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PrepareCode VARCHAR(255) = @PrepareCode

    SELECT
		@OddQty = COUNT(CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN NULL ELSE ps.ProductBarcode END)
	FROM
		dbo.PalletStatuses ps WITH (NOLOCK)
		JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
		JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
											 AND p.PackingType = 'C'
											 AND p.IsDeleted = 0
	WHERE
		pl.ReferenceNbr = @_PrepareCode

	SELECT @OddQty = ISNULL(@OddQty, 0)
END