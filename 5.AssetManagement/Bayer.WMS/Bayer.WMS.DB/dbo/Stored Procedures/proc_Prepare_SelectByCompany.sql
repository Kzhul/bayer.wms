﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Prepare_SelectByCompany]
	@DOImportCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @DateToGet DATE = CONVERT(DATE, DATEADD(dd,-5,GETDATE()))
	DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode

	--PRINT (@DateToGet)
	SELECT DISTINCT
		P.CompanyCode
		,C.CompanyName
		,P.DeliveryDate
		,StrDeliveryDate = FORMAT(P.DeliveryDate, 'dd/MM/yyyy')
		,DOQuantity = SUM(P.DOQuantity)
		,PreparedQty = SUM(P.PreparedQty)
		,DeliveredQty = SUM(P.DeliveredQty)
	INTO #A
	FROM 
		DeliveryOrderSum P WITH (NOLOCK)
		LEFT JOIN Companies C WITH (NOLOCK)
			ON P.CompanyCode = C.CompanyCode
	WHERE
		--P.DeliveryDate >= @DateToGet
		--AND 
		P.DOImportCode = @_DOImportCode
	GROUP BY 
		P.DeliveryDate
		,P.CompanyCode
		,C.CompanyName

	SELECT DISTINCT 
		* 
	FROM #A AS C
	WHERE
		DeliveredQty <= DOQuantity
	ORDER BY C.CompanyName
END