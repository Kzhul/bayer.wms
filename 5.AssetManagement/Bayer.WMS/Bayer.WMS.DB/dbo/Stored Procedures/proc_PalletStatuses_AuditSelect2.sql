﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_AuditSelect2]
	@PalletCode NVARCHAR(50)
	,@ProductLot NVARCHAR(50)
	,@ProductCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode NVARCHAR(50) = @PalletCode
	DECLARE @_ProductLot NVARCHAR(50) = @ProductLot
	DECLARE @_ProductCode NVARCHAR(50) = @ProductCode
	DECLARE @LenStr INT 
	DECLARE @YEAR NVARCHAR(10) 
	DECLARE @MONTH NVARCHAR(10)
	DECLARE @DAY NVARCHAR(10) 

	IF EXISTS (
		SELECT TOP 1 * 
		FROM 
			StockReceivingDetails WITH (NOLOCK)
		WHERE
			BatchCode = @_ProductLot
	)
	BEGIN
		--nếu có số lô, vẩn hiển thị nếu như quantity = 0
		SELECT DISTINCT
			ID = ROW_NUMBER() OVER ( ORDER BY PD.Description, srd.ExpiredDate ASC)
			,PalletCode = @PalletCode
			,LocationCode = ISNULL(P.LocationCode,'')
			,PD.ProductID
			,PD.ProductCode
			,PD.Description AS ProductName
			,ProductLot = srd.BatchCode
			,P.Status
			,srd.DeliveryDate
			,ExpiredDate = FORMAT(srd.ExpiredDate, 'dd/MM/yyyy')
			,ISNULL(PS.Qty, 0) AS PalletQuantity
			,srd.ExpiredDate AS DateExpiredDate
			,[PackageSize] = CONVERT(INT,PPS.Quantity)
			,PD.[UOM]
		FROM
			Pallets P WITH (NOLOCK) 
			LEFT JOIN PalletStatuses PS WITH (NOLOCK) 
			LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
				ON srd.ProductID = ps.ProductID
				AND srd.BatchCode = ps.ProductLot
				AND PS.PalletCode = @_PalletCode
				ON @_PalletCode = P.PalletCode
			LEFT JOIN Products PD WITH (NOLOCK) 
				ON PS.ProductID = PD.ProductID
			LEFT JOIN [dbo].[ProductPackings] PPS
				ON PD.ProductID = PPS.ProductID
				AND PPS.Type != 'P'
		WHERE
			(PS.ProductLot = @ProductLot OR @ProductLot = '')
			AND PS.PalletCode = @_PalletCode
		ORDER BY
			PD.Description
			,srd.ExpiredDate ASC
	END
	ELSE
		BEGIN

			SET @_ProductCode =  (
				SELECT TOP 1 ProductCode 
				FROM 
					Products WITH (NOLOCK)
				WHERE
					@_ProductCode LIKE ProductCode + '%'
			)
			SET @LenStr = LEN(@ProductCode)
			SET @YEAR = SUBSTRING(@ProductCode,@LenStr-5,2)
			SET @MONTH = SUBSTRING(@ProductCode,@LenStr-7,2)
			SET @DAY = SUBSTRING(@ProductCode,@LenStr-9,2)
			SET @_ProductLot = REPLACE(@ProductCode,@_ProductCode,'')
			SET @_ProductLot = SUBSTRING(@_ProductLot,3,9)
			PRINT(@_ProductLot)
			PRINT(@_PalletCode)

			IF EXISTS (
				SELECT TOP 1 * 
				FROM 
					PalletStatuses WITH (NOLOCK)
				WHERE
					PalletCode = @_PalletCode
					AND ProductLot = @_ProductLot
			)
			BEGIN
				--nếu có số lô, vẩn hiển thị nếu như quantity = 0
				SELECT DISTINCT
					ID = ROW_NUMBER() OVER ( ORDER BY PD.Description, srd.ExpiredDate ASC)
					,PalletCode = @PalletCode
					,LocationCode = ISNULL(P.LocationCode,'')
					,PD.ProductID
					,PD.ProductCode
					,PD.Description AS ProductName
					,ProductLot = @_ProductLot
					,P.Status
					,DeliveryDate = FORMAT(GETDATE(), 'dd/MM/yyyy')
					,ExpiredDate = @DAY + '/' + @MONTH + '/20' + @YEAR
					,ISNULL(PS.Qty,0) AS PalletQuantity
					,DateExpiredDate = DATEFROMPARTS('20' + @YEAR, @MONTH, @DAY)
					,[PackageSize] = CONVERT(INT,PPS.Quantity)
					,PD.[UOM]
				FROM
					Pallets P WITH (NOLOCK) 
					LEFT JOIN PalletStatuses PS WITH (NOLOCK) 
					LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
						ON srd.ProductID = ps.ProductID
						AND srd.BatchCode = ps.ProductLot
						AND PS.PalletCode = @_PalletCode
						ON @_PalletCode = P.PalletCode
					LEFT JOIN Products PD WITH (NOLOCK) 
						ON PS.ProductID = PD.ProductID
					LEFT JOIN [dbo].[ProductPackings] PPS
						ON PD.ProductID = PPS.ProductID
						AND PPS.Type != 'P'
				WHERE
					(PS.ProductLot = @_ProductLot OR @_ProductLot = '')
					AND PS.PalletCode = @_PalletCode
				ORDER BY
					PD.Description
			END
			ELSE
			BEGIN
				SET @_ProductCode =  (
					SELECT TOP 1 ProductCode 
					FROM 
						Products WITH (NOLOCK)
					WHERE
						@_ProductCode LIKE ProductCode + '%'
				)
				SET @LenStr = LEN(@ProductCode)
				SET @YEAR = SUBSTRING(@ProductCode,@LenStr-5,2)
				SET @MONTH = SUBSTRING(@ProductCode,@LenStr-7,2)
				SET @DAY = SUBSTRING(@ProductCode,@LenStr-9,2)
				SET @_ProductLot = REPLACE(@ProductCode,@_ProductCode,'')
				SET @_ProductLot = SUBSTRING(@_ProductLot,3,9)
				PRINT(@_ProductLot)
				PRINT(@YEAR)
				PRINT(@MONTH)
				PRINT(@DAY)
				
				SELECT DISTINCT
					ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ASC)
					,PalletCode = @PalletCode
					,LocationCode = ISNULL(P.LocationCode,'')
					,PD.ProductID
					,PD.ProductCode
					,PD.Description AS ProductName
					,ProductLot = @_ProductLot
					,P.Status
					,DeliveryDate = FORMAT(GETDATE(), 'dd/MM/yyyy')
					,ExpiredDate = @DAY + '/' + @MONTH + '/20' + @YEAR
					,0 AS PalletQuantity
					,DateExpiredDate = DATEFROMPARTS('20' + @YEAR, @MONTH, @DAY)
					,[PackageSize] = CONVERT(INT,PPS.Quantity)
					,PD.[UOM]
				FROM
					Pallets P WITH (NOLOCK) 
					LEFT JOIN Products PD WITH (NOLOCK) 
						ON PD.ProductCode = @_ProductCode
					LEFT JOIN [dbo].[ProductPackings] PPS
						ON PD.ProductID = PPS.ProductID
						AND PPS.Type != 'P'
				WHERE
					P.PalletCode = @_PalletCode
			END
		END
END