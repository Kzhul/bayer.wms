﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SelectByCompany]
	@CompanyCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode

	SELECT 
		# = ROW_NUMBER() OVER(ORDER BY tmp.PalletCode ASC),
		P.PalletCode
		,UserFullName = U.LastName + ' ' + U.FirstName
		,CartonOddQuantity = CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) + ' /' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
	FROM 
		Pallets P WITH (NOLOCK)
		LEFT JOIN 
		(
			SELECT
				ps.PalletCode
				, OddQuantity = SUM(CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN NULL ELSE ISNULL(ps.Qty, 1) END)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_CompanyCode
			GROUP BY
				ps.PalletCode
		) tmp
			ON P.PalletCode = tmp.PalletCode
		LEFT JOIN 
		(
			SELECT
				ps.PalletCode
				, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_CompanyCode
				AND CartonBarcode IS NOT NULL 
				AND CartonBarcode != ''
			GROUP BY
				ps.PalletCode
		) tmpC
			ON P.PalletCode = tmpC.PalletCode
		LEFT JOIN Users U WITH (NOLOCK)
			ON P.UpdatedBy = U.UserID
	WHERE 
		P.CompanyCode = @_CompanyCode
	ORDER BY P.PalletCode

	--SELECT DISTINCT PS.* FROM PalletStatuses PS LEFT JOIN Pallets P ON PS.PalletCode = P.PalletCode WHERE P.CompanyCode = '764497'
END