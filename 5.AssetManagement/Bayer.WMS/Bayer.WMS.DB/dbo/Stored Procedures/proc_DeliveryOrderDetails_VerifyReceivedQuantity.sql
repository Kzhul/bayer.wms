﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity]
--EXEC [proc_DeliveryOrderDetails_VerifyPreparedQuantity] '1100189', '170901F08C009'
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_VerifyReceivedQuantity]
	@DOImportCode VARCHAR(255)
	,@CompanyCode VARCHAR(255)
	,@PalletCode VARCHAR(255)
	,@BarCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_BarCode VARCHAR(255) = @BarCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode

	SELECT DISTINCT
		PS.ProductLot
		, PS.CartonBarcode
		, PS.ProductBarcode
		, ProductCode = P.ProductCode
		, ProductName = P.Description
		, P.ProductID
		, NextQuantity = PS.Qty
	FROM 
		PalletStatuses PS
		JOIN Pallets PL
			ON PS.PalletCode = PL.PalletCode
		LEFT JOIN Products P WITH (NOLOCK)
			ON PS.ProductID = P.ProductID
	WHERE
		PL.CompanyCode = @CompanyCode
		AND PL.DOImportCode = @_DOImportCode
		AND PL.PalletCode = @_PalletCode
		AND (
			PS.CartonBarcode = @_BarCode
			OR PS.ProductBarcode = @_BarCode		
			OR PS.EncryptedProductBarcode = @_BarCode
		)
END