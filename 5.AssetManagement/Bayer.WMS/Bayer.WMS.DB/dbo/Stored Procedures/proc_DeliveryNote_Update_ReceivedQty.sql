﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryNote_Update_ReceivedQty]
	@ExportCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
	, @ProductID INT
	, @BatchCode VARCHAR(255)
	, @ReceivedQty DECIMAL(18, 2)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @_ExportCode VARCHAR(255) = @ExportCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_BatchCode VARCHAR(255) = @BatchCode
	DECLARE @_ReceivedQty DECIMAL(18, 2) = @ReceivedQty
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_ExportTime INT

	SELECT @_ExportTime = (
	SELECT
		CASE
			WHEN MAX(dnd.Receiver3) IS NOT NULL THEN 4
			WHEN MAX(dnd.Receiver2) IS NOT NULL THEN 3
			WHEN MAX(dnd.Receiver1) IS NOT NULL THEN 2
			ELSE 1
		END
	FROM
		dbo.DeliveryNoteDetails dnd WITH (NOLOCK)
	WHERE
		dnd.ExportCode = @_ExportCode
		AND ProductID = @_ProductID
		AND BatchCode = @_BatchCode)

    UPDATE dbo.DeliveryNoteDetails
	SET
		Shipper1 =	CASE WHEN @_ExportTime = 1 THEN @_ReceivedQty ELSE Shipper1 END
		, Shipper2 = CASE WHEN @_ExportTime = 2 THEN @_ReceivedQty - Shipper1 ELSE Shipper2 END
		, Shipper3 = CASE WHEN @_ExportTime = 3 THEN @_ReceivedQty - Shipper1 - Shipper2 ELSE Shipper3 END
		, Receiver1 = CASE WHEN @_ExportTime = 1 THEN @_UserID ELSE Receiver1 END
		, Receiver2 = CASE WHEN @_ExportTime = 2 THEN @_UserID ELSE Receiver2 END
		, Receiver3 = CASE WHEN @_ExportTime = 3 THEN @_UserID ELSE Receiver3 END
		, Status = CASE WHEN @_ReceivedQty = Quantity THEN 'C' ELSE 'I' END
		, ReceivedQty = @_ReceivedQty
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		ExportCode = @_ExportCode
		AND ProductID = @_ProductID
		AND BatchCode = @_BatchCode
	
	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="DeliveryNoteDetail">' 
		+ (SELECT * FROM dbo.DeliveryNoteDetails dnd WHERE dnd.ExportCode = @_ExportCode AND dnd.ProductID = @_ProductID AND dnd.BatchCode = @_BatchCode FOR XML PATH(''))
		+ '</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'DeliveryNoteDetails', @_Data, 'UPD', @_Method, @_Date, @_Date

	IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.DeliveryNoteDetails WHERE ExportCode = @_ExportCode AND ISNULL(ReceivedQty, 0) != Quantity)
		UPDATE dbo.DeliveryNoteHeaders
		SET
			Status = 'C'
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE
			ExportCode = @_ExportCode
	ELSE
		UPDATE dbo.DeliveryNoteHeaders
		SET
			Status = 'I'
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE
			ExportCode = @_ExportCode

	INSERT INTO dbo.DeliveryOrderExecutors
	( 
		DOImportCode
		, UserID
		, Type 
	)
	VALUES
	(
		@_DOImportCode
		, @_UserID
		, 'R'
	)
END