﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_Select_SumByPackingType]
	@Barcode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @Barcode
	DECLARE @Carton VARCHAR(255) = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'C')
	DECLARE @Bag VARCHAR(255) = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'B')
	DECLARE @Shove VARCHAR(255) = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'S')
	DECLARE @Can VARCHAR(255) = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'A')
	DECLARE @StrQuantity VARCHAR(255) = ''
	IF(@Carton != '')
		SET @StrQuantity = @StrQuantity + @Carton  + ' ; '
	IF(@Bag != '')
		SET @StrQuantity = @StrQuantity + @Bag  + ' ; '
	IF(@Shove != '')
		SET @StrQuantity = @StrQuantity + @Shove + ' ; '
	IF(@Can != '')
		SET @StrQuantity = @StrQuantity + @Can + ' ; '

	SELECT TOP 1
		# = ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC),
		P.PalletCode 
		,StrQuantity = @StrQuantity
		,UserFullName = U.LastName + ' ' + U.FirstName
		,StrStatus = CASE 
		                WHEN P.[Status] = 'N' THEN N'Mới'
						WHEN P.[Status] = 'P' THEN N'Soạn hàng'
						WHEN P.[Status] = 'B' THEN N'Chờ xe nâng'
						WHEN P.[Status] = 'A' THEN N'Đang giữ'
						WHEN P.[Status] = 'X' THEN N'Chờ nhận hàng'
						WHEN P.[Status] = 'R' THEN N'Đã nhận hàng'
						WHEN P.[Status] = 'T' THEN N'Trả lại cho kho'
						ELSE '' END
		,P.CompanyCode
		,CompanyName = (SELECT TOP 1 CompanyName FROM DeliveryOrderDetails WITH (NOLOCK) WHERE CompanyCode = P.CompanyCode)
	FROM 
		Pallets P WITH (NOLOCK)
		LEFT JOIN Users U WITH (NOLOCK)
			ON P.UpdatedBy = U.UserID
	WHERE 
		P.PalletCode = @_PalletCode
	ORDER BY P.PalletCode
END