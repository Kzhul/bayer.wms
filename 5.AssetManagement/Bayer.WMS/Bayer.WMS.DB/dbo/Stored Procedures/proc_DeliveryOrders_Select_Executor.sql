﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryOrders_Select_Executor]
	@DOImportCode VARCHAR(MAX)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_DOImportCode VARCHAR(MAX) = @DOImportCode
	DECLARE @_Type CHAR(1) = @Type

	IF @_Type = 'P'
		SELECT DISTINCT
			doe.UserID
		FROM
			dbo.DeliveryOrderExecutors doe WITH (NOLOCK)
		WHERE
			(',' + @_DOImportCode + ',') LIKE '%,' + doe.DOImportCode + ',%'
			AND doe.Type = @_Type
	ELSE
		SELECT DISTINCT
			doe.UserID
		FROM
			dbo.DeliveryOrderExecutors doe WITH (NOLOCK)
		WHERE
			doe.DOImportCode = @_DOImportCode
			AND doe.Type = @_Type
END