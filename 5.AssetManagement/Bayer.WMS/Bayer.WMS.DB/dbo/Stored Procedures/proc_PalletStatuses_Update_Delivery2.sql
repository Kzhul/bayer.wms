﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_Delivery2]
	@PalletCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_CompanyCode VARCHAR(255) = (SELECT TOP 1 CompanyCode FROM Pallets WITH (NOLOCK) WHERE PalletCode = @_PalletCode)
	DECLARE @_CompanyName NVARCHAR(255) = (SELECT TOP 1 CompanyName FROM DeliveryOrderDetails WITH (NOLOCK) WHERE CompanyCode = @_CompanyCode)
	DECLARE @_ActualDeliveryDate DATE = GETDATE()	
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

	INSERT INTO dbo.DeliveryHistories
	(
		ProductID
		, ProductLot
		, ProductBarcode
	    , EncryptedProductBarcode
	    , CartonBarcode
		, DeliveryTicketCode
	    , CompanyCode
	    , CompanyName
		, TruckNo
		, ActualDeliveryDate
	    , DeliveryDate
	    , CreatedBy
	    , CreatedBySitemapID
	    , CreatedDateTime
	    , UpdatedBy
	    , UpdatedBySitemapID
	    , UpdatedDateTime
	)
	SELECT
		ps.ProductID
		, ps.ProductLot
		, ps.ProductBarcode
		, ps.EncryptedProductBarcode
		, ps.CartonBarcode
		, ''
		, @_CompanyCode
		, @_CompanyName
		, ''
		, @_ActualDeliveryDate
		, @_ActualDeliveryDate
		, @_UserID
		, @_SitemapID
		, @_Date
		, @_UserID
		, @_SitemapID
		, @_Date
	FROM
		dbo.PalletStatuses ps
	WHERE
		ps.PalletCode = @_PalletCode

	DELETE FROM dbo.PalletStatuses WHERE PalletCode = @_PalletCode

	UPDATE dbo.Pallets 
	SET
		Status = 'N'
		, DOImportCode = NULL
		, ReferenceNbr = NULL
		, CompanyCode = NULL
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallet">' 
		+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode FOR XML PATH(''))
		+ '</BaseEntity>'

	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'UPD', @_Method, @_Date, @_Date
END