﻿--EXEC [proc_ReportReturnDO] 'RD04081702', NULL, NULL
CREATE PROCEDURE [dbo].[proc_ReturnDOUpdateAllTicketDetailData]
	@DOImportCode NVARCHAR(50)
	,@LineNbr INT
	,@Delivery NVARCHAR(50)
	,@ProductID INT
	,@BatchCode NVARCHAR(50)
	,@CompanyCode NVARCHAR(50)
	,@RequestReturnQty DECIMAL(18,2)
	,@UserID INT
	,@SitemapID INT
	,@Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
--DOImportCode
--LineNbr
--Delivery
--ProductID
--BatchCode
--CompanyCode
--RequestReturnQty
DECLARE @Date DATETIME = GETDATE()

UPDATE [dbo].[DeliveryOrderDetails]
SET
	[DOQuantity] = [Quantity]
	,[Quantity] = [Quantity] - @RequestReturnQty
	,[RequireReturnQty] = ISNULL([RequireReturnQty],0) + @RequestReturnQty
	, UpdatedBy = @UserID
	, UpdatedBySitemapID = @SitemapID
	, UpdatedDateTime = @Date
WHERE
	DOImportCode = @DOImportCode
	AND LineNbr = @LineNbr
	AND Delivery = @Delivery
	AND ProductID = @ProductID
	AND BatchCode = @BatchCode
	AND CompanyCode = @CompanyCode


UPDATE [dbo].[DeliveryNoteDetails]
SET	
	[Quantity] = [Quantity] - @RequestReturnQty
	,[RequireReturnQty] = ISNULL([RequireReturnQty],0) + @RequestReturnQty
	, UpdatedBy = @UserID
	, UpdatedBySitemapID = @SitemapID
	, UpdatedDateTime = @Date
WHERE
	DOCode = @DOImportCode
	AND ProductID = @ProductID
	AND BatchCode = @BatchCode

UPDATE [dbo].[SplitNoteDetails]
SET
	[Quantity] = [Quantity] - @RequestReturnQty
	,[RequireReturnQty] = ISNULL([RequireReturnQty],0) + @RequestReturnQty
	, UpdatedBy = @UserID
	, UpdatedBySitemapID = @SitemapID
	, UpdatedDateTime = @Date
WHERE
	DOCode = @DOImportCode
	AND DeliveryCode = @Delivery
	AND ProductID = @ProductID
	AND BatchCode = @BatchCode
	AND CompanyCode = @CompanyCode



UPDATE [dbo].[PrepareNoteDetails]
SET
	[Quantity] = [Quantity] - @RequestReturnQty
	,[RequireReturnQty] = ISNULL([RequireReturnQty],0) + @RequestReturnQty
	, UpdatedBy = @UserID
	, UpdatedBySitemapID = @SitemapID
	, UpdatedDateTime = @Date
WHERE
	DOCode = @DOImportCode
	AND DeliveryCode = @Delivery
	AND ProductID = @ProductID
	AND BatchCode = @BatchCode

END