﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryNoteDetailSplits_Driver]
	@PalletCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_DocumentNbr NVARCHAR(50) = (SELECT TOP 1 ReferenceNbr FROM Pallets WITH (NOLOCK) WHERE PalletCode = @PalletCode)
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()

	--1.Update [Pallets] set trạng thái về N, sẵn sàng sử dụng
	--2.Update [StockReceivingDetailSplits] SET trạng thái là C, hoàn tất nhập hàng vào kho, thông tin xe nâng
	--3.Update [StockReceivingDetail] 
		--SET số pallet đã đưa lên kệ


--1.Update [Pallets] set trạng thái về N, sẵn sàng sử dụng
	UPDATE Pallets
	SET 
		[Status] = 'N'
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
		,[Driver] = @_UserID
	WHERE
		PalletCode = @_PalletCode

	UPDATE [dbo].[DeliveryNoteDetailSplits]
	SET 
		[Status] = 'C'
		, Driver = @_UserID
		, LocationPutDate = @_Date
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode
		AND ReferenceNbr = @_DocumentNbr
END