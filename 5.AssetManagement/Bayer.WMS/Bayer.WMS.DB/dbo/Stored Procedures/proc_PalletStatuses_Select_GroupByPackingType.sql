﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Select_GroupByPackingType]
	@PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @Barcode VARCHAR(255) = @PalletCode

	SELECT
		pl.PalletCode
		, pl.Status
		, p.PackingType
		, StrPackingType =	CASE p.PackingType
								WHEN 'C' THEN N'Thùng'
								WHEN 'B' THEN N'Bao'
								WHEN 'S' THEN N'Xô'
								WHEN 'A' THEN N'Can'
							END
		, Quantity = COUNT(DISTINCT CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN ps.CartonBarcode ELSE ps.ProductBarcode END)
	FROM
		dbo.PalletStatuses ps WITH (NOLOCK)
		JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
													AND p.IsDeleted = 0
	WHERE
		PS.PalletCode = @Barcode
		OR PS.ProductBarcode = @Barcode
		OR PS.EncryptedProductBarcode = @Barcode
		OR PS.CartonBarcode = @Barcode
	GROUP BY
		pl.PalletCode
		, pl.Status
		, p.PackingType
END