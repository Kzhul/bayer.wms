﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Packaging_Destroy]
	@Barcode VARCHAR(255)
	, @EncryptedBarcode VARCHAR(255)
	, @Type CHAR(1)
	, @ProductLot VARCHAR(255)
	, @ProductID INT
	, @PackagingDate DATETIME
	, @ProcessDate DATETIME
	, @UserID INT
	, @SitemapID INT
	, @Device VARCHAR(255)
	, @Note NVARCHAR(255)
	, @Status CHAR(1)
	, @CartonBarcode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_EncryptedBarcode VARCHAR(255) = @EncryptedBarcode
	DECLARE @_Type CHAR(1) = @Type
	DECLARE @_ProductLot VARCHAR(255) = @ProductLot
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_PackagingDate DATETIME = @PackagingDate
	DECLARE @_ProcessDate DATETIME = @ProcessDate
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Device VARCHAR(255) = @Device
	DECLARE @_Note NVARCHAR(255) = @Note
	DECLARE @_Status CHAR(1) = @Status
	DECLARE @_CartonBarcode VARCHAR(255) = @CartonBarcode

	DELETE FROM PackagingLogs WHERE Barcode = @_Barcode
	DELETE FROM PalletStatuses WHERE ProductBarcode = @_Barcode

	UPDATE ProductBarcodes SET Status = @_Status, CartonBarcode = '', PalletCode = '' WHERE Barcode = @_Barcode
	UPDATE CartonBarcodes SET Status = 'N' WHERE Barcode = @_CartonBarcode

	INSERT INTO dbo.PackagingErrors
	(
		Barcode
		, EncryptedBarcode
		, Type
		, ProductLot
		, ProductID
		, PackagingDate
		, ProcessDate
		, UserID
		, Device
		, Note
		, CreatedBy
		, CreatedBySitemapID
		, CreatedDateTime
		, UpdatedBy
		, UpdatedBySitemapID
		, UpdatedDateTime
	)
	VALUES
	(
		@_Barcode
		, @_EncryptedBarcode
		, @_Type
		, @_ProductLot
		, @_ProductID
		, @_PackagingDate
		, @_ProcessDate
		, @_UserID
		, @_Device
		, @_Note
		, @_UserID
		, @_SitemapID
		, GETDATE()
		, @_UserID
		, @_SitemapID
		, GETDATE()
	)

	IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.ProductBarcodes WHERE Barcode = @_Barcode)
		INSERT INTO dbo.ProductBarcodes
		(
			Barcode
			, EncryptedBarcode
			, ProductLot
			, ProductID
			, Status
			, CreatedBy
			, CreatedBySitemapID
			, CreatedDateTime
			, UpdatedBy
			, UpdatedBySitemapID
			, UpdatedDateTime
		)
		VALUES
		(
			@_Barcode
			, @_EncryptedBarcode
			, @_ProductLot
			, @_ProductID
			, @_Status
			, @_UserID
			, @_SitemapID
			, GETDATE()
			, @_UserID
			, @_SitemapID
			, GETDATE()
		)
END