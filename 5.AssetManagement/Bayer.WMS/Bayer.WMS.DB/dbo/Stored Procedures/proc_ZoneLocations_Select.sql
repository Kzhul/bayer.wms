﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ZoneLocations_Select]
	@LocationCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_LocationCode NVARCHAR(50) = @LocationCode

   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	   [LocationCode]
      ,[ZoneCode]
      ,[LineCode]
      ,[LocationName]
      ,[Description]
      ,[LengthID]
      ,[LevelID]
      ,[Status]
      ,[IsDeleted]
      ,[CreatedBy]
      ,[CreatedBySitemapID]
      ,[CreatedDateTime]
      ,[UpdatedBy]
      ,[UpdatedBySitemapID]
      ,[UpdatedDateTime]
      ,[RowVersion]
      ,[CurrentPalletCode]
FROM [dbo].[ZoneLocations] ZL WITH (NOLOCK)
WHERE
	LocationCode = @_LocationCode




END