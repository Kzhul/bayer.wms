﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_SelectPallet_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	IDSum = ROW_NUMBER() OVER ( ORDER BY PD.ProductID, srd.ExpiredDate ASC)
	,PalletCodeSuggest = PS.PalletCode
	,LocationCode = ISNULL(ZL.LocationName,'')
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,PS.ProductLot
	,P.Status
	,P.CompanyCode
	,srd.DeliveryDate
	,ExpiredDate = FORMAT(srd.ExpiredDate, 'dd/MM/yyyy')
	,RequestQty = ISNULL(RM.RequestQty,0) - ISNULL(RM.[PickedQty],0)
	,SUM(ISNULL(PS.Qty, 1)) AS PalletQuantity
	,NeedQuantity = 0
	,SumHaveQuantity = 0
INTO #TBPick
FROM
	RequestMaterialLines RM WITH (NOLOCK) 
	LEFT JOIN PalletStatuses PS WITH (NOLOCK) 
		ON RM.ProductID = PS.ProductID
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON PS.PalletCode = P.PalletCode
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON PS.ProductID = PD.ProductID
	LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
		ON srd.ProductID = ps.ProductID
		AND srd.BatchCode = ps.ProductLot
	LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
		ON P.LocationCode = ZL.LocationCode
WHERE
	P.Status = 'N'
	AND 
	RM.DocumentNbr = @_DocumentNbr
	AND srd.ExpiredDate IS NOT NULL
	AND srd.ExpiredDate > GETDATE()
	AND P.PalletCode NOT IN (
		SELECT DISTINCT PalletCode
		FROM [dbo].[RequestMaterialLineSplits] WITH (NOLOCK)
		WHERE
			DocumentNbr = @_DocumentNbr
	)
GROUP BY
	PS.PalletCode
	,PD.ProductID
	,PS.ProductLot
	,P.Status
	,P.CompanyCode
	,srd.DeliveryDate
	,srd.ExpiredDate
	,RM.RequestQty
	,RM.[PickedQty]
	,PD.ProductID
	,PD.ProductCode
	,PD.Description
	,ISNULL(ZL.LocationName,'')
ORDER BY
	PD.ProductID
	,srd.ExpiredDate ASC

UPDATE #TBPick
SET SumHaveQuantity = (SELECT SUM(PalletQuantity) FROM #TBPick AS B WHERE B.ProductID = #TBPick.ProductID AND B.IDSum <= #TBPick.IDSum)
	
UPDATE #TBPick
SET SumHaveQuantity = 0
WHERE (SumHaveQuantity - PalletQuantity) >= RequestQty

UPDATE #TBPick
SET NeedQuantity = PalletQuantity

SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY ProductName, PalletCodeSuggest ASC)
	,* 
FROM #TBPick
WHERE
	SumHaveQuantity > 0
ORDER BY 
	ProductName, PalletCodeSuggest
END