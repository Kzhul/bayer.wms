﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_BarcodeBankManager_Insert]
	@UserID INT
	,@ProductCode NVARCHAR(50)
	,@Description NVARCHAR(250)
	,@FileName NVARCHAR(550)
	,@Year INT
	,@Quantity INT
	,@SequenceFrom INT
	,@SequenceTo INT
AS
BEGIN
	SET NOCOUNT ON

INSERT INTO [dbo].[BarcodeBankManager]
           ([UserID]
           ,[ProductCode]
           ,[Description]
           ,[FileName]
           ,[Year]
		   ,[Quantity]
           ,[SequenceFrom]
           ,[SequenceTo]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
     VALUES
           (@UserID
           ,@ProductCode
           ,@Description
           ,@FileName
           ,@Year
		   ,@Quantity
           ,@SequenceFrom
           ,@SequenceTo
           ,@UserID
           ,1
           ,GETDATE()
           ,@UserID
           ,1
           ,GETDATE()
	)
END