﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SelectDeliveryByPallet_SumByPackingType]
	@Barcode VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @Barcode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	DECLARE @Carton VARCHAR(255) = ''
	DECLARE @Bag VARCHAR(255) = ''
	DECLARE @Shove VARCHAR(255) = ''
	DECLARE @Can VARCHAR(255) = ''
	DECLARE @StrQuantity VARCHAR(255) = ''

	--Lấy sản phẩm trên pallet đang soạn hàng trong kho
	IF EXISTS(
		SELECT TOP 1 * FROM Pallets WITH (NOLOCK) WHERE PalletCode = @_PalletCode AND CompanyCode = @_CompanyCode
	)
	BEGIN
		SET @Carton = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'C')
		SET @Bag = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'B')
		SET @Shove = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'S')
		SET @Can = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'A')

		SET @StrQuantity = ''
		IF(@Carton != '')
			SET @StrQuantity = @StrQuantity + @Carton  + ' ; '
		IF(@Bag != '')
			SET @StrQuantity = @StrQuantity + @Bag  + ' ; '
		IF(@Shove != '')
			SET @StrQuantity = @StrQuantity + @Shove + ' ; '
		IF(@Can != '')
			SET @StrQuantity = @StrQuantity + @Can + ' ; '

		SELECT TOP 1
			# = ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC),
			P.PalletCode 
			,StrQuantity = @StrQuantity
			,UserFullName = U.LastName + ' ' + U.FirstName
			,StrStatus = CASE WHEN P.[Status] = 'N' THEN N'Mới'
							WHEN P.[Status] = 'P' THEN N'Soạn hàng'
							ELSE '' END
			,P.CompanyCode
			,CompanyName = (SELECT TOP 1 CompanyName FROM Companies WITH (NOLOCK) WHERE CompanyCode = P.CompanyCode)
		FROM 
			Pallets P WITH (NOLOCK)
			LEFT JOIN Users U WITH (NOLOCK)
				ON P.UpdatedBy = U.UserID
		WHERE 
			P.PalletCode = @_PalletCode
		ORDER BY P.PalletCode
	END
	ELSE
	BEGIN
		SET @Carton = dbo.[fnSelectQuantityByPackagingTypeEmptyForDeliveredPallet](@_PalletCode,@_CompanyCode,@_DOImportCode,'C')
		SET @Bag = dbo.[fnSelectQuantityByPackagingTypeEmptyForDeliveredPallet](@_PalletCode,@_CompanyCode,@_DOImportCode,'B')
		SET @Shove = dbo.[fnSelectQuantityByPackagingTypeEmptyForDeliveredPallet](@_PalletCode,@_CompanyCode,@_DOImportCode,'S')
		SET @Can = dbo.[fnSelectQuantityByPackagingTypeEmptyForDeliveredPallet](@_PalletCode,@_CompanyCode,@_DOImportCode,'A')

		SET @StrQuantity = ''
		IF(@Carton != '')
			SET @StrQuantity = @StrQuantity + @Carton  + ' ; '
		IF(@Bag != '')
			SET @StrQuantity = @StrQuantity + @Bag  + ' ; '
		IF(@Shove != '')
			SET @StrQuantity = @StrQuantity + @Shove + ' ; '
		IF(@Can != '')
			SET @StrQuantity = @StrQuantity + @Can + ' ; '

		SELECT TOP 1
			# = ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC),
			P.PalletCode 
			,StrQuantity = @StrQuantity
			,UserFullName = U.LastName + ' ' + U.FirstName
			,StrStatus = N'Đã giao'
			,P.CompanyCode
			,CompanyName = (SELECT TOP 1 CompanyName FROM Companies WITH (NOLOCK) WHERE CompanyCode = P.CompanyCode)
		FROM 
			DeliveryHistories P WITH (NOLOCK)
			LEFT JOIN Users U WITH (NOLOCK)
				ON P.UpdatedBy = U.UserID
		WHERE 
			P.PalletCode = @_PalletCode
			AND P.CompanyCode = @_CompanyCode
			AND P.DeliveryTicketCode = @_DOImportCode
		ORDER BY P.PalletCode

	END
END