﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_DeliveryNoteHeader]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method
DECLARE @_ExportCode NVARCHAR(50) = (SELECT TOP 1 ExportCode FROM DeliveryNoteHeaders WHERE DOImportCode = @_DOImportCode)
DECLARE @_DeliveryDate DATE = (SELECT TOP 1 DeliveryDate FROM DeliveryOrderDetails WHERE DOImportCode = @_DOImportCode)
--------------SOURCE-------------------
IF NOT EXISTS (SELECT * FROM DeliveryNoteHeaders WHERE DOImportCode = @_DOImportCode)
BEGIN
	INSERT INTO [dbo].[DeliveryNoteHeaders]
           ([ExportCode]
           ,[ExportDate]
           ,[DOImportCode]
           ,[Description]
           ,[DeliveryDate]
           ,[Status]
           ,[IsDeleted]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
     VALUES
           (
		   dbo.[fnTicketNextNBR]('XH')
           ,GETDATE()
           ,@_DOImportCode
           ,''
           ,@_DeliveryDate
           ,'N'
           ,0
			,@_UserID--SOURCE.[CreatedBy]
			,@_SitemapID--SOURCE.[CreatedBySitemapID]
			,@_SyncDate--SOURCE.[CreatedDateTime]
			,@_UserID--SOURCE.[UpdatedBy]
			,@_SitemapID--[UpdatedBySitemapID]
			,@_SyncDate--[UpdatedDateTime]
			)
END
END