﻿CREATE PROCEDURE [dbo].[proc_Bay4SData_Insert]
	@ProductCode NVARCHAR(250)
	,@ProductName NVARCHAR(250)
	,@BatchCode NVARCHAR(250)
	,@BatchCodeDistributor NVARCHAR(250)
	,@UOM NVARCHAR(250)
	,@ExpiredDate DATETIME
	,@Unrestricted NVARCHAR(250)
	,@InQualityInsp NVARCHAR(250)
	,@Blocked NVARCHAR(250)
	,@ManufacturingDate DATETIME
	,@UserID INT
AS
BEGIN
	SET NOCOUNT ON;

    
INSERT INTO [dbo].[Bay4SData]
           ([ProductCode]
           ,[ProductName]
           ,[BatchCode]
           ,[BatchCodeDistributor]
           ,[UOM]
           ,[ExpiredDate]
           ,[Unrestricted]
           ,[InQualityInsp]
           ,[Blocked]
           ,[ManufacturingDate]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
     VALUES
           (@ProductCode
           ,@ProductName
           ,@BatchCode
           ,@BatchCodeDistributor
           ,@UOM
           ,@ExpiredDate
           ,@Unrestricted
           ,@InQualityInsp
           ,@Blocked
           ,@ManufacturingDate
           ,@UserID
           ,1
           ,GETDATE()
           ,@UserID
           ,1
           ,GETDATE())
END