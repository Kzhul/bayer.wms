﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_MoveProductLot]
	@PalletSource VARCHAR(255)
	, @PalletCode VARCHAR(255)
	, @Barcode VARCHAR(255)
	, @Type CHAR(1)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
    DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type
	DECLARE @_PalletSource VARCHAR(255) = @PalletSource
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'MOV:' + @_Barcode + '->' + @_PalletCode
	
	UPDATE dbo.PalletStatuses
	SET
		PalletCode = @_PalletCode
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletSource
		AND ProductLot = @_Barcode

	SELECT * 
	FROM
		PalletStatuses WITH (NOLOCK)
	WHERE
		PalletCode = @_PalletCode
		AND ProductLot = @_Barcode

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PalletStatus">' + 
			(SELECT * FROM dbo.PalletStatuses ps WHERE (@_Type = 'C' AND ps.CartonBarcode = @_Barcode) OR (@_Type = 'E' AND ps.ProductBarcode = @_Barcode) FOR XML PATH('PalletStatus')) + 
		'</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'PalletStatuses', @_Data, 'UPD', @_Method, @_Date, @_Date

	--CLear pallet if not have any product
	IF NOT EXISTS (SELECT TOP 1 PalletCode FROM dbo.PalletStatuses WHERE PalletCode = @PalletSource)
		EXEC dbo.proc_Pallets_Update_Status @PalletSource, @PalletSource, NULL, NULL, 'N', @_UserID, @_SitemapID, @_Method

	--UPDATE PreparedQty
	EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity] @_PalletSource
	EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity] @_PalletCode
END