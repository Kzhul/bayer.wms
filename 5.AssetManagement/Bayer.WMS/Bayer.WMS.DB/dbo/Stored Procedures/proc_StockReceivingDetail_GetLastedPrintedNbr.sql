﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceivingDetail_GetLastedPrintedNbr]
	@DocumentNbr NVARCHAR(50)
	,@BatchCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_BatchCode NVARCHAR(50) = @BatchCode
	
	SELECT TOP 1
		CONVERT(NVARCHAR(20),BatchCodePrintedNbr)
	FROM	
		[dbo].[StockReceivingDetails] WITH (NOLOCK)
	WHERE
		[StockReceivingCode] = @_DocumentNbr
		AND BatchCode = @_BatchCode
END