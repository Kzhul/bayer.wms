﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Users_Update_Password]
	@UserID INT
	, @Password VARCHAR(255)
	, @PasswordChangeOnNextLogin BIT
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_UserID INT = @UserID
	DECLARE @_Password VARCHAR(255) = @Password
	DECLARE @_PasswordChangeOnNextLogin BIT = @PasswordChangeOnNextLogin

	UPDATE dbo.Users
	SET
		Password = @_Password
		, PasswordChangeOnNextLogin = @_PasswordChangeOnNextLogin
	WHERE
		UserID = @_UserID

	EXEC dbo.proc_UsersPasswords_Insert_LastPassword
		@UserID = @_UserID
		, @Password = @_Password
	
END