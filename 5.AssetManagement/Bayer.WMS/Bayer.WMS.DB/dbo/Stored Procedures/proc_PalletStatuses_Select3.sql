﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Select3]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
	, @ProductID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type
	DECLARE @_ProductID INT = @ProductID

	--Mã pallet
	--Số thùng
	--Số SP
	--Số SP lẻ
	--Danh sách thùng
	--Danh sách SP lẻ

	SELECT 
		# = ROW_NUMBER() OVER(ORDER BY ProductDescription ASC, CartonBarcode ASC, ProductQty DESC),
		* 
	FROM
	(
		SELECT DISTINCT
			pl.PalletCode
			, pl.DOImportCode
			, pl.Status
			, pl.ReferenceNbr
			, pl.CompanyCode
			, tmp.CartonBarcode
			, tmp.ProductLot
			, tmp.ProductQty
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
		FROM
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, ps.CartonBarcode
					, ProductQty = COUNT(DISTINCT ProductBarcode)
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
					AND CartonBarcode IS NOT NULL 
					AND CartonBarcode != ''
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, ps.CartonBarcode
			)tmp
			JOIN dbo.Pallets pl WITH (NOLOCK) 
				ON pl.PalletCode = tmp.PalletCode
			LEFT JOIN dbo.Products p WITH (NOLOCK) 
				ON p.ProductID = tmp.ProductID
				AND p.IsDeleted = 0	

		UNION

		SELECT DISTINCT
			pl.PalletCode
			, pl.DOImportCode
			, pl.Status
			, pl.ReferenceNbr
			, pl.CompanyCode
			, tmp.CartonBarcode
			, tmp.ProductLot
			, tmp.ProductQty
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
		FROM
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonBarcode = ps.EncryptedProductBarcode
					, ProductQty = 1
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
					AND (ps.CartonBarcode IS NULL OR ps.CartonBarcode = '')
			)tmp
			JOIN dbo.Pallets pl WITH (NOLOCK) 
				ON pl.PalletCode = tmp.PalletCode
			LEFT JOIN dbo.Products p WITH (NOLOCK) 
				ON p.ProductID = tmp.ProductID
				AND p.IsDeleted = 0
	) AS A
	--WHERE
	--	ProductID = @_ProductID
	ORDER BY 
		ProductDescription ASC, 
		CartonBarcode ASC,								  
		ProductQty DESC

END