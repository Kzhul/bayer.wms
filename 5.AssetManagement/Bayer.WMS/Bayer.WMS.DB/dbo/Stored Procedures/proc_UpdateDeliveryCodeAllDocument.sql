﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[proc_UpdateDeliveryCodeAllDocument]
	@DOImportCode NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON
	--DECLARE @DOImportCode NVARCHAR(50)

	--[PrepareNoteDetails]
	UPDATE PND
	SET 
		PND.DeliveryCode = DOD.Delivery
	FROM 
		[dbo].[DeliveryOrderDetails] DOD
		JOIN [dbo].[PrepareNoteHeaders] PNH
			ON DOD.DOImportCode = PNH.DOImportCode
		JOIN [dbo].[PrepareNoteDetails] PND
			ON DOD.BatchCode = PND.BatchCode
			AND DOD.ProductID = PND.ProductID
			AND DOD.CompanyCode = PNH.CompanyCode
			AND DOD.DeliveryDate = PNH.DeliveryDate
			AND DOD.DOImportCode = PND.DOCode
			AND PNH.PrepareCode = PND.PrepareCode
		WHERE 
			DOD.DOImportCode = @DOImportCode


	--[SplitNoteDetails]
	UPDATE PND
	SET 
		PND.DeliveryCode = DOD.Delivery
	FROM 
		[dbo].[DeliveryOrderDetails] DOD
		JOIN [dbo].SplitNoteHeaders PNH
			ON DOD.DOImportCode = PNH.DOImportCode
		JOIN [dbo].SplitNoteDetails PND
			ON DOD.BatchCode = PND.BatchCode
			AND DOD.ProductID = PND.ProductID
			AND DOD.CompanyCode = PND.CompanyCode
			AND DOD.DeliveryDate = PNH.DeliveryDate
			AND DOD.DOImportCode = PND.DOCode
			AND PNH.SplitCode = PND.SplitCode
		WHERE 
			DOD.DOImportCode = @DOImportCode

	--ReturnDODetails
	UPDATE PND
	SET 
		PND.Delivery = DOD.Delivery
	FROM 
		[dbo].[DeliveryOrderDetails] DOD
		JOIN [dbo].ReturnDOHeaders PNH
			ON DOD.DOImportCode = PNH.DOImportCode
		JOIN [dbo].ReturnDODetails PND
			ON DOD.BatchCode = PND.BatchCode
			AND DOD.ProductID = PND.ProductID
			AND DOD.CompanyCode = PND.CompanyCode
			AND DOD.DeliveryDate = PND.DeliveryDate
			AND DOD.DOImportCode = PND.DOImportCode
			AND PNH.ReturnDOCode = PND.ReturnDOCode
		WHERE 
			DOD.DOImportCode = @DOImportCode

	--ReturnDOHeaders
	UPDATE PNH
	SET 
		PNH.Delivery = DOD.Delivery
	FROM 
		[dbo].[DeliveryOrderDetails] DOD
		JOIN [dbo].ReturnDOHeaders PNH
			ON DOD.DOImportCode = PNH.DOImportCode
		JOIN [dbo].ReturnDODetails PND
			ON DOD.BatchCode = PND.BatchCode
			AND DOD.ProductID = PND.ProductID
			AND DOD.CompanyCode = PND.CompanyCode
			AND DOD.DeliveryDate = PND.DeliveryDate
			AND DOD.DOImportCode = PND.DOImportCode
			AND PNH.ReturnDOCode = PND.ReturnDOCode
		WHERE 
			DOD.DOImportCode = @DOImportCode

END