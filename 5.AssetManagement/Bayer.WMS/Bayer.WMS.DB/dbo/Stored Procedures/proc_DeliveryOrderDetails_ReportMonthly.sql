﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_ReportMonthly] '2017-11-01','2017-11-30'
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_ReportMonthly]
	@FromDate VARCHAR(255)
	, @ToDate DATE
AS
BEGIN
	SET NOCOUNT ON

	---------HARCODE HERE FOR DEMO ONLY-----------
	SET @FromDate = '2017-11-01'
	SET @ToDate = '2017-11-30'

	DECLARE @_FromDate Date = @FromDate
	DECLARE @_ToDate DATE = @ToDate

	SELECT DISTINCT
		DO.DeliveryDate
		,ProductLot = DO.BatchCode
		,DOQuantity = SUM(DO.Quantity)
	INTO #DO
	FROM 
		DeliveryOrderDetails DO WITH(NOLOCK)
	WHERE 
		DeliveryDate >= @_FromDate
		AND DeliveryDate <= @_ToDate
	GROUP BY
		DO.DeliveryDate
		,BatchCode

	CREATE CLUSTERED INDEX #idx_DO ON #DO(DeliveryDate, ProductLot)  

	SELECT DISTINCT
		DO.DeliveryDate
		,ProductLot
		,DeliveryQuantity = COUNT(DISTINCT ProductBarcode)
	INTO #DN
	FROM 
		DeliveryHistories DO WITH(NOLOCK)
	WHERE 
		DeliveryDate >= @_FromDate
		AND DeliveryDate <= @_ToDate
	GROUP BY
		DO.DeliveryDate
		,ProductLot

	CREATE CLUSTERED INDEX #idx_DN ON #DN(DeliveryDate, ProductLot)  

	SELECT DISTINCT
		DO.DeliveryDate
		,DOQuantity = SUM(DO.DOQuantity)
		,DeliveryQuantity = SUM(DN.DeliveryQuantity)
		,DeliveryPercent = FORMAT(CASE WHEN SUM(DO.DOQuantity) > 0.00 AND SUM(DO.DOQuantity) > SUM(DN.DeliveryQuantity)
								THEN SUM(DN.DeliveryQuantity) * 100.00 / SUM(DO.DOQuantity)
								WHEN SUM(DO.DOQuantity) < SUM(DN.DeliveryQuantity)
								THEN 100.00
								ELSE 0.00 END,'N2')
	FROM
		#DO DO
		JOIN #DN DN
			ON DO.DeliveryDate = DN.DeliveryDate
			AND DO.ProductLot = DN.ProductLot
			AND DN.DeliveryQuantity > 0
	GROUP BY
		DO.DeliveryDate
	ORDER BY DO.DeliveryDate
END