﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SelectByPackingType]
	@PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @Carton VARCHAR(255) = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'C')
	DECLARE @Bag VARCHAR(255) = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'B')
	DECLARE @Shove VARCHAR(255) = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'S')
	DECLARE @Can VARCHAR(255) = dbo.[fnSelectQuantityByPackagingTypeEmpty](@_PalletCode,'A')
	DECLARE @StrQuantity VARCHAR(255) = ''
	IF(@Carton != '')
		SET @StrQuantity = @StrQuantity + @Carton 
	IF(@Bag != '')
		SET @StrQuantity = @StrQuantity + ' ; ' + @Bag 
	IF(@Shove != '')
		SET @StrQuantity = @StrQuantity + ' ; ' + @Shove
	IF(@Can != '')
		SET @StrQuantity = @StrQuantity + ' ; ' + @Can

	SELECT TOP 1
		# = ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC),
		P.PalletCode 
		,StrQuantity = @StrQuantity
		,UserFullName = U.LastName + ' ' + U.FirstName
	FROM 
		Pallets P WITH (NOLOCK)
		LEFT JOIN Users U WITH (NOLOCK)
			ON P.UpdatedBy = U.UserID
	WHERE 
		P.PalletCode = @_PalletCode
	ORDER BY P.PalletCode
END