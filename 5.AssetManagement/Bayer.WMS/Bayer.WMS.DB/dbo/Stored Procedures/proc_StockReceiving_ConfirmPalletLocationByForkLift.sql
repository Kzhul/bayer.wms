﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceiving_ConfirmPalletLocationByForkLift]
	@PalletCode VARCHAR(255)
	, @LocationCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_LocationCode VARCHAR(255) = @LocationCode
	DECLARE @_DocumentNbr NVARCHAR(50) = (SELECT TOP 1 ReferenceNbr FROM Pallets WITH (NOLOCK) WHERE PalletCode = @PalletCode)
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()

	--1.Update [Pallets] set trạng thái về N, sẵn sàng sử dụng
	--2.Update [StockReceivingDetailSplits] SET trạng thái là C, hoàn tất nhập hàng vào kho, thông tin xe nâng
	--3.Update [StockReceivingDetail] 
		--SET số pallet đã đưa lên kệ


--1.Update [Pallets] set trạng thái về N, sẵn sàng sử dụng
	UPDATE Pallets
	SET 
		[Status] = 'N'
	WHERE
		PalletCode = @_PalletCode

--------------------- Process for Stock Receiving
IF EXISTS(SELECT * FROM StockReceivingDetailSplits WHERE StockReceivingCode = @_DocumentNbr)
BEGIN
	DECLARE @_ProductLot VARCHAR(255) = (SELECT TOP 1 ProductLot FROM StockReceivingDetailSplits WITH (NOLOCK) WHERE PalletCode = @PalletCode AND StockReceivingCode = @_DocumentNbr)
	--2.Update [StockReceivingDetailSplits] SET trạng thái là C, hoàn tất nhập hàng vào kho, thông tin xe nâng
	UPDATE StockReceivingDetailSplits
	SET 
		[Status] = 'C'
		, ForkliftID = @_UserID
		, ForkliftDateTime = @_Date
		, SetLocationDateTime = @_Date
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode
		AND StockReceivingCode = @_DocumentNbr
		AND ProductLot = @_ProductLot

--3.Update [StockReceivingDetail] 
	--SET số pallet đã đưa lên kệ
	UPDATE dbo.StockReceivingDetails
	SET
		PalletSetLocation = (SELECT COUNT(PalletCode) FROM  StockReceivingDetailSplits WITH (NOLOCK) WHERE StockReceivingCode = @_DocumentNbr AND ProductLot = @_ProductLot AND SetLocationDateTime IS NOT NULL )
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		StockReceivingCode = @_DocumentNbr
		AND BatchCode = @_ProductLot


	--Update AUDIT REASON and OLD QUANTITY HERE
END

------------------- Process for Stock Return
IF EXISTS(SELECT * FROM StockReturnDetails WHERE StockReturnCode = @_DocumentNbr)
BEGIN
	--2.Update [StockReceivingDetailSplits] SET trạng thái là C, hoàn tất nhập hàng vào kho, thông tin xe nâng
	UPDATE StockReturnDetails
	SET 
		[Status] = 'C'
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode
		AND StockReturnCode = @_DocumentNbr
END

END