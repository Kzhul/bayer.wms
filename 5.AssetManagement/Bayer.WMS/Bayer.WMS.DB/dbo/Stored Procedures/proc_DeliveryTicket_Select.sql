﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryTicket_Select]
	@DeliveryTicketCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DeliveryTicketCode VARCHAR(255) = @DeliveryTicketCode
	DECLARE @_DOImportCode VARCHAR(MAX)

	SET @_DOImportCode = (
	SELECT
		pnh.DOImportCode + ','
	FROM
		dbo.DeliveryTicketDetails dtd WITH (NOLOCK)
		JOIN dbo.PrepareNoteHeaders pnh WITH (NOLOCK) ON pnh.PrepareCode = dtd.PrepareCode
	WHERE
		dtd.DeliveryTicketCode = @_DeliveryTicketCode
	FOR	XML	PATH(''))

	SELECT
		dth.DeliveryTicketCode
		, dth.DeliveryDate
		, dth.CompanyCode
		, dth.CompanyName
		, TruckNo = ISNULL(dth.TruckNo, '')
		, DOImportCode = @_DOImportCode
		, dth.TotalBoxes
		, DeliveredBoxes = 0
		, dth.TotalBags
		, DeliveredBags = 0
		, dth.TotalBuckets
		, DeliveredBuckets = 0
		, dth.TotalCans
		, DeliveredCans = 0
	FROM
		dbo.DeliveryTicketHeaders dth WITH (NOLOCK)
	WHERE
		dth.DeliveryTicketCode = @_DeliveryTicketCode
END