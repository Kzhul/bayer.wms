﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_SelectPallet]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ASC)
	,PalletCode = PS.PalletCode
	,LocationCode = ISNULL(ZL.LocationName,'')
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,PS.ProductLot
	,P.Status
	,P.CompanyCode
	,DeliveryDate = R.[Date]
	,ExpiredDate = FORMAT(SR.ExpiryDate, 'dd/MM/yyyy')
	,RequestQty = ISNULL(RL.RequestQty,0)
	,PalletQuantity = PS.Quantity
	,NeedQuantity = ISNULL(RL.RequestQty,0) - ISNULL(RL.[PickedQty],0)
FROM
	[RequestMaterialLineSplits] PS WITH (NOLOCK) 
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON PS.ProductID = PD.ProductID
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON PS.PalletCode = P.PalletCode
	LEFT JOIN RequestMaterials R  WITH (NOLOCK) 
		ON PS.DocumentNbr = R.DocumentNbr
	LEFT JOIN RequestMaterialLines RL WITH (NOLOCK) 
		ON PS.DocumentNbr = RL.DocumentNbr
		AND PS.ProductID = RL.ProductID
	LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
		ON P.LocationCode = ZL.LocationCode
	LEFT JOIN StockReceivingDetails SR WITH (NOLOCK) 
		ON PS.ProductLot = SR.BatchCode
		AND PS.ProductID = SR.ProductID
WHERE
	PS.DocumentNbr = @_DocumentNbr
ORDER BY
	PD.Description ASC
END