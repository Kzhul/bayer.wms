﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_DeliveryOrderSum]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN

DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method

	
--UPDATE dbo.DeliveryOrderSum
--SET [DOQuantity] = 0
--WHERE 
--	DOImportCode = @_DOImportCode

--------------SOURCE-------------------
SELECT DISTINCT
	[DOImportCode]
	,[CompanyCode]
	,[ProductID]
	,[BatchCode]
	,[DeliveryDate]
	,[DOQuantity] = SUM([Quantity])
	,[PreparedQty] = 0
	,[DeliveredQty] = 0
INTO #SOURCE
FROM
	DeliveryOrderDetails WITH (NOLOCK)
WHERE
	DOImportCode = @DOImportCode
	AND ProductID != 0
GROUP BY 
	[DOImportCode]
	,[CompanyCode]
	,[ProductID]
	,[BatchCode]
	,[DeliveryDate]


--Synchronize the target table with
--refreshed data from source table
;
WITH TARGET AS 
(
    SELECT * 
    FROM dbo.DeliveryOrderSum
    WHERE 
		DOImportCode = @_DOImportCode
)
MERGE INTO TARGET
USING #SOURCE AS SOURCE 
ON 
	TARGET.[DOImportCode] = SOURCE.[DOImportCode]
	AND TARGET.[CompanyCode] = SOURCE.[CompanyCode]
	AND TARGET.[ProductID] = SOURCE.[ProductID]
	AND TARGET.[DeliveryDate] = SOURCE.[DeliveryDate]
	AND TARGET.[BatchCode] = SOURCE.[BatchCode]
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND 
	(
		TARGET.[DOQuantity] <> SOURCE.[DOQuantity]
	)
	THEN
	UPDATE SET 
		TARGET.[DOQuantity] = SOURCE.[DOQuantity]
		,TARGET.[UpdatedBy] = @_UserID
		,TARGET.[UpdatedBySitemapID] = @_SitemapID
		,TARGET.[UpdatedDateTime] = @_SyncDate
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET 
	THEN 
		INSERT (
			[DOImportCode]
			,[CompanyCode]
			,[ProductID]
			,[BatchCode]
			,[DeliveryDate]
			,[DOQuantity]
			,[PreparedQty]
			,[DeliveredQty]
			,[CreatedBy]
			,[CreatedBySitemapID]
			,[CreatedDateTime]
			,[UpdatedBy]
			,[UpdatedBySitemapID]
			,[UpdatedDateTime]
			) 
		VALUES (
			SOURCE.[DOImportCode]
			,SOURCE.[CompanyCode]
			,SOURCE.[ProductID]
			,SOURCE.[BatchCode]
			,SOURCE.[DeliveryDate]
			,SOURCE.[DOQuantity]
			,SOURCE.[PreparedQty]
			,SOURCE.[DeliveredQty]
			,@_UserID--SOURCE.[CreatedBy]
			,@_SitemapID--SOURCE.[CreatedBySitemapID]
			,@_SyncDate--SOURCE.[CreatedDateTime]
			,@_UserID--SOURCE.[UpdatedBy]
			,@_SitemapID--[UpdatedBySitemapID]
			,@_SyncDate--[UpdatedDateTime]
			)
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table
WHEN NOT MATCHED BY SOURCE 
	THEN 
		UPDATE SET 
		TARGET.[DOQuantity] = 0
		,TARGET.[UpdatedBy] = @_UserID
		,TARGET.[UpdatedBySitemapID] = @_SitemapID
		,TARGET.[UpdatedDateTime] = @_SyncDate
		;
SELECT @@ROWCOUNT;

--DROP TABLE #SOURCE
DROP TABLE #SOURCE
END