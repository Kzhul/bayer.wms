﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_Tracking]
AS
BEGIN
	SET NOCOUNT ON

SELECT DISTINCT
	P.PalletCode
	, P.LocationCode	
	, ZL.LocationName
	, P.LocationPutDate
	, strWarehouseVerifyDate = FORMAT(P.WarehouseVerifyDate,'dd/MM/yyyy HH:mm:ss')
	, P.WarehouseKeeper
	, WarehouseKeeperName = UW.LastName + '' + UW.FirstName
	, strLocationPutDate = FORMAT(P.LocationPutDate,'dd/MM/yyyy HH:mm:ss')
	, P.Driver
	, DriverName = U.LastName + '' + U.FirstName
	, P.LocationSuggestion
	, LocationSuggestionName = ZLS.LocationName
	, [ManufacturingDate] = PP.[ManufacturingDate]
	, [ExpiryDate] = PP.[ExpiryDate]
	, [strManufacturingDate] = FORMAT(PP.[ManufacturingDate],'dd/MM/yyyy')
	, [strExpiryDate] = FORMAT(PP.[ExpiryDate],'dd/MM/yyyy')
	, PP.ProductID
	, PD.ProductCode
	, PD.Description AS ProductDescription
	, PP.ProductLot
	, PP.Note
	, PP.[PackageSize]
	, PP.[UOM]
	, COUNT(DISTINCT PS.CartonBarcode) AS CartonQuantity
	, COUNT(DISTINCT PS.ProductBarCode) AS Quantity
	, ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
FROM 
	[ProductionPlans] PP WITH (NOLOCK)
	JOIN PalletStatuses PS WITH (NOLOCK)
		ON PP.ProductLot = PS.ProductLot
	JOIN Pallets P WITH (NOLOCK)
		ON PS.PalletCode = P.PalletCode
	JOIN Products PD WITH (NOLOCK)
		ON PP.ProductID = PD.ProductID
	LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
		ON P.LocationCode = ZL.LocationCode
	LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
		ON P.LocationSuggestion = ZLS.LocationCode
	LEFT JOIN Users U WITH (NOLOCK)
		ON U.UserID = P.Driver
	LEFT JOIN Users UW WITH (NOLOCK)
		ON UW.UserID = P.WarehouseKeeper
WHERE 
	PP.[Status] = 'C'
	AND P.PalletCode NOT LIKE '%99999'
	--AND (P.LocationCode IS NULL or P.LocationCode = '')
GROUP BY
	P.PalletCode
	, PP.[ManufacturingDate]
	, PP.[ExpiryDate]
	, PP.[PackageSize]
	, PP.[UOM]
	, PP.ProductID
	, PD.ProductCode
	, PD.Description
	, PP.ProductLot
	, PP.Note
	, P.LocationCode
	, P.LocationSuggestion
	, ZL.LocationName
	, ZLS.LocationName
	, P.LocationPutDate
	, P.Driver
	, U.FirstName
	, U.LastName
	, P.WarehouseKeeper
	, P.WarehouseVerifyDate
	, UW.FirstName
	, UW.LastName
ORDER BY
	PD.Description
	, PP.ProductLot
	, P.LocationCode

END