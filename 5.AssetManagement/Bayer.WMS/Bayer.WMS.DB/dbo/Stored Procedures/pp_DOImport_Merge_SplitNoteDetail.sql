﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_SplitNoteDetail]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN

DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method
DECLARE @_ExportCode NVARCHAR(50) = (SELECT TOP 1 SplitCode FROM SplitNoteHeaders WHERE DOImportCode = @_DOImportCode)
--------------SOURCE-------------------
SELECT DISTINCT
	DOImportCode
	,ProductID
	,BatchCode
	,CompanyCode
	,Quantity = SUM(DOQuantity)
INTO #SOURCE
FROM
	[DeliveryOrderSum] 
WHERE
	DOImportCode = @DOImportCode
GROUP BY 
	DOImportCode
	,ProductID
	,BatchCode
	,CompanyCode


--Synchronize the target table with
--refreshed data from source table
;
WITH TARGET AS 
(
    SELECT * 
    FROM dbo.SplitNoteDetails
    WHERE 
		DOCode = @_DOImportCode
)
MERGE INTO TARGET
USING #SOURCE AS SOURCE 
ON 
	TARGET.[DOCode] = SOURCE.[DOImportCode]
	AND TARGET.[ProductID] = SOURCE.[ProductID]
	AND TARGET.[BatchCode] = SOURCE.[BatchCode]
	AND TARGET.[CompanyCode] = SOURCE.[CompanyCode]
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND 
		(
			TARGET.[Quantity] <> SOURCE.[Quantity]
		)
	THEN
	UPDATE SET 
		TARGET.[LastQuantity] = TARGET.[Quantity]
		,TARGET.[Quantity] = SOURCE.[Quantity]
		,TARGET.[PackQuantity] = dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 1)
		,TARGET.[PackType] = [dbo].[fnPackagingType] (SOURCE.[ProductID])
		,TARGET.[ProductQuantity] = dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 0)
		,TARGET.[UpdatedBy] = @_UserID
		,TARGET.[UpdatedBySitemapID] = @_SitemapID
		,TARGET.[UpdatedDateTime] = @_SyncDate
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET 
	THEN 
		INSERT (
			[SplitCode]
			,[DOCode]
			,[DeliveryCode]
			,[ProductID]
			,[BatchCode]
			,[BatchCodeDistributor]
			,[CompanyCode]
			,[CompanyName]
			,[Province]
			,[Quantity]
			,[PackQuantity]
			,[PackType]
			,[ProductQuantity]
			,[SplittedQty]
			,[RequireReturnQty]
			,[ReturnedQty]
			,[Status]
			,[CreatedBy]
			,[CreatedBySitemapID]
			,[CreatedDateTime]
			,[UpdatedBy]
			,[UpdatedBySitemapID]
			,[UpdatedDateTime]
			--,[RowVersion]
			,[LastQuantity]
			) 
		VALUES (
			@_ExportCode--SOURCE.[ExportCode]
			,SOURCE.[DOImportCode]
			,''--SOURCE.[Delivery]
			,SOURCE.[ProductID]
			,SOURCE.[BatchCode]
			,''--SOURCE.[BatchCodeDistributor]
			,SOURCE.[CompanyCode]--[CompanyCode]
			,(SELECT TOP 1 [CompanyName] FROM DeliveryOrderDetails WHERE DOImportCode = @_DOImportCode AND CompanyCode = SOURCE.[CompanyCode])
			,(SELECT TOP 1 [ProvinceShipTo] FROM DeliveryOrderDetails WHERE DOImportCode = @_DOImportCode AND CompanyCode = SOURCE.[CompanyCode])
			,SOURCE.[Quantity]
			,dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 1)--[PackQuantity]
			,[dbo].[fnPackagingType] (SOURCE.[ProductID])
			,dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 0)
			,0--SOURCE.[SplittedQty]
			,0--SOURCE.[RequireReturnQty]
			,0--SOURCE.[ReturnedQty]
			,'N'--SOURCE.[Status]
			,@_UserID--SOURCE.[CreatedBy]
			,@_SitemapID--SOURCE.[CreatedBySitemapID]
			,@_SyncDate--SOURCE.[CreatedDateTime]
			,@_UserID--SOURCE.[UpdatedBy]
			,@_SitemapID--[UpdatedBySitemapID]
			,@_SyncDate--[UpdatedDateTime]
			--,SOURCE.[RowVersion]
			,0--[LastQuantity]
			)
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table
WHEN NOT MATCHED BY SOURCE 
	THEN 
		UPDATE SET 
		TARGET.[LastQuantity] = TARGET.[Quantity]
		,TARGET.[Quantity] = 0
		,TARGET.[PackQuantity] = 0
		,TARGET.[ProductQuantity] = 0
		,TARGET.[UpdatedBy] = @_UserID
		,TARGET.[UpdatedBySitemapID] = @_SitemapID
		,TARGET.[UpdatedDateTime] = @_SyncDate
		;
SELECT @@ROWCOUNT;

--DROP TABLE #SOURCE
DROP TABLE #SOURCE
END