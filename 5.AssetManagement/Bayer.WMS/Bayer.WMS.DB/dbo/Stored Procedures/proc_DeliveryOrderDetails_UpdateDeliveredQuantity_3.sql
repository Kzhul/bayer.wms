﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdateDeliveredQuantity]
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_UpdateDeliveredQuantity]
	@CompanyCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	IF(@_CompanyCode != '')
	BEGIN
		SELECT DISTINCT
			PS.CompanyCode 
			,PS.ProductLot
			,PS.ProductID
			,DeliveredQuantity = ISNULL(COUNT(DISTINCT ps.ProductBarcode),0)
		INTO #A
		FROM 
			DeliveryHistories PS WITH (NOLOCK)
		WHERE
			PS.CompanyCode = @_CompanyCode
			--AND PS.[DeliveryDate] >= CONVERT(DATE,GETDATE())
			AND DeliveryTicketCode = @_DOImportCode
		GROUP BY 
			PS.CompanyCode 
			,PS.ProductLot
			,PS.ProductID

		IF EXISTS (SELECT * FROM #A)
		BEGIN
			UPDATE DeliveryOrderSum
			SET DeliveredQty = ISNULL(A.DeliveredQuantity,0)
			FROM
				DeliveryOrderSum DO
				LEFT JOIN #A AS A
					ON DO.BatchCode = A.ProductLot
					AND DO.CompanyCode = A.CompanyCode
					AND DO.ProductID = A.ProductID
					--AND DO.[DeliveryDate] >= CONVERT(DATE,GETDATE())
			WHERE
				DO.CompanyCode = @_CompanyCode
				AND DO.DOImportCode = @_DOImportCode
		END
	END
END