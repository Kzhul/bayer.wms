﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_MovePromotion]
	@PalletCode VARCHAR(255)
	, @ProductID INT
	, @ProductCode VARCHAR(255)
	, @ReferenceNbr VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @Qty INT
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
    DECLARE @_ProductID INT = @ProductID
	DECLARE @_ProductCode VARCHAR(255) = @ProductCode
	DECLARE @_ReferenceNbr VARCHAR(255) = @ReferenceNbr
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_Qty INT = @Qty
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_ProductBarcode VARCHAR(255) = @_ProductCode + '_' + @_ReferenceNbr + '_' + @_CompanyCode

	INSERT INTO dbo.PalletStatuses
	(
		PalletCode
	    , ProductBarcode
	    , EncryptedProductBarcode
	    , ProductID
	    , Qty
	    , CreatedBy
	    , CreatedBySitemapID
	    , CreatedDateTime
	    , UpdatedBy
	    , UpdatedBySitemapID
	    , UpdatedDateTime
	)
	VALUES 
	(
		@_PalletCode
		, @_ProductBarcode
		, @_ProductBarcode
		, @_ProductID
		, @_Qty
		, @_UserID
		, @_SitemapID
		, @_Date
		, @_UserID
		, @_SitemapID
		, @_Date
	)
	
	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PalletStatus">' + 
			(SELECT * FROM dbo.PalletStatuses ps WHERE ps.ProductBarcode = @_ProductBarcode FOR XML PATH('PalletStatus')) + 
		'</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'PalletStatuses', @_Data, 'UPD', @_Method, @_Date, @_Date
END