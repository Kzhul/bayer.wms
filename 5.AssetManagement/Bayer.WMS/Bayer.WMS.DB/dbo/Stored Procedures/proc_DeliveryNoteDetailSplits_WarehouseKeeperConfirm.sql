﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryNoteDetailSplits_WarehouseKeeperConfirm]
	@DOImportCode VARCHAR(255)
	,@ReferenceNbr VARCHAR(255)
	,@UserID INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_ReferenceNbr VARCHAR(255) = @ReferenceNbr
	DECLARE @_UserID VARCHAR(255) = @UserID

	UPDATE
		[DeliveryNoteDetailSplits]
	SET [WarehouseConfirmID] = @_UserID
	    ,[WarehouseConfirmdate] = GETDATE()
		,Status = 'B'
		,UpdatedBy = @_UserID
		,UpdatedDateTime = GETDATE()
	WHERE
		[DOImportCode] = @_DOImportCode
        AND [ReferenceNbr] = @_ReferenceNbr
		AND IsDeleted = 0
		AND [Status] = 'N'
END