﻿CREATE PROCEDURE [dbo].[proc_Bay4SData_ReportDetail]
	@ProductCode NVARCHAR(50)
	, @ProductLot NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT 
		B.ProductCode
		,B.ProductName
		,B.BatchCode
		,B.BatchCodeDistributor
		,B.UOM
		,B.Unrestricted
		,B.InQualityInsp
		,B.Blocked
		,B.ManufacturingDate
		,B.ExpiredDate
		, TB.ZoneName
		, TB.LineName
		, TB.LocationCode
		, TB.LocationName
		, TB.PalletCode
		, TB.PackSize
		, TB.Quantity
		, StrDriverReceivedDate
		, StrDriverReceived 
		, StrLocationPutDate
		, StrDriver
		, StrWarehouseVerifyDate
		, StrWarehouseKeeper
		, WarehouseVerifyNote
	FROM
	[dbo].[Bay4SData] B
	LEFT JOIN 
	(
		SELECT DISTINCT
			 Z.ZoneName
			 , ZL.LineName
			 , L.LocationCode
			 , L.LocationName
			 , P.PalletCode
			 , PS.ProductLot
			 , PR.ProductCode
			 , PR.Description AS ProductName
			 , PR.UOM
			 , CONVERT(INT,PPS.Quantity) AS PackSize
			 , COUNT(DISTINCT CASE WHEN PS.CartonBarcode = '' THEN 0 ELSE 1 END) AS CartonQuantity
			 , SUM(ISNULL(PS.Qty,1)) AS Quantity	 
			 , ManufacturingDate = FORMAT(PP.ManufacturingDate, 'dd/MM/yyyy')
			 , ExpiryDate = FORMAT(PP.ExpiryDate, 'dd/MM/yyyy')
			 , StrDriverReceivedDate = FORMAT(P.DriverReceivedDate, 'dd/MM/yyyy HH:mm:ss')
			 , StrDriverReceived = ISNULL(R.LastName + ' ', '') + ISNULL(R.FirstName, '')
			 , StrLocationPutDate = FORMAT(P.LocationPutDate, 'dd/MM/yyyy HH:mm:ss')
			 , StrDriver = ISNULL(D.LastName + ' ', '') + ISNULL(D.FirstName, '')
			 , StrWarehouseVerifyDate = FORMAT(P.WarehouseVerifyDate, 'dd/MM/yyyy HH:mm:ss')
			 , StrWarehouseKeeper = ISNULL(W.LastName + ' ', '') + ISNULL(W.FirstName, '')	 
			 , Description = PR.Description
			 , WarehouseVerifyNote
		FROM 		
			[dbo].[Pallets] P WITH (NOLOCK)
			LEFT JOIN [dbo].[ZoneLocations] L WITH (NOLOCK)
				ON P.LocationCode = L.LocationCode
				AND P.LocationCode IS NOT NULL
				AND P.LocationCode != ''
			LEFT JOIN [dbo].[ZoneLines] ZL WITH (NOLOCK)
				ON ZL.ZoneCode = L.ZoneCode
				AND ZL.LineCode = L.LineCode
			LEFT JOIN Zones Z WITH (NOLOCK)
				ON Z.ZoneCode = ZL.ZoneCode
			LEFT JOIN PalletStatuses PS WITH (NOLOCK)
				ON P.PalletCode = PS.PalletCode	
			LEFT JOIN Products PR WITH (NOLOCK)
				 ON PS.ProductID = PR.ProductID
			LEFT JOIN ProductionPlans pp WITH (NOLOCK)
				ON PS.ProductLot = PP.ProductLot
			LEFT JOIN [dbo].[ProductPackings] PPS WITH (NOLOCK)
				ON PR.ProductID = PPS.ProductID
				AND PPS.Type != 'P'
			LEFT JOIN Users R WITH (NOLOCK)
				 ON P.DriverReceived = R.UserID
			LEFT JOIN Users W WITH (NOLOCK)
				 ON P.WarehouseKeeper = W.UserID
			LEFT JOIN Users D WITH (NOLOCK)
				 ON P.Driver = D.UserID
		WHERE
			P.PalletCode IS NOT NULL
			AND PS.ProductLot IS NOT NULL
			AND P.PalletCode NOT LIKE '%99999'
		GROUP BY
			Z.ZoneName
			 , ZL.LineName
			 , L.LocationCode
			 , L.LocationName
			 , P.PalletCode
			 , PS.ProductLot
			 , PR.ProductCode
			 , PR.Description-- AS ProductName
			 , PR.UOM
			 , PPS.Quantity-- AS PackSize
			 , PP.ManufacturingDate
			 , PP.ExpiryDate
			 , P.LocationPutDate
			 , P.[WarehouseVerifyDate]
			 , P.[DriverReceivedDate]
			 , R.FirstName
			 , R.LastName
			 , D.FirstName
			 , D.LastName
			 , W.FirstName
			 , W.LastName
			 , WarehouseVerifyNote
	) AS TB
		ON B.ProductCode = TB.ProductCode
		AND B.BatchCode = TB.ProductLot
	WHERE
		(@ProductCode = '' OR B.ProductCode = @ProductCode)
		AND (@ProductLot = '' OR B.BatchCode = @ProductLot)
END