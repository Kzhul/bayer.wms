﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_Join_Carton]
	@ProductBarcode VARCHAR(255)
	, @CartonBarcode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_ProductBarcode VARCHAR(255) = @ProductBarcode
	DECLARE @_CartonBarcode VARCHAR(255) = @CartonBarcode

    UPDATE dbo.PalletStatuses SET CartonBarcode = @_CartonBarcode WHERE ProductBarcode = @_ProductBarcode
END