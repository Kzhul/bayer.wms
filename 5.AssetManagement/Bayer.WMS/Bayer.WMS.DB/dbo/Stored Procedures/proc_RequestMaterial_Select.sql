﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_Select]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description, srd.ExpiredDate ASC)
	,PalletCodeSuggest = PS.PalletCode
	,LocationCode = ISNULL(P.LocationCode,'')
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,PS.ProductLot
	,P.Status
	,srd.DeliveryDate
	,srd.ExpiredDate
	,SUM(ISNULL(PS.Qty, 1)) AS PalletQuantity
FROM
	PalletStatuses PS WITH (NOLOCK) 
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON PS.PalletCode = P.PalletCode
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON PS.ProductID = PD.ProductID
	LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
		ON srd.ProductID = ps.ProductID
		AND srd.BatchCode = ps.ProductLot
WHERE
	(
		PS.ProductLot = @DocumentNbr
		OR PD.ProductCode = @DocumentNbr
		OR PS.PalletCode = @DocumentNbr
	)
	--AND P.Status = 'N'
GROUP BY
	PS.PalletCode
	,PD.ProductID
	,PS.ProductLot
	,P.Status
	,P.CompanyCode
	,srd.DeliveryDate
	,srd.ExpiredDate
	,PD.ProductID
	,PD.ProductCode
	,PD.Description
	,P.LocationCode
ORDER BY
	PD.Description
	,srd.ExpiredDate ASC

END