﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_ConfirmPallet]
	@DocumentNbr NVARCHAR(50)
	,@PalletCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_PalletCode NVARCHAR(50) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()

	--1.Update [Pallets] set trạng thái về P, Đã mang hàng xuống, đợi thủ kho duyệt, hoặc mang thẳng xuống SX
	UPDATE Pallets
	SET 
		[Status] = 'N'
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
		, DOImportCode = NULL
		, ReferenceNbr = NULL
		, CompanyCode = NULL		
		, [LocationCode] = NULL
		, [LocationSuggestion] = NULL
		, [BatchCode] = NULL
		, [BatchCodeDistributor] = NULL
		, [ImportStatus] = NULL
		, [Weigh] = NULL
		, [CartonNumber] = NULL
	WHERE
		PalletCode = @_PalletCode

	INSERT INTO [dbo].[RequestMaterialLineSplits]
			   ([DocumentNbr]
			   ,[LineNbr]
			   ,[ProductID]
			   ,[PalletCode]
			   ,[ProductLot]
			   ,[Quantity]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedBySitemapID]
			   ,[CreatedDateTime]
			   ,[UpdatedBy]
			   ,[UpdatedBySitemapID]
			   ,[UpdatedDateTime])
	SELECT
		[DocumentNbr] = @_DocumentNbr
		,[LineNbr] = 0
		,[ProductID] = PS.[ProductID]
		,[PalletCode] = P.PalletCode
		,[ProductLot] = PS.ProductLot
		,[Quantity] = SUM(ISNULL(PS.Qty, 1))
		,[Status] = 'N'
		,[CreatedBy] = 1
		,[CreatedBySitemapID] = 1
		,[CreatedDateTime] = GETDATE()
		,[UpdatedBy] = 1
		,[UpdatedBySitemapID] = 1
		,[UpdatedDateTime] = GETDATE()
	FROM
		PalletStatuses PS WITH (NOLOCK) 
		JOIN Pallets P WITH (NOLOCK) 
			ON PS.PalletCode = P.PalletCode
	WHERE
		PS.PalletCode = @_PalletCode
	GROUP BY
		PS.[ProductID]
		,P.PalletCode
		,PS.ProductLot


	UPDATE [dbo].[RequestMaterialLines]
	SET 
		[PickedQty] = 
		(
			SELECT
				SUM(RS.[Quantity])
			FROM
				[RequestMaterialLines] RML
				JOIN [dbo].[RequestMaterialLineSplits] RS
					ON RML.DocumentNbr = RS.DocumentNbr
					AND RML.ProductID = RS.ProductID
			WHERE
				RML.DocumentNbr = @_DocumentNbr
				AND [RequestMaterialLines].ProductID = RML.ProductID
		)
	WHERE
		DocumentNbr = @_DocumentNbr


	UPDATE RequestMaterialLineSplits
	SET 
		[Status] = 'C'
		, WarehouseKeeper = @_UserID
		, WarehouseVerifyDate = @_Date
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode
		AND DocumentNbr = @_DocumentNbr
END