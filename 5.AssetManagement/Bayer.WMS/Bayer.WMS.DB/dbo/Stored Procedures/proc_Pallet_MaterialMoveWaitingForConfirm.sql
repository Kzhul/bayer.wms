﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallet_MaterialMoveWaitingForConfirm]
AS
BEGIN
	SET NOCOUNT ON
	SELECT DISTINCT
		 P.PalletCode
		 , [ManufacturingDate] = FORMAT(srd.DeliveryDate,'dd/MM/yyyy')
		 , [ExpiryDate] = FORMAT(srd.[ExpiryDate],'dd/MM/yyyy')
		 , RM.ProductID
		 , PD.ProductCode
		 , PD.Description AS ProductName
		 , srd.BatchCode AS ProductLot
		 , PS.Note
		 , RM.[UOM]
		 , 0 AS CartonQuantity
		 , SUM(ISNULL(PPS.Qty,1)) AS Quantity
		 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
		 , P.LocationCode
		 , P.LocationSuggestion
		 , ZL.LocationName
		 , LocationSuggestionName = ''
	FROM 
		RequestMaterialLines RM WITH (NOLOCK) 
		JOIN RequestMaterialLineSplits PS WITH (NOLOCK) 
			ON RM.ProductID = PS.ProductID
		JOIN Pallets P WITH (NOLOCK) 
			ON PS.PalletCode = P.PalletCode
		JOIN PalletStatuses PPS WITH (NOLOCK) 
			ON PPS.PalletCode = P.PalletCode
		LEFT JOIN Products PD WITH (NOLOCK) 
			ON PS.ProductID = PD.ProductID
		LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
			ON srd.ProductID = ps.ProductID
			AND srd.BatchCode = ps.ProductLot
		LEFT JOIN ZoneLocations ZL WITH (NOLOCK) 
			ON P.LocationCode = ZL.LocationCode
	WHERE 
		PS.[Status] = 'P'
		AND P.Status = 'P'
	GROUP BY
		P.PalletCode
		 , srd.DeliveryDate
		 , srd.[ExpiryDate]
		 , RM.[UOM]
		 , RM.ProductID
		 , PD.ProductCode
		 , PD.Description
		 , srd.BatchCode
		 , PS.Note
		 , P.LocationCode
		 , P.LocationSuggestion
		 , ZL.LocationName
		
END