﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SetLocation]
	@PalletCode VARCHAR(255)
	, @LocationCode VARCHAR(255)
	, @CurrentWork  VARCHAR(5)--NOT USE
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_LocationCode VARCHAR(255) = NULLIF(@LocationCode, '')
	DECLARE @_CurrentWork VARCHAR(255) = NULLIF(@CurrentWork, '')--NOT USE
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'SLC' + @_PalletCode + '->' + @_LocationCode

	IF EXISTS (SELECT * FROM ZoneLocations ZL WITH (NOLOCK) WHERE ZL.LocationCode = @_LocationCode)
	BEGIN
		UPDATE dbo.Pallets 
		SET 
			LastLocationCode = LocationCode
			, LastLocationPutDate = LocationPutDate
			, LastDriver = Driver
		WHERE 
			PalletCode = @_PalletCode

		UPDATE dbo.Pallets 
		SET 
			LocationCode = @_LocationCode
			, LocationPutDate = @_Date
			, Driver = @UserID
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE 
			PalletCode = @_PalletCode

		UPDATE dbo.ZoneLocations 
		SET 
			CurrentPalletCode = @_PalletCode
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE 
			LocationCode = @_LocationCode

		SET @_Data = 
			'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallet">' 
			+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode FOR XML PATH(''))
			+ '</BaseEntity>'
		EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, @_CurrentWork, @_Method, @_Date, @_Date, @_AuditDescription
	END

	SELECT * FROM ZoneLocations ZL WITH (NOLOCK) WHERE ZL.LocationCode = @_LocationCode
END