﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity]
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_UpdatePreparedQuantity]
	@PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_CompanyCode VARCHAR(255) = ISNULL((SELECT TOP 1 CompanyCode FROM Pallets WITH (NOLOCK) WHERE PalletCode = @_PalletCode),'')
	IF(@_CompanyCode != '')
	BEGIN
		SELECT DISTINCT
			P.CompanyCode 
			,PS.ProductLot
			,PS.ProductID
			,PreparedQuantity = COUNT(DISTINCT ps.ProductBarcode)	
		INTO #A
		FROM 
			PalletStatuses PS WITH (NOLOCK)
			LEFT JOIN Pallets p WITH (NOLOCK)
				ON ps.PalletCode = p.PalletCode
		WHERE
			CompanyCode = @_CompanyCode
		GROUP BY 
			P.CompanyCode 
			,PS.ProductLot
			,PS.ProductID

		UPDATE DeliveryOrderDetails
		SET [PreparedQty] = A.PreparedQuantity
		FROM
			DeliveryOrderDetails DO
			LEFT JOIN #A AS A
				ON DO.BatchCode = A.ProductLot
				AND DO.CompanyCode = A.CompanyCode
				AND DO.ProductID = A.ProductID
				AND DATEDIFF(DD,DO.DeliveryDate,GETDATE()) >= 0
		WHERE
			DO.CompanyCode = @_CompanyCode
	END
END