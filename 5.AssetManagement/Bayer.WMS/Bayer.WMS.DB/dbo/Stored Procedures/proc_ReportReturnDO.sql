﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_ReportReturnDO] '', NULL, NULL
CREATE PROCEDURE [dbo].[proc_ReportReturnDO]
	@DOImportCode VARCHAR(255)
	,@FromDate Date
	,@ToDate Date
AS
BEGIN
	SET NOCOUNT ON

SELECT DISTINCT
	DO.DOImportCode
	,DO.DeliveryDate
	,dbo.fnStatusOfTicket(DO.[Status]) as DOStatus
	,dbo.fnStatusOfTicket(EX.[Status]) as EXStatus
	,dbo.fnStatusOfTicket(SP.[Status]) as SPStatus
	,dbo.fnStatusOfTicket(PR.[Status]) as PRStatus
	,dbo.fnStatusOfTicket(DH.[Status]) as DHStatus
	,EX.ExportCode
	,SP.SplitCode
	,PR.PrepareCode
	,DT.[DeliveryTicketCode]
FROM
	ReturnDOHeaders RD
	LEFT JOIN ReturnDODetails RDT
		ON RD.ReturnDOCode = RDT.ReturnDOCode
	LEFT JOIN DeliveryOrderDetails DOT
		ON RD.Delivery = DOT.Delivery
	LEFT JOIN DeliveryOrderHeaders DO
		ON DOT.DOImportCode = DO.DOImportCode
	LEFT JOIN DeliveryNoteHeaders EX
		ON ex.DOImportCode = DO.DOImportCode
	LEFT JOIN SplitNoteHeaders SP
		ON SP.DOImportCode = DO.DOImportCode
	LEFT JOIN PrepareNoteDetails PRT
		ON PRT.DeliveryCode = RDT.Delivery
		AND PRT.BatchCode = RDT.BatchCode
		AND PRT.ProductID = RDT.ProductID
		AND PRT.DOCode = RDT.DOImportCode
		--AND RDT.BatchCode = RDT.BatchCode
		--AND RDT.BatchCode = RDT.BatchCode
		--AND RDT.BatchCode = RDT.BatchCode
	LEFT JOIN PrepareNoteHeaders PR
		ON PR.PrepareCode = PRT.PrepareCode
	LEFT JOIN [dbo].[DeliveryTicketDetails] DT
		ON DT.PrepareCode = PR.PrepareCode
	LEFT JOIN [dbo].[DeliveryTicketHeaders] DH
		ON DT.[DeliveryTicketCode] = DH.[DeliveryTicketCode]	
WHERE
	(@DOImportCode = '' 
		OR
		( 
			RD.ReturnDOCode = @DOImportCode
		)
	)
	AND (@FromDate IS NULL OR DO.DeliveryDate >= @FromDate)
	AND (@ToDate IS NULL OR DO.DeliveryDate <= @ToDate)




END