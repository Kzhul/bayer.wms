﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterialSplit_SelectPallet_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

--Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT DISTINCT

	RM.LineNbr
	,PalletCode = PS.PalletCode
	,LocationCode = ISNULL(P.LocationCode,'')
	,LocationName = ISNULL(ZL.LocationName,'')
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductDescription
	,PS.ProductLot
	,PS.Status
	,P.CompanyCode
	,srd.DeliveryDate
	,srd.ExpiredDate
	,PS.ActualQuantity
	,PS.Quantity AS PalletQuantity
	,PS.Note
	,StatusDisplay = CASE WHEN PS.Status = 'N' THEN N'Mới'
					 WHEN PS.Status = 'B' THEN N'Chờ rớt hàng'
					 WHEN PS.Status = 'P' THEN N'Chờ chuyển xuống SX'
					 WHEN PS.Status = 'D' THEN N'Đã chuyển xuống SX'
					 ELSE '' END
	,StrDriver = ISNULL(Driver.LastName + ' ', '') + ISNULL(Driver.FirstName, '')
	,StrDriverExportDate = FORMAT(PS.DriverExportDate, 'dd/MM/yyyy HH:mm:ss')
	,StrWarehouseKeeper = ISNULL(WarehouseKeeper.LastName + ' ', '') + ISNULL(WarehouseKeeper.FirstName, '')
	,StrWarehouseVerifyDate = FORMAT(PS.WarehouseVerifyDate, 'dd/MM/yyyy HH:mm:ss')
	,StrDriverToProduction = ISNULL(DriverToProduction.LastName + ' ', '') + ISNULL(DriverToProduction.FirstName, '')
	,StrDriverToProductionDate = FORMAT(PS.DriverToProductionDate, 'dd/MM/yyyy HH:mm:ss')
FROM
	RequestMaterialLines RM WITH (NOLOCK) 
	JOIN RequestMaterialLineSplits PS WITH (NOLOCK) 
		ON RM.ProductID = PS.ProductID
		AND RM.DocumentNbr = PS.DocumentNbr
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON PS.PalletCode = P.PalletCode
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON PS.ProductID = PD.ProductID
	LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
		ON srd.ProductID = ps.ProductID
		AND srd.BatchCode = ps.ProductLot
	LEFT JOIN ZoneLocations ZL WITH (NOLOCK) 
		ON P.LocationCode = ZL.LocationCode
	LEFT JOIN Users Driver WITH (NOLOCK) 
		ON PS.Driver = Driver.UserID
	LEFT JOIN Users WarehouseKeeper WITH (NOLOCK) 
		ON PS.Driver = WarehouseKeeper.UserID
	LEFT JOIN Users DriverToProduction WITH (NOLOCK) 
		ON PS.Driver = DriverToProduction.UserID
WHERE
	RM.DocumentNbr = @_DocumentNbr
ORDER BY
	RM.LineNbr
	,srd.ExpiredDate ASC
END