﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_PrepareNoteDetail]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN

DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method
--------------SOURCE-------------------
SELECT DISTINCT
	DD.DOImportCode
	,PH.PrepareCode
	,DD.ProductID
	,DD.BatchCode
	,DD.CompanyCode
	,Quantity = SUM(DD.DOQuantity)
INTO #SOURCE
FROM
	[DeliveryOrderSum] DD
	LEFT JOIN PrepareNoteHeaders PH
		ON DD.DOImportCode = PH.DOImportCode
		AND DD.CompanyCode = PH.CompanyCode
WHERE
	DD.DOImportCode = @DOImportCode
GROUP BY 
	DD.DOImportCode
	,PH.PrepareCode
	,DD.ProductID
	,DD.BatchCode
	,DD.CompanyCode


--Synchronize the target table with
--refreshed data from source table
;
WITH TARGET AS 
(
    SELECT * 
    FROM dbo.PrepareNoteDetails
    WHERE 
		DOCode = @_DOImportCode
)
MERGE INTO TARGET
USING #SOURCE AS SOURCE 
ON 
	TARGET.[DOCode] = SOURCE.[DOImportCode]
	AND TARGET.[ProductID] = SOURCE.[ProductID]
	AND TARGET.[BatchCode] = SOURCE.[BatchCode]
	AND TARGET.[PrepareCode] = SOURCE.[PrepareCode]
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND 
		(
			TARGET.[Quantity] <> SOURCE.[Quantity]
		)
	THEN
	UPDATE SET 
		TARGET.[LastQuantity] = TARGET.[Quantity]
		,TARGET.[Quantity] = SOURCE.[Quantity]
		,TARGET.[PackQuantity] = dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 1)
		,TARGET.[PackType] = [dbo].[fnPackagingType] (SOURCE.[ProductID])
		,TARGET.[PackSize] = (SELECT TOP 1 Quantity FROM ProductPackings WHERE [Type] != 'P' AND ProductID = SOURCE.[ProductID] ) 
		,TARGET.[ProductQuantity] = dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 0)		
		,TARGET.[UpdatedBy] = @_UserID
		,TARGET.[UpdatedBySitemapID] = @_SitemapID
		,TARGET.[UpdatedDateTime] = @_SyncDate
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET 
	THEN 
		INSERT (
			[DOCode]
			,[PrepareCode]
			,[DeliveryCode]
			,[ProductID]
			,[BatchCode]
			,[BatchCodeDistributor]
			,[Quantity]
			,[PackQuantity]
			,[PackType]
			,[PackSize]
			,[PackWeight]
			,[ProductQuantity]
			,[ProductCase]
			,[ProductUnit]
			,[ProductWeight]
			,[UserPrepare]
			,[UserVerify]
			,[PreparedQty]
			,[DeliveredQty]
			,[RequireReturnQty]
			,[ReturnedQty]
			,[Status]
			,[CreatedBy]
			,[CreatedBySitemapID]
			,[CreatedDateTime]
			,[UpdatedBy]
			,[UpdatedBySitemapID]
			,[UpdatedDateTime]
			,[LastQuantity]
			) 
		VALUES (
			SOURCE.[DOImportCode]
			,SOURCE.[PrepareCode]
			,''--SOURCE.[Delivery]
			,SOURCE.[ProductID]
			,SOURCE.[BatchCode]
			,''--SOURCE.[BatchCodeDistributor]
			,SOURCE.[Quantity]
			,dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 1)--[PackQuantity]
			,[dbo].[fnPackagingType] (SOURCE.[ProductID])
			,(SELECT TOP 1 Quantity FROM ProductPackings WHERE [Type] != 'P' AND ProductID = SOURCE.[ProductID] ) --[PackSize]			
			,0--[PackWeight]
			,dbo.[fnPackagingQuantity] (SOURCE.[ProductID], SOURCE.[Quantity], 0)
			,1--SOURCE.[ProductCase]
			,'G'--SOURCE.[ProductUnit]
			,NULL--[ProductWeight]
			,NULL--[UserPrepare]
			,NULL--[UserVerify]
			,NULL--[PreparedQty]
			,NULL--[DeliveredQty]
			,NULL--[RequireReturnQty]
			,NULL--[ReturnedQty]
			,'N'--SOURCE.[Status]
			,@_UserID--SOURCE.[CreatedBy]
			,@_SitemapID--SOURCE.[CreatedBySitemapID]
			,@_SyncDate--SOURCE.[CreatedDateTime]
			,@_UserID--SOURCE.[UpdatedBy]
			,@_SitemapID--[UpdatedBySitemapID]
			,@_SyncDate--[UpdatedDateTime]
			--,SOURCE.[RowVersion]
			,0--[LastQuantity]
			)
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table
WHEN NOT MATCHED BY SOURCE 
	THEN 
		UPDATE SET 
		TARGET.[LastQuantity] = TARGET.[Quantity]
		,TARGET.[Quantity] = 0
		,TARGET.[PackQuantity] = 0
		,TARGET.[ProductQuantity] = 0
		,TARGET.[UpdatedBy] = @_UserID
		,TARGET.[UpdatedBySitemapID] = @_SitemapID
		,TARGET.[UpdatedDateTime] = @_SyncDate
		;
SELECT @@ROWCOUNT;

--DROP TABLE #SOURCE
DROP TABLE #SOURCE
END