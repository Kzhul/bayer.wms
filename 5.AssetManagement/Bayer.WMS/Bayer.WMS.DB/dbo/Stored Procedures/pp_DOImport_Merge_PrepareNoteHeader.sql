﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_PrepareNoteHeader]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN

DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method
DECLARE @_DeliveryDate DATE = (SELECT TOP 1 DeliveryDate FROM DeliveryOrderDetails WHERE DOImportCode = @_DOImportCode)
--------------SOURCE-------------------
DECLARE @ids TABLE(idx int identity(1,1), DOImportCode NVARCHAR(50), CompanyCode NVARCHAR(50))

INSERT INTO @ids
SELECT DISTINCT 
	DOImportCode
	,CompanyCode
FROM
	[DeliveryOrderSum] 
WHERE
	DOImportCode = @DOImportCode

DECLARE @i INT
DECLARE @cnt INT
DECLARE @id NVARCHAR(50)
SELECT @i = MIN(idx) - 1, @cnt = MAX(idx) FROM @ids
WHILE @i < @cnt
	BEGIN
		SELECT @i = @i + 1
		SET @id = (SELECT TOP 1 CompanyCode FROM @ids WHERE idx = @i)
	
		IF NOT EXISTS (SELECT * FROM PrepareNoteHeaders WHERE DOImportCode = @_DOImportCode AND CompanyCode = @id)
		BEGIN
			INSERT INTO [dbo].[PrepareNoteHeaders]
			   ([PrepareCode]
			   ,[PrepareDate]
			   ,[DOImportCode]
			   ,[Description]
			   ,[DeliveryDate]
			   ,[CompanyCode]
			   ,[CompanyName]
			   ,[Province]
			   ,[Status]
			   ,[IsDeleted]
			   ,[CreatedBy]
			   ,[CreatedBySitemapID]
			   ,[CreatedDateTime]
			   ,[UpdatedBy]
			   ,[UpdatedBySitemapID]
			   ,[UpdatedDateTime]
			   ,[UserCreateFullName]
			   ,[UserWareHouse]
			   ,[UserReview])
			VALUES (
				dbo.[fnTicketNextNBR]('SH')
				,GETDATE()--[PrepareDate]
				,@_DOImportCode
				,''--[Description]
				,@_DeliveryDate
				,@id
				,(SELECT TOP 1 [CompanyName] FROM Companies WHERE CompanyCode = @id)
				,(SELECT TOP 1 [ProvinceShipTo] FROM Companies WHERE CompanyCode = @id)
				,'N'
				,0
				,@_UserID--SOURCE.[CreatedBy]
				,@_SitemapID--SOURCE.[CreatedBySitemapID]
				,@_SyncDate--SOURCE.[CreatedDateTime]
				,@_UserID--SOURCE.[UpdatedBy]
				,@_SitemapID--[UpdatedBySitemapID]
				,@_SyncDate--[UpdatedDateTime]
				,''
				,''
				,''
				)
		END
	END
END