﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ProductReturn_SelectProductDelivered_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

    --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ASC)
	,RM.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,RM.CartonBarcode
	,RM.ProductBarcode
	,PD.UOM
	,ProductLot = RM.BatchCode
--INTO #TBPick
FROM
	ProductReturnDetailSplits RM WITH (NOLOCK) 
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RM.ProductID = PD.ProductID
WHERE
	RM.ProductReturnCode = @_DocumentNbr
ORDER BY PD.Description
END