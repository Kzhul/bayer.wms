﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ReportLocation1]
AS
BEGIN
	SET NOCOUNT ON

	--Zone Selection
	SELECT DISTINCT
		 Z.ZoneName
		 , Z.Description
		 , COUNT(DISTINCT L.LocationCode) AS Total
		 , COUNT(DISTINCT P.LocationCode) AS CheckedLocation
		 , EmptyLocation = COUNT(DISTINCT L.LocationCode) - COUNT(DISTINCT P.LocationCode)
	FROM 
		Zones Z
		LEFT JOIN [dbo].[ZoneLines] ZL
			ON Z.ZoneCode = ZL.ZoneCode
		LEFT JOIN [dbo].[ZoneLocations] L
			ON ZL.ZoneCode = L.ZoneCode
			AND ZL.LineCode = L.LineCode
		LEFT JOIN [dbo].[Pallets] P
			ON P.LocationCode = L.LocationCode
			AND P.LocationCode IS NOT NULL
			AND P.LocationCode != ''
	GROUP BY
		Z.ZoneCode
		 , Z.ZoneName
		 , Z.Description

END