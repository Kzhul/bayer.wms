﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_Delivery]
	@DeliveryTicketCode VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @CompanyName NVARCHAR(255)
	, @TruckNo VARCHAR(255)
	, @ActualDeliveryDate DATE
	, @DeliveryDate DATE
	, @PalletCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DeliveryTicketCode VARCHAR(255) = @DeliveryTicketCode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_CompanyName NVARCHAR(255) = @CompanyName
	DECLARE @_TruckNo VARCHAR(255) = @TruckNo
	DECLARE @_ActualDeliveryDate DATE = @ActualDeliveryDate
	DECLARE @_DeliveryDate DATE = @DeliveryDate
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

	SELECT
		p.ReferenceNbr
		, ps.ProductID
		, ps.ProductLot
		, Quantity = COUNT(ps.ProductBarcode)
	INTO #tmp
	FROM
		dbo.PalletStatuses ps
		JOIN dbo.Pallets p ON p.PalletCode = ps.PalletCode
	WHERE
		ps.PalletCode = @_PalletCode
	GROUP BY
		p.ReferenceNbr
		, ps.ProductID
		, ps.ProductLot

	UPDATE pnd
	SET
		pnd.DeliveredQty = ISNULL(pnd.DeliveredQty, 0) + #tmp.Quantity
	FROM
		dbo.PrepareNoteDetails pnd
		JOIN #tmp ON #tmp.ProductID = pnd.ProductID
					 AND #tmp.ProductLot = pnd.BatchCode
					 AND #tmp.ReferenceNbr = pnd.PrepareCode

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PrepareNoteDetails">' 
		+ (
			SELECT
				pnd.*
			FROM
				dbo.PrepareNoteDetails pnd
				JOIN #tmp ON #tmp.ProductID = pnd.ProductID
							 AND #tmp.ProductLot = pnd.BatchCode
							 AND #tmp.ReferenceNbr = pnd.PrepareCode
			FOR XML PATH(''))
		+ '</BaseEntity>'
		
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'PrepareNoteDetails', @_Data, 'UPD', @_Method, @_Date, @_Date
	

	INSERT INTO dbo.DeliveryHistories
	(
		ProductID
		, ProductLot
		, ProductBarcode
	    , EncryptedProductBarcode
	    , CartonBarcode
		, DeliveryTicketCode
	    , CompanyCode
	    , CompanyName
		, TruckNo
		, ActualDeliveryDate
	    , DeliveryDate
	    , CreatedBy
	    , CreatedBySitemapID
	    , CreatedDateTime
	    , UpdatedBy
	    , UpdatedBySitemapID
	    , UpdatedDateTime
	)
	SELECT
		ps.ProductID
		, ps.ProductLot
		, ps.ProductBarcode
		, ps.EncryptedProductBarcode
		, ps.CartonBarcode
		, @_DeliveryTicketCode
		, @_CompanyCode
		, @_CompanyName
		, @_TruckNo
		, @_ActualDeliveryDate
		, @_DeliveryDate
		, @_UserID
		, @_SitemapID
		, @_Date
		, @_UserID
		, @_SitemapID
		, @_Date
	FROM
		dbo.PalletStatuses ps
	WHERE
		ps.PalletCode = @_PalletCode

	DELETE FROM dbo.PalletStatuses WHERE PalletCode = @_PalletCode

	UPDATE dbo.Pallets 
	SET
		Status = 'N'
		, DOImportCode = NULL
		, ReferenceNbr = NULL
		, CompanyCode = NULL
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallet">' 
		+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode FOR XML PATH(''))
		+ '</BaseEntity>'

	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'UPD', @_Method, @_Date, @_Date
END