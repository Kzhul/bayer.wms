﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ProductReturn_Update_Delivery]
	@BarCode VARCHAR(255)
	, @DocumentNBR VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_BarCode VARCHAR(255) = @BarCode
	DECLARE @_DocumentNBR VARCHAR(255) = @DocumentNBR
	DECLARE @_CompanyCode VARCHAR(255) = (SELECT TOP 1 CompanyCode FROM ProductReturnHeaders WITH (NOLOCK) WHERE ProductReturnCode = @DocumentNBR)
	DECLARE @_CompanyName NVARCHAR(255) = (SELECT TOP 1 CompanyName FROM Companies WITH (NOLOCK) WHERE CompanyCode = @_CompanyCode)
	DECLARE @_DeliveryDate DATE = GETDATE()
	DECLARE @_ActualDeliveryDate DATE = GETDATE()	
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

	
INSERT INTO [dbo].[ProductReturnDetailSplits]
           ([ProductReturnCode]
           ,[LineNbr]
           ,[ProductID]
           ,[BatchCode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[CartonBarcode]
           ,[PalletCode]
           ,[DeliveryDate]
           ,[Description]
           ,[Status]
           ,[IsDeleted]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
	SELECT
		[ProductReturnCode] = @_DocumentNBR
        ,[LineNbr] = 0
        ,[ProductID]
        ,[BatchCode] = PS.ProductLot
        ,[ProductBarcode]
        ,[EncryptedProductBarcode]
        ,[CartonBarcode]
        ,[PalletCode]
        ,[DeliveryDate] = @_Date
        ,[Description] = ''
		, 'C'
		, 0
		, @_UserID
		, @_SitemapID
		, @_Date
		, @_UserID
		, @_SitemapID
		, @_Date
	FROM
		dbo.PalletStatuses ps
	WHERE
		PS.CartonBarcode = @_BarCode 
		OR PS.ProductBarcode = @_BarCode 
		OR PS.EncryptedProductBarcode = @_BarCode
		OR PS.PalletCode = @_BarCode 


	INSERT INTO dbo.DeliveryHistories
	(
		ProductID
		, ProductLot
		, ProductBarcode
	    , EncryptedProductBarcode
	    , CartonBarcode
		, DeliveryTicketCode
	    , CompanyCode
	    , CompanyName
		, TruckNo
		, ActualDeliveryDate
	    , DeliveryDate
	    , CreatedBy
	    , CreatedBySitemapID
	    , CreatedDateTime
	    , UpdatedBy
	    , UpdatedBySitemapID
	    , UpdatedDateTime
		, PalletCode
	)
	SELECT
		ps.ProductID
		, ps.ProductLot
		, ps.ProductBarcode
		, ps.EncryptedProductBarcode
		, ps.CartonBarcode
		, @_DocumentNBR
		, @_CompanyCode
		, @_CompanyName
		, ''
		, @_ActualDeliveryDate
		, @_DeliveryDate
		, @_UserID
		, @_SitemapID
		, @_Date
		, @_UserID
		, @_SitemapID
		, @_Date
		, ps.PalletCode
	FROM
		dbo.PalletStatuses ps
	WHERE
		PS.CartonBarcode = @_BarCode 
		OR PS.ProductBarcode = @_BarCode 
		OR PS.EncryptedProductBarcode = @_BarCode
		OR PS.PalletCode = @_BarCode 

	--WriteHistory
	INSERT INTO [PalletStatusesHistory]
		([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime]
           ,[DOImportCode]
	      ,[ReferenceNbr]
	      ,[CompanyCode]
           )
     
	( 
		SELECT DISTINCT
			PS.[PalletCode]
	      ,PS.[CartonBarcode]
	      ,PS.[ProductBarcode]
	      ,PS.[EncryptedProductBarcode]
	      ,PS.[ProductID]
	      ,PS.[ProductLot]
	      ,PS.[Qty]
	      ,PS.[CreatedBy]
	      ,PS.[CreatedBySitemapID]
	      ,PS.[CreatedDateTime]
	      ,PS.[UpdatedBy]
	      ,PS.[UpdatedBySitemapID]
	      ,PS.[UpdatedDateTime]
	      ,P.[DOImportCode]
	      ,P.[ReferenceNbr]
	      ,P.[CompanyCode]
	   	FROM 
	   		dbo.PalletStatuses PS
	   		LEFT JOIN Pallets P 
	   			ON PS.PalletCode = P.PalletCode
	   	WHERE 
			PS.CartonBarcode = @_BarCode 
			OR PS.ProductBarcode = @_BarCode 
			OR PS.EncryptedProductBarcode = @_BarCode
			OR PS.PalletCode = @_BarCode 
	   )
	--WriteHistory_DONE

	DELETE FROM dbo.PalletStatuses
	WHERE 
			CartonBarcode = @_BarCode 
			OR ProductBarcode = @_BarCode 
			OR EncryptedProductBarcode = @_BarCode
			OR PalletCode = @_BarCode 

	--4.Update vào bảng [dbo].[StockReturnDetail]
	UPDATE dbo.ProductReturnDetails
	SET
		DeliveredQty = CASE WHEN ProductReturnDetails.ReturnQty <= (SELECT COUNT(DISTINCT B.ProductBarcode) FROM [ProductReturnDetailSplits] B 
																		WHERE 
																			B.ProductReturnCode = ProductReturnDetails.ProductReturnCode
																			AND B.ProductID = ProductReturnDetails.ProductID
																			)
								THEN (SELECT COUNT(DISTINCT B.ProductBarcode) FROM [ProductReturnDetailSplits] B 
																		WHERE 
																			B.ProductReturnCode = ProductReturnDetails.ProductReturnCode
																			AND B.ProductID = ProductReturnDetails.ProductID
																			)
								ELSE ProductReturnDetails.ReturnQty END
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
		, Status = 'C'
	WHERE
		ProductReturnCode = @_DocumentNbr

END