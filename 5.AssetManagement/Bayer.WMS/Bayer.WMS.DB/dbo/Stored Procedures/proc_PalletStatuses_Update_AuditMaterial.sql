﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_AuditMaterial]
	@PalletCode VARCHAR(255)
	, @ProductLot VARCHAR(255)
	, @ProductID INT
	, @Quantity INT
	, @OldQuantity INT
	, @ReasonID NVARCHAR(20)
	, @UserID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_ProductLot VARCHAR(255) = @ProductLot
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_Quantity INT = @Quantity
	DECLARE @_OldQuantity INT = @OldQuantity
	DECLARE @_ReasonID NVARCHAR(20) = @ReasonID
	DECLARE @_UserID INT = @UserID
	DECLARE @_Date DATETIME = GETDATE()

--Tăng cho pallet đích
	IF NOT EXISTS (
		SELECT * 
		FROM 
			dbo.PalletStatuses
		WHERE
			PalletCode = @_PalletCode
			AND ProductLot = @_ProductLot
	)
	BEGIN
		INSERT INTO [dbo].[PalletStatuses]
           ([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
     SELECT
		[PalletCode] = @_PalletCode
        ,[CartonBarcode] = ''
        ,[ProductBarcode] = ''
        ,[EncryptedProductBarcode] = NULL
        ,[ProductID] = @_ProductID
        ,[ProductLot] = @_ProductLot
        ,[Qty] = @_Quantity
        ,[CreatedBy] = @_UserID
        ,[CreatedBySitemapID] = 1
        ,[CreatedDateTime] = GETDATE()
        ,[UpdatedBy] = @_UserID
        ,[UpdatedBySitemapID] = 1
        ,[UpdatedDateTime] = GETDATE()
	END
	ELSE
	BEGIN
		UPDATE dbo.PalletStatuses
		SET
			Qty = @Quantity
			,[UpdatedBy] = @_UserID
			,[UpdatedBySitemapID] = 1
			,[UpdatedDateTime] = GETDATE()
		WHERE
			PalletCode = @_PalletCode
			AND ProductLot = @_ProductLot
	END

	--Update AUDIT REASON and OLD QUANTITY HERE

	INSERT INTO [dbo].[PalletStatusesAuditLog]
           ([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[Qty_Old]
           ,[ReasonID]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
	 SELECT DISTINCT
		[PalletCode] = @_PalletCode
        ,[CartonBarcode] = ''
        ,[ProductBarcode] = ''
        ,[EncryptedProductBarcode] = NULL
        ,[ProductID] = @_ProductID
        ,[ProductLot] = @_ProductLot
        ,[Qty] = @_Quantity
		,[Qty_Old] = @OldQuantity
		,[ReasonID] = @_ReasonID
        ,[CreatedBy] = 1
        ,[CreatedBySitemapID] = 1
        ,[CreatedDateTime] = GETDATE()
        ,[UpdatedBy] = 1
        ,[UpdatedBySitemapID] = 1
        ,[UpdatedDateTime] = GETDATE()

	UPDATE dbo.Pallets
	SET
		UpdatedBy = @_UserID
		, UpdatedBySitemapID = 0
		, UpdatedDateTime = @_Date
		, WarehouseVerifyNote = 'OK'
		, WarehouseKeeper = @_UserID
		, WarehouseVerifyDate = @_Date
	WHERE
		PalletCode = @_PalletCode
END