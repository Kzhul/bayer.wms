﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_CompanyGetByDeliveryDate]
	@FromDate DATE
	,@ToDate DATE
AS
BEGIN
	SET NOCOUNT ON
	SELECT DISTINCT C.* 
	FROM 
		Companies C WITH (NOLOCK)
		JOIN DeliveryHistories DH WITH (NOLOCK)
			ON C.CompanyCode = DH.CompanyCode
	WHERE
		(@FromDate IS NULL OR DH.ActualDeliveryDate >= @FromDate)
		AND (@ToDate IS NULL OR DH.ActualDeliveryDate <= @ToDate)
	ORDER BY CompanyName
END