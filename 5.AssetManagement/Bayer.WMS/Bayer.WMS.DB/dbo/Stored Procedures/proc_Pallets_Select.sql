﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_Select]
	@PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode

	SELECT DISTINCT TOP 1
		P.*
		,StrStatus = dbo.[fnPalletStatus](P.[Status])
	FROM
		dbo.Pallets P WITH (NOLOCK)
		LEFT JOIN PalletStatuses PS WITH (NOLOCK)
			ON P.PalletCode = PS.PalletCode
	WHERE
		P.PalletCode = @_PalletCode
		OR PS.CartonBarcode = @_PalletCode
		OR PS.EncryptedProductBarcode = @_PalletCode
		OR PS.ProductBarcode = @_PalletCode
END