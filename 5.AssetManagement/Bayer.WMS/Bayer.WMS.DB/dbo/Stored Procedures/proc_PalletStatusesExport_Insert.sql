﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatusesExport_Insert]
	@PalletCode VARCHAR(255)
	, @UserID INT
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_Date DATETIME = GETDATE()

	DELETE FROM [dbo].[PalletStatusesExport]
	WHERE
		PalletCode = @_PalletCode

	INSERT INTO [dbo].[PalletStatusesExport]
           ([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[Received]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime]
           ,[DOImportCode]
           ,[ReferenceNbr]
           ,[CompanyCode])
     SELECT DISTINCT
		PS.[PalletCode]
		,[CartonBarcode] = ISNULL([CartonBarcode],'')
		,[ProductBarcode]
		,[EncryptedProductBarcode]
		,[ProductID]
		,[ProductLot]
		,[Qty]
		,[Received] = 0
		,[CreatedBy] = @_UserID
		,[CreatedBySitemapID] = 0
		,[CreatedDateTime] = @_Date
		,[UpdatedBy] = @_UserID
		,[UpdatedBySitemapID] = 0
		,[UpdatedDateTime] = @_Date
		,[DOImportCode]
		,[ReferenceNbr]
		,[CompanyCode]
	FROM
		PalletStatuses PS WITH (NOLOCK)
		JOIN Pallets P WITH (NOLOCK)
			ON PS.PalletCode = P.PalletCode
	WHERE
		PS.PalletCode = @_PalletCode
END