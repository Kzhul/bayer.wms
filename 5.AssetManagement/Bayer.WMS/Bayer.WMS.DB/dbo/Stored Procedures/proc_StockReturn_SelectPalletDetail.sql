﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReturn_SelectPalletDetail]
	@DocumentNbr NVARCHAR(50)
	,@PalletCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_PalletCode NVARCHAR(50) = @PalletCode

   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ,RM.PalletCode ASC)
	,PalletCode = RM.PalletCode
	,LocationCode = ISNULL(ZL.LocationName,'')
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,ProductLot = RM.BatchCode
	,Status = CASE WHEN RM.Status = 'N' THEN N'Chưa nhập kho'
					WHEN RM.Status = 'B' THEN N'Chờ lên kệ'
					WHEN RM.Status = 'C' THEN N'Đã nhập kho'
					END
	,RM.ReturnQty AS ReturnQty
	,RM.ReceivedQty AS ReceivedQty
FROM
	StockReturnDetails RM WITH (NOLOCK)
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RM.ProductID = PD.ProductID
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON RM.[PalletCode] = P.PalletCode
	LEFT JOIN ZoneLocations ZL WITH (NOLOCK) 
		ON ZL.LocationCode = P.LocationCode
WHERE
	RM.StockReturnCode = @_DocumentNbr
	AND RM.PalletCode = @_PalletCode
ORDER BY
	PD.Description
	,RM.PalletCode ASC
END