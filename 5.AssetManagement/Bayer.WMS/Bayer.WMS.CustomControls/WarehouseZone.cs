﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls.Properties;

namespace Bayer.WMS.CustomControls
{
    public partial class WarehouseZone : UserControl
    {
        private List<int> _line = new List<int>();
        private List<int> _path = new List<int>();
        private Dictionary<string, Panel> _location = new Dictionary<string, Panel>();

        public WarehouseZone()
        {
            InitializeComponent();
        }

        public string ZoneName
        {
            get => lblZoneName.Text;
            set => lblZoneName.Text = value;
        }

        public int Line { get; set; }

        public int Length { get; set; }

        public void DrawLine(int order)
        {
            if (order > 0)
            {
                Width += 52;
                tlpZoneLine.Width += 50;
                tlpZoneLine.ColumnStyles.Add(new ColumnStyle());
            }

            _line.Add(order);
        }

        public void DrawPath(int order)
        {
            if (order > 0)
            {
                Width += 22;
                tlpZoneLine.Width += 20;
                tlpZoneLine.ColumnStyles.Add(new ColumnStyle());
            }

            _path.Add(order);
        }

        public void DrawLength(int row)
        {
            for (int i = 1; i < row; i++)
            {
                Height += 50;
                tlpZoneLine.Height += 50;
                tlpZoneLine.RowStyles.Add(new RowStyle());
            }
        }
        public void DrawLocation(string locationCode, int order, int length)
        {
            var panel = new Panel();
            panel.Dock = DockStyle.Fill;
            panel.BackColor = Color.Red;

            if (String.IsNullOrWhiteSpace(locationCode))
                panel.BackgroundImage = Resources.DontHave;

            tlpZoneLine.Controls.Add(panel, order - 1, length - 1);
        }

        public override void Refresh()
        {
            base.Refresh();

            tlpZoneLine.ColumnCount = tlpZoneLine.ColumnStyles.Count;
            tlpZoneLine.RowCount = tlpZoneLine.RowStyles.Count;

            int additionWidth = tlpZoneLine.ColumnStyles.Count;
            int additionHeight = tlpZoneLine.RowStyles.Count;

            Width += additionWidth;
            Height += additionHeight + 13;
            tlpZoneLine.Width += additionWidth;
            tlpZoneLine.Height += additionHeight + 13;

            for (int i = 0; i < tlpZoneLine.ColumnStyles.Count; i++)
            {
                tlpZoneLine.ColumnStyles[i].SizeType = SizeType.Absolute;
                if (_line.Contains(i))
                    tlpZoneLine.ColumnStyles[i].Width = 50F;
                else if (_path.Contains(i))
                    tlpZoneLine.ColumnStyles[i].Width = 20F;

                if (i == tlpZoneLine.ColumnStyles.Count - 1)
                    tlpZoneLine.ColumnStyles[i].Width += additionWidth;
            }

            for (int i = 0; i < tlpZoneLine.RowStyles.Count; i++)
            {
                tlpZoneLine.RowStyles[i].SizeType = SizeType.Absolute;
                tlpZoneLine.RowStyles[i].Height = 50F;

                if (i == tlpZoneLine.RowStyles.Count - 1)
                    tlpZoneLine.RowStyles[i].Height += additionHeight;
            }
        }
    }
}
