﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Bayer.WMS.CustomControls
{
    public class CustomConfirmBox : Form
    {
        private PictureBox _pictureBox1;
        private Button _btnYes;
        private Button _btnCancel;
        private Label _lblMessage;
        private Action<CustomConfirmBox> _invokeOK;
        private Action<CustomConfirmBox> _invokeCancel;

        public CustomConfirmBox()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomConfirmBox));
            this._lblMessage = new System.Windows.Forms.Label();
            this._pictureBox1 = new System.Windows.Forms.PictureBox();
            this._btnYes = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // _lblMessage
            // 
            this._lblMessage.BackColor = System.Drawing.SystemColors.Control;
            this._lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblMessage.Location = new System.Drawing.Point(68, 9);
            this._lblMessage.Name = "_lblMessage";
            this._lblMessage.Size = new System.Drawing.Size(504, 117);
            this._lblMessage.TabIndex = 0;
            this._lblMessage.Text = "Message";
            // 
            // _pictureBox1
            // 
            this._pictureBox1.Location = new System.Drawing.Point(12, 12);
            this._pictureBox1.Name = "_pictureBox1";
            this._pictureBox1.Size = new System.Drawing.Size(50, 50);
            this._pictureBox1.TabIndex = 1;
            this._pictureBox1.TabStop = false;
            // 
            // _btnYes
            // 
            this._btnYes.BackColor = System.Drawing.Color.Green;
            this._btnYes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnYes.ForeColor = System.Drawing.Color.White;
            this._btnYes.Location = new System.Drawing.Point(179, 129);
            this._btnYes.Name = "_btnYes";
            this._btnYes.Size = new System.Drawing.Size(120, 40);
            this._btnYes.TabIndex = 2;
            this._btnYes.Text = "ĐỒNG Ý";
            this._btnYes.UseVisualStyleBackColor = false;
            this._btnYes.Click += new System.EventHandler(this._btnYes_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnCancel.Location = new System.Drawing.Point(305, 129);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(100, 40);
            this._btnCancel.TabIndex = 3;
            this._btnCancel.Text = "KHÔNG";
            this._btnCancel.UseVisualStyleBackColor = true;
            this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
            // 
            // CustomConfirmBox
            // 
            this.AcceptButton = this._btnYes;
            this.CancelButton = this._btnCancel;
            this.ClientSize = new System.Drawing.Size(584, 181);
            this.Controls.Add(this._btnCancel);
            this.Controls.Add(this._btnYes);
            this.Controls.Add(this._pictureBox1);
            this.Controls.Add(this._lblMessage);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomConfirmBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this._pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        private void _btnYes_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
            _invokeOK?.Invoke(this);
        }

        private void _btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            _invokeCancel?.Invoke(this);
        }

        public DialogResult ShowDialog(Control parent, string message, string caption)
        {
            AcceptButton = _btnYes;
            Top = (parent.Height - Height) / 2;
            Left = (parent.Width - Width) / 2 + parent.Left;

            Text = caption;
            _lblMessage.Text = message;

            //SetName
            if (parent != null)
                this.Name = $"{parent.Name}_CustomConfirmBox";

            return ShowDialog();
        }

        public void Show(Control parent, string message, string caption)
        {
            AcceptButton = _btnYes;
            Top = (parent.Height - Height) / 2;
            Left = (parent.Width - Width) / 2 + parent.Left;

            Text = caption;
            _lblMessage.Text = message;

            //SetName
            if (parent != null)
                this.Name = $"{parent.Name}_CustomConfirmBox";

            Show();
        }

        public void PressOk()
        {
            _btnYes.PerformClick();
        }

        public void PressCancel()
        {
            _btnCancel.PerformClick();
        }

        public Action<CustomConfirmBox> InvokeOK { set => _invokeOK = value; }

        public Action<CustomConfirmBox> InvokeCancel { set => _invokeCancel = value; }
    }
}
