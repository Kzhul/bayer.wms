﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bayer.WMS.Cof.Presenters
{
    public interface IUserMaintView : IBaseView
    {
        DataTable Users { set; }

        IList<Sitemap> Sitemaps { set; }

        User User { get; set; }

        IList<UsersRole> UsersRoles { get; set; }
    }

    public interface IUserMaintPresenter : IBasePresenter
    {
        Task LoadUsers();

        Task LoadSitemaps();

        Task LoadUsersRoles(int userID = 0);
    }

    public class UserMaintPresenter : BasePresenter, IUserMaintPresenter
    {
        private IUserMaintView _mainView;
        private IUserRepository _userRepository;
        private IUsersRoleRepository _usersRoleRepository;
        private IUsersPasswordRepository _usersPasswordRepository;
        private ISitemapRepository _sitemapRepository;
        private const string _defaultPwd = "123456aA@@Aa";
        private string _oldPIN = string.Empty;

        public UserMaintPresenter(IUserMaintView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IUserRepository userRepository, IUsersRoleRepository usersRoleRepository, IUsersPasswordRepository usersPasswordRepository, ISitemapRepository sitemapRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _userRepository = userRepository;
            _usersRoleRepository = usersRoleRepository;
            _usersPasswordRepository = usersPasswordRepository;
            _sitemapRepository = sitemapRepository;
        }

        private async Task ValidatePassword(User user)
        {
            user.ValidatePassword();
            string password = Utility.MD5Encrypt(user.Password);

            if (user.UserID > 0)
            {
                var oldUser = await _userRepository.GetSingleAsync(p => p.UserID == user.UserID);

                //// Validate password with last 10 last change times
                if (oldUser.Password != password && await _usersPasswordRepository.GetSingleAsync(p => p.UserID == user.UserID && p.Password == password) != null)
                    throw new WrappedException(Messages.Validate_Policy_PasswordNotIn10LastChangeTimes);
            }
        }

        /// <summary>
        /// Event trigger when user select item in combobox
        /// </summary>
        /// <param name="selectedItem"></param>
        public async void OnUsernameSelectChange(DataRow selectedItem)
        {
            try
            {
                var user = selectedItem.ToEntity<User>();
                await LoadUsersRoles(user.UserID);

                _oldPIN = user.PIN;
                user.Password = _defaultPwd;
                user.ConfirmPassword = _defaultPwd;
                user.ConfirmPIN = user.PIN;
                user.IsRecordAuditTrail = true;
                _mainView.User = user;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get users from database for combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadUsers()
        {
            try
            {
                var users = await _userRepository.GetAsync(p => !p.IsDeleted && p.UserID > 1);
                _mainView.Users = users.ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get sitemaps from database for combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadSitemaps()
        {
            try
            {
                var sitemaps = await _sitemapRepository.GetAsync(p => p.ClassName != null);
                sitemaps.Insert(0, new Sitemap());
                _mainView.Sitemaps = sitemaps;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get UserRoles from database for datagridview
        /// </summary>
        /// <returns></returns>
        public async Task LoadUsersRoles(int userID = 0)
        {
            try
            {
                _mainView.UsersRoles = await _usersRoleRepository.GetWithAllRolesAsync(p => p.UserID == userID);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async void Insert()
        {
            _mainView.User = new User();
            await LoadUsersRoles();
        }

        /// <summary>
        /// Save user to database, 2 cases:
        ///     Case 1: insert new user
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var user = _mainView.User;
            var usersRoles = _mainView.UsersRoles;

            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _userRepository = unitOfWork.Register(_userRepository.GetType()) as IUserRepository;
                    _usersRoleRepository = unitOfWork.Register(_usersRoleRepository.GetType()) as IUsersRoleRepository;
                    _usersPasswordRepository = unitOfWork.Register(_usersPasswordRepository.GetType()) as IUsersPasswordRepository;
                    
                    //PIN changed
                    if (_oldPIN != user.PIN || user.PIN != user.ConfirmPIN)
                    {
                        user.ValidatePIN();
                        user.PINChangedDate = DateTime.Now;
                    }

                    if (user.Password != _defaultPwd || user.Password != user.ConfirmPassword)
                    {
                        await ValidatePassword(user);
                    }

                    string plainPwd = user.Password;
                    user.Password = Utility.MD5Encrypt(user.Password);
                    user.IsRecordAuditTrail = true;

                    //// check user id to determine next action (insert or update)
                    if (user.UserID == 0)
                    {
                        user.PasswordChangeOnNextLogin = true;
                        await _userRepository.Insert(user);

                        //// commit to get new product id
                        await _userRepository.Commit();
                    }
                    else if (user.State == EntityState.Modified)
                    {
                        //// if password does not change (equal default password)
                        //// call function update user without update password
                        if (plainPwd == _defaultPwd)
                            await _userRepository.Update(user, null, new string[] { "Password" }, new object[] { user.UserID });
                        else
                            await _userRepository.Update(user, new object[] { user.UserID });
                    }

                    //// save last password change to validate in next time
                    if (plainPwd != _defaultPwd)
                    {
                        await _userRepository.ExecuteNonQuery("proc_UsersPasswords_Insert_LastPassword",
                            new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.VarChar, Value = user.UserID },
                            new SqlParameter { ParameterName = "@Password", SqlDbType = SqlDbType.VarChar, Value = user.Password });
                    }

                    //// foreach deleted user role and delete it
                    foreach (var userRole in usersRoles.Where(p => p.UserID != 0 && !p.Checked))
                    {
                        userRole.IsRecordAuditTrail = true;
                        await _usersRoleRepository.Delete(new object[] { userRole.UserID, userRole.RoleID });
                    }

                    //// foreach new or udpated user roles and insert/update to database
                    //// determine user role is deleted to delete it
                    foreach (var userRole in usersRoles.Where(p => p.Checked))
                    {
                        userRole.UserID = user.UserID;
                        userRole.IsRecordAuditTrail = true;
                        await _usersRoleRepository.InsertOrUpdate(userRole, new object[] { userRole.UserID, userRole.RoleID });
                    }

                    await unitOfWork.Commit();
                }

                user.Password = _defaultPwd;
                user.ConfirmPassword = _defaultPwd;

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (WrappedException)
            {
                throw;
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _userRepository = _mainPresenter.Resolve(_userRepository.GetType()) as IUserRepository;
                _usersRoleRepository = _mainPresenter.Resolve(_usersRoleRepository.GetType()) as IUsersRoleRepository;
                _usersPasswordRepository = _mainPresenter.Resolve(_usersPasswordRepository.GetType()) as IUsersPasswordRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadUsers();

                    _mainView.User = user;
                    _mainView.UsersRoles = usersRoles;
                }
            }
        }
    }
}
