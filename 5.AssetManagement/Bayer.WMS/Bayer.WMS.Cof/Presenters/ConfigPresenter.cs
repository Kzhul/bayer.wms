﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Bayer.WMS.Cof.Presenters
{
    [Serializable]
    public class PackagingMultScreenConfig
    {
        public PackagingMultScreenConfig() { }

        public PackagingMultScreenConfig(int screen, string packagingLine, string portName, string baudRate, string parity, string dataBits, string stopBits)
        {
            Screen = screen;
            PackagingLine = packagingLine;
            PortName = portName;
            BaudRate = baudRate;
            Parity = parity;
            DataBits = dataBits;
            StopBits = stopBits;
        }

        /// <summary>
        /// Screen number
        /// </summary>
        public int Screen { get; set; }

        /// <summary>
        /// Scanner PackagingLine
        /// </summary>
        public string PackagingLine { get; set; }

        public string PortName { get; set; }

        public string BaudRate { get; set; }

        public string Parity { get; set; }

        public string DataBits { get; set; }

        public string StopBits { get; set; }


    }

    [Serializable]
    public class PrinterConfig
    {
        public PrinterConfig() { }

        public PrinterConfig(string portName, string baudRate, string parity, string dataBits, string stopBits)
        {
            PortName = portName;
            BaudRate = baudRate;
            Parity = parity;
            DataBits = dataBits;
            StopBits = stopBits;
        }

        public string PortName { get; set; }

        public string BaudRate { get; set; }

        public string Parity { get; set; }

        public string DataBits { get; set; }

        public string StopBits { get; set; }

        public bool MultiProductPerPallet { get; set; }

        public string MultiProductPerPalletReason { get; set; }
    }

    public interface IConfigView : IBaseView
    {
        bool PackagingConfig_IsMultScreen { get; set; }

        IList<PackagingMultScreenConfig> PackagingConfig_MultiScreenConfigs { get; set; }

        PrinterConfig PrinterConfig { get; set; }
    }

    public interface IConfigPresenter : IBasePresenter
    {
        Task LoadPackagingConfig();

        void DeletePackagingMultScreenConfig(PackagingMultScreenConfig config);
    }

    public class ConfigPresenter : BasePresenter, IConfigPresenter
    {
        private IConfigView _mainView;
        private IZoneRepository _zoneRepository;
        private bool PermisionToChange = true;

        public ConfigPresenter(IConfigView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter
            ,IZoneRepository zoneRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _zoneRepository = zoneRepository;
        }

        public async Task LoadPackagingConfig()
        {
            try
            {
                await Task.Run(() =>
                {
                    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    XmlDocument doc = new XmlDocument();
                    doc.Load(config.FilePath);

                    bool isMultScreen = false;
                    var packagingConfig_MultiScreenConfigs = new List<PackagingMultScreenConfig>();
                    var node = doc.SelectSingleNode("//packagingConfig");
                    if (node != null)
                    {
                        bool.TryParse(node.Attributes["IsMultScreen"].Value, out isMultScreen);

                        foreach (XmlNode child in node.ChildNodes)
                        {
                            packagingConfig_MultiScreenConfigs.Add(
                                new PackagingMultScreenConfig(
                                    int.Parse(child.Attributes["Screen"].Value),
                                    child.Attributes["PackagingLine"].Value,
                                    child.Attributes["PortName"].Value,
                                    child.Attributes["BaudRate"].Value,
                                    child.Attributes["Parity"].Value,
                                    child.Attributes["DataBits"].Value,
                                    child.Attributes["StopBits"].Value));
                        }
                    }

                    node = doc.SelectSingleNode("//printerConfig");
                    var printerConfig = new PrinterConfig();
                    if (node != null)
                    {
                        printerConfig = new PrinterConfig(
                            node.Attributes["PortName"].Value,
                            node.Attributes["BaudRate"].Value,
                            node.Attributes["Parity"].Value,
                            node.Attributes["DataBits"].Value,
                            node.Attributes["StopBits"].Value);
                    }

                    _mainView.PackagingConfig_IsMultScreen = isMultScreen;
                    _mainView.PackagingConfig_MultiScreenConfigs = packagingConfig_MultiScreenConfigs;
                    _mainView.PrinterConfig = printerConfig;
                });

                List<SqlParameter> listParam = new List<SqlParameter>();
                DataTable dt = await _zoneRepository.ExecuteDataTable("proc_AppSettings_GetAll");
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["SettingID"].ToString() == "MultiProductPerPallet")
                    {
                        _mainView.PrinterConfig.MultiProductPerPallet = Utility.BoolParse( dr["SettingValue"].ToString());
                    }
                    else if (dr["SettingID"].ToString() == "MultiProductPerPalletReason")
                    {
                        _mainView.PrinterConfig.MultiProductPerPalletReason = dr["SettingValue"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        public void DeletePackagingMultScreenConfig(PackagingMultScreenConfig config)
        {
            _mainView.PackagingConfig_MultiScreenConfigs.Remove(config);
        }

        public override async Task Save()
        {
            try
            {
                var method = MethodBase.GetCurrentMethod();
                await Task.Run(() =>
                {
                    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    XmlDocument doc = new XmlDocument();
                    doc.Load(config.FilePath);

                    var node = (XmlElement)doc.SelectSingleNode("//packagingConfig");
                    if (node == null)
                    {
                        node = doc.CreateElement("packagingConfig");
                        doc.SelectSingleNode("configuration").AppendChild(node);
                    }

                    //// remove all config and add new
                    node.RemoveAll();

                    //// set value config
                    node.SetAttribute("IsMultScreen", $"{_mainView.PackagingConfig_IsMultScreen}");

                    //// add new screen config
                    foreach (var item in _mainView.PackagingConfig_MultiScreenConfigs.Where(p => p.PortName != null && p.PortName.Length > 0))
                    {
                        var child = doc.CreateElement("add");
                        child.SetAttribute("Screen", $"{item.Screen}");
                        child.SetAttribute("PackagingLine", item.PackagingLine);
                        child.SetAttribute("PortName", item.PortName);
                        child.SetAttribute("BaudRate", $"{item.BaudRate}");
                        child.SetAttribute("Parity", item.Parity);
                        child.SetAttribute("DataBits", $"{item.DataBits}");
                        child.SetAttribute("StopBits", item.StopBits);

                        node.AppendChild(child);
                        RecordAuditTrail(item.ToXML(), AuditTrail.action.Config, method);
                    }

                    node = (XmlElement)doc.SelectSingleNode("//printerConfig");
                    if (node == null)
                    {
                        node = doc.CreateElement("printerConfig");
                        doc.SelectSingleNode("configuration").AppendChild(node);
                    }

                    //// remove all config and add new
                    node.RemoveAll();

                    //// set value config
                    node.SetAttribute("PortName", $"{_mainView.PrinterConfig.PortName}");
                    node.SetAttribute("BaudRate", $"{_mainView.PrinterConfig.BaudRate}");
                    node.SetAttribute("Parity", $"{_mainView.PrinterConfig.Parity}");
                    node.SetAttribute("DataBits", $"{_mainView.PrinterConfig.DataBits}");
                    node.SetAttribute("StopBits", $"{_mainView.PrinterConfig.StopBits}");

                    RecordAuditTrail(_mainView.PrinterConfig.ToXML(), AuditTrail.action.Config, method);
                    doc.Save(config.FilePath);
                });

                if (PermisionToChange)
                {
                    if (_mainView.PrinterConfig.MultiProductPerPallet && string.IsNullOrWhiteSpace(_mainView.PrinterConfig.MultiProductPerPalletReason))
                    {
                        _mainPresenter.SetMessage(Messages.MultiProductPerPalletReason, Utility.MessageType.Error);
                        return;
                    }


                    // SAVE DATA
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter code = new SqlParameter("MultiProductPerPallet", _mainView.PrinterConfig.MultiProductPerPallet);
                    listParam.Add(code);
                    SqlParameter param1 = new SqlParameter("MultiProductPerPalletReason", _mainView.PrinterConfig.MultiProductPerPalletReason);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("UserID", LoginInfo.UserID);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("SiteMapID", 47);
                    listParam.Add(param3);
                    await _zoneRepository.ExecuteNonQuery("proc_AppSettings_Save_MultiProductPerPallet", listParam.ToArray());
                }
                else
                {
                    _mainPresenter.SetMessage(Messages.MultiProductPerPalletPermissiontoChange, Utility.MessageType.Error);
                    return;
                }

                ConfigurationManager.RefreshSection("packagingConfig");
                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }
    }
}
