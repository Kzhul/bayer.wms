﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Cof.Presenters
{
    public interface ICompanyView : IBaseView
    {
        System.Data.DataTable Companies { get; set; }

        Company Company { get; set; }
    }

    public interface ICompanyPresenter : IBasePresenter
    {
        Task LoadCompanies();

        Task Print();
    }

    public class CompanyPresenter : BasePresenter, ICompanyPresenter
    {
        private ICompanyView _mainView;
        private ICompanyRepository _CompanyRepository;
        private System.Data.DataTable listCompany;

        public CompanyPresenter(ICompanyView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            ICompanyRepository CompanyRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;            
            _CompanyRepository = CompanyRepository;
            listCompany = new System.Data.DataTable();
            //_mainView.Company = new Company();
            //_mainView.Companies = new DataTable();
        }

        /// <summary>
        /// Event trigger when user select item in combobox
        /// </summary>
        /// <param name="selectedItem"></param>
        public async void OnCompanyNameSelectChange(DataRow selectedItem)
        {
            try
            {
                var Company = selectedItem.ToEntity<Company>();
                Company.State = EntityState.Unchanged;
                _mainView.Company = Company;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get Companies from database for combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadCompanies()
        {
            try
            {
                var Companies = await _CompanyRepository.GetAsync(p => !p.IsDeleted);
                listCompany = Companies.ToList().OrderBy(a=>a.CompanyName).ToDataTable();
                _mainView.Companies = listCompany;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }
        
        public override async void Insert()
        {
            try
            {
                _mainView.Company = new Company();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var details = _mainView.Companies;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\CompanyExport.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            bool isSuccess = false;
            int cellRowIndex = 2;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;
                int n = details.Rows.Count;
                #region Fill Data Detail
                for (int i = 0; i < n; i++)
                {
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CompanyCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CompanyCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CompanyName"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CompanyAddress"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProvinceSoldTo"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProvinceShipTo"];
                    cellColumnIndex++;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                #endregion                

                excelApp.DisplayAlerts = false;
                exportPath += "ListCompany" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion
                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var Company = _mainView.Company;

            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _CompanyRepository = unitOfWork.Register(_CompanyRepository.GetType()) as ICompanyRepository;

                    Company.IsRecordAuditTrail = true;
                    bool contains = listCompany.AsEnumerable().Any(row => Company.CompanyCode == row.Field<String>("CompanyCode"));
                    if (!contains)
                    {
                        await _CompanyRepository.Insert(Company);

                        //// Commit to get new Company id
                        await _CompanyRepository.Commit();
                    }
                    else
                        await _CompanyRepository.Update(Company, new object[] { Company.CompanyCode });
                    
                    await unitOfWork.Commit();
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Company
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    _mainPresenter.SetMessage(message, Utility.MessageType.Error);
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _CompanyRepository = _mainPresenter.Resolve(_CompanyRepository.GetType()) as ICompanyRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadCompanies();

                    _mainView.Company = Company;
                }
            }
        }
    }
}
