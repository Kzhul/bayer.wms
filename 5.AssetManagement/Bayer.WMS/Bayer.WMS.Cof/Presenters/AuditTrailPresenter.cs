﻿using Bayer.WMS.Objs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;

namespace Bayer.WMS.Cof.Presenters
{
    public interface IAuditTrailView : IBaseView
    {
        DateTime FromDate { get; }

        DateTime ToDate { get; }
        string Code { get; }
        string User { get; }
        DataTable AuditTrail { set; }
    }

    public interface IAuditTrailPresenter : IBasePresenter
    {
    }

    public class AuditTrailPresenter : BasePresenter, IAuditTrailPresenter
    {
        private IAuditTrailView _mainView;
        private IAuditTrailRepository _auditTrailRepository;

        public AuditTrailPresenter(IAuditTrailView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IAuditTrailRepository auditTrailRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;

            _auditTrailRepository = auditTrailRepository;
        }

        public override async Task LoadMainData()
        {
            try
            {

                _mainView.AuditTrail = await _auditTrailRepository.ExecuteDataTable("proc_AuditTrails_Select",
                    new SqlParameter { ParameterName = "FromDate", SqlDbType = SqlDbType.Date, Value = _mainView.FromDate },
                    new SqlParameter { ParameterName = "ToDate", SqlDbType = SqlDbType.Date, Value = _mainView.ToDate },
                    new SqlParameter { ParameterName = "Code", SqlDbType = SqlDbType.NVarChar, Value = _mainView.Code },
                    new SqlParameter { ParameterName = "User", SqlDbType = SqlDbType.NVarChar, Value = _mainView.User }
                    );
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }
    }
}
