﻿namespace Bayer.WMS.Cof.Views
{
    partial class AuditTrailView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuditTrailView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtgAuditTrail = new System.Windows.Forms.DataGridView();
            this.bdsAuditTrail = new System.Windows.Forms.BindingSource(this.components);
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.colAuditTrail_User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAuditTrail_Action = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAuditTrail_DateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAuditTrail_Sitemap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAuditTrail_Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgAuditTrail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsAuditTrail)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(159, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 16);
            this.label1.TabIndex = 38;
            this.label1.Text = "-";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd.MM.yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(174, 12);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(90, 22);
            this.dtpToDate.TabIndex = 36;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd.MM.yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(64, 12);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(90, 22);
            this.dtpFromDate.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 37;
            this.label3.Text = "Ngày:";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(801, 7);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 30);
            this.btnSearch.TabIndex = 39;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtgAuditTrail
            // 
            this.dtgAuditTrail.AllowUserToAddRows = false;
            this.dtgAuditTrail.AllowUserToOrderColumns = true;
            this.dtgAuditTrail.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgAuditTrail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgAuditTrail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgAuditTrail.AutoGenerateColumns = false;
            this.dtgAuditTrail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgAuditTrail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgAuditTrail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colAuditTrail_User,
            this.colAuditTrail_Action,
            this.colAuditTrail_DateTime,
            this.colAuditTrail_Sitemap,
            this.colAuditTrail_Description});
            this.dtgAuditTrail.DataSource = this.bdsAuditTrail;
            this.dtgAuditTrail.GridColor = System.Drawing.SystemColors.Control;
            this.dtgAuditTrail.Location = new System.Drawing.Point(12, 43);
            this.dtgAuditTrail.Name = "dtgAuditTrail";
            this.dtgAuditTrail.ReadOnly = true;
            this.dtgAuditTrail.Size = new System.Drawing.Size(1098, 424);
            this.dtgAuditTrail.TabIndex = 40;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(330, 12);
            this.txtCode.MaxLength = 255;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(200, 22);
            this.txtCode.TabIndex = 42;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 16);
            this.label2.TabIndex = 41;
            this.label2.Text = "Mã:";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(595, 12);
            this.txtUser.MaxLength = 255;
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(200, 22);
            this.txtUser.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(538, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 16);
            this.label4.TabIndex = 43;
            this.label4.Text = "Người:";
            // 
            // colAuditTrail_User
            // 
            this.colAuditTrail_User.DataPropertyName = "User";
            this.colAuditTrail_User.HeaderText = "Người thực hiện";
            this.colAuditTrail_User.Name = "colAuditTrail_User";
            this.colAuditTrail_User.ReadOnly = true;
            this.colAuditTrail_User.Width = 200;
            // 
            // colAuditTrail_Action
            // 
            this.colAuditTrail_Action.DataPropertyName = "Action";
            this.colAuditTrail_Action.HeaderText = "Hành động";
            this.colAuditTrail_Action.Name = "colAuditTrail_Action";
            this.colAuditTrail_Action.ReadOnly = true;
            this.colAuditTrail_Action.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colAuditTrail_Action.Width = 200;
            // 
            // colAuditTrail_DateTime
            // 
            this.colAuditTrail_DateTime.DataPropertyName = "DateTime";
            dataGridViewCellStyle2.Format = "dd.MM.yyyy - HH:mm";
            this.colAuditTrail_DateTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.colAuditTrail_DateTime.HeaderText = "Thời gian";
            this.colAuditTrail_DateTime.Name = "colAuditTrail_DateTime";
            this.colAuditTrail_DateTime.ReadOnly = true;
            this.colAuditTrail_DateTime.Width = 150;
            // 
            // colAuditTrail_Sitemap
            // 
            this.colAuditTrail_Sitemap.DataPropertyName = "Sitemap";
            this.colAuditTrail_Sitemap.HeaderText = "Màn hình";
            this.colAuditTrail_Sitemap.Name = "colAuditTrail_Sitemap";
            this.colAuditTrail_Sitemap.ReadOnly = true;
            this.colAuditTrail_Sitemap.Width = 200;
            // 
            // colAuditTrail_Description
            // 
            this.colAuditTrail_Description.DataPropertyName = "Description";
            this.colAuditTrail_Description.HeaderText = "Ghi chú";
            this.colAuditTrail_Description.Name = "colAuditTrail_Description";
            this.colAuditTrail_Description.ReadOnly = true;
            this.colAuditTrail_Description.Width = 400;
            // 
            // AuditTrailView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 479);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtgAuditTrail);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.label3);
            this.Name = "AuditTrailView";
            this.Text = "AuditTrailView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgAuditTrail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsAuditTrail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dtgAuditTrail;
        private System.Windows.Forms.BindingSource bdsAuditTrail;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAuditTrail_User;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAuditTrail_Action;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAuditTrail_DateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAuditTrail_Sitemap;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAuditTrail_Description;
    }
}