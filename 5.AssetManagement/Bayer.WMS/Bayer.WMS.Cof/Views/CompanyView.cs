﻿using Bayer.WMS.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs;
using Bayer.WMS.CustomControls;
using System.Data.Entity;
using Bayer.WMS.Cof.Presenters;
using System.Reflection;

namespace Bayer.WMS.Cof.Views
{
    public partial class CompanyView : BaseForm, ICompanyView
    {
        public CompanyView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductPacking);
        }
        
        public override void InitializeComboBox()
        {
            cmbRoleName.PageSize = 20;
            cmbRoleName.ValueMember = "CompanyCode";
            cmbRoleName.DisplayMember = "CompanyName";
            cmbRoleName.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("CompanyCode", "Mã công ty", 100),
                new MultiColumnComboBox.ComboBoxColumn("CompanyName", "Tên công ty", 200),
                new MultiColumnComboBox.ComboBoxColumn("ProvinceShipTo", "Tỉnh thành", 100),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbRoleName.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadCompanies", "OnCompanyNameSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var CompanyPresenter = _presenter as ICompanyPresenter;

                await CompanyPresenter.LoadCompanies();

                if (!isRefresh)
                    CompanyPresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        private DataTable _companies;

        public DataTable Companies
        {
            get {
                return _companies;
            }
            set {
                _companies = value;
                cmbRoleName.Source = _companies;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsRoleSiteMap.DataSource = _companies;
                    });
                }
                else
                {
                    bdsRoleSiteMap.DataSource = _companies;
                }
            }
        }
        
        private Company _company;

        public Company Company
        {
            get {
                _company.Status = rdbActive.Checked ? "A" : "I";
                return _company;
            }
            set
            {
                _company = value;

                cmbRoleName.DataBindings.Clear();
                txtCompanyName.DataBindings.Clear();
                txtCompanyAddress.DataBindings.Clear();
                txtProvinceShipTo.DataBindings.Clear();
                txtProvinceSoldTo.DataBindings.Clear();
                txtDescription.DataBindings.Clear();


                cmbRoleName.DataBindings.Add("Text", Company, "CompanyCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCompanyName.DataBindings.Add("Text", Company, "CompanyName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCompanyAddress.DataBindings.Add("Text", Company, "CompanyAddress", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProvinceShipTo.DataBindings.Add("Text", Company, "ProvinceShipTo", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProvinceSoldTo.DataBindings.Add("Text", Company, "ProvinceSoldTo", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", Company, "Description", true, DataSourceUpdateMode.OnPropertyChanged);

                if (_company.Status == Company.status.Active)
                {
                    rdbActive.Checked = true;
                }
                else
                {
                    rdbInactive.Checked = true;
                }
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    txtCompanyName.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        public async void Print()
        {
            var CompanyPresenter = _presenter as ICompanyPresenter;
            await CompanyPresenter.Print();
        }
    }
}
