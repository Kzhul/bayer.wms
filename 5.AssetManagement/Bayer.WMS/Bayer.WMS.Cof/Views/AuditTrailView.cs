﻿using Bayer.WMS.Base;
using Bayer.WMS.Cof.Presenters;
using Bayer.WMS.Objs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Cof.Views
{
    public partial class AuditTrailView : BaseForm, IAuditTrailView
    {
        public AuditTrailView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgAuditTrail);
        }

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            try
            {
                await _presenter.LoadMainData();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnSearch.Enabled = true;
            }
        }

        public override void InitializeComboBox()
        {
            //colAuditTrail_Action.ValueMember = Objs.Models.AuditTrail.action.ValueMember;
            //colAuditTrail_Action.DisplayMember = Objs.Models.AuditTrail.action.DisplayMember;
            //colAuditTrail_Action.DataSource = Objs.Models.AuditTrail.action.Get();
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DateTime FromDate
        {
            get { return dtpFromDate.Value; }
        }

        public DateTime ToDate
        {
            get { return dtpToDate.Value; }
        }

        public string Code
        {
            get { return txtCode.Text; }
        }

        public string User
        {
            get { return txtUser.Text; }
        }

        public DataTable AuditTrail
        {
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsAuditTrail.DataSource = value;
                    });
                }
                else
                {
                    bdsAuditTrail.DataSource = value;
                }
            }
        }
    }
}
