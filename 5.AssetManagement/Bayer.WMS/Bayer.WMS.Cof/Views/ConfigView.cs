﻿using Bayer.WMS.Base;
using Bayer.WMS.Cof.Presenters;
using Bayer.WMS.Objs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Cof.Views
{
    public partial class ConfigView : BaseForm, IConfigView
    {
        public ConfigView()
        {
            InitializeComponent();
        }

        private void chkPackagingConfig_IsMultScreen_CheckedChanged(object sender, EventArgs e)
        {
            ////
            //// TODO: won't do anything
            ////

            //btnPackagingConfig_Delete.Enabled = chkPackagingConfig_IsMultScreen.Checked;
            //dtgPackagingConfig_ScreenConfig.AllowUserToAddRows = chkPackagingConfig_IsMultScreen.Checked;
            //dtgPackagingConfig_ScreenConfig.ReadOnly = !chkPackagingConfig_IsMultScreen.Checked;
        }

        private void btnPackagingConfig_Delete_Click(object sender, EventArgs e)
        {
            var current = bdsPackagingConfig_MultiScreenConfigs.Current as PackagingMultScreenConfig;
            if (current != null)
            {
                bdsPackagingConfig_MultiScreenConfigs.RemoveCurrent();
                (_presenter as IConfigPresenter).DeletePackagingMultScreenConfig(current);
            }
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            var configPresenter = _presenter as IConfigPresenter;

            await configPresenter.LoadPackagingConfig();
        }

        public bool PackagingConfig_IsMultScreen
        {
            get { return chkPackagingConfig_IsMultScreen.Checked; }
            set { chkPackagingConfig_IsMultScreen.Checked = value; }
        }

        private IList<PackagingMultScreenConfig> _packagingConfig_MultiScreenConfigs;

        public IList<PackagingMultScreenConfig> PackagingConfig_MultiScreenConfigs
        {
            get { return _packagingConfig_MultiScreenConfigs; }
            set
            {
                _packagingConfig_MultiScreenConfigs = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsPackagingConfig_MultiScreenConfigs.DataSource = _packagingConfig_MultiScreenConfigs;
                    });
                }
                else
                {
                    bdsPackagingConfig_MultiScreenConfigs.DataSource = _packagingConfig_MultiScreenConfigs;
                }
            }
        }

        private PrinterConfig _printerConfig;

        public PrinterConfig PrinterConfig
        {
            get { return _printerConfig; }
            set
            {
                _printerConfig = value;

                txtPrinter_COMPort.DataBindings.Clear();
                cmbPrinter_BaudRate.DataBindings.Clear();
                cmbPrinter_Parity.DataBindings.Clear();
                cmbPrinter_DataBits.DataBindings.Clear();
                cmbPrinter_StopBits.DataBindings.Clear();
                chkMultiProductPerPallet.DataBindings.Clear();
                txtMultiProductPerPalletReason.DataBindings.Clear();

                txtPrinter_COMPort.DataBindings.Add("Text", PrinterConfig, "PortName", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbPrinter_BaudRate.DataBindings.Add("SelectedItem", PrinterConfig, "BaudRate", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbPrinter_Parity.DataBindings.Add("SelectedItem", PrinterConfig, "Parity", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbPrinter_DataBits.DataBindings.Add("SelectedItem", PrinterConfig, "DataBits", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbPrinter_StopBits.DataBindings.Add("SelectedItem", PrinterConfig, "StopBits", true, DataSourceUpdateMode.OnPropertyChanged);

                chkMultiProductPerPallet.DataBindings.Add("Checked", PrinterConfig, "MultiProductPerPallet", true, DataSourceUpdateMode.OnPropertyChanged);
                txtMultiProductPerPalletReason.DataBindings.Add("Text", PrinterConfig, "MultiProductPerPalletReason", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }
    }
}
