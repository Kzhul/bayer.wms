﻿namespace Bayer.WMS.Cof.Views
{
    partial class ConfigView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigView));
            this.chkPackagingConfig_IsMultScreen = new System.Windows.Forms.CheckBox();
            this.dtgPackagingConfig_ScreenConfig = new System.Windows.Forms.DataGridView();
            this.colPackagingConfig_Screen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingConfig_PackagingLine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingConfig_PortName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingConfig_BaudRate = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colPackagingConfig_Parity = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colPackagingConfig_DataBits = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.StopBits = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bdsPackagingConfig_MultiScreenConfigs = new System.Windows.Forms.BindingSource(this.components);
            this.btnPackagingConfig_Delete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPrinter_StopBits = new System.Windows.Forms.ComboBox();
            this.cmbPrinter_DataBits = new System.Windows.Forms.ComboBox();
            this.cmbPrinter_Parity = new System.Windows.Forms.ComboBox();
            this.cmbPrinter_BaudRate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPrinter_COMPort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMultiProductPerPalletReason = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkMultiProductPerPallet = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPackagingConfig_ScreenConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackagingConfig_MultiScreenConfigs)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkPackagingConfig_IsMultScreen
            // 
            this.chkPackagingConfig_IsMultScreen.AutoSize = true;
            this.chkPackagingConfig_IsMultScreen.Location = new System.Drawing.Point(12, 12);
            this.chkPackagingConfig_IsMultScreen.Name = "chkPackagingConfig_IsMultScreen";
            this.chkPackagingConfig_IsMultScreen.Size = new System.Drawing.Size(167, 20);
            this.chkPackagingConfig_IsMultScreen.TabIndex = 0;
            this.chkPackagingConfig_IsMultScreen.Text = "Sử dụng nhiều màn hình";
            this.chkPackagingConfig_IsMultScreen.UseVisualStyleBackColor = true;
            this.chkPackagingConfig_IsMultScreen.CheckedChanged += new System.EventHandler(this.chkPackagingConfig_IsMultScreen_CheckedChanged);
            // 
            // dtgPackagingConfig_ScreenConfig
            // 
            this.dtgPackagingConfig_ScreenConfig.AllowUserToDeleteRows = false;
            this.dtgPackagingConfig_ScreenConfig.AllowUserToOrderColumns = true;
            this.dtgPackagingConfig_ScreenConfig.AllowUserToResizeRows = false;
            this.dtgPackagingConfig_ScreenConfig.AutoGenerateColumns = false;
            this.dtgPackagingConfig_ScreenConfig.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPackagingConfig_ScreenConfig.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPackagingConfig_ScreenConfig.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPackagingConfig_Screen,
            this.colPackagingConfig_PackagingLine,
            this.colPackagingConfig_PortName,
            this.colPackagingConfig_BaudRate,
            this.colPackagingConfig_Parity,
            this.colPackagingConfig_DataBits,
            this.StopBits});
            this.dtgPackagingConfig_ScreenConfig.DataSource = this.bdsPackagingConfig_MultiScreenConfigs;
            this.dtgPackagingConfig_ScreenConfig.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgPackagingConfig_ScreenConfig.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPackagingConfig_ScreenConfig.Location = new System.Drawing.Point(12, 74);
            this.dtgPackagingConfig_ScreenConfig.Name = "dtgPackagingConfig_ScreenConfig";
            this.dtgPackagingConfig_ScreenConfig.Size = new System.Drawing.Size(854, 195);
            this.dtgPackagingConfig_ScreenConfig.TabIndex = 2;
            // 
            // colPackagingConfig_Screen
            // 
            this.colPackagingConfig_Screen.DataPropertyName = "Screen";
            this.colPackagingConfig_Screen.HeaderText = "Màn hình";
            this.colPackagingConfig_Screen.Name = "colPackagingConfig_Screen";
            // 
            // colPackagingConfig_PackagingLine
            // 
            this.colPackagingConfig_PackagingLine.DataPropertyName = "PackagingLine";
            this.colPackagingConfig_PackagingLine.HeaderText = "Line đóng gói";
            this.colPackagingConfig_PackagingLine.Name = "colPackagingConfig_PackagingLine";
            this.colPackagingConfig_PackagingLine.Width = 120;
            // 
            // colPackagingConfig_PortName
            // 
            this.colPackagingConfig_PortName.DataPropertyName = "PortName";
            this.colPackagingConfig_PortName.HeaderText = "COM";
            this.colPackagingConfig_PortName.Name = "colPackagingConfig_PortName";
            // 
            // colPackagingConfig_BaudRate
            // 
            this.colPackagingConfig_BaudRate.DataPropertyName = "BaudRate";
            this.colPackagingConfig_BaudRate.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colPackagingConfig_BaudRate.HeaderText = "Baud Rate";
            this.colPackagingConfig_BaudRate.Items.AddRange(new object[] {
            "1200",
            "1800",
            "2400",
            "4800",
            "7200",
            "9600"});
            this.colPackagingConfig_BaudRate.Name = "colPackagingConfig_BaudRate";
            this.colPackagingConfig_BaudRate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colPackagingConfig_BaudRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // colPackagingConfig_Parity
            // 
            this.colPackagingConfig_Parity.DataPropertyName = "Parity";
            this.colPackagingConfig_Parity.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colPackagingConfig_Parity.HeaderText = "Parity";
            this.colPackagingConfig_Parity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Mark",
            "Odd",
            "Space"});
            this.colPackagingConfig_Parity.Name = "colPackagingConfig_Parity";
            this.colPackagingConfig_Parity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colPackagingConfig_Parity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // colPackagingConfig_DataBits
            // 
            this.colPackagingConfig_DataBits.DataPropertyName = "DataBits";
            this.colPackagingConfig_DataBits.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colPackagingConfig_DataBits.HeaderText = "Data Bits";
            this.colPackagingConfig_DataBits.Items.AddRange(new object[] {
            "6",
            "7",
            "8",
            "9"});
            this.colPackagingConfig_DataBits.Name = "colPackagingConfig_DataBits";
            this.colPackagingConfig_DataBits.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colPackagingConfig_DataBits.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // StopBits
            // 
            this.StopBits.DataPropertyName = "StopBits";
            this.StopBits.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.StopBits.HeaderText = "Stop Bits";
            this.StopBits.Items.AddRange(new object[] {
            "1",
            "1.5",
            "2"});
            this.StopBits.Name = "StopBits";
            this.StopBits.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.StopBits.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnPackagingConfig_Delete
            // 
            this.btnPackagingConfig_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btnPackagingConfig_Delete.Image")));
            this.btnPackagingConfig_Delete.Location = new System.Drawing.Point(12, 38);
            this.btnPackagingConfig_Delete.Name = "btnPackagingConfig_Delete";
            this.btnPackagingConfig_Delete.Size = new System.Drawing.Size(50, 30);
            this.btnPackagingConfig_Delete.TabIndex = 1;
            this.btnPackagingConfig_Delete.TabStop = false;
            this.btnPackagingConfig_Delete.UseVisualStyleBackColor = true;
            this.btnPackagingConfig_Delete.Click += new System.EventHandler(this.btnPackagingConfig_Delete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbPrinter_StopBits);
            this.groupBox1.Controls.Add(this.cmbPrinter_DataBits);
            this.groupBox1.Controls.Add(this.cmbPrinter_Parity);
            this.groupBox1.Controls.Add(this.cmbPrinter_BaudRate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtPrinter_COMPort);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(12, 275);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 178);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Máy in nhãn";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(7, 146);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 16);
            this.label5.TabIndex = 39;
            this.label5.Text = "Stop Bits:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(7, 116);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 16);
            this.label4.TabIndex = 38;
            this.label4.Text = "Data Bits:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(7, 86);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 16);
            this.label2.TabIndex = 37;
            this.label2.Text = "Parity:";
            // 
            // cmbPrinter_StopBits
            // 
            this.cmbPrinter_StopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinter_StopBits.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbPrinter_StopBits.FormattingEnabled = true;
            this.cmbPrinter_StopBits.Items.AddRange(new object[] {
            "1",
            "1.5",
            "2"});
            this.cmbPrinter_StopBits.Location = new System.Drawing.Point(90, 143);
            this.cmbPrinter_StopBits.Name = "cmbPrinter_StopBits";
            this.cmbPrinter_StopBits.Size = new System.Drawing.Size(121, 24);
            this.cmbPrinter_StopBits.TabIndex = 4;
            // 
            // cmbPrinter_DataBits
            // 
            this.cmbPrinter_DataBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinter_DataBits.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbPrinter_DataBits.FormattingEnabled = true;
            this.cmbPrinter_DataBits.Items.AddRange(new object[] {
            "6",
            "7",
            "8",
            "9"});
            this.cmbPrinter_DataBits.Location = new System.Drawing.Point(90, 113);
            this.cmbPrinter_DataBits.Name = "cmbPrinter_DataBits";
            this.cmbPrinter_DataBits.Size = new System.Drawing.Size(121, 24);
            this.cmbPrinter_DataBits.TabIndex = 3;
            // 
            // cmbPrinter_Parity
            // 
            this.cmbPrinter_Parity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinter_Parity.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbPrinter_Parity.FormattingEnabled = true;
            this.cmbPrinter_Parity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Mark",
            "Odd",
            "Space"});
            this.cmbPrinter_Parity.Location = new System.Drawing.Point(90, 83);
            this.cmbPrinter_Parity.Name = "cmbPrinter_Parity";
            this.cmbPrinter_Parity.Size = new System.Drawing.Size(121, 24);
            this.cmbPrinter_Parity.TabIndex = 2;
            // 
            // cmbPrinter_BaudRate
            // 
            this.cmbPrinter_BaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrinter_BaudRate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbPrinter_BaudRate.FormattingEnabled = true;
            this.cmbPrinter_BaudRate.Items.AddRange(new object[] {
            "1200",
            "1800",
            "2400",
            "4800",
            "7200",
            "9600"});
            this.cmbPrinter_BaudRate.Location = new System.Drawing.Point(90, 53);
            this.cmbPrinter_BaudRate.Name = "cmbPrinter_BaudRate";
            this.cmbPrinter_BaudRate.Size = new System.Drawing.Size(121, 24);
            this.cmbPrinter_BaudRate.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(7, 56);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 16);
            this.label1.TabIndex = 32;
            this.label1.Text = "Baud Rate:";
            // 
            // txtPrinter_COMPort
            // 
            this.txtPrinter_COMPort.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtPrinter_COMPort.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtPrinter_COMPort.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtPrinter_COMPort.Location = new System.Drawing.Point(90, 25);
            this.txtPrinter_COMPort.Name = "txtPrinter_COMPort";
            this.txtPrinter_COMPort.Size = new System.Drawing.Size(121, 22);
            this.txtPrinter_COMPort.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(7, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 16);
            this.label3.TabIndex = 30;
            this.label3.Text = "Cổng COM:";
            // 
            // txtMultiProductPerPalletReason
            // 
            this.txtMultiProductPerPalletReason.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMultiProductPerPalletReason.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtMultiProductPerPalletReason.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMultiProductPerPalletReason.Location = new System.Drawing.Point(54, 41);
            this.txtMultiProductPerPalletReason.Name = "txtMultiProductPerPalletReason";
            this.txtMultiProductPerPalletReason.Size = new System.Drawing.Size(221, 22);
            this.txtMultiProductPerPalletReason.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(3, 44);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 16);
            this.label6.TabIndex = 41;
            this.label6.Text = "Lý do:";
            // 
            // chkMultiProductPerPallet
            // 
            this.chkMultiProductPerPallet.AutoSize = true;
            this.chkMultiProductPerPallet.Location = new System.Drawing.Point(6, 21);
            this.chkMultiProductPerPallet.Name = "chkMultiProductPerPallet";
            this.chkMultiProductPerPallet.Size = new System.Drawing.Size(249, 20);
            this.chkMultiProductPerPallet.TabIndex = 42;
            this.chkMultiProductPerPallet.Text = "Cho phép chất nhiều lô lên 1 màn hình";
            this.chkMultiProductPerPallet.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkMultiProductPerPallet);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtMultiProductPerPalletReason);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(251, 275);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(281, 178);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chất hàng";
            // 
            // ConfigView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 607);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnPackagingConfig_Delete);
            this.Controls.Add(this.dtgPackagingConfig_ScreenConfig);
            this.Controls.Add(this.chkPackagingConfig_IsMultScreen);
            this.Name = "ConfigView";
            this.Text = "ConfigView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgPackagingConfig_ScreenConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackagingConfig_MultiScreenConfigs)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkPackagingConfig_IsMultScreen;
        private System.Windows.Forms.DataGridView dtgPackagingConfig_ScreenConfig;
        private System.Windows.Forms.BindingSource bdsPackagingConfig_MultiScreenConfigs;
        private System.Windows.Forms.Button btnPackagingConfig_Delete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbPrinter_StopBits;
        private System.Windows.Forms.ComboBox cmbPrinter_DataBits;
        private System.Windows.Forms.ComboBox cmbPrinter_Parity;
        private System.Windows.Forms.ComboBox cmbPrinter_BaudRate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPrinter_COMPort;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingConfig_Screen;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingConfig_PackagingLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingConfig_PortName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colPackagingConfig_BaudRate;
        private System.Windows.Forms.DataGridViewComboBoxColumn colPackagingConfig_Parity;
        private System.Windows.Forms.DataGridViewComboBoxColumn colPackagingConfig_DataBits;
        private System.Windows.Forms.DataGridViewComboBoxColumn StopBits;
        private System.Windows.Forms.TextBox txtMultiProductPerPalletReason;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkMultiProductPerPallet;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}