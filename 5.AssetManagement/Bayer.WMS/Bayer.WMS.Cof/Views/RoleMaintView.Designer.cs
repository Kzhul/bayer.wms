﻿namespace Bayer.WMS.Cof.Views
{
    partial class RoleMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label8 = new System.Windows.Forms.Label();
            this.dtgSitemap = new System.Windows.Forms.DataGridView();
            this.colRoleSitemap_SitemapDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRoleSitemap_AccessRights = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bdsRoleSiteMap = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbRoleName = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgSitemap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRoleSiteMap)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(13, 77);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 16);
            this.label8.TabIndex = 41;
            this.label8.Text = "Danh sách chức năng";
            // 
            // dtgSitemap
            // 
            this.dtgSitemap.AllowUserToAddRows = false;
            this.dtgSitemap.AllowUserToDeleteRows = false;
            this.dtgSitemap.AllowUserToOrderColumns = true;
            this.dtgSitemap.AllowUserToResizeRows = false;
            this.dtgSitemap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgSitemap.AutoGenerateColumns = false;
            this.dtgSitemap.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgSitemap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgSitemap.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colRoleSitemap_SitemapDescription,
            this.colRoleSitemap_AccessRights});
            this.dtgSitemap.DataSource = this.bdsRoleSiteMap;
            this.dtgSitemap.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgSitemap.GridColor = System.Drawing.SystemColors.Control;
            this.dtgSitemap.Location = new System.Drawing.Point(12, 96);
            this.dtgSitemap.Name = "dtgSitemap";
            this.dtgSitemap.Size = new System.Drawing.Size(976, 495);
            this.dtgSitemap.TabIndex = 38;
            this.dtgSitemap.CurrentCellDirtyStateChanged += new System.EventHandler(this.dtgSitemap_CurrentCellDirtyStateChanged);
            // 
            // colRoleSitemap_SitemapDescription
            // 
            this.colRoleSitemap_SitemapDescription.DataPropertyName = "SitemapDescription";
            this.colRoleSitemap_SitemapDescription.HeaderText = "Tên chức năng";
            this.colRoleSitemap_SitemapDescription.Name = "colRoleSitemap_SitemapDescription";
            this.colRoleSitemap_SitemapDescription.ReadOnly = true;
            this.colRoleSitemap_SitemapDescription.Width = 200;
            // 
            // colRoleSitemap_AccessRights
            // 
            this.colRoleSitemap_AccessRights.DataPropertyName = "AccessRights";
            this.colRoleSitemap_AccessRights.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colRoleSitemap_AccessRights.HeaderText = "Quyền truy cập";
            this.colRoleSitemap_AccessRights.Name = "colRoleSitemap_AccessRights";
            this.colRoleSitemap_AccessRights.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colRoleSitemap_AccessRights.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colRoleSitemap_AccessRights.Width = 200;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 16);
            this.label4.TabIndex = 39;
            this.label4.Text = "Mô tả";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(116, 42);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(400, 22);
            this.txtDescription.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 16);
            this.label3.TabIndex = 35;
            this.label3.Text = "Tên vai trò";
            // 
            // cmbRoleName
            // 
            this.cmbRoleName.Columns = null;
            this.cmbRoleName.DropDownHeight = 1;
            this.cmbRoleName.DropDownWidth = 500;
            this.cmbRoleName.FormattingEnabled = true;
            this.cmbRoleName.IntegralHeight = false;
            this.cmbRoleName.Location = new System.Drawing.Point(116, 12);
            this.cmbRoleName.MaxLength = 255;
            this.cmbRoleName.Name = "cmbRoleName";
            this.cmbRoleName.PageSize = 0;
            this.cmbRoleName.PresenterInfo = null;
            this.cmbRoleName.Size = new System.Drawing.Size(400, 24);
            this.cmbRoleName.Source = null;
            this.cmbRoleName.TabIndex = 45;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(522, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 16);
            this.label7.TabIndex = 46;
            this.label7.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(522, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 16);
            this.label1.TabIndex = 47;
            this.label1.Text = "*";
            // 
            // RoleMaintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 603);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbRoleName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dtgSitemap);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "RoleMaintView";
            this.Text = "RoleMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgSitemap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRoleSiteMap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dtgSitemap;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label3;
        private CustomControls.MultiColumnComboBox cmbRoleName;
        private System.Windows.Forms.BindingSource bdsRoleSiteMap;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRoleSitemap_SitemapDescription;
        private System.Windows.Forms.DataGridViewComboBoxColumn colRoleSitemap_AccessRights;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
    }
}