﻿using Bayer.WMS.CustomControls;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Bayer.WMS.Objs.Utility;

namespace Bayer.WMS.Base
{
    public partial class BaseForm : Form, IBaseView
    {
        protected IBasePresenter _presenter;
        protected IBasePresenter _mainPresenter;

        public BaseForm()
        {
            InitializeComponent();
        }

        private void ShowMessageBoxError(string message, string caption, MessageBoxIcon icon, string parent)
        {
            Bitmap img = null;
            if (icon == MessageBoxIcon.Error)
                img = new Bitmap(Resources.Error);
            else if (icon == MessageBoxIcon.Warning)
                img = new Bitmap(Resources.Warning);
            else if (icon == MessageBoxIcon.Information)
                img = new Bitmap(Resources.Info);

            var messageBox = new CustomMessageBox();
            if (!String.IsNullOrWhiteSpace(parent))
                messageBox.Name = $"{parent}_CustomMessageBox";
            messageBox.ShowMessage(this, message, caption, img);
        }

        public virtual void InitializeComboBox()
        {

        }

        public virtual void Insert()
        {
            try
            {
                _presenter.Insert();
            }
            catch (WrappedException ex)
            {
                SetMessage(ex.Message, MessageType.Error);
            }
        }

        public virtual async Task Refresh(int sitemapID)
        {
            try
            {
                await _presenter.Refresh(sitemapID);
            }
            catch (WrappedException ex)
            {
                SetMessage(ex.Message, MessageType.Error);
            }
        }

        public virtual async Task Save()
        {
            try
            {
                await _presenter.Save();
            }
            catch (WrappedException ex)
            {
                SetMessage(ex.Message, MessageType.Error);
            }
        }

        public virtual async Task Delete()
        {
            try
            {
                using (var cfm = new CustomConfirmBox())
                {
                    if (cfm.ShowDialog(this, "Bạn có chắc chắn muôn xóa?", "Xác nhận") != DialogResult.Yes)
                        return;
                }

                await _presenter.Delete();
            }
            catch (WrappedException ex)
            {
                SetMessage(ex.Message, MessageType.Error);
            }
        }

        public virtual async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await Task.Run(() => { _presenter = presenter; });
        }

        public virtual void SetMessage(string message, MessageType messageType, string parent = "")
        {
            switch (messageType)
            {
                case MessageType.Warning:
                    ShowMessageBoxError(message, "Cảnh báo", MessageBoxIcon.Warning, parent);
                    break;
                case MessageType.Error:
                    ShowMessageBoxError(message, "Lỗi", MessageBoxIcon.Error, parent);
                    break;
                case MessageType.Information:
                    ShowMessageBoxError(message, "Thông báo", MessageBoxIcon.Information, parent);
                    break;
                default:
                    break;
            }
        }

        public virtual bool GetConfirm(string message)
        {
            var messageBox = new CustomConfirmBox();
            messageBox.Name= $"{this.Name}_CustomMessageBox";
            return messageBox.ShowDialog(this, message, "Xác nhận") == DialogResult.Yes;
        }

        public virtual void GetConfirmVoid(string message, Action<CustomConfirmBox> invokeOK, Action<CustomConfirmBox> invokeCancel)
        {
            var messageBox = new CustomConfirmBox();
            messageBox.Name = $"{this.Name}_CustomMessageBox";
            messageBox.InvokeOK = invokeOK;
            messageBox.InvokeCancel = invokeCancel;
            messageBox.Show(this, message, "Xác nhận");
        }

        public virtual void HandlePermission(string accessRight)
        {

        }

        protected void SetDoubleBuffered(DataGridView dtg)
        {
            Type dgvType = dtg.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dtg, true, null);

            dtg.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(Utility.dgGrid_RowPostPaint);
        }

        public Sitemap Sitemap { get; set; }
    }
}
