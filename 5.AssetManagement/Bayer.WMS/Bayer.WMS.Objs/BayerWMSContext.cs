namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    public interface IBayerWMSContext
    {
        void Refresh(object entity);
    }

    public partial class BayerWMSContext : DbContext, IBayerWMSContext
    {
        public BayerWMSContext()
            : base(Utility.ConnStr)
        {
            Configuration.LazyLoadingEnabled = true;
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = 300;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AppSettingConfiguration());
            modelBuilder.Configurations.Add(new AuditTrailConfiguration());
            modelBuilder.Configurations.Add(new BarcodeBankConfiguration());
            modelBuilder.Configurations.Add(new CartonBarcodeConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new CompanyConfiguration());
            modelBuilder.Configurations.Add(new DeliveryNoteDetailConfiguration());
            modelBuilder.Configurations.Add(new DeliveryNoteHeaderConfiguration());
            modelBuilder.Configurations.Add(new DeliveryOrderDetailConfiguration());
            modelBuilder.Configurations.Add(new DeliveryOrderHeaderConfiguration());
            modelBuilder.Configurations.Add(new DeviceConfiguration());
            modelBuilder.Configurations.Add(new PackagingErrorConfiguration());
            modelBuilder.Configurations.Add(new PackagingForceEndConfiguration());
            modelBuilder.Configurations.Add(new PackagingLogConfiguration());
            modelBuilder.Configurations.Add(new PackagingSampleConfiguration());
            modelBuilder.Configurations.Add(new PalletConfiguration());
            modelBuilder.Configurations.Add(new PalletStatusConfiguration());
            modelBuilder.Configurations.Add(new ProductBarcodeConfiguration());
            modelBuilder.Configurations.Add(new ProductionPlanImportLogConfiguration());
            modelBuilder.Configurations.Add(new ProductionPlanConfiguration());
            modelBuilder.Configurations.Add(new ProductPackingConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new RoleSitemapConfiguration());
            modelBuilder.Configurations.Add(new SitemapConfiguration());
            modelBuilder.Configurations.Add(new SplitNoteDetailConfiguration());
            modelBuilder.Configurations.Add(new SplitNoteHeaderConfiguration());
            modelBuilder.Configurations.Add(new UOMConfiguration());
            modelBuilder.Configurations.Add(new UsersPasswordConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UsersRoleConfiguration());
            modelBuilder.Configurations.Add(new PrepareNoteDetailConfiguration());
            modelBuilder.Configurations.Add(new PrepareNoteHeaderConfiguration());
            modelBuilder.Configurations.Add(new DeliveryTicketDetailConfiguration());
            modelBuilder.Configurations.Add(new DeliveryTicketHeaderConfiguration());
            modelBuilder.Configurations.Add(new ReturnDODetailConfiguration());
            modelBuilder.Configurations.Add(new ReturnDOHeaderConfiguration());
            modelBuilder.Configurations.Add(new StockReceivingDetailConfiguration());
            modelBuilder.Configurations.Add(new StockReceivingHeaderConfiguration());
            modelBuilder.Configurations.Add(new WarehouseConfiguration());
            modelBuilder.Configurations.Add(new ZoneConfiguration());
            modelBuilder.Configurations.Add(new ZoneLineConfiguration());
            modelBuilder.Configurations.Add(new ZoneLocationConfiguration());
            modelBuilder.Configurations.Add(new ZoneCategoryConfiguration());
            modelBuilder.Configurations.Add(new ReferenceConfiguration());
            modelBuilder.Configurations.Add(new WarehouseVisualizationConfiguration());
            modelBuilder.Configurations.Add(new StockReturnDetailConfiguration());
            modelBuilder.Configurations.Add(new StockReturnHeaderConfiguration());
            modelBuilder.Configurations.Add(new ProductReturnDetailConfiguration());
            modelBuilder.Configurations.Add(new ProductReturnHeaderConfiguration());
            modelBuilder.Configurations.Add(new ProductReturnDetailSplitConfiguration());
            modelBuilder.Configurations.Add(new AssetConfiguration());
            modelBuilder.Configurations.Add(new InventoryConfiguration());
            modelBuilder.Configurations.Add(new InventoryCheckConfiguration());
            modelBuilder.Configurations.Add(new InventoryMachineAssignConfiguration());
        }

        public override Task<int> SaveChangesAsync()
        {
            var objectContext = ((IObjectContextAdapter)this).ObjectContext;

            var auditableEntries = (from entry in objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified | EntityState.Deleted)
                                    where !entry.IsRelationship
                                    let entity = entry.Entity as BaseEntity
                                    where entity != null
                                    select new
                                    {
                                        Entity = entity,
                                        Entry = entry
                                    }).ToList();

            foreach (var entry in auditableEntries.Where(x => x.Entry.State == EntityState.Added))
            {
                entry.Entity.CreatedBy = LoginInfo.UserID;
                entry.Entity.CreatedBySitemapID = Utility.CurrentSitemapID;
                entry.Entity.CreatedDateTime = DateTime.Now;
                entry.Entity.UpdatedBy = LoginInfo.UserID;
                entry.Entity.UpdatedBySitemapID = Utility.CurrentSitemapID;
                entry.Entity.UpdatedDateTime = DateTime.Now;

                if (entry.Entity.IsRecordAuditTrail)
                    this.AuditTrails.Add(new AuditTrail(entry.Entry.EntityKey.EntitySetName, entry.Entity.ToXML(), AuditTrail.action.Insert));
            }

            foreach (var entry in auditableEntries.Where(x => x.Entry.State == EntityState.Modified))
            {
                entry.Entry.SetModifiedProperty("UpdatedDateTime");
                entry.Entry.SetModifiedProperty("UpdatedBy");

                entry.Entity.UpdatedBy = LoginInfo.UserID;
                entry.Entity.UpdatedBySitemapID = Utility.CurrentSitemapID;
                entry.Entity.UpdatedDateTime = DateTime.Now;

                if (entry.Entity.IsRecordAuditTrail)
                    this.AuditTrails.Add(new AuditTrail(entry.Entry.EntityKey.EntitySetName, entry.Entity.ToXML(), AuditTrail.action.Update));
            }

            foreach (var entry in auditableEntries.Where(x => x.Entry.State == EntityState.Deleted))
            {
                if (entry.Entity.IsRecordAuditTrail)
                    this.AuditTrails.Add(new AuditTrail(entry.Entry.EntityKey.EntitySetName, entry.Entity.ToXML(), AuditTrail.action.Delete));
            }

            return base.SaveChangesAsync();
        }

        public virtual void Refresh(object entity)
        {
            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            objectContext.Refresh(RefreshMode.StoreWins, entity);
        }

        public virtual DbSet<AppSetting> AppSettings { get; set; }
        public virtual DbSet<AuditTrail> AuditTrails { get; set; }
        public virtual DbSet<BarcodeBank> BarcodeBanks { get; set; }
        public virtual DbSet<CartonBarcode> CartonBarcodes { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<DeliveryNoteDetail> DeliveryNoteDetails { get; set; }
        public virtual DbSet<DeliveryNoteHeader> DeliveryNoteHeaders { get; set; }
        public virtual DbSet<DeliveryOrderDetail> DeliveryOrderDetails { get; set; }
        public virtual DbSet<DeliveryOrderHeader> DeliveryOrderHeaders { get; set; }
        public virtual DbSet<PackagingError> PackagingErrors { get; set; }
        public virtual DbSet<PackagingForceEnd> PackagingForceEnds { get; set; }
        public virtual DbSet<PackagingLog> PackagingLogs { get; set; }
        public virtual DbSet<PackagingSample> PackagingSamples { get; set; }
        public virtual DbSet<Pallet> Pallets { get; set; }
        public virtual DbSet<PalletStatus> PalletStatuses { get; set; }
        public virtual DbSet<ProductBarcode> ProductBarcodes { get; set; }
        public virtual DbSet<ProductionPlanImportLog> ProductionPlanImportLogs { get; set; }
        public virtual DbSet<ProductionPlan> ProductionPlans { get; set; }
        public virtual DbSet<ProductPacking> ProductPackings { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleSitemap> RoleSitemaps { get; set; }
        public virtual DbSet<Sitemap> Sitemaps { get; set; }
        public virtual DbSet<SplitNoteDetail> SplitNoteDetails { get; set; }
        public virtual DbSet<SplitNoteHeader> SplitNoteHeaders { get; set; }
        public virtual DbSet<UOM> UOMs { get; set; }
        public virtual DbSet<UsersPassword> UsersPasswords { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UsersRole> UsersRoles { get; set; }
        public virtual DbSet<PrepareNoteDetail> PrepareNoteDetails { get; set; }
        public virtual DbSet<PrepareNoteHeader> PrepareNoteHeaders { get; set; }
        public virtual DbSet<DeliveryTicketDetail> DeliveryTicketDetails { get; set; }
        public virtual DbSet<DeliveryTicketHeader> DeliveryTicketHeaders { get; set; }
        public virtual DbSet<ReturnDODetail> ReturnDODetails { get; set; }
        public virtual DbSet<ReturnDOHeader> ReturnDOHeaders { get; set; }
        public virtual DbSet<StockReceivingDetail> StockReceivingDetails { get; set; }
        public virtual DbSet<StockReceivingHeader> StockReceivingHeaders { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<Zone> Zones { get; set; }
        public virtual DbSet<ZoneLine> ZoneLines { get; set; }
        public virtual DbSet<ZoneLocation> ZoneLocations { get; set; }
        public virtual DbSet<ZoneCategory> ZoneCategories { get; set; }
        public virtual DbSet<ZoneTemperature> ZoneTemperatures { get; set; }
        public virtual DbSet<Reference> References { get; set; }
        public virtual DbSet<WarehouseVisualization> WarehouseVisualizations { get; set; }
        public virtual DbSet<RequestMaterial> RequestMaterials { get; set; }
        public virtual DbSet<RequestMaterialLine> RequestMaterialLines { get; set; }
        public virtual DbSet<RequestMaterialLineSplit> RequestMaterialLineSplits { get; set; }
        public virtual DbSet<StockReturnDetail> StockReturnDetails { get; set; }
        public virtual DbSet<StockReturnHeader> StockReturnHeaders { get; set; }

        public virtual DbSet<ProductReturnDetail> ProductReturnDetails { get; set; }
        public virtual DbSet<ProductReturnDetailSplit> ProductReturnDetailSplits { get; set; }
        public virtual DbSet<ProductReturnHeader> ProductReturnHeaders { get; set; }
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<Inventory> Inventories { get; set; }
        public virtual DbSet<InventoryCheck> InventoryChecks { get; set; }
        public virtual DbSet<InventoryMachineAssign> InventoryMachineAssigns { get; set; }
    }
}
