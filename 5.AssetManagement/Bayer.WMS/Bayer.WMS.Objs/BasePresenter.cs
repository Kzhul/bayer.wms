﻿using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Practices.Unity;
using System;
using System.Reflection;
using System.Threading.Tasks;
using static Bayer.WMS.Objs.Utility;

namespace Bayer.WMS.Objs
{
    public interface IBaseView
    {
        Sitemap Sitemap { get; }

        void SetMessage(string message, MessageType messageType, string parent);

        bool GetConfirm(string message);
    }

    public interface IBasePresenter
    {
        Task LoadMainData();

        void Insert();

        Task Refresh();

        Task Refresh(int sitemapID);

        Task Save();

        Task Delete();

        IBasePresenter GetMainPresenter();

        void SetMessage(string message, MessageType messageType, string parent = "");

        bool GetConfirm(string message);

        object Resolve(Type type, params ResolverOverride[] resolverOverride);
    }

    public class BasePresenter : IBasePresenter
    {
        protected IBaseView _view;
        protected IUnitOfWorkManager _unitOfWorkManager;
        protected IBasePresenter _mainPresenter;
        protected IAuditTrailRepository _auditRepository;

        public BasePresenter(IBaseView view, IUnitOfWorkManager unitOfWorkManager)
        {
            _view = view;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public BasePresenter(IBaseView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter)
        {
            _view = view;
            _unitOfWorkManager = unitOfWorkManager;
            _mainPresenter = mainPresenter;
        }

        public virtual async Task LoadMainData()
        {
            await Task.Run(() => { });
        }

        public virtual void Insert()
        {
        }

        public virtual async Task Refresh()
        {
            await Task.Run(() => { });
        }

        public virtual async Task Refresh(int sitemapID)
        {
            await Task.Run(() => { });
        }

        public virtual async Task Save()
        {
            await Task.Run(() => { });
        }

        public virtual async Task Delete()
        {
            await Task.Run(() => { });
        }

        public virtual void SetAuditTrailInformation(MethodBase method)
        {
            Utility.CurrentSitemapID = _view.Sitemap.SitemapID;
            Utility.CurrentMethod = method.ReflectedType.FullName;
        }

        public virtual IBasePresenter GetMainPresenter()
        {
            return _mainPresenter;
        }

        public virtual void SetMessage(string message, MessageType messageType, string parent = "")
        {
            _view.SetMessage(message, messageType, parent);
        }

        public virtual bool GetConfirm(string message)
        {
            return _view.GetConfirm(message);
        }

        public virtual object Resolve(Type type, params ResolverOverride[] resolverOverride)
        {
            throw new NotImplementedException();
        }

        public virtual void RecordAuditTrail(int userID, string action, MethodBase method, string description = "")
        {
            RecordAuditTrail(userID, null, null, action, method, description);
        }

        public virtual void RecordAuditTrail(string action, MethodBase method, string description = "")
        {
            RecordAuditTrail(null, null, action, method, description);
        }

        public virtual void RecordAuditTrail(string data, string action, MethodBase method, string description = "")
        {
            RecordAuditTrail(null, data, action, method, description);
        }

        public virtual async void RecordAuditTrail(string tableName, string data, string action, MethodBase method, string description = "")
        {
            //Task.Run(async () =>
            //{
                try
                {
                    _auditRepository = _mainPresenter.Resolve(typeof(IAuditTrailRepository)) as IAuditTrailRepository;

                    var auditTrail = new AuditTrail();
                    auditTrail.UserID = LoginInfo.UserID;
                    auditTrail.SitemapID = _view.Sitemap.SitemapID;
                    auditTrail.TableName = tableName;
                    auditTrail.Data = data;
                    auditTrail.Action = action;
                    auditTrail.Method = method.ReflectedType.FullName;
                    auditTrail.Date = DateTime.Today;
                    auditTrail.DateTime = DateTime.Now;
                    auditTrail.Description = description;

                    await _auditRepository.Insert(auditTrail);
                    await _auditRepository.Commit();
                }
                finally
                {
                    _auditRepository = _mainPresenter.Resolve(_auditRepository.GetType()) as IAuditTrailRepository;
                }
            //});
        }

        public virtual async void RecordAuditTrail(int userID, string tableName, string data, string action, MethodBase method, string description = "")
        {
            //Task.Run(async () =>
            //{
                try
                {
                    _auditRepository = _mainPresenter.Resolve(typeof(IAuditTrailRepository)) as IAuditTrailRepository;

                    var auditTrail = new AuditTrail();
                    auditTrail.UserID = userID;
                    auditTrail.SitemapID = _view.Sitemap.SitemapID;
                    auditTrail.TableName = tableName;
                    auditTrail.Data = data;
                    auditTrail.Action = action;
                    auditTrail.Method = method.ReflectedType.FullName;
                    auditTrail.Date = DateTime.Today;
                    auditTrail.DateTime = DateTime.Now;
                    auditTrail.Description = description;

                    await _auditRepository.Insert(auditTrail);
                    await _auditRepository.Commit();
                }
                finally
                {
                    _auditRepository = _mainPresenter.Resolve(_auditRepository.GetType()) as IAuditTrailRepository;
                }
            //});
        }
    }
}
