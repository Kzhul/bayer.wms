﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs
{
    public interface IUnitOfWorkManager : IDisposable
    {
        IUnitOfWork NewUnitOfWork();

        IUnitOfWork NewUnitOfWork2();

        IBayerWMSContext CurrentContext { get; }
    }

    public class UnitOfWorkManager : IUnitOfWorkManager
    {
        private bool _isDisposed;
        private readonly BayerWMSContext _context;

        public UnitOfWorkManager(IBayerWMSContext context)
        {
            Database.SetInitializer<BayerWMSContext>(null);
            _context = context as BayerWMSContext;
        }

        public IUnitOfWork NewUnitOfWork()
        {
            return new UnitOfWork(_context, true);
        }

        public IUnitOfWork NewUnitOfWork2()
        {
            return new UnitOfWork(_context, false);
        }

        public void Dispose()
        {
            if (_isDisposed == false)
            {
                _context.Dispose();
                _isDisposed = true;
            }
        }

        public IBayerWMSContext CurrentContext
        {
            get { return _context; }
        }
    }
}
