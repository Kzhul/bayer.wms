﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IProductReturnHeaderRepository : IBaseRepository, IBaseRepository<ProductReturnHeader>
    {
        Task<string> GetNextNBR();
        Task<IList<ProductReturnHeader>> GetIncludeCreatedByAsync(Expression<Func<ProductReturnHeader, bool>> predicate = null);
    }

    public class ProductReturnHeaderRepository : BaseRepository<BayerWMSContext, ProductReturnHeader>, IProductReturnHeaderRepository
    {
        public ProductReturnHeaderRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.ProductReturnHeaders.Where(a=>a.CreatedDateTime >= DateTime.Today).OrderByDescending(a => a.ProductReturnCode).FirstOrDefaultAsync();
            if (model != null)
            {
                lastestNumber = model.ProductReturnCode;
                string a = lastestNumber.Substring(lastestNumber.Length - 2);
                int NextDONumber = Convert.ToInt32(a) + 1;
                lastestNumber = "PR" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }
            else
            {
                int NextDONumber = 1;
                lastestNumber = "PR" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }

            return lastestNumber;
        }

        public virtual async Task<IList<ProductReturnHeader>> GetIncludeCreatedByAsync(Expression<Func<ProductReturnHeader, bool>> predicate = null)
        {
            var headers = await _context.ProductReturnHeaders
                                .Where(predicate)
                                .Join(_context.Users,
                                    srh => srh.CreatedBy,
                                    u => u.UserID,
                                    (srh, u) => new { ProductReturnHeader = srh, User = u })
                                .Join(_context.Companies,
                                    srh => srh.ProductReturnHeader.CompanyCode,
                                    u => u.CompanyCode,
                                    (srh, u) => new { ProductReturnHeader = srh.ProductReturnHeader, User = srh.User, Company = u })
                                .Select(p => new
                                {
                                    ProductReturnCode = p.ProductReturnHeader.ProductReturnCode,
                                    CompanyCode = p.ProductReturnHeader.CompanyCode,
                                    CompanyName = p.Company.CompanyName,
                                    Province = p.Company.ProvinceShipTo,
                                    Date = p.ProductReturnHeader.Date,
                                    Description = p.ProductReturnHeader.Description,
                                    Status = p.ProductReturnHeader.Status,
                                    CreatedByName = p.User.FirstName + " " + p.User.LastName,
                                    CreatedBy = p.ProductReturnHeader.CreatedBy,
                                    CreatedDateTime = p.ProductReturnHeader.CreatedDateTime,
                                    CreatedBySitemapID = p.ProductReturnHeader.CreatedBySitemapID,
                                    UpdatedBy = p.ProductReturnHeader.CreatedBy,
                                    UpdatedDateTime = p.ProductReturnHeader.UpdatedDateTime,
                                    UpdatedBySitemapID = p.ProductReturnHeader.UpdatedBySitemapID,
                                    RowVersion = p.ProductReturnHeader.RowVersion,
                                })
                                .ToListAsync();

            return headers.Select(p => new ProductReturnHeader(p)).ToList();
        }
    }
}
