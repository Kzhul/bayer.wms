﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IDeliveryTicketDetailRepository : IBaseRepository, IBaseRepository<DeliveryTicketDetail>
    {
        Task<IList<DeliveryTicketDetail>> GetIncludePrepareAsync(Expression<Func<DeliveryTicketDetail, bool>> predicate = null);
    }

    public class DeliveryTicketDetailRepository : BaseRepository<BayerWMSContext, DeliveryTicketDetail>, IDeliveryTicketDetailRepository
    {
        public DeliveryTicketDetailRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<DeliveryTicketDetail>> GetIncludePrepareAsync(Expression<Func<DeliveryTicketDetail, bool>> predicate = null)
        {
            var DeliveryTicketDetails = await _context.DeliveryTicketDetails
                                                .Where(predicate)
                                                .Join(_context.PrepareNoteHeaders,
                                                    pp => pp.PrepareCode,
                                                    p => p.PrepareCode,
                                                    (pp, p) => new { DeliveryTicketDetail = pp, PrepareHeader = p })
                                                .Select(p => new //DeliveryTicketDetail()
                                                {
                                                    DeliveryTicketCode = p.DeliveryTicketDetail.DeliveryTicketCode,
                                                    DOImportCode = p.PrepareHeader.DOImportCode,
                                                    PrepareCode = p.DeliveryTicketDetail.PrepareCode,
                                                    PrepareDate = p.PrepareHeader.PrepareDate,
                                                    DeliveryDate = p.PrepareHeader.DeliveryDate,
                                                    CompanyCode = p.PrepareHeader.CompanyCode,
                                                    CompanyName = p.PrepareHeader.CompanyName,
                                                    Province = p.PrepareHeader.Province,
                                                    Description = p.PrepareHeader.Description,
                                                    Status = p.DeliveryTicketDetail.Status,
                                                    CreatedBy = p.DeliveryTicketDetail.CreatedBy,
                                                    CreatedBySitemapID = p.DeliveryTicketDetail.CreatedBySitemapID,
                                                    CreatedDateTime = p.DeliveryTicketDetail.CreatedDateTime,
                                                    UpdatedBy = p.DeliveryTicketDetail.UpdatedBy,
                                                    UpdatedBySitemapID = p.DeliveryTicketDetail.UpdatedBySitemapID,
                                                    UpdatedDateTime = p.DeliveryTicketDetail.UpdatedDateTime,
                                                    
                                                    RowVersion = p.DeliveryTicketDetail.RowVersion
                                                })
                                                .Distinct()
                                                .ToListAsync();

            return DeliveryTicketDetails.Select(p => new DeliveryTicketDetail(p)).ToList();
        }


    }
}