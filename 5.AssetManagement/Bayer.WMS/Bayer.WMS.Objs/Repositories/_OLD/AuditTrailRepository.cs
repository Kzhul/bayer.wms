﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IAuditTrailRepository : IBaseRepository, IBaseRepository<AuditTrail>
    {
    }

    public class AuditTrailRepository : BaseRepository<BayerWMSContext, AuditTrail>, IAuditTrailRepository
    {
        public AuditTrailRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public override async Task Insert(AuditTrail entity)
        {
            await Task.Run(() =>
            {
                var conn = _context.Database.Connection as SqlConnection;
                if (conn.State != ConnectionState.Open && conn.State != ConnectionState.Connecting)
                    conn.Open();

                int threshold = 0;
                while (conn.State == ConnectionState.Connecting)
                {
                    threshold++;
                    if (threshold == 10)
                        throw new WrappedException("Kết nối không thành công, vui lòng kiểm tra lại cáp mạng hoặc wifi");
                    Thread.Sleep(1000);
                }

                using (var cmd = new SqlCommand("proc_AuditTrails_Insert", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();

                    cmd.Parameters.Add(new SqlParameter { ParameterName = "UserID", SqlDbType = SqlDbType.Int, Value = entity.UserID });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "SitemapID", SqlDbType = SqlDbType.Int, Value = entity.SitemapID });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "TableName", SqlDbType = SqlDbType.VarChar, Value = entity.TableName });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "Data", SqlDbType = SqlDbType.Xml, Value = entity.Data });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "Action", SqlDbType = SqlDbType.VarChar, Value = entity.Action });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "Method", SqlDbType = SqlDbType.VarChar, Value = entity.Method });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "Date", SqlDbType = SqlDbType.Date, Value = entity.Date });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "DateTime", SqlDbType = SqlDbType.DateTime, Value = entity.DateTime });
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "Description", SqlDbType = SqlDbType.NVarChar, Value = entity.Description });

                    cmd.ExecuteNonQuery();
                }
            });
        }
    }
}
