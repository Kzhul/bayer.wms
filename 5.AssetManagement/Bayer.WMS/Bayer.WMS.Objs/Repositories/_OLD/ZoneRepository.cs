﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IZoneRepository : IBaseRepository, IBaseRepository<Zone>
    {
        Task<IList<Zone>> GetIncludeWarehouseAsync(Expression<Func<Zone, bool>> predicate = null);
    }

    public class ZoneRepository : BaseRepository<BayerWMSContext, Zone>, IZoneRepository
    {
        public ZoneRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<Zone>> GetIncludeWarehouseAsync(Expression<Func<Zone, bool>> predicate = null)
        {
            var zones = await _context.Zones
                                        .Where(predicate)
                                        .Join(_context.Warehouses,
                                            z => z.WarehouseID,
                                            w => w.WarehouseID,
                                            (z, w) => new { Zone = z, Warehouse = w })
                                        .Select(p => new
                                        {
                                            ZoneCode = p.Zone.ZoneCode,
                                            ZoneName = p.Zone.ZoneName,
                                            Description = p.Zone.Description,
                                            WarehouseID = p.Warehouse.WarehouseID,
                                            WarehouseDescription = p.Warehouse.Description,
                                            Status = p.Zone.Status,
                                            IsDeleted = p.Zone.IsDeleted,
                                            CreatedBy = p.Zone.CreatedBy,
                                            CreatedBySitemapID = p.Zone.CreatedBySitemapID,
                                            CreatedDateTime = p.Zone.CreatedDateTime,
                                            UpdatedBy = p.Zone.CreatedBy,
                                            UpdatedBySitemapID = p.Zone.UpdatedBySitemapID,
                                            UpdatedDateTime = p.Zone.UpdatedDateTime,
                                            RowVersion = p.Zone.RowVersion
                                        }).ToListAsync();

            return zones.Select(p => new Zone(p)).ToList();
        }
    }
}
