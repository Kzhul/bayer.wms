﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IRequestMaterialLineRepository : IBaseRepository, IBaseRepository<RequestMaterialLine>
    {
        Task<IList<RequestMaterialLine>> GetIncludeProductAsync(Expression<Func<RequestMaterialLine, bool>> predicate = null);
    }

    public class RequestMaterialLineRepository : BaseRepository<BayerWMSContext, RequestMaterialLine>, IRequestMaterialLineRepository
    {
        public RequestMaterialLineRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<RequestMaterialLine>> GetIncludeProductAsync(Expression<Func<RequestMaterialLine, bool>> predicate = null)
        {
            var reqMaterialLines = await _context.RequestMaterialLines
                                        .Where(predicate)
                                        .Join(_context.Products,
                                            rml => rml.ProductID,
                                            p => p.ProductID,
                                            (rml, p) => new { RequestMaterialLine = rml, Product = p })
                                        .Select(p => new
                                        {
                                            DocumentNbr = p.RequestMaterialLine.DocumentNbr,
                                            LineNbr = p.RequestMaterialLine.LineNbr,
                                            ProductID = p.RequestMaterialLine.ProductID,
                                            ProductCode = p.Product.ProductCode,
                                            ProductDescription = p.Product.Description,
                                            RequestQty = p.RequestMaterialLine.RequestQty,
                                            BookedQty = p.RequestMaterialLine.BookedQty,
                                            PickedQty = p.RequestMaterialLine.PickedQty,
                                            TransferedQty = p.RequestMaterialLine.TransferedQty,
                                            UOM = p.RequestMaterialLine.UOM,
                                            Note = p.RequestMaterialLine.Note,
                                            Status = p.RequestMaterialLine.Status,
                                            CreatedBy = p.RequestMaterialLine.CreatedBy,
                                            CreatedBySitemapID = p.RequestMaterialLine.CreatedBySitemapID,
                                            CreatedDateTime = p.RequestMaterialLine.CreatedDateTime,
                                            UpdatedBy = p.RequestMaterialLine.CreatedBy,
                                            UpdatedBySitemapID = p.RequestMaterialLine.UpdatedBySitemapID,
                                            UpdatedDateTime = p.RequestMaterialLine.UpdatedDateTime,
                                            RowVersion = p.RequestMaterialLine.RowVersion
                                        }).ToListAsync();

            return reqMaterialLines.Select(p => new RequestMaterialLine(p)).ToList();
        }
    }
}
