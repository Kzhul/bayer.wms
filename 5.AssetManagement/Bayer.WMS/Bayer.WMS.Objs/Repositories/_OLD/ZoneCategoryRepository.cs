﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IZoneCategoryRepository : IBaseRepository, IBaseRepository<ZoneCategory>
    {
        Task<IList<ZoneCategory>> GetWithAllCategoriesAsync(Expression<Func<ZoneCategory, bool>> predicate = null);
    }

    public class ZoneCategoryRepository : BaseRepository<BayerWMSContext, ZoneCategory>, IZoneCategoryRepository
    {
        public ZoneCategoryRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<ZoneCategory>> GetWithAllCategoriesAsync(Expression<Func<ZoneCategory, bool>> predicate = null)
        {
            var zoneCategories = await _context.Categories.Where(p => !p.IsDeleted)
                                        .GroupJoin(_context.ZoneCategories.Where(predicate),
                                            c => c.CategoryID,
                                            zc => zc.CategoryID,
                                            (c, zc) => new { Category = c, ZoneCategory = zc.FirstOrDefault() })
                                        .Select(p => new
                                        {
                                            ZoneCode = p.ZoneCategory != null ? p.ZoneCategory.ZoneCode : String.Empty,
                                            CategoryID = p.Category.CategoryID,
                                            CategoryDescription = p.Category.Description,
                                            Checked = p.ZoneCategory != null,
                                            CreatedBy = p.ZoneCategory != null ? p.ZoneCategory.CreatedBy : null,
                                            CreatedBySitemapID = p.ZoneCategory != null ? p.ZoneCategory.CreatedBySitemapID : null,
                                            CreatedDateTime = p.ZoneCategory != null ? p.ZoneCategory.CreatedDateTime : null,
                                            UpdatedBy = p.ZoneCategory != null ? p.ZoneCategory.UpdatedBy : null,
                                            UpdatedBySitemapID = p.ZoneCategory != null ? p.ZoneCategory.UpdatedBySitemapID : null,
                                            UpdatedDateTime = p.ZoneCategory != null ? p.ZoneCategory.UpdatedDateTime : null,
                                            RowVersion = p.ZoneCategory != null ? p.ZoneCategory.RowVersion : null,
                                        }).ToListAsync();

            return zoneCategories.Select(p => new ZoneCategory(p)).ToList();
        }
    }
}
