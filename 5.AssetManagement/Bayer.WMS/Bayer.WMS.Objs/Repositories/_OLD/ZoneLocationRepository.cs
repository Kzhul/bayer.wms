﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IZoneLocationRepository : IBaseRepository, IBaseRepository<ZoneLocation>
    {
        Task<IList<ZoneLocation>> GetByWarehouseID(int warehouseID);
    }

    public class ZoneLocationRepository : BaseRepository<BayerWMSContext, ZoneLocation>, IZoneLocationRepository
    {
        public ZoneLocationRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<ZoneLocation>> GetByWarehouseID(int warehouseID)
        {
            var zoneLocations = await _context.Zones
                                        .Where(p => p.WarehouseID == warehouseID && !p.IsDeleted)
                                        .Join(_context.ZoneLocations,
                                            z => z.ZoneCode,
                                            zl => zl.ZoneCode,
                                            (z, zl) => new { Zone = z, ZoneLocation = zl })
                                        .Select(p => new
                                        {
                                            LocationCode = p.ZoneLocation.LocationCode,
                                            ZoneCode = p.ZoneLocation.ZoneCode,
                                            LineCode = p.ZoneLocation.LineCode,
                                            LocationName = p.ZoneLocation.LocationName,
                                            LengthID = p.ZoneLocation.LengthID,
                                            LevelID = p.ZoneLocation.LevelID,
                                            Description = p.ZoneLocation.Description,
                                            CurrentPalletCode = p.ZoneLocation.CurrentPalletCode,
                                            Status = p.ZoneLocation.Status,
                                            IsDeleted = p.Zone.IsDeleted,
                                            CreatedBy = p.Zone.CreatedBy,
                                            CreatedBySitemapID = p.Zone.CreatedBySitemapID,
                                            CreatedDateTime = p.Zone.CreatedDateTime,
                                            UpdatedBy = p.Zone.CreatedBy,
                                            UpdatedBySitemapID = p.Zone.UpdatedBySitemapID,
                                            UpdatedDateTime = p.Zone.UpdatedDateTime,
                                            RowVersion = p.Zone.RowVersion
                                        }).ToListAsync();

            return zoneLocations.Select(p => new ZoneLocation(p)).ToList();
        }
    }
}
