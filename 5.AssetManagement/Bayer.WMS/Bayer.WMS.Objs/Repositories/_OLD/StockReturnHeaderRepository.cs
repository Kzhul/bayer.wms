﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IStockReturnHeaderRepository : IBaseRepository, IBaseRepository<StockReturnHeader>
    {
        Task<string> GetNextNBR();
        Task<IList<StockReturnHeader>> GetIncludeCreatedByAsync(Expression<Func<StockReturnHeader, bool>> predicate = null);
    }

    public class StockReturnHeaderRepository : BaseRepository<BayerWMSContext, StockReturnHeader>, IStockReturnHeaderRepository
    {
        public StockReturnHeaderRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.StockReturnHeaders.Where(a=>a.CreatedDateTime >= DateTime.Today).OrderByDescending(a => a.StockReturnCode).FirstOrDefaultAsync();
            if (model != null)
            {
                lastestNumber = model.StockReturnCode;
                string a = lastestNumber.Substring(lastestNumber.Length - 2);
                int NextDONumber = Convert.ToInt32(a) + 1;
                lastestNumber = "MR" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }
            else
            {
                int NextDONumber = 1;
                lastestNumber = "MR" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }

            return lastestNumber;
        }

        public virtual async Task<IList<StockReturnHeader>> GetIncludeCreatedByAsync(Expression<Func<StockReturnHeader, bool>> predicate = null)
        {
            var headers = await _context.StockReturnHeaders
                                .Where(predicate)
                                .Join(_context.Users,
                                    srh => srh.CreatedBy,
                                    u => u.UserID,
                                    (srh, u) => new { StockReturnHeader = srh, User = u })
                                .Select(p => new
                                {
                                    StockReturnCode = p.StockReturnHeader.StockReturnCode,
                                    Date = p.StockReturnHeader.Date,
                                    Description = p.StockReturnHeader.Description,
                                    Status = p.StockReturnHeader.Status,
                                    CreatedByName = p.User.FirstName + " " + p.User.LastName,
                                    CreatedBy = p.StockReturnHeader.CreatedBy,
                                    CreatedDateTime = p.StockReturnHeader.CreatedDateTime,
                                    CreatedBySitemapID = p.StockReturnHeader.CreatedBySitemapID,
                                    UpdatedBy = p.StockReturnHeader.CreatedBy,
                                    UpdatedDateTime = p.StockReturnHeader.UpdatedDateTime,
                                    UpdatedBySitemapID = p.StockReturnHeader.UpdatedBySitemapID,
                                    RowVersion = p.StockReturnHeader.RowVersion,
                                })
                                .ToListAsync();

            return headers.Select(p => new StockReturnHeader(p)).ToList();
        }
    }
}
