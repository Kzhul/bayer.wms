﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IRoleRepository : IBaseRepository, IBaseRepository<Role>
    {
        
    }

    public class RoleRepository : BaseRepository<BayerWMSContext, Role>, IRoleRepository
    {
        public RoleRepository(IBayerWMSContext context) 
            : base(context)
        {
        }
    }
}
