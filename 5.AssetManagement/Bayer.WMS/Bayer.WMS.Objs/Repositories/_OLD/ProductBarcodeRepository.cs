﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IProductBarcodeRepository : IBaseRepository, IBaseRepository<ProductBarcode>
    {
       
    }

    public class ProductBarcodeRepository : BaseRepository<BayerWMSContext, ProductBarcode>, IProductBarcodeRepository
    {
        public ProductBarcodeRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }
    }
}
