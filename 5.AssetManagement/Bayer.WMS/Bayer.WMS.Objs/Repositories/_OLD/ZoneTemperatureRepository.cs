﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IZoneTemperatureRepository : IBaseRepository, IBaseRepository<ZoneTemperature>
    {
        Task<IList<ZoneTemperature>> GetWithAllTemperaturAsync(Expression<Func<ZoneTemperature, bool>> predicate = null);
    }

    public class ZoneTemperatureRepository : BaseRepository<BayerWMSContext, ZoneTemperature>, IZoneTemperatureRepository
    {
        public ZoneTemperatureRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<ZoneTemperature>> GetWithAllTemperaturAsync(Expression<Func<ZoneTemperature, bool>> predicate = null)
        {
            var zoneTemperatures = await _context.References.Where(p => !p.IsDeleted && p.GroupCode == "ZoneTemperature")
                                        .GroupJoin(_context.ZoneTemperatures.Where(predicate),
                                            r => new { r.GroupCode, r.RefValue },
                                            zt => new { zt.GroupCode, zt.RefValue },
                                            (r, zt) => new { Reference = r, ZoneTemperature = zt.FirstOrDefault() })
                                        .Select(p => new
                                        {
                                            ZoneCode = p.ZoneTemperature != null ? p.ZoneTemperature.ZoneCode : String.Empty,
                                            GroupCode = p.Reference.GroupCode,
                                            RefValue = p.Reference.RefValue,
                                            ReferenceDescription = p.Reference.Description,
                                            Checked = p.ZoneTemperature != null,
                                            CreatedBy = p.ZoneTemperature != null ? p.ZoneTemperature.CreatedBy : null,
                                            CreatedBySitemapID = p.ZoneTemperature != null ? p.ZoneTemperature.CreatedBySitemapID : null,
                                            CreatedDateTime = p.ZoneTemperature != null ? p.ZoneTemperature.CreatedDateTime : null,
                                            UpdatedBy = p.ZoneTemperature != null ? p.ZoneTemperature.UpdatedBy : null,
                                            UpdatedBySitemapID = p.ZoneTemperature != null ? p.ZoneTemperature.UpdatedBySitemapID : null,
                                            UpdatedDateTime = p.ZoneTemperature != null ? p.ZoneTemperature.UpdatedDateTime : null,
                                            RowVersion = p.ZoneTemperature != null ? p.ZoneTemperature.RowVersion : null,
                                        }).ToListAsync();

            return zoneTemperatures.Select(p => new ZoneTemperature(p)).ToList();
        }
    }
}
