﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IAppSettingRepository : IBaseRepository, IBaseRepository<AppSetting>
    {
    }

    public class AppSettingRepository : BaseRepository<BayerWMSContext, AppSetting>, IAppSettingRepository
    {
        public AppSettingRepository(IBayerWMSContext context)
            : base(context)
        {

        }
    }
}
