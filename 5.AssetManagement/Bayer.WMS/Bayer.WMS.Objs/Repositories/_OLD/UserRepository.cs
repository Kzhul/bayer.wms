﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IUserRepository : IBaseRepository, IBaseRepository<User>
    {
       
    }

    public class UserRepository : BaseRepository<BayerWMSContext, User>, IUserRepository
    {
        public UserRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }
    }
}
