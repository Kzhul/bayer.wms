﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IRoleSiteMapRepository : IBaseRepository, IBaseRepository<RoleSitemap>
    {
        Task<IList<RoleSitemap>> GetWithAllSitemapsAsync(Expression<Func<RoleSitemap, bool>> predicate = null);
    }

    public class RoleSiteMapRepository : BaseRepository<BayerWMSContext, RoleSitemap>, IRoleSiteMapRepository
    {
        public RoleSiteMapRepository(IBayerWMSContext context) 
            : base(context)
        {
        }

        public virtual async Task<IList<RoleSitemap>> GetWithAllSitemapsAsync(Expression<Func<RoleSitemap, bool>> predicate = null)
        {
            var roleSitemaps = await _context.Sitemaps
                                        .GroupJoin(_context.RoleSitemaps.Where(predicate),
                                                s => s.SitemapID,
                                                rs => rs.SitemapID,
                                                (s, rs) => new { Sitemap = s, RoleSitemap = rs.FirstOrDefault() })
                                        .Select(p => new
                                        {
                                            RoleID = p.RoleSitemap == null ? 0 : p.RoleSitemap.RoleID,
                                            SitemapID = p.Sitemap.SitemapID,
                                            SitemapDescription = p.Sitemap.Description,
                                            SitemapParentID = p.Sitemap.ParentID,
                                            SitemapOrder = p.Sitemap.Order,
                                            AccessRights = p.RoleSitemap.AccessRights,
                                            CreatedBy = p.RoleSitemap.CreatedBy,
                                            CreatedBySitemapID = p.RoleSitemap.CreatedBySitemapID,
                                            CreatedDateTime = p.RoleSitemap.CreatedDateTime,
                                            UpdatedBy = p.RoleSitemap.UpdatedBy,
                                            UpdatedBySitemapID = p.RoleSitemap.UpdatedBySitemapID,
                                            UpdatedDateTime = p.RoleSitemap.UpdatedDateTime,
                                            RowVersion = p.RoleSitemap.RowVersion
                                        }).ToListAsync();

            return roleSitemaps.Select(p => new RoleSitemap(p)).ToList();
        }
    }
}
