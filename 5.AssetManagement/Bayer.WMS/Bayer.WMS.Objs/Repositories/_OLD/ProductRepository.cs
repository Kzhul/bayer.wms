﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IProductRepository : IBaseRepository, IBaseRepository<Product>
    {
        Task<IList<Product>> GetIncludeCategoryAsync(Expression<Func<Product, bool>> predicate = null);
    }

    public class ProductRepository : BaseRepository<BayerWMSContext, Product>, IProductRepository
    {
        public ProductRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<Product>> GetIncludeCategoryAsync(Expression<Func<Product, bool>> predicate = null)
        {
            var products = await _context.Products
                                        .Where(predicate)
                                        .GroupJoin(_context.Categories,
                                            p => p.CategoryID,
                                            c => c.CategoryID,
                                            (p, c) => new { Product = p, Category = c.FirstOrDefault() })
                                        .Select(p => new
                                        {
                                            ProductID = p.Product.ProductID,
                                            ProductCode = p.Product.ProductCode,
                                            Description = p.Product.Description,
                                            CategoryID = p.Category == null ? null : (int?)p.Category.CategoryID,
                                            CategoryDescription = p.Category == null ? null : p.Category.Description,
                                            Type = p.Product.Type,
                                            UOM = p.Product.UOM,
                                            PackingType = p.Product.PackingType,
                                            SampleQty = p.Product.SampleQty,
                                            PrintLabelPercentage = p.Product.PrintLabelPercentage,
                                            Status = p.Product.Status,
                                            IsDeleted = p.Product.IsDeleted,
                                            CreatedBy = p.Product.CreatedBy,
                                            CreatedBySitemapID = p.Product.CreatedBySitemapID,
                                            CreatedDateTime = p.Product.CreatedDateTime,
                                            UpdatedBy = p.Product.CreatedBy,
                                            UpdatedBySitemapID = p.Product.UpdatedBySitemapID,
                                            UpdatedDateTime = p.Product.UpdatedDateTime,
                                            RowVersion = p.Product.RowVersion,
                                            PalletSize = p.Product.PalletSize,
                                            QuantityByUnit = p.Product.QuantityByUnit,
                                            QuantityByCarton = p.Product.QuantityByCarton

                                        }).ToListAsync();

            return products.Select(p => new Product(p)).ToList();
        }
    }
}
