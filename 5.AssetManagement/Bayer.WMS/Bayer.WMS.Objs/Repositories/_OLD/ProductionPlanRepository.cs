﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IProductionPlanRepository : IBaseRepository, IBaseRepository<ProductionPlan>
    {
        Task<IList<ProductionPlan>> GetIncludeProductAsync(Expression<Func<ProductionPlan, bool>> predicate = null);
    }

    public class ProductionPlanRepository : BaseRepository<BayerWMSContext, ProductionPlan>, IProductionPlanRepository
    {
        public ProductionPlanRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<ProductionPlan>> GetIncludeProductAsync(Expression<Func<ProductionPlan, bool>> predicate = null)
        {
            var productionPlans = await _context.ProductionPlans
                                                .Where(predicate)
                                                .OrderBy(p => p.ProductLot)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { ProductionPlan = pp, Product = p })
                                                .Select(p => new
                                                {
                                                    ProductLot = p.ProductionPlan.ProductLot,
                                                    SupplierLot = p.ProductionPlan.SupplierLot,
                                                    Device = p.ProductionPlan.Device,
                                                    PONumber = p.ProductionPlan.PONumber,
                                                    ProductID = p.ProductionPlan.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,
                                                    ProductPackingType = p.Product.PackingType,
                                                    Quantity = p.ProductionPlan.Quantity,
                                                    PlanDate = p.ProductionPlan.PlanDate,
                                                    ManufacturingDate = p.ProductionPlan.ManufacturingDate,
                                                    ExpiryDate = p.ProductionPlan.ExpiryDate,
                                                    PackageQuantity = p.ProductionPlan.PackageQuantity,
                                                    PackageSize = p.ProductionPlan.PackageSize,
                                                    UOM = p.ProductionPlan.UOM,
                                                    Sequence = p.ProductionPlan.Sequence,
                                                    Note = p.ProductionPlan.Note,
                                                    IsGenProductBarcode = p.ProductionPlan.IsGenProductBarcode,
                                                    IsGenCartonBarcode = p.ProductionPlan.IsGenCartonBarcode,
                                                    CurrentLabelCartonBarcode = p.ProductionPlan.CurrentLabelCartonBarcode,
                                                    MaxLabelCartonBarcode = p.ProductionPlan.MaxLabelCartonBarcode,
                                                    PackagingStartTime = p.ProductionPlan.PackagingStartTime,
                                                    PackagingEndTime = p.ProductionPlan.PackagingEndTime,
                                                    Status = p.ProductionPlan.Status,
                                                    CreatedBy = p.ProductionPlan.CreatedBy,
                                                    CreatedDateTime = p.ProductionPlan.CreatedDateTime,
                                                    UpdatedBy = p.ProductionPlan.UpdatedBy,
                                                    UpdatedDateTime = p.ProductionPlan.UpdatedDateTime,
                                                    RowVersion = p.ProductionPlan.RowVersion
                                                }).ToListAsync();

            return productionPlans.Select(p => new ProductionPlan(p)).OrderBy(p => p.ProductLot).ToList();
        }
    }
}
