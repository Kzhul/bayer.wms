﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IStockReceivingHeaderRepository : IBaseRepository, IBaseRepository<StockReceivingHeader>
    {
        Task<string> GetNextNBR();
        Task<IList<StockReceivingHeader>> GetIncludeCreatedByAsync(Expression<Func<StockReceivingHeader, bool>> predicate = null);
    }

    public class StockReceivingHeaderRepository : BaseRepository<BayerWMSContext, StockReceivingHeader>, IStockReceivingHeaderRepository
    {
        public StockReceivingHeaderRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.StockReceivingHeaders.Where(a=>a.CreatedDateTime >= DateTime.Today).OrderByDescending(a => a.StockReceivingCode).FirstOrDefaultAsync();
            if (model != null)
            {
                lastestNumber = model.StockReceivingCode;
                string a = lastestNumber.Substring(lastestNumber.Length - 2);
                int NextDONumber = Convert.ToInt32(a) + 1;
                lastestNumber = "SR" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }
            else
            {
                int NextDONumber = 1;
                lastestNumber = "SR" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }

            return lastestNumber;
        }

        public virtual async Task<IList<StockReceivingHeader>> GetIncludeCreatedByAsync(Expression<Func<StockReceivingHeader, bool>> predicate = null)
        {
            var headers = await _context.StockReceivingHeaders
                                .Where(predicate)
                                .Join(_context.Users,
                                    srh => srh.CreatedBy,
                                    u => u.UserID,
                                    (srh, u) => new { StockReceivingHeader = srh, User = u })
                                .Select(p => new
                                {
                                    StockReceivingCode = p.StockReceivingHeader.StockReceivingCode,
                                    DeliveryDate = p.StockReceivingHeader.DeliveryDate,
                                    CompanyName = p.StockReceivingHeader.CompanyName,
                                    TruckNo = p.StockReceivingHeader.TruckNo,
                                    Driver = p.StockReceivingHeader.Driver,
                                    Description = p.StockReceivingHeader.Description,
                                    MaterialType = p.StockReceivingHeader.MaterialType,
                                    VerifyStatus = p.StockReceivingHeader.VerifyStatus,
                                    ImportStatus = p.StockReceivingHeader.ImportStatus,
                                    CreatedByName = p.User.FirstName + " " + p.User.LastName,
                                    CreatedBy = p.StockReceivingHeader.CreatedBy,
                                    CreatedDateTime = p.StockReceivingHeader.CreatedDateTime,
                                    CreatedBySitemapID = p.StockReceivingHeader.CreatedBySitemapID,
                                    UpdatedBy = p.StockReceivingHeader.CreatedBy,
                                    UpdatedDateTime = p.StockReceivingHeader.UpdatedDateTime,
                                    UpdatedBySitemapID = p.StockReceivingHeader.UpdatedBySitemapID,
                                    RowVersion = p.StockReceivingHeader.RowVersion,
                                })
                                .ToListAsync();

            return headers.Select(p => new StockReceivingHeader(p)).ToList();
        }
    }
}
