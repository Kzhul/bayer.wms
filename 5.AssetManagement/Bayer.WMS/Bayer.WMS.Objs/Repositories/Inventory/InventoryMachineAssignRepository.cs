﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IInventoryMachineAssignRepository : IBaseRepository, IBaseRepository<InventoryMachineAssign>
    {
    }

    public class InventoryMachineAssignRepository : BaseRepository<BayerWMSContext, InventoryMachineAssign>, IInventoryMachineAssignRepository
    {
        public InventoryMachineAssignRepository(IBayerWMSContext context)
            : base(context)
        {

        }
    }
}
