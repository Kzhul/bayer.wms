﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace Bayer.WMS.Objs
{
    public static class Utility
    {
        public static Regex EmployeeRegex = new Regex(@"NV_.*$", RegexOptions.IgnoreCase);
        public static Regex ProductLotRegex = new Regex(@"\d{6}[FVABR]\d{2}$", RegexOptions.IgnoreCase);
        public static Regex PalletRegex = new Regex(@"P[A-Z][01]\d\d{2}\d{5}$", RegexOptions.IgnoreCase);
        public static Regex CartonRegex = new Regex(@"\d{6}\w\d{2}(C|c)\w{3}$", RegexOptions.IgnoreCase);
        public static Regex Carton2Regex = new Regex(@"SH\d{8}C\d{3}$", RegexOptions.IgnoreCase);
        public static Regex ProductRegex = new Regex(@"\d{6}\w\d{2}\d{5}", RegexOptions.IgnoreCase);
        public static Regex Product2Regex = new Regex(@"\d{10}", RegexOptions.IgnoreCase);

        public static string AppPath;
        public static string ConnStr;
        public static int CurrentSitemapID;
        public static string CurrentMethod;
        public static byte[] bytes = Encoding.ASCII.GetBytes("BayerWMS");
        public static IList<Sitemap> Sitemaps;
        public static CultureInfo info;
        public static bool debugMode = false;

        public enum MessageType
        {
            Warning,
            Error,
            Information
        }

        public static string MD5Encrypt(string planString)
        {
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] hashedBytes;
            UTF8Encoding encoder = new UTF8Encoding();
            hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(planString));

            string result = String.Empty;
            foreach (Byte b in hashedBytes)
                result += b.ToString("x2");
            return result;
        }

        public static string AESEncrypt(string planString)
        {
            if (String.IsNullOrEmpty(planString))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);

            var writer = new StreamWriter(cryptoStream);
            writer.Write(planString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();

            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        public static string AESDecrypt(string cipherText)
        {
            try
            {
                if (String.IsNullOrEmpty(cipherText))
                    return String.Empty;

                var cryptoProvider = new DESCryptoServiceProvider();
                var memoryStream = new MemoryStream(Convert.FromBase64String(cipherText));
                var cryptoStream = new CryptoStream(memoryStream,
                    cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
                var reader = new StreamReader(cryptoStream);

                return reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                return cipherText;
                //Do Nothing here
            }
            return string.Empty;
        }

        public static string DescryptConnString(string cryptedConnStr)
        {
            try
            {
                string[] connStrComponent = cryptedConnStr.Split(';');

                string dataSource = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("data source")).Split('=')[1].Trim();
                string initialCatalog = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("initial catalog")).Split('=')[1].Trim();
                string[] splitUserID = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("user id")).Split('=');
                string[] splitPwd = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("pwd")).Split('=');

                string connStr = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Pwd={3};MultipleActiveResultSets=True;Persist Security Info=True;",
                    dataSource, initialCatalog, Utility.AESDecrypt(splitUserID[1].Trim() + (splitUserID.Length == 3 ? "=" : splitUserID.Length == 4 ? "==" : String.Empty)), Utility.AESDecrypt(splitPwd[1].Trim() + (splitPwd.Length == 3 ? "=" : splitPwd.Length == 4 ? "==" : String.Empty)));

                return connStr;
            }
            catch (Exception ex)
            {
                //Do Nothing here
            }
            return string.Empty;
        }

        public static DateTime GetFirstDayOfMonth(this DateTime date)
        {
            DateTime temp = new DateTime(date.Year, date.Month, 1);
            return temp;
        }

        public static DateTime GetLastDayOfMonth(this DateTime date)
        {
            DateTime temp = new DateTime(date.Year, date.Month, 1);
            return temp.AddMonths(1).AddDays(-1);
        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static T ToEntity<T>(this DataRow row)
        {
            Type type = typeof(T);
            var entity = Activator.CreateInstance<T>();
            if (row != null)
            {
                int number = row.Table.Columns.Count;
                for (int i = 0; i < number; i++)
                {
                    if (row[i] != DBNull.Value)
                    {
                        PropertyInfo pi = type.GetProperty(row.Table.Columns[i].ColumnName);
                        if (pi != null && pi.GetSetMethod() != null)
                        {
                            pi.SetValue(entity, row[i], null);
                        }
                    }
                }
            }
            return entity;
        }

        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        public static string ToXML<T>(this T entity)
        {
            using (var sw = new StringWriter())
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(sw, entity);
                return sw.ToString();
            }
        }

        public static string DecimalToString(decimal? input, int formatOdd)
        {
            if (input == null)
            {
                return string.Empty;
            }
            else
            {
                return input.Value.ToString("N" + formatOdd.ToString());
            }
        }

        public static string DecimalToString(decimal? input)
        {
            if (input == null)
            {
                return string.Empty;
            }
            else
            {
                return input.Value.ToString("N0");
            }
        }

        public static string DecimalToEmptyString(decimal? input)
        {
            if (input == null || input == 0)
            {
                return string.Empty;
            }
            else
            {
                return input.Value.ToString("N0");
            }
        }

        public static string DecimalToEmptyString(decimal input)
        {
            if (input == 0)
            {
                return string.Empty;
            }
            else
            {
                return input.ToString("N0");
            }
        }

        public static string DecimalToEmptyString(int input)
        {
            if (input == 0)
            {
                return string.Empty;
            }
            else
            {
                return input.ToString("N0");
            }
        }

        //public static string StringParse(string input)
        //{
        //    if (input == null)
        //    {
        //        return string.Empty;
        //    }
        //    return input;
        //}

        public static string CleanFileName(string fileName)
        {
            fileName = Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
            fileName = fileName.Replace(" ", "_");
            return fileName;
        }

        //public static decimal DecimalParse(object o)
        //{
        //    if (o == null)
        //    {
        //        return 0;
        //    }

        //    decimal de = 0M;
        //    if (decimal.TryParse(o.ToString(), NumberStyles.Any, info, out de))
        //    {
        //    }
        //    return de;
        //}

        public static Color StatusToColor(int status)
        {
            if (status == 0)
            {
                return Color.Red;
            }
            else if (status == 1)
            {
                return Color.LightYellow;
            }
            else if (status == 2)
            {
                return Color.YellowGreen;
            }
            else if (status == 3)
            {
                return Color.Yellow;
            }
            else if (status == 4)
            {
                return Color.Green;
            }

            return Color.Gray;
        }

        public static Color StatusToColor(string status)
        {
            if (status == "N")
            {
                return Color.Red;
            }
            else if (status == "I")
            {
                return Color.Yellow;
            }
            else if (status == "C")
            {
                return Color.Green;
            }

            return Color.Gray;
        }

        public static string ProcessProductBarcode(string barcode)
        {
            barcode = Utility.ProductRegex.Match(barcode).Captures[0].Value;
            //barcode = Utility.AESDecrypt(barcode.Remove(0, 2));
            barcode = Utility.AESDecrypt(barcode);

            return barcode;
        }

        public static void Empty(this System.IO.DirectoryInfo directory)
        {
            foreach (System.IO.FileInfo file in directory.GetFiles()) file.Delete();
            foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
        }

        public static T CloneObject<T>(this object source)
        {
            T result = Activator.CreateInstance<T>();

            //// **** made things

            return result;
        }

        public static void dgGrid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            Font f = new Font("Microsoft Sans Serif", 9.75F);
            e.Graphics.DrawString(rowIdx, f, SystemBrushes.ControlText, headerBounds, centerFormat);
        }

        public static void CreateDirectory(string path)
        {
            try
            {
                // If the directory doesn't exist, create it.
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception)
            {
                // Fail silently
            }
        }
        public static void RemoveDirectory(string path)
        {
            try
            {
                // If the directory doesn't exist, create it.
                if (!Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }
            }
            catch (Exception)
            {
                // Fail silently
            }
        }

        public static void LogEx(Exception ex, MethodBase method)
        {
            using (var sw = new StreamWriter("ErrorLog.txt", true))
            {
                sw.WriteLine("=======================================================================================================");
                sw.WriteLine($"Method: {method.ReflectedType.Name}");
                sw.WriteLine($"Date: {DateTime.Now:dd.MM.yyyy hh:mm:ss}");

                sw.WriteLine("================Exception===================");
                sw.WriteLine(ex.Message);
                sw.WriteLine(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    sw.WriteLine("================InnerException===================");
                    sw.WriteLine(ex.InnerException.Message);
                    sw.WriteLine(ex.InnerException.StackTrace);

                    if (ex.InnerException.InnerException != null)
                    {
                        sw.WriteLine("================InnerException.InnerException===================");
                        sw.WriteLine(ex.InnerException.InnerException.Message);
                        sw.WriteLine(ex.InnerException.InnerException.StackTrace);

                        if (ex.InnerException.InnerException.InnerException != null)
                        {
                            sw.WriteLine("================InnerException.InnerException.InnerException===================");
                            sw.WriteLine(ex.InnerException.InnerException.InnerException.Message);
                            sw.WriteLine(ex.InnerException.InnerException.InnerException.StackTrace);
                        }
                    }
                }
                sw.WriteLine();
                sw.WriteLine();
                sw.Flush();
            }
        }

        public static void LogEx(Exception ex)
        {
            using (var sw = new StreamWriter("ErrorLog.txt", true))
            {
                sw.WriteLine("=======================================================================================================");
                sw.WriteLine($"Date: {DateTime.Now:dd.MM.yyyy hh:mm:ss}");

                sw.WriteLine("================Exception===================");
                sw.WriteLine(ex.Message);
                sw.WriteLine(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    sw.WriteLine("================InnerException===================");
                    sw.WriteLine(ex.InnerException.Message);
                    sw.WriteLine(ex.InnerException.StackTrace);

                    if (ex.InnerException.InnerException != null)
                    {
                        sw.WriteLine("================InnerException.InnerException===================");
                        sw.WriteLine(ex.InnerException.InnerException.Message);
                        sw.WriteLine(ex.InnerException.InnerException.StackTrace);

                        if (ex.InnerException.InnerException.InnerException != null)
                        {
                            sw.WriteLine("================InnerException.InnerException.InnerException===================");
                            sw.WriteLine(ex.InnerException.InnerException.InnerException.Message);
                            sw.WriteLine(ex.InnerException.InnerException.InnerException.StackTrace);
                        }
                    }
                }
                sw.WriteLine();
                sw.WriteLine();
                sw.Flush();
            }
        }

        public static void LogEx(string ex, string method)
        {
            using (var sw = new StreamWriter("ErrorLog.txt", true))
            {
                sw.WriteLine("=======================================================================================================");
                sw.WriteLine("Ex:" + ex);
                sw.WriteLine("Message:" + method);
                sw.WriteLine($"Date: {DateTime.Now:dd.MM.yyyy hh:mm:ss}");
                sw.WriteLine();
                sw.WriteLine();
                sw.Flush();
            }
        }


        #region Special Char

        public static string FormatSpecialChar(string Str)
        {
            Str = Str.Replace("\\", "\\\\");
            Str = Str.Replace("'", "\\'");
            Str = Str.Replace(@"""", "&quot;");
            return Str;
        }

        public static string FormatSQLSpecial(string Str)
        {
            if (!string.IsNullOrEmpty(Str))
            {
                Str = Str.Trim();
                Str = Str.Replace("'", "''");
                Str = Str.Replace("--", "");
            }
            return Str;
        }


        public static string DecodeVietnamese(string text)
        {
            string[] pattern = new string[7];
            pattern[0] = "a|(á|ả|à|ạ|ã|ă|ắ|ẳ|ằ|ặ|ẵ|â|ấ|ẩ|ầ|ậ|ẫ)";
            pattern[1] = "o|(ó|ỏ|ò|ọ|õ|ô|ố|ổ|ồ|ộ|ỗ|ơ|ớ|ở|ờ|ợ|ỡ)";
            pattern[2] = "e|(é|è|ẻ|ẹ|ẽ|ê|ế|ề|ể|ệ|ễ)";
            pattern[3] = "u|(ú|ù|ủ|ụ|ũ|ư|ứ|ừ|ử|ự|ữ)";
            pattern[4] = "i|(í|ì|ỉ|ị|ĩ)";
            pattern[5] = "y|(ý|ỳ|ỷ|ỵ|ỹ)";
            pattern[6] = "d|đ";
            for (int i = 0; i < pattern.Length; i++)
            {
                char replaceChar = pattern[i][0];
                System.Text.RegularExpressions.MatchCollection matchs = System.Text.RegularExpressions.Regex.Matches(text, pattern[i]);
                foreach (System.Text.RegularExpressions.Match m in matchs)
                {
                    text = text.Replace(m.Value[0], replaceChar);
                }
            }
            text = text.Replace(" ", "_");
            return text;
        }

        public static string UrlToString(string text)
        {
            text = text.Trim();
            text = text.Replace(":", "");
            text = text.Replace("/", "-");
            text = text.Replace("\\", "-");
            text = text.Replace("?", "");
            text = text.Replace(" ", "-");
            return text;
        }
        #endregion

        #region Convert

        #region Bool & BoolN Parse

        public static bool BoolParse(int i)
        {
            return (i == 1);
        }

        public static bool BoolParse(int? i)
        {
            return (i.HasValue && i == 1);
        }

        public static bool BoolParse(object o)
        {
            if (o == null)
                return false;

            if (o.ToString() == "1")
            {
                return true;
            }

            bool temp = false;
            if (bool.TryParse(o.ToString(), out temp))
                return temp;

            return false;
        }


        #endregion

        #region Int & IntN Parse

        public static int IntParse(bool b)
        {
            return b ? 1 : 0;
        }

        public static int IntParse(bool? b)
        {
            return (b.HasValue && b.Value) ? 1 : 0;
        }

        public static int IntParse(string str)
        {
            int i = 0;
            if (int.TryParse(str, out i))
            {
            }
            return i;
        }

        public static int IntParse(object o)
        {
            if (o == null)
            {
                return 0;
            }
            int i = 0;
            if (int.TryParse(o.ToString(), out i))
            {
            }
            return i;
        }

        public static int? IntNParse(string str)
        {
            int i = 0;
            if (int.TryParse(str, out i))
            {
                return i;
            }
            return null;
        }

        public static int? IntNParse(object o)
        {
            if (o == null)
            {
                return null;
            }
            int i = 0;
            if (int.TryParse(o.ToString(), out i))
            {
                return i;
            }
            return null;
        }

        #endregion

        #region Decimal & DecimalN Parse
        public static decimal DecimalParseFromHour(decimal hour, decimal minute)
        {
            decimal de = 0M;
            de = hour + minute / 60;
            return de;
        }

        public static decimal DecimalParse(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = str.Trim();
            }

            decimal de = 0M;
            if (decimal.TryParse(str, NumberStyles.Any, info, out de))
            {
            }
            return de;
        }

        public static decimal DecimalParseVN(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = str.Trim();
                str = str.Replace(",", "");
            }

            decimal de = 0M;
            if (decimal.TryParse(str, NumberStyles.Any, info, out de))
            {
            }
            return de;
        }

        public static decimal DecimalParse(object o)
        {
            if (o == null)
            {
                return 0;
            }

            decimal de = 0M;
            if (decimal.TryParse(o.ToString(), NumberStyles.Any, info, out de))
            {
            }
            return de;
        }

        public static decimal? DecimalNParse(string str)
        {
            decimal de = 0M;
            if (decimal.TryParse(str, NumberStyles.Any, info, out de))
            {
                return de;
            }
            return null;
        }

        public static decimal? DecimalNParse(object o)
        {
            if (o == null)
            {
                return null;
            }
            decimal de = 0;
            if (decimal.TryParse(o.ToString(), NumberStyles.Number, info, out de))
            {
                return de;
            }
            return null;
        }

        #endregion

        #region String & StringN Parse

        public static string StringParse(string input)
        {
            input = input ?? string.Empty;
            input = input == "null" ? string.Empty : input;
            input = input.Trim();
            string pattern = "\\s+";
            string replacement = " ";
            Regex rgx = new Regex(pattern);
            string result = rgx.Replace(input, replacement);
            return input;
        }

        public static string StringParse(decimal de)
        {
            return String.Format(info, "{0:N}", de);
        }

        public static string StringParse(decimal? de)
        {
            if (!de.HasValue)
            {
                return "0";
            }
            return StringParse(de.Value);
        }

        public static string StringParse(int[] ID)
        {
            if (ID == null)
            {
                return "0";
            }
            else
            {
                string result = String.Empty;
                for (int i = 0; i < ID.Length; i++)
                {
                    result = result + ID[i] + ",";
                }

                if (result.Length > 0)
                    return result.Remove(result.Length - 1);
                return "0";
            }
        }

        public static string StringParse(List<int> ID)
        {
            if (ID == null)
            {
                return "0";
            }
            else
            {
                string result = String.Empty;
                for (int i = 0; i < ID.Count; i++)
                {
                    result = result + ID[i] + ",";
                }

                if (result.Length > 0)
                    return result.Remove(result.Length - 1);
                return "0";
            }
        }

        public static string StringParseToSQLQuery(List<string> ID)
        {
            if (ID == null)
            {
                return "''";
            }
            else
            {
                string result = "'";
                for (int i = 0; i < ID.Count; i++)
                {
                    result = result + ID[i] + "','";
                }
                if (result.Length > 0)
                    return result.Remove(result.Length - 2);
                return "";
            }
        }

        public static string StringParse(List<string> ID)
        {
            if (ID == null)
            {
                return "";
            }
            else
            {
                string result = String.Empty;
                for (int i = 0; i < ID.Count; i++)
                {
                    result = result + ID[i] + ", ";
                }
                if (result.Length > 0)
                    return result.Remove(result.Length - 1);
                return "";
            }
        }

        public static string StringParse(byte[] b)
        {
            return BitConverter.ToString(b);
        }

        public static string StringParse(object o)
        {
            return o == null ? String.Empty : o.ToString();
        }

        public static string StringNParse(object o)
        {
            return o == null ? String.Empty : o.ToString();
        }

        public static string StringParse(bool? bo)
        {
            if (bo.HasValue)
            {
                if (bo.Value)
                    return "1";
            }

            return "0";
        }

        public static string StringNParse(bool? bo)
        {
            if (bo.HasValue)
            {
                if (bo.Value)
                    return "1";
            }

            return string.Empty;
        }

        public static string StringNParse(decimal? de)
        {
            if (!de.HasValue || de == 0M)
            {
                return String.Empty;
            }
            return StringParse(de.Value);
        }

        public static string StringParseWithDecimalDegit(decimal de)
        {
            //info.NumberFormat.NumberDecimalDigits = 2;
            //info.NumberFormat.CurrencyDecimalDigits = 2;
            string result = String.Format(info, "{0:C}", de);
            //info.NumberFormat.NumberDecimalDigits = 0;
            //info.NumberFormat.CurrencyDecimalDigits = 0;
            if (!string.IsNullOrEmpty(result))
            {
                result = result.Trim();
            }
            return result;
        }
        
        public static string StringParseWithDecimalDegit(decimal? de)
        {
            if (!de.HasValue)
            {
                return "0.00";
            }
            return StringParseWithDecimalDegit(de.Value);
        }

        public static string StringNParseWithDecimalDegit(decimal? de)
        {
            if (!de.HasValue || de == 0M)
            {
                return String.Empty;
            }
            return StringParseWithDecimalDegit(de.Value);
        }

        public static string StringParseWithAutoDecimalDegit(decimal? de)
        {
            char groupSer = char.Parse(Utility.info.NumberFormat.NumberGroupSeparator);
            if (!de.HasValue || de == 0M)
            {
                return "0";
            }
            //string[] str = de.Value.ToString().Split(groupSer);
            //str[1] = str[1].Replace("00", String.Empty);
            //if (str[1].EndsWith("0"))
            //{
            //    str[1] = str[1].Remove(str[1].Length - 1);
            //}
            info.NumberFormat.NumberDecimalDigits = 2;// str[1].Length;
            info.NumberFormat.CurrencyDecimalDigits = 2;// str[1].Length;

            string result = String.Format(info, "{0:C}", de);
            info.NumberFormat.NumberDecimalDigits = 0;
            info.NumberFormat.CurrencyDecimalDigits = 0;
            return result;
        }

        public static string StringNParseWithAutoDecimalDegit(decimal? de)
        {
            char groupSer = char.Parse(Utility.info.NumberFormat.NumberGroupSeparator);
            if (!de.HasValue || de == 0M)
            {
                return String.Empty;
            }
            string[] str = de.Value.ToString().Split(groupSer);
            str[1] = str[1].Replace("00", String.Empty);
            if (str[1].EndsWith("0"))
            {
                str[1] = str[1].Remove(str[1].Length - 1);
            }
            info.NumberFormat.NumberDecimalDigits = 2;// str[1].Length;
            info.NumberFormat.CurrencyDecimalDigits = 2;// str[1].Length;

            string result = String.Format(info, "{0:C}", de);
            info.NumberFormat.NumberDecimalDigits = 0;
            info.NumberFormat.CurrencyDecimalDigits = 0;
            return result;
        }

        public static string StringParseWithRoundingDecimalDegit(decimal de)
        {
            return String.Format(info, "{0:N}", de);
        }

        public static string StringParseWithRoundingDecimalDegit(decimal? de)
        {
            if (!de.HasValue)
            {
                return "0";
            }
            return StringParseWithRoundingDecimalDegit(de.Value);
        }

        public static string StringNParseWithRoundingDecimalDegit(decimal? de)
        {
            if (!de.HasValue || de == 0M)
            {
                return String.Empty;
            }
            return StringParseWithRoundingDecimalDegit(de.Value);
        }
        #endregion

        #region Byte Parse

        public static byte[] ByteParse(string hexStr)
        {
            int numberChars = hexStr.Length;
            byte[] b = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
                b[i / 2] = Convert.ToByte(hexStr.Substring(i, 2), 16);
            return b;
        }

        #endregion

        #region DateTime Parse

        public static DateTime DateTimeParse(object o)
        {
            if (o == null)
            {
                return DateTime.MinValue;
            }
            DateTime dt = new DateTime();
            if (DateTime.TryParse(o.ToString(), info, DateTimeStyles.None, out dt))
            {
                return dt;
            }
            return DateTime.MinValue;
        }

        public static DateTime? DateTimeNParse(object o)
        {
            if (o == null)
            {
                return null;
            }
            DateTime dt = new DateTime();
            if (DateTime.TryParse(o.ToString(), info, DateTimeStyles.None, out dt))
            {
                return dt;
            }
            return null;
        }

        public static void DateTimeSecondToHourAndMinute(decimal second, out decimal hour, out decimal minute)
        {
            try
            {
                TimeSpan t = TimeSpan.FromSeconds((double)second);
                hour = t.Hours;
                minute = t.Minutes;
            }
            catch
            {
                hour = 0;
                minute = 0;
            }
        }

        public static void DateTimeMinuteToHourAndMinute(decimal numberOfMinute, out decimal hour, out decimal minute)
        {
            try
            {
                TimeSpan t = TimeSpan.FromMinutes((double)numberOfMinute);
                hour = t.Hours;
                minute = t.Minutes;
            }
            catch
            {
                hour = 0;
                minute = 0;
            }
        }

        public static int DateTimeMinuteToHour(decimal minute)
        {
            try
            {
                TimeSpan t = TimeSpan.FromMinutes((double)minute);
                return t.Hours;
            }
            catch
            {
                return 0;
            }
        }

        public static decimal DateTimeMinuteToMinute(decimal minute)
        {
            try
            {
                TimeSpan t = TimeSpan.FromMinutes((double)minute);
                return t.Minutes;
            }
            catch
            {
                return 0;
            }
        }

        public static int DateTimeSecondToHour(decimal second)
        {
            try
            {
                TimeSpan t = TimeSpan.FromSeconds((double)second);
                return t.Hours;
            }
            catch
            {
                return 0;
            }
        }

        public static decimal DateTimeSecondToMinute(decimal second)
        {
            try
            {
                TimeSpan t = TimeSpan.FromSeconds((double)second);
                return t.Minutes;
            }
            catch
            {
                return 0;
            }
        }

        public static decimal DateTimeTimeToSecond(decimal hour, decimal minute)
        {
            decimal de = 0M;
            de = (hour * 3600) + (minute * 60);
            return de;
        }

        public static decimal DateTimeTimeToMinute(decimal hour, decimal minute)
        {
            decimal de = 0M;
            de = (hour * 60) + minute;
            return de;
        }

        #endregion

        #endregion

    }

    public static class LoginInfo
    {
        public static int UserID { get; set; }

        public static string Username { get; set; }

        public static string FirstName { get; set; }

        public static string LastName { get; set; }

        public static int? SitemapID { get; set; }
    }

    public class WrappedException : Exception
    {
        public WrappedException()
            : base()
        {

        }

        public WrappedException(string message)
            : base(message)
        {

        }

        public WrappedException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
