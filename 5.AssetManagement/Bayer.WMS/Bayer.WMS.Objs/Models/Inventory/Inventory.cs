﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class Inventory : BaseEntity
    {
        public Inventory()
        {
            Status = status.A;
            IsDeleted = false;
        }

        public Inventory(dynamic inventory)
        {
            _inventoryCode = inventory.InventoryCode;
            _inventoryName = inventory.InventoryName;
            _description = inventory.Description;
            _fromDate = inventory.FromDate;
            _toDate = inventory.ToDate;
            _status = inventory.Status;
            _assignOrNot = inventory.AssignOrNot;
            _isDeleted = inventory.IsDeleted;
            _createdBy = inventory.CreatedBy;
            _createdBySitemapID = inventory.CreatedBySitemapID;
            _createdDateTime = inventory.CreatedDateTime;
            _updatedBy = inventory.CreatedBy;
            _updatedBySitemapID = inventory.UpdatedBySitemapID;
            _updatedDateTime = inventory.UpdatedDateTime;
            _rowVersion = inventory.RowVersion;
        }

        private string _inventoryCode;

        [Key]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_InventoryCode)]
        public string InventoryCode
        {
            get { return _inventoryCode; }
            set
            {
                _inventoryCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("InventoryCode"));
            }
        }

        private string _inventoryName;

        [StringLength(255)]
        public string InventoryName
        {
            get { return _inventoryName; }
            set
            {
                _inventoryName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("InventoryName"));
            }
        }


        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private DateTime? _fromDate;        
        public DateTime? FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("FromDate"));
            }
        }

        [NotMapped]
        public string StrFromDate
        {
            get { return FromDate.HasValue ? FromDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {

            }
        }

        private DateTime? _toDate;
        public DateTime? ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ToDate"));
            }
        }

        [NotMapped]
        public string StrToDate
        {
            get { return ToDate.HasValue ? ToDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {

            }
        }

        private Boolean _assignOrNot;
        public Boolean AssignOrNot
        {
            get { return _assignOrNot; }
            set
            {
                _assignOrNot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("AssignOrNot"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string A = "A";
            public const string I = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = "A", Display = "Đang hiệu lực" },
                    new status { Value = "I", Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case A:
                        display = "Đang hiệu lực";
                        break;
                    case I:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }
    }

    public class InventoryConfiguration : EntityTypeConfiguration<Inventory>
    {
        public InventoryConfiguration()
        {
            Property(e => e.InventoryCode).IsUnicode(false);
            Property(e => e.Status).IsFixedLength();
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
