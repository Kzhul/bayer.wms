﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class InventoryCheck : BaseEntity
    {
        public InventoryCheck()
        {
            Status = status.A;
            IsDeleted = false;
        }

        public InventoryCheck(dynamic inventoryCheck)
        {
            _inventoryCode = inventoryCheck.InventoryCode;
            _assetCode = inventoryCheck.AssetCode;
            _checkDate = inventoryCheck.CheckDate;
            _checkBy = inventoryCheck.CheckBy;
            _status = inventoryCheck.Status;
            _isDeleted = inventoryCheck.IsDeleted;
            _createdBy = inventoryCheck.CreatedBy;
            _createdBySitemapID = inventoryCheck.CreatedBySitemapID;
            _createdDateTime = inventoryCheck.CreatedDateTime;
            _updatedBy = inventoryCheck.CreatedBy;
            _updatedBySitemapID = inventoryCheck.UpdatedBySitemapID;
            _updatedDateTime = inventoryCheck.UpdatedDateTime;
            _rowVersion = inventoryCheck.RowVersion;
        }

        private string _inventoryCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_InventoryCode)]
        public string InventoryCode
        {
            get { return _inventoryCode; }
            set
            {
                _inventoryCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("InventoryCode"));
            }
        }

        private string _assetCode;

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string AssetCode
        {
            get { return _assetCode; }
            set
            {
                _assetCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("AssetCode"));
            }
        }


        private string _checkDate;

        [StringLength(255)]
        public string CheckDate
        {
            get { return _checkDate; }
            set
            {
                _checkDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CheckDate"));
            }
        }

        private int? _checkBy;

        [Required(ErrorMessage = Messages.Validate_Required_Warehouse)]
        public int? CheckBy
        {
            get { return _checkBy; }
            set
            {
                _checkBy = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CheckBy"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string A = "A";
            public const string I = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = "A", Display = "Đang hiệu lực" },
                    new status { Value = "I", Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case A:
                        display = "Đang hiệu lực";
                        break;
                    case I:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }
    }

    public class InventoryCheckConfiguration : EntityTypeConfiguration<InventoryCheck>
    {
        public InventoryCheckConfiguration()
        {
            Property(e => e.InventoryCode).IsUnicode(false);
            Property(e => e.Status).IsFixedLength();
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
