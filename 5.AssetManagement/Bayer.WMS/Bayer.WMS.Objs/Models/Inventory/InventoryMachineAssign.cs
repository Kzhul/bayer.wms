﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class InventoryMachineAssign : BaseEntity
    {
        public InventoryMachineAssign()
        {
        }

        public InventoryMachineAssign(dynamic item)
        {
            _inventoryCode = item.InventoryCode;
            _machineCode = item.MachineCode;
            _createdBy = item.CreatedBy;
            _createdBySitemapID = item.CreatedBySitemapID;
            _createdDateTime = item.CreatedDateTime;
            _updatedBy = item.CreatedBy;
            _updatedBySitemapID = item.UpdatedBySitemapID;
            _updatedDateTime = item.UpdatedDateTime;
            _rowVersion = item.RowVersion;
        }

        private string _inventoryCode;

        [Key]
        [Column(Order = 0)]
        public string InventoryCode
        {
            get { return _inventoryCode; }
            set
            {
                _inventoryCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("InventoryCode"));
            }
        }

        private string _machineCode;

        [Key]
        [Column(Order = 1)]
        public string MachineCode
        {
            get { return _machineCode; }
            set
            {
                _machineCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("MachineCode"));
            }
        }

        private string _assetCode;
        [Key]
        [Column(Order = 2)]
        public string AssetCode
        {
            get { return _assetCode; }
            set
            {
                _assetCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("AssetCode"));
            }
        }
    }

    public class InventoryMachineAssignConfiguration : EntityTypeConfiguration<InventoryMachineAssign>
    {
        public InventoryMachineAssignConfiguration()
        {
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
