﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class Asset : BaseEntity
    {
        public Asset()
        {
            IsDeleted = false;
        }

        public Asset(dynamic asset)
        {
            _assetCode = asset.AssetCode;
            _assetName = asset.AssetName;
            _description = asset.Description;
            _costCenter = asset.CostCenter;
            _startUseDate = asset.StartUseDate;
            _lastCheckDate = asset.LastCheckDate;
            _lastCheckBy = asset.LastCheckBy;
            _status = asset.Status;
            _isDeleted = asset.IsDeleted;
            _createdBy = asset.CreatedBy;
            _createdBySitemapID = asset.CreatedBySitemapID;
            _createdDateTime = asset.CreatedDateTime;
            _updatedBy = asset.CreatedBy;
            _updatedBySitemapID = asset.UpdatedBySitemapID;
            _updatedDateTime = asset.UpdatedDateTime;
            _rowVersion = asset.RowVersion;
        }

        private string _assetCode;

        [Key]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_AssetCode)]
        public string AssetCode
        {
            get { return _assetCode; }
            set
            {
                _assetCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("AssetCode"));
            }
        }

        private string _assetName;

        [StringLength(255)]
        public string AssetName
        {
            get { return _assetName; }
            set
            {
                _assetName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("AssetName"));
            }
        }


        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _costCenter;
        public string CostCenter
        {
            get { return _costCenter; }
            set
            {
                _costCenter = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CostCenter"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay;

        private DateTime? _startUseDate;
        public DateTime? StartUseDate
        {
            get { return _startUseDate; }
            set
            {
                _startUseDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("StartUseDate"));
            }
        }

        private DateTime? _lastCheckDate;
        public DateTime? LastCheckDate
        {
            get { return _lastCheckDate; }
            set
            {
                _lastCheckDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LastCheckDate"));
            }
        }

        private int? _lastCheckBy;
        public int? LastCheckBy
        {
            get { return _lastCheckBy; }
            set
            {
                _lastCheckBy = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LastCheckBy"));
            }
        }


        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }
    }

    public class AssetConfiguration : EntityTypeConfiguration<Asset>
    {
        public AssetConfiguration()
        {
            Property(e => e.AssetCode).IsUnicode(false);
            Property(e => e.Status).IsFixedLength();
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
