namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class AppSetting
    {
        [Key]
        [StringLength(255)]
        public string SettingID { get; set; }

        [StringLength(255)]
        public string SettingValue { get; set; }
    }

    public class AppSettingConfiguration : EntityTypeConfiguration<AppSetting>
    {
        public AppSettingConfiguration()
        {
            Property(e => e.SettingID).IsUnicode(false);
            Property(e => e.SettingValue).IsUnicode(false);
        }
    }
}
