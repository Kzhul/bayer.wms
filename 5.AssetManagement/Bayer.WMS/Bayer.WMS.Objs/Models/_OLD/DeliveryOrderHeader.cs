﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class DeliveryOrderHeader : BaseEntity
    {
        public DeliveryOrderHeader()
        {
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string DOImportCode { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeliveryDate { get; set; }

        [NotMapped]
        public string StrDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {

            }
        }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public bool IsDeleted { get; set; }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string New = "N";
            public const string InProgress = "I";
            public const string Complete = "C";
            public const string Deleted = "D";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = InProgress, Display = "Đang xử lý" },
                    new status { Value = Complete, Display = "Hoàn thành" },
                    new status { Value = Complete, Display = "Đã xóa" },
                };
            }

            public static string GetDisplay(string Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case New:
                        display = "Mới";
                        break;
                    case InProgress:
                        display = "Đang xử lý";
                        break;
                    case Complete:
                        display = "Hoàn thành";
                        break;
                    case Deleted:
                        display = "Đã xóa";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class statusTicket
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const int Empty = 0;
            public const int Part = 1;
            public const int CreatedFull = 2;
            public const int Working = 3;
            public const int Finish = 4;

            public int Value { get; set; }

            public string Display { get; set; }

            public static List<statusTicket> Get()
            {
                return new List<statusTicket>
                {
                    new statusTicket { Value = Empty, Display = "Chưa tạo" },
                    new statusTicket { Value = Part, Display = "Đã tạo 1 phần" },
                    new statusTicket { Value = CreatedFull, Display = "Đã tạo hết" },
                    new statusTicket { Value = Working, Display = "Đang xuất hàng" },
                    new statusTicket { Value = Finish, Display = "Đã xuất xong" },
                };
            }

            public static string GetDisplay(int Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case Empty:
                        display = "Chưa tạo";
                        break;
                    case Part:
                        display = "Đã tạo 1 phần";
                        break;
                    case CreatedFull:
                        display = "Đã tạo hết";
                        break;
                    case Working:
                        display = "Đang xử lý";
                        break;
                    case Finish:
                        display = "Hoàn thành";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        public int IsExportFull { get; set; }
        [NotMapped]
        public string StatusExportDisplay
        {
            get { return statusTicket.GetDisplay(IsExportFull); }
        }


        public int IsSplitFull { get; set; }
        [NotMapped]
        public string StatusSplitDisplay
        {
            get { return statusTicket.GetDisplay(IsSplitFull); }
        }
        public int IsPrepareFull { get; set; }
        [NotMapped]
        public string StatusPrepareDisplay
        {
            get { return statusTicket.GetDisplay(IsPrepareFull); }
        }
        public int IsDeliveryFull { get; set; }
        [NotMapped]
        public string StatusDeliveryDisplay
        {
            get { return statusTicket.GetDisplay(IsDeliveryFull); }
        }
    }

    public class DeliveryOrderHeaderConfiguration : EntityTypeConfiguration<DeliveryOrderHeader>
    {
        public DeliveryOrderHeaderConfiguration()
        {
            ToTable("DeliveryOrderHeaders");
        }
    }
}
