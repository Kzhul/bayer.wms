﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class ZoneCategory : BaseEntity
    {
        public ZoneCategory()
        {
        }

        public ZoneCategory(dynamic zoneCategory)
        {
            _zoneCode = zoneCategory.ZoneCode;
            _categoryID = zoneCategory.CategoryID;
            _categoryDescription = zoneCategory.CategoryDescription;
            _checked = zoneCategory.Checked;
            _createdBy = zoneCategory.CreatedBy;
            _createdBySitemapID = zoneCategory.CreatedBySitemapID;
            _createdDateTime = zoneCategory.CreatedDateTime;
            _updatedBy = zoneCategory.CreatedBy;
            _updatedBySitemapID = zoneCategory.UpdatedBySitemapID;
            _updatedDateTime = zoneCategory.UpdatedDateTime;
            _rowVersion = zoneCategory.RowVersion;
        }

        private string _zoneCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string ZoneCode
        {
            get { return _zoneCode; }
            set
            {
                _zoneCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ZoneCode"));
            }
        }

        private int _categoryID;

        [Key]
        [Column(Order = 1)]
        [Required(ErrorMessage = Messages.Validate_Required_Category)]
        public int CategoryID
        {
            get { return _categoryID; }
            set
            {
                _categoryID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CategoryID"));
            }
        }

        private string _categoryDescription;

        [NotMapped]
        public string CategoryDescription
        {
            get { return _categoryDescription; }
            set { _categoryDescription = value; }
        }

        private bool _checked;

        [NotMapped]
        public bool Checked
        {
            get { return _checked; }
            set
            {
                _checked = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Checked"));
            }
        }
    }

    public class ZoneCategoryConfiguration : EntityTypeConfiguration<ZoneCategory>
    {
        public ZoneCategoryConfiguration()
        {
            Property(e => e.ZoneCode).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
