﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class Pallet : BaseEntity
    {
        private string _palletCode;

        [Key]
        [StringLength(255)]
        public string PalletCode
        {
            get { return _palletCode; }
            set
            {
                _palletCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletCode"));
            }
        }

        private string _type;

        [StringLength(2)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_ProductPackingType)]
        public string Type
        {
            get { return _type; }
            set
            {
                _type = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Type"));
            }
        }

        [NotMapped]
        public string TypeDisplay
        {
            get { return type.GetDisplay(Type); }
        }

        public class type
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Wood = "PW";
            public const string Plastic = "PP";
            public const string PlasticForFA = "PF";
            public const string Aluminum = "PA";
            public const string PlasticCabinet = "PC";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<type> Get()
            {
                return new List<type>
                {
                    new type { Value = Wood, Display = "Gỗ" },
                    new type { Value = Plastic, Display = "Nhựa phẳng" },
                    new type { Value = PlasticForFA, Display = "Nhựa lỗ" },
                    new type { Value = Aluminum, Display = "Nhôm" }
                };
            }

            public static List<type> GetWithAll()
            {
                return new List<type>
                {
                    new type { Value = String.Empty, Display = "Tất cả" },
                    new type { Value = Wood, Display = "Gỗ" },
                    new type { Value = Plastic, Display = "Nhựa phẳng" },
                    new type { Value = PlasticForFA, Display = "Nhựa lỗ" },
                    new type { Value = Aluminum, Display = "Nhôm" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Wood:
                        display = "Gỗ";
                        break;
                    case Plastic:
                        display = "Nhựa phẳng";
                        break;
                    case PlasticForFA:
                        display = "Nhựa lỗ";
                        break;
                    case Aluminum:
                        display = "Nhôm";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private DateTime? _startDate;

        [Column(TypeName = "date")]
        public DateTime? StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("StartDate"));
            }
        }

        private DateTime? _expiryDate;

        private int? _usageTime;

        public int? UsageTime
        {
            get { return _usageTime; }
            set
            {
                _usageTime = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UsageTime"));
            }
        }

        [Column(TypeName = "date")]
        public DateTime? ExpiryDate
        {
            get { return _expiryDate; }
            set
            {
                _expiryDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ExpiryDate"));
            }
        }

        private int? _sequence;

        public int? Sequence
        {
            get { return _sequence; }
            set
            {
                _sequence = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Sequence"));
            }
        }

        private string _companyCode;

        [StringLength(255)]
        public string CompanyCode
        {
            get { return _companyCode; }
            set
            {
                _companyCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CompanyCode"));
            }
        }

        private string _dOImportCode;

        [StringLength(255)]
        public string DOImportCode
        {
            get { return _dOImportCode; }
            set
            {
                _dOImportCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DOImportCode"));
            }
        }

        private string _referenceNbr;

        [StringLength(255)]
        public string ReferenceNbr
        {
            get { return _referenceNbr; }
            set
            {
                _referenceNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ReferenceNbr"));
            }
        }

        private string _capacityStatus;

        [StringLength(1)]
        public string CapacityStatus
        {
            get { return _capacityStatus; }
            set
            {
                _capacityStatus = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CapacityStatus"));
            }
        }

        public class capacityStatus
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Empty = "E";
            public const string Partial = "P";
            public const string Full = "F";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<capacityStatus> Get()
            {
                return new List<capacityStatus>
                {
                    new capacityStatus { Value = Empty, Display = "Trống" },
                    new capacityStatus { Value = Partial, Display = "Chưa đầy" },
                    new capacityStatus { Value = Full, Display = "Đầy" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Empty:
                        display = "Trống";
                        break;
                    case Partial:
                        display = "Chưa đầy";
                        break;
                    case Full:
                        display = "Đầy";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string New = "N";
            public const string Exported = "E";
            public const string Received = "R";
            public const string Splitted = "S";
            public const string Prepared = "P";
            public const string Booking = "B";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = Exported, Display = "Đã xuất hàng" },
                    new status { Value = Received, Display = "Đã nhận hàng" },
                    new status { Value = Splitted, Display = "Đã chia hàng" },
                    new status { Value = Prepared, Display = "Đã soạn hàng" },
                    new status { Value = Booking, Display = "Đang giữa hàng" },
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case New:
                        display = "Mới";
                        break;
                    case Exported:
                        display = "Đã xuất hàng";
                        break;
                    case Received:
                        display = "Đã nhận hàng";
                        break;
                    case Splitted:
                        display = "Đã chia hàng";
                        break;
                    case Prepared:
                        display = "Đã soạn hàng";
                        break;
                    case Booking:
                        display = "Đang giữa hàng";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        #region For Location in WareHouse
        //public string LocationCode { get; set; }
        //public string LocationSuggestion { get; set; }
        //public string BatchCode { get; set; }
        //public string BatchCodeDistributor { get; set; }
        //public string ImportStatus { get; set; }
        //public decimal? Weigh { get; set; }
        //public string CartonNumber { get; set; }
        //public int? WarehouseKeeper { get; set; }
        //[NotMapped]
        //public string StrWarehouseKeeper { get; set; }
        //public DateTime? WarehouseVerifyDate { get; set; }
        //[NotMapped]
        //public string StrWarehouseVerifyDate
        //{
        //    get { return WarehouseVerifyDate.HasValue ? WarehouseVerifyDate.Value.ToString("dd/MM/yyyy HH:mm") : DateTime.Today.ToString("dd/MM/yyyy"); }
        //    set
        //    {

        //    }
        //}

        //public int? Driver { get; set; }
        //public DateTime? DriverReceivedDate { get; set; }
        //[NotMapped]
        //public string StrDriverReceivedDate
        //{
        //    get { return DriverReceivedDate.HasValue ? DriverReceivedDate.Value.ToString("dd/MM/yyyy HH:mm") : DateTime.Today.ToString("dd/MM/yyyy"); }
        //    set
        //    {

        //    }
        //}
        //public DateTime? LocationPutDate { get; set; }
        //[NotMapped]
        //public string StrLocationPutDate
        //{
        //    get { return LocationPutDate.HasValue ? LocationPutDate.Value.ToString("dd/MM/yyyy HH:mm") : DateTime.Today.ToString("dd/MM/yyyy"); }
        //    set
        //    {

        //    }
        //}
        #endregion

    }

    public class PalletConfiguration : EntityTypeConfiguration<Pallet>
    {
        public PalletConfiguration()
        {
            Property(e => e.PalletCode).IsUnicode(false);
            Property(e => e.Type).IsFixedLength();
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
