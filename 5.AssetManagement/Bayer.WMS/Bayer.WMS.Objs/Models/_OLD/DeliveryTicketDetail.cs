namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class DeliveryTicketDetail : BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string DeliveryTicketCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string PrepareCode { get; set; }

        public string Status { get; set; }

        [NotMapped]
        public DateTime? PrepareDate { get; set; }
        [NotMapped]
        public string StrPrepareDate
        {
            get { return PrepareDate.HasValue ? PrepareDate.Value.ToString("dd/MM/yyyy") : string.Empty; }
            set
            {

            }
        }
        [NotMapped]
        public string DOImportCode { get; set; }
        [NotMapped]
        public string Description { get; set; }
        [NotMapped]
        public DateTime? DeliveryDate { get; set; }
        [NotMapped]
        public string StrDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy") : string.Empty; }
            set
            {

            }
        }
        [NotMapped]
        public string CompanyCode { get; set; }
        [NotMapped]
        public string CompanyName { get; set; }
        [NotMapped]
        public string Province { get; set; }

        public DeliveryTicketDetail()
        {
        }

        public DeliveryTicketDetail(dynamic p)
        {
            DeliveryTicketCode = p.DeliveryTicketCode;
            DOImportCode = p.DOImportCode;
            PrepareCode = p.PrepareCode;
            PrepareDate = p.PrepareDate;
            DeliveryDate = p.DeliveryDate;
            CompanyCode = p.CompanyCode;
            CompanyName = p.CompanyName;
            Province = p.Province;
            Description = p.Description;
            Status = p.Status;


            _createdBy = p.CreatedBy;
            CreatedBySitemapID = p.CreatedBySitemapID;
            _createdDateTime = p.CreatedDateTime;
            _updatedBy = p.UpdatedBy;
            UpdatedBySitemapID = p.UpdatedBySitemapID;
            _updatedDateTime = p.UpdatedDateTime;
            _rowVersion = p.RowVersion;           
        }
    }

    public class DeliveryTicketDetailConfiguration : EntityTypeConfiguration<DeliveryTicketDetail>
    {
        public DeliveryTicketDetailConfiguration()
        {
            ToTable("DeliveryTicketDetails");
        }
    }
}