﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class Warehouse : BaseEntity
    {
        public Warehouse()
        {
            Status = status.A;
            IsDeleted = false;
        }

        private int _warehouseID;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WarehouseID
        {
            get { return _warehouseID; }
            set
            {
                _warehouseID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("WarehouseID"));
            }
        }

        private string _warehouseCode;

        [Index("UK_Warehouses_WarehouseCode", IsUnique = true)]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_WarehouseCode)]
        public string WarehouseCode
        {
            get { return _warehouseCode; }
            set
            {
                _warehouseCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("WarehouseCode"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string A = "A";
            public const string I = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = "A", Display = "Đang hiệu lực" },
                    new status { Value = "I", Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case A:
                        display = "Đang hiệu lực";
                        break;
                    case I:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }
    }

    public class WarehouseConfiguration : EntityTypeConfiguration<Warehouse>
    {
        public WarehouseConfiguration()
        {
            Property(e => e.WarehouseCode).IsUnicode(false);
            Property(e => e.Status).IsFixedLength();
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
