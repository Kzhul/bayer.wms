namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class ProductionPlanImportLog : BaseEntity
    {
        public ProductionPlanImportLog()
        {

        }

        public ProductionPlanImportLog(int userID, string note, DateTime fromDate, DateTime toDate, string fileName)
        {
            _userID = userID;
            _note = note;
            _fromDate = fromDate;
            _toDate = toDate;
            _fileName = fileName;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        private int? _userID;

        public int? UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UserID"));
            }
        }

        private string _note;

        [StringLength(255)]
        public string Note
        {
            get { return _note; }
            set
            {
                _note = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Note"));
            }
        }

        private DateTime? _fromDate;

        [Column(TypeName = "date")]
        public DateTime? FromDate
        {
            get { return _fromDate; }
            set
            {
                _fromDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("FromDate"));
            }
        }

        private DateTime? _toDate;

        [Column(TypeName = "date")]
        public DateTime? ToDate
        {
            get { return _toDate; }
            set
            {
                _toDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ToDate"));
            }
        }

        private string _fileName;

        [StringLength(255)]
        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("FileName"));
            }
        }
    }

    public class ProductionPlanImportLogConfiguration : EntityTypeConfiguration<ProductionPlanImportLog>
    {
        public ProductionPlanImportLogConfiguration()
        {
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
