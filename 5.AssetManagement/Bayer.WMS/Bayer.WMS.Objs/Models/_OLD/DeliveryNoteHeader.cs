﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public class DeliveryNoteFull
    {
        public DeliveryNoteHeader header { get; set; }
        public List<DeliveryNoteDetail> listDetail { get; set; }
    }

    public partial class DeliveryNoteHeader : BaseEntity
    {
        [Key]
        [StringLength(30)]
        public string ExportCode { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ExportDate { get; set; }

        [StringLength(50)]
        public string DOImportCode { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeliveryDate { get; set; }

        [NotMapped]
        public string StrDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {

            }
        }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public bool IsDeleted { get; set; }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string New = "N";
            public const string InProgress = "I";
            public const string Complete = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = InProgress, Display = "Đang xử lý" },
                    new status { Value = Complete, Display = "Hoàn thành" },
                };
            }

            public static string GetDisplay(string Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case New:
                        display = "Mới";
                        break;
                    case InProgress:
                        display = "Đang xử lý";
                        break;
                    case Complete:
                        display = "Hoàn thành";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

       
    }

    public class DeliveryNoteHeaderConfiguration : EntityTypeConfiguration<DeliveryNoteHeader>
    {
        public DeliveryNoteHeaderConfiguration()
        {
            ToTable("DeliveryNoteHeaders");
        }
    }
}
