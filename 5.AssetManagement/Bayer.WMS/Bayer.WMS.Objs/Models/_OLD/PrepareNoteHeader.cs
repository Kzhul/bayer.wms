﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public class PrepareNoteFull
    {
        public PrepareNoteHeader header { get; set; }
        public List<PrepareNoteDetail> listDetail { get; set; }
    }

    public partial class PrepareNoteHeader : BaseEntity
    {
        [Key]
        [StringLength(30)]
        public string PrepareCode { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PrepareDate { get; set; }

        [StringLength(50)]
        public string DOImportCode { get; set; }

        [StringLength(30)]
        public string CompanyCode { get; set; }

        [StringLength(250)]
        public string CompanyName { get; set; }

        [StringLength(250)]
        public string Province { get; set; }

        public string Description { get; set; }

        public DateTime? DeliveryDate { get; set; }

        [NotMapped]
        public string StrDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {

            }
        }

        public bool IsDeleted { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string New = "N";
            public const string InProgress = "I";
            public const string Complete = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = InProgress, Display = "Đang xử lý" },
                    new status { Value = Complete, Display = "Hoàn thành" },
                };
            }

            public static string GetDisplay(string Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case New:
                        display = "Mới";
                        break;
                    case InProgress:
                        display = "Đang xử lý";
                        break;
                    case Complete:
                        display = "Hoàn thành";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        [StringLength(250)]
        public string UserCreateFullName { get; set; }
        [StringLength(250)]
        public string UserWareHouse { get; set; }
        [StringLength(250)]
        public string UserReview { get; set; }

        public PrepareNoteHeader()
        {

        }


        public PrepareNoteHeader(dynamic p)
        {
            PrepareCode = p.PrepareCode;
            PrepareDate = p.PrepareDate;
            DOImportCode = p.DOImportCode;
            CompanyCode = p.CompanyCode;
            CompanyName = p.CompanyName;
            Province = p.Province;
            Description = p.Description;
            DeliveryDate = p.DeliveryDate;
            IsDeleted = p.IsDeleted;
            Status = p.Status;
            UserCreateFullName = p.UserCreateFullName;
            UserWareHouse = p.UserWareHouse;
            UserReview = p.UserReview;
            _createdBy = p.CreatedBy;
            CreatedBySitemapID = p.CreatedBySitemapID;
            _createdDateTime = p.CreatedDateTime;
            _updatedBy = p.UpdatedBy;
            UpdatedBySitemapID = p.UpdatedBySitemapID;
            _updatedDateTime = p.UpdatedDateTime;
            //_rowVersion = p.RowVersion;
        }

        //public PrepareNoteHeader(dynamic p)
        //{
        //    CompanyCode = p.CompanyCode;
        //    CompanyName = p.CompanyName;
        //    Province = p.Province;
        //    DeliveryDate = p.DeliveryDate;
        //}
    }

    public class PrepareNoteHeaderConfiguration : EntityTypeConfiguration<PrepareNoteHeader>
    {
        public PrepareNoteHeaderConfiguration()
        {
            ToTable("PrepareNoteHeaders");
        }
    }

    public class PrepareForDeliveryNote
    {
        [NotMapped]
        public string CompanyCode { get; set; }

        [NotMapped]
        public string CompanyName { get; set; }

        [NotMapped]
        public string Province { get; set; }

        [NotMapped]
        public DateTime? DeliveryDate { get; set; }

        [NotMapped]
        public string StrDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {

            }
        }

        public PrepareForDeliveryNote()
        { 
        
        }

        public PrepareForDeliveryNote(dynamic p)
        {
            CompanyCode = p.CompanyCode;
            CompanyName = p.CompanyName;
            Province = p.Province;
            DeliveryDate = p.DeliveryDate;
        }
    }
}
