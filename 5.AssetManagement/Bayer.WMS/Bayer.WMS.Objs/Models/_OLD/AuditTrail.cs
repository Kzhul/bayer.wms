﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class AuditTrail
    {
        public AuditTrail()
        {
            UserID = LoginInfo.UserID;
            SitemapID = Utility.CurrentSitemapID;
            Method = Utility.CurrentMethod;
        }

        public AuditTrail(string tableName, string data, string action)
        {
            UserID = LoginInfo.UserID;
            SitemapID = Utility.CurrentSitemapID;
            TableName = tableName;
            Data = data;
            Action = action;
            Method = Utility.CurrentMethod;
            Date = DateTime.Today;
            DateTime = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int? UserID { get; set; }

        public int? SitemapID { get; set; }

        [StringLength(255)]
        public string TableName { get; set; }

        [Column(TypeName = "xml")]
        public string Data { get; set; }

        [StringLength(255)]
        public string Action { get; set; }

        public class action
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string Insert = "INS";
            public const string Update = "UPD";
            public const string Delete = "DEL";
            public const string Config = "COF";
            public const string Print = "PRT";
            public const string ScanEmployee = "SEM";
            public const string ScanProductLot = "SPL";
            public const string ScanCustomer = "SCM";
            public const string ScanPallet = "SP";
            public const string ScanCarton = "SCT";
            public const string ScanProduct = "SPD";
            public const string ScanEnd = "SED";
            public const string ScanDetroyLabel = "SDL";
            public const string ScanDetroyLabelEnd = "SDLE";
            public const string ScanDetroyLabelForceEnd = "SDLFE";
            public const string ScanTakeSample = "STS";
            public const string ScanTakeSampleEnd = "STSE";
            public const string ScanBarcodeInvalid = "SBI";
            public const string ScanExportNote = "SEN";
            public const string ScanSplitNote = "SSN";
            public const string ScanPrepareNote = "SPN";
            public const string ScanDeliveryNote = "SDN";
            public const string ScanDriverSign = "SDS";
            public const string PrintCartonLabel = "PCL";
            public const string PrintPalletLabel = "PPL";
            public const string ExportCartonBarcode = "ECB";
            public const string ExportProductBarcode = "EPB";
            public const string GenerateCartonBarcode = "GCB";
            public const string GenerateProductBarcode = "GPB";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<action> Get()
            {
                return new List<action>
                {
                    new action { Value = Insert, Display = "Thêm mới" },
                    new action { Value = Update, Display = "Cập nhật" },
                    new action { Value = Delete, Display = "Xóa" },
                    new action { Value = Config, Display = "Sửa thiết lập" },
                    new action { Value = Print, Display = "In" },
                    new action { Value = ScanEmployee, Display = "Quét mã NV" },
                    new action { Value = ScanProductLot, Display = "Quét mã lô thành phẩm" },
                    new action { Value = ScanCustomer, Display = "Quét mã KH" },
                    new action { Value = ScanPallet, Display = "Quét mã QR pallet" },
                    new action { Value = ScanCarton, Display = "Quét mã QR thùng" },
                    new action { Value = ScanProduct, Display = "Quét mã QR thành phẩm" },
                    new action { Value = ScanEnd, Display = "Quét mã kết thúc" },
                    new action { Value = ScanDetroyLabel, Display = "Quét mã hủy bao bì" },
                    new action { Value = ScanDetroyLabelEnd, Display = "Quét mã kết thúc hủy bao bì" },
                    new action { Value = ScanDetroyLabelForceEnd, Display = "Quét mã force kết thúc hủy bao bì" },
                    new action { Value = ScanTakeSample, Display = "Quét mã lấy mẫu" },
                    new action { Value = ScanTakeSampleEnd, Display = "Quét mã kết thúc lấy mẫu" },
                    new action { Value = ScanBarcodeInvalid, Display = "Quét mã QR không hợp lệ" },
                    new action { Value = ScanExportNote, Display = "Quét mã phiếu xuất hàng" },
                    new action { Value = ScanSplitNote, Display = "Quét mã phiếu chia hàng" },
                    new action { Value = ScanPrepareNote, Display = "Quét mã phiếu soạn hàng" },
                    new action { Value = ScanDeliveryNote, Display = "Quét mã phiếu giao hàng" },
                    new action { Value = ScanDriverSign, Display = "Quét biển số xe" },
                    new action { Value = PrintCartonLabel, Display = "In nhãn thùng" },
                    new action { Value = PrintPalletLabel, Display = "In nhãn pallet" },
                    new action { Value = ExportCartonBarcode, Display = "Xuất dữ liệu QR thùng" },
                    new action { Value = ExportProductBarcode, Display = "Xuất dữ liệu QR thành phẩm" },
                    new action { Value = GenerateCartonBarcode, Display = "Tạo dữ liệu QR thùng" },
                    new action { Value = GenerateProductBarcode, Display = "Tạo dữ liệu QR thành phẩm" },
                };
            }
        }

        [StringLength(255)]
        public string Method { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public DateTime DateTime { get; set; }

        public string Description { get; set; }
    }

    public class AuditTrailConfiguration : EntityTypeConfiguration<AuditTrail>
    {
        public AuditTrailConfiguration()
        {
            Property(e => e.TableName).IsUnicode(false);
            Property(e => e.Method).IsUnicode(false);
        }
    }
}
