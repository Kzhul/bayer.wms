﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public class ProductReturnFull
    {
        public ProductReturnHeader header { get; set; }
        public List<ProductReturnDetail> listDetail { get; set; }
    }

    public partial class ProductReturnHeader : BaseEntity
    {
        public ProductReturnHeader()
        {
            _Date = DateTime.Today;
            _status = status.New;
        }

        public ProductReturnHeader(dynamic header)
        {
            _ProductReturnCode = header.ProductReturnCode;
            _companyCode = header.CompanyCode;
            Province = header.Province;
            CompanyName = header.CompanyName;
            _Date = header.Date;
            _description = header.Description;
            _status = header.Status;
            _createdByName = header.CreatedByName;
            _createdBy = header.CreatedBy;
            _createdDateTime = header.CreatedDateTime;
            _createdBySitemapID = header.CreatedBySitemapID;
            _updatedBy = header.CreatedBy;
            _updatedDateTime = header.UpdatedDateTime;
            _updatedBySitemapID = header.UpdatedBySitemapID;
            _rowVersion = header.RowVersion;
        }

        private string _ProductReturnCode;

        [Key]
        [StringLength(255)]
        public string ProductReturnCode
        {
            get { return _ProductReturnCode; }
            set
            {
                _ProductReturnCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductReturnCode"));
            }
        }

        private DateTime _Date;

        [Column(TypeName = "date")]
        public DateTime Date
        {
            get { return _Date; }
            set
            {
                _Date = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Date"));
            }
        }
        
        [NotMapped]
        public string StrDate { get => _Date.ToString("dd/MM/yyyy"); }
        
        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _companyCode;

        [StringLength(255)]
        public string CompanyCode
        {
            get { return _companyCode; }
            set
            {
                _companyCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CompanyCode"));
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay { get => status.GetDisplay(_status); }

        public bool IsDeleted { get; set; }

        [NotMapped]
        public string CompanyName { get; set; }

        [NotMapped]
        public string Province { get; set; }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string New = "N";
            public const string Processing = "P";
            public const string Completed = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = Processing, Display = "Đang xử lý" },
                    new status { Value = Completed, Display = "Đã hoàn tất" }
                };
            }

            public static string GetDisplay(string Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case New:
                        display = "Mới";
                        break;
                    case Processing:
                        display = "Đang xử lý";
                        break;
                    case Completed:
                        display = "Đã hoàn tất";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private string _createdByName;

        [NotMapped]
        public string CreatedByName
        {
            get => _createdByName;
            set => _createdByName = value;
        }
        public dynamic Header { get; }
    }

    public class ProductReturnHeaderConfiguration : EntityTypeConfiguration<ProductReturnHeader>
    {
        public ProductReturnHeaderConfiguration()
        {
            ToTable("ProductReturnHeaders");
        }
    }
}
