namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class StockReceivingDetail : BaseEntity
    {
        public StockReceivingDetail()
        {
            _quantityPlanning = 0;
            _status = StockReceivingHeader.status.New;
        }

        public StockReceivingDetail(dynamic p)
        {
            _stockReceivingCode = p.StockReceivingCode;
            _lineNbr = p.LineNbr;
            _companyName = p.CompanyName;
            _productID = p.ProductID;
            _batchCode = p.BatchCode;
            _batchCodeDistributor = p.BatchCodeDistributor;
            _productCode = p.ProductCode;
            _productDescription = p.ProductDescription;
            _expiryDate = p.ExpiryDate;
            _pOCode = p.POCode;
            _pOLine = p.POLine;
            _uOM = p.UOM;
            _quantityPlanning = p.QuantityPlanning;
            _cartonPlanning = p.CartonPlanning;
            _palletPlanning = p.PalletPlanning;
            _quantityReceived = p.QuantityReceived;
            _cartonReceived = p.CartonReceived;
            _palletReceived = p.PalletReceived;
            _palletSetLocation = p.PalletSetLocation;
            _deliveryDate = p.DeliveryDate;
            _deliveryHour = p.DeliveryHour;
            _description = p.Description;
            _receiver = p.Receiver;
            _status = p.Status;
            _createdBy = p.CreatedBy;
            _createdDateTime = p.CreatedDateTime;
            _createdBySitemapID = p.CreatedBySitemapID;
            _updatedBy = p.UpdatedBy;
            _updatedDateTime = p.UpdatedDateTime;
            _updatedBySitemapID = p.UpdatedBySitemapID;
            _rowVersion = p.RowVersion;
            _palletPackaging = p.PalletPackaging;
            _batchCodePrintedNbr = p.BatchCodePrintedNbr;
            _manufacturingDate = p.ManufacturingDate;
        }

        private string _stockReceivingCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string StockReceivingCode
        {
            get { return _stockReceivingCode; }
            set
            {
                _stockReceivingCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("StockReceivingCode"));
            }
        }

        private int _lineNbr;

        [Key]
        [Column(Order = 1)]
        public int LineNbr
        {
            get { return _lineNbr; }
            set
            {
                _lineNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineNbr"));
            }
        }

        private string _companyName;

        [StringLength(255)]
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                _companyName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CompanyName"));
            }
        }

        private int _productID;

        public int ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _batchCode;

        [StringLength(255)]
        public string BatchCode
        {
            get { return _batchCode; }
            set
            {
                _batchCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCode"));
            }
        }

        private int? _batchCodePrintedNbr;
        
        public int? BatchCodePrintedNbr
        {
            get { return _batchCodePrintedNbr; }
            set
            {
                _batchCodePrintedNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCodePrintedNbr"));
            }
        }        

        private string _batchCodeDistributor;

        [StringLength(255)]
        public string BatchCodeDistributor
        {
            get { return _batchCodeDistributor; }
            set
            {
                _batchCodeDistributor = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCodeDistributor"));
            }
        }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        private DateTime? _expiryDate;

        [Column(TypeName = "date")]
        public DateTime? ExpiryDate
        {
            get { return _expiryDate; }
            set
            {
                _expiryDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ExpiryDate"));
            }
        }

        private DateTime? _manufacturingDate;

        [Column(TypeName = "date")]
        public DateTime? ManufacturingDate
        {
            get { return _manufacturingDate; }
            set
            {
                _manufacturingDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ManufacturingDate"));
            }
        }

        

        private string _pOCode;

        [StringLength(255)]
        public string POCode
        {
            get { return _pOCode; }
            set
            {
                _pOCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("POCode"));
            }
        }

        private string _pOLine;

        [StringLength(255)]
        public string POLine
        {
            get { return _pOLine; }
            set
            {
                _pOLine = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("POLine"));
            }
        }

        private string _uOM;

        [StringLength(255)]
        public string UOM
        {
            get { return _uOM; }
            set
            {
                _uOM = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UOM"));
            }
        }

        private decimal? _quantityPlanning;

        public decimal? QuantityPlanning
        {
            get { return _quantityPlanning; }
            set
            {
                _quantityPlanning = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("QuantityPlanning"));
            }
        }



        private decimal? _cartonPlanning;

        public decimal? CartonPlanning
        {
            get { return _cartonPlanning; }
            set
            {
                _cartonPlanning = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CartonPlanning"));
            }
        }

        private decimal? _palletPlanning;

        public decimal? PalletPlanning
        {
            get { return _palletPlanning; }
            set
            {
                _palletPlanning = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletPlanning"));
            }
        }

        private decimal? _quantityReceived;

        public decimal? QuantityReceived
        {
            get { return _quantityReceived; }
            set
            {
                _quantityReceived = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("QuantityReceived"));
            }
        }

        private decimal? _cartonReceived;

        public decimal? CartonReceived
        {
            get { return _cartonReceived; }
            set
            {
                _cartonReceived = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CartonReceived"));
            }
        }

        private decimal? _palletReceived;

        public decimal? PalletReceived
        {
            get { return _palletReceived; }
            set
            {
                _palletReceived = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletReceived"));
            }
        }

        private decimal? _palletSetLocation;

        public decimal? PalletSetLocation
        {
            get { return _palletSetLocation; }
            set
            {
                _palletSetLocation = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletSetLocation"));
            }
        }

        private decimal? _cartonPackaging;

        [NotMapped]
        public decimal? CartonPackaging
        {
            get { return _cartonPackaging; }
            set
            {
                _cartonPackaging = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CartonPackaging"));
            }
        }

        private decimal? _palletPackaging;

        [NotMapped]
        public decimal? PalletPackaging
        {
            get { return _palletPackaging; }
            set
            {
                _palletPackaging = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletPackaging"));
            }
        }

        private DateTime? _deliveryDate;

        [Column(TypeName = "date")]
        public DateTime? DeliveryDate
        {
            get { return _deliveryDate; }
            set
            {
                _deliveryDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DeliveryDate"));
            }
        }

        private string _deliveryHour;

        [StringLength(255)]
        public string DeliveryHour
        {
            get { return _deliveryHour; }
            set
            {
                _deliveryHour = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DeliveryHour"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _receiver;

        [StringLength(255)]
        public string Receiver
        {
            get { return _receiver; }
            set
            {
                _receiver = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Receiver"));
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay { get => StockReceivingHeader.status.GetDisplay(_status); }
    }

    public class StockReceivingDetailConfiguration : EntityTypeConfiguration<StockReceivingDetail>
    {
        public StockReceivingDetailConfiguration()
        {
            ToTable("StockReceivingDetails");
        }
    }
}
