﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public partial class Product : BaseEntity
    {
        public Product()
        {
            _type = type.F;
            _status = status.Active;
        }

        public Product(dynamic product)
        {
            _productID = product.ProductID;
            _productCode = product.ProductCode;
            _description = product.Description;
            _categoryID = product.CategoryID;
            _categoryDescription = product.CategoryDescription;
            _type = product.Type;
            _uom = product.UOM;
            _packingType = product.PackingType;
            _sampleQty = product.SampleQty;
            _printLabelPercentage = product.PrintLabelPercentage;
            _status = product.Status;
            _isDeleted = product.IsDeleted;
            _createdBy = product.CreatedBy;
            _createdBySitemapID = product.CreatedBySitemapID;
            _createdDateTime = product.CreatedDateTime;
            _updatedBy = product.CreatedBy;
            _updatedBySitemapID = product.UpdatedBySitemapID;
            _updatedDateTime = product.UpdatedDateTime;
            _rowVersion = product.RowVersion;
            _palletSize = product.PalletSize;
            _quantityByUnit = product.QuantityByUnit;
            _quantityByCarton = product.QuantityByCarton;
        }

        private int _productID;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _productCode;

        [Index("UK_Products_ProductCode", IsUnique = true)]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_ProductCode)]
        public string ProductCode
        {
            get { return _productCode; }
            set
            {
                _productCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductCode"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private int? _categoryID;

        public int? CategoryID
        {
            get { return _categoryID; }
            set
            {
                _categoryID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CategoryID"));
            }
        }

        private string _categoryDescription;

        [NotMapped]
        public string CategoryDescription
        {
            get { return _categoryDescription; }
            set { _categoryDescription = value; }
        }

        private string _type;

        [StringLength(1)]
        [Required]
        public string Type
        {
            get { return _type; }
            set
            {
                _type = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Type"));
            }
        }

        [NotMapped]
        public string TypeDisplay
        {
            get { return type.GetDisplay(_type); }
        }

        public class type
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string F = "F";
            public const string M = "M";
            public const string P = "P";
            public const string K = "K";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<type> Get()
            {
                return new List<type>
                {
                    new type { Value = F, Display = "Thành phẩm" },
                    new type { Value = M, Display = "Nguyên liệu" },
                    new type { Value = P, Display = "Bao bì" },
                    new type { Value = K, Display = "Hàng khuyến mãi" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case F:
                        display = "Thành phẩm";
                        break;
                    case M:
                        display = "Nguyên liệu";
                        break;
                    case P:
                        display = "Bao bì";
                        break;
                    case K:
                        display = "Hàng khuyến mãi";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private string _uom;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_UOM)]
        public string UOM
        {
            get { return _uom; }
            set
            {
                _uom = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UOM"));
            }
        }

        private string _packingType;

        [StringLength(1)]
        public string PackingType
        {
            get { return _packingType; }
            set
            {
                _packingType = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PackingType"));
            }
        }

        [NotMapped]
        public string PackingTypeDisplay
        {
            get { return packingType.GetDisplay(_packingType); }
        }

        private int? _sampleQty;

        public int? SampleQty
        {
            get { return _sampleQty; }
            set
            {
                _sampleQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("SampleQty"));
            }
        }

        private decimal? _printLabelPercentage;

        public decimal? PrintLabelPercentage
        {
            get { return _printLabelPercentage; }
            set
            {
                _printLabelPercentage = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PrintLabelPercentage"));
            }
        }

        public class packingType
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Carton = "C";
            public const string Shove = "S";
            public const string Bag = "B";
            public const string Can = "A";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = Carton, Display = "Thùng" },
                    new status { Value = Shove, Display = "Xô" },
                    new status { Value = Can, Display = "Can" },
                    new status { Value = Bag, Display = "Bao lớn" }
                };
            }

            public static List<status> GetWithAll()
            {
                return new List<status>
                {
                    new status { Value = String.Empty, Display = String.Empty },
                    new status { Value = Carton, Display = "Thùng" },
                    new status { Value = Shove, Display = "Xô" },
                    new status { Value = Can, Display = "Can" },
                    new status { Value = Bag, Display = "Bao lớn" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Carton:
                        display = "Thùng";
                        break;
                    case Shove:
                        display = "Xô";
                        break;
                    case Can:
                        display = "Can";
                        break;
                    case Bag:
                        display = "Bao lớn";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private string _status;

        [StringLength(1)]
        [Required]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(_status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Active = "A";
            public const string Inactive = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = Active, Display = "Đang hiệu lực" },
                    new status { Value = Inactive, Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Active:
                        display = "Đang hiệu lực";
                        break;
                    case Inactive:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        [NotMapped]
        public string ProductDisplay
        {
            get { return $"{_productCode} - {_description}"; }
        }

        private decimal? _cartonPackaging;


        private int? _palletSize;

        public int? PalletSize
        {
            get { return _palletSize; }
            set
            {
                _palletSize = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletSize"));
            }
        }

        private decimal? _quantityByUnit;

        public decimal? QuantityByUnit
        {
            get { return _quantityByUnit; }
            set
            {
                _quantityByUnit = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("QuantityByUnit"));
            }
        }

        private decimal? _quantityByCarton;

        public decimal? QuantityByCarton
        {
            get { return _quantityByCarton; }
            set
            {
                _quantityByCarton = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("QuantityByCarton"));
            }
        }

    }

    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            Property(e => e.ProductCode).IsUnicode(false);
            Property(e => e.Type).IsFixedLength().IsUnicode(false);
            Property(e => e.UOM).IsUnicode(false);
            Property(e => e.Status).IsFixedLength().IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
