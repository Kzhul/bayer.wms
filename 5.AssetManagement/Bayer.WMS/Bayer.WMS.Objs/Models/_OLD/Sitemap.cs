namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class Sitemap
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SitemapID { get; set; }

        public int? ParentID { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(255)]
        public string ClassName { get; set; }

        [StringLength(255)]
        public string InterfaceName { get; set; }

        [StringLength(255)]
        public string AssemblyName { get; set; }

        public int? Order { get; set; }

        [StringLength(255)]
        public string Icon { get; set; }

        public bool? EnableRefresh { get; set; }

        public bool? EnableInsert { get; set; }

        public bool? EnableSave { get; set; }

        public bool? EnableDelete { get; set; }

        public bool? EnablePrint { get; set; }

        public string Type { get; set; }

        public class type
        {
            public const string Menu = "M";
            public const string View = "V";
            public const string Feature = "F";
        }

        private string _accessRights;

        [NotMapped]
        public string AccessRights
        {
            get { return _accessRights; }
            set { _accessRights = value; }
        }
    }

    public class SitemapConfiguration : EntityTypeConfiguration<Sitemap>
    {
        public SitemapConfiguration()
        {
            Property(e => e.ClassName).IsUnicode(false);
            Property(e => e.InterfaceName).IsUnicode(false);
            Property(e => e.AssemblyName).IsUnicode(false);
            Property(e => e.Icon).IsUnicode(false);
            Property(e => e.Type).IsUnicode(false).IsFixedLength();
        }
    }
}
