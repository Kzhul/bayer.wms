﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class ProductPacking : BaseEntity
    {
        public ProductPacking()
        {
            Type = type.Pallet;
            State = EntityState.Unchanged;
        }

        public ProductPacking(EntityState state)
        {
            State = state;
        }

        public ProductPacking(int? productID, string type, decimal? quantity, string size, string weight, string note)
        {
            _productID = productID;
            _type = type;
            _quantity = quantity;
            _size = size;
            _weight = weight;
            _note = note;
        }

        private string _productPackingCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string ProductPackingCode
        {
            get { return _productPackingCode; }
            set
            {
                _productPackingCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductPackingCode"));
            }
        }

        private int? _productID;

        [Key]
        [Column(Order = 1)]
        [Required(ErrorMessage = Messages.Validate_Required_Product)]
        public int? ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _type;

        [StringLength(1)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_ProductPackingType)]
        public string Type
        {
            get { return _type; }
            set
            {
                _type = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Type"));
            }
        }

        [NotMapped]
        public string TypeDisplay
        {
            get { return type.GetDisplay(Type); }
        }

        public class type
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Pallet = "P";
            public const string Carton = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<type> Get()
            {
                return new List<type>
                {
                    new type { Value = Pallet, Display = "Pallet" },
                    new type { Value = Carton, Display = "Thùng" }
                };
            }

            public static List<type> GetWithAll()
            {
                return new List<type>
                {
                    new type { Value = String.Empty, Display = "Tất cả" },
                    new type { Value = Pallet, Display = "Pallet" },
                    new type { Value = Carton, Display = "Thùng" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Pallet:
                        display = "Pallet";
                        break;
                    case Carton:
                        display = "Thùng";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private decimal? _quantity;

        [Required(ErrorMessage = Messages.Validate_Required_ProductPackingQuantity)]
        [Range(typeof(Decimal), "0.00", "1000000000000000000", ErrorMessage = Messages.Validate_Range_PackingQuantity)]
        public decimal? Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Quantity"));
            }
        }

        private string _size;

        [StringLength(255)]
        public string Size
        {
            get { return _size; }
            set
            {
                _size = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Size"));
            }
        }

        private string _weight;

        [StringLength(255)]
        public string Weight
        {
            get { return _weight; }
            set
            {
                _weight = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Weight"));
            }
        }

        private string _note;

        [StringLength(255)]
        public string Note
        {
            get { return _note; }
            set
            {
                _note = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Note"));
            }
        }

        public void GenerateProductPackingCode()
        {
            _productPackingCode = $"{_productID:00000}_{_type}";
        }
    }

    public class ProductPackingConfiguration : EntityTypeConfiguration<ProductPacking>
    {
        public ProductPackingConfiguration()
        {
            Property(e => e.ProductPackingCode).IsUnicode(false);
            Property(e => e.Type).IsFixedLength().IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
