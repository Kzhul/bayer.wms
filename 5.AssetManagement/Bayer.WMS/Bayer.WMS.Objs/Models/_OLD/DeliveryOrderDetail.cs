﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class DeliveryOrderDetail : BaseEntity
    {
        public DeliveryOrderDetail()
        {
        }

        public DeliveryOrderDetail( int lineNBR, string delivery, int productID, string productCode, string productDescription, string batchCode, decimal? quantity, string companyCode, string companyName, string provinceSoldTo, string provinceShipTo, DateTime? deliveryDate, string status, string batchCodeDistributor)
        {
            _lineNbr = lineNBR;
            _delivery = delivery;
            _productID = productID;
            _batchCode = batchCode;
            _quantity = quantity;
            _companyCode = companyCode;
            _companyName = companyName;
            _provinceSoldTo = provinceSoldTo;
            _provinceShipTo = provinceShipTo;
            _deliveryDate = deliveryDate;
            _status = status;
            _productName = productDescription;
            _productCode = productCode;
            _batchCodeDistributor = batchCodeDistributor;
        }

        public DeliveryOrderDetail(dynamic p)
        {
            DOImportCode = p.DOImportCode;
            _lineNbr = p.LineNbr;
            _delivery = p.Delivery;
            _productID = p.ProductID;
            _productCode = p.ProductCode;
            _productName = p.ProductDescription;
            _quantity = p.Quantity;
            _batchCode = p.BatchCode;
           _batchCodeDistributor = p.BatchCodeDistributor;
            _companyCode = p.CompanyCode;
            _companyName = p.CompanyName;
            _provinceSoldTo = p.ProvinceSoldTo;
            _provinceShipTo = p.ProvinceShipTo;
            _deliveryDate = p.DeliveryDate;
            _status = p.Status;
            _createdBy = p.CreatedBy;
            _createdDateTime = p.CreatedDateTime;
            _updatedBy = p.UpdatedBy;
            _updatedDateTime = p.UpdatedDateTime;
            _rowVersion = p.RowVersion;

            PreparedQty = p.PreparedQty;
            DeliveredQty = p.DeliveredQty;
            DOQuantity = p.DOQuantity;
            RequireReturnQty = p.RequireReturnQty;
            ChangeReason = p.ChangeReason;
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string DOImportCode { get; set; }

        private int _lineNbr;

        [Key]
        [Column(Order = 1)]
        //[StringLength(255)]
        public int LineNbr
        {
            get { return _lineNbr; }
            set
            {
                _lineNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineNbr"));
            }
        }

        private string _delivery;

        [StringLength(255)]
        public string Delivery
        {
            get { return _delivery; }
            set
            {
                _delivery = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Delivery"));
            }
        }

        private int _productID;

        public int ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _productCode;        
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productName;
        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }

        private string _batchCode;

        [StringLength(255)]
        public string BatchCode
        {
            get { return _batchCode; }
            set
            {
                _batchCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCode"));
            }
        }

        private string _batchCodeDistributor;

        [StringLength(255)]
        public string BatchCodeDistributor
        {
            get { return _batchCodeDistributor; }
            set
            {
                _batchCodeDistributor = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCodeDistributor"));
            }
        }

        private decimal? _quantity;

        public decimal? Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Quantity"));
            }
        }

        private string _companyCode;

        [StringLength(255)]
        public string CompanyCode
        {
            get { return _companyCode; }
            set
            {
                _companyCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CompanyCode"));
            }
        }

        private string _companyName;

        [StringLength(255)]
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                _companyName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CompanyName"));
            }
        }

        private string _provinceSoldTo;

        [StringLength(255)]
        public string ProvinceSoldTo
        {
            get { return _provinceSoldTo; }
            set
            {
                _provinceSoldTo = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProvinceSoldTo"));
            }
        }

        private string _provinceShipTo;

        [StringLength(255)]
        public string ProvinceShipTo
        {
            get { return _provinceShipTo; }
            set
            {
                _provinceShipTo = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProvinceShipTo"));
            }
        }

        private DateTime? _deliveryDate;

        [Column(TypeName = "date")]
        public DateTime? DeliveryDate
        {
            get { return _deliveryDate; }
            set
            {
                _deliveryDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DeliveryDate"));
            }
        }

        [NotMapped]
        public string StrDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {

            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        public decimal? PreparedQty { get; set; }
        
        public decimal? DeliveredQty { get; set; }
        public decimal? DOQuantity { get; set; }
        public decimal? RequireReturnQty { get; set; }

        private string _changeReason;

        [StringLength(255)]
        public string ChangeReason
        {
            get { return _changeReason; }
            set
            {
                _changeReason = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ChangeReason"));
            }
        }

        [NotMapped]
        public decimal? DiffQuantity { get; set; }

        [NotMapped]
        public decimal? ChangedQuantity { get; set; }

        [NotMapped]
        public int ChangedCase { get; set; }
        //1 = tăng số SL
        //2 = giảm SL
        //3 = dòng mới
    }

    public class DeliveryOrderDetailConfiguration : EntityTypeConfiguration<DeliveryOrderDetail>
    {
        public DeliveryOrderDetailConfiguration()
        {
            ToTable("DeliveryOrderDetails");
        }
    }
}
