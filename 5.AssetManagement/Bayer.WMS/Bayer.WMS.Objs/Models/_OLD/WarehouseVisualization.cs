﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class WarehouseVisualization : BaseEntity
    {
        public WarehouseVisualization()
        {
        }

        private int _userID;

        [Key]
        [Column(Order = 0)]
        public int UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UserID"));
            }
        }

        private string _control;

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string Control
        {
            get { return _control; }
            set
            {
                _control = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Control"));
            }
        }

        private decimal _x;

        [Range(typeof(Decimal), "0.00000", "1000000000.00000")]
        public decimal X
        {
            get { return _x; }
            set
            {
                _x = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("X"));
            }
        }

        private decimal _y;

        [Range(typeof(Decimal), "0.00000", "1000000000.0000")]
        public decimal Y
        {
            get { return _y; }
            set
            {
                _y = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Y"));
            }
        }

        private string _parent;

        [StringLength(255)]
        public string Parent
        {
            get { return _parent; }
            set
            {
                _parent = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Parent"));
            }
        }
    }

    public class WarehouseVisualizationConfiguration : EntityTypeConfiguration<WarehouseVisualization>
    {
        public WarehouseVisualizationConfiguration()
        {
            Property(e => e.Control).IsUnicode(false);
            Property(e => e.Parent).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
