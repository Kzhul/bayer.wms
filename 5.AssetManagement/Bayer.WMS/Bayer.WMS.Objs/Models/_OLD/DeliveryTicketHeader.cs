﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Globalization;

    public class DeliveryTicketFull
    {
        public DeliveryTicketHeader header { get; set; }
        public List<DeliveryTicketDetail> listPrepare { get; set; }
        public List<PrepareNoteDetail> listProductDetail { get; set; }
    }

    public partial class DeliveryTicketHeader : BaseEntity
    {
        [Key]
        [StringLength(30)]
        public string DeliveryTicketCode { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public DateTime? DeliveryDate { get; set; }

        [StringLength(250)]
        public string DeliveryAddress { get; set; }

        [StringLength(30)]
        public string CompanyCode { get; set; }

        [StringLength(250)]
        public string CompanyName { get; set; }

        [StringLength(250)]
        public string Province { get; set; }

        [StringLength(250)]
        public string UserCreateFullName { get; set; }

        [StringLength(250)]
        public string UserWareHouse { get; set; }

        [StringLength(250)]
        public string UserReview { get; set; }

        [StringLength(250)]
        public string ListOrderNumbers { get; set; }

        [StringLength(250)]
        public string ListFreeItems { get; set; }

        [StringLength(250)]
        public string FreeItemsWeight { get; set; }

        public decimal? TotalBoxes { get; set; }        

        public decimal? TotalBags { get; set; }

        public decimal? TotalBuckets { get; set; }

        public decimal? TotalCans { get; set; }

        public decimal? GrossWeight { get; set; }

        [StringLength(250)]
        public string TruckNo { get; set; }

        [StringLength(250)]
        public string Driver { get; set; }

        [StringLength(250)]
        public string DriverIdentification { get; set; }        

        [StringLength(250)]
        public string ReceiptBy { get; set; }

        [StringLength(250)]
        public string DeliveverSign { get; set; }

        [StringLength(250)]
        public string DriverSign { get; set; }

        [NotMapped]
        public string StrDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {
                
            }
        }

        public bool IsDeleted { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        [NotMapped]
        public string StrTotalBoxes { get; set; }

        [NotMapped]
        public string StrTotalBags { get; set; }

        [NotMapped]
        public string StrTotalBuckets { get; set; }

        [NotMapped]
        public string StrTotalCans { get; set; }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string New = "N";
            public const string InProgress = "I";
            public const string Complete = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = InProgress, Display = "Đang xử lý" },
                    new status { Value = Complete, Display = "Hoàn thành" },
                };
            }

            public static string GetDisplay(string Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case New:
                        display = "Mới";
                        break;
                    case InProgress:
                        display = "Đang xử lý";
                        break;
                    case Complete:
                        display = "Hoàn thành";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

    }

    public class DeliveryTicketHeaderConfiguration : EntityTypeConfiguration<DeliveryTicketHeader>
    {
        public DeliveryTicketHeaderConfiguration()
        {
            ToTable("DeliveryTicketHeaders");
        }
    }
}
