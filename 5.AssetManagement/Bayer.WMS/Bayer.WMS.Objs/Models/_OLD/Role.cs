﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class Role : BaseEntity
    {
        public Role()
        {
            IsDeleted = false;
        }

        private int _roleID;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoleID
        {
            get { return _roleID; }
            set
            {
                _roleID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RoleID"));
            }
        }

        private string _roleName;

        [Index("UK_Roles_RoleName", IsUnique = true)]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_RoleName)]
        public string RoleName
        {
            get { return _roleName; }
            set
            {
                _roleName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RoleName"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(IsDeleted); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public bool Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = true, Display = "Đang hiệu lực" },
                    new status { Value = false, Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(bool IsDeleted)
            {
                string display = String.Empty;
                switch (IsDeleted)
                {
                    case false:
                        display = "Đang hiệu lực";
                        break;
                    case true:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }
    }

    public class RoleConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleConfiguration()
        {
            ToTable("Roles");
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
