﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    [Table("PalletStatuses")]
    public partial class PalletStatus : BaseEntity
    {
        public PalletStatus()
        {
        }

        public PalletStatus(string palletCode, string cartonBarcode, string productBarcode, string encryptedProductBarcode, int? productID, string productLot)
        {
            _palletCode = palletCode;
            _cartonBarcode = cartonBarcode;
            _productBarcode = productBarcode;
            _encryptedProductBarcode = encryptedProductBarcode;
            _productID = productID;
            _productLot = productLot;
        }

        private string _palletCode;

        [StringLength(255)]
        public string PalletCode
        {
            get { return _palletCode; }
            set
            {
                _palletCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletCode"));
            }
        }

        private string _cartonBarcode;

        [StringLength(255)]
        public string CartonBarcode
        {
            get { return _cartonBarcode; }
            set
            {
                _cartonBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CartonBarcode"));
            }
        }

        private string _productBarcode;

        [Key]
        [StringLength(255)]
        public string ProductBarcode
        {
            get { return _productBarcode; }
            set
            {
                _productBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Barcode"));
            }
        }

        private string _encryptedProductBarcode;

        [StringLength(255)]
        public string EncryptedProductBarcode
        {
            get { return _encryptedProductBarcode; }
            set
            {
                _encryptedProductBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("EncryptedBarcode"));
            }
        }

        private int? _productID;

        public int? ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _productLot;

        [StringLength(255)]
        public string ProductLot
        {
            get { return _productLot; }
            set
            {
                _productLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductLot"));
            }
        }
    }

    public class PalletStatusConfiguration : EntityTypeConfiguration<PalletStatus>
    {
        public PalletStatusConfiguration()
        {
            Property(e => e.ProductBarcode).IsUnicode(false);
            Property(e => e.EncryptedProductBarcode).IsUnicode(false);
            Property(e => e.ProductLot).IsUnicode(false);
            Property(e => e.CartonBarcode).IsUnicode(false);
            Property(e => e.PalletCode).IsUnicode(false);
        }
    }
}
