﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class RoleSitemap : BaseEntity
    {
        public RoleSitemap()
        {
        }

        public RoleSitemap(dynamic roleSitemap)
        {
            _roleID = roleSitemap.RoleID;
            _sitemapID = roleSitemap.SitemapID;
            _sitemapDescription = roleSitemap.SitemapDescription;
            _sitemapParentID = roleSitemap.SitemapParentID;
            _sitemapOrder = roleSitemap.SitemapOrder;
            _accessRights = roleSitemap.AccessRights;
            _accessRights = !String.IsNullOrWhiteSpace(_accessRights) ? _accessRights : (_sitemapParentID.HasValue ? accessRights.Inherited : accessRights.NotSet);
            _createdBy = roleSitemap.CreatedBy;
            _createdBySitemapID = roleSitemap.CreatedBySitemapID;
            _createdDateTime = roleSitemap.CreatedDateTime;
            _updatedBy = roleSitemap.UpdatedBy;
            _updatedBySitemapID = roleSitemap.UpdatedBySitemapID;
            _updatedDateTime = roleSitemap.UpdatedDateTime;
            _rowVersion = roleSitemap.RowVersion;
        }

        private int _roleID;

        [Key]
        [Column(Order = 0)]
        public int RoleID
        {
            get { return _roleID; }
            set
            {
                _roleID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RoleID"));
            }
        }

        private int _sitemapID;

        [Key]
        [Column(Order = 1)]
        public int SitemapID
        {
            get { return _sitemapID; }
            set
            {
                _sitemapID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("SitemapID"));
            }
        }

        private string _sitemapDescription;

        [NotMapped]
        public string SitemapDescription
        {
            get { return _sitemapDescription; }
            set { _sitemapDescription = value; }
        }

        private int? _sitemapParentID;

        [NotMapped]
        public int? SitemapParentID
        {
            get { return _sitemapParentID; }
            set { _sitemapParentID = value; }
        }

        private int? _sitemapOrder;

        [NotMapped]
        public int? SitemapOrder
        {
            get { return _sitemapOrder; }
            set { _sitemapOrder = value; }
        }

        private int? _sitemapLevel;

        [NotMapped]
        public int? SitemapLevel
        {
            get { return _sitemapLevel; }
            set { _sitemapLevel = value; }
        }
        
        private string _sitemapTree;

        [NotMapped]
        public string SitemapTree
        {
            get { return _sitemapTree; }
            set { _sitemapTree = value; }
        }

        private string _accessRights;

        [StringLength(1)]
        public string AccessRights
        {
            get { return _accessRights; }
            set
            {
                _accessRights = !String.IsNullOrWhiteSpace(value) ? value : accessRights.Read;
                InvokePropertyChanged(new PropertyChangedEventArgs("AccessRights"));
            }
        }

        public class accessRights
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string NotSet = "N";
            public const string Read = "R";
            public const string Write = "W";
            public const string Modify = "M";
            public const string Inherited = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<accessRights> Get()
            {
                return new List<accessRights>
                {
                    new accessRights { Value = NotSet, Display = "Not Set" },
                    new accessRights { Value = Inherited, Display = "Inherited" },
                    new accessRights { Value = Read, Display = "Read" },
                    new accessRights { Value = Write, Display = "Write" },
                    new accessRights { Value = Modify, Display = "Modify" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case NotSet:
                        display = "Not Set";
                        break;
                    case Inherited:
                        display = "Inherited";
                        break;
                    case Read:
                        display = "Read";
                        break;
                    case Write:
                        display = "Write";
                        break;
                    case Modify:
                        display = "Modify";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }
    }

    public class RoleSitemapConfiguration : EntityTypeConfiguration<RoleSitemap>
    {
        public RoleSitemapConfiguration()
        {
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
