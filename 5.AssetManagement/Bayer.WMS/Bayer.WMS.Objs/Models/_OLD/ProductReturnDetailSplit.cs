namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class ProductReturnDetailSplit : BaseEntity
    {
        public ProductReturnDetailSplit()
        {
            _status = ProductReturnHeader.status.New;
        }

        public ProductReturnDetailSplit(dynamic p)
        {
            _productReturnCode = p.ProductReturnCode;
            _lineNbr = p.LineNbr;
            _productID = p.ProductID;
            _batchCode = p.BatchCode;
            _productBarcode = p.ProductBarcode;
            _encryptedProductBarcode = p.EncryptedProductBarcode;
            _cartonBarcode = p.CartonBarcode;
            _palletCode = p.PalletCode;
            _deliveryDate = p.DeliveryDate;
            _description = p.Description;
            _status = p.Status;
            _createdBy = p.CreatedBy;
            _createdDateTime = p.CreatedDateTime;
            _createdBySitemapID = p.CreatedBySitemapID;
            _updatedBy = p.UpdatedBy;
            _updatedDateTime = p.UpdatedDateTime;
            _updatedBySitemapID = p.UpdatedBySitemapID;
            _rowVersion = p.RowVersion;
            _productCode = p.ProductCode;
            _productDescription = p.ProductDescription;
            PackageSize = p.PackageSize;
            UOM = p.UOM;
        }

        private string _productReturnCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string ProductReturnCode
        {
            get { return _productReturnCode; }
            set
            {
                _productReturnCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductReturnCode"));
            }
        }

        private int _lineNbr;

        [Key]
        [Column(Order = 1)]
        public int LineNbr
        {
            get { return _lineNbr; }
            set
            {
                _lineNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineNbr"));
            }
        }

        private int _productID;

        public int ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _batchCode;

        [StringLength(255)]
        public string BatchCode
        {
            get { return _batchCode; }
            set
            {
                _batchCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCode"));
            }
        }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }
        
        private string _palletCode;

        [StringLength(255)]
        public string PalletCode
        {
            get { return _palletCode; }
            set
            {
                _palletCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletCode"));
            }
        }

        private string _productBarcode;

        [StringLength(255)]
        public string ProductBarcode
        {
            get { return _productBarcode; }
            set
            {
                _productBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductBarcode"));
            }
        }

        private string _encryptedProductBarcode;

        [StringLength(255)]
        public string EncryptedProductBarcode
        {
            get { return _encryptedProductBarcode; }
            set
            {
                _encryptedProductBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("EncryptedProductBarcode"));
            }
        }

        private string _cartonBarcode;

        public string CartonBarcode
        {
            get { return _cartonBarcode; }
            set
            {
                _cartonBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CartonBarcode"));
            }
        }

        private DateTime? _deliveryDate;

        [Column(TypeName = "datetime")]
        public DateTime? DeliveryDate
        {
            get { return _deliveryDate; }
            set
            {
                _deliveryDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DeliveryDate"));
            }
        }        

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay { get => ProductReturnHeader.status.GetDisplay(_status); }

        [NotMapped]
        public string UOM { get; set; }

        [NotMapped]
        public decimal? PackageSize { get; set; }
    }

    public class ProductReturnDetailSplitConfiguration : EntityTypeConfiguration<ProductReturnDetailSplit>
    {
        public ProductReturnDetailSplitConfiguration()
        {
            ToTable("ProductReturnDetailSplits");
        }
    }
}
