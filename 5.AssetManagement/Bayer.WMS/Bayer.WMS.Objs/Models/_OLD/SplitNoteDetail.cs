namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class SplitNoteDetail : BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string SplitCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string DOCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(255)]
        public string DeliveryCode { get; set; }

        [Key]
        [Column(Order = 3)]
        public int ProductID { get; set; }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        [Key]
        [Column(Order = 4)]
        [StringLength(255)]
        public string BatchCode { get; set; }

        [StringLength(255)]
        public string BatchCodeDistributor { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? PackQuantity { get; set; }

        [StringLength(255)]
        public string PackType { get; set; }

        [NotMapped]
        public string StrPackType
        {
            get { return PackType == "C" ? "T" : PackType; }
            set { }
        }

        [Key]
        [Column(Order = 5)]
        [StringLength(255)]
        public string CompanyCode { get; set; }

        [StringLength(255)]
        public string CompanyName { get; set; }

        [StringLength(255)]
        public string Province { get; set; }

        public decimal? ProductQuantity { get; set; }

        public decimal? SplittedQty { get; set; }

        public decimal? RequireReturnQty { get; set; }
        public decimal? ReturnedQty { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [NotMapped]
        public decimal? PackSize { get; set; }

        [NotMapped]
        public string PalletCodes { get; set; }

        public SplitNoteDetail()
        {
        }

        public SplitNoteDetail(dynamic p)
        {
            DOCode = p.DOCode;
            SplitCode = p.SplitCode;
            ProductID = p.ProductID;
            DeliveryCode = p.DeliveryCode;
            _productCode = p.ProductCode;
            _productDescription = p.ProductDescription;
            Quantity = p.Quantity;
            PackQuantity = p.PackQuantity;
            PackType = p.PackType;
            PackSize = p.PackSize;
            ProductQuantity = p.ProductQuantity;
            BatchCode = p.BatchCode;
            Status = p.Status;

            CompanyCode = p.CompanyCode;
            CompanyName = p.CompanyName;
            Province = p.Province;
            BatchCodeDistributor = p.BatchCodeDistributor;

            _createdBy = p.CreatedBy;
            _createdDateTime = p.CreatedDateTime;
            _updatedBy = p.UpdatedBy;
            _updatedDateTime = p.UpdatedDateTime;
            _rowVersion = p.RowVersion;

            SplittedQty = p.SplittedQty;
            RequireReturnQty = p.RequireReturnQty;
            ReturnedQty = p.ReturnedQty;
        }
    }

    public class SplitNoteDetailConfiguration : EntityTypeConfiguration<SplitNoteDetail>
    {
        public SplitNoteDetailConfiguration()
        {
            
        }
    }
}
