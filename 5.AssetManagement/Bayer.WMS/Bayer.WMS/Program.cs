﻿using Bayer.WMS.Inv.Views;
using Bayer.WMS.Objs;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            #region UtilitySetting
            Utility.AppPath = Application.StartupPath;
            Utility.info = new System.Globalization.CultureInfo("en-US");
            Utility.info.NumberFormat.NumberDecimalDigits = 0;
            Utility.info.NumberFormat.CurrencyDecimalDigits = 0;
            Utility.info.NumberFormat.CurrencyGroupSeparator = ",";
            Utility.info.NumberFormat.CurrencyDecimalSeparator = ".";
            Utility.info.NumberFormat.CurrencySymbol = String.Empty;
            Utility.info.DateTimeFormat.FullDateTimePattern = "dd/MM/yyyy HH:mm";
            Utility.info.DateTimeFormat.ShortTimePattern = "HH:mm:ss";
            Utility.info.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            #endregion

            // Add handler to handle the exception raised by main threads
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

            // Add handler to handle the exception raised by additional threads
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);


            var container = UnityConfig.GetConfiguredContainer();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainView());


            // Stop the application and all the threads in suspended state.
            Environment.Exit(-1);
        }


        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {// All exceptions thrown by the main thread are handled over this method

            ShowExceptionDetails(e.Exception);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // All exceptions thrown by additional threads are handled in this method
            ShowExceptionDetails(e.ExceptionObject as Exception);

            // Suspend the current thread for now to stop the exception from throwing.
            Thread.CurrentThread.Suspend();
        }

        static void ShowExceptionDetails(Exception ex)
        {
            WriteToFile(ex);

            // Do logging of exception details
            MessageBox.Show(ex.Message, ex.TargetSite.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private static void WriteToFile(Exception ex)
        {
            using (var sw = new StreamWriter("ErrorLog.txt", true))
            {
                sw.WriteLine("=======================================================================================================");
                sw.WriteLine($"Date: {DateTime.Now:dd.MM.yyyy hh:mm:ss}");

                sw.WriteLine("================Exception===================");
                sw.WriteLine(ex.Message);
                sw.WriteLine(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    sw.WriteLine("================InnerException===================");
                    sw.WriteLine(ex.InnerException.Message);
                    sw.WriteLine(ex.StackTrace);

                    if (ex.InnerException.InnerException != null)
                    {
                        sw.WriteLine("================InnerException.InnerException===================");
                        sw.WriteLine(ex.InnerException.Message);
                        sw.WriteLine(ex.StackTrace);
                    }
                }
                sw.WriteLine();
                sw.WriteLine();
                sw.Flush();
            }
        }
    }
}
