﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Reflection;
using Microsoft.Practices.Unity;

namespace Bayer.WMS.Inv.Views
{
    public partial class ReturnDOView : BaseForm, IReturnDOView
    {
        public ReturnDOView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductPacking);
            SetDoubleBuffered(dataGridView1);
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "ReturnDOCode";
            cmbCode.DisplayMember = "ReturnDOCode - Delivery - Description";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("ReturnDOCode", "Mã phiếu trả", 120),
                new MultiColumnComboBox.ComboBoxColumn("Delivery", "Mã DO", 100),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200)
            };
            cmbCode.DropDownWidth = 500;
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "GetUsers", "OnCodeSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            InitializeComboBox();

            var doPresenter = _presenter as IReturnDOPresenter;
            await Task.WhenAll(doPresenter.LoadReturnDOHeaders());

            if (!isRefresh)
                doPresenter.Insert();
        }

        public DataTable ReturnDOHeaders
        {
            set { cmbCode.Source = value; }
        }

        private ReturnDOHeader _returnDOHeader;

        public ReturnDOHeader ReturnDOHeader
        {
            get { return _returnDOHeader; }
            set
            {
                _returnDOHeader = value;

                #region Data Binding
                cmbCode.DataBindings.Clear();
                txtDescription.DataBindings.Clear();
                txtStatusDisplay.DataBindings.Clear();
                txtDeliveryDate.DataBindings.Clear();
                txtDeliveryCode.DataBindings.Clear();

                cmbCode.DataBindings.Add("Text", ReturnDOHeader, "ReturnDOCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", ReturnDOHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDeliveryDate.DataBindings.Add("Text", ReturnDOHeader, "StrReturnDate", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStatusDisplay.DataBindings.Add("Text", ReturnDOHeader, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDeliveryCode.DataBindings.Add("Text", ReturnDOHeader, "Delivery", true, DataSourceUpdateMode.OnPropertyChanged);
                #endregion

                #region Set Control
                if (!string.IsNullOrWhiteSpace(_returnDOHeader.ReturnDOCode))
                {
                    colRequestReturnQty.ReadOnly = true;
                    txtDeliveryCode.ReadOnly = true;
                    txtDescription.ReadOnly = true;
                    btnLoadDO.Visible = false;
                    butReturnFull.Visible = false;
                }
                else
                {
                    colRequestReturnQty.ReadOnly = false;
                    txtDeliveryCode.ReadOnly = false;
                    txtDescription.ReadOnly = false;
                    btnLoadDO.Visible = true;
                    butReturnFull.Visible = true;
                }

                
                #endregion
            }
        }



        private IList<ReturnDODetail> _returnDODetail;

        public IList<ReturnDODetail> ReturnDODetails
        {
            get { return _returnDODetail; }
            set
            {
                _returnDODetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _returnDODetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _returnDODetail;
                }
            }
        }

        private DataTable _ReportDODetail;

        public DataTable ReportDODetails
        {
            get { return _ReportDODetail; }
            set
            {
                _ReportDODetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsReport.DataSource = _ReportDODetail;
                    });
                }
                else
                {
                    bdsReport.DataSource = _ReportDODetail;
                }
            }
        }

        private IList<ReturnDODetail> _deletedReturnDODetails;

        public IList<ReturnDODetail> DeletedReturnDODetails
        {
            get { return _deletedReturnDODetails; }
            set { _deletedReturnDODetails = value; }
        }

        public override async Task Delete()
        {
            try
            {
                var current = bdsDetails.Current as ReturnDODetail;
                if (current != null)
                {
                    await Task.Run(() =>
                    {
                        if (InvokeRequired)
                        {
                            BeginInvoke((MethodInvoker)delegate
                            {
                                bdsDetails.RemoveCurrent();
                            });
                        }
                        else
                        {
                            bdsDetails.RemoveCurrent();
                        }
                    });

                    (_presenter as IReturnDOPresenter).DeleteReturnDODetail(current);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override void HandlePermission(string accessRight)
        {

        }

        public async void Print()
        {
            var doPresenter = _presenter as IReturnDOPresenter;
            await doPresenter.Print();
        }

        private async void btnLoadDO_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IReturnDOPresenter;
            await doPresenter.LoadDetailsByDeliveryCode(txtDeliveryCode.Text);
        }

        private async void butReturnFull_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtDescription.Text))
            {
                this.SetMessage("Vui lòng nhập lý do trả.", Utility.MessageType.Error);
                return;
            }

            DialogResult cfm = MessageBox.Show("Bạn có chắc muốn trả tất cả sản phẩm trong DO này ?", "Xác nhận", MessageBoxButtons.OKCancel);
            if (cfm == DialogResult.OK)
            {
                foreach (DataGridViewRow dr in dtgProductPacking.Rows)
                {
                    dr.Cells["colRequestReturnQty"].Value = dr.Cells["colProductionPlan_PackageQuantity"].Value;
                    dr.Cells["colDescription"].Value = txtDescription.Text;
                }

                await this.Save();
            }
        }

        public override async Task Save()
        {
            if (ValidateInput())
            {
                DialogResult cfm = MessageBox.Show("Phiếu này chỉ cho phép lưu 1 lần, đã lưu là không được phép chỉnh sửa nữa. \nHệ thống sẽ cập nhật tất cả các phiếu liên quan như: Import DO, Xuất Hàng, Chia Hàng, Soạn Hàng, Giao Hàng. \nBạn có chắc chắn là thông tin nhập liệu đã chính xác và muốn lưu.", "Xác nhận", MessageBoxButtons.OKCancel);
                if (cfm == DialogResult.OK)
                {
                    var doPresenter = _presenter as IReturnDOPresenter;
                    await doPresenter.Save();
                }
            }
        }

        private bool ValidateInput()
        {
            bool result = true;
            decimal doQuantity = 0;
            decimal requestReturnQuantity = 0;
            int lineReturn = 0;
            foreach (DataGridViewRow dr in dtgProductPacking.Rows)
            {
                doQuantity = Utility.DecimalParse(dr.Cells["colProductionPlan_PackageQuantity"].Value);
                requestReturnQuantity = Utility.DecimalParse(dr.Cells["colRequestReturnQty"].Value);

                if (doQuantity < requestReturnQuantity)
                {
                    result = false;
                    this.SetMessage("Nhập liệu sai, bạn không được trả nhiều hơn số lượng DO.", Utility.MessageType.Error);
                }

                if (requestReturnQuantity > 0)
                {
                    lineReturn++;
                }
            }

            if (string.IsNullOrWhiteSpace(txtDescription.Text))
            {
                result = false;
                this.SetMessage("Vui lòng nhập lý do trả.", Utility.MessageType.Error);                
            }

            if (lineReturn == 0)
            {
                result = false;
                this.SetMessage("Phải phải trả ít nhất một sản phẩm.", Utility.MessageType.Error);
            }

            return result;
        }
    }
}
