﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Reflection;
using Microsoft.Practices.Unity;

namespace Bayer.WMS.Inv.Views
{
    public partial class CompanyDefineView : BaseForm, ICompanyDefineView
    {
        public CompanyDefineView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductPacking);
            SetDoubleBuffered(dataGridView1);
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "DOImportCode";
            cmbCode.DisplayMember = "DOImportCode - StrDeliveryDate";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("DOImportCode", "Mã DO", 100),                
                new MultiColumnComboBox.ComboBoxColumn("StrDeliveryDate", "Ngày GH", 100),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200)
            };
            cmbCode.DropDownWidth = 500;
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "GetUsers", "OnCodeSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            InitializeComboBox();

            var doPresenter = _presenter as IDeliveryOrderPresenter;
            await Task.WhenAll(doPresenter.LoadDeliveryOrderHeaders());

            if (!isRefresh)
                doPresenter.Insert();
        }

        public DataTable DeliveryOrderHeaders
        {
            set { cmbCode.Source = value; }
        }

        private DeliveryOrderHeader _deliveryOrderHeader;

        public DeliveryOrderHeader DeliveryOrderHeader
        {
            get { return _deliveryOrderHeader; }
            set
            {
                _deliveryOrderHeader = value;

                #region Data Binding
                cmbCode.DataBindings.Clear();
                txtProductDescription.DataBindings.Clear();
                txtStatusDisplay.DataBindings.Clear();
                txtStrIsDeliveryFull.DataBindings.Clear();
                txtStrIsExportFull.DataBindings.Clear();
                txtStrIsPrepareFull.DataBindings.Clear();
                txtStrIsSplitFull.DataBindings.Clear();

                cmbCode.DataBindings.Add("Text", DeliveryOrderHeader, "DOImportCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProductDescription.DataBindings.Add("Text", DeliveryOrderHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStatusDisplay.DataBindings.Add("Text", DeliveryOrderHeader, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStrIsDeliveryFull.DataBindings.Add("Text", DeliveryOrderHeader, "StatusDeliveryDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStrIsExportFull.DataBindings.Add("Text", DeliveryOrderHeader, "StatusExportDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStrIsPrepareFull.DataBindings.Add("Text", DeliveryOrderHeader, "StatusPrepareDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStrIsSplitFull.DataBindings.Add("Text", DeliveryOrderHeader, "StatusSplitDisplay", true, DataSourceUpdateMode.OnPropertyChanged); 
                #endregion

                #region Color
                txtStrIsDeliveryFull.BackColor = Utility.StatusToColor(this.DeliveryOrderHeader.IsDeliveryFull);
                txtStrIsExportFull.BackColor = Utility.StatusToColor(this.DeliveryOrderHeader.IsExportFull);
                txtStrIsPrepareFull.BackColor = Utility.StatusToColor(this.DeliveryOrderHeader.IsPrepareFull);
                txtStrIsSplitFull.BackColor = Utility.StatusToColor(this.DeliveryOrderHeader.IsSplitFull);
                #endregion
            }
        }
        
        private IList<DeliveryOrderDetail> _deliveryOrderDetail;

        public IList<DeliveryOrderDetail> DeliveryOrderDetails
        {
            get { return _deliveryOrderDetail; }
            set
            {
                _deliveryOrderDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _deliveryOrderDetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _deliveryOrderDetail;
                }
                bdsDetails.ResetBindings(false);

                #region Process Color
                foreach (DataGridViewRow row in dtgProductPacking.Rows)
                {
                    if (row.Cells["colLineNbr"].Value != null && Convert.ToInt32(row.Cells["colLineNbr"].Value) == 0)
                    {
                        row.DefaultCellStyle.BackColor = Color.LightYellow;
                    }
                    else if (
                        row.Cells["colQuantity"].Value != null
                        && row.Cells["colDOQuantity"].Value != null
                        && Convert.ToDecimal(row.Cells["colQuantity"].Value) != Convert.ToDecimal(row.Cells["colDOQuantity"].Value)
                        )
                    {
                        row.DefaultCellStyle.BackColor = Color.OrangeRed;
                    }
                    else
                    {
                        dtgProductPacking.BackgroundColor = SystemColors.Control;
                    }


                }
                #endregion


                #region Check if this a Additional DO
                //Disable Import btn
                //If Not Additional DO
                //Disable Panel Additional
                #endregion
            }
        }

        private DataTable _ReportDODetail;

        public DataTable ReportDODetails
        {
            get { return _ReportDODetail; }
            set
            {
                _ReportDODetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsReport.DataSource = _ReportDODetail;
                    });
                }
                else
                {
                    bdsReport.DataSource = _ReportDODetail;
                }
            }
        }

        private IList<DeliveryOrderDetail> _deletedDeliveryOrderDetails;

        public IList<DeliveryOrderDetail> DeletedDeliveryOrderDetails
        {
            get { return _deletedDeliveryOrderDetails; }
            set { _deletedDeliveryOrderDetails = value; }
        }

        public override async Task Delete()
        {
            try
            {
                var current = bdsDetails.Current as DeliveryOrderDetail;
                if (current != null)
                {
                    await Task.Run(() =>
                    {
                        if (InvokeRequired)
                        {
                            BeginInvoke((MethodInvoker)delegate
                            {
                                bdsDetails.RemoveCurrent();
                            });
                        }
                        else
                        {
                            bdsDetails.RemoveCurrent();
                        }
                    });

                    (_presenter as IDeliveryOrderPresenter).DeleteDeliveryOrderDetail(current);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override void HandlePermission(string accessRight)
        {
            string permissionImportExcel = Utility.Sitemaps.First(p => p.SitemapID == 21).AccessRights;
            string permissionGenDeliveryNote = Utility.Sitemaps.First(p => p.SitemapID == 22).AccessRights;
            string permissionGenSplitNote = Utility.Sitemaps.First(p => p.SitemapID == 23).AccessRights;
            string permissionGenPrepareNote = Utility.Sitemaps.First(p => p.SitemapID == 24).AccessRights;

            if (permissionImportExcel == RoleSitemap.accessRights.NotSet || permissionImportExcel == RoleSitemap.accessRights.Read)
            {
                btnUpload.Enabled = false;
                btnUpload.Click -= btnUpload_Click;
            }            
        }

        public string _fileName;

        public async void Print()
        {
            var doPresenter = _presenter as IDeliveryOrderPresenter;
            await doPresenter.Print();
        }

        private async void btnUpload_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        _fileName = ofd.FileName;
                        await (_presenter as IDeliveryOrderPresenter).ImportExcel(_fileName, txtProductDescription.Text);
                    }
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnUpload.Enabled = true;
            }
        }
        
        private async void btnCorrectAdditionalDO_Click(object sender, EventArgs e)
        {
            btnCorrectAdditionalDO.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        _fileName = ofd.FileName;
                        await (_presenter as IDeliveryOrderPresenter).CorrectAdditionalDO(_fileName, txtProductDescription.Text);
                    }
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnCorrectAdditionalDO.Enabled = true;
            }
        }

        private async void btnUpdateRelateDO_Click(object sender, EventArgs e)
        {
            await(_presenter as IDeliveryOrderPresenter).UpdateRelateDO();
        }
    }
}
