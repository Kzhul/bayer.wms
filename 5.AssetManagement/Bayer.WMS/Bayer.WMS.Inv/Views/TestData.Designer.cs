﻿namespace Bayer.WMS.Inv.Views
{
    partial class TestData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.btnGenBarcode = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtBarcode
            // 
            this.txtBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBarcode.Location = new System.Drawing.Point(3, 40);
            this.txtBarcode.Multiline = true;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(414, 430);
            this.txtBarcode.TabIndex = 0;
            // 
            // btnGenBarcode
            // 
            this.btnGenBarcode.BackColor = System.Drawing.Color.Green;
            this.btnGenBarcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenBarcode.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnGenBarcode.Location = new System.Drawing.Point(3, 4);
            this.btnGenBarcode.Name = "btnGenBarcode";
            this.btnGenBarcode.Size = new System.Drawing.Size(110, 30);
            this.btnGenBarcode.TabIndex = 2;
            this.btnGenBarcode.TabStop = false;
            this.btnGenBarcode.Text = "SAVE";
            this.btnGenBarcode.UseVisualStyleBackColor = false;
            this.btnGenBarcode.Click += new System.EventHandler(this.btnGenBarcode_Click);
            // 
            // TestData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 472);
            this.Controls.Add(this.btnGenBarcode);
            this.Controls.Add(this.txtBarcode);
            this.Name = "TestData";
            this.Text = "TestData";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Button btnGenBarcode;
    }
}