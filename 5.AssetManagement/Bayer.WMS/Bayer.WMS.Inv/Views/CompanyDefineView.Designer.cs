﻿using Bayer.WMS.Objs;

namespace Bayer.WMS.Inv.Views
{
    partial class CompanyDefineView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompanyDefineView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.btnUpload = new System.Windows.Forms.Button();
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatusDisplay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.colLineNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Device = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_MFG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colChangedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colChangeReason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveredQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequireReturnQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.colDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExportCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplitCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrepareCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveryTicketCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyCode1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEXStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSPStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPRStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDHStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsReport = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtStrIsExportFull = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStrIsSplitFull = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtStrIsPrepareFull = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtStrIsDeliveryFull = new System.Windows.Forms.TextBox();
            this.btnUpdateRelateDO = new System.Windows.Forms.Button();
            this.btnCorrectAdditionalDO = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsReport)).BeginInit();
            this.SuspendLayout();
            // 
            // btnUpload
            // 
            this.btnUpload.Image = ((System.Drawing.Image)(resources.GetObject("btnUpload.Image")));
            this.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpload.Location = new System.Drawing.Point(12, 100);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(72, 30);
            this.btnUpload.TabIndex = 2;
            this.btnUpload.TabStop = false;
            this.btnUpload.Text = "Import";
            this.btnUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(91, 12);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(310, 24);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 0;
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(91, 42);
            this.txtProductDescription.MaxLength = 255;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(310, 22);
            this.txtProductDescription.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Mô tả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã DO:";
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.Location = new System.Drawing.Point(91, 72);
            this.txtStatusDisplay.MaxLength = 255;
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.ReadOnly = true;
            this.txtStatusDisplay.Size = new System.Drawing.Size(310, 22);
            this.txtStatusDisplay.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Trạng thái:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(4, 136);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1030, 512);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgProductPacking);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1022, 483);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách sản phẩm";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToAddRows = false;
            this.dtgProductPacking.AllowUserToDeleteRows = false;
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLineNbr,
            this.colDOCode,
            this.colProductionPlan_ProductLot,
            this.colProductionPlan_Device,
            this.colProductionPlan_ProductCode,
            this.colProductionPlan_ProductDescription,
            this.colProductionPlan_Quantity,
            this.colProductionPlan_PackageSize,
            this.colQuantity,
            this.colProductionPlan_MFG,
            this.colBatchCodeDistributor,
            this.colChangedQuantity,
            this.colChangeReason,
            this.colDOQuantity,
            this.colDeliveredQty,
            this.colRequireReturnQty});
            this.dtgProductPacking.DataSource = this.bdsDetails;
            this.dtgProductPacking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(3, 3);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.Size = new System.Drawing.Size(1016, 477);
            this.dtgProductPacking.TabIndex = 7;
            // 
            // colLineNbr
            // 
            this.colLineNbr.DataPropertyName = "LineNbr";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colLineNbr.DefaultCellStyle = dataGridViewCellStyle2;
            this.colLineNbr.HeaderText = "STT";
            this.colLineNbr.Name = "colLineNbr";
            this.colLineNbr.ReadOnly = true;
            this.colLineNbr.Width = 40;
            // 
            // colDOCode
            // 
            this.colDOCode.DataPropertyName = "Delivery";
            dataGridViewCellStyle3.NullValue = null;
            this.colDOCode.DefaultCellStyle = dataGridViewCellStyle3;
            this.colDOCode.HeaderText = "Mã DO";
            this.colDOCode.Name = "colDOCode";
            this.colDOCode.ReadOnly = true;
            // 
            // colProductionPlan_ProductLot
            // 
            this.colProductionPlan_ProductLot.DataPropertyName = "CompanyName";
            this.colProductionPlan_ProductLot.HeaderText = "Tên Công Ty";
            this.colProductionPlan_ProductLot.Name = "colProductionPlan_ProductLot";
            this.colProductionPlan_ProductLot.ReadOnly = true;
            this.colProductionPlan_ProductLot.Width = 200;
            // 
            // colProductionPlan_Device
            // 
            this.colProductionPlan_Device.DataPropertyName = "Province";
            this.colProductionPlan_Device.HeaderText = "Tỉnh Thành";
            this.colProductionPlan_Device.Name = "colProductionPlan_Device";
            this.colProductionPlan_Device.ReadOnly = true;
            // 
            // colProductionPlan_ProductCode
            // 
            this.colProductionPlan_ProductCode.DataPropertyName = "CompanyCode";
            this.colProductionPlan_ProductCode.HeaderText = "Mã Công Ty";
            this.colProductionPlan_ProductCode.Name = "colProductionPlan_ProductCode";
            this.colProductionPlan_ProductCode.ReadOnly = true;
            // 
            // colProductionPlan_ProductDescription
            // 
            this.colProductionPlan_ProductDescription.DataPropertyName = "ProductCode";
            this.colProductionPlan_ProductDescription.HeaderText = "Mã Sản Phẩm";
            this.colProductionPlan_ProductDescription.Name = "colProductionPlan_ProductDescription";
            this.colProductionPlan_ProductDescription.ReadOnly = true;
            // 
            // colProductionPlan_Quantity
            // 
            this.colProductionPlan_Quantity.DataPropertyName = "ProductDescription";
            dataGridViewCellStyle4.NullValue = null;
            this.colProductionPlan_Quantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductionPlan_Quantity.HeaderText = "Mô Tả";
            this.colProductionPlan_Quantity.Name = "colProductionPlan_Quantity";
            this.colProductionPlan_Quantity.ReadOnly = true;
            this.colProductionPlan_Quantity.Width = 200;
            // 
            // colProductionPlan_PackageSize
            // 
            this.colProductionPlan_PackageSize.DataPropertyName = "BatchCode";
            dataGridViewCellStyle5.NullValue = null;
            this.colProductionPlan_PackageSize.DefaultCellStyle = dataGridViewCellStyle5;
            this.colProductionPlan_PackageSize.HeaderText = "Số Lô";
            this.colProductionPlan_PackageSize.Name = "colProductionPlan_PackageSize";
            this.colProductionPlan_PackageSize.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            this.colQuantity.DefaultCellStyle = dataGridViewCellStyle6;
            this.colQuantity.HeaderText = "Số lượng";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.ReadOnly = true;
            // 
            // colProductionPlan_MFG
            // 
            this.colProductionPlan_MFG.DataPropertyName = "StrDeliveryDate";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.NullValue = null;
            this.colProductionPlan_MFG.DefaultCellStyle = dataGridViewCellStyle7;
            this.colProductionPlan_MFG.HeaderText = "Ngày Giao Hàng";
            this.colProductionPlan_MFG.Name = "colProductionPlan_MFG";
            this.colProductionPlan_MFG.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "Số lô NCC";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            this.colBatchCodeDistributor.ReadOnly = true;
            // 
            // colChangedQuantity
            // 
            this.colChangedQuantity.DataPropertyName = "ChangedQuantity";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = null;
            this.colChangedQuantity.DefaultCellStyle = dataGridViewCellStyle8;
            this.colChangedQuantity.HeaderText = "Số lượng thay đổi";
            this.colChangedQuantity.Name = "colChangedQuantity";
            // 
            // colChangeReason
            // 
            this.colChangeReason.DataPropertyName = "ChangeReason";
            this.colChangeReason.HeaderText = "Lý do thay đổi";
            this.colChangeReason.Name = "colChangeReason";
            // 
            // colDOQuantity
            // 
            this.colDOQuantity.DataPropertyName = "DOQuantity";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            this.colDOQuantity.DefaultCellStyle = dataGridViewCellStyle9;
            this.colDOQuantity.HeaderText = "SL DO ban đầu";
            this.colDOQuantity.Name = "colDOQuantity";
            this.colDOQuantity.ReadOnly = true;
            // 
            // colDeliveredQty
            // 
            this.colDeliveredQty.DataPropertyName = "DeliveredQty";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N0";
            this.colDeliveredQty.DefaultCellStyle = dataGridViewCellStyle10;
            this.colDeliveredQty.HeaderText = "SL đã giao hàng";
            this.colDeliveredQty.Name = "colDeliveredQty";
            this.colDeliveredQty.ReadOnly = true;
            // 
            // colRequireReturnQty
            // 
            this.colRequireReturnQty.DataPropertyName = "RequireReturnQty";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N0";
            this.colRequireReturnQty.DefaultCellStyle = dataGridViewCellStyle11;
            this.colRequireReturnQty.HeaderText = "SL yêu cầu trả";
            this.colRequireReturnQty.Name = "colRequireReturnQty";
            this.colRequireReturnQty.ReadOnly = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1022, 483);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Danh sách phiếu liên quan";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.Azure;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDeliveryDate,
            this.colExportCode,
            this.colSplitCode,
            this.colPrepareCode,
            this.colDeliveryTicketCode,
            this.colCompanyCode1,
            this.colCompanyName1,
            this.colEXStatus,
            this.colSPStatus,
            this.colPRStatus,
            this.colDHStatus});
            this.dataGridView1.DataSource = this.bdsReport;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1016, 477);
            this.dataGridView1.TabIndex = 9;
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.DataPropertyName = "DeliveryDate";
            dataGridViewCellStyle13.Format = "dd/MM/yyyy";
            dataGridViewCellStyle13.NullValue = null;
            this.colDeliveryDate.DefaultCellStyle = dataGridViewCellStyle13;
            this.colDeliveryDate.HeaderText = "Ngày giao hàng";
            this.colDeliveryDate.Name = "colDeliveryDate";
            this.colDeliveryDate.ReadOnly = true;
            // 
            // colExportCode
            // 
            this.colExportCode.DataPropertyName = "ExportCode";
            this.colExportCode.HeaderText = "Phiếu Xuất Hàng";
            this.colExportCode.Name = "colExportCode";
            this.colExportCode.ReadOnly = true;
            // 
            // colSplitCode
            // 
            this.colSplitCode.DataPropertyName = "SplitCode";
            this.colSplitCode.HeaderText = "Phiếu Chia Hàng";
            this.colSplitCode.Name = "colSplitCode";
            this.colSplitCode.ReadOnly = true;
            // 
            // colPrepareCode
            // 
            this.colPrepareCode.DataPropertyName = "PrepareCode";
            this.colPrepareCode.HeaderText = "Phiếu Soạn Hàng";
            this.colPrepareCode.Name = "colPrepareCode";
            this.colPrepareCode.ReadOnly = true;
            // 
            // colDeliveryTicketCode
            // 
            this.colDeliveryTicketCode.DataPropertyName = "DeliveryTicketCode";
            this.colDeliveryTicketCode.HeaderText = "Phiếu Giao Hàng";
            this.colDeliveryTicketCode.Name = "colDeliveryTicketCode";
            this.colDeliveryTicketCode.ReadOnly = true;
            // 
            // colCompanyCode1
            // 
            this.colCompanyCode1.DataPropertyName = "CompanyCode";
            this.colCompanyCode1.HeaderText = "Mã Công Ty";
            this.colCompanyCode1.Name = "colCompanyCode1";
            this.colCompanyCode1.ReadOnly = true;
            // 
            // colCompanyName1
            // 
            this.colCompanyName1.DataPropertyName = "CompanyName";
            this.colCompanyName1.HeaderText = "Tên Công Ty";
            this.colCompanyName1.Name = "colCompanyName1";
            this.colCompanyName1.ReadOnly = true;
            // 
            // colEXStatus
            // 
            this.colEXStatus.DataPropertyName = "EXStatus";
            this.colEXStatus.HeaderText = "Trạng thái Xuất Hàng";
            this.colEXStatus.Name = "colEXStatus";
            this.colEXStatus.ReadOnly = true;
            // 
            // colSPStatus
            // 
            this.colSPStatus.DataPropertyName = "SPStatus";
            this.colSPStatus.HeaderText = "Trạng thái Chia Hàng";
            this.colSPStatus.Name = "colSPStatus";
            this.colSPStatus.ReadOnly = true;
            // 
            // colPRStatus
            // 
            this.colPRStatus.DataPropertyName = "PRStatus";
            this.colPRStatus.HeaderText = "Trạng thái Soạn Hàng";
            this.colPRStatus.Name = "colPRStatus";
            this.colPRStatus.ReadOnly = true;
            // 
            // colDHStatus
            // 
            this.colDHStatus.DataPropertyName = "DHStatus";
            this.colDHStatus.HeaderText = "Trạng thái Giao Hàng";
            this.colDHStatus.Name = "colDHStatus";
            this.colDHStatus.ReadOnly = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(426, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 16);
            this.label4.TabIndex = 19;
            this.label4.Text = "Trạng thái phiếu xuất hàng:";
            // 
            // txtStrIsExportFull
            // 
            this.txtStrIsExportFull.Location = new System.Drawing.Point(600, 12);
            this.txtStrIsExportFull.MaxLength = 255;
            this.txtStrIsExportFull.Name = "txtStrIsExportFull";
            this.txtStrIsExportFull.ReadOnly = true;
            this.txtStrIsExportFull.Size = new System.Drawing.Size(117, 22);
            this.txtStrIsExportFull.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(426, 45);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(168, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "Trạng thái phiếu chia hàng:";
            // 
            // txtStrIsSplitFull
            // 
            this.txtStrIsSplitFull.Location = new System.Drawing.Point(600, 42);
            this.txtStrIsSplitFull.MaxLength = 255;
            this.txtStrIsSplitFull.Name = "txtStrIsSplitFull";
            this.txtStrIsSplitFull.ReadOnly = true;
            this.txtStrIsSplitFull.Size = new System.Drawing.Size(117, 22);
            this.txtStrIsSplitFull.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(730, 15);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 16);
            this.label6.TabIndex = 23;
            this.label6.Text = "Trạng thái phiếu soạn hàng:";
            // 
            // txtStrIsPrepareFull
            // 
            this.txtStrIsPrepareFull.Location = new System.Drawing.Point(904, 12);
            this.txtStrIsPrepareFull.MaxLength = 255;
            this.txtStrIsPrepareFull.Name = "txtStrIsPrepareFull";
            this.txtStrIsPrepareFull.ReadOnly = true;
            this.txtStrIsPrepareFull.Size = new System.Drawing.Size(121, 22);
            this.txtStrIsPrepareFull.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(730, 43);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 16);
            this.label7.TabIndex = 25;
            this.label7.Text = "Trạng thái phiếu giao hàng:";
            // 
            // txtStrIsDeliveryFull
            // 
            this.txtStrIsDeliveryFull.Location = new System.Drawing.Point(904, 40);
            this.txtStrIsDeliveryFull.MaxLength = 255;
            this.txtStrIsDeliveryFull.Name = "txtStrIsDeliveryFull";
            this.txtStrIsDeliveryFull.ReadOnly = true;
            this.txtStrIsDeliveryFull.Size = new System.Drawing.Size(121, 22);
            this.txtStrIsDeliveryFull.TabIndex = 24;
            // 
            // btnUpdateRelateDO
            // 
            this.btnUpdateRelateDO.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnUpdateRelateDO.Location = new System.Drawing.Point(307, 100);
            this.btnUpdateRelateDO.Name = "btnUpdateRelateDO";
            this.btnUpdateRelateDO.Size = new System.Drawing.Size(214, 30);
            this.btnUpdateRelateDO.TabIndex = 29;
            this.btnUpdateRelateDO.Text = "Cập nhật DO các phiếu liên quan";
            this.btnUpdateRelateDO.UseVisualStyleBackColor = true;
            this.btnUpdateRelateDO.Click += new System.EventHandler(this.btnUpdateRelateDO_Click);
            // 
            // btnCorrectAdditionalDO
            // 
            this.btnCorrectAdditionalDO.Image = ((System.Drawing.Image)(resources.GetObject("btnCorrectAdditionalDO.Image")));
            this.btnCorrectAdditionalDO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCorrectAdditionalDO.Location = new System.Drawing.Point(90, 100);
            this.btnCorrectAdditionalDO.Name = "btnCorrectAdditionalDO";
            this.btnCorrectAdditionalDO.Size = new System.Drawing.Size(211, 30);
            this.btnCorrectAdditionalDO.TabIndex = 28;
            this.btnCorrectAdditionalDO.TabStop = false;
            this.btnCorrectAdditionalDO.Text = "Cập nhật DO cho DO Bổ Sung";
            this.btnCorrectAdditionalDO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCorrectAdditionalDO.UseVisualStyleBackColor = true;
            this.btnCorrectAdditionalDO.Click += new System.EventHandler(this.btnCorrectAdditionalDO_Click);
            // 
            // CompanyDefineView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 653);
            this.Controls.Add(this.btnUpdateRelateDO);
            this.Controls.Add(this.btnCorrectAdditionalDO);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtStrIsDeliveryFull);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtStrIsPrepareFull);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtStrIsSplitFull);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtStrIsExportFull);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtStatusDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnUpload);
            this.Name = "CompanyDefineView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsDetails;
        private System.Windows.Forms.Button btnUpload;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatusDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource bdsReport;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStrIsExportFull;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStrIsSplitFull;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtStrIsPrepareFull;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtStrIsDeliveryFull;
        private System.Windows.Forms.Button btnCorrectAdditionalDO;
        private System.Windows.Forms.Button btnUpdateRelateDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Device;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_MFG;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChangedQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChangeReason;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveredQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequireReturnQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExportCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplitCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrepareCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryTicketCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyCode1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEXStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSPStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPRStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDHStatus;
    }
}