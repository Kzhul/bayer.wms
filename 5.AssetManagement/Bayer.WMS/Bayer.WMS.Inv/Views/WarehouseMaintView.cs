﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.CustomControls;
using Bayer.WMS.Objs;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class WarehouseMaintView : BaseForm, IWarehouseMaintView
    {
        public WarehouseMaintView()
        {
            InitializeComponent();
        }

        public override void InitializeComboBox()
        {
            cmbStatus.ValueMember = Product.status.ValueMember;
            cmbStatus.DisplayMember = Product.status.DisplayMember;
            cmbStatus.DataSource = Product.status.Get();

            cmbWarehouseCode.DropDownWidth = 700;
            cmbWarehouseCode.PageSize = 20;
            cmbWarehouseCode.ValueMember = "WarehouseID";
            cmbWarehouseCode.DisplayMember = "WarehouseCode";
            cmbWarehouseCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("WarehouseCode", "Mã kho", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbWarehouseCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadWarehouses", "OnWarehouseCodeSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var warehouseMaintPresenter = _presenter as IWarehouseMaintPresenter;

                await Task.WhenAll(warehouseMaintPresenter.LoadWarehouses());

                if (!isRefresh)
                    warehouseMaintPresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DataTable Warehouses { set => cmbWarehouseCode.Source = value; }

        private Warehouse _warehouse;

        public Warehouse Warehouse
        {
            get
            {
                return _warehouse;
            }
            set
            {
                _warehouse = value;

                cmbWarehouseCode.DataBindings.Clear();
                txtDescription.DataBindings.Clear();
                cmbStatus.DataBindings.Clear();

                cmbWarehouseCode.DataBindings.Add("Text", Warehouse, "WarehouseCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", Warehouse, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbStatus.DataBindings.Add("SelectedValue", Warehouse, "Status", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }
    }
}
