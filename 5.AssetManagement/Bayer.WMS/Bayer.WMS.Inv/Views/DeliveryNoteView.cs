﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class DeliveryNoteView : BaseForm, IDeliveryNoteView
    {
        public DeliveryNoteView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductPacking);
            SetDoubleBuffered(dtgPalletSuggest);
            SetDoubleBuffered(dtgProductPacking);

            this.dtgProductPacking.AllowUserToOrderColumns = true;
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "ExportCode";
            cmbCode.DisplayMember = "ExportCode - Description";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("StrDeliveryDate", "Ngày giao hàng", 130),
                new MultiColumnComboBox.ComboBoxColumn("ExportCode", "Mã", 130),
                new MultiColumnComboBox.ComboBoxColumn("DOImportCode", "Mã DO", 130),                
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 130)
            };
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "GetUsers", "OnCodeSelectChange");
            cmbCode.DropDownWidth = 700;
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            InitializeComboBox();

            var doPresenter = _presenter as IDeliveryNotePresenter;
            await Task.WhenAll(doPresenter.LoadDeliveryNoteHeader());

            if (!isRefresh)
                doPresenter.Insert();
        }

        public string DOImportCode
        {
            get
            {
                return cmbCode.SelectedItem["ExportCode"].ToString();
            }
        }

        public DataTable DeliveryNoteHeaders
        {
            set { cmbCode.Source = value; }
        }

        private DeliveryNoteHeader _DeliveryNoteHeader;

        public DeliveryNoteHeader DeliveryNoteHeader
        {
            get { return _DeliveryNoteHeader; }
            set
            {
                _DeliveryNoteHeader = value;

                cmbCode.DataBindings.Clear();
                txtProductDescription.DataBindings.Clear();
                txtStatusDisplay.DataBindings.Clear();
                txtDeliveryDate.DataBindings.Clear();
                txtDOImportCode.DataBindings.Clear();

                cmbCode.DataBindings.Add("Text", DeliveryNoteHeader, "ExportCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProductDescription.DataBindings.Add("Text", DeliveryNoteHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStatusDisplay.DataBindings.Add("Text", DeliveryNoteHeader, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDeliveryDate.DataBindings.Add("Text", DeliveryNoteHeader, "StrDeliveryDate", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDOImportCode.DataBindings.Add("Text", DeliveryNoteHeader, "DOImportCode", true, DataSourceUpdateMode.OnPropertyChanged);
                
                txtStatusDisplay.ForeColor = Utility.StatusToColor(this.DeliveryNoteHeader.Status);
            }
        }

        private IList<DeliveryNoteDetail> _DeliveryNoteDetail;

        public IList<DeliveryNoteDetail> DeliveryNoteDetails
        {
            get { return _DeliveryNoteDetail; }
            set
            {
                _DeliveryNoteDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _DeliveryNoteDetail;//.ToDataTable();
                    });
                }
                else
                {
                    bdsDetails.DataSource = _DeliveryNoteDetail;//.ToDataTable();
                }
            }
        }

        private IList<DeliveryNoteDetail> _deletedDeliveryNoteDetails;

        public IList<DeliveryNoteDetail> DeletedDeliveryNoteDetails
        {
            get { return _deletedDeliveryNoteDetails; }
            set { _deletedDeliveryNoteDetails = value; }
        }

        private DataTable _ReportLocationSuggest;

        public DataTable ReportLocationSuggest
        {
            get { return _ReportLocationSuggest; }
            set
            {
                _ReportLocationSuggest = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsLocationSuggest.DataSource = _ReportLocationSuggest;
                    });
                }
                else
                {
                    bdsLocationSuggest.DataSource = _ReportLocationSuggest;
                }
            }
        }

        private DataTable _ReportPalletPick;

        public DataTable ReportPalletPick
        {
            get { return _ReportPalletPick; }
            set
            {
                _ReportPalletPick = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsPalletPick.DataSource = _ReportPalletPick;
                    });
                }
                else
                {
                    bdsPalletPick.DataSource = _ReportPalletPick;
                }
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }


        //protected BayerWMSContext _context;
        //protected SqlConnection _conn;

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool isSucess = false;
            string lastestNumber = string.Empty;
            try
            {
                using (BayerWMSContext db = new BayerWMSContext())
                {

                    if (this.DeliveryNoteDetails.Count == 0)
                    {
                        this.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                        return;
                    }

                    #region VerifyDetailQuantity

                    var listOrderDetail = db.DeliveryOrderDetails.Where(a => a.DOImportCode == this.DeliveryNoteHeader.DOImportCode).ToList();
                    var listAnotherNoteDetails = db.DeliveryNoteDetails.Where(a => a.DOCode == this.DeliveryNoteHeader.DOImportCode && a.ExportCode != this.DeliveryNoteHeader.ExportCode).ToList();

                    string productErr = string.Empty;
                    if (listOrderDetail != null && listOrderDetail.Count > 0)
                    {
                        //List<string, string> listProduct = listOrderDetail.Select(a => a.ProductCode).Distinct().ToList();
                        var listMaterialCodeBatchCode = listOrderDetail.Select(a => new { a.ProductID, a.BatchCode }).Distinct().ToList();

                        foreach (var item in listMaterialCodeBatchCode)
                        {
                            decimal totalNoteQuantity = 0;
                            decimal? totalAnotherNoteQuantity = listAnotherNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                            totalAnotherNoteQuantity = totalAnotherNoteQuantity ?? totalAnotherNoteQuantity;
                            decimal? totalThisNoteQuantity = this.DeliveryNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                            totalThisNoteQuantity = totalThisNoteQuantity ?? totalThisNoteQuantity;
                            totalNoteQuantity = totalThisNoteQuantity.Value + totalAnotherNoteQuantity.Value;

                            decimal? totalOrderQuantity = listOrderDetail.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                            totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                            if (totalNoteQuantity > totalOrderQuantity)
                            {
                                decimal? totalNoteQuantityValid = totalOrderQuantity - totalAnotherNoteQuantity;
                                var firstDetailErr = this.DeliveryNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).FirstOrDefault();
                                productErr = firstDetailErr.ProductCode + "----" + firstDetailErr.BatchCode + "----O:" + totalOrderQuantity.ToString() + "----A:" + totalAnotherNoteQuantity.ToString() + "----N:" + totalThisNoteQuantity.ToString();
                                throw new WrappedException(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(productErr))
                    {
                        return;
                    }

                    #endregion

                    #region Save DN Header and Details

                    var modelUpdate = db.DeliveryNoteHeaders.Where(a => a.ExportCode == this.DeliveryNoteHeader.ExportCode).FirstOrDefault();
                    //Insert
                    if (string.IsNullOrEmpty(this.DeliveryNoteHeader.ExportCode) || modelUpdate == null)
                    {
                        var model = db.DeliveryNoteHeaders.Where(a => a.ExportDate >= DateTime.Today.Date).OrderByDescending(a => a.ExportCode).FirstOrDefault();
                        int nextDONumber = 1;
                        if (model != null)
                        {
                            nextDONumber = int.Parse(model.ExportCode.Substring(model.ExportCode.Length - 2)) + 1;
                        }

                        lastestNumber = $"XH{DateTime.Today:yyMMdd}{nextDONumber:00}";

                        this.DeliveryNoteHeader.ExportCode = lastestNumber;
                        this.DeliveryNoteHeader.ExportDate = DateTime.Today;
                        this.DeliveryNoteHeader.Status = DeliveryNoteHeader.status.New;
                        this.DeliveryNoteHeader.IsDeleted = false;
                        this.DeliveryNoteHeader.CreatedBy = LoginInfo.UserID;
                        this.DeliveryNoteHeader.CreatedBySitemapID = LoginInfo.SitemapID;
                        this.DeliveryNoteHeader.CreatedDateTime = DateTime.Now;
                        this.DeliveryNoteHeader.UpdatedBy = LoginInfo.UserID;
                        this.DeliveryNoteHeader.UpdatedBySitemapID = LoginInfo.SitemapID;
                        this.DeliveryNoteHeader.UpdatedDateTime = DateTime.Now;

                        db.DeliveryNoteHeaders.Add(this.DeliveryNoteHeader);
                    }
                    //Update
                    else
                    {
                        modelUpdate.Description = this.DeliveryNoteHeader.Description;
                        modelUpdate.DeliveryDate = this.DeliveryNoteHeader.DeliveryDate;
                        modelUpdate.UpdatedBy = LoginInfo.UserID;
                        modelUpdate.UpdatedBySitemapID = LoginInfo.SitemapID;
                        modelUpdate.UpdatedDateTime = DateTime.Now;
                    }


                    //Detail
                    ////Delete all and insert new
                    var listDetailOld = db.DeliveryNoteDetails.Where(a => a.ExportCode == this.DeliveryNoteHeader.ExportCode).ToList();
                    foreach (DeliveryNoteDetail item in listDetailOld)
                    {
                        db.DeliveryNoteDetails.Remove(item);
                    }

                    foreach (DeliveryNoteDetail item in this.DeliveryNoteDetails)
                    {
                        item.ExportCode = this.DeliveryNoteHeader.ExportCode;
                        item.Status = DeliveryNoteHeader.status.New;
                        item.CreatedBy = LoginInfo.UserID;
                        item.CreatedBySitemapID = LoginInfo.SitemapID;
                        item.CreatedDateTime = DateTime.Now;
                        item.UpdatedBy = LoginInfo.UserID;
                        item.UpdatedBySitemapID = LoginInfo.SitemapID;
                        item.UpdatedDateTime = DateTime.Now;
                        db.DeliveryNoteDetails.Add(item);
                    }

                    db.SaveChanges();
                }
                #endregion

                #region Update DO Status
                #region Check quantity of each row

                //int isExportFull = 1;
                ////var listOrderDetail = db.DeliveryOrderDetails.Where(a => a.DOImportCode == this.DeliveryNoteHeader.DOImportCode ).ToList();
                //var listNoteDetails = db.DeliveryNoteDetails.Where(a => a.DOCode == this.DeliveryNoteHeader.DOImportCode).ToList();
                //if (listNoteDetails != null && listNoteDetails.Count > 0)
                //{
                //    //List<string, string> listProduct = listOrderDetail.Select(a => a.ProductCode).Distinct().ToList();
                //    var listMaterialCodeBatchCode = listOrderDetail.Select(a => new { a.ProductID, a.BatchCode }).Distinct().ToList();

                //    foreach (var item in listMaterialCodeBatchCode)
                //    {
                //        decimal? totalNoteQuantity = listNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                //        totalNoteQuantity = totalNoteQuantity ?? totalNoteQuantity;

                //        decimal? totalOrderQuantity = listOrderDetail.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                //        totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                //        //if not yet export full of one product, can create
                //        if (totalNoteQuantity < totalOrderQuantity)
                //        {
                //            isExportFull = 0;
                //            break;
                //        }
                //    }
                //}
                //else
                //{
                //    isExportFull = 0;
                //}

                //var doHeader = db.DeliveryOrderHeaders.Where(a => a.DOImportCode == this.DeliveryNoteHeader.DOImportCode && !a.IsDeleted).FirstOrDefault();
                //doHeader.IsExportFull = isExportFull;
                //db.SaveChanges();
                //isSucess = true; 
                #endregion

                using (BayerWMSContext db = new BayerWMSContext())
                {
                    db.Database.ExecuteSqlCommand("dbo.proc_UpdateStatusAllDocument @Code, @DOImportCode, @Type, @Status"
                    , new System.Data.SqlClient.SqlParameter("Code", this.DeliveryNoteHeader.ExportCode)
                    , new System.Data.SqlClient.SqlParameter("DOImportCode", this.DeliveryNoteHeader.DOImportCode)
                    , new System.Data.SqlClient.SqlParameter("Type", "DeliveryNote")
                    , new System.Data.SqlClient.SqlParameter("Status", DeliveryNoteHeader.status.New)
                    );
                    db.SaveChanges();
                }

                isSucess = true;
                #endregion
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                //await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    //isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                //_deliveryNoteHeaderRepository = _mainPresenter.Resolve(_deliveryNoteHeaderRepository.GetType()) as IDeliveryNoteHeaderRepository;
                //_deliveryNoteDetailRepository = _mainPresenter.Resolve(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;

                ////// if insert or update sucessfully, refresh combobox
                //if (isSuccess)
                //{
                //    await LoadDeliveryNoteHeader();

                //    _mainView.DeliveryNoteHeader = header;
                //    _mainView.DeliveryNoteDetails = details;
                //}
                if (isSucess) { MessageBox.Show("Lưu thành công " + lastestNumber + " !"); this.Close(); }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        public async void Print()
        {
            var doPresenter = _presenter as IDeliveryNotePresenter;
            await doPresenter.Print();
        }

        private async void btnConfirm_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IDeliveryNotePresenter;
            await doPresenter.Confirm();
        }

        private async void btnDownload_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IDeliveryNotePresenter;
            await doPresenter.ExportSuggestion();
        }

        private async void btnConfirm_Click_1(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IDeliveryNotePresenter;
            await doPresenter.WarehouseManagerConfirm();
        }

        private async void btnImportPalletPicking_Click(object sender, EventArgs e)
        {
            btnImportPalletPicking.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        await(_presenter as IDeliveryNotePresenter).ImportExcel(ofd.FileName);
                    }
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnImportPalletPicking.Enabled = true;
            }
        }

        private async void dtgPalletSuggest_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4)
                {
                    string palletCode = dtgPalletSuggest["colPalletCodeSuggest", e.RowIndex].Value.ToString();
                    await (_presenter as IDeliveryNotePresenter).SavePalletSuggestion(palletCode);
                    this.SetMessage("Xác nhận rớt pallet: " + palletCode, Utility.MessageType.Information);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {

            }
        }

        private async void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3)
                {
                    string palletCode = dataGridView1["dataGridViewTextBoxColumn12", e.RowIndex].Value.ToString();
                    await (_presenter as IDeliveryNotePresenter).DeletePalletSuggestion(palletCode);
                    this.SetMessage("Bỏ xác nhận rớt pallet: " + palletCode, Utility.MessageType.Information);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {

            }
        }

        private async void btnSearchSugestion_Click(object sender, EventArgs e)
        {
            SearchSugestion();
        }

        private async void SearchSugestion()
        {
            var doPresenter = _presenter as IDeliveryNotePresenter;
            await doPresenter.LoadReportLocationSuggest(txtSugestionSearch.Text);
        }

        private void txtSugestionSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                SearchSugestion();
            }
        }
    }
}