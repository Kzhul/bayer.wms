﻿namespace Bayer.WMS.Inv.Views
{
    partial class PrepareNoteView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.colProductionPlan_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUserPrepare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUserVerify = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPreparedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCodes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveredQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequireReturnQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReturnedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatusDisplay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDeliveryDate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDOImportCode = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCompanyCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProvince = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUserCreateFullName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUserWareHouse = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUserReview = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToAddRows = false;
            this.dtgProductPacking.AllowUserToDeleteRows = false;
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductPacking.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductionPlan_ProductDescription,
            this.colProductionPlan_Quantity,
            this.colProductionPlan_PackageSize,
            this.colProductionPlan_PackageQuantity,
            this.colBatchCodeDistributor,
            this.PackQuantity,
            this.colPackType,
            this.colPackSize,
            this.colPackWeight,
            this.ProductQuantity,
            this.colProductUnit,
            this.colUserPrepare,
            this.colUserVerify,
            this.colPreparedQty,
            this.colPalletCodes,
            this.colDeliveredQty,
            this.colRequireReturnQty,
            this.colReturnedQty});
            this.dtgProductPacking.DataSource = this.bdsDetails;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(14, 239);
            this.dtgProductPacking.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.ReadOnly = true;
            this.dtgProductPacking.Size = new System.Drawing.Size(1431, 562);
            this.dtgProductPacking.TabIndex = 8;
            // 
            // colProductionPlan_ProductDescription
            // 
            this.colProductionPlan_ProductDescription.DataPropertyName = "ProductCode";
            this.colProductionPlan_ProductDescription.HeaderText = "Mã Sản Phẩm";
            this.colProductionPlan_ProductDescription.Name = "colProductionPlan_ProductDescription";
            this.colProductionPlan_ProductDescription.ReadOnly = true;
            // 
            // colProductionPlan_Quantity
            // 
            this.colProductionPlan_Quantity.DataPropertyName = "ProductDescription";
            dataGridViewCellStyle2.NullValue = null;
            this.colProductionPlan_Quantity.DefaultCellStyle = dataGridViewCellStyle2;
            this.colProductionPlan_Quantity.HeaderText = "Mô Tả";
            this.colProductionPlan_Quantity.Name = "colProductionPlan_Quantity";
            this.colProductionPlan_Quantity.ReadOnly = true;
            this.colProductionPlan_Quantity.Width = 200;
            // 
            // colProductionPlan_PackageSize
            // 
            this.colProductionPlan_PackageSize.DataPropertyName = "BatchCode";
            dataGridViewCellStyle3.NullValue = null;
            this.colProductionPlan_PackageSize.DefaultCellStyle = dataGridViewCellStyle3;
            this.colProductionPlan_PackageSize.HeaderText = "Số Lô";
            this.colProductionPlan_PackageSize.Name = "colProductionPlan_PackageSize";
            this.colProductionPlan_PackageSize.ReadOnly = true;
            // 
            // colProductionPlan_PackageQuantity
            // 
            this.colProductionPlan_PackageQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            this.colProductionPlan_PackageQuantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductionPlan_PackageQuantity.HeaderText = "Số lượng";
            this.colProductionPlan_PackageQuantity.Name = "colProductionPlan_PackageQuantity";
            this.colProductionPlan_PackageQuantity.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "Số Lô NCC";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            this.colBatchCodeDistributor.ReadOnly = true;
            this.colBatchCodeDistributor.Visible = false;
            // 
            // PackQuantity
            // 
            this.PackQuantity.DataPropertyName = "PackQuantity";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.PackQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.PackQuantity.HeaderText = "Thùng chẵn";
            this.PackQuantity.Name = "PackQuantity";
            this.PackQuantity.ReadOnly = true;
            // 
            // colPackType
            // 
            this.colPackType.DataPropertyName = "StrPackType";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colPackType.DefaultCellStyle = dataGridViewCellStyle6;
            this.colPackType.HeaderText = "Loại Thùng";
            this.colPackType.Name = "colPackType";
            this.colPackType.ReadOnly = true;
            // 
            // colPackSize
            // 
            this.colPackSize.DataPropertyName = "PackSize";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.NullValue = null;
            this.colPackSize.DefaultCellStyle = dataGridViewCellStyle7;
            this.colPackSize.HeaderText = "Quy cách thùng chẵn";
            this.colPackSize.Name = "colPackSize";
            this.colPackSize.ReadOnly = true;
            // 
            // colPackWeight
            // 
            this.colPackWeight.DataPropertyName = "PackWeight";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = null;
            this.colPackWeight.DefaultCellStyle = dataGridViewCellStyle8;
            this.colPackWeight.HeaderText = "Trọng lượng thùng";
            this.colPackWeight.Name = "colPackWeight";
            this.colPackWeight.ReadOnly = true;
            // 
            // ProductQuantity
            // 
            this.ProductQuantity.DataPropertyName = "ProductQuantity";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = null;
            this.ProductQuantity.DefaultCellStyle = dataGridViewCellStyle9;
            this.ProductQuantity.HeaderText = "Số lượng lẻ";
            this.ProductQuantity.Name = "ProductQuantity";
            this.ProductQuantity.ReadOnly = true;
            // 
            // colProductUnit
            // 
            this.colProductUnit.DataPropertyName = "ProductUnit";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colProductUnit.DefaultCellStyle = dataGridViewCellStyle10;
            this.colProductUnit.HeaderText = "Đơn vị tính";
            this.colProductUnit.Name = "colProductUnit";
            this.colProductUnit.ReadOnly = true;
            // 
            // colUserPrepare
            // 
            this.colUserPrepare.DataPropertyName = "StrUserPrepare";
            this.colUserPrepare.HeaderText = "Tên người soạn";
            this.colUserPrepare.Name = "colUserPrepare";
            this.colUserPrepare.ReadOnly = true;
            // 
            // colUserVerify
            // 
            this.colUserVerify.DataPropertyName = "UserVerify";
            this.colUserVerify.HeaderText = "Tên người kiểm";
            this.colUserVerify.Name = "colUserVerify";
            this.colUserVerify.ReadOnly = true;
            // 
            // colPreparedQty
            // 
            this.colPreparedQty.DataPropertyName = "PreparedQty";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N0";
            dataGridViewCellStyle11.NullValue = null;
            this.colPreparedQty.DefaultCellStyle = dataGridViewCellStyle11;
            this.colPreparedQty.HeaderText = "SL đã soạn";
            this.colPreparedQty.Name = "colPreparedQty";
            this.colPreparedQty.ReadOnly = true;
            // 
            // colPalletCodes
            // 
            this.colPalletCodes.DataPropertyName = "PalletCodes";
            this.colPalletCodes.HeaderText = "Pallet";
            this.colPalletCodes.Name = "colPalletCodes";
            this.colPalletCodes.ReadOnly = true;
            // 
            // colDeliveredQty
            // 
            this.colDeliveredQty.DataPropertyName = "DeliveredQty";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N0";
            dataGridViewCellStyle12.NullValue = null;
            this.colDeliveredQty.DefaultCellStyle = dataGridViewCellStyle12;
            this.colDeliveredQty.HeaderText = "SL đã giao";
            this.colDeliveredQty.Name = "colDeliveredQty";
            this.colDeliveredQty.ReadOnly = true;
            // 
            // colRequireReturnQty
            // 
            this.colRequireReturnQty.DataPropertyName = "RequireReturnQty";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N0";
            dataGridViewCellStyle13.NullValue = null;
            this.colRequireReturnQty.DefaultCellStyle = dataGridViewCellStyle13;
            this.colRequireReturnQty.HeaderText = "SL yêu cầu trả";
            this.colRequireReturnQty.Name = "colRequireReturnQty";
            this.colRequireReturnQty.ReadOnly = true;
            // 
            // colReturnedQty
            // 
            this.colReturnedQty.DataPropertyName = "ReturnedQty";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N0";
            dataGridViewCellStyle14.NullValue = null;
            this.colReturnedQty.DefaultCellStyle = dataGridViewCellStyle14;
            this.colReturnedQty.HeaderText = "SL đã trả";
            this.colReturnedQty.Name = "colReturnedQty";
            this.colReturnedQty.ReadOnly = true;
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(156, 15);
            this.cmbCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(450, 28);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 12;
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(156, 159);
            this.txtProductDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtProductDescription.MaxLength = 255;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(450, 26);
            this.txtProductDescription.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 162);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "Mô tả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã:";
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.Location = new System.Drawing.Point(817, 85);
            this.txtStatusDisplay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatusDisplay.MaxLength = 255;
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.ReadOnly = true;
            this.txtStatusDisplay.Size = new System.Drawing.Size(223, 26);
            this.txtStatusDisplay.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(675, 89);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "Trạng thái:";
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Location = new System.Drawing.Point(817, 48);
            this.txtDeliveryDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDeliveryDate.MaxLength = 255;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.ReadOnly = true;
            this.txtDeliveryDate.Size = new System.Drawing.Size(223, 26);
            this.txtDeliveryDate.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(675, 50);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 20);
            this.label7.TabIndex = 25;
            this.label7.Text = "Ngày giao hàng:";
            // 
            // txtDOImportCode
            // 
            this.txtDOImportCode.Location = new System.Drawing.Point(817, 11);
            this.txtDOImportCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDOImportCode.MaxLength = 255;
            this.txtDOImportCode.Name = "txtDOImportCode";
            this.txtDOImportCode.ReadOnly = true;
            this.txtDOImportCode.Size = new System.Drawing.Size(223, 26);
            this.txtDOImportCode.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(675, 15);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 20);
            this.label8.TabIndex = 33;
            this.label8.Text = "Mã LO:";
            // 
            // txtCompanyCode
            // 
            this.txtCompanyCode.Location = new System.Drawing.Point(156, 54);
            this.txtCompanyCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyCode.MaxLength = 255;
            this.txtCompanyCode.Name = "txtCompanyCode";
            this.txtCompanyCode.ReadOnly = true;
            this.txtCompanyCode.Size = new System.Drawing.Size(450, 26);
            this.txtCompanyCode.TabIndex = 36;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 58);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 20);
            this.label4.TabIndex = 37;
            this.label4.Text = "Mã khách hàng:";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(156, 89);
            this.txtCompanyName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyName.MaxLength = 255;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.ReadOnly = true;
            this.txtCompanyName.Size = new System.Drawing.Size(450, 26);
            this.txtCompanyName.TabIndex = 38;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 92);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 20);
            this.label5.TabIndex = 39;
            this.label5.Text = "Tên khách hàng:";
            // 
            // txtProvince
            // 
            this.txtProvince.Location = new System.Drawing.Point(156, 124);
            this.txtProvince.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtProvince.MaxLength = 255;
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.ReadOnly = true;
            this.txtProvince.Size = new System.Drawing.Size(450, 26);
            this.txtProvince.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 128);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 20);
            this.label6.TabIndex = 41;
            this.label6.Text = "Tỉnh thành:";
            // 
            // txtUserCreateFullName
            // 
            this.txtUserCreateFullName.Location = new System.Drawing.Point(156, 194);
            this.txtUserCreateFullName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUserCreateFullName.MaxLength = 255;
            this.txtUserCreateFullName.Name = "txtUserCreateFullName";
            this.txtUserCreateFullName.ReadOnly = true;
            this.txtUserCreateFullName.Size = new System.Drawing.Size(450, 26);
            this.txtUserCreateFullName.TabIndex = 42;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 198);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 20);
            this.label9.TabIndex = 43;
            this.label9.Text = "Người lập phiếu:";
            // 
            // txtUserWareHouse
            // 
            this.txtUserWareHouse.Location = new System.Drawing.Point(817, 120);
            this.txtUserWareHouse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUserWareHouse.MaxLength = 255;
            this.txtUserWareHouse.Name = "txtUserWareHouse";
            this.txtUserWareHouse.ReadOnly = true;
            this.txtUserWareHouse.Size = new System.Drawing.Size(223, 26);
            this.txtUserWareHouse.TabIndex = 44;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(675, 124);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 20);
            this.label10.TabIndex = 45;
            this.label10.Text = "Nhân viên kho:";
            // 
            // txtUserReview
            // 
            this.txtUserReview.Location = new System.Drawing.Point(817, 159);
            this.txtUserReview.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUserReview.MaxLength = 255;
            this.txtUserReview.Name = "txtUserReview";
            this.txtUserReview.Size = new System.Drawing.Size(223, 26);
            this.txtUserReview.TabIndex = 46;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(675, 162);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 20);
            this.label11.TabIndex = 47;
            this.label11.Text = "Người kiểm:";
            // 
            // PrepareNoteView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1458, 816);
            this.Controls.Add(this.txtUserReview);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtUserWareHouse);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtUserCreateFullName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtProvince);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCompanyName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCompanyCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDOImportCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDeliveryDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtStatusDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtgProductPacking);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "PrepareNoteView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.BindingSource bdsDetails;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatusDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDeliveryDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDOImportCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCompanyCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtProvince;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUserCreateFullName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtUserWareHouse;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtUserReview;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserPrepare;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserVerify;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPreparedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCodes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveredQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequireReturnQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReturnedQty;
    }
}