﻿namespace Bayer.WMS.Inv.Views
{
    partial class DeliveryTicketView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgProduct = new System.Windows.Forms.DataGridView();
            this.colProductionPlan_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductCase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUserPrepare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUserVerify = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCodes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveredQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtDeliveryAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatusDisplay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDeliveryDate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCompanyCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProvince = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTruckNo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDriver = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbTabProduct = new System.Windows.Forms.TabPage();
            this.tbPrepareNote = new System.Windows.Forms.TabPage();
            this.dtgPrepareNotes = new System.Windows.Forms.DataGridView();
            this.bdsPrepareNotes = new System.Windows.Forms.BindingSource(this.components);
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtGrossWeight = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtListOrderNumbers = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtListFreeItems = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTotalBoxes = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTotalBags = new System.Windows.Forms.TextBox();
            this.txtTotalBuckets = new System.Windows.Forms.TextBox();
            this.txtTotalCans = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtReceiptBy = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDeliveverSign = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtFreeItemsWeight = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.colPrepareCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrepareDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOImportCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProvince = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBlank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tbTabProduct.SuspendLayout();
            this.tbPrepareNote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPrepareNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPrepareNotes)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgProduct
            // 
            this.dtgProduct.AllowUserToAddRows = false;
            this.dtgProduct.AllowUserToDeleteRows = false;
            this.dtgProduct.AllowUserToOrderColumns = true;
            this.dtgProduct.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProduct.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProduct.AutoGenerateColumns = false;
            this.dtgProduct.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProduct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductionPlan_ProductDescription,
            this.colProductionPlan_Quantity,
            this.colProductionPlan_PackageSize,
            this.colProductionPlan_PackageQuantity,
            this.colBatchCodeDistributor,
            this.PackQuantity,
            this.colPackType,
            this.colPackSize,
            this.colPackWeight,
            this.ProductQuantity,
            this.colProductCase,
            this.colProductUnit,
            this.colProductWeight,
            this.colUserPrepare,
            this.colUserVerify,
            this.colPalletCodes,
            this.colDeliveredQty});
            this.dtgProduct.DataSource = this.bdsDetails;
            this.dtgProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgProduct.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProduct.Location = new System.Drawing.Point(3, 4);
            this.dtgProduct.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtgProduct.Name = "dtgProduct";
            this.dtgProduct.ReadOnly = true;
            this.dtgProduct.Size = new System.Drawing.Size(1260, 399);
            this.dtgProduct.TabIndex = 0;
            // 
            // colProductionPlan_ProductDescription
            // 
            this.colProductionPlan_ProductDescription.DataPropertyName = "ProductCode";
            this.colProductionPlan_ProductDescription.HeaderText = "Mã Sản Phẩm";
            this.colProductionPlan_ProductDescription.Name = "colProductionPlan_ProductDescription";
            this.colProductionPlan_ProductDescription.ReadOnly = true;
            // 
            // colProductionPlan_Quantity
            // 
            this.colProductionPlan_Quantity.DataPropertyName = "ProductDescription";
            dataGridViewCellStyle2.NullValue = null;
            this.colProductionPlan_Quantity.DefaultCellStyle = dataGridViewCellStyle2;
            this.colProductionPlan_Quantity.HeaderText = "Mô Tả";
            this.colProductionPlan_Quantity.Name = "colProductionPlan_Quantity";
            this.colProductionPlan_Quantity.ReadOnly = true;
            this.colProductionPlan_Quantity.Width = 200;
            // 
            // colProductionPlan_PackageSize
            // 
            this.colProductionPlan_PackageSize.DataPropertyName = "BatchCode";
            dataGridViewCellStyle3.NullValue = null;
            this.colProductionPlan_PackageSize.DefaultCellStyle = dataGridViewCellStyle3;
            this.colProductionPlan_PackageSize.HeaderText = "Số Lô";
            this.colProductionPlan_PackageSize.Name = "colProductionPlan_PackageSize";
            this.colProductionPlan_PackageSize.ReadOnly = true;
            // 
            // colProductionPlan_PackageQuantity
            // 
            this.colProductionPlan_PackageQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            this.colProductionPlan_PackageQuantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductionPlan_PackageQuantity.HeaderText = "Số lượng";
            this.colProductionPlan_PackageQuantity.Name = "colProductionPlan_PackageQuantity";
            this.colProductionPlan_PackageQuantity.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "Số Lô NCC";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            this.colBatchCodeDistributor.ReadOnly = true;
            this.colBatchCodeDistributor.Visible = false;
            // 
            // PackQuantity
            // 
            this.PackQuantity.DataPropertyName = "PackQuantity";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.PackQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.PackQuantity.HeaderText = "Thùng chẵn";
            this.PackQuantity.Name = "PackQuantity";
            this.PackQuantity.ReadOnly = true;
            // 
            // colPackType
            // 
            this.colPackType.DataPropertyName = "StrPackType";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colPackType.DefaultCellStyle = dataGridViewCellStyle6;
            this.colPackType.HeaderText = "Loại";
            this.colPackType.Name = "colPackType";
            this.colPackType.ReadOnly = true;
            // 
            // colPackSize
            // 
            this.colPackSize.DataPropertyName = "PackSize";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.NullValue = null;
            this.colPackSize.DefaultCellStyle = dataGridViewCellStyle7;
            this.colPackSize.HeaderText = "Quy cách thùng chẵn";
            this.colPackSize.Name = "colPackSize";
            this.colPackSize.ReadOnly = true;
            // 
            // colPackWeight
            // 
            this.colPackWeight.DataPropertyName = "PackWeight";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this.colPackWeight.DefaultCellStyle = dataGridViewCellStyle8;
            this.colPackWeight.HeaderText = "Trọng lượng thùng";
            this.colPackWeight.Name = "colPackWeight";
            this.colPackWeight.ReadOnly = true;
            // 
            // ProductQuantity
            // 
            this.ProductQuantity.DataPropertyName = "ProductQuantity";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = null;
            this.ProductQuantity.DefaultCellStyle = dataGridViewCellStyle9;
            this.ProductQuantity.HeaderText = "Số lượng lẻ";
            this.ProductQuantity.Name = "ProductQuantity";
            this.ProductQuantity.ReadOnly = true;
            // 
            // colProductCase
            // 
            this.colProductCase.DataPropertyName = "ProductCase";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N0";
            dataGridViewCellStyle10.NullValue = null;
            this.colProductCase.DefaultCellStyle = dataGridViewCellStyle10;
            this.colProductCase.HeaderText = "Số lượng thùng lẻ";
            this.colProductCase.Name = "colProductCase";
            this.colProductCase.ReadOnly = true;
            // 
            // colProductUnit
            // 
            this.colProductUnit.DataPropertyName = "ProductUnit";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colProductUnit.DefaultCellStyle = dataGridViewCellStyle11;
            this.colProductUnit.HeaderText = "Đơn vị tính";
            this.colProductUnit.Name = "colProductUnit";
            this.colProductUnit.ReadOnly = true;
            // 
            // colProductWeight
            // 
            this.colProductWeight.DataPropertyName = "ProductWeight";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N2";
            dataGridViewCellStyle12.NullValue = null;
            this.colProductWeight.DefaultCellStyle = dataGridViewCellStyle12;
            this.colProductWeight.HeaderText = "Trọng lượng thùng lẻ";
            this.colProductWeight.Name = "colProductWeight";
            this.colProductWeight.ReadOnly = true;
            // 
            // colUserPrepare
            // 
            this.colUserPrepare.DataPropertyName = "StrUserPrepare";
            this.colUserPrepare.HeaderText = "Tên người soạn";
            this.colUserPrepare.Name = "colUserPrepare";
            this.colUserPrepare.ReadOnly = true;
            // 
            // colUserVerify
            // 
            this.colUserVerify.DataPropertyName = "UserVerify";
            this.colUserVerify.HeaderText = "Tên người kiểm";
            this.colUserVerify.Name = "colUserVerify";
            this.colUserVerify.ReadOnly = true;
            // 
            // colPalletCodes
            // 
            this.colPalletCodes.DataPropertyName = "PalletCodes";
            this.colPalletCodes.HeaderText = "Pallet";
            this.colPalletCodes.Name = "colPalletCodes";
            this.colPalletCodes.ReadOnly = true;
            // 
            // colDeliveredQty
            // 
            this.colDeliveredQty.DataPropertyName = "DeliveredQty";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N0";
            this.colDeliveredQty.DefaultCellStyle = dataGridViewCellStyle13;
            this.colDeliveredQty.HeaderText = "SL đã giao";
            this.colDeliveredQty.Name = "colDeliveredQty";
            this.colDeliveredQty.ReadOnly = true;
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(156, 15);
            this.cmbCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(450, 28);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 0;
            // 
            // txtDeliveryAddress
            // 
            this.txtDeliveryAddress.Location = new System.Drawing.Point(156, 159);
            this.txtDeliveryAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDeliveryAddress.MaxLength = 255;
            this.txtDeliveryAddress.Name = "txtDeliveryAddress";
            this.txtDeliveryAddress.Size = new System.Drawing.Size(450, 26);
            this.txtDeliveryAddress.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 159);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Địa chỉ giao hàng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã:";
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.Location = new System.Drawing.Point(813, 54);
            this.txtStatusDisplay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatusDisplay.MaxLength = 255;
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.ReadOnly = true;
            this.txtStatusDisplay.Size = new System.Drawing.Size(223, 26);
            this.txtStatusDisplay.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(652, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Trạng thái:";
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Location = new System.Drawing.Point(813, 15);
            this.txtDeliveryDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDeliveryDate.MaxLength = 255;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.ReadOnly = true;
            this.txtDeliveryDate.Size = new System.Drawing.Size(223, 26);
            this.txtDeliveryDate.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(652, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Ngày giao hàng:";
            // 
            // txtCompanyCode
            // 
            this.txtCompanyCode.Location = new System.Drawing.Point(156, 54);
            this.txtCompanyCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyCode.MaxLength = 255;
            this.txtCompanyCode.Name = "txtCompanyCode";
            this.txtCompanyCode.ReadOnly = true;
            this.txtCompanyCode.Size = new System.Drawing.Size(450, 26);
            this.txtCompanyCode.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 54);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã khách hàng:";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(156, 89);
            this.txtCompanyName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyName.MaxLength = 255;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.ReadOnly = true;
            this.txtCompanyName.Size = new System.Drawing.Size(450, 26);
            this.txtCompanyName.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 89);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Tên khách hàng:";
            // 
            // txtProvince
            // 
            this.txtProvince.Location = new System.Drawing.Point(156, 124);
            this.txtProvince.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtProvince.MaxLength = 255;
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.ReadOnly = true;
            this.txtProvince.Size = new System.Drawing.Size(450, 26);
            this.txtProvince.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 124);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tỉnh thành:";
            // 
            // txtTruckNo
            // 
            this.txtTruckNo.Location = new System.Drawing.Point(813, 159);
            this.txtTruckNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTruckNo.MaxLength = 255;
            this.txtTruckNo.Name = "txtTruckNo";
            this.txtTruckNo.Size = new System.Drawing.Size(223, 26);
            this.txtTruckNo.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(652, 155);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "Số xe:";
            // 
            // txtDriver
            // 
            this.txtDriver.Location = new System.Drawing.Point(813, 194);
            this.txtDriver.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDriver.MaxLength = 255;
            this.txtDriver.Name = "txtDriver";
            this.txtDriver.Size = new System.Drawing.Size(223, 26);
            this.txtDriver.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(652, 190);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "Tài xế:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tbTabProduct);
            this.tabControl1.Controls.Add(this.tbPrepareNote);
            this.tabControl1.Location = new System.Drawing.Point(14, 334);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1274, 440);
            this.tabControl1.TabIndex = 0;
            // 
            // tbTabProduct
            // 
            this.tbTabProduct.AutoScroll = true;
            this.tbTabProduct.Controls.Add(this.dtgProduct);
            this.tbTabProduct.Location = new System.Drawing.Point(4, 29);
            this.tbTabProduct.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTabProduct.Name = "tbTabProduct";
            this.tbTabProduct.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbTabProduct.Size = new System.Drawing.Size(1266, 407);
            this.tbTabProduct.TabIndex = 0;
            this.tbTabProduct.Text = "Danh sách sản phẩm";
            this.tbTabProduct.UseVisualStyleBackColor = true;
            // 
            // tbPrepareNote
            // 
            this.tbPrepareNote.AutoScroll = true;
            this.tbPrepareNote.Controls.Add(this.dtgPrepareNotes);
            this.tbPrepareNote.Location = new System.Drawing.Point(4, 29);
            this.tbPrepareNote.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPrepareNote.Name = "tbPrepareNote";
            this.tbPrepareNote.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPrepareNote.Size = new System.Drawing.Size(1266, 407);
            this.tbPrepareNote.TabIndex = 0;
            this.tbPrepareNote.Text = "Danh sách phiếu soạn hàng";
            this.tbPrepareNote.UseVisualStyleBackColor = true;
            // 
            // dtgPrepareNotes
            // 
            this.dtgPrepareNotes.AllowUserToAddRows = false;
            this.dtgPrepareNotes.AllowUserToDeleteRows = false;
            this.dtgPrepareNotes.AllowUserToOrderColumns = true;
            this.dtgPrepareNotes.AllowUserToResizeRows = false;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.Azure;
            this.dtgPrepareNotes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle14;
            this.dtgPrepareNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPrepareNotes.AutoGenerateColumns = false;
            this.dtgPrepareNotes.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPrepareNotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPrepareNotes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPrepareCode,
            this.colPrepareDate,
            this.colDOImportCode,
            this.colDescription,
            this.colDeliveryDate,
            this.colCompanyCode,
            this.colCompanyName,
            this.colProvince,
            this.colBlank});
            this.dtgPrepareNotes.DataSource = this.bdsPrepareNotes;
            this.dtgPrepareNotes.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPrepareNotes.Location = new System.Drawing.Point(3, 8);
            this.dtgPrepareNotes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtgPrepareNotes.Name = "dtgPrepareNotes";
            this.dtgPrepareNotes.ReadOnly = true;
            this.dtgPrepareNotes.Size = new System.Drawing.Size(1258, 392);
            this.dtgPrepareNotes.TabIndex = 0;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(156, 299);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(880, 26);
            this.txtDescription.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 299);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "Ghi chú:";
            // 
            // txtGrossWeight
            // 
            this.txtGrossWeight.Location = new System.Drawing.Point(813, 89);
            this.txtGrossWeight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGrossWeight.MaxLength = 255;
            this.txtGrossWeight.Name = "txtGrossWeight";
            this.txtGrossWeight.Size = new System.Drawing.Size(223, 26);
            this.txtGrossWeight.TabIndex = 0;
            this.txtGrossWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(652, 85);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tổng trọng lượng:";
            // 
            // txtListOrderNumbers
            // 
            this.txtListOrderNumbers.Location = new System.Drawing.Point(156, 194);
            this.txtListOrderNumbers.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtListOrderNumbers.MaxLength = 255;
            this.txtListOrderNumbers.Name = "txtListOrderNumbers";
            this.txtListOrderNumbers.Size = new System.Drawing.Size(450, 26);
            this.txtListOrderNumbers.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 194);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "Số hóa đơn:";
            // 
            // txtListFreeItems
            // 
            this.txtListFreeItems.Location = new System.Drawing.Point(156, 229);
            this.txtListFreeItems.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtListFreeItems.MaxLength = 255;
            this.txtListFreeItems.Name = "txtListFreeItems";
            this.txtListFreeItems.Size = new System.Drawing.Size(450, 26);
            this.txtListFreeItems.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 264);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(119, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "Tổng số lượng:";
            // 
            // txtTotalBoxes
            // 
            this.txtTotalBoxes.Location = new System.Drawing.Point(156, 264);
            this.txtTotalBoxes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalBoxes.MaxLength = 255;
            this.txtTotalBoxes.Name = "txtTotalBoxes";
            this.txtTotalBoxes.Size = new System.Drawing.Size(39, 26);
            this.txtTotalBoxes.TabIndex = 0;
            this.txtTotalBoxes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalBoxes.WordWrap = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 229);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(143, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "Hàng khuyến mãi:";
            // 
            // txtTotalBags
            // 
            this.txtTotalBags.Location = new System.Drawing.Point(246, 264);
            this.txtTotalBags.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalBags.MaxLength = 255;
            this.txtTotalBags.Name = "txtTotalBags";
            this.txtTotalBags.Size = new System.Drawing.Size(39, 26);
            this.txtTotalBags.TabIndex = 0;
            this.txtTotalBags.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalBags.WordWrap = false;
            // 
            // txtTotalBuckets
            // 
            this.txtTotalBuckets.Location = new System.Drawing.Point(321, 264);
            this.txtTotalBuckets.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalBuckets.MaxLength = 255;
            this.txtTotalBuckets.Name = "txtTotalBuckets";
            this.txtTotalBuckets.Size = new System.Drawing.Size(39, 26);
            this.txtTotalBuckets.TabIndex = 0;
            this.txtTotalBuckets.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalBuckets.WordWrap = false;
            // 
            // txtTotalCans
            // 
            this.txtTotalCans.Location = new System.Drawing.Point(392, 264);
            this.txtTotalCans.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalCans.MaxLength = 255;
            this.txtTotalCans.Name = "txtTotalCans";
            this.txtTotalCans.Size = new System.Drawing.Size(39, 26);
            this.txtTotalCans.TabIndex = 0;
            this.txtTotalCans.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalCans.WordWrap = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(196, 268);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "Thùng";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(285, 268);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "Bao";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(362, 268);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Xô";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(431, 268);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 20);
            this.label19.TabIndex = 0;
            this.label19.Text = "Can";
            // 
            // txtReceiptBy
            // 
            this.txtReceiptBy.Location = new System.Drawing.Point(813, 231);
            this.txtReceiptBy.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtReceiptBy.MaxLength = 255;
            this.txtReceiptBy.Name = "txtReceiptBy";
            this.txtReceiptBy.Size = new System.Drawing.Size(223, 26);
            this.txtReceiptBy.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(652, 231);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Người nhận hàng:";
            // 
            // txtDeliveverSign
            // 
            this.txtDeliveverSign.Location = new System.Drawing.Point(813, 266);
            this.txtDeliveverSign.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDeliveverSign.MaxLength = 255;
            this.txtDeliveverSign.Name = "txtDeliveverSign";
            this.txtDeliveverSign.Size = new System.Drawing.Size(223, 26);
            this.txtDeliveverSign.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(652, 266);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(134, 20);
            this.label20.TabIndex = 0;
            this.label20.Text = "Người giao hàng:";
            // 
            // txtFreeItemsWeight
            // 
            this.txtFreeItemsWeight.Location = new System.Drawing.Point(813, 124);
            this.txtFreeItemsWeight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFreeItemsWeight.MaxLength = 255;
            this.txtFreeItemsWeight.Name = "txtFreeItemsWeight";
            this.txtFreeItemsWeight.Size = new System.Drawing.Size(223, 26);
            this.txtFreeItemsWeight.TabIndex = 0;
            this.txtFreeItemsWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFreeItemsWeight.WordWrap = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(652, 124);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(173, 20);
            this.label21.TabIndex = 0;
            this.label21.Text = "Trọng lượng hàng KM:";
            // 
            // colPrepareCode
            // 
            this.colPrepareCode.DataPropertyName = "PrepareCode";
            this.colPrepareCode.HeaderText = "Phiếu Soạn Hàng";
            this.colPrepareCode.Name = "colPrepareCode";
            this.colPrepareCode.ReadOnly = true;
            // 
            // colPrepareDate
            // 
            this.colPrepareDate.DataPropertyName = "StrPrepareDate";
            dataGridViewCellStyle15.NullValue = null;
            this.colPrepareDate.DefaultCellStyle = dataGridViewCellStyle15;
            this.colPrepareDate.HeaderText = "Ngày soạn hàng";
            this.colPrepareDate.Name = "colPrepareDate";
            this.colPrepareDate.ReadOnly = true;
            // 
            // colDOImportCode
            // 
            this.colDOImportCode.DataPropertyName = "DOImportCode";
            dataGridViewCellStyle16.NullValue = null;
            this.colDOImportCode.DefaultCellStyle = dataGridViewCellStyle16;
            this.colDOImportCode.HeaderText = "Mã phiếu LO";
            this.colDOImportCode.Name = "colDOImportCode";
            this.colDOImportCode.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "Description";
            this.colDescription.HeaderText = "Ghi chú";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            this.colDescription.Width = 200;
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.DataPropertyName = "StrDeliveryDate";
            this.colDeliveryDate.HeaderText = "Ngày giao hàng";
            this.colDeliveryDate.Name = "colDeliveryDate";
            this.colDeliveryDate.ReadOnly = true;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.DataPropertyName = "CompanyCode";
            this.colCompanyCode.HeaderText = "Mã Công Ty";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.ReadOnly = true;
            // 
            // colCompanyName
            // 
            this.colCompanyName.DataPropertyName = "CompanyName";
            this.colCompanyName.HeaderText = "Tên Công Ty";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.ReadOnly = true;
            this.colCompanyName.Width = 200;
            // 
            // colProvince
            // 
            this.colProvince.DataPropertyName = "Province";
            this.colProvince.HeaderText = "Tỉnh Thành";
            this.colProvince.Name = "colProvince";
            this.colProvince.ReadOnly = true;
            // 
            // colBlank
            // 
            this.colBlank.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.RoyalBlue;
            this.colBlank.DefaultCellStyle = dataGridViewCellStyle17;
            this.colBlank.HeaderText = "Xem phiếu";
            this.colBlank.Name = "colBlank";
            this.colBlank.ReadOnly = true;
            // 
            // DeliveryTicketView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 789);
            this.Controls.Add(this.txtFreeItemsWeight);
            this.Controls.Add(this.txtDeliveverSign);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtReceiptBy);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtTotalCans);
            this.Controls.Add(this.txtTotalBuckets);
            this.Controls.Add(this.txtTotalBags);
            this.Controls.Add(this.txtTotalBoxes);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtListFreeItems);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtListOrderNumbers);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtGrossWeight);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtDriver);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtTruckNo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtProvince);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCompanyName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCompanyCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDeliveryDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtStatusDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Controls.Add(this.txtDeliveryAddress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label21);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "DeliveryTicketView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tbTabProduct.ResumeLayout(false);
            this.tbPrepareNote.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPrepareNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPrepareNotes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgProduct;
        private System.Windows.Forms.BindingSource bdsDetails;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.TextBox txtDeliveryAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatusDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDeliveryDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCompanyCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtProvince;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTruckNo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDriver;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbTabProduct;
        private System.Windows.Forms.TabPage tbPrepareNote;
        private System.Windows.Forms.DataGridView dtgPrepareNotes;
        private System.Windows.Forms.BindingSource bdsPrepareNotes;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtGrossWeight;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtListOrderNumbers;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtListFreeItems;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTotalBoxes;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTotalBags;
        private System.Windows.Forms.TextBox txtTotalBuckets;
        private System.Windows.Forms.TextBox txtTotalCans;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtReceiptBy;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDeliveverSign;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtFreeItemsWeight;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductCase;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserPrepare;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserVerify;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCodes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveredQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrepareCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrepareDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOImportCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProvince;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBlank;
    }
}