﻿using Bayer.WMS.Base;
using Bayer.WMS.CustomControls;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Inv.Views
{
    public partial class ProductPackingMaintView : BaseForm, IProductPackingMaintView
    {
        public ProductPackingMaintView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductPacking);

            dtgProductPacking.Columns[colProductPacking_Quantity.Index].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            toolTip1.SetToolTip(btnUpload, "Import quy cách đóng gói");
        }

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            try
            {
                await _presenter.LoadMainData();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnSearch.Enabled = true;
            }
        }

        private async void btnUpload_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() != DialogResult.OK)
                        return;

                    await (_presenter as IProductPackingMaintPresenter).ImportExcel(ofd.FileName);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnUpload.Enabled = true;
            }
        }

        public override void InitializeComboBox()
        {
            cmbType.ValueMember = ProductPacking.type.ValueMember;
            cmbType.DisplayMember = ProductPacking.type.DisplayMember;
            cmbType.DataSource = ProductPacking.type.GetWithAll();

            cmbProduct.PageSize = 20;
            cmbProduct.ValueMember = "ProductID";
            cmbProduct.DisplayMember = "ProductCode - Description";
            cmbProduct.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("ProductCode", "Mã sản phẩm", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("TypeDisplay", "Loại sản phẩm", 150),
                new MultiColumnComboBox.ComboBoxColumn("UOM", "ĐVT", 100),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };

            colProductPacking_ProductID.ValueMember = "ProductID";
            colProductPacking_ProductID.DisplayMember = "ProductDisplay";

            colProductPacking_Type.ValueMember = ProductPacking.type.ValueMember;
            colProductPacking_Type.DisplayMember = ProductPacking.type.DisplayMember;
            colProductPacking_Type.DataSource = ProductPacking.type.Get();
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var productPackingMaintPresenter = _presenter as IProductPackingMaintPresenter;

                await Task.WhenAll(productPackingMaintPresenter.LoadProducts(), productPackingMaintPresenter.LoadMainData());
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override async Task Delete()
        {
            try
            {
                var current = bdsProductPacking.Current as ProductPacking;
                if (current != null)
                {
                    await Task.Run(() =>
                    {
                        if (InvokeRequired)
                        {
                            BeginInvoke((MethodInvoker)delegate
                            {
                                bdsProductPacking.RemoveCurrent();
                            });
                        }
                        else
                        {
                            bdsProductPacking.RemoveCurrent();
                        }
                    });

                    (_presenter as IProductPackingMaintPresenter).DeleteProductPacking(current);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public string PackingType
        {
            get { return cmbType.SelectedValue == null ? String.Empty : cmbType.SelectedValue.ToString(); }
        }

        public int ProductID
        {
            get
            {
                if (cmbProduct.SelectedItem == null)
                    return 0;
                else
                    return int.Parse($"{cmbProduct.SelectedItem["ProductID"]}");
            }
        }

        public DataTable ProductsForCMB
        {
            set { cmbProduct.Source = value; }
        }

        public IList<Product> Products
        {
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        colProductPacking_ProductID.DataSource = value;
                    });
                }
                else
                {
                    colProductPacking_ProductID.DataSource = value;
                }
            }
        }

        private IList<ProductPacking> _productPackings;

        public IList<ProductPacking> ProductPackings
        {
            get { return _productPackings; }
            set
            {
                _productPackings = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsProductPacking.DataSource = _productPackings;
                    });
                }
                else
                {
                    bdsProductPacking.DataSource = _productPackings;
                }
            }
        }

        private IList<ProductPacking> _deletedProductPackings;

        public IList<ProductPacking> DeletedProductPackings
        {
            get { return _deletedProductPackings; }
            set { _deletedProductPackings = value; }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    btnUpload.Enabled = false;
                    dtgProductPacking.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }
    }
}
