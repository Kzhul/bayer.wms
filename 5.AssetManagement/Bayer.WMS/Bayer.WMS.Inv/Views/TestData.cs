﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Inv.Views
{
    public partial class TestData : Form
    {
        private IProductRepository _productRepository;
        public TestData(IProductRepository productRepository)
        {
            InitializeComponent();
            _productRepository = productRepository;
        }

        private void btnGenBarcode_Click(object sender, EventArgs e)
        {
            _productRepository.ExecuteNonQuery("proc_Barcode_Clear");
            foreach (string line in txtBarcode.Lines)
            {
                //your code, but working with 'line' - one at a time
                _productRepository.ExecuteNonQuery("proc_Barcode_Insert",
                         new SqlParameter { ParameterName = "BarCode", SqlDbType = SqlDbType.VarChar, Value = line });
            }
            MessageBox.Show("Done");
        }
    }
}
