﻿namespace Bayer.WMS.Inv.Views
{
    partial class ProductPackingMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductPackingMaintView));
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.colProductPacking_ProductID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colProductPacking_Type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colProductPacking_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductPacking_Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductPacking_Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductPacking_Note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsProductPacking = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.btnUpload = new System.Windows.Forms.Button();
            this.cmbProduct = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductPacking)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgProductPacking.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductPacking_ProductID,
            this.colProductPacking_Type,
            this.colProductPacking_Quantity,
            this.colProductPacking_Size,
            this.colProductPacking_Weight,
            this.colProductPacking_Note});
            this.dtgProductPacking.DataSource = this.bdsProductPacking;
            this.dtgProductPacking.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(12, 88);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.Size = new System.Drawing.Size(974, 553);
            this.dtgProductPacking.TabIndex = 4;
            // 
            // colProductPacking_ProductID
            // 
            this.colProductPacking_ProductID.DataPropertyName = "ProductID";
            this.colProductPacking_ProductID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colProductPacking_ProductID.DropDownWidth = 500;
            this.colProductPacking_ProductID.HeaderText = "Sản phẩm";
            this.colProductPacking_ProductID.Name = "colProductPacking_ProductID";
            this.colProductPacking_ProductID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colProductPacking_ProductID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colProductPacking_ProductID.Width = 400;
            // 
            // colProductPacking_Type
            // 
            this.colProductPacking_Type.DataPropertyName = "Type";
            this.colProductPacking_Type.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colProductPacking_Type.HeaderText = "Loại";
            this.colProductPacking_Type.Name = "colProductPacking_Type";
            this.colProductPacking_Type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colProductPacking_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // colProductPacking_Quantity
            // 
            this.colProductPacking_Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = null;
            this.colProductPacking_Quantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductPacking_Quantity.HeaderText = "Quy cách";
            this.colProductPacking_Quantity.Name = "colProductPacking_Quantity";
            // 
            // colProductPacking_Size
            // 
            this.colProductPacking_Size.DataPropertyName = "Size";
            this.colProductPacking_Size.HeaderText = "Kích thước";
            this.colProductPacking_Size.Name = "colProductPacking_Size";
            // 
            // colProductPacking_Weight
            // 
            this.colProductPacking_Weight.DataPropertyName = "Weight";
            this.colProductPacking_Weight.HeaderText = "Khối lượng + sản phẩm (tham khảo)";
            this.colProductPacking_Weight.Name = "colProductPacking_Weight";
            this.colProductPacking_Weight.Width = 150;
            // 
            // colProductPacking_Note
            // 
            this.colProductPacking_Note.DataPropertyName = "Note";
            this.colProductPacking_Note.HeaderText = "Ghi chú";
            this.colProductPacking_Note.Name = "colProductPacking_Note";
            this.colProductPacking_Note.Width = 400;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 29;
            this.label3.Text = "Loại:";
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(57, 12);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(200, 24);
            this.cmbType.TabIndex = 0;
            // 
            // btnUpload
            // 
            this.btnUpload.Image = ((System.Drawing.Image)(resources.GetObject("btnUpload.Image")));
            this.btnUpload.Location = new System.Drawing.Point(12, 52);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(50, 30);
            this.btnUpload.TabIndex = 3;
            this.btnUpload.TabStop = false;
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // cmbProduct
            // 
            this.cmbProduct.Columns = null;
            this.cmbProduct.DropDownHeight = 1;
            this.cmbProduct.DropDownWidth = 500;
            this.cmbProduct.FormattingEnabled = true;
            this.cmbProduct.IntegralHeight = false;
            this.cmbProduct.Location = new System.Drawing.Point(353, 12);
            this.cmbProduct.MaxLength = 255;
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.PageSize = 0;
            this.cmbProduct.PresenterInfo = null;
            this.cmbProduct.Size = new System.Drawing.Size(400, 24);
            this.cmbProduct.Source = null;
            this.cmbProduct.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(274, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 41;
            this.label4.Text = "Sản phẩm:";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(759, 10);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 30);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ProductPackingMaintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 653);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.cmbProduct);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgProductPacking);
            this.Name = "ProductPackingMaintView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductPacking)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.BindingSource bdsProductPacking;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.DataGridViewComboBoxColumn colProductPacking_ProductID;
        private System.Windows.Forms.DataGridViewComboBoxColumn colProductPacking_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductPacking_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductPacking_Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductPacking_Weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductPacking_Note;
        private CustomControls.MultiColumnComboBox cmbProduct;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}