﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;
using System.Windows.Forms.DataVisualization.Charting;

namespace Bayer.WMS.Inv.Views
{
    public partial class ReportLocationView : BaseForm, IReportLocationView
    {
        public ReportLocationView()
        {
            InitializeComponent();
            InitializeComboBox();
            SetDoubleBuffered(dtgLocation);
            SetDoubleBuffered(dtgLine);
            SetDoubleBuffered(dtgZone);
            SetDoubleBuffered(dtgTopCustomer);
            SetDoubleBuffered(dtgZoneList);
            SetDoubleBuffered(dtgPallet);
            tabControl1.TabPages.Remove(tabChart);
        }

        public override void InitializeComboBox()
        {

        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);
        }

        private DataTable _ReportZones;

        public DataTable ReportZones
        {
            get { return _ReportZones; }
            set
            {
                _ReportZones = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsZone.DataSource = _ReportZones;
                    });
                }
                else
                {
                    bdsZone.DataSource = _ReportZones;
                }
            }
        }

        private DataTable _ReportLines;

        public DataTable ReportLines
        {
            get { return _ReportLines; }
            set
            {
                _ReportLines = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsLine.DataSource = _ReportLines;
                    });
                }
                else
                {
                    bdsLine.DataSource = _ReportLines;
                }
            }
        }

        private DataTable _ReportLocations;

        public DataTable ReportLocations
        {
            get { return _ReportLocations; }
            set
            {
                _ReportLocations = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsLocation.DataSource = _ReportLocations;
                    });
                }
                else
                {
                    bdsLocation.DataSource = _ReportLocations;
                }
            }
        }

        private DataTable _ReportZoneChart;

        public DataTable ReportZoneChart
        {
            get { return _ReportZoneChart; }
            set
            {
                _ReportZoneChart = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsZoneChart.DataSource = _ReportZoneChart;
                    });
                }
                else
                {
                    bdsZoneChart.DataSource = _ReportZoneChart;
                }
            }
        }

        private DataTable _ReportTopCustomer;

        public DataTable ReportTopCustomer
        {
            get { return _ReportTopCustomer; }
            set
            {
                _ReportTopCustomer = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsTopCustomer.DataSource = _ReportTopCustomer;
                    });
                }
                else
                {
                    bdsTopCustomer.DataSource = _ReportTopCustomer;
                }
            }
        }

        private DataTable _ReportPalletReady;

        public DataTable ReportPalletReady
        {
            get { return _ReportPalletReady; }
            set
            {
                _ReportPalletReady = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsPallet.DataSource = _ReportPalletReady;
                    });
                }
                else
                {
                    bdsPallet.DataSource = _ReportPalletReady;
                }
            }
        }

        private DataTable _ReportChart1;

        public DataTable ReportChart1
        {
            get { return _ReportChart1; }
            set
            {
                _ReportChart1 = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {

                    });
                }
                else
                {
                    chart1.Series.Clear();
                    chart1.Series.Add("% Tỉ lệ giao hàng thành công trong tháng");
                    chart1.Series[0].XValueMember = "DeliveryDate";
                    chart1.Series[0].YValueMembers = "DeliveryPercent";
                    chart1.ChartAreas[0].AxisX.Interval = 1;
                    chart1.DataSource = _ReportChart1;
                    chart1.DataBind();
                    chart1.Series[0].ToolTip = "#VALX: Tỉ lệ giao hàng #VALY{F0} %";
                    //chart1.Series[0].Points[0].YValues
                    foreach (DataPoint dt in chart1.Series[0].Points)
                    {
                        if (dt.YValues[0] >= 90)
                        {
                            dt.Color = Color.Blue;
                        }
                        else if (dt.YValues[0] >= 60 && dt.YValues[0] < 90)
                        {
                            dt.Color = Color.Yellow;
                        }
                        else if (dt.YValues[0] >= 0 && dt.YValues[0] < 60)
                        {
                            dt.Color = Color.OrangeRed;
                        }
                    }

                }
            }
        }

        private DataTable _ReportChartPie1;

        public DataTable ReportChartPie1
        {
            get { return _ReportChartPie1; }
            set
            {
                _ReportChartPie1 = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {

                    });
                }
                else
                {
                    chartZone1.Series.Clear();
                    chartZone1.Series.Add("% sử dụng khu vực Kho 1");
                    chartZone1.Series[0].XValueMember = "ValueName";
                    chartZone1.Series[0].YValueMembers = "Value";
                    chartZone1.ChartAreas[0].AxisX.Interval = 1;
                    chartZone1.Series[0].ChartType = SeriesChartType.Pie;
                    chartZone1.DataSource = _ReportChartPie1;
                    chartZone1.DataBind();
                    chartZone1.Series[0].ToolTip = "#VALX: #VALY{F0} vị trí";
                }
            }
        }

        private DataTable _ReportChartPie2;

        public DataTable ReportChartPie2
        {
            get { return _ReportChartPie2; }
            set
            {
                _ReportChartPie2 = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {

                    });
                }
                else
                {
                    chartZone2.Series.Clear();
                    
                    chartZone2.Series.Add("% sử dụng khu vực Kho 2");
                    chartZone2.Series[0].XValueMember = "ValueName";
                    chartZone2.Series[0].YValueMembers = "Value";
                    chartZone2.ChartAreas[0].AxisX.Interval = 1;
                    chartZone2.Series[0].ChartType = SeriesChartType.Pie;
                    chartZone2.DataSource = _ReportChartPie2;
                    chartZone2.DataBind();
                    chartZone2.Series[0].ToolTip = "#VALX: #VALY{F0} vị trí";
                }
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }

        public async void Print()
        {
            var doPresenter = _presenter as IReportLocationPresenter;
            await doPresenter.Print();
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            int searchDate = 0;
            if (dtpToDate.Enabled == true)
            {
                if (rdbWarehouseVerifyDate.Checked)
                {
                    searchDate = 1;
                }
                else
                {
                    searchDate = 2;
                }
            }
            var doPresenter = _presenter as IReportLocationPresenter;
            doPresenter.Search(txtProduct.Text, txtProductLot.Text, txtUser.Text,txtWarehouseName.Text, txtZoneName.Text, txtLineName.Text, dtpFromDate.Value, dtpToDate.Value, searchDate);
        }

        private void rdbWarehouseVerifyDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpFromDate.Enabled = true;
            dtpToDate.Enabled = true;
        }

        private async void btnDownload_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IReportLocationPresenter;
            await doPresenter.Print();
        }
    }
}