﻿namespace Bayer.WMS.Inv.Views
{
    partial class ReturnDOView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatusDisplay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.colDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOImportCode2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExportCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplitCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrepareCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveryTicketCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEXStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSPStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPRStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDHStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsReport = new System.Windows.Forms.BindingSource(this.components);
            this.txtDeliveryCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnLoadDO = new System.Windows.Forms.Button();
            this.butReturnFull = new System.Windows.Forms.Button();
            this.txtDeliveryDate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.colDOCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequestReturnQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Device = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_MFG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOImportCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsReport)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(107, 12);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(400, 24);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 0;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(107, 71);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(400, 22);
            this.txtDescription.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Lý do trả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã Phiếu Trả:";
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.Location = new System.Drawing.Point(655, 41);
            this.txtStatusDisplay.MaxLength = 255;
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.ReadOnly = true;
            this.txtStatusDisplay.Size = new System.Drawing.Size(199, 22);
            this.txtStatusDisplay.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(541, 44);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Trạng thái:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(4, 103);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(992, 545);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgProductPacking);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(984, 516);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách sản phẩm";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToAddRows = false;
            this.dtgProductPacking.AllowUserToDeleteRows = false;
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDOCode,
            this.colProductionPlan_PackageSize,
            this.colProductionPlan_ProductDescription,
            this.colProductionPlan_Quantity,
            this.colProductionPlan_PackageQuantity,
            this.colRequestReturnQty,
            this.colDescription,
            this.colProductionPlan_ProductCode,
            this.colProductionPlan_ProductLot,
            this.colProductionPlan_Device,
            this.colProductionPlan_MFG,
            this.colBatchCodeDistributor,
            this.colDOImportCode});
            this.dtgProductPacking.DataSource = this.bdsDetails;
            this.dtgProductPacking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(3, 3);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.Size = new System.Drawing.Size(978, 510);
            this.dtgProductPacking.TabIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(984, 516);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Danh sách phiếu liên quan";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Azure;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDeliveryDate,
            this.colDOImportCode2,
            this.colExportCode,
            this.colSplitCode,
            this.colPrepareCode,
            this.colDeliveryTicketCode,
            this.colEXStatus,
            this.colSPStatus,
            this.colPRStatus,
            this.colDHStatus});
            this.dataGridView1.DataSource = this.bdsReport;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(978, 510);
            this.dataGridView1.TabIndex = 9;
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.DataPropertyName = "DeliveryDate";
            dataGridViewCellStyle10.Format = "dd/MM/yyyy";
            dataGridViewCellStyle10.NullValue = null;
            this.colDeliveryDate.DefaultCellStyle = dataGridViewCellStyle10;
            this.colDeliveryDate.HeaderText = "Ngày giao hàng";
            this.colDeliveryDate.Name = "colDeliveryDate";
            this.colDeliveryDate.ReadOnly = true;
            // 
            // colDOImportCode2
            // 
            this.colDOImportCode2.DataPropertyName = "DOImportCode";
            this.colDOImportCode2.HeaderText = "Mã phiếu DO Import";
            this.colDOImportCode2.Name = "colDOImportCode2";
            this.colDOImportCode2.ReadOnly = true;
            // 
            // colExportCode
            // 
            this.colExportCode.DataPropertyName = "ExportCode";
            this.colExportCode.HeaderText = "Phiếu Xuất Hàng";
            this.colExportCode.Name = "colExportCode";
            this.colExportCode.ReadOnly = true;
            // 
            // colSplitCode
            // 
            this.colSplitCode.DataPropertyName = "SplitCode";
            this.colSplitCode.HeaderText = "Phiếu Chia Hàng";
            this.colSplitCode.Name = "colSplitCode";
            this.colSplitCode.ReadOnly = true;
            // 
            // colPrepareCode
            // 
            this.colPrepareCode.DataPropertyName = "PrepareCode";
            this.colPrepareCode.HeaderText = "Phiếu Soạn Hàng";
            this.colPrepareCode.Name = "colPrepareCode";
            this.colPrepareCode.ReadOnly = true;
            // 
            // colDeliveryTicketCode
            // 
            this.colDeliveryTicketCode.DataPropertyName = "DeliveryTicketCode";
            this.colDeliveryTicketCode.HeaderText = "Phiếu Giao Hàng";
            this.colDeliveryTicketCode.Name = "colDeliveryTicketCode";
            this.colDeliveryTicketCode.ReadOnly = true;
            // 
            // colEXStatus
            // 
            this.colEXStatus.DataPropertyName = "EXStatus";
            this.colEXStatus.HeaderText = "Trạng thái Xuất Hàng";
            this.colEXStatus.Name = "colEXStatus";
            this.colEXStatus.ReadOnly = true;
            // 
            // colSPStatus
            // 
            this.colSPStatus.DataPropertyName = "SPStatus";
            this.colSPStatus.HeaderText = "Trạng thái Chia Hàng";
            this.colSPStatus.Name = "colSPStatus";
            this.colSPStatus.ReadOnly = true;
            // 
            // colPRStatus
            // 
            this.colPRStatus.DataPropertyName = "PRStatus";
            this.colPRStatus.HeaderText = "Trạng thái Soạn Hàng";
            this.colPRStatus.Name = "colPRStatus";
            this.colPRStatus.ReadOnly = true;
            // 
            // colDHStatus
            // 
            this.colDHStatus.DataPropertyName = "DHStatus";
            this.colDHStatus.HeaderText = "Trạng thái Giao Hàng";
            this.colDHStatus.Name = "colDHStatus";
            this.colDHStatus.ReadOnly = true;
            // 
            // txtDeliveryCode
            // 
            this.txtDeliveryCode.Location = new System.Drawing.Point(107, 43);
            this.txtDeliveryCode.MaxLength = 255;
            this.txtDeliveryCode.Name = "txtDeliveryCode";
            this.txtDeliveryCode.Size = new System.Drawing.Size(135, 22);
            this.txtDeliveryCode.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 28;
            this.label4.Text = "Mã DO:";
            // 
            // btnLoadDO
            // 
            this.btnLoadDO.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLoadDO.Location = new System.Drawing.Point(248, 39);
            this.btnLoadDO.Name = "btnLoadDO";
            this.btnLoadDO.Size = new System.Drawing.Size(82, 30);
            this.btnLoadDO.TabIndex = 29;
            this.btnLoadDO.Text = "Load DO";
            this.btnLoadDO.UseVisualStyleBackColor = true;
            this.btnLoadDO.Click += new System.EventHandler(this.btnLoadDO_Click);
            // 
            // butReturnFull
            // 
            this.butReturnFull.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.butReturnFull.Location = new System.Drawing.Point(655, 67);
            this.butReturnFull.Name = "butReturnFull";
            this.butReturnFull.Size = new System.Drawing.Size(132, 30);
            this.butReturnFull.TabIndex = 30;
            this.butReturnFull.Text = "Trả nguyên DO";
            this.butReturnFull.UseVisualStyleBackColor = true;
            this.butReturnFull.Click += new System.EventHandler(this.butReturnFull_Click);
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Location = new System.Drawing.Point(655, 12);
            this.txtDeliveryDate.MaxLength = 255;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.ReadOnly = true;
            this.txtDeliveryDate.Size = new System.Drawing.Size(199, 22);
            this.txtDeliveryDate.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(541, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 16);
            this.label7.TabIndex = 32;
            this.label7.Text = "Ngày trả hàng:";
            // 
            // colDOCode
            // 
            this.colDOCode.DataPropertyName = "Delivery";
            dataGridViewCellStyle2.NullValue = null;
            this.colDOCode.DefaultCellStyle = dataGridViewCellStyle2;
            this.colDOCode.HeaderText = "Mã DO";
            this.colDOCode.Name = "colDOCode";
            this.colDOCode.ReadOnly = true;
            // 
            // colProductionPlan_PackageSize
            // 
            this.colProductionPlan_PackageSize.DataPropertyName = "BatchCode";
            dataGridViewCellStyle3.NullValue = null;
            this.colProductionPlan_PackageSize.DefaultCellStyle = dataGridViewCellStyle3;
            this.colProductionPlan_PackageSize.HeaderText = "Số Lô";
            this.colProductionPlan_PackageSize.Name = "colProductionPlan_PackageSize";
            this.colProductionPlan_PackageSize.ReadOnly = true;
            // 
            // colProductionPlan_ProductDescription
            // 
            this.colProductionPlan_ProductDescription.DataPropertyName = "ProductCode";
            this.colProductionPlan_ProductDescription.HeaderText = "Mã Sản Phẩm";
            this.colProductionPlan_ProductDescription.Name = "colProductionPlan_ProductDescription";
            this.colProductionPlan_ProductDescription.ReadOnly = true;
            // 
            // colProductionPlan_Quantity
            // 
            this.colProductionPlan_Quantity.DataPropertyName = "ProductDescription";
            dataGridViewCellStyle4.NullValue = null;
            this.colProductionPlan_Quantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductionPlan_Quantity.HeaderText = "Mô Tả";
            this.colProductionPlan_Quantity.Name = "colProductionPlan_Quantity";
            this.colProductionPlan_Quantity.ReadOnly = true;
            this.colProductionPlan_Quantity.Width = 200;
            // 
            // colProductionPlan_PackageQuantity
            // 
            this.colProductionPlan_PackageQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colProductionPlan_PackageQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.colProductionPlan_PackageQuantity.HeaderText = "Số lượng trên DO";
            this.colProductionPlan_PackageQuantity.Name = "colProductionPlan_PackageQuantity";
            this.colProductionPlan_PackageQuantity.ReadOnly = true;
            // 
            // colRequestReturnQty
            // 
            this.colRequestReturnQty.DataPropertyName = "RequestReturnQty";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.colRequestReturnQty.DefaultCellStyle = dataGridViewCellStyle6;
            this.colRequestReturnQty.HeaderText = "Số lượng yêu cầu trả";
            this.colRequestReturnQty.Name = "colRequestReturnQty";
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "Description";
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Red;
            this.colDescription.DefaultCellStyle = dataGridViewCellStyle7;
            this.colDescription.HeaderText = "Lý do trả";
            this.colDescription.Name = "colDescription";
            this.colDescription.Width = 200;
            // 
            // colProductionPlan_ProductCode
            // 
            this.colProductionPlan_ProductCode.DataPropertyName = "CompanyCode";
            this.colProductionPlan_ProductCode.HeaderText = "Mã Công Ty";
            this.colProductionPlan_ProductCode.Name = "colProductionPlan_ProductCode";
            this.colProductionPlan_ProductCode.ReadOnly = true;
            // 
            // colProductionPlan_ProductLot
            // 
            this.colProductionPlan_ProductLot.DataPropertyName = "CompanyName";
            this.colProductionPlan_ProductLot.HeaderText = "Tên Công Ty";
            this.colProductionPlan_ProductLot.Name = "colProductionPlan_ProductLot";
            this.colProductionPlan_ProductLot.ReadOnly = true;
            this.colProductionPlan_ProductLot.Width = 200;
            // 
            // colProductionPlan_Device
            // 
            this.colProductionPlan_Device.DataPropertyName = "Province";
            this.colProductionPlan_Device.HeaderText = "Tỉnh Thành";
            this.colProductionPlan_Device.Name = "colProductionPlan_Device";
            this.colProductionPlan_Device.ReadOnly = true;
            // 
            // colProductionPlan_MFG
            // 
            this.colProductionPlan_MFG.DataPropertyName = "StrDeliveryDate";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.NullValue = null;
            this.colProductionPlan_MFG.DefaultCellStyle = dataGridViewCellStyle8;
            this.colProductionPlan_MFG.HeaderText = "Ngày Giao Hàng";
            this.colProductionPlan_MFG.Name = "colProductionPlan_MFG";
            this.colProductionPlan_MFG.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "Số lô NCC";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            this.colBatchCodeDistributor.ReadOnly = true;
            // 
            // colDOImportCode
            // 
            this.colDOImportCode.DataPropertyName = "DOImportCode";
            this.colDOImportCode.HeaderText = "Mã phiếu DOImport";
            this.colDOImportCode.Name = "colDOImportCode";
            this.colDOImportCode.ReadOnly = true;
            // 
            // ReturnDOView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 653);
            this.Controls.Add(this.txtDeliveryDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.butReturnFull);
            this.Controls.Add(this.btnLoadDO);
            this.Controls.Add(this.txtDeliveryCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtStatusDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label2);
            this.Name = "ReturnDOView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsDetails;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatusDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource bdsReport;
        private System.Windows.Forms.TextBox txtDeliveryCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnLoadDO;
        private System.Windows.Forms.Button butReturnFull;
        private System.Windows.Forms.TextBox txtDeliveryDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOImportCode2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExportCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplitCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrepareCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryTicketCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEXStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSPStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPRStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDHStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestReturnQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Device;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_MFG;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOImportCode;
    }
}