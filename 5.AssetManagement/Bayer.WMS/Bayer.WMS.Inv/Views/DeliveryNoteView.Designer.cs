﻿namespace Bayer.WMS.Inv.Views
{
    partial class DeliveryNoteView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeliveryNoteView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatusDisplay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDeliveryDate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDOImportCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.colProductionPlan_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExportedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceivedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colShipper1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceiver1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colShipper2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceiver2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colShipper3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceiver3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCodes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colConfirmQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequireReturnQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExportReturnedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceiveReturnedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnSearchSugestion = new System.Windows.Forms.Button();
            this.txtSugestionSearch = new System.Windows.Forms.TextBox();
            this.dtgPalletSuggest = new System.Windows.Forms.DataGridView();
            this.bdsLocationSuggest = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bdsPalletPick = new System.Windows.Forms.BindingSource(this.components);
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnImportPalletPicking = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCodeSuggest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLineName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLengthID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLevelID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLineName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLengthID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLevelID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPalletSuggest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsLocationSuggest)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletPick)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(73, 12);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(400, 24);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 12;
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(73, 42);
            this.txtProductDescription.MaxLength = 255;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(400, 22);
            this.txtProductDescription.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Mô tả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã:";
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.Location = new System.Drawing.Point(604, 42);
            this.txtStatusDisplay.MaxLength = 255;
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.ReadOnly = true;
            this.txtStatusDisplay.Size = new System.Drawing.Size(199, 22);
            this.txtStatusDisplay.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(490, 45);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Trạng thái:";
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Location = new System.Drawing.Point(604, 12);
            this.txtDeliveryDate.MaxLength = 255;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.ReadOnly = true;
            this.txtDeliveryDate.Size = new System.Drawing.Size(199, 22);
            this.txtDeliveryDate.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(490, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 16);
            this.label7.TabIndex = 27;
            this.label7.Text = "Ngày giao hàng:";
            // 
            // txtDOImportCode
            // 
            this.txtDOImportCode.Location = new System.Drawing.Point(73, 70);
            this.txtDOImportCode.MaxLength = 255;
            this.txtDOImportCode.Name = "txtDOImportCode";
            this.txtDOImportCode.ReadOnly = true;
            this.txtDOImportCode.Size = new System.Drawing.Size(199, 22);
            this.txtDOImportCode.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 73);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 16);
            this.label4.TabIndex = 31;
            this.label4.Text = "Mã LO:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 134);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(924, 291);
            this.tabControl1.TabIndex = 32;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgProductPacking);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(916, 262);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách sản phẩm";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToAddRows = false;
            this.dtgProductPacking.AllowUserToDeleteRows = false;
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductionPlan_ProductDescription,
            this.colProductionPlan_Quantity,
            this.colProductionPlan_PackageSize,
            this.colBatchCodeDistributor,
            this.colProductionPlan_PackageQuantity,
            this.colExportedQty,
            this.colReceivedQty,
            this.PackQuantity,
            this.colPackSize,
            this.colPackType,
            this.ProductQuantity,
            this.colShipper1,
            this.colReceiver1,
            this.colShipper2,
            this.colReceiver2,
            this.colShipper3,
            this.colReceiver3,
            this.colPalletCodes,
            this.colConfirmQty,
            this.colRequireReturnQty,
            this.colExportReturnedQty,
            this.colReceiveReturnedQty});
            this.dtgProductPacking.DataSource = this.bdsDetails;
            this.dtgProductPacking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(3, 3);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.ReadOnly = true;
            this.dtgProductPacking.Size = new System.Drawing.Size(910, 256);
            this.dtgProductPacking.TabIndex = 9;
            // 
            // colProductionPlan_ProductDescription
            // 
            this.colProductionPlan_ProductDescription.DataPropertyName = "ProductCode";
            this.colProductionPlan_ProductDescription.HeaderText = "Mã Sản Phẩm";
            this.colProductionPlan_ProductDescription.Name = "colProductionPlan_ProductDescription";
            this.colProductionPlan_ProductDescription.ReadOnly = true;
            // 
            // colProductionPlan_Quantity
            // 
            this.colProductionPlan_Quantity.DataPropertyName = "ProductDescription";
            dataGridViewCellStyle2.NullValue = null;
            this.colProductionPlan_Quantity.DefaultCellStyle = dataGridViewCellStyle2;
            this.colProductionPlan_Quantity.HeaderText = "Mô Tả";
            this.colProductionPlan_Quantity.Name = "colProductionPlan_Quantity";
            this.colProductionPlan_Quantity.ReadOnly = true;
            this.colProductionPlan_Quantity.Width = 200;
            // 
            // colProductionPlan_PackageSize
            // 
            this.colProductionPlan_PackageSize.DataPropertyName = "BatchCode";
            dataGridViewCellStyle3.NullValue = null;
            this.colProductionPlan_PackageSize.DefaultCellStyle = dataGridViewCellStyle3;
            this.colProductionPlan_PackageSize.HeaderText = "Số Lô";
            this.colProductionPlan_PackageSize.Name = "colProductionPlan_PackageSize";
            this.colProductionPlan_PackageSize.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "Số Lô NCC";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            this.colBatchCodeDistributor.ReadOnly = true;
            // 
            // colProductionPlan_PackageQuantity
            // 
            this.colProductionPlan_PackageQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            this.colProductionPlan_PackageQuantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductionPlan_PackageQuantity.HeaderText = "Số lượng";
            this.colProductionPlan_PackageQuantity.Name = "colProductionPlan_PackageQuantity";
            this.colProductionPlan_PackageQuantity.ReadOnly = true;
            // 
            // colExportedQty
            // 
            this.colExportedQty.DataPropertyName = "ExportedQty";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colExportedQty.DefaultCellStyle = dataGridViewCellStyle5;
            this.colExportedQty.HeaderText = "SL đã xuất";
            this.colExportedQty.Name = "colExportedQty";
            this.colExportedQty.ReadOnly = true;
            // 
            // colReceivedQty
            // 
            this.colReceivedQty.DataPropertyName = "ReceivedQty";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            this.colReceivedQty.DefaultCellStyle = dataGridViewCellStyle6;
            this.colReceivedQty.HeaderText = "SL đã nhận";
            this.colReceivedQty.Name = "colReceivedQty";
            this.colReceivedQty.ReadOnly = true;
            // 
            // PackQuantity
            // 
            this.PackQuantity.DataPropertyName = "PackQuantity";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            dataGridViewCellStyle7.NullValue = null;
            this.PackQuantity.DefaultCellStyle = dataGridViewCellStyle7;
            this.PackQuantity.HeaderText = "Thùng chẵn";
            this.PackQuantity.Name = "PackQuantity";
            this.PackQuantity.ReadOnly = true;
            // 
            // colPackSize
            // 
            this.colPackSize.DataPropertyName = "PackSize";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            this.colPackSize.DefaultCellStyle = dataGridViewCellStyle8;
            this.colPackSize.HeaderText = "Quy cách";
            this.colPackSize.Name = "colPackSize";
            this.colPackSize.ReadOnly = true;
            // 
            // colPackType
            // 
            this.colPackType.DataPropertyName = "StrPackType";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colPackType.DefaultCellStyle = dataGridViewCellStyle9;
            this.colPackType.HeaderText = "Loại Thùng";
            this.colPackType.Name = "colPackType";
            this.colPackType.ReadOnly = true;
            this.colPackType.Width = 50;
            // 
            // ProductQuantity
            // 
            this.ProductQuantity.DataPropertyName = "ProductQuantity";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N0";
            dataGridViewCellStyle10.NullValue = null;
            this.ProductQuantity.DefaultCellStyle = dataGridViewCellStyle10;
            this.ProductQuantity.HeaderText = "Gói lẻ";
            this.ProductQuantity.Name = "ProductQuantity";
            this.ProductQuantity.ReadOnly = true;
            // 
            // colShipper1
            // 
            this.colShipper1.DataPropertyName = "Shipper1";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N0";
            dataGridViewCellStyle11.NullValue = null;
            this.colShipper1.DefaultCellStyle = dataGridViewCellStyle11;
            this.colShipper1.HeaderText = "Giao lần 1";
            this.colShipper1.Name = "colShipper1";
            this.colShipper1.ReadOnly = true;
            // 
            // colReceiver1
            // 
            this.colReceiver1.DataPropertyName = "StrReceiver1";
            this.colReceiver1.HeaderText = "Người nhận";
            this.colReceiver1.Name = "colReceiver1";
            this.colReceiver1.ReadOnly = true;
            this.colReceiver1.Width = 150;
            // 
            // colShipper2
            // 
            this.colShipper2.DataPropertyName = "Shipper2";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N0";
            this.colShipper2.DefaultCellStyle = dataGridViewCellStyle12;
            this.colShipper2.HeaderText = "Giao lần 2";
            this.colShipper2.Name = "colShipper2";
            this.colShipper2.ReadOnly = true;
            // 
            // colReceiver2
            // 
            this.colReceiver2.DataPropertyName = "StrReceiver2";
            this.colReceiver2.HeaderText = "Người nhận";
            this.colReceiver2.Name = "colReceiver2";
            this.colReceiver2.ReadOnly = true;
            this.colReceiver2.Width = 150;
            // 
            // colShipper3
            // 
            this.colShipper3.DataPropertyName = "Shipper3";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N0";
            this.colShipper3.DefaultCellStyle = dataGridViewCellStyle13;
            this.colShipper3.HeaderText = "Giao lần 3";
            this.colShipper3.Name = "colShipper3";
            this.colShipper3.ReadOnly = true;
            // 
            // colReceiver3
            // 
            this.colReceiver3.DataPropertyName = "StrReceiver3";
            this.colReceiver3.HeaderText = "Người nhận";
            this.colReceiver3.Name = "colReceiver3";
            this.colReceiver3.ReadOnly = true;
            this.colReceiver3.Width = 150;
            // 
            // colPalletCodes
            // 
            this.colPalletCodes.DataPropertyName = "PalletCodes";
            this.colPalletCodes.HeaderText = "Pallet";
            this.colPalletCodes.Name = "colPalletCodes";
            this.colPalletCodes.ReadOnly = true;
            // 
            // colConfirmQty
            // 
            this.colConfirmQty.DataPropertyName = "ConfirmQty";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N0";
            this.colConfirmQty.DefaultCellStyle = dataGridViewCellStyle14;
            this.colConfirmQty.HeaderText = "SL xác nhận";
            this.colConfirmQty.Name = "colConfirmQty";
            this.colConfirmQty.ReadOnly = true;
            // 
            // colRequireReturnQty
            // 
            this.colRequireReturnQty.DataPropertyName = "RequireReturnQty";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "N0";
            this.colRequireReturnQty.DefaultCellStyle = dataGridViewCellStyle15;
            this.colRequireReturnQty.HeaderText = "SL yêu cầu trả";
            this.colRequireReturnQty.Name = "colRequireReturnQty";
            this.colRequireReturnQty.ReadOnly = true;
            // 
            // colExportReturnedQty
            // 
            this.colExportReturnedQty.DataPropertyName = "ExportReturnedQty";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle16.Format = "N0";
            this.colExportReturnedQty.DefaultCellStyle = dataGridViewCellStyle16;
            this.colExportReturnedQty.HeaderText = "SL đã trả lại";
            this.colExportReturnedQty.Name = "colExportReturnedQty";
            this.colExportReturnedQty.ReadOnly = true;
            // 
            // colReceiveReturnedQty
            // 
            this.colReceiveReturnedQty.DataPropertyName = "ReceiveReturnedQty";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle17.Format = "N0";
            this.colReceiveReturnedQty.DefaultCellStyle = dataGridViewCellStyle17;
            this.colReceiveReturnedQty.HeaderText = "SL đã nhận lại";
            this.colReceiveReturnedQty.Name = "colReceiveReturnedQty";
            this.colReceiveReturnedQty.ReadOnly = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnSearchSugestion);
            this.tabPage2.Controls.Add(this.txtSugestionSearch);
            this.tabPage2.Controls.Add(this.dtgPalletSuggest);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(916, 262);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Danh sách vị trí gợi ý";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnSearchSugestion
            // 
            this.btnSearchSugestion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchSugestion.Location = new System.Drawing.Point(208, 3);
            this.btnSearchSugestion.Name = "btnSearchSugestion";
            this.btnSearchSugestion.Size = new System.Drawing.Size(217, 30);
            this.btnSearchSugestion.TabIndex = 93;
            this.btnSearchSugestion.TabStop = false;
            this.btnSearchSugestion.Text = "Tìm kiếm";
            this.btnSearchSugestion.UseVisualStyleBackColor = true;
            this.btnSearchSugestion.Click += new System.EventHandler(this.btnSearchSugestion_Click);
            // 
            // txtSugestionSearch
            // 
            this.txtSugestionSearch.Location = new System.Drawing.Point(3, 7);
            this.txtSugestionSearch.MaxLength = 255;
            this.txtSugestionSearch.Name = "txtSugestionSearch";
            this.txtSugestionSearch.Size = new System.Drawing.Size(199, 22);
            this.txtSugestionSearch.TabIndex = 93;
            this.txtSugestionSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSugestionSearch_KeyPress);
            // 
            // dtgPalletSuggest
            // 
            this.dtgPalletSuggest.AllowUserToAddRows = false;
            this.dtgPalletSuggest.AllowUserToDeleteRows = false;
            this.dtgPalletSuggest.AllowUserToOrderColumns = true;
            this.dtgPalletSuggest.AllowUserToResizeRows = false;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.Azure;
            this.dtgPalletSuggest.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dtgPalletSuggest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPalletSuggest.AutoGenerateColumns = false;
            this.dtgPalletSuggest.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPalletSuggest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPalletSuggest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.colPalletCodeSuggest,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.colLocationName,
            this.colZoneName,
            this.colLineName,
            this.colLengthID,
            this.colLevelID,
            this.colLocationCode});
            this.dtgPalletSuggest.DataSource = this.bdsLocationSuggest;
            this.dtgPalletSuggest.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPalletSuggest.Location = new System.Drawing.Point(3, 35);
            this.dtgPalletSuggest.Name = "dtgPalletSuggest";
            this.dtgPalletSuggest.ReadOnly = true;
            this.dtgPalletSuggest.Size = new System.Drawing.Size(910, 224);
            this.dtgPalletSuggest.TabIndex = 11;
            this.dtgPalletSuggest.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPalletSuggest_CellDoubleClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView1);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage3.Size = new System.Drawing.Size(916, 262);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Danh sách pallet rớt hàng, xuất hàng";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.Azure;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.colLocationName1,
            this.colZoneName1,
            this.colLineName1,
            this.colLengthID1,
            this.colLevelID1,
            this.dataGridViewTextBoxColumn13});
            this.dataGridView1.DataSource = this.bdsPalletPick;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(910, 256);
            this.dataGridView1.TabIndex = 12;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // btnDownload
            // 
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownload.Location = new System.Drawing.Point(3, 98);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(168, 30);
            this.btnDownload.TabIndex = 33;
            this.btnDownload.TabStop = false;
            this.btnDownload.Text = "1.Download vị trí gợi ý";
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnImportPalletPicking
            // 
            this.btnImportPalletPicking.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImportPalletPicking.Location = new System.Drawing.Point(177, 98);
            this.btnImportPalletPicking.Name = "btnImportPalletPicking";
            this.btnImportPalletPicking.Size = new System.Drawing.Size(217, 30);
            this.btnImportPalletPicking.TabIndex = 34;
            this.btnImportPalletPicking.TabStop = false;
            this.btnImportPalletPicking.Text = "2.Import danh sách pallet rớt hàng";
            this.btnImportPalletPicking.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImportPalletPicking.UseVisualStyleBackColor = true;
            this.btnImportPalletPicking.Click += new System.EventHandler(this.btnImportPalletPicking_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirm.Location = new System.Drawing.Point(400, 98);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(213, 30);
            this.btnConfirm.TabIndex = 92;
            this.btnConfirm.TabStop = false;
            this.btnConfirm.Text = "3.Xác nhận cho xe nâng rớt hàng";
            this.btnConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click_1);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DeliveryDate";
            dataGridViewCellStyle19.Format = "dd/MM/yyyy";
            dataGridViewCellStyle19.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn1.HeaderText = "Ngày giao hàng";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn2.HeaderText = "Mã sản phẩm";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ProductName";
            this.dataGridViewTextBoxColumn3.HeaderText = "Tên sản phẩm";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 200;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ProductLot";
            this.dataGridViewTextBoxColumn4.HeaderText = "Số lô";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // colPalletCodeSuggest
            // 
            this.colPalletCodeSuggest.DataPropertyName = "PalletCode";
            this.colPalletCodeSuggest.HeaderText = "Mã pallet gợi ý";
            this.colPalletCodeSuggest.Name = "colPalletCodeSuggest";
            this.colPalletCodeSuggest.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "DOQuantity";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N0";
            dataGridViewCellStyle20.NullValue = null;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn10.HeaderText = "Số lượng LO yêu cầu";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "PalletQuantity";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.Format = "N0";
            dataGridViewCellStyle21.NullValue = null;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn11.HeaderText = "Số lượng pallet hiện có";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // colLocationName
            // 
            this.colLocationName.DataPropertyName = "LocationName";
            this.colLocationName.HeaderText = "Tên vị trí";
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.ReadOnly = true;
            // 
            // colZoneName
            // 
            this.colZoneName.DataPropertyName = "ZoneName";
            this.colZoneName.HeaderText = "Khu vực";
            this.colZoneName.Name = "colZoneName";
            this.colZoneName.ReadOnly = true;
            // 
            // colLineName
            // 
            this.colLineName.DataPropertyName = "LineName";
            this.colLineName.HeaderText = "Dãy";
            this.colLineName.Name = "colLineName";
            this.colLineName.ReadOnly = true;
            // 
            // colLengthID
            // 
            this.colLengthID.DataPropertyName = "LengthID";
            this.colLengthID.HeaderText = "Kệ";
            this.colLengthID.Name = "colLengthID";
            this.colLengthID.ReadOnly = true;
            // 
            // colLevelID
            // 
            this.colLevelID.DataPropertyName = "LevelID";
            this.colLevelID.HeaderText = "Tầng";
            this.colLevelID.Name = "colLevelID";
            this.colLevelID.ReadOnly = true;
            // 
            // colLocationCode
            // 
            this.colLocationCode.DataPropertyName = "LocationCode";
            this.colLocationCode.HeaderText = "Mã vị trí gợi ý";
            this.colLocationCode.Name = "colLocationCode";
            this.colLocationCode.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn7.HeaderText = "Mã sản phẩm";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ProductName";
            this.dataGridViewTextBoxColumn8.HeaderText = "Tên sản phẩm";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 200;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "ProductLot";
            this.dataGridViewTextBoxColumn9.HeaderText = "Số lô";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "PalletCode";
            this.dataGridViewTextBoxColumn12.HeaderText = "Mã Pallet";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "DOQuantity";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle23.Format = "N0";
            dataGridViewCellStyle23.NullValue = null;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn14.HeaderText = "Số lượng LO yêu cầu";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "PalletQuantity";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle24.Format = "N0";
            dataGridViewCellStyle24.NullValue = null;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn15.HeaderText = "Số lượng pallet hiện có";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // colLocationName1
            // 
            this.colLocationName1.DataPropertyName = "LocationName";
            this.colLocationName1.HeaderText = "Tên vị trí";
            this.colLocationName1.Name = "colLocationName1";
            this.colLocationName1.ReadOnly = true;
            // 
            // colZoneName1
            // 
            this.colZoneName1.DataPropertyName = "ZoneName";
            this.colZoneName1.HeaderText = "Khu vực";
            this.colZoneName1.Name = "colZoneName1";
            this.colZoneName1.ReadOnly = true;
            // 
            // colLineName1
            // 
            this.colLineName1.DataPropertyName = "LineName";
            this.colLineName1.HeaderText = "Dãy";
            this.colLineName1.Name = "colLineName1";
            this.colLineName1.ReadOnly = true;
            // 
            // colLengthID1
            // 
            this.colLengthID1.DataPropertyName = "LengthID";
            this.colLengthID1.HeaderText = "Kệ";
            this.colLengthID1.Name = "colLengthID1";
            this.colLengthID1.ReadOnly = true;
            // 
            // colLevelID1
            // 
            this.colLevelID1.DataPropertyName = "LevelID";
            this.colLevelID1.HeaderText = "Tầng";
            this.colLevelID1.Name = "colLevelID1";
            this.colLevelID1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "LocationCode";
            this.dataGridViewTextBoxColumn13.HeaderText = "Mã vị trí";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // DeliveryNoteView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 426);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnImportPalletPicking);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtDOImportCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDeliveryDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtStatusDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.label2);
            this.Name = "DeliveryNoteView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPalletSuggest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsLocationSuggest)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletPick)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsDetails;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatusDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDeliveryDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDOImportCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dtgPalletSuggest;
        private System.Windows.Forms.BindingSource bdsLocationSuggest;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnImportPalletPicking;
        private System.Windows.Forms.BindingSource bdsPalletPick;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExportedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceivedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colShipper1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceiver1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colShipper2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceiver2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colShipper3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceiver3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCodes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colConfirmQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequireReturnQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExportReturnedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceiveReturnedQty;
        private System.Windows.Forms.Button btnSearchSugestion;
        private System.Windows.Forms.TextBox txtSugestionSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCodeSuggest;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLengthID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLevelID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLengthID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLevelID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
    }
}