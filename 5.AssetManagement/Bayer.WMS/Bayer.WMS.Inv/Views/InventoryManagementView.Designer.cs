﻿using Bayer.WMS.Objs;

namespace Bayer.WMS.Inv.Views
{
    partial class InventoryManagementView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InventoryManagementView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.btnUpload = new System.Windows.Forms.Button();
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtInventoryName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatusDisplay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgDODetail = new System.Windows.Forms.DataGridView();
            this.colLineNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAssetCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAssetName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCostCenter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMachineCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrLastCheckBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrLastCheckDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dtgExportProcess = new System.Windows.Forms.DataGridView();
            this.colMachineCode2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMustCheck = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colChecked = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRemain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsExportProcess = new System.Windows.Forms.BindingSource(this.components);
            this.btnDownload = new System.Windows.Forms.Button();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDownloadTemplate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDODetail)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgExportProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsExportProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // btnUpload
            // 
            this.btnUpload.Image = ((System.Drawing.Image)(resources.GetObject("btnUpload.Image")));
            this.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpload.Location = new System.Drawing.Point(16, 100);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(81, 30);
            this.btnUpload.TabIndex = 2;
            this.btnUpload.TabStop = false;
            this.btnUpload.Text = "1.Import";
            this.btnUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(117, 12);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(284, 24);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 0;
            // 
            // txtInventoryName
            // 
            this.txtInventoryName.Location = new System.Drawing.Point(117, 42);
            this.txtInventoryName.MaxLength = 255;
            this.txtInventoryName.Name = "txtInventoryName";
            this.txtInventoryName.Size = new System.Drawing.Size(284, 22);
            this.txtInventoryName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Tên kỳ kiểm kê:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã kỳ kiểm kê:";
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.Location = new System.Drawing.Point(486, 39);
            this.txtStatusDisplay.MaxLength = 255;
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.ReadOnly = true;
            this.txtStatusDisplay.Size = new System.Drawing.Size(215, 22);
            this.txtStatusDisplay.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(410, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Trạng thái:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(4, 136);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1278, 512);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgDODetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1270, 483);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách tài sản cần kiểm kê và gán thiết bị";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgDODetail
            // 
            this.dtgDODetail.AllowUserToAddRows = false;
            this.dtgDODetail.AllowUserToDeleteRows = false;
            this.dtgDODetail.AllowUserToOrderColumns = true;
            this.dtgDODetail.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Azure;
            this.dtgDODetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgDODetail.AutoGenerateColumns = false;
            this.dtgDODetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgDODetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDODetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLineNbr,
            this.colAssetCode,
            this.colAssetName,
            this.colCostCenter,
            this.colDescription,
            this.colMachineCode,
            this.colStrStatus,
            this.colStrLastCheckBy,
            this.colStrLastCheckDate});
            this.dtgDODetail.DataSource = this.bdsDetails;
            this.dtgDODetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgDODetail.GridColor = System.Drawing.SystemColors.Control;
            this.dtgDODetail.Location = new System.Drawing.Point(3, 3);
            this.dtgDODetail.Name = "dtgDODetail";
            this.dtgDODetail.Size = new System.Drawing.Size(1264, 477);
            this.dtgDODetail.TabIndex = 7;
            // 
            // colLineNbr
            // 
            this.colLineNbr.DataPropertyName = "LineNbr";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colLineNbr.DefaultCellStyle = dataGridViewCellStyle9;
            this.colLineNbr.HeaderText = "STT";
            this.colLineNbr.Name = "colLineNbr";
            this.colLineNbr.ReadOnly = true;
            this.colLineNbr.Width = 40;
            // 
            // colAssetCode
            // 
            this.colAssetCode.DataPropertyName = "AssetCode";
            dataGridViewCellStyle10.NullValue = null;
            this.colAssetCode.DefaultCellStyle = dataGridViewCellStyle10;
            this.colAssetCode.HeaderText = "Mã tài sản";
            this.colAssetCode.Name = "colAssetCode";
            this.colAssetCode.ReadOnly = true;
            // 
            // colAssetName
            // 
            this.colAssetName.DataPropertyName = "AssetName";
            this.colAssetName.HeaderText = "Tên tài sản";
            this.colAssetName.Name = "colAssetName";
            this.colAssetName.ReadOnly = true;
            this.colAssetName.Width = 200;
            // 
            // colCostCenter
            // 
            this.colCostCenter.DataPropertyName = "CostCenter";
            this.colCostCenter.HeaderText = "CostCenter";
            this.colCostCenter.Name = "colCostCenter";
            this.colCostCenter.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "Description";
            this.colDescription.HeaderText = "Ghi chú";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            // 
            // colMachineCode
            // 
            this.colMachineCode.DataPropertyName = "MachineCode";
            this.colMachineCode.HeaderText = "Máy kiểm kê";
            this.colMachineCode.Name = "colMachineCode";
            this.colMachineCode.ReadOnly = true;
            // 
            // colStrStatus
            // 
            this.colStrStatus.DataPropertyName = "StrStatus";
            this.colStrStatus.HeaderText = "Trạng thái kiểm kê";
            this.colStrStatus.Name = "colStrStatus";
            this.colStrStatus.ReadOnly = true;
            // 
            // colStrLastCheckBy
            // 
            this.colStrLastCheckBy.DataPropertyName = "StrCheckBy";
            dataGridViewCellStyle11.NullValue = null;
            this.colStrLastCheckBy.DefaultCellStyle = dataGridViewCellStyle11;
            this.colStrLastCheckBy.HeaderText = "Người kiểm";
            this.colStrLastCheckBy.Name = "colStrLastCheckBy";
            this.colStrLastCheckBy.ReadOnly = true;
            this.colStrLastCheckBy.Width = 200;
            // 
            // colStrLastCheckDate
            // 
            this.colStrLastCheckDate.DataPropertyName = "StrCheckDate";
            dataGridViewCellStyle12.NullValue = null;
            this.colStrLastCheckDate.DefaultCellStyle = dataGridViewCellStyle12;
            this.colStrLastCheckDate.HeaderText = "Ngày kiểm";
            this.colStrLastCheckDate.Name = "colStrLastCheckDate";
            this.colStrLastCheckDate.ReadOnly = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dtgExportProcess);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1270, 483);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Tiến độ kiểm kê";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dtgExportProcess
            // 
            this.dtgExportProcess.AllowUserToAddRows = false;
            this.dtgExportProcess.AllowUserToDeleteRows = false;
            this.dtgExportProcess.AllowUserToOrderColumns = true;
            this.dtgExportProcess.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.Azure;
            this.dtgExportProcess.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dtgExportProcess.AutoGenerateColumns = false;
            this.dtgExportProcess.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgExportProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgExportProcess.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMachineCode2,
            this.colMustCheck,
            this.colChecked,
            this.colRemain});
            this.dtgExportProcess.DataSource = this.bdsExportProcess;
            this.dtgExportProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgExportProcess.GridColor = System.Drawing.SystemColors.Control;
            this.dtgExportProcess.Location = new System.Drawing.Point(0, 0);
            this.dtgExportProcess.Name = "dtgExportProcess";
            this.dtgExportProcess.ReadOnly = true;
            this.dtgExportProcess.Size = new System.Drawing.Size(1270, 483);
            this.dtgExportProcess.TabIndex = 12;
            this.dtgExportProcess.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dtgExportProcess_CellFormatting);
            // 
            // colMachineCode2
            // 
            this.colMachineCode2.DataPropertyName = "MachineCode";
            this.colMachineCode2.HeaderText = "Máy kiểm kê";
            this.colMachineCode2.Name = "colMachineCode2";
            this.colMachineCode2.ReadOnly = true;
            this.colMachineCode2.Width = 200;
            // 
            // colMustCheck
            // 
            this.colMustCheck.DataPropertyName = "MustCheck";
            this.colMustCheck.HeaderText = "Cần kiểm";
            this.colMustCheck.Name = "colMustCheck";
            this.colMustCheck.ReadOnly = true;
            // 
            // colChecked
            // 
            this.colChecked.DataPropertyName = "Checked";
            this.colChecked.HeaderText = "Đã kiểm";
            this.colChecked.Name = "colChecked";
            this.colChecked.ReadOnly = true;
            // 
            // colRemain
            // 
            this.colRemain.DataPropertyName = "Remain";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Format = "N0";
            dataGridViewCellStyle14.NullValue = null;
            this.colRemain.DefaultCellStyle = dataGridViewCellStyle14;
            this.colRemain.HeaderText = "Còn lại";
            this.colRemain.Name = "colRemain";
            this.colRemain.ReadOnly = true;
            // 
            // btnDownload
            // 
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownload.Location = new System.Drawing.Point(475, 100);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(226, 30);
            this.btnDownload.TabIndex = 30;
            this.btnDownload.TabStop = false;
            this.btnDownload.Text = "3.Download danh sách kiểm kê";
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(117, 71);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(584, 22);
            this.txtDescription.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 75);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 16);
            this.label6.TabIndex = 35;
            this.label6.Text = "Mô tả:";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd.MM.yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(486, 12);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(90, 22);
            this.dtpFromDate.TabIndex = 36;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(408, 16);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 16);
            this.label7.TabIndex = 37;
            this.label7.Text = "Từ ngày:";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd.MM.yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(611, 12);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(90, 22);
            this.dtpToDate.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(579, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 16);
            this.label4.TabIndex = 39;
            this.label4.Text = "Đến";
            // 
            // btnDownloadTemplate
            // 
            this.btnDownloadTemplate.Image = ((System.Drawing.Image)(resources.GetObject("btnDownloadTemplate.Image")));
            this.btnDownloadTemplate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownloadTemplate.Location = new System.Drawing.Point(103, 100);
            this.btnDownloadTemplate.Name = "btnDownloadTemplate";
            this.btnDownloadTemplate.Size = new System.Drawing.Size(165, 30);
            this.btnDownloadTemplate.TabIndex = 40;
            this.btnDownloadTemplate.TabStop = false;
            this.btnDownloadTemplate.Text = "2.Download template";
            this.btnDownloadTemplate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownloadTemplate.UseVisualStyleBackColor = true;
            this.btnDownloadTemplate.Click += new System.EventHandler(this.btnDownloadTemplate_Click);
            // 
            // InventoryManagementView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 653);
            this.Controls.Add(this.btnDownloadTemplate);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtStatusDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Controls.Add(this.txtInventoryName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.label4);
            this.Name = "InventoryManagementView";
            this.Text = "ProductPackingMaintView";
            this.Load += new System.EventHandler(this.InventoryManagementView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDODetail)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgExportProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsExportProcess)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsDetails;
        private System.Windows.Forms.Button btnUpload;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.TextBox txtInventoryName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatusDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgDODetail;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dtgExportProcess;
        private System.Windows.Forms.BindingSource bdsExportProcess;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMachineCode2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMustCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChecked;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRemain;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAssetCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAssetName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCostCenter;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMachineCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrLastCheckBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrLastCheckDate;
        private System.Windows.Forms.Button btnDownloadTemplate;
    }
}