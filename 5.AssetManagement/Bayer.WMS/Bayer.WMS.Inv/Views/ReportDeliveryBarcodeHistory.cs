﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class ReportDeliveryBarcodeHistoryView : BaseForm, IReportDeliveryBarcodeHistoryView
    {
        public ReportDeliveryBarcodeHistoryView()
        {
            InitializeComponent();
            InitializeComboBox();
            _ListCompanies = new DataTable();
            SetDoubleBuffered(dtgProductPacking);
            SetDoubleBuffered(dtgTotal);
        }

        public override void InitializeComboBox()
        {
            //cmbCompanyCode.DataBindings.Clear();
            #region cmbCompanyCode
            cmbCompanyCode.PageSize = 20;
            cmbCompanyCode.ValueMember = "CompanyCode";
            cmbCompanyCode.DisplayMember = "CompanyCode - CompanyName";
            cmbCompanyCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("CompanyCode", "Mã NPP", 100),
                new MultiColumnComboBox.ComboBoxColumn("CompanyName", "Tên NPP", 300),
                new MultiColumnComboBox.ComboBoxColumn("Province", "Tỉnh thành", 130)
            };
            cmbCompanyCode.DropDownWidth = 800;
            #endregion

            dtpFromDate.Value = DateTime.Today;
            dtpToDate.Value = DateTime.Today;
            this.dtpFromDate.ValueChanged += new System.EventHandler(this.dtpFromDate_ValueChanged);
            this.dtpToDate.ValueChanged += new System.EventHandler(this.dtpToDate_ValueChanged);
        }

        private DataTable _ListCompanies;
        public DataTable ListCompanies
        {
            get { return _ListCompanies; }
            set
            {
                
                _ListCompanies = value;
                Utility.LogEx("View_ListCompanies:" + _ListCompanies.Rows.Count.ToString(), string.Empty);
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        cmbCompanyCode.Source = _ListCompanies;
                        Utility.LogEx("MethodInvoker:DataSource", string.Empty);
                    });
                }
                else
                {
                    cmbCompanyCode.Source = _ListCompanies;
                    Utility.LogEx("DataSource", string.Empty);
                }
            }
        }

        private DataTable _PalletList;

        public DataTable PalletList
        {
            get { return _PalletList; }
            set
            {
                _PalletList = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsPalletList.DataSource = _PalletList;
                    });
                }
                else
                {
                    bdsPalletList.DataSource = _PalletList;
                }
            }
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

        }

        public DataTable Sheet1 { get; set; }

        public DataTable Sheet2 { get; set; }

        private DataTable _ReportDeliveryBarcodeHistoryTotal;

        public DataTable ReportDeliveryBarcodeHistoryTotals
        {
            get { return _ReportDeliveryBarcodeHistoryTotal; }
            set
            {
                _ReportDeliveryBarcodeHistoryTotal = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsTotal.DataSource = _ReportDeliveryBarcodeHistoryTotal;
                    });
                }
                else
                {
                    bdsTotal.DataSource = _ReportDeliveryBarcodeHistoryTotal;
                }
            }
        }

        private DataTable _ReportDeliveryBarcodeHistoryDetail;

        public DataTable ReportDeliveryBarcodeHistoryDetails
        {
            get { return _ReportDeliveryBarcodeHistoryDetail; }
            set
            {
                _ReportDeliveryBarcodeHistoryDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _ReportDeliveryBarcodeHistoryDetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _ReportDeliveryBarcodeHistoryDetail;
                }
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }

        public async void Print()
        {
            var doPresenter = _presenter as IReportDeliveryBarcodeHistoryPresenter;
            if (dtpFromDate.Value == dtpToDate.Value & cmbCompanyCode.SelectedItem != null)
            {
                await doPresenter.Print();
            }
            else
            {
                await doPresenter.PrintAll();
            }
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IReportDeliveryBarcodeHistoryPresenter;
            if (cmbCompanyCode.SelectedItem != null)
            {
                doPresenter.Search(cmbCompanyCode.SelectedItem["CompanyCode"].ToString(), dtpFromDate.Value, dtpToDate.Value);
            }
            else
            {
                doPresenter.Search(string.Empty, dtpFromDate.Value, dtpToDate.Value);
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IReportDeliveryBarcodeHistoryPresenter;
            doPresenter.LoadCompany(dtpFromDate.Value, dtpToDate.Value);
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IReportDeliveryBarcodeHistoryPresenter;
            doPresenter.LoadCompany(dtpFromDate.Value, dtpToDate.Value);
        }

        private void btnLoadQRCode_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IReportDeliveryBarcodeHistoryPresenter;
            if (cmbCompanyCode.SelectedItem != null)
            {
                doPresenter.SearchDetail(cmbCompanyCode.SelectedItem["CompanyCode"].ToString(), dtpFromDate.Value, dtpToDate.Value);
            }
            else
            {
                doPresenter.SearchDetail(string.Empty, dtpFromDate.Value, dtpToDate.Value);
            }
        }
    }
}