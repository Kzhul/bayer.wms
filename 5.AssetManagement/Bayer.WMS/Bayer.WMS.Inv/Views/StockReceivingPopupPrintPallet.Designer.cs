﻿namespace Bayer.WMS.Inv.Views
{
    partial class StockReceivingPopupPrintPallet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StockReceivingPopupPrintPallet));
            this.cmbListPrinter = new System.Windows.Forms.ComboBox();
            this.txtNumberToPrint = new System.Windows.Forms.TextBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbListPrinter
            // 
            this.cmbListPrinter.FormattingEnabled = true;
            this.cmbListPrinter.Location = new System.Drawing.Point(79, 38);
            this.cmbListPrinter.Name = "cmbListPrinter";
            this.cmbListPrinter.Size = new System.Drawing.Size(231, 21);
            this.cmbListPrinter.TabIndex = 1;
            // 
            // txtNumberToPrint
            // 
            this.txtNumberToPrint.Location = new System.Drawing.Point(79, 12);
            this.txtNumberToPrint.Name = "txtNumberToPrint";
            this.txtNumberToPrint.Size = new System.Drawing.Size(231, 20);
            this.txtNumberToPrint.TabIndex = 0;
            this.txtNumberToPrint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNumberToPrint.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumberToPrint_KeyPress);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(79, 65);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(231, 41);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Xác nhận In";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Số lượng in";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Máy in";
            // 
            // StockReceivingPopupPrintPallet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 125);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.txtNumberToPrint);
            this.Controls.Add(this.cmbListPrinter);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StockReceivingPopupPrintPallet";
            this.Text = "StockReceivingPopupPrintPallet";
            this.Load += new System.EventHandler(this.StockReceivingPopupPrintPallet_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbListPrinter;
        private System.Windows.Forms.TextBox txtNumberToPrint;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}