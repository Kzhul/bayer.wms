﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Inv.Views
{
    public partial class GenPalletView : BaseForm
    {
        public GenPalletView()
        {
            InitializeComponent();
        }

        private void GenPalletView_Load(object sender, EventArgs e)
        {
            InitializeComboBox();
        }

        private void dtpStartDate_ValueChanged(object sender, EventArgs e)
        {
            txtExpiryDate.Text = String.Format("{0:dd.MM.yyyy}", dtpStartDate.Value.AddMonths((int)nudUsageTime.Value));
        }

        private void nudUsageTime_ValueChanged(object sender, EventArgs e)
        {
            txtExpiryDate.Text = String.Format("{0:dd.MM.yyyy}", dtpStartDate.Value.AddMonths((int)nudUsageTime.Value));
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public override void InitializeComboBox()
        {
            cmbType.ValueMember = Pallet.type.ValueMember;
            cmbType.DisplayMember = Pallet.type.DisplayMember;
            cmbType.DataSource = Pallet.type.Get();
        }

        public string PalletType => cmbType.SelectedValue.ToString();

        public DateTime StartDate => dtpStartDate.Value;

        public int UsageTime => (int)nudUsageTime.Value;

        public DateTime ExpiryDate => dtpStartDate.Value.AddMonths((int)nudUsageTime.Value);

        public int Qty => (int)nudQty.Value;
    }
}
