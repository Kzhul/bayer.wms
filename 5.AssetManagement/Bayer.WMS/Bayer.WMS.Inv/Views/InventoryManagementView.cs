﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Reflection;
using Microsoft.Practices.Unity;

namespace Bayer.WMS.Inv.Views
{
    public partial class InventoryManagementView : BaseForm, IInventoryManagementView
    {
        public InventoryManagementView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgDODetail);
            SetDoubleBuffered(dtgExportProcess);
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "InventoryCode";
            cmbCode.DisplayMember = "InventoryCode - InventoryName";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("InventoryCode", "Mã kỳ kiểm kê", 200),
                new MultiColumnComboBox.ComboBoxColumn("InventoryName", "Tên", 100),
                new MultiColumnComboBox.ComboBoxColumn("StrFromDate", "Từ ngày", 100),
                new MultiColumnComboBox.ComboBoxColumn("StrToDate", "Đến ngày", 100),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbCode.DropDownWidth = 500;

            var _mainPresenter = _presenter as IInventoryManagementPresenter;
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_mainPresenter, "LoadHeader", "OnCodeSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            InitializeComboBox();

            var doPresenter = _presenter as IInventoryManagementPresenter;
            await Task.WhenAll(doPresenter.LoadHeader());

            if (!isRefresh)
                doPresenter.Insert();
        }

        public DataTable ListInventories
        {
            set { cmbCode.Source = value; }
        }
        
        private DataTable _ReportDetails;

        public DataTable ReportDetails
        {
            get { return _ReportDetails; }
            set
            {
                _ReportDetails = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _ReportDetails;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _ReportDetails;
                }
            }
        }
                
        private DataTable _ReportProcess;

        public DataTable ReportProcess
        {
            get { return _ReportProcess; }
            set
            {
                _ReportProcess = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsExportProcess.DataSource = _ReportProcess;
                    });
                }
                else
                {
                    bdsExportProcess.DataSource = _ReportProcess;
                }

               
            }
        }

        private Inventory _header;

        public Inventory Header
        {
            get { return _header; }
            set
            {
                _header = value;

                #region Data Binding
                cmbCode.DataBindings.Clear();
                txtInventoryName.DataBindings.Clear();                
                dtpFromDate.DataBindings.Clear();
                dtpToDate.DataBindings.Clear();
                txtStatusDisplay.DataBindings.Clear();
                txtDescription.DataBindings.Clear();

                cmbCode.DataBindings.Add("Text", Header, "InventoryCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtInventoryName.DataBindings.Add("Text", Header, "InventoryName", true, DataSourceUpdateMode.OnPropertyChanged);
                dtpFromDate.DataBindings.Add("Value", Header, "FromDate", true, DataSourceUpdateMode.OnPropertyChanged);
                dtpToDate.DataBindings.Add("Value", Header, "ToDate", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStatusDisplay.DataBindings.Add("Text", Header, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", Header, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                #endregion
                
                HandlePermission(string.Empty);
            }
        }

        public override async Task Delete()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(Header.InventoryCode))
                {
                    if (MessageBox.Show("Kỳ - " + Header.InventoryCode, "Bạn có chắc muốn xóa kỳ kiểm kê này ?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var doPresenter = _presenter as IInventoryManagementPresenter;
                        await doPresenter.Delete();
                    }
                }

            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override void HandlePermission(string accessRight)
        {
            //string permissionImportExcel = Utility.Sitemaps.First(p => p.SitemapID == 21).AccessRights;
            //if (permissionImportExcel == RoleSitemap.accessRights.NotSet || permissionImportExcel == RoleSitemap.accessRights.Read)
            //{
            //    btnUpload.Enabled = false;
            //    btnUpload.Click -= btnUpload_Click;
            //}
        }

        public string _fileName;

        public async void Print()
        {
            var doPresenter = _presenter as IInventoryManagementPresenter;
            await doPresenter.Print();
        }

        private async void btnUpload_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        _fileName = ofd.FileName;
                        await (_presenter as IInventoryManagementPresenter).ImportExcel(_fileName, txtInventoryName.Text);
                    }
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnUpload.Enabled = true;
            }
        }
        
        private async void btnDownload_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IInventoryManagementPresenter;
            await doPresenter.Export();
        }

        private async void btnDownloadProcess_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IInventoryManagementPresenter;
            await doPresenter.ExportProcess();
        }

        private void InventoryManagementView_Load(object sender, EventArgs e)
        {
            
        }
        
        private void dtgExportProcess_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //foreach (DataGridViewRow row in dtgExportProcess.Rows)
            //{
            //    if (Convert.ToString(row.Cells["colQRCode"].Value) == "QRCode" && Convert.ToInt32(row.Cells["colNeedExportQuantity"].Value) > 0)
            //    {
            //        row.Cells["colNeedExportQuantity"].Style.BackColor = Color.Red;
            //        row.Cells["colNeedReceiveQuantity"].Style.BackColor = Color.Red;
            //    }
            //}
        }

        private async void btnDownloadTemplate_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IInventoryManagementPresenter;
            await doPresenter.DownloadTemplate();
        }
    }
}