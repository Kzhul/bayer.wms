﻿using Bayer.WMS.Objs;

namespace Bayer.WMS.Inv.Views
{
    partial class DeliveryOrderView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeliveryOrderView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.btnUpload = new System.Windows.Forms.Button();
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatusDisplay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgDODetail = new System.Windows.Forms.DataGridView();
            this.colLineNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Device = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProvinceShipTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_MFG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colChangedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colChangeReason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequireReturnQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dtgExportProcess = new System.Windows.Forms.DataGridView();
            this.bdsExportProcess = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dtgDOProcess = new System.Windows.Forms.DataGridView();
            this.bdsDOProcess = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dtgPallet = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsPalletList = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dtgDocumentRelated = new System.Windows.Forms.DataGridView();
            this.colDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExportCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplitCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrepareCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveryTicketCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyCode1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEXStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSPStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPRStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDHStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsReport = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtStrIsExportFull = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStrIsSplitFull = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtStrIsPrepareFull = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtStrIsDeliveryFull = new System.Windows.Forms.TextBox();
            this.btnUpdateRelateDO = new System.Windows.Forms.Button();
            this.btnCorrectAdditionalDO = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnDownloadProcess = new System.Windows.Forms.Button();
            this.btnTestData = new System.Windows.Forms.Button();
            this.btnConfirmDeliveryFinish = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQRCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCartonOddQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNeedExportQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNeedReceiveQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHasQRCode1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveredQty2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNeedPrepareQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNeedDeliveryQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDODetail)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgExportProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsExportProcess)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDOProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDOProcess)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletList)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDocumentRelated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsReport)).BeginInit();
            this.SuspendLayout();
            // 
            // btnUpload
            // 
            this.btnUpload.Image = ((System.Drawing.Image)(resources.GetObject("btnUpload.Image")));
            this.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpload.Location = new System.Drawing.Point(12, 100);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(72, 30);
            this.btnUpload.TabIndex = 2;
            this.btnUpload.TabStop = false;
            this.btnUpload.Text = "1.Import";
            this.btnUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(91, 12);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(310, 24);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 0;
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(91, 42);
            this.txtProductDescription.MaxLength = 255;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(310, 22);
            this.txtProductDescription.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Mô tả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã LO:";
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.Location = new System.Drawing.Point(91, 72);
            this.txtStatusDisplay.MaxLength = 255;
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.ReadOnly = true;
            this.txtStatusDisplay.Size = new System.Drawing.Size(310, 22);
            this.txtStatusDisplay.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Trạng thái:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(4, 136);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1278, 512);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgDODetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1270, 483);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách sản phẩm";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgDODetail
            // 
            this.dtgDODetail.AllowUserToAddRows = false;
            this.dtgDODetail.AllowUserToDeleteRows = false;
            this.dtgDODetail.AllowUserToOrderColumns = true;
            this.dtgDODetail.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgDODetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgDODetail.AutoGenerateColumns = false;
            this.dtgDODetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgDODetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDODetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLineNbr,
            this.colDOCode,
            this.colProductionPlan_ProductLot,
            this.colProductionPlan_Device,
            this.colProvinceShipTo,
            this.colProductionPlan_ProductCode,
            this.colProductionPlan_ProductDescription,
            this.colProductionPlan_Quantity,
            this.colProductionPlan_PackageSize,
            this.colQuantity,
            this.colProductionPlan_MFG,
            this.colBatchCodeDistributor,
            this.colChangedQuantity,
            this.colChangeReason,
            this.colDOQuantity,
            this.colRequireReturnQty});
            this.dtgDODetail.DataSource = this.bdsDetails;
            this.dtgDODetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgDODetail.GridColor = System.Drawing.SystemColors.Control;
            this.dtgDODetail.Location = new System.Drawing.Point(3, 3);
            this.dtgDODetail.Name = "dtgDODetail";
            this.dtgDODetail.Size = new System.Drawing.Size(1264, 477);
            this.dtgDODetail.TabIndex = 7;
            // 
            // colLineNbr
            // 
            this.colLineNbr.DataPropertyName = "LineNbr";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colLineNbr.DefaultCellStyle = dataGridViewCellStyle2;
            this.colLineNbr.HeaderText = "STT";
            this.colLineNbr.Name = "colLineNbr";
            this.colLineNbr.ReadOnly = true;
            this.colLineNbr.Width = 40;
            // 
            // colDOCode
            // 
            this.colDOCode.DataPropertyName = "Delivery";
            dataGridViewCellStyle3.NullValue = null;
            this.colDOCode.DefaultCellStyle = dataGridViewCellStyle3;
            this.colDOCode.HeaderText = "Mã DO";
            this.colDOCode.Name = "colDOCode";
            this.colDOCode.ReadOnly = true;
            // 
            // colProductionPlan_ProductLot
            // 
            this.colProductionPlan_ProductLot.DataPropertyName = "CompanyName";
            this.colProductionPlan_ProductLot.HeaderText = "Tên Công Ty";
            this.colProductionPlan_ProductLot.Name = "colProductionPlan_ProductLot";
            this.colProductionPlan_ProductLot.ReadOnly = true;
            this.colProductionPlan_ProductLot.Width = 200;
            // 
            // colProductionPlan_Device
            // 
            this.colProductionPlan_Device.DataPropertyName = "ProvinceSoldTo";
            this.colProductionPlan_Device.HeaderText = "Tỉnh Thành";
            this.colProductionPlan_Device.Name = "colProductionPlan_Device";
            this.colProductionPlan_Device.ReadOnly = true;
            // 
            // colProvinceShipTo
            // 
            this.colProvinceShipTo.DataPropertyName = "ProvinceShipTo";
            this.colProvinceShipTo.HeaderText = "Tỉnh thành (ShipTo)";
            this.colProvinceShipTo.Name = "colProvinceShipTo";
            this.colProvinceShipTo.ReadOnly = true;
            // 
            // colProductionPlan_ProductCode
            // 
            this.colProductionPlan_ProductCode.DataPropertyName = "CompanyCode";
            this.colProductionPlan_ProductCode.HeaderText = "Mã Công Ty";
            this.colProductionPlan_ProductCode.Name = "colProductionPlan_ProductCode";
            this.colProductionPlan_ProductCode.ReadOnly = true;
            // 
            // colProductionPlan_ProductDescription
            // 
            this.colProductionPlan_ProductDescription.DataPropertyName = "ProductCode";
            this.colProductionPlan_ProductDescription.HeaderText = "Mã Sản Phẩm";
            this.colProductionPlan_ProductDescription.Name = "colProductionPlan_ProductDescription";
            this.colProductionPlan_ProductDescription.ReadOnly = true;
            // 
            // colProductionPlan_Quantity
            // 
            this.colProductionPlan_Quantity.DataPropertyName = "ProductName";
            dataGridViewCellStyle4.NullValue = null;
            this.colProductionPlan_Quantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductionPlan_Quantity.HeaderText = "Mô Tả";
            this.colProductionPlan_Quantity.Name = "colProductionPlan_Quantity";
            this.colProductionPlan_Quantity.ReadOnly = true;
            this.colProductionPlan_Quantity.Width = 200;
            // 
            // colProductionPlan_PackageSize
            // 
            this.colProductionPlan_PackageSize.DataPropertyName = "BatchCode";
            dataGridViewCellStyle5.NullValue = null;
            this.colProductionPlan_PackageSize.DefaultCellStyle = dataGridViewCellStyle5;
            this.colProductionPlan_PackageSize.HeaderText = "Số Lô";
            this.colProductionPlan_PackageSize.Name = "colProductionPlan_PackageSize";
            this.colProductionPlan_PackageSize.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            this.colQuantity.DefaultCellStyle = dataGridViewCellStyle6;
            this.colQuantity.HeaderText = "Số lượng";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.ReadOnly = true;
            // 
            // colProductionPlan_MFG
            // 
            this.colProductionPlan_MFG.DataPropertyName = "StrDeliveryDate";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.NullValue = null;
            this.colProductionPlan_MFG.DefaultCellStyle = dataGridViewCellStyle7;
            this.colProductionPlan_MFG.HeaderText = "Ngày Giao Hàng";
            this.colProductionPlan_MFG.Name = "colProductionPlan_MFG";
            this.colProductionPlan_MFG.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "Số lô NCC";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            this.colBatchCodeDistributor.ReadOnly = true;
            // 
            // colChangedQuantity
            // 
            this.colChangedQuantity.DataPropertyName = "ChangedQuantity";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = null;
            this.colChangedQuantity.DefaultCellStyle = dataGridViewCellStyle8;
            this.colChangedQuantity.HeaderText = "Số lượng thay đổi";
            this.colChangedQuantity.Name = "colChangedQuantity";
            this.colChangedQuantity.Visible = false;
            // 
            // colChangeReason
            // 
            this.colChangeReason.DataPropertyName = "ChangeReason";
            this.colChangeReason.HeaderText = "Lý do thay đổi";
            this.colChangeReason.Name = "colChangeReason";
            this.colChangeReason.Visible = false;
            // 
            // colDOQuantity
            // 
            this.colDOQuantity.DataPropertyName = "DOQuantity";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            this.colDOQuantity.DefaultCellStyle = dataGridViewCellStyle9;
            this.colDOQuantity.HeaderText = "SL DO ban đầu";
            this.colDOQuantity.Name = "colDOQuantity";
            this.colDOQuantity.ReadOnly = true;
            this.colDOQuantity.Visible = false;
            // 
            // colRequireReturnQty
            // 
            this.colRequireReturnQty.DataPropertyName = "RequireReturnQty";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N0";
            this.colRequireReturnQty.DefaultCellStyle = dataGridViewCellStyle10;
            this.colRequireReturnQty.HeaderText = "SL yêu cầu trả";
            this.colRequireReturnQty.Name = "colRequireReturnQty";
            this.colRequireReturnQty.ReadOnly = true;
            this.colRequireReturnQty.Visible = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dtgExportProcess);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1270, 483);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Tiến độ xuất hàng";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dtgExportProcess
            // 
            this.dtgExportProcess.AllowUserToAddRows = false;
            this.dtgExportProcess.AllowUserToDeleteRows = false;
            this.dtgExportProcess.AllowUserToOrderColumns = true;
            this.dtgExportProcess.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Azure;
            this.dtgExportProcess.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dtgExportProcess.AutoGenerateColumns = false;
            this.dtgExportProcess.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgExportProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgExportProcess.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.colQRCode,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.colPackSize,
            this.colCartonOddQuantity,
            this.dataGridViewTextBoxColumn24,
            this.colNeedExportQuantity,
            this.colNeedReceiveQuantity});
            this.dtgExportProcess.DataSource = this.bdsExportProcess;
            this.dtgExportProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgExportProcess.GridColor = System.Drawing.SystemColors.Control;
            this.dtgExportProcess.Location = new System.Drawing.Point(0, 0);
            this.dtgExportProcess.Name = "dtgExportProcess";
            this.dtgExportProcess.ReadOnly = true;
            this.dtgExportProcess.Size = new System.Drawing.Size(1270, 483);
            this.dtgExportProcess.TabIndex = 12;
            this.dtgExportProcess.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dtgExportProcess_CellFormatting);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dtgDOProcess);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1270, 483);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tiến độ giao hàng";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dtgDOProcess
            // 
            this.dtgDOProcess.AllowUserToAddRows = false;
            this.dtgDOProcess.AllowUserToDeleteRows = false;
            this.dtgDOProcess.AllowUserToOrderColumns = true;
            this.dtgDOProcess.AllowUserToResizeRows = false;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.Azure;
            this.dtgDOProcess.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dtgDOProcess.AutoGenerateColumns = false;
            this.dtgDOProcess.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgDOProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDOProcess.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.colHasQRCode1,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.colDeliveredQty2,
            this.colNeedPrepareQty,
            this.colNeedDeliveryQty});
            this.dtgDOProcess.DataSource = this.bdsDOProcess;
            this.dtgDOProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgDOProcess.GridColor = System.Drawing.SystemColors.Control;
            this.dtgDOProcess.Location = new System.Drawing.Point(0, 0);
            this.dtgDOProcess.Name = "dtgDOProcess";
            this.dtgDOProcess.ReadOnly = true;
            this.dtgDOProcess.Size = new System.Drawing.Size(1270, 483);
            this.dtgDOProcess.TabIndex = 11;
            this.dtgDOProcess.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dtgDOProcess_CellFormatting);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dtgPallet);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1270, 483);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "Danh sách pallet";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dtgPallet
            // 
            this.dtgPallet.AllowUserToAddRows = false;
            this.dtgPallet.AllowUserToDeleteRows = false;
            this.dtgPallet.AllowUserToOrderColumns = true;
            this.dtgPallet.AllowUserToResizeRows = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.Azure;
            this.dtgPallet.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dtgPallet.AutoGenerateColumns = false;
            this.dtgPallet.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPallet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPallet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn27});
            this.dtgPallet.DataSource = this.bdsPalletList;
            this.dtgPallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallet.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPallet.Location = new System.Drawing.Point(0, 0);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.ReadOnly = true;
            this.dtgPallet.Size = new System.Drawing.Size(1270, 483);
            this.dtgPallet.TabIndex = 10;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PalletCode";
            this.dataGridViewTextBoxColumn4.HeaderText = "Mã Pallet";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "StrStatus";
            this.dataGridViewTextBoxColumn5.HeaderText = "Trạng thái";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "UserFullName";
            this.dataGridViewTextBoxColumn10.HeaderText = "Người sử dụng";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 130;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "CompanyName";
            this.dataGridViewTextBoxColumn11.HeaderText = "Tên Công Ty";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 250;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn16.HeaderText = "Mã SP";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "ProductDescription";
            this.dataGridViewTextBoxColumn17.HeaderText = "Tên SP";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "ProductLot";
            this.dataGridViewTextBoxColumn18.HeaderText = "Số lô";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "Quantity";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle26.Format = "N0";
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn27.HeaderText = "Số lượng";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgDocumentRelated);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1270, 483);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Danh sách phiếu liên quan";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dtgDocumentRelated
            // 
            this.dtgDocumentRelated.AllowUserToAddRows = false;
            this.dtgDocumentRelated.AllowUserToDeleteRows = false;
            this.dtgDocumentRelated.AllowUserToOrderColumns = true;
            this.dtgDocumentRelated.AllowUserToResizeRows = false;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.Azure;
            this.dtgDocumentRelated.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle27;
            this.dtgDocumentRelated.AutoGenerateColumns = false;
            this.dtgDocumentRelated.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgDocumentRelated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDocumentRelated.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDeliveryDate,
            this.colExportCode,
            this.colSplitCode,
            this.colPrepareCode,
            this.colDeliveryTicketCode,
            this.colCompanyCode1,
            this.colCompanyName1,
            this.colEXStatus,
            this.colSPStatus,
            this.colPRStatus,
            this.colDHStatus});
            this.dtgDocumentRelated.DataSource = this.bdsReport;
            this.dtgDocumentRelated.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgDocumentRelated.GridColor = System.Drawing.SystemColors.Control;
            this.dtgDocumentRelated.Location = new System.Drawing.Point(3, 3);
            this.dtgDocumentRelated.Name = "dtgDocumentRelated";
            this.dtgDocumentRelated.ReadOnly = true;
            this.dtgDocumentRelated.Size = new System.Drawing.Size(1264, 477);
            this.dtgDocumentRelated.TabIndex = 9;
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.DataPropertyName = "DeliveryDate";
            dataGridViewCellStyle28.Format = "dd/MM/yyyy";
            dataGridViewCellStyle28.NullValue = null;
            this.colDeliveryDate.DefaultCellStyle = dataGridViewCellStyle28;
            this.colDeliveryDate.HeaderText = "Ngày giao hàng";
            this.colDeliveryDate.Name = "colDeliveryDate";
            this.colDeliveryDate.ReadOnly = true;
            // 
            // colExportCode
            // 
            this.colExportCode.DataPropertyName = "ExportCode";
            this.colExportCode.HeaderText = "Phiếu Xuất Hàng";
            this.colExportCode.Name = "colExportCode";
            this.colExportCode.ReadOnly = true;
            // 
            // colSplitCode
            // 
            this.colSplitCode.DataPropertyName = "SplitCode";
            this.colSplitCode.HeaderText = "Phiếu Chia Hàng";
            this.colSplitCode.Name = "colSplitCode";
            this.colSplitCode.ReadOnly = true;
            // 
            // colPrepareCode
            // 
            this.colPrepareCode.DataPropertyName = "PrepareCode";
            this.colPrepareCode.HeaderText = "Phiếu Soạn Hàng";
            this.colPrepareCode.Name = "colPrepareCode";
            this.colPrepareCode.ReadOnly = true;
            // 
            // colDeliveryTicketCode
            // 
            this.colDeliveryTicketCode.DataPropertyName = "DeliveryTicketCode";
            this.colDeliveryTicketCode.HeaderText = "Phiếu Giao Hàng";
            this.colDeliveryTicketCode.Name = "colDeliveryTicketCode";
            this.colDeliveryTicketCode.ReadOnly = true;
            // 
            // colCompanyCode1
            // 
            this.colCompanyCode1.DataPropertyName = "CompanyCode";
            this.colCompanyCode1.HeaderText = "Mã Công Ty";
            this.colCompanyCode1.Name = "colCompanyCode1";
            this.colCompanyCode1.ReadOnly = true;
            // 
            // colCompanyName1
            // 
            this.colCompanyName1.DataPropertyName = "CompanyName";
            this.colCompanyName1.HeaderText = "Tên Công Ty";
            this.colCompanyName1.Name = "colCompanyName1";
            this.colCompanyName1.ReadOnly = true;
            // 
            // colEXStatus
            // 
            this.colEXStatus.DataPropertyName = "EXStatus";
            this.colEXStatus.HeaderText = "Trạng thái Xuất Hàng";
            this.colEXStatus.Name = "colEXStatus";
            this.colEXStatus.ReadOnly = true;
            // 
            // colSPStatus
            // 
            this.colSPStatus.DataPropertyName = "SPStatus";
            this.colSPStatus.HeaderText = "Trạng thái Chia Hàng";
            this.colSPStatus.Name = "colSPStatus";
            this.colSPStatus.ReadOnly = true;
            // 
            // colPRStatus
            // 
            this.colPRStatus.DataPropertyName = "PRStatus";
            this.colPRStatus.HeaderText = "Trạng thái Soạn Hàng";
            this.colPRStatus.Name = "colPRStatus";
            this.colPRStatus.ReadOnly = true;
            // 
            // colDHStatus
            // 
            this.colDHStatus.DataPropertyName = "DHStatus";
            this.colDHStatus.HeaderText = "Trạng thái Giao Hàng";
            this.colDHStatus.Name = "colDHStatus";
            this.colDHStatus.ReadOnly = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(426, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 16);
            this.label4.TabIndex = 19;
            this.label4.Text = "Trạng thái phiếu xuất hàng:";
            this.label4.Visible = false;
            // 
            // txtStrIsExportFull
            // 
            this.txtStrIsExportFull.Location = new System.Drawing.Point(600, 12);
            this.txtStrIsExportFull.MaxLength = 255;
            this.txtStrIsExportFull.Name = "txtStrIsExportFull";
            this.txtStrIsExportFull.ReadOnly = true;
            this.txtStrIsExportFull.Size = new System.Drawing.Size(117, 22);
            this.txtStrIsExportFull.TabIndex = 18;
            this.txtStrIsExportFull.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(426, 45);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(168, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "Trạng thái phiếu chia hàng:";
            this.label5.Visible = false;
            // 
            // txtStrIsSplitFull
            // 
            this.txtStrIsSplitFull.Location = new System.Drawing.Point(600, 42);
            this.txtStrIsSplitFull.MaxLength = 255;
            this.txtStrIsSplitFull.Name = "txtStrIsSplitFull";
            this.txtStrIsSplitFull.ReadOnly = true;
            this.txtStrIsSplitFull.Size = new System.Drawing.Size(117, 22);
            this.txtStrIsSplitFull.TabIndex = 20;
            this.txtStrIsSplitFull.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(730, 15);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(173, 16);
            this.label6.TabIndex = 23;
            this.label6.Text = "Trạng thái phiếu soạn hàng:";
            this.label6.Visible = false;
            // 
            // txtStrIsPrepareFull
            // 
            this.txtStrIsPrepareFull.Location = new System.Drawing.Point(904, 12);
            this.txtStrIsPrepareFull.MaxLength = 255;
            this.txtStrIsPrepareFull.Name = "txtStrIsPrepareFull";
            this.txtStrIsPrepareFull.ReadOnly = true;
            this.txtStrIsPrepareFull.Size = new System.Drawing.Size(121, 22);
            this.txtStrIsPrepareFull.TabIndex = 22;
            this.txtStrIsPrepareFull.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(730, 43);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 16);
            this.label7.TabIndex = 25;
            this.label7.Text = "Trạng thái phiếu giao hàng:";
            this.label7.Visible = false;
            // 
            // txtStrIsDeliveryFull
            // 
            this.txtStrIsDeliveryFull.Location = new System.Drawing.Point(904, 40);
            this.txtStrIsDeliveryFull.MaxLength = 255;
            this.txtStrIsDeliveryFull.Name = "txtStrIsDeliveryFull";
            this.txtStrIsDeliveryFull.ReadOnly = true;
            this.txtStrIsDeliveryFull.Size = new System.Drawing.Size(121, 22);
            this.txtStrIsDeliveryFull.TabIndex = 24;
            this.txtStrIsDeliveryFull.Visible = false;
            // 
            // btnUpdateRelateDO
            // 
            this.btnUpdateRelateDO.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnUpdateRelateDO.Location = new System.Drawing.Point(1031, 79);
            this.btnUpdateRelateDO.Name = "btnUpdateRelateDO";
            this.btnUpdateRelateDO.Size = new System.Drawing.Size(220, 30);
            this.btnUpdateRelateDO.TabIndex = 29;
            this.btnUpdateRelateDO.Text = "4.Cập nhật DO các phiếu liên quan";
            this.btnUpdateRelateDO.UseVisualStyleBackColor = true;
            this.btnUpdateRelateDO.Visible = false;
            this.btnUpdateRelateDO.Click += new System.EventHandler(this.btnUpdateRelateDO_Click);
            // 
            // btnCorrectAdditionalDO
            // 
            this.btnCorrectAdditionalDO.Image = ((System.Drawing.Image)(resources.GetObject("btnCorrectAdditionalDO.Image")));
            this.btnCorrectAdditionalDO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCorrectAdditionalDO.Location = new System.Drawing.Point(1031, 12);
            this.btnCorrectAdditionalDO.Name = "btnCorrectAdditionalDO";
            this.btnCorrectAdditionalDO.Size = new System.Drawing.Size(217, 30);
            this.btnCorrectAdditionalDO.TabIndex = 28;
            this.btnCorrectAdditionalDO.TabStop = false;
            this.btnCorrectAdditionalDO.Text = "3.Cập nhật DO cho DO Bổ Sung";
            this.btnCorrectAdditionalDO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCorrectAdditionalDO.UseVisualStyleBackColor = true;
            this.btnCorrectAdditionalDO.Visible = false;
            this.btnCorrectAdditionalDO.Click += new System.EventHandler(this.btnCorrectAdditionalDO_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownload.Location = new System.Drawing.Point(91, 100);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(132, 30);
            this.btnDownload.TabIndex = 30;
            this.btnDownload.TabStop = false;
            this.btnDownload.Text = "2.Download LO";
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnDownloadProcess
            // 
            this.btnDownloadProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnDownloadProcess.Image")));
            this.btnDownloadProcess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownloadProcess.Location = new System.Drawing.Point(229, 100);
            this.btnDownloadProcess.Name = "btnDownloadProcess";
            this.btnDownloadProcess.Size = new System.Drawing.Size(217, 30);
            this.btnDownloadProcess.TabIndex = 31;
            this.btnDownloadProcess.TabStop = false;
            this.btnDownloadProcess.Text = "3.Download tiến độ giao hàng";
            this.btnDownloadProcess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownloadProcess.UseVisualStyleBackColor = true;
            this.btnDownloadProcess.Click += new System.EventHandler(this.btnDownloadProcess_Click);
            // 
            // btnTestData
            // 
            this.btnTestData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTestData.Location = new System.Drawing.Point(1031, 43);
            this.btnTestData.Name = "btnTestData";
            this.btnTestData.Size = new System.Drawing.Size(102, 30);
            this.btnTestData.TabIndex = 32;
            this.btnTestData.TabStop = false;
            this.btnTestData.Text = "Refresh Data";
            this.btnTestData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTestData.UseVisualStyleBackColor = true;
            this.btnTestData.Visible = false;
            this.btnTestData.Click += new System.EventHandler(this.btnTestData_Click);
            // 
            // btnConfirmDeliveryFinish
            // 
            this.btnConfirmDeliveryFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmDeliveryFinish.Location = new System.Drawing.Point(452, 100);
            this.btnConfirmDeliveryFinish.Name = "btnConfirmDeliveryFinish";
            this.btnConfirmDeliveryFinish.Size = new System.Drawing.Size(205, 30);
            this.btnConfirmDeliveryFinish.TabIndex = 33;
            this.btnConfirmDeliveryFinish.TabStop = false;
            this.btnConfirmDeliveryFinish.Text = "Xác nhận giao hàng thành công";
            this.btnConfirmDeliveryFinish.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmDeliveryFinish.UseVisualStyleBackColor = true;
            this.btnConfirmDeliveryFinish.Visible = false;
            this.btnConfirmDeliveryFinish.Click += new System.EventHandler(this.btnConfirmDeliveryFinish_Click);
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn19.HeaderText = "Mã sản phẩm";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "ProductName";
            this.dataGridViewTextBoxColumn20.HeaderText = "Tên sản phẩm";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 200;
            // 
            // colQRCode
            // 
            this.colQRCode.DataPropertyName = "StrHasQRCode";
            this.colQRCode.HeaderText = "QRCode";
            this.colQRCode.Name = "colQRCode";
            this.colQRCode.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "BatchCode";
            this.dataGridViewTextBoxColumn21.HeaderText = "Số lô";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "DOQuantity";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "N0";
            dataGridViewCellStyle12.NullValue = null;
            this.dataGridViewTextBoxColumn22.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn22.HeaderText = "Số lượng (LO)";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "ExportedQty";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "N0";
            dataGridViewCellStyle13.NullValue = null;
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridViewTextBoxColumn23.HeaderText = "Đã xuất";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // colPackSize
            // 
            this.colPackSize.DataPropertyName = "PackSize";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colPackSize.DefaultCellStyle = dataGridViewCellStyle14;
            this.colPackSize.HeaderText = "Quy cách";
            this.colPackSize.Name = "colPackSize";
            this.colPackSize.ReadOnly = true;
            // 
            // colCartonOddQuantity
            // 
            this.colCartonOddQuantity.DataPropertyName = "CartonOddQuantity";
            this.colCartonOddQuantity.HeaderText = "Số Thùng/Lẻ";
            this.colCartonOddQuantity.Name = "colCartonOddQuantity";
            this.colCartonOddQuantity.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "ReceivedQty";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle15.Format = "N0";
            this.dataGridViewTextBoxColumn24.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn24.HeaderText = "Đã nhận";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // colNeedExportQuantity
            // 
            this.colNeedExportQuantity.DataPropertyName = "NeedExportQuantity";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle16.Format = "N0";
            this.colNeedExportQuantity.DefaultCellStyle = dataGridViewCellStyle16;
            this.colNeedExportQuantity.HeaderText = "Cần xuất";
            this.colNeedExportQuantity.Name = "colNeedExportQuantity";
            this.colNeedExportQuantity.ReadOnly = true;
            // 
            // colNeedReceiveQuantity
            // 
            this.colNeedReceiveQuantity.DataPropertyName = "NeedReceiveQuantity";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle17.Format = "N0";
            this.colNeedReceiveQuantity.DefaultCellStyle = dataGridViewCellStyle17;
            this.colNeedReceiveQuantity.HeaderText = "Cần nhận";
            this.colNeedReceiveQuantity.Name = "colNeedReceiveQuantity";
            this.colNeedReceiveQuantity.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "DeliveryDate";
            dataGridViewCellStyle19.Format = "dd/MM/yyyy";
            dataGridViewCellStyle19.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn6.HeaderText = "Ngày giao hàng";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "CompanyCode";
            this.dataGridViewTextBoxColumn12.HeaderText = "Mã công ty";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "CompanyName";
            this.dataGridViewTextBoxColumn13.HeaderText = "Tên công ty";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 200;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn7.HeaderText = "Mã sản phẩm";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "ProductName";
            this.dataGridViewTextBoxColumn8.HeaderText = "Tên sản phẩm";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 200;
            // 
            // colHasQRCode1
            // 
            this.colHasQRCode1.DataPropertyName = "StrHasQRCode";
            this.colHasQRCode1.HeaderText = "QRCode";
            this.colHasQRCode1.Name = "colHasQRCode1";
            this.colHasQRCode1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "BatchCode";
            this.dataGridViewTextBoxColumn9.HeaderText = "Số lô";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "DOQuantity";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N0";
            dataGridViewCellStyle20.NullValue = null;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn14.HeaderText = "Số lượng (LO)";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "PreparedQty";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.Format = "N0";
            dataGridViewCellStyle21.NullValue = null;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn15.HeaderText = "Đã soạn";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // colDeliveredQty2
            // 
            this.colDeliveredQty2.DataPropertyName = "DeliveredQty";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle22.Format = "N0";
            this.colDeliveredQty2.DefaultCellStyle = dataGridViewCellStyle22;
            this.colDeliveredQty2.HeaderText = "Đã giao";
            this.colDeliveredQty2.Name = "colDeliveredQty2";
            this.colDeliveredQty2.ReadOnly = true;
            // 
            // colNeedPrepareQty
            // 
            this.colNeedPrepareQty.DataPropertyName = "NeedPrepareQty";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle23.Format = "N0";
            this.colNeedPrepareQty.DefaultCellStyle = dataGridViewCellStyle23;
            this.colNeedPrepareQty.HeaderText = "Cần soạn";
            this.colNeedPrepareQty.Name = "colNeedPrepareQty";
            this.colNeedPrepareQty.ReadOnly = true;
            // 
            // colNeedDeliveryQty
            // 
            this.colNeedDeliveryQty.DataPropertyName = "NeedDeliveryQty";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle24.Format = "N0";
            this.colNeedDeliveryQty.DefaultCellStyle = dataGridViewCellStyle24;
            this.colNeedDeliveryQty.HeaderText = "Cần giao";
            this.colNeedDeliveryQty.Name = "colNeedDeliveryQty";
            this.colNeedDeliveryQty.ReadOnly = true;
            // 
            // DeliveryOrderView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 653);
            this.Controls.Add(this.btnConfirmDeliveryFinish);
            this.Controls.Add(this.btnTestData);
            this.Controls.Add(this.btnDownloadProcess);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.btnUpdateRelateDO);
            this.Controls.Add(this.btnCorrectAdditionalDO);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtStrIsDeliveryFull);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtStrIsPrepareFull);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtStrIsSplitFull);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtStrIsExportFull);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtStatusDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnUpload);
            this.Name = "DeliveryOrderView";
            this.Text = "ProductPackingMaintView";
            this.Load += new System.EventHandler(this.DeliveryOrderView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDODetail)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgExportProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsExportProcess)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDOProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDOProcess)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletList)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDocumentRelated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsDetails;
        private System.Windows.Forms.Button btnUpload;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatusDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgDODetail;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dtgDocumentRelated;
        private System.Windows.Forms.BindingSource bdsReport;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStrIsExportFull;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStrIsSplitFull;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtStrIsPrepareFull;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtStrIsDeliveryFull;
        private System.Windows.Forms.Button btnCorrectAdditionalDO;
        private System.Windows.Forms.Button btnUpdateRelateDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExportCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplitCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrepareCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryTicketCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyCode1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEXStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSPStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPRStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDHStatus;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dtgDOProcess;
        private System.Windows.Forms.BindingSource bdsDOProcess;
        private System.Windows.Forms.Button btnDownloadProcess;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dtgExportProcess;
        private System.Windows.Forms.BindingSource bdsExportProcess;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dtgPallet;
        private System.Windows.Forms.BindingSource bdsPalletList;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.Button btnTestData;
        private System.Windows.Forms.Button btnConfirmDeliveryFinish;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Device;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProvinceShipTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_MFG;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChangedQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChangeReason;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequireReturnQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQRCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonOddQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNeedExportQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNeedReceiveQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHasQRCode1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveredQty2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNeedPrepareQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNeedDeliveryQty;
    }
}