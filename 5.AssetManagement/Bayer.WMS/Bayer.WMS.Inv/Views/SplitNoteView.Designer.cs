﻿namespace Bayer.WMS.Inv.Views
{
    partial class SplitNoteView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.colProductionPlan_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplittedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCodes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequireReturnQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReturnedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatusDisplay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDeliveryDate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDOImportCode = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToAddRows = false;
            this.dtgProductPacking.AllowUserToDeleteRows = false;
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductPacking.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductionPlan_ProductDescription,
            this.colProductionPlan_Quantity,
            this.colCompanyCode,
            this.colCompanyName,
            this.colProductionPlan_PackageSize,
            this.colProductionPlan_PackageQuantity,
            this.colBatchCodeDistributor,
            this.colPackSize,
            this.PackQuantity,
            this.colPackType,
            this.ProductQuantity,
            this.colSplittedQty,
            this.colPalletCodes,
            this.colRequireReturnQty,
            this.colReturnedQty});
            this.dtgProductPacking.DataSource = this.bdsDetails;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(14, 132);
            this.dtgProductPacking.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.ReadOnly = true;
            this.dtgProductPacking.Size = new System.Drawing.Size(1351, 409);
            this.dtgProductPacking.TabIndex = 8;
            // 
            // colProductionPlan_ProductDescription
            // 
            this.colProductionPlan_ProductDescription.DataPropertyName = "ProductCode";
            this.colProductionPlan_ProductDescription.HeaderText = "Mã Sản Phẩm";
            this.colProductionPlan_ProductDescription.Name = "colProductionPlan_ProductDescription";
            this.colProductionPlan_ProductDescription.ReadOnly = true;
            // 
            // colProductionPlan_Quantity
            // 
            this.colProductionPlan_Quantity.DataPropertyName = "ProductDescription";
            dataGridViewCellStyle2.NullValue = null;
            this.colProductionPlan_Quantity.DefaultCellStyle = dataGridViewCellStyle2;
            this.colProductionPlan_Quantity.HeaderText = "Mô Tả";
            this.colProductionPlan_Quantity.Name = "colProductionPlan_Quantity";
            this.colProductionPlan_Quantity.ReadOnly = true;
            this.colProductionPlan_Quantity.Width = 200;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.DataPropertyName = "CompanyCode";
            this.colCompanyCode.HeaderText = "Mã khách hàng";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.ReadOnly = true;
            // 
            // colCompanyName
            // 
            this.colCompanyName.DataPropertyName = "CompanyName";
            this.colCompanyName.HeaderText = "Tên khách hàng";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.ReadOnly = true;
            this.colCompanyName.Width = 200;
            // 
            // colProductionPlan_PackageSize
            // 
            this.colProductionPlan_PackageSize.DataPropertyName = "BatchCode";
            dataGridViewCellStyle3.NullValue = null;
            this.colProductionPlan_PackageSize.DefaultCellStyle = dataGridViewCellStyle3;
            this.colProductionPlan_PackageSize.HeaderText = "Số Lô";
            this.colProductionPlan_PackageSize.Name = "colProductionPlan_PackageSize";
            this.colProductionPlan_PackageSize.ReadOnly = true;
            // 
            // colProductionPlan_PackageQuantity
            // 
            this.colProductionPlan_PackageQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            this.colProductionPlan_PackageQuantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductionPlan_PackageQuantity.HeaderText = "Số lượng";
            this.colProductionPlan_PackageQuantity.Name = "colProductionPlan_PackageQuantity";
            this.colProductionPlan_PackageQuantity.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "Số Lô NCC";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            this.colBatchCodeDistributor.ReadOnly = true;
            this.colBatchCodeDistributor.Visible = false;
            // 
            // colPackSize
            // 
            this.colPackSize.DataPropertyName = "PackSize";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colPackSize.DefaultCellStyle = dataGridViewCellStyle5;
            this.colPackSize.HeaderText = "Quy cách";
            this.colPackSize.Name = "colPackSize";
            this.colPackSize.ReadOnly = true;
            // 
            // PackQuantity
            // 
            this.PackQuantity.DataPropertyName = "PackQuantity";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.PackQuantity.DefaultCellStyle = dataGridViewCellStyle6;
            this.PackQuantity.HeaderText = "Thùng chẵn";
            this.PackQuantity.Name = "PackQuantity";
            this.PackQuantity.ReadOnly = true;
            // 
            // colPackType
            // 
            this.colPackType.DataPropertyName = "StrPackType";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colPackType.DefaultCellStyle = dataGridViewCellStyle7;
            this.colPackType.HeaderText = "Loại Thùng";
            this.colPackType.Name = "colPackType";
            this.colPackType.ReadOnly = true;
            // 
            // ProductQuantity
            // 
            this.ProductQuantity.DataPropertyName = "ProductQuantity";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = null;
            this.ProductQuantity.DefaultCellStyle = dataGridViewCellStyle8;
            this.ProductQuantity.HeaderText = "Gói lẻ";
            this.ProductQuantity.Name = "ProductQuantity";
            this.ProductQuantity.ReadOnly = true;
            // 
            // colSplittedQty
            // 
            this.colSplittedQty.DataPropertyName = "SplittedQty";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            this.colSplittedQty.DefaultCellStyle = dataGridViewCellStyle9;
            this.colSplittedQty.HeaderText = "SL đã chia";
            this.colSplittedQty.Name = "colSplittedQty";
            this.colSplittedQty.ReadOnly = true;
            // 
            // colPalletCodes
            // 
            this.colPalletCodes.DataPropertyName = "PalletCodes";
            this.colPalletCodes.HeaderText = "Pallet";
            this.colPalletCodes.Name = "colPalletCodes";
            this.colPalletCodes.ReadOnly = true;
            // 
            // colRequireReturnQty
            // 
            this.colRequireReturnQty.DataPropertyName = "RequireReturnQty";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N0";
            this.colRequireReturnQty.DefaultCellStyle = dataGridViewCellStyle10;
            this.colRequireReturnQty.HeaderText = "SL yêu cầu trả";
            this.colRequireReturnQty.Name = "colRequireReturnQty";
            this.colRequireReturnQty.ReadOnly = true;
            // 
            // colReturnedQty
            // 
            this.colReturnedQty.DataPropertyName = "ReturnedQty";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N0";
            this.colReturnedQty.DefaultCellStyle = dataGridViewCellStyle11;
            this.colReturnedQty.HeaderText = "SL đã trả";
            this.colReturnedQty.Name = "colReturnedQty";
            this.colReturnedQty.ReadOnly = true;
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(102, 12);
            this.cmbCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(450, 28);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 12;
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(102, 52);
            this.txtProductDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtProductDescription.MaxLength = 255;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(450, 26);
            this.txtProductDescription.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 56);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "Mô tả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã:";
            // 
            // txtStatusDisplay
            // 
            this.txtStatusDisplay.Location = new System.Drawing.Point(700, 52);
            this.txtStatusDisplay.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatusDisplay.MaxLength = 255;
            this.txtStatusDisplay.Name = "txtStatusDisplay";
            this.txtStatusDisplay.ReadOnly = true;
            this.txtStatusDisplay.Size = new System.Drawing.Size(223, 26);
            this.txtStatusDisplay.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(572, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "Trạng thái:";
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Location = new System.Drawing.Point(700, 12);
            this.txtDeliveryDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDeliveryDate.MaxLength = 255;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.ReadOnly = true;
            this.txtDeliveryDate.Size = new System.Drawing.Size(223, 26);
            this.txtDeliveryDate.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(572, 16);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 20);
            this.label7.TabIndex = 25;
            this.label7.Text = "Ngày giao hàng:";
            // 
            // txtDOImportCode
            // 
            this.txtDOImportCode.Location = new System.Drawing.Point(102, 92);
            this.txtDOImportCode.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDOImportCode.MaxLength = 255;
            this.txtDOImportCode.Name = "txtDOImportCode";
            this.txtDOImportCode.ReadOnly = true;
            this.txtDOImportCode.Size = new System.Drawing.Size(223, 26);
            this.txtDOImportCode.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 96);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 20);
            this.label8.TabIndex = 33;
            this.label8.Text = "Mã LO:";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(791, 88);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(84, 38);
            this.btnCancel.TabIndex = 35;
            this.btnCancel.Text = "Thoát";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Location = new System.Drawing.Point(700, 88);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 38);
            this.btnSave.TabIndex = 34;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // SplitNoteView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1378, 556);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtDOImportCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDeliveryDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtStatusDisplay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtgProductPacking);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "SplitNoteView";
            this.Text = "Chia hàng";
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.BindingSource bdsDetails;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatusDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDeliveryDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDOImportCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplittedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCodes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequireReturnQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReturnedQty;
    }
}