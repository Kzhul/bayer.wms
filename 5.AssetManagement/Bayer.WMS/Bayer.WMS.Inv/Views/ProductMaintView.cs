﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs;
using Bayer.WMS.CustomControls;
using System.Data.Entity;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class ProductMaintView : BaseForm, IProductMaintView
    {
        public ProductMaintView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductPacking);

            toolTip1.SetToolTip(btnProductPacking_Insert, "Thêm quy cách đóng gói");
            toolTip1.SetToolTip(btnProductPacking_Delete, "Xóa quy cách đóng gói");
        }

        private void cmbCategoryID_SelectedValueChanged(object sender, EventArgs e)
        {
            txtCategoryID_ReadOnly.Text = cmbCategoryID.Text;
        }

        private void cmbType_SelectedValueChanged(object sender, EventArgs e)
        {
            txtType_ReadOnly.Text = cmbType.Text;
            string value = $"{cmbPackingType.SelectedValue}";
            switch (value)
            {
                case Product.type.F:
                    cmbPackingType.Enabled = true;
                    break;
                case Product.type.M:
                case Product.type.P:
                    cmbPackingType.Enabled = false;
                    cmbPackingType.SelectedValue = null;
                    break;
                default:
                    break;
            }
        }

        private void cmbUOM_SelectedValueChanged(object sender, EventArgs e)
        {
            txtUOM_ReadOnly.Text = cmbUOM.Text;
        }

        private void cmbPackingType_SelectedValueChanged(object sender, EventArgs e)
        {
            txtPackingType_ReadOnly.Text = cmbPackingType.Text;
        }

        private void cmbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            txtStatus_ReadOnly.Text = cmbStatus.Text;
        }

        private void btnProductPacking_Insert_Click(object sender, EventArgs e)
        {
            bdsProductPacking.AddNew();
        }

        private void btnProductPacking_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtgProductPacking.SelectedRows)
                {
                    var productPacking = bdsProductPacking[row.Index] as ProductPacking;

                    bdsProductPacking.Remove(productPacking);
                    (_presenter as IProductMaintPresenter).DeleteProductPacking(productPacking);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        private void dtgProductPacking_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dtgProductPacking.CurrentCell.ColumnIndex == colProductPacking_Type.Index)
            {
                dtgProductPacking.EndEdit();
                if (dtgProductPacking.IsCurrentCellDirty)
                {
                    dtgProductPacking.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
        }

        public override void InitializeComboBox()
        {
            cmbType.ValueMember = Product.type.ValueMember;
            cmbType.DisplayMember = Product.type.DisplayMember;
            cmbType.DataSource = Product.type.Get();

            cmbCategoryID.ValueMember = "CategoryID";
            cmbCategoryID.DisplayMember = "Description";

            cmbUOM.ValueMember = "UOM1";
            cmbUOM.DisplayMember = "UOM1";

            cmbPackingType.ValueMember = Product.packingType.ValueMember;
            cmbPackingType.DisplayMember = Product.packingType.DisplayMember;
            cmbPackingType.DataSource = Product.packingType.GetWithAll();

            cmbStatus.ValueMember = Product.status.ValueMember;
            cmbStatus.DisplayMember = Product.status.DisplayMember;
            cmbStatus.DataSource = Product.status.Get();

            cmbProductCode.DropDownWidth = 700;
            cmbProductCode.PageSize = 20;
            cmbProductCode.ValueMember = "ProductID";
            cmbProductCode.DisplayMember = "ProductCode - Description";
            cmbProductCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("ProductCode", "Mã sản phẩm", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("CategoryDescription", "Nhóm sản phẩm", 150),
                new MultiColumnComboBox.ComboBoxColumn("TypeDisplay", "Loại sản phẩm", 150),
                new MultiColumnComboBox.ComboBoxColumn("UOM", "ĐVT", 80),
                new MultiColumnComboBox.ComboBoxColumn("PackingTypeDisplay", "Loại đóng gói", 150),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbProductCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadProducts", "OnProductCodeSelectChange");

            colProductPacking_Type.ValueMember = ProductPacking.type.ValueMember;
            colProductPacking_Type.DisplayMember = ProductPacking.type.DisplayMember;
            colProductPacking_Type.DataSource = ProductPacking.type.Get();
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var productMaintPresenter = _presenter as IProductMaintPresenter;

                await Task.WhenAll(productMaintPresenter.LoadProducts(), productMaintPresenter.LoadCategories(), productMaintPresenter.LoadUOMs());

                if (!isRefresh)
                    productMaintPresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DataTable Products
        {
            set { cmbProductCode.Source = value; }
        }

        public IList<Category> Categories
        {
            set
            {
                cmbCategoryID.SelectedValueChanged -= cmbCategoryID_SelectedValueChanged;

                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        cmbCategoryID.DataSource = value;
                    });
                }
                else
                {
                    cmbCategoryID.DataSource = value;
                }

                cmbCategoryID.SelectedValueChanged += cmbCategoryID_SelectedValueChanged;
            }
        }

        public IList<UOM> UOMs
        {
            set
            {
                cmbUOM.SelectedValueChanged -= cmbUOM_SelectedValueChanged;

                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        cmbUOM.DataSource = value;
                    });
                }
                else
                {
                    cmbUOM.DataSource = value;
                }

                cmbUOM.SelectedValueChanged += cmbUOM_SelectedValueChanged;
            }
        }

        private IList<ProductPacking> _productPackings;

        public IList<ProductPacking> ProductPackings
        {
            get { return _productPackings; }
            set
            {
                _productPackings = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsProductPacking.DataSource = _productPackings;
                    });
                }
                else
                {
                    bdsProductPacking.DataSource = _productPackings;
                }
            }
        }

        private IList<ProductPacking> _deletedProductPackings;

        public IList<ProductPacking> DeletedProductPackings
        {
            get { return _deletedProductPackings; }
            set { _deletedProductPackings = value; }
        }

        private Product _product;

        public Product Product
        {
            get
            {
                return _product;
            }
            set
            {
                _product = value;

                cmbProductCode.DataBindings.Clear();
                txtProductDescription.DataBindings.Clear();
                cmbCategoryID.DataBindings.Clear();
                cmbType.DataBindings.Clear();
                cmbUOM.DataBindings.Clear();
                cmbPackingType.DataBindings.Clear();
                nudSampleQty.DataBindings.Clear();
                nudPrintLabelPercentage.DataBindings.Clear();
                cmbStatus.DataBindings.Clear();
                nudPalletSize.DataBindings.Clear();
                nudQuantityByUnit.DataBindings.Clear();
                nudQuantityByCarton.DataBindings.Clear();

                cmbProductCode.DataBindings.Add("Text", Product, "ProductCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProductDescription.DataBindings.Add("Text", Product, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbCategoryID.DataBindings.Add("SelectedValue", Product, "CategoryID", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbType.DataBindings.Add("SelectedValue", Product, "Type", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbUOM.DataBindings.Add("SelectedValue", Product, "UOM", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbPackingType.DataBindings.Add("SelectedValue", Product, "PackingType", true, DataSourceUpdateMode.OnPropertyChanged);
                nudSampleQty.DataBindings.Add("Value", Product, "SampleQty", true, DataSourceUpdateMode.OnPropertyChanged);
                nudPrintLabelPercentage.DataBindings.Add("Value", Product, "PrintLabelPercentage", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbStatus.DataBindings.Add("SelectedValue", Product, "Status", true, DataSourceUpdateMode.OnPropertyChanged);
                nudPalletSize.DataBindings.Add("Value", Product, "PalletSize", true, DataSourceUpdateMode.OnPropertyChanged);
                nudQuantityByUnit.DataBindings.Add("Value", Product, "QuantityByUnit", true, DataSourceUpdateMode.OnPropertyChanged);
                nudQuantityByCarton.DataBindings.Add("Value", Product, "QuantityByCarton", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        public int SampleQty
        {
            get { return Convert.ToInt32(nudSampleQty.Value); }
        }

        public decimal PrintLabelPercentage
        {
            get { return nudPrintLabelPercentage.Value; }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    txtProductDescription.ReadOnly = true;
                    txtCategoryID_ReadOnly.BringToFront();
                    txtType_ReadOnly.BringToFront();
                    txtUOM_ReadOnly.BringToFront();
                    txtPackingType_ReadOnly.BringToFront();
                    nudSampleQty.ReadOnly = true;
                    nudPrintLabelPercentage.ReadOnly = true;
                    txtStatus_ReadOnly.BringToFront();
                    dtgProductPacking.ReadOnly = true;
                    btnProductPacking_Insert.Enabled = false;
                    btnProductPacking_Delete.Enabled = false;
                    nudPalletSize.ReadOnly = true;
                    nudQuantityByUnit.ReadOnly = true;
                    nudQuantityByCarton.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }
    }
}
