﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class AssetManagementView : BaseForm, IAssetManagementView
    {
        public AssetManagementView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgTotal);
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);
        }
        
        private DataTable _ReportBay4sTotals;

        public DataTable ReportBay4sTotals
        {
            get { return _ReportBay4sTotals; }
            set
            {
                _ReportBay4sTotals = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _ReportBay4sTotals;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _ReportBay4sTotals;
                }
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }

        public async void Print()
        {
            var doPresenter = _presenter as IAssetManagementPresenter;
            await doPresenter.Print();
        }

        public string _fileName;
        private async void btnSave_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        _fileName = ofd.FileName;
                        await (_presenter as IAssetManagementPresenter).ImportExcel(_fileName, string.Empty);
                    }
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnSave.Enabled = true;
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IAssetManagementPresenter;
            await doPresenter.Print();
        }

        private async void btnDownloadTemplate_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IAssetManagementPresenter;
            await doPresenter.DownloadTemplate();
        }
    }
}
