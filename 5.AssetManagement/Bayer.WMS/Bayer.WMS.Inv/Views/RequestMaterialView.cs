﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs;
using System.Reflection;
using Bayer.WMS.CustomControls;

namespace Bayer.WMS.Inv.Views
{
    public partial class RequestMaterialView : BaseForm, IRequestMaterialView
    {
        public RequestMaterialView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgRequestMaterial);
            SetDoubleBuffered(dtgSplit);
            tbpRequestMaterial_Detail.TabPages.Remove(tabPage3);
        }

        private async void btnUpload_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "(Tệp .xlsx)|*.xlsx|(Tệp .xls)|*.xls";
                    if (ofd.ShowDialog() != DialogResult.OK)
                        return;

                    var presenter = _presenter as IRequestMaterialPresenter;
                    await presenter.ImportExcel(ofd.FileName);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnUpload.Enabled = true;
            }
        }

        private async void btnSuggest_Click(object sender, EventArgs e)
        {
            btnSuggest.Enabled = false;
            try
            {
                var presenter = _presenter as IRequestMaterialPresenter;
                await presenter.SuggestPickupProductFEFO();

                tbpRequestMaterial_Detail.SelectedTab = tabPage2;
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnSuggest.Enabled = true;
            }
        }

        public override void InitializeComboBox()
        {
            cmbDocumentNbr.DropDownWidth = 700;
            cmbDocumentNbr.PageSize = 20;
            cmbDocumentNbr.ValueMember = "DocumentNbr";
            cmbDocumentNbr.DisplayMember = "DocumentNbr";
            cmbDocumentNbr.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("DocumentNbr", "Số phiếu", 100),
                new MultiColumnComboBox.ComboBoxColumn("Note", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbDocumentNbr.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadRequestMaterials", "OnDocumentNbrSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var reqMaterialPresenter = _presenter as IRequestMaterialPresenter;

                await Task.WhenAll(reqMaterialPresenter.LoadRequestMaterials());

                if (!isRefresh)
                    reqMaterialPresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DataTable RequestMaterials { set => cmbDocumentNbr.Source = value; }

        private RequestMaterial _reqMaterial;

        public RequestMaterial RequestMaterial
        {
            get => _reqMaterial;
            set
            {
                _reqMaterial = value;
                cmbDocumentNbr.Text = _reqMaterial.DocumentNbr;
                txtNote.DataBindings.Clear();
                dtpDate.DataBindings.Clear();
                txtStatus.DataBindings.Clear();

                txtNote.DataBindings.Add("Text", RequestMaterial, "Note", true, DataSourceUpdateMode.OnPropertyChanged);
                dtpDate.DataBindings.Add("Value", RequestMaterial, "Date", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStatus.DataBindings.Add("Text", RequestMaterial, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private IList<RequestMaterialLine> _reqMaterialLines;

        public IList<RequestMaterialLine> RequestMaterialLines
        {
            get => _reqMaterialLines;
            set
            {
                _reqMaterialLines = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsRequestMaterial.DataSource = value;
                    });
                }
                else
                {
                    bdsRequestMaterial.DataSource = value;
                }
            }
        }

        private System.Data.DataTable _reqMaterialLineSplits;

        public System.Data.DataTable RequestMaterialLineSplits
        {
            get => _reqMaterialLineSplits;
            set
            {
                _reqMaterialLineSplits = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsRequestMaterialSplit.DataSource = null;
                        bdsRequestMaterialSplit.DataSource = value;
                    });
                }
                else
                {
                    bdsRequestMaterialSplit.DataSource = null;
                    bdsRequestMaterialSplit.DataSource = value;
                }
            }
        }

        private IList<RequestMaterialLineSplit> _deletedRequestMaterialLineSplits;

        public IList<RequestMaterialLineSplit> DeletedRequestMaterialLineSplits
        {
            get { return _deletedRequestMaterialLineSplits; }
            set { _deletedRequestMaterialLineSplits = value; }
        }

        public override void HandlePermission(string accessRight)
        {
            //// permission gen QR code
            string prmSuggest = Utility.Sitemaps.First(p => p.SitemapID == 42).AccessRights;

            if (prmSuggest == RoleSitemap.accessRights.NotSet || prmSuggest == RoleSitemap.accessRights.Read)
            {
                btnSuggest.Enabled = false;
                btnSuggest.Click -= btnSuggest_Click;
            }
        }

        private void btnImportLineSplit_Click(object sender, EventArgs e)
        {

        }

        private void btnDownload_Click(object sender, EventArgs e)
        {

        }

        private async void btnConfirmExport_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IRequestMaterialPresenter;
            await doPresenter.WarehouseKeeperBookPallet();
        }

        private void btnPrintExportPallet_Click(object sender, EventArgs e)
        {

        }
    }
}
