﻿namespace Bayer.WMS.Inv.Views
{
    partial class StockReceivingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StockReceivingView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtCreatedBy = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnDetail_Delete = new System.Windows.Forms.Button();
            this.btnDetail_Upload = new System.Windows.Forms.Button();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.lblNCC = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtImportStatus = new System.Windows.Forms.TextBox();
            this.txtVerifyStatus = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDownload = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgDetail = new System.Windows.Forms.DataGridView();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dtgDetailSplit = new System.Windows.Forms.DataGridView();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductLot_NBR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExpiredDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackageQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceiver = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceiveDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colForklift = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSetLocationDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsDetailSplits = new System.Windows.Forms.BindingSource(this.components);
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPrintPalletLabel = new System.Windows.Forms.Button();
            this.btnConfirmImportToWarehouse = new System.Windows.Forms.Button();
            this.colSTT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_POCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackageQuantity1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_QtyPlanning = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_QtyReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_PalletPlanning = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_PalletReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletSetLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDetailSplit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetailSplits)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(125, 12);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(400, 24);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã:";
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Location = new System.Drawing.Point(720, 102);
            this.txtCreatedBy.MaxLength = 255;
            this.txtCreatedBy.Multiline = true;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.ReadOnly = true;
            this.txtCreatedBy.Size = new System.Drawing.Size(150, 24);
            this.txtCreatedBy.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(608, 105);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Người tạo phiếu:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(584, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 16);
            this.label7.TabIndex = 25;
            this.label7.Text = "Ngày nhận hàng:";
            // 
            // btnDetail_Delete
            // 
            this.btnDetail_Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetail_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btnDetail_Delete.Image")));
            this.btnDetail_Delete.Location = new System.Drawing.Point(1238, 112);
            this.btnDetail_Delete.Name = "btnDetail_Delete";
            this.btnDetail_Delete.Size = new System.Drawing.Size(50, 30);
            this.btnDetail_Delete.TabIndex = 27;
            this.btnDetail_Delete.TabStop = false;
            this.btnDetail_Delete.UseVisualStyleBackColor = true;
            this.btnDetail_Delete.Click += new System.EventHandler(this.btnDetail_Delete_Click);
            // 
            // btnDetail_Upload
            // 
            this.btnDetail_Upload.Image = ((System.Drawing.Image)(resources.GetObject("btnDetail_Upload.Image")));
            this.btnDetail_Upload.Location = new System.Drawing.Point(13, 112);
            this.btnDetail_Upload.Name = "btnDetail_Upload";
            this.btnDetail_Upload.Size = new System.Drawing.Size(105, 30);
            this.btnDetail_Upload.TabIndex = 7;
            this.btnDetail_Upload.TabStop = false;
            this.btnDetail_Upload.UseVisualStyleBackColor = true;
            this.btnDetail_Upload.Click += new System.EventHandler(this.btnDetail_Upload_Click);
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(125, 42);
            this.txtCompanyName.MaxLength = 255;
            this.txtCompanyName.Multiline = true;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(400, 24);
            this.txtCompanyName.TabIndex = 2;
            // 
            // lblNCC
            // 
            this.lblNCC.AutoSize = true;
            this.lblNCC.Location = new System.Drawing.Point(13, 45);
            this.lblNCC.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNCC.Name = "lblNCC";
            this.lblNCC.Size = new System.Drawing.Size(94, 16);
            this.lblNCC.TabIndex = 50;
            this.lblNCC.Text = "Nhà cung cấp:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(584, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 16);
            this.label4.TabIndex = 81;
            this.label4.Text = "Trạng thái nhập kho:";
            // 
            // dtpDeliveryDate
            // 
            this.dtpDeliveryDate.CustomFormat = "dd.MM.yyyy";
            this.dtpDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDeliveryDate.Location = new System.Drawing.Point(720, 12);
            this.dtpDeliveryDate.Name = "dtpDeliveryDate";
            this.dtpDeliveryDate.Size = new System.Drawing.Size(90, 22);
            this.dtpDeliveryDate.TabIndex = 4;
            // 
            // txtImportStatus
            // 
            this.txtImportStatus.Location = new System.Drawing.Point(720, 42);
            this.txtImportStatus.MaxLength = 255;
            this.txtImportStatus.Multiline = true;
            this.txtImportStatus.Name = "txtImportStatus";
            this.txtImportStatus.ReadOnly = true;
            this.txtImportStatus.Size = new System.Drawing.Size(150, 24);
            this.txtImportStatus.TabIndex = 83;
            // 
            // txtVerifyStatus
            // 
            this.txtVerifyStatus.Location = new System.Drawing.Point(720, 72);
            this.txtVerifyStatus.MaxLength = 255;
            this.txtVerifyStatus.Multiline = true;
            this.txtVerifyStatus.Name = "txtVerifyStatus";
            this.txtVerifyStatus.ReadOnly = true;
            this.txtVerifyStatus.Size = new System.Drawing.Size(150, 24);
            this.txtVerifyStatus.TabIndex = 84;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(584, 75);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 16);
            this.label2.TabIndex = 85;
            this.label2.Text = "Trạng thái kiểm tra:";
            // 
            // btnDownload
            // 
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownload.Location = new System.Drawing.Point(125, 112);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(108, 30);
            this.btnDownload.TabIndex = 86;
            this.btnDownload.TabStop = false;
            this.btnDownload.Text = "2.Download";
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 148);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1285, 473);
            this.tabControl1.TabIndex = 87;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1277, 444);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Chi tiết phiếu nhập kho";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgDetail
            // 
            this.dtgDetail.AllowUserToAddRows = false;
            this.dtgDetail.AllowUserToDeleteRows = false;
            this.dtgDetail.AllowUserToOrderColumns = true;
            this.dtgDetail.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgDetail.AutoGenerateColumns = false;
            this.dtgDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSTT,
            this.colDetail_ProductCode,
            this.colDetail_ProductDescription,
            this.colBatchCode,
            this.colBatchCodeDistributor,
            this.colExpiryDate,
            this.colDetail_POCode,
            this.colDetail_UOM,
            this.colPackageQuantity1,
            this.colDetail_QtyPlanning,
            this.colDetail_QtyReceived,
            this.colDetail_PalletPlanning,
            this.colDetail_PalletReceived,
            this.colPalletSetLocation,
            this.colDetail_Description,
            this.colDetail_Status});
            this.dtgDetail.DataSource = this.bdsDetails;
            this.dtgDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dtgDetail.Location = new System.Drawing.Point(1, 1);
            this.dtgDetail.Name = "dtgDetail";
            this.dtgDetail.Size = new System.Drawing.Size(1276, 440);
            this.dtgDetail.TabIndex = 10;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgDetailSplit);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1277, 444);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Chi tiết pallet nhập hàng";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dtgDetailSplit
            // 
            this.dtgDetailSplit.AllowUserToAddRows = false;
            this.dtgDetailSplit.AllowUserToDeleteRows = false;
            this.dtgDetailSplit.AllowUserToOrderColumns = true;
            this.dtgDetailSplit.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Azure;
            this.dtgDetailSplit.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dtgDetailSplit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgDetailSplit.AutoGenerateColumns = false;
            this.dtgDetailSplit.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgDetailSplit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDetailSplit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colStatus,
            this.colLocationName,
            this.colProductLot_NBR,
            this.colPalletCode,
            this.colProductCode,
            this.colProductName,
            this.colExpiredDate,
            this.colProductLot,
            this.colQuantity,
            this.colUOM,
            this.colPackageQuantity,
            this.colReceiver,
            this.colReceiveDateTime,
            this.colForklift,
            this.colSetLocationDateTime});
            this.dtgDetailSplit.DataSource = this.bdsDetailSplits;
            this.dtgDetailSplit.GridColor = System.Drawing.SystemColors.Control;
            this.dtgDetailSplit.Location = new System.Drawing.Point(0, 1);
            this.dtgDetailSplit.Name = "dtgDetailSplit";
            this.dtgDetailSplit.ReadOnly = true;
            this.dtgDetailSplit.Size = new System.Drawing.Size(1274, 443);
            this.dtgDetailSplit.TabIndex = 11;
            // 
            // colStatus
            // 
            this.colStatus.DataPropertyName = "StrStatus";
            this.colStatus.HeaderText = "Trạng thái";
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            // 
            // colLocationName
            // 
            this.colLocationName.DataPropertyName = "LocationName";
            this.colLocationName.HeaderText = "Vị trí";
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.ReadOnly = true;
            // 
            // colProductLot_NBR
            // 
            this.colProductLot_NBR.DataPropertyName = "ProductLot_NBR";
            this.colProductLot_NBR.HeaderText = "STT theo Lô";
            this.colProductLot_NBR.Name = "colProductLot_NBR";
            this.colProductLot_NBR.ReadOnly = true;
            // 
            // colPalletCode
            // 
            this.colPalletCode.DataPropertyName = "PalletCode";
            this.colPalletCode.HeaderText = "Pallet";
            this.colPalletCode.Name = "colPalletCode";
            this.colPalletCode.ReadOnly = true;
            // 
            // colProductCode
            // 
            this.colProductCode.DataPropertyName = "ProductCode";
            this.colProductCode.HeaderText = "Mã NL";
            this.colProductCode.Name = "colProductCode";
            this.colProductCode.ReadOnly = true;
            // 
            // colProductName
            // 
            this.colProductName.DataPropertyName = "ProductName";
            this.colProductName.HeaderText = "Tên nguyên liệu";
            this.colProductName.Name = "colProductName";
            this.colProductName.ReadOnly = true;
            // 
            // colExpiredDate
            // 
            this.colExpiredDate.DataPropertyName = "StrExpiredDate";
            this.colExpiredDate.HeaderText = "Hạn sử dụng";
            this.colExpiredDate.Name = "colExpiredDate";
            this.colExpiredDate.ReadOnly = true;
            // 
            // colProductLot
            // 
            this.colProductLot.DataPropertyName = "ProductLot";
            this.colProductLot.HeaderText = "Số lô";
            this.colProductLot.Name = "colProductLot";
            this.colProductLot.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N0";
            this.colQuantity.DefaultCellStyle = dataGridViewCellStyle10;
            this.colQuantity.HeaderText = "Số lượng";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.ReadOnly = true;
            // 
            // colUOM
            // 
            this.colUOM.DataPropertyName = "UOM";
            this.colUOM.HeaderText = "ĐVT";
            this.colUOM.Name = "colUOM";
            this.colUOM.ReadOnly = true;
            // 
            // colPackageQuantity
            // 
            this.colPackageQuantity.DataPropertyName = "PackageQuantity";
            this.colPackageQuantity.HeaderText = "Quy cách pallet";
            this.colPackageQuantity.Name = "colPackageQuantity";
            this.colPackageQuantity.ReadOnly = true;
            // 
            // colReceiver
            // 
            this.colReceiver.DataPropertyName = "Receiver";
            this.colReceiver.HeaderText = "Người nhận hàng";
            this.colReceiver.Name = "colReceiver";
            this.colReceiver.ReadOnly = true;
            // 
            // colReceiveDateTime
            // 
            this.colReceiveDateTime.DataPropertyName = "StrReceiveDateTime";
            this.colReceiveDateTime.HeaderText = "Ngày nhận hàng";
            this.colReceiveDateTime.Name = "colReceiveDateTime";
            this.colReceiveDateTime.ReadOnly = true;
            // 
            // colForklift
            // 
            this.colForklift.DataPropertyName = "Forklift";
            this.colForklift.HeaderText = "Xe nâng";
            this.colForklift.Name = "colForklift";
            this.colForklift.ReadOnly = true;
            // 
            // colSetLocationDateTime
            // 
            this.colSetLocationDateTime.DataPropertyName = "StrSetLocationDateTime";
            this.colSetLocationDateTime.HeaderText = "Ngày lên kệ";
            this.colSetLocationDateTime.Name = "colSetLocationDateTime";
            this.colSetLocationDateTime.ReadOnly = true;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(125, 72);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(400, 24);
            this.txtDescription.TabIndex = 88;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 75);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 16);
            this.label5.TabIndex = 89;
            this.label5.Text = "Ghi chú:";
            // 
            // btnPrintPalletLabel
            // 
            this.btnPrintPalletLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrintPalletLabel.Location = new System.Drawing.Point(239, 112);
            this.btnPrintPalletLabel.Name = "btnPrintPalletLabel";
            this.btnPrintPalletLabel.Size = new System.Drawing.Size(104, 30);
            this.btnPrintPalletLabel.TabIndex = 90;
            this.btnPrintPalletLabel.TabStop = false;
            this.btnPrintPalletLabel.Text = "3.In nhãn pallet";
            this.btnPrintPalletLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrintPalletLabel.UseVisualStyleBackColor = true;
            this.btnPrintPalletLabel.Click += new System.EventHandler(this.btnPrintPalletLabel_Click);
            // 
            // btnConfirmImportToWarehouse
            // 
            this.btnConfirmImportToWarehouse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmImportToWarehouse.Location = new System.Drawing.Point(349, 112);
            this.btnConfirmImportToWarehouse.Name = "btnConfirmImportToWarehouse";
            this.btnConfirmImportToWarehouse.Size = new System.Drawing.Size(223, 30);
            this.btnConfirmImportToWarehouse.TabIndex = 91;
            this.btnConfirmImportToWarehouse.TabStop = false;
            this.btnConfirmImportToWarehouse.Text = "4.Xác nhận cho xe nâng chất hàng";
            this.btnConfirmImportToWarehouse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmImportToWarehouse.UseVisualStyleBackColor = true;
            this.btnConfirmImportToWarehouse.Click += new System.EventHandler(this.btnConfirmImportToWarehouse_Click);
            // 
            // colSTT
            // 
            this.colSTT.DataPropertyName = "LineNbr";
            this.colSTT.HeaderText = "STT";
            this.colSTT.Name = "colSTT";
            this.colSTT.ReadOnly = true;
            // 
            // colDetail_ProductCode
            // 
            this.colDetail_ProductCode.DataPropertyName = "ProductCode";
            this.colDetail_ProductCode.HeaderText = "Mã hàng";
            this.colDetail_ProductCode.Name = "colDetail_ProductCode";
            this.colDetail_ProductCode.ReadOnly = true;
            // 
            // colDetail_ProductDescription
            // 
            this.colDetail_ProductDescription.DataPropertyName = "ProductDescription";
            dataGridViewCellStyle2.NullValue = null;
            this.colDetail_ProductDescription.DefaultCellStyle = dataGridViewCellStyle2;
            this.colDetail_ProductDescription.HeaderText = "Tên hàng";
            this.colDetail_ProductDescription.Name = "colDetail_ProductDescription";
            this.colDetail_ProductDescription.ReadOnly = true;
            this.colDetail_ProductDescription.Width = 200;
            // 
            // colBatchCode
            // 
            this.colBatchCode.DataPropertyName = "BatchCode";
            this.colBatchCode.HeaderText = "Số lô Bayer";
            this.colBatchCode.Name = "colBatchCode";
            this.colBatchCode.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "Số lô NCC";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.DataPropertyName = "ExpiryDate";
            this.colExpiryDate.HeaderText = "Hạn sử dụng";
            this.colExpiryDate.Name = "colExpiryDate";
            // 
            // colDetail_POCode
            // 
            this.colDetail_POCode.DataPropertyName = "POCode";
            this.colDetail_POCode.HeaderText = "Số đơn hàng";
            this.colDetail_POCode.Name = "colDetail_POCode";
            // 
            // colDetail_UOM
            // 
            this.colDetail_UOM.DataPropertyName = "UOM";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colDetail_UOM.DefaultCellStyle = dataGridViewCellStyle3;
            this.colDetail_UOM.HeaderText = "ĐVT";
            this.colDetail_UOM.Name = "colDetail_UOM";
            this.colDetail_UOM.ReadOnly = true;
            this.colDetail_UOM.Width = 50;
            // 
            // colPackageQuantity1
            // 
            this.colPackageQuantity1.DataPropertyName = "PalletPackaging";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            this.colPackageQuantity1.DefaultCellStyle = dataGridViewCellStyle4;
            this.colPackageQuantity1.HeaderText = "Quy cách pallet";
            this.colPackageQuantity1.Name = "colPackageQuantity1";
            this.colPackageQuantity1.ReadOnly = true;
            // 
            // colDetail_QtyPlanning
            // 
            this.colDetail_QtyPlanning.DataPropertyName = "QuantityPlanning";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colDetail_QtyPlanning.DefaultCellStyle = dataGridViewCellStyle5;
            this.colDetail_QtyPlanning.HeaderText = "Số lượng";
            this.colDetail_QtyPlanning.Name = "colDetail_QtyPlanning";
            // 
            // colDetail_QtyReceived
            // 
            this.colDetail_QtyReceived.DataPropertyName = "QuantityReceived";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            this.colDetail_QtyReceived.DefaultCellStyle = dataGridViewCellStyle6;
            this.colDetail_QtyReceived.HeaderText = "Số lượng đã nhận";
            this.colDetail_QtyReceived.Name = "colDetail_QtyReceived";
            this.colDetail_QtyReceived.ReadOnly = true;
            // 
            // colDetail_PalletPlanning
            // 
            this.colDetail_PalletPlanning.DataPropertyName = "PalletPlanning";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            this.colDetail_PalletPlanning.DefaultCellStyle = dataGridViewCellStyle7;
            this.colDetail_PalletPlanning.HeaderText = "Số pallet kế hoạch";
            this.colDetail_PalletPlanning.Name = "colDetail_PalletPlanning";
            this.colDetail_PalletPlanning.ReadOnly = true;
            // 
            // colDetail_PalletReceived
            // 
            this.colDetail_PalletReceived.DataPropertyName = "PalletReceived";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            this.colDetail_PalletReceived.DefaultCellStyle = dataGridViewCellStyle8;
            this.colDetail_PalletReceived.HeaderText = "Số pallet đã nhận";
            this.colDetail_PalletReceived.Name = "colDetail_PalletReceived";
            this.colDetail_PalletReceived.ReadOnly = true;
            // 
            // colPalletSetLocation
            // 
            this.colPalletSetLocation.DataPropertyName = "PalletSetLocation";
            this.colPalletSetLocation.HeaderText = "Số pallet đã lên kệ";
            this.colPalletSetLocation.Name = "colPalletSetLocation";
            this.colPalletSetLocation.ReadOnly = true;
            // 
            // colDetail_Description
            // 
            this.colDetail_Description.DataPropertyName = "Description";
            this.colDetail_Description.HeaderText = "Ghi chú";
            this.colDetail_Description.Name = "colDetail_Description";
            this.colDetail_Description.Width = 200;
            // 
            // colDetail_Status
            // 
            this.colDetail_Status.DataPropertyName = "StatusDisplay";
            this.colDetail_Status.HeaderText = "Trạng thái";
            this.colDetail_Status.Name = "colDetail_Status";
            this.colDetail_Status.ReadOnly = true;
            this.colDetail_Status.Width = 150;
            // 
            // StockReceivingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 633);
            this.Controls.Add(this.btnConfirmImportToWarehouse);
            this.Controls.Add(this.btnPrintPalletLabel);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtVerifyStatus);
            this.Controls.Add(this.btnDetail_Delete);
            this.Controls.Add(this.btnDetail_Upload);
            this.Controls.Add(this.txtImportStatus);
            this.Controls.Add(this.dtpDeliveryDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCompanyName);
            this.Controls.Add(this.lblNCC);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCreatedBy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Name = "StockReceivingView";
            this.Text = "ProductPackingMaintView";
            this.Load += new System.EventHandler(this.StockReceivingView_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDetailSplit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetailSplits)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsDetails;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCreatedBy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.BindingSource bdsDetailSplits;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label lblNCC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDeliveryDate;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnDetail_Upload;
        private System.Windows.Forms.TextBox txtImportStatus;
        private System.Windows.Forms.Button btnDetail_Delete;
        private System.Windows.Forms.TextBox txtVerifyStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dtgDetail;
        private System.Windows.Forms.DataGridView dtgDetailSplit;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPrintPalletLabel;
        private System.Windows.Forms.Button btnConfirmImportToWarehouse;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot_NBR;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExpiredDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackageQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceiver;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceiveDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colForklift;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSetLocationDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSTT;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_POCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackageQuantity1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_QtyPlanning;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_QtyReceived;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_PalletPlanning;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_PalletReceived;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletSetLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_Status;
    }
}