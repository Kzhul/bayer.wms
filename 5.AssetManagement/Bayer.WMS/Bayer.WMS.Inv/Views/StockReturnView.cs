﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class StockReturnView : BaseForm, IStockReturnView
    {
        public StockReturnView()
        {
            InitializeComponent();

            SetDoubleBuffered(dtgDetail);
        }

        private async void btnConfirm_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IStockReturnPresenter;
            await doPresenter.Confirm();
        }

        private async void btnDetail_Upload_Click(object sender, EventArgs e)
        {
            btnDetail_Upload.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        await (_presenter as IStockReturnPresenter).ImportExcel(ofd.FileName);
                    }
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnDetail_Upload.Enabled = true;
            }
        }

        private void btnDetail_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtgDetail.SelectedRows)
                {
                    var detail = bdsDetails[row.Index] as StockReturnDetail;
                    bdsDetails.Remove(detail);
                    (_presenter as IStockReturnPresenter).DeleteDetail(detail);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "StockReturnCode";
            cmbCode.DisplayMember = "StockReturnCode";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("StockReturnCode", "Mã", 100),
                new MultiColumnComboBox.ComboBoxColumn("StrDate", "Ngày giao hàng", 120),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái nhập kho", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Ghi chú", 200)
            };
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadStockReturnHeader", "OnCodeSelectChange");
            cmbCode.DropDownWidth = 800;
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);
            InitializeComboBox();
            var doPresenter = _presenter as IStockReturnPresenter;
            await Task.WhenAll(doPresenter.LoadStockReturnHeaders());
            if (!isRefresh)
                doPresenter.Insert();
        }

        public DataTable StockReturnHeaders
        {
            set { cmbCode.Source = value; }
        }

        private StockReturnHeader _stockReturnHeader;

        public StockReturnHeader StockReturnHeader
        {
            get { return _stockReturnHeader; }
            set
            {
                _stockReturnHeader = value;
                cmbCode.DataBindings.Clear();
                txtDescription.DataBindings.Clear();
                txtCreatedBy.DataBindings.Clear();
                dtpDeliveryDate.DataBindings.Clear();
                txtImportStatus.DataBindings.Clear();

                cmbCode.DataBindings.Add("Text", StockReturnHeader, "StockReturnCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", StockReturnHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCreatedBy.DataBindings.Add("Text", StockReturnHeader, "CreatedByName", true, DataSourceUpdateMode.OnPropertyChanged);
                dtpDeliveryDate.DataBindings.Add("Value", StockReturnHeader, "Date", true, DataSourceUpdateMode.OnPropertyChanged);
                txtImportStatus.DataBindings.Add("Text", StockReturnHeader, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private IList<StockReturnDetail> _stockReturnDetail;

        public IList<StockReturnDetail> StockReturnDetails
        {
            get => _stockReturnDetail;
            set
            {
                _stockReturnDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _stockReturnDetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _stockReturnDetail;
                }
            }
        }

        private IList<StockReturnDetail> _deletedStockReturnDetails;

        public IList<StockReturnDetail> DeletedStockReturnDetails
        {
            get { return _deletedStockReturnDetails; }
            set { _deletedStockReturnDetails = value; }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }

        public async void Print()
        {
            bool validate = true;
            if (validate)
            {
                var doPresenter = _presenter as IStockReturnPresenter;
                await doPresenter.Print();
            }
        }

        private async void btnDownload_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IStockReturnPresenter;
            await doPresenter.Export();
        }

        private void StockReturnView_Load(object sender, EventArgs e)
        {

        }
    }
}