﻿namespace Bayer.WMS.Inv.Views
{
    partial class ReportBay4sView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgTotal = new System.Windows.Forms.DataGridView();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dtgPallets = new System.Windows.Forms.DataGridView();
            this.colZoneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackSize1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUOM1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsPallets = new System.Windows.Forms.BindingSource(this.components);
            this.colProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUnrestricted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInQualityInsp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBlocked = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSumQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDifQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colManufacturingDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExpiredDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCodeDistributor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallets)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgTotal
            // 
            this.dtgTotal.AllowUserToAddRows = false;
            this.dtgTotal.AllowUserToDeleteRows = false;
            this.dtgTotal.AllowUserToOrderColumns = true;
            this.dtgTotal.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgTotal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgTotal.AutoGenerateColumns = false;
            this.dtgTotal.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTotal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductCode,
            this.colProductName,
            this.colBatchCode,
            this.colUOM,
            this.colUnrestricted,
            this.colInQualityInsp,
            this.colBlocked,
            this.colSumQuantity,
            this.colQuantity,
            this.colDifQuantity,
            this.colManufacturingDate,
            this.colExpiredDate,
            this.colPackSize,
            this.colBatchCodeDistributor});
            this.dtgTotal.DataSource = this.bdsDetails;
            this.dtgTotal.GridColor = System.Drawing.SystemColors.Control;
            this.dtgTotal.Location = new System.Drawing.Point(12, 48);
            this.dtgTotal.Name = "dtgTotal";
            this.dtgTotal.ReadOnly = true;
            this.dtgTotal.Size = new System.Drawing.Size(974, 376);
            this.dtgTotal.TabIndex = 8;
            this.dtgTotal.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgTotal_CellClick);
            this.dtgTotal.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgTotal_CellContentClick);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Location = new System.Drawing.Point(12, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(140, 30);
            this.btnSave.TabIndex = 42;
            this.btnSave.Text = "1.Upload Bay4S";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.Green;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(846, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 30);
            this.button1.TabIndex = 43;
            this.button1.Text = "2.Download Data";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtgPallets
            // 
            this.dtgPallets.AllowUserToAddRows = false;
            this.dtgPallets.AllowUserToDeleteRows = false;
            this.dtgPallets.AllowUserToOrderColumns = true;
            this.dtgPallets.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Azure;
            this.dtgPallets.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgPallets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPallets.AutoGenerateColumns = false;
            this.dtgPallets.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPallets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPallets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneName,
            this.colLocationName,
            this.colPalletCode,
            this.colPackSize1,
            this.colUOM1,
            this.colQuantity1});
            this.dtgPallets.DataSource = this.bdsPallets;
            this.dtgPallets.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPallets.Location = new System.Drawing.Point(12, 430);
            this.dtgPallets.Name = "dtgPallets";
            this.dtgPallets.ReadOnly = true;
            this.dtgPallets.Size = new System.Drawing.Size(974, 211);
            this.dtgPallets.TabIndex = 44;
            // 
            // colZoneName
            // 
            this.colZoneName.DataPropertyName = "ZoneName";
            this.colZoneName.HeaderText = "ZoneName";
            this.colZoneName.Name = "colZoneName";
            this.colZoneName.ReadOnly = true;
            this.colZoneName.Width = 200;
            // 
            // colLocationName
            // 
            this.colLocationName.DataPropertyName = "LocationName";
            this.colLocationName.HeaderText = "LocationName";
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.ReadOnly = true;
            // 
            // colPalletCode
            // 
            this.colPalletCode.DataPropertyName = "PalletCode";
            this.colPalletCode.HeaderText = "PalletCode";
            this.colPalletCode.Name = "colPalletCode";
            this.colPalletCode.ReadOnly = true;
            // 
            // colPackSize1
            // 
            this.colPackSize1.DataPropertyName = "PackSize";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            this.colPackSize1.DefaultCellStyle = dataGridViewCellStyle9;
            this.colPackSize1.HeaderText = "PackSize";
            this.colPackSize1.Name = "colPackSize1";
            this.colPackSize1.ReadOnly = true;
            // 
            // colUOM1
            // 
            this.colUOM1.DataPropertyName = "UOM";
            this.colUOM1.HeaderText = "UOM";
            this.colUOM1.Name = "colUOM1";
            this.colUOM1.ReadOnly = true;
            // 
            // colQuantity1
            // 
            this.colQuantity1.DataPropertyName = "Quantity";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "N0";
            this.colQuantity1.DefaultCellStyle = dataGridViewCellStyle10;
            this.colQuantity1.HeaderText = "Quantity";
            this.colQuantity1.Name = "colQuantity1";
            this.colQuantity1.ReadOnly = true;
            // 
            // colProductCode
            // 
            this.colProductCode.DataPropertyName = "ProductCode";
            this.colProductCode.HeaderText = "ProductCode";
            this.colProductCode.Name = "colProductCode";
            this.colProductCode.ReadOnly = true;
            // 
            // colProductName
            // 
            this.colProductName.DataPropertyName = "ProductName";
            this.colProductName.HeaderText = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.ReadOnly = true;
            this.colProductName.Width = 200;
            // 
            // colBatchCode
            // 
            this.colBatchCode.DataPropertyName = "BatchCode";
            this.colBatchCode.HeaderText = "BatchCode";
            this.colBatchCode.Name = "colBatchCode";
            this.colBatchCode.ReadOnly = true;
            // 
            // colUOM
            // 
            this.colUOM.DataPropertyName = "UOM";
            this.colUOM.HeaderText = "UOM";
            this.colUOM.Name = "colUOM";
            this.colUOM.ReadOnly = true;
            // 
            // colUnrestricted
            // 
            this.colUnrestricted.DataPropertyName = "Unrestricted";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            this.colUnrestricted.DefaultCellStyle = dataGridViewCellStyle2;
            this.colUnrestricted.HeaderText = "(A) Unrestricted";
            this.colUnrestricted.Name = "colUnrestricted";
            this.colUnrestricted.ReadOnly = true;
            // 
            // colInQualityInsp
            // 
            this.colInQualityInsp.DataPropertyName = "InQualityInsp";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            this.colInQualityInsp.DefaultCellStyle = dataGridViewCellStyle3;
            this.colInQualityInsp.HeaderText = "(B) InQualityInsp";
            this.colInQualityInsp.Name = "colInQualityInsp";
            this.colInQualityInsp.ReadOnly = true;
            // 
            // colBlocked
            // 
            this.colBlocked.DataPropertyName = "Blocked";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            this.colBlocked.DefaultCellStyle = dataGridViewCellStyle4;
            this.colBlocked.HeaderText = "(C) Blocked";
            this.colBlocked.Name = "colBlocked";
            this.colBlocked.ReadOnly = true;
            // 
            // colSumQuantity
            // 
            this.colSumQuantity.DataPropertyName = "SumQuantity";
            this.colSumQuantity.HeaderText = "(E) A+B+C";
            this.colSumQuantity.Name = "colSumQuantity";
            this.colSumQuantity.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.colQuantity.HeaderText = "(D) BarCode Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.ReadOnly = true;
            // 
            // colDifQuantity
            // 
            this.colDifQuantity.DataPropertyName = "DifQuantity";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            this.colDifQuantity.DefaultCellStyle = dataGridViewCellStyle6;
            this.colDifQuantity.HeaderText = "Lệch = D - E";
            this.colDifQuantity.Name = "colDifQuantity";
            this.colDifQuantity.ReadOnly = true;
            // 
            // colManufacturingDate
            // 
            this.colManufacturingDate.DataPropertyName = "StrManufacturingDate";
            this.colManufacturingDate.HeaderText = "ManufacturingDate";
            this.colManufacturingDate.Name = "colManufacturingDate";
            this.colManufacturingDate.ReadOnly = true;
            // 
            // colExpiredDate
            // 
            this.colExpiredDate.DataPropertyName = "StrExpiredDate";
            this.colExpiredDate.HeaderText = "ExpiredDate";
            this.colExpiredDate.Name = "colExpiredDate";
            this.colExpiredDate.ReadOnly = true;
            // 
            // colPackSize
            // 
            this.colPackSize.DataPropertyName = "PackSize";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            this.colPackSize.DefaultCellStyle = dataGridViewCellStyle7;
            this.colPackSize.HeaderText = "PackSize";
            this.colPackSize.Name = "colPackSize";
            this.colPackSize.ReadOnly = true;
            // 
            // colBatchCodeDistributor
            // 
            this.colBatchCodeDistributor.DataPropertyName = "BatchCodeDistributor";
            this.colBatchCodeDistributor.HeaderText = "BatchCodeDistributor";
            this.colBatchCodeDistributor.Name = "colBatchCodeDistributor";
            this.colBatchCodeDistributor.ReadOnly = true;
            // 
            // ReportBay4sView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 653);
            this.Controls.Add(this.dtgPallets);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dtgTotal);
            this.Name = "ReportBay4sView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallets)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgTotal;
        private System.Windows.Forms.BindingSource bdsDetails;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dtgPallets;
        private System.Windows.Forms.BindingSource bdsPallets;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackSize1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUOM1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUnrestricted;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInQualityInsp;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBlocked;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSumQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDifQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colManufacturingDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExpiredDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCodeDistributor;
    }
}