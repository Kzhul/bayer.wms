﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class DeliveryTicketView : BaseForm, IDeliveryTicketView
    {
        public DeliveryTicketView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgPrepareNotes);
            SetDoubleBuffered(dtgProduct);

            int index = 0;
            cmbCode.TabIndex = index;

            index++;
            txtDeliveryAddress.TabIndex = index;

            index++;
            txtListOrderNumbers.TabIndex = index;

            index++;
            txtListFreeItems.TabIndex = index;

            index++;
            txtDescription.TabIndex = index;

            index++;
            txtGrossWeight.TabIndex = index;

            index++;
            txtFreeItemsWeight.TabIndex = index;
            
            index++;
            txtTruckNo.TabIndex = index;

            index++;
            txtDriver.TabIndex = index;

            //index++;
            //txtDriverIdentification.TabIndex = index;            

            index++;
            txtReceiptBy.TabIndex = index;           

            index++;
            txtDeliveverSign.TabIndex = index;
        }

        public override void InitializeComboBox()
        {
            #region cmbCode
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "DeliveryTicketCode";
            cmbCode.DisplayMember = "DeliveryTicketCode - Description";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("StrDeliveryDate", "Ngày giao hàng", 130),
                new MultiColumnComboBox.ComboBoxColumn("DeliveryTicketCode", "Mã", 100),
                new MultiColumnComboBox.ComboBoxColumn("CompanyCode", "Mã NPP", 100),
                new MultiColumnComboBox.ComboBoxColumn("CompanyName", "Tên NPP", 300),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 130),
                new MultiColumnComboBox.ComboBoxColumn("Province", "Tỉnh thành", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 130)
            };
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "GetUsers", "OnCodeSelectChange");
            cmbCode.DropDownWidth = 800;
            #endregion
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            InitializeComboBox();

            var doPresenter = _presenter as IDeliveryTicketPresenter;
            await Task.WhenAll(doPresenter.LoadDeliveryTicketHeader());

            if (!isRefresh)
                doPresenter.Insert();
        }

        public string DeliveryTicketCode
        {
            get
            {
                return cmbCode.SelectedItem["DeliveryTicketCode"].ToString();
            }
        }

        public DataTable DeliveryTicketHeaders
        {
            set { cmbCode.Source = value; }
        }
        
        private DeliveryTicketHeader _DeliveryTicketHeader;

        public DeliveryTicketHeader DeliveryTicketHeader
        {
            get { return _DeliveryTicketHeader; }
            set
            {
                _DeliveryTicketHeader = value;

                cmbCode.DataBindings.Clear();
                txtDescription.DataBindings.Clear();
                txtDeliveryDate.DataBindings.Clear();
                txtDeliveryAddress.DataBindings.Clear();
                txtCompanyCode.DataBindings.Clear();
                txtCompanyName.DataBindings.Clear();
                txtProvince.DataBindings.Clear();
                txtListOrderNumbers.DataBindings.Clear();
                txtListFreeItems.DataBindings.Clear();
                txtTotalBoxes.DataBindings.Clear();
                txtTotalBags.DataBindings.Clear();
                txtTotalBuckets.DataBindings.Clear();
                txtTotalCans.DataBindings.Clear();
                txtGrossWeight.DataBindings.Clear();
                txtTruckNo.DataBindings.Clear();
                txtDriver.DataBindings.Clear();
                txtReceiptBy.DataBindings.Clear();
                txtDeliveverSign.DataBindings.Clear();
                txtStatusDisplay.DataBindings.Clear();
                //txtDriverIdentification.DataBindings.Clear();
                txtFreeItemsWeight.DataBindings.Clear();

                cmbCode.DataBindings.Add("Text", DeliveryTicketHeader, "DeliveryTicketCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", DeliveryTicketHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDeliveryDate.DataBindings.Add("Text", DeliveryTicketHeader, "StrDeliveryDate", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDeliveryAddress.DataBindings.Add("Text", DeliveryTicketHeader, "DeliveryAddress", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCompanyCode.DataBindings.Add("Text", DeliveryTicketHeader, "CompanyCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCompanyName.DataBindings.Add("Text", DeliveryTicketHeader, "CompanyName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProvince.DataBindings.Add("Text", DeliveryTicketHeader, "Province", true, DataSourceUpdateMode.OnPropertyChanged);
                txtListOrderNumbers.DataBindings.Add("Text", DeliveryTicketHeader, "ListOrderNumbers", true, DataSourceUpdateMode.OnPropertyChanged);
                txtListFreeItems.DataBindings.Add("Text", DeliveryTicketHeader, "ListFreeItems", true, DataSourceUpdateMode.OnPropertyChanged);
                txtTotalBoxes.DataBindings.Add("Text", DeliveryTicketHeader, "TotalBoxes", true, DataSourceUpdateMode.OnPropertyChanged);
                txtTotalBags.DataBindings.Add("Text", DeliveryTicketHeader, "TotalBags", true, DataSourceUpdateMode.OnPropertyChanged);
                txtTotalBuckets.DataBindings.Add("Text", DeliveryTicketHeader, "TotalBuckets", true, DataSourceUpdateMode.OnPropertyChanged);
                txtTotalCans.DataBindings.Add("Text", DeliveryTicketHeader, "TotalCans", true, DataSourceUpdateMode.OnPropertyChanged);

                txtGrossWeight.DataBindings.Add("Text", DeliveryTicketHeader, "GrossWeight", true, DataSourceUpdateMode.OnPropertyChanged);
                txtTruckNo.DataBindings.Add("Text", DeliveryTicketHeader, "TruckNo", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDriver.DataBindings.Add("Text", DeliveryTicketHeader, "Driver", true, DataSourceUpdateMode.OnPropertyChanged);
                //txtDriverIdentification.DataBindings.Add("Text", DeliveryTicketHeader, "DriverIdentification", true, DataSourceUpdateMode.OnPropertyChanged);
                txtReceiptBy.DataBindings.Add("Text", DeliveryTicketHeader, "ReceiptBy", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDeliveverSign.DataBindings.Add("Text", DeliveryTicketHeader, "DeliveverSign", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStatusDisplay.DataBindings.Add("Text", DeliveryTicketHeader, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtFreeItemsWeight.DataBindings.Add("Text", DeliveryTicketHeader, "FreeItemsWeight", true, DataSourceUpdateMode.OnPropertyChanged);

                txtStatusDisplay.ForeColor = Utility.StatusToColor(this.DeliveryTicketHeader.Status);
            }
        }

        private IList<DeliveryTicketDetail> _DeliveryTicketDetail;

        public IList<DeliveryTicketDetail> DeliveryTicketDetails
        {
            get { return _DeliveryTicketDetail; }
            set
            {
                _DeliveryTicketDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsPrepareNotes.DataSource = _DeliveryTicketDetail;
                    });
                }
                else
                {
                    bdsPrepareNotes.DataSource = _DeliveryTicketDetail;
                }
                dtgPrepareNotes.DataBindings.Clear();
                BindingSource source = new BindingSource();
                source.DataSource = bdsPrepareNotes;
                dtgPrepareNotes.DataSource = source;

                #region Load Header Info

                if (this.DeliveryTicketHeader != null)
                {
                    if (!string.IsNullOrEmpty(this.DeliveryTicketHeader.CompanyCode))
                    {
                        txtCompanyCode.Text = this.DeliveryTicketHeader.CompanyCode;
                    }
                    if (!string.IsNullOrEmpty(this.DeliveryTicketHeader.CompanyName))
                    {
                        txtCompanyName.Text = this.DeliveryTicketHeader.CompanyName;
                    }
                    if (!string.IsNullOrEmpty(this.DeliveryTicketHeader.Province))
                    {
                        txtProvince.Text = this.DeliveryTicketHeader.Province;
                    }
                    txtDeliveryDate.Text = this.DeliveryTicketHeader.StrDeliveryDate;
                }
                #endregion
            }
        }

        private IList<DeliveryTicketDetail> _deletedDeliveryTicketDetails;

        public IList<DeliveryTicketDetail> DeletedDeliveryTicketDetails
        {
            get { return _deletedDeliveryTicketDetails; }
            set { _deletedDeliveryTicketDetails = value; }
        }


        private IList<PrepareNoteDetail> _PrepareNoteDetail;

        public IList<PrepareNoteDetail> PrepareNoteDetails
        {
            get { return _PrepareNoteDetail; }
            set
            {
                _PrepareNoteDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _PrepareNoteDetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _PrepareNoteDetail;
                }
                dtgProduct.DataBindings.Clear();
                BindingSource source = new BindingSource();
                source.DataSource = bdsDetails;
                dtgProduct.DataSource = source;             
            }
        }


        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }
        
        public async void Print()
        {
            bool validate = true;
            #region Validate Data
            //Số xe, tên tài xế, người nhận hàng, số hóa đơn là dữ liệu bắt buộc để in phiếu
            if (string.IsNullOrEmpty(this.DeliveryTicketHeader.TruckNo)
                || string.IsNullOrEmpty(this.DeliveryTicketHeader.ListOrderNumbers)
                )
            {
                MessageBox.Show("Số xe, số hóa đơn là dữ liệu bắt buộc để in phiếu.");
                validate = false;
                return;
            }
            #endregion

            //Nếu hàng khuyến mãi trống, hỏi user có muốn nhập trước khi in hay ko
            if (string.IsNullOrEmpty(this.DeliveryTicketHeader.ListFreeItems))
            {
                if (MessageBox.Show("Hàng khuyến mãi đang trống. Bạn có muốn nhập hàng khuyến mãi ?", "Hàng khuyến mãi", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    txtListFreeItems.Focus();
                    validate = false;
                    return;
                }
            }

            if (validate)
            {
                var doPresenter = _presenter as IDeliveryTicketPresenter;
                await doPresenter.Save();
                await doPresenter.Print();
            }
        }
    }
}
