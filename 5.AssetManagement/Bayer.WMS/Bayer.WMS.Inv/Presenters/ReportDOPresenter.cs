﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IReportDOView : IBaseView
    {
        System.Data.DataTable ReportDODetails { get; set; }
    }

    public interface IReportDOPresenter : IBasePresenter
    {
        void SearchDO(string DOCode, DateTime? fromDate, DateTime? toDate);

        Task Print();
    }

    public class ReportDOPresenter : BasePresenter, IReportDOPresenter
    {
        private IReportDOView _mainView;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository;

        public ReportDOPresenter(IReportDOView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryOrderHeaderRepository = deliveryOrderHeaderRepository;
            _mainView.ReportDODetails = new System.Data.DataTable();
        }

        public async void SearchDO(string DOCode, DateTime? fromDate, DateTime? toDate)
        {
            List<SqlParameter> listParam = new List<SqlParameter>();
            SqlParameter code = new SqlParameter("DOImportCode", DOCode);
            listParam.Add(code);
            SqlParameter paramFromDate = new SqlParameter("FromDate", fromDate);
            listParam.Add(paramFromDate);
            SqlParameter paramToDate = new SqlParameter("ToDate", toDate);
            listParam.Add(paramToDate);
            _mainView.ReportDODetails = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportDO", listParam.ToArray());
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var details = _mainView.ReportDODetails;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\ReportDO.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            bool isSuccess = false;
            int cellRowIndex = 5;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                for (int i = 0; i < details.Rows.Count; i++)
                {
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = (i + 1).ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DOImportCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DeliveryDate"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DOStatus"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["EXStatus"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["SPStatus"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["PRStatus"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DHStatus"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ExportCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["SplitCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["PrepareCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DeliveryTicketCode"];
                    cellColumnIndex++;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A6", "L" + (cellRowIndex - 1).ToString());
                foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                {
                    cell.BorderAround();
                }
                #endregion

                #region Fill Data Header
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "BaoCaoCongViec" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion
                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }
    }
}
