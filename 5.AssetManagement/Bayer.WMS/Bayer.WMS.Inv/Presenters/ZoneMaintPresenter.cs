﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IZoneMaintView : IBaseView
    {
        Warehouse Warehouse { get; set; }

        DataTable Warehouses { set; }

        DataTable Zones { set; }

        IList<ZoneLine> ZoneLines { get; set; }

        IList<ZoneLine> DeletedZoneLines { get; set; }

        IList<ZoneLocation> ZoneLocations { get; set; }

        IList<ZoneLocation> DeletedZoneLocations { get; set; }

        IList<ZoneCategory> ZoneCategories { get; set; }

        IList<ZoneTemperature> ZoneTemperatures { get; set; }

        Zone Zone { get; set; }
    }

    public interface IZoneMaintPresenter : IBasePresenter
    {
        Task LoadZones();
        Task LoadWarehouses();
        void DeleteZoneLine(ZoneLine zoneLine);
        void DeleteZoneLocation(ZoneLocation zoneLocation);
    }

    public class ZoneMaintPresenter : BasePresenter, IZoneMaintPresenter
    {
        private IZoneMaintView _mainView;
        private IZoneRepository _zoneRepository;
        private IZoneLineRepository _zoneLineRepository;
        private IZoneLocationRepository _zoneLocationRepository;
        private IZoneCategoryRepository _zoneCategoryRepository;
        private IZoneTemperatureRepository _zoneTemperatureRepository;
        private IWarehouseRepository _warehouseRepository;

        public ZoneMaintPresenter(IZoneMaintView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IZoneRepository zoneRepository,
            IZoneLineRepository zoneLineRepository,
            IZoneLocationRepository zoneLocationRepository,
            IZoneCategoryRepository zoneCategoryRepository,
            IZoneTemperatureRepository zoneTemperatureRepository,
            IWarehouseRepository warehouseRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _zoneRepository = zoneRepository;
            _zoneLineRepository = zoneLineRepository;
            _zoneLocationRepository = zoneLocationRepository;
            _zoneCategoryRepository = zoneCategoryRepository;
            _zoneTemperatureRepository = zoneTemperatureRepository;
            _warehouseRepository = warehouseRepository;

            _mainView.ZoneLines = new List<ZoneLine>();
            _mainView.DeletedZoneLines = new List<ZoneLine>();
            _mainView.ZoneLocations = new List<ZoneLocation>();
            _mainView.DeletedZoneLocations = new List<ZoneLocation>();
        }

        private async Task HandleZoneCategory(Zone zone)
        {
            var zoneCategories = _mainView.ZoneCategories;

            foreach (var zoneCate in zoneCategories.Where(p => !String.IsNullOrWhiteSpace(p.ZoneCode) && !p.Checked))
            {
                zoneCate.IsRecordAuditTrail = true;
                await _zoneCategoryRepository.Delete(new object[] { zoneCate.ZoneCode, zoneCate.CategoryID });
            }

            foreach (var zoneCate in zoneCategories.Where(p => p.Checked))
            {
                zoneCate.ZoneCode = zone.ZoneCode;
                zoneCate.IsRecordAuditTrail = true;
                await _zoneCategoryRepository.InsertOrUpdate(zoneCate, new object[] { zone.ZoneCode, zoneCate.CategoryID });
            }
        }

        private async Task HandleZoneTemperature(Zone zone)
        {
            var zoneTemperatures = _mainView.ZoneTemperatures;

            foreach (var zoneTemp in zoneTemperatures.Where(p => !String.IsNullOrWhiteSpace(p.ZoneCode) && !p.Checked))
            {
                zoneTemp.IsRecordAuditTrail = true;
                await _zoneTemperatureRepository.Delete(new object[] { zoneTemp.ZoneCode, zoneTemp.RefValue, zoneTemp.GroupCode });
            }

            foreach (var zoneTemp in zoneTemperatures.Where(p => p.Checked))
            {
                zoneTemp.ZoneCode = zone.ZoneCode;
                zoneTemp.IsRecordAuditTrail = true;
                await _zoneTemperatureRepository.InsertOrUpdate(zoneTemp, new object[] { zone.ZoneCode, zoneTemp.RefValue, zoneTemp.GroupCode });
            }
        }

        public async Task OnWarehouseCodeSelectChange(DataRow selectedItem)
        {
            try
            {
                var warehouse = selectedItem.ToEntity<Warehouse>();

                warehouse.State = EntityState.Unchanged;
                _mainView.Warehouse = warehouse;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Event trigger when user select item in combobox
        /// </summary>
        /// <param name="selectedItem"></param>
        public async Task OnZoneCodeSelectChange(DataRow selectedItem)
        {
            try
            {
                var zone = selectedItem.ToEntity<Zone>();
                await Task.WhenAll(LoadZoneLines(zone.ZoneCode), LoadZoneLocations(zone.ZoneCode), LoadZoneCategories(zone.ZoneCode), LoadZoneTemperatures(zone.ZoneCode));

                zone.State = EntityState.Unchanged;
                _mainView.Zone = zone;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadWarehouses()
        {
            try
            {
                var warehouses = await _warehouseRepository.GetAsync(p => !p.IsDeleted);
                _mainView.Warehouses = warehouses.OrderBy(a => a.Description).ToList().ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get products from database to combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadZones()
        {
            try
            {
                var zones = await _zoneRepository.GetIncludeWarehouseAsync(p => !p.IsDeleted);
                _mainView.Zones = zones.OrderBy(a => a.Description).ToList().ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadZoneLines(string zoneCode)
        {
            try
            {
                _mainView.ZoneLines = await _zoneLineRepository.GetAsync(p => p.ZoneCode == zoneCode && !p.IsDeleted);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadZoneLocations(string zoneCode)
        {
            try
            {
                _mainView.ZoneLocations = await _zoneLocationRepository.GetAsync(p => p.ZoneCode == zoneCode && !p.IsDeleted);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadZoneCategories(string zoneCode)
        {
            try
            {
                _mainView.ZoneCategories = await _zoneCategoryRepository.GetWithAllCategoriesAsync(p => p.ZoneCode == zoneCode);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadZoneTemperatures(string zoneCode)
        {
            try
            {
                _mainView.ZoneTemperatures = await _zoneTemperatureRepository.GetWithAllTemperaturAsync(p => p.ZoneCode == zoneCode);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public void DeleteZoneLine(ZoneLine zoneLine)
        {
            try
            {
                _mainView.DeletedZoneLines.Add(zoneLine);
                _mainView.ZoneLines.Remove(zoneLine);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public void DeleteZoneLocation(ZoneLocation zoneLocation)
        {
            try
            {
                _mainView.DeletedZoneLocations.Add(zoneLocation);
                _mainView.ZoneLocations.Remove(zoneLocation);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async void Insert()
        {
            _mainView.Zone = new Zone();
            _mainView.ZoneLines = new List<ZoneLine>();
            _mainView.DeletedZoneLines = new List<ZoneLine>();
            _mainView.ZoneLocations = new List<ZoneLocation>();
            _mainView.DeletedZoneLocations = new List<ZoneLocation>();

            await Task.WhenAll(LoadZoneCategories(String.Empty), LoadZoneTemperatures(String.Empty));
        }

        /// <summary>
        /// Save product to database, 2 cases:
        ///     Case 1: insert new product
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var zone = _mainView.Zone;
            zone.WarehouseID = _mainView.Warehouse.WarehouseID;
            var zoneLines = _mainView.ZoneLines;
            var deletedZoneLines = _mainView.DeletedZoneLines;
            var zoneLocations = _mainView.ZoneLocations;
            var deletedZoneLocations = _mainView.DeletedZoneLocations;


            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _zoneRepository = unitOfWork.Register(_zoneRepository.GetType()) as IZoneRepository;
                    _zoneLineRepository = unitOfWork.Register(_zoneLineRepository.GetType()) as IZoneLineRepository;
                    _zoneLocationRepository = unitOfWork.Register(_zoneLocationRepository.GetType()) as IZoneLocationRepository;
                    _zoneCategoryRepository = unitOfWork.Register(_zoneCategoryRepository.GetType()) as IZoneCategoryRepository;
                    _zoneTemperatureRepository = unitOfWork.Register(_zoneTemperatureRepository.GetType()) as IZoneTemperatureRepository;

                    //// check zone id to determine next action (insert or update)
                    zone.IsRecordAuditTrail = true;
                    await _zoneRepository.InsertOrUpdate(zone, new object[] { zone.ZoneCode });

                    //foreach (var line in deletedZoneLines.Where(p => !String.IsNullOrWhiteSpace(p.LineCode)))
                    //{
                    //    line.IsRecordAuditTrail = true;
                    //    line.IsDeleted = true;
                    //    await _zoneLineRepository.Update(line, new string[] { "IsDeleted" }, new object[] { zone.ZoneCode, line.LineCode });

                    //    foreach (var location in zoneLocations.Where(p => p.LineCode == line.LineCode))
                    //    {
                    //        location.IsRecordAuditTrail = true;
                    //        location.IsDeleted = true;
                    //        await _zoneLocationRepository.Update(location, new string[] { "IsDeleted" }, new object[] { location.LocationCode, zone.ZoneCode });
                    //    }
                    //}
                    //foreach (var location in zoneLocations.Where(p => p.ZoneCode == zone.ZoneCode))
                    //{
                    //    location.IsRecordAuditTrail = false;
                    //    location.IsDeleted = true;
                    //    await _zoneLocationRepository.Update(location, new string[] { "CurrentPalletCode" }, new object[] { location.LocationCode, zone.ZoneCode });
                    //}

                    foreach (var line in zoneLines.Where(p => !String.IsNullOrWhiteSpace(p.Type) && p.State == EntityState.Modified))
                    {
                        line.ZoneCode = zone.ZoneCode;
                        line.IsRecordAuditTrail = true;

                        await _zoneLineRepository.InsertOrUpdate(line, new object[] { zone.ZoneCode, line.LineCode });

                        //if (line.Type == ZoneLine.type.Line)
                        {
                            for (int i = 1; i <= line.Level; i++)
                            {
                                for (int j = 1; j <= line.Length; j++)
                                {
                                    string locationName = $"{line.LineName}{i}-{j:00}";
                                    string tmpLocationCode = $"{line.LineCode}{i}-{j:00}";
                                    string locationCode = $"{zone.ZoneCode}_{line.LineCode}_{tmpLocationCode}";

                                    if (!deletedZoneLocations.Any(p => p.LocationCode == locationCode))
                                    {
                                        //var zoneLocation = zoneLocations.FirstOrDefault(p => p.LocationCode == locationCode);
                                        //Thread.Sleep(2000);
                                        _mainPresenter.SetMessage("Đang xử lý: " + line.LineName + ", " + i.ToString() + ", " + j.ToString(), Utility.MessageType.Information);
                                        await _zoneLocationRepository.InsertOrUpdate(
                                            new ZoneLocation
                                            {
                                                LocationCode = locationCode,
                                                LocationName = locationName,
                                                ZoneCode = line.ZoneCode,
                                                LineCode = line.LineCode,
                                                LengthID = j,
                                                LevelID = i,
                                                Status = ZoneLocation.status.Active,
                                                Description = String.Empty,
                                                //Status = zoneLocation == null ? ZoneLocation.status.Active : zoneLocation.Status,
                                                //Description = zoneLocation == null ? String.Empty : zoneLocation.Description,
                                                IsDeleted = false,
                                            },  new object[] { locationCode, zone.ZoneCode });
                                        
                                       
                                    }
                                }
                            }
                        }
                    }

                    foreach (var location in deletedZoneLocations.Where(p => !String.IsNullOrWhiteSpace(p.LocationCode)))
                    {
                        location.IsRecordAuditTrail = false;
                        location.IsDeleted = true;
                        await _zoneLocationRepository.Update(location, new string[] { "IsDeleted" }, new object[] { location.LocationCode, zone.ZoneCode });
                    }

                    await HandleZoneCategory(zone);
                    await HandleZoneTemperature(zone);

                    await unitOfWork.Commit();
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            case 2627: //// Duplicate unique key
                                message = await ResolveDuplicate(zone);
                                break;
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _zoneRepository = _mainPresenter.Resolve(_zoneRepository.GetType()) as IZoneRepository;
                _zoneLineRepository = _mainPresenter.Resolve(_zoneLineRepository.GetType()) as IZoneLineRepository;
                _zoneLocationRepository = _mainPresenter.Resolve(_zoneLocationRepository.GetType()) as IZoneLocationRepository;
                _zoneCategoryRepository = _mainPresenter.Resolve(_zoneCategoryRepository.GetType()) as IZoneCategoryRepository;
                _zoneTemperatureRepository = _mainPresenter.Resolve(_zoneTemperatureRepository.GetType()) as IZoneTemperatureRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadZones();
                    await LoadZoneLines(zone.ZoneCode);
                    await LoadZoneLocations(zone.ZoneCode);
                    _mainView.Zone = zone;
                }
            }
        }

        /// <summary>
        /// Delete logical
        /// </summary>
        /// <returns></returns>
        public override async Task Delete()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var zone = _mainView.Zone;
            bool isSuccess = false;
            try
            {
                zone.IsDeleted = true;
                zone.IsRecordAuditTrail = true;

                await _zoneRepository.Update(zone, new string[] { "IsDeleted" }, new object[] { zone.ZoneCode });
                await _zoneRepository.Commit();

                await _zoneRepository.ExecuteNonQuery("proc_Zones_HandleDeleted",
                    new SqlParameter { ParameterName = "@ZoneCode", SqlDbType = SqlDbType.VarChar, Value = zone.ZoneCode });

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                {
                    await LoadZones();
                    Insert();
                }
            }
        }

        /// <summary>
        /// Resolve when insert new product which product code is already existed, 2 cases:
        ///     Case 1: existing product is not deleted, throw exception.
        ///     Case 2: existing product is deleted, ask user to undelete it
        /// </summary>
        /// <param name="zone"></param>
        /// <returns></returns>
        public async Task<string> ResolveDuplicate(Zone zone)
        {
            string message = String.Empty;
            var existProduct = await _zoneRepository.GetSingleAsync(p => p.ZoneCode == zone.ZoneCode);
            if (!existProduct.IsDeleted)
            {
                message = Messages.Error_ProductCodeExisted;
            }
            else if (_mainView.GetConfirm(Messages.Question_RecoverDeleted))
            {
                zone.ZoneCode = existProduct.ZoneCode;
                zone.RowVersion = existProduct.RowVersion;
                await _zoneRepository.Update(zone, new object[] { zone.ZoneCode });
                await _zoneRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
            }

            return message;
        }
    }
}