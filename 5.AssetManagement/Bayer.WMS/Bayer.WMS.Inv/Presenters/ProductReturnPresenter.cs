﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IProductReturnView : IBaseView
    {
        System.Data.DataTable ProductReturnHeaders { set; }
        System.Data.DataTable ProductReturnDetailSplits { get; set; }
        ProductReturnHeader ProductReturnHeader { get; set; }
        IList<ProductReturnDetail> ProductReturnDetails { get; set; }
        IList<ProductReturnDetail> DeletedProductReturnDetails { get; set; }
    }

    public interface IProductReturnPresenter : IBasePresenter
    {
        Task LoadProductReturnHeaders();
        void DeleteDetail(ProductReturnDetail item);
        Task Print();
        Task Confirm();
        Task ImportExcel(string fileName);

        Task Export();
    }

    public class ProductReturnPresenter : BasePresenter, IProductReturnPresenter
    {
        private IProductReturnView _mainView;
        private IProductReturnHeaderRepository _stockReturnHeaderRepository;
        private IProductReturnDetailRepository _stockReturnDetailRepository;
        private IPalletRepository _palletRepository;
        private IProductRepository _productRepository;
        private IProductPackingRepository _productPackingRepository;
        private ICompanyRepository _companyRepository;

        public ProductReturnPresenter(IProductReturnView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IProductReturnHeaderRepository stockReturnHeaderRepository,
            IProductReturnDetailRepository stockReturnDetailRepository,
            IPalletRepository palletRepository,
            IProductRepository productRepository,
            IProductPackingRepository productPackingRepository,
            ICompanyRepository companyRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _stockReturnHeaderRepository = stockReturnHeaderRepository;
            _stockReturnDetailRepository = stockReturnDetailRepository;
            _palletRepository = palletRepository;
            _productRepository = productRepository;
            _productPackingRepository = productPackingRepository;
            _companyRepository = companyRepository;

            _mainView.ProductReturnDetails = new List<ProductReturnDetail>();
            _mainView.DeletedProductReturnDetails = new List<ProductReturnDetail>();
            _mainView.ProductReturnHeaders = new System.Data.DataTable();
            _mainView.ProductReturnDetailSplits = new System.Data.DataTable();
        }

        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            var model = selectedItem.ToEntity<ProductReturnHeader>();
            await LoadProductReturnDetails(model.ProductReturnCode);
            _mainView.ProductReturnHeader = model;
        }

        public async Task LoadProductReturnHeaders()
        {
            try
            {
                _mainView.ProductReturnHeaders = (await _stockReturnHeaderRepository.GetIncludeCreatedByAsync(p => !p.IsDeleted)).OrderByDescending(p => p.CreatedDateTime).ToList().ToDataTable();
                if (_mainView.ProductReturnHeader != null)
                {
                    await LoadProductReturnDetails(_mainView.ProductReturnHeader.ProductReturnCode);
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadProductReturnDetails(string ProductReturnCode)
        {
            _mainView.ProductReturnDetails = await _stockReturnDetailRepository.GetIncludeProductAsync(p => p.ProductReturnCode == ProductReturnCode);
            _mainView.ProductReturnDetailSplits = await _stockReturnDetailRepository.ExecuteDataTable("proc_ProductReturnDetailSplit_Select_ByDocument",
                new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = ProductReturnCode });
        }

        public void DeleteDetail(ProductReturnDetail item)
        {
            try
            {
                _mainView.DeletedProductReturnDetails.Add(item);
                _mainView.ProductReturnDetails.Remove(item);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override void Insert()
        {
            _mainView.ProductReturnHeader = new ProductReturnHeader();
            _mainView.ProductReturnDetails = new List<ProductReturnDetail>();
            _mainView.DeletedProductReturnDetails = new List<ProductReturnDetail>();
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.ProductReturnHeader;
            var details = _mainView.ProductReturnDetails;
            var deletedDetails = _mainView.DeletedProductReturnDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuGiaoHang.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\PhieuGiaoHang\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Header
                //sheetDetail.get_Range(positionCompanyName, positionCompanyName).Cells[0] = header.CompanyCode + " - " + header.CompanyName + " - " + header.Province;
                //sheetDetail.get_Range(positionProductReturnCode, positionProductReturnCode).Cells[0] = header.ProductReturnCode;
                //sheetDetail.get_Range(positionBarCodeProductReturnCode, positionBarCodeProductReturnCode).Cells[0] = "*" + header.ProductReturnCode + "*";

                //sheetDetail.get_Range(positionDeliveryDate, positionDeliveryDate).Cells[0] = header.StrDeliveryDate;
                //sheetDetail.get_Range(positionOrderNumbers, positionOrderNumbers).Cells[0] = header.ListOrderNumbers;

                //sheetDetail.get_Range(positionBoxes, positionBoxes).Cells[0] = Utility.DecimalToString( header.TotalBoxes);
                //sheetDetail.get_Range(positionBags, positionBags).Cells[0] = Utility.DecimalToString(header.TotalBags);
                //sheetDetail.get_Range(positionBuckets, positionBuckets).Cells[0] = Utility.DecimalToString(header.TotalBuckets);
                //sheetDetail.get_Range(positionCans, positionCans).Cells[0] = Utility.DecimalToString(header.TotalCans);

                //sheetDetail.get_Range(positionFreeItems, positionFreeItems).Cells[0] = header.ListFreeItems;

                //sheetDetail.get_Range(positionGrossWeight, positionGrossWeight).Cells[0] = header.GrossWeight;

                //sheetDetail.get_Range(positionTruckNo, positionTruckNo).Cells[0] = header.TruckNo;

                //sheetDetail.get_Range(positionDriver, positionDriver).Cells[0] = header.Driver;

                //sheetDetail.get_Range(positionDriverIdentification, positionDriverIdentification).Cells[0] = header.DriverIdentification;

                //sheetDetail.get_Range(positionReceiptBy, positionReceiptBy).Cells[0] = header.ReceiptBy;
                #endregion

                #region Fill Page Number Info
                sheetDetail.PageSetup.LeftFooter = @"Phát hành theo biểu mẫu WH-55-03 đã được đăng kí"; // This worked correctly
                sheetDetail.PageSetup.RightFooter = @"Page &P of &N"; // This worked correctly
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.ProductReturnCode + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    //ProcessStartInfo startInfo;
                    //startInfo = new ProcessStartInfo(exportPath);
                    //startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    //Process newProcess = new Process();
                    //newProcess.StartInfo = startInfo;
                    //newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    //printFromExcel(exportPath);

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully + ": " + header.ProductReturnCode, Utility.MessageType.Information);
                }
            }
        }

        public static void PrintFromExcel(string yourFileName)
        {
            //Microsoft.Office.Interop.Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();
            //xl.Workbooks.Open(yourFileName, Type., optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);
            //xl.Worksheets.PrintOut(optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

            //if (xl != null)
            //{
            //    xl.Quit();
            //}
        }

        public async Task Confirm()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var header = _mainView.ProductReturnHeader;
            bool isSuccess = false;

            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                SqlParameter param1 = new SqlParameter("Code", header.ProductReturnCode);
                listParam.Add(param1);
                SqlParameter param2 = new SqlParameter("DOImportCode", string.Empty);
                listParam.Add(param2);
                SqlParameter param3 = new SqlParameter("Type", "ProductReturn");
                listParam.Add(param3);
                SqlParameter param4 = new SqlParameter("Status", DeliveryNoteHeader.status.Complete);
                listParam.Add(param4);

                await _stockReturnHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _stockReturnHeaderRepository = _mainPresenter.Resolve(_stockReturnHeaderRepository.GetType()) as IProductReturnHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_ConfirmSucessfully, Utility.MessageType.Information);

                    await LoadProductReturnHeaders();
                }
            }
        }

        public async Task ImportExcel(string fileName)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            int rowStart = 10;
            string start = "A" + rowStart;
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            try
            {
                workbook = workbooks.Open(fileName);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                //// get a range to work with
                range = sheetDetail.get_Range(start, Missing.Value);

                //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                range = range.get_End(XlDirection.xlDown);

                //// get the address of the bottom cell
                string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                //// specific end column
                end = downAddress.Remove(0, 1);

                end = "L" + end;
                end = "L5000";

                //// Get the range, then values from start to end
                range = sheetDetail.get_Range(start, end);
                var values = (object[,])range.Value2;
                int count = values.GetLength(0);

                var header = await _stockReturnHeaderRepository.GetSingleAsync(p => p.ProductReturnCode == _mainView.ProductReturnHeader.ProductReturnCode && !p.IsDeleted);
                IList<ProductReturnDetail> details = new List<ProductReturnDetail>();
                if (header != null)
                {
                    if (header.Status == ProductReturnHeader.status.Completed)
                        throw new WrappedException("Phiếu trả hàng này đã hoàn tất, không thể import bổ sung.");

                    details = await _stockReturnDetailRepository.GetAsync(p => p.ProductReturnCode == _mainView.ProductReturnHeader.ProductReturnCode);
                }

                header = header ?? new ProductReturnHeader();
                header.Date = DateTime.Today;
                header.CreatedByName = $"{LoginInfo.LastName} {LoginInfo.FirstName}";
                header.CompanyCode = $"{values[1, 3]}";
                var company = await _companyRepository.GetFirstAsync(a => a.CompanyCode == header.CompanyCode);

                if (company != null)
                {
                    header.CompanyName = company.CompanyName;
                    header.Province = company.ProvinceShipTo;
                }
                else
                {
                    throw new WrappedException("Mã công ty: " + header.CompanyCode + " không tồn tại trong hệ thống. Vui lòng kiểm tra lại");
                }

                var products = await _productRepository.GetAsync(p => !p.IsDeleted);
                var productPackagings = await _productPackingRepository.GetAsync(p => p.Type != ProductPacking.type.Pallet);

                for (int i = 6; i <= count; i++)
                {
                    string productCode = $"{values[i, 2]}";
                    string productDescr = $"{values[i, 3]}";
                    if (String.IsNullOrWhiteSpace(productCode))
                        break;

                    int lineNbr = int.Parse($"{values[i, 1]}");

                    //// Validate product
                    var product = products.FirstOrDefault(p => p.ProductCode == productCode);
                    if (product == null)
                        throw new WrappedException(String.Format(Messages.Validate_Product_NotExist, productDescr));

                    //// Validate request quantity
                    decimal planQty;
                    if (!decimal.TryParse($"{values[i, 5]}", out planQty))
                        throw new WrappedException(Messages.Validate_Required_RequestQty);
                    if (planQty < 0)
                        throw new WrappedException(Messages.Validate_Range_RequestQty);

                    var line = details.FirstOrDefault(p => p.LineNbr == lineNbr);
                    if (line == null)
                    {
                        line = new ProductReturnDetail();
                        line.LineNbr = lineNbr;
                        line.DeliveredQty = 0;
                        line.ReturnQty = planQty;

                        details.Add(line);
                    }

                    line.ProductID = product.ProductID;
                    line.ProductCode = productCode;
                    line.ProductDescription = productDescr;
                    line.BatchCode = $"{values[i, 4]}";
                    line.Description = $"{values[i, 6]}";

                    var packaging = productPackagings.FirstOrDefault(p => p.ProductID == product.ProductID);
                    if (packaging != null)
                    {
                        line.PackageSize = packaging.Quantity.Value;
                    }

                    line.UOM = product.UOM;
                }

                _mainView.ProductReturnHeader = header;
                _mainView.ProductReturnDetails = details;
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(String.Format(Messages.Information_ImportSucessfullyWithError, String.Join(",", errorLine)), Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.ProductReturnHeader;
            var details = _mainView.ProductReturnDetails;
            var deletedDetails = _mainView.DeletedProductReturnDetails;
            bool isSuccess = false;

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _stockReturnHeaderRepository = unitOfWork.Register(_stockReturnHeaderRepository.GetType()) as IProductReturnHeaderRepository;
                    _stockReturnDetailRepository = unitOfWork.Register(_stockReturnDetailRepository.GetType()) as IProductReturnDetailRepository;

                    if (String.IsNullOrWhiteSpace(header.ProductReturnCode))
                    {
                        string code = await _stockReturnHeaderRepository.GetNextNBR();
                        header.ProductReturnCode = code;
                        await _stockReturnHeaderRepository.Insert(header);
                    }
                    else
                    {
                        await _stockReturnHeaderRepository.Update(header, null, new string[] { "Status" }, new object[] { header.ProductReturnCode });
                    }

                    //// foreach deleted DO details and delete it
                    foreach (var item in deletedDetails)
                    {
                        await _stockReturnDetailRepository.Delete(new object[] { item.ProductReturnCode, item.LineNbr });
                    }

                    foreach (var item in details)
                    {
                        item.ProductReturnCode = header.ProductReturnCode;
                        await _stockReturnDetailRepository.InsertOrUpdate(item, null, new string[] { "Status", "ReceivedQty" }, new object[] { item.ProductReturnCode, item.LineNbr });
                    }

                    await unitOfWork.Commit();
                }

                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _stockReturnHeaderRepository = _mainPresenter.Resolve(_stockReturnHeaderRepository.GetType()) as IProductReturnHeaderRepository;
                _stockReturnDetailRepository = _mainPresenter.Resolve(_stockReturnDetailRepository.GetType()) as IProductReturnDetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);

                    await LoadProductReturnHeaders();
                    _mainView.ProductReturnHeader = header;
                }
            }

        }

        public override async Task Delete()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.ProductReturnHeader;

            bool isSuccess = false;
            try
            {
                if (header.Status != ProductReturnHeader.status.New)
                {
                    if (header.Status == ProductReturnHeader.status.Processing)
                        throw new WrappedException("Phiếu trả hàng đang được kiểm tra không thể xóa");
                    if (header.Status == ProductReturnHeader.status.Completed)
                        throw new WrappedException("Phiếu trả hàng đã kiểm tra hoàn tất không thể xóa");
                }

                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _stockReturnHeaderRepository = unitOfWork.Register(_stockReturnHeaderRepository.GetType()) as IProductReturnHeaderRepository;

                    await _stockReturnHeaderRepository.ExecuteNonQuery("proc_ProductReturnHeaders_Delete",
                        new SqlParameter { ParameterName = "@ProductReturnCode", SqlDbType = SqlDbType.VarChar, Value = header.ProductReturnCode });

                    await unitOfWork.Commit();
                }


                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                _stockReturnHeaderRepository = _mainPresenter.Resolve(_stockReturnHeaderRepository.GetType()) as IProductReturnHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadProductReturnHeaders();
                    Insert();
                }
            }
        }

        public async Task Export()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.ProductReturnHeader;
            var details = _mainView.ProductReturnDetails;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuTraHangThanhPham.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 11;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                int i = 0;
                foreach (var item in details)
                {
                    i++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = (i).ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductDescription;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Description;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = string.Empty;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCode;
                    cellColumnIndex++;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A11", "K" + (cellRowIndex - 1).ToString());
                foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                {
                    cell.BorderAround();
                }
                #endregion

                #region Fill Data Header
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "PhieuTraHangThanhPham" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }
    }
}
