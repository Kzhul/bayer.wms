﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IDeliveryTicketView : IBaseView
    {
        DeliveryTicketHeader DeliveryTicketHeader { get; set; }

        System.Data.DataTable DeliveryTicketHeaders { set; }

        IList<DeliveryTicketDetail> DeliveryTicketDetails { get; set; }

        IList<DeliveryTicketDetail> DeletedDeliveryTicketDetails { get; set; }

        IList<PrepareNoteDetail> PrepareNoteDetails { get; set; }
    }

    public interface IDeliveryTicketPresenter : IBasePresenter
    {
        Task LoadDeliveryTicketHeader();

        void DeleteDeliveryTicketDetail(DeliveryTicketDetail item);

        Task Print();
    }

    public class DeliveryTicketPresenter : BasePresenter, IDeliveryTicketPresenter
    {
        private IDeliveryTicketView _mainView;
        private IDeliveryTicketHeaderRepository _deliveryTicketHeaderRepository;
        private IDeliveryTicketDetailRepository _deliveryTicketDetailRepository;
        private IPrepareNoteHeaderRepository _prepareNoteHeaderRepository;
        private IPrepareNoteDetailRepository _prepareNoteDetailRepository;

        public DeliveryTicketPresenter(IDeliveryTicketView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryTicketHeaderRepository deliveryTicketHeaderRepository
            , IDeliveryTicketDetailRepository deliveryTicketDetailRepository
            , IPrepareNoteHeaderRepository prepareNoteHeaderRepository
            , IPrepareNoteDetailRepository prepareNoteDetailRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryTicketHeaderRepository = deliveryTicketHeaderRepository;
            _deliveryTicketDetailRepository = deliveryTicketDetailRepository;
            _prepareNoteHeaderRepository = prepareNoteHeaderRepository;
            _prepareNoteDetailRepository = prepareNoteDetailRepository;

            _mainView.DeliveryTicketDetails = new List<DeliveryTicketDetail>();
            _mainView.DeletedDeliveryTicketDetails = new List<DeliveryTicketDetail>();
            _mainView.DeliveryTicketHeader = new DeliveryTicketHeader();
            _mainView.DeliveryTicketHeader.Status = DeliveryOrderHeader.status.New;
        }
        DeliveryTicketHeader model;
        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            model = selectedItem.ToEntity<DeliveryTicketHeader>();
            await LoadDeliveryTicketDetails(model.DeliveryTicketCode);
        }

        public async void OnPrepareCodeSelectChange(DataRow selectedItem)
        {
            if (string.IsNullOrWhiteSpace(_mainView.DeliveryTicketHeader.DeliveryTicketCode))
            {
                var model = selectedItem.ToEntity<PrepareForDeliveryNote>();
                if (model != null && !string.IsNullOrEmpty(model.CompanyCode))
                {
                    await LoadPrepareHeaderByCode(model.CompanyCode, model.DeliveryDate.Value);
                }
            }
        }

        public async Task LoadDeliveryTicketHeader()
        {
            var model = await _deliveryTicketHeaderRepository.GetAsync(p => !p.IsDeleted);
            _mainView.DeliveryTicketHeaders = model.OrderByDescending(a => a.CreatedDateTime).ToList().ToDataTable();
        }

        public async Task LoadDeliveryTicketDetails(string DeliveryTicketCode)
        {
            var details = await _deliveryTicketDetailRepository.GetIncludePrepareAsync(p => p.DeliveryTicketCode == DeliveryTicketCode);

            List<PrepareNoteDetail> listProduct = new List<PrepareNoteDetail>();
            foreach (DeliveryTicketDetail ticketDetail in details)
            {
                listProduct.AddRange(await _prepareNoteDetailRepository.GetIncludeProductAsync(a => a.PrepareCode == ticketDetail.PrepareCode));
            }

            #region Set Header, must be here to trigger Value
            //listProduct = listProduct.OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode }).ToList();
            _mainView.PrepareNoteDetails = (from a in listProduct orderby a.ProductDescription, a.Quantity, a.BatchCode select a).ToList();
            if (_mainView.PrepareNoteDetails != null)
            {
                //Thùng -> C
                //Bao -> B
                //Xô -> S
                //Can -> A
                //decimal totalPack = _mainView.PrepareNoteDetails.Where(a => a.PackType == "C").Sum(a => a.PackQuantity).Value;
                //totalPack += _mainView.PrepareNoteDetails.Where(a => a.PackType == "C" && a.ProductQuantity > 0).Count();
                //model.TotalBoxes = totalPack;s               
                if (model.TotalBoxes == 0
                    && model.TotalBoxes == 0
                    && model.TotalBoxes == 0
                    && model.TotalBoxes == 0
                    )
                {
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("DeliveryTicketCode", model.DeliveryTicketCode);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("CompanyCode", model.CompanyCode);
                    listParam.Add(param2);
                    string strCarton = await _prepareNoteDetailRepository.ExecuteScalar("proc_DeliveryTicket_SelectNumberOfCarton", listParam.ToArray());

                    model.TotalBoxes = Utility.IntParse(strCarton);
                    model.StrTotalBoxes = Utility.StringNParseWithRoundingDecimalDegit(model.TotalBoxes);
                    decimal totalBag = _mainView.PrepareNoteDetails.Where(a => a.PackType == "B").Sum(a => a.Quantity).Value;
                    model.TotalBags = totalBag;
                    model.StrTotalBags = Utility.StringNParseWithRoundingDecimalDegit(model.TotalBags);
                    decimal totalBucket = _mainView.PrepareNoteDetails.Where(a => a.PackType == "S").Sum(a => a.Quantity).Value;
                    model.TotalBuckets = totalBucket;
                    model.StrTotalBuckets = Utility.StringNParseWithRoundingDecimalDegit(model.TotalBuckets);
                    decimal totalCan = _mainView.PrepareNoteDetails.Where(a => a.PackType == "A").Sum(a => a.Quantity).Value;
                    model.TotalCans = totalCan;
                    model.StrTotalCans = Utility.StringNParseWithRoundingDecimalDegit(model.TotalCans);
                }

                _mainView.DeliveryTicketHeader = model;
            }

            //Trigger Model here
            _mainView.DeliveryTicketDetails = details;
            #endregion
        }

        public void DeleteDeliveryTicketDetail(DeliveryTicketDetail item)
        {
            try
            {
                _mainView.DeletedDeliveryTicketDetails.Add(item);
                _mainView.DeliveryTicketDetails.Remove(item);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadPrepareHeaderByCode(string companyCode, DateTime deliveryDate)
        {
            if (!string.IsNullOrEmpty(companyCode))
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    var details = _mainView.DeliveryTicketDetails;
                    if (details.FirstOrDefault(a => a.PrepareCode == companyCode) == null)
                    {
                        _prepareNoteHeaderRepository = unitOfWork.Register(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;
                        _prepareNoteDetailRepository = unitOfWork.Register(_prepareNoteDetailRepository.GetType()) as IPrepareNoteDetailRepository;
                        _deliveryTicketDetailRepository = unitOfWork.Register(_deliveryTicketDetailRepository.GetType()) as IDeliveryTicketDetailRepository;
                        var existed = new List<DeliveryTicketDetail>(); //await _deliveryTicketDetailRepository.GetAsync(a => a.PrepareCode == companyCode);
                        if (existed.Count == 0)
                        {
                            var listItem = await _prepareNoteHeaderRepository.GetAsync(a => a.CompanyCode == companyCode && a.DeliveryDate == deliveryDate);
                            if (listItem != null && listItem.Count > 0)
                            {
                                details = new List<DeliveryTicketDetail>();
                                foreach (PrepareNoteHeader item in listItem)
                                {
                                    DeliveryTicketDetail model = new DeliveryTicketDetail()
                                    {
                                        CompanyCode = item.CompanyCode,
                                        CompanyName = item.CompanyName,
                                        DeliveryDate = item.DeliveryDate,
                                        Description = item.Description,
                                        DOImportCode = item.DOImportCode,
                                        PrepareCode = item.PrepareCode,
                                        PrepareDate = item.PrepareDate,
                                        Province = item.Province,
                                    };
                                    details.Add(model);
                                }

                                #region Load Tab Product
                                List<PrepareNoteDetail> listProduct = new List<PrepareNoteDetail>();
                                foreach (DeliveryTicketDetail ticketDetail in details)
                                {
                                    listProduct.AddRange(await _prepareNoteDetailRepository.GetIncludeProductAndPalletAsync(a => a.PrepareCode == ticketDetail.PrepareCode));
                                }


                                #endregion

                                #region Load data header
                                //if (string.IsNullOrEmpty(_mainView.DeliveryTicketHeader.CompanyCode))
                                {
                                    _mainView.DeliveryTicketHeader.CompanyCode = listItem[0].CompanyCode;
                                    _mainView.DeliveryTicketHeader.CompanyName = listItem[0].CompanyName;
                                    _mainView.DeliveryTicketHeader.Province = listItem[0].Province;
                                    _mainView.DeliveryTicketHeader.DeliveryDate = listItem[0].DeliveryDate;
                                }
                                #endregion

                                #region Set Header, must be here to trigger Value
                                _mainView.PrepareNoteDetails = (from a in listProduct orderby a.ProductDescription, a.Quantity, a.BatchCode select a).ToList();//listProduct.OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode }).ToList();
                                //Trigger Model here
                                _mainView.DeliveryTicketDetails = details;
                                #endregion
                            }
                        }
                        else
                        {
                            _mainPresenter.SetMessage("Mã khách hàng: " + companyCode + " đã tồn tại trên đã tồn tại trên phiếu giao hàng: " + existed.FirstOrDefault().DeliveryTicketCode, Utility.MessageType.Error);
                        }
                    }
                    else
                    {
                        _mainPresenter.SetMessage("Mã khách hàng: " + companyCode + " đã tồn tại trên danh sách", Utility.MessageType.Error);
                    }
                }
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryTicketHeader;
            var details = _mainView.DeliveryTicketDetails;
            var deletedDetails = _mainView.DeletedDeliveryTicketDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _deliveryTicketHeaderRepository = unitOfWork.Register(_deliveryTicketHeaderRepository.GetType()) as IDeliveryTicketHeaderRepository;
                    _prepareNoteHeaderRepository = unitOfWork.Register(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;
                    _deliveryTicketDetailRepository = unitOfWork.Register(_deliveryTicketDetailRepository.GetType()) as IDeliveryTicketDetailRepository;
                    _prepareNoteDetailRepository = unitOfWork.Register(_prepareNoteDetailRepository.GetType()) as IPrepareNoteDetailRepository;

                    #region Save DN Header and Details
                    if (String.IsNullOrWhiteSpace(header.DeliveryTicketCode))
                    {
                        string code = await _deliveryTicketHeaderRepository.GetNextNBR();

                        header.DeliveryTicketCode = code;
                        header.Status = PrepareNoteHeader.status.InProgress;
                        header.IsDeleted = false;
                        await _deliveryTicketHeaderRepository.Insert(header);
                    }
                    else
                    {
                        await _deliveryTicketHeaderRepository.Update(header, new object[] { header.DeliveryTicketCode });
                    }

                    //// foreach deleted DO details and delete it
                    foreach (var item in deletedDetails)
                    {
                        await _deliveryTicketDetailRepository.Delete(new object[] { item.DeliveryTicketCode, item.PrepareCode });
                    }

                    foreach (var item in details)
                    {
                        item.DeliveryTicketCode = header.DeliveryTicketCode;
                        item.Status = PrepareNoteHeader.status.InProgress;
                        await _deliveryTicketDetailRepository.InsertOrUpdate(item, new object[] { item.DeliveryTicketCode, item.PrepareCode });
                    }
                    #endregion

                    await unitOfWork.Commit();
                }

                {
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("Code", header.DeliveryTicketCode);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("DOImportCode", string.Empty);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("Type", "DeliveryTicket");
                    listParam.Add(param3);
                    SqlParameter param4 = new SqlParameter("Status", SplitNoteHeader.status.InProgress);
                    listParam.Add(param4);

                    await _deliveryTicketHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                    isSuccess = true;
                }


                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _deliveryTicketHeaderRepository = _mainPresenter.Resolve(_deliveryTicketHeaderRepository.GetType()) as IDeliveryTicketHeaderRepository;
                _deliveryTicketDetailRepository = _mainPresenter.Resolve(_deliveryTicketDetailRepository.GetType()) as IDeliveryTicketDetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);

                    await LoadDeliveryTicketHeader();
                    //await LoadDeliveryTicketDetails(header.DeliveryTicketCode);

                    _mainView.DeliveryTicketHeader = header;
                }
            }

        }

        public override void Insert()
        {
            _mainView.DeliveryTicketHeader = new DeliveryTicketHeader();
            _mainView.DeliveryTicketDetails = new List<DeliveryTicketDetail>();
            _mainView.PrepareNoteDetails = new List<PrepareNoteDetail>();
            _mainView.DeletedDeliveryTicketDetails = new List<DeliveryTicketDetail>();
            //_mainView.PrepareNoteHeaders = new System.Data.DataTable();

        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryTicketHeader;
            var details = _mainView.DeliveryTicketDetails;
            var deletedDetails = _mainView.DeletedDeliveryTicketDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuGiaoHang.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\PhieuGiaoHang\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            //config Template Element
            string positionCompanyName = "M7";
            string positionDeliveryTicketCode = "AT7";
            string positionBarCodeDeliveryTicketCode = "AK8";
            string positionDeliveryDate = "I10";
            string positionOrderNumbers = "V10";

            string positionBoxes = "I13";
            string positionBags = "P13";
            string positionBuckets = "W13";
            string positionCans = "AC13";

            string positionFreeItems = "L16";

            string positionGrossWeight = "K18";
            string positionFreeItemWeight = "AV16";

            string positionTruckNo = "E21";

            string positionDriver = "AI21";
            //string positionDriverIdentification = "AC24";

            string positionNote = "E24";
            string positionReceiptBy = "J27";

            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Header
                sheetDetail.get_Range(positionCompanyName, positionCompanyName).Cells[0] = header.CompanyCode + " - " + header.CompanyName + " - " + header.Province;
                sheetDetail.get_Range(positionDeliveryTicketCode, positionDeliveryTicketCode).Cells[0] = header.DeliveryTicketCode;
                sheetDetail.get_Range(positionBarCodeDeliveryTicketCode, positionBarCodeDeliveryTicketCode).Cells[0] = "*" + header.DeliveryTicketCode + "*";

                sheetDetail.get_Range(positionDeliveryDate, positionDeliveryDate).Cells[0] = header.StrDeliveryDate;
                sheetDetail.get_Range(positionOrderNumbers, positionOrderNumbers).Cells[0] = header.ListOrderNumbers;

                sheetDetail.get_Range(positionBoxes, positionBoxes).Cells[0] = Utility.DecimalToString(header.TotalBoxes);
                sheetDetail.get_Range(positionBags, positionBags).Cells[0] = Utility.DecimalToString(header.TotalBags);
                sheetDetail.get_Range(positionBuckets, positionBuckets).Cells[0] = Utility.DecimalToString(header.TotalBuckets);
                sheetDetail.get_Range(positionCans, positionCans).Cells[0] = Utility.DecimalToString(header.TotalCans);

                sheetDetail.get_Range(positionFreeItems, positionFreeItems).Cells[0] = header.ListFreeItems;

                sheetDetail.get_Range(positionGrossWeight, positionGrossWeight).Cells[0] = header.GrossWeight;

                sheetDetail.get_Range(positionFreeItemWeight, positionFreeItemWeight).Cells[0] = header.FreeItemsWeight;

                sheetDetail.get_Range(positionTruckNo, positionTruckNo).Cells[0] = header.TruckNo;

                sheetDetail.get_Range(positionDriver, positionDriver).Cells[0] = header.Driver;

                sheetDetail.get_Range(positionNote, positionNote).Cells[0] = header.Description;

                sheetDetail.get_Range(positionReceiptBy, positionReceiptBy).Cells[0] = header.ReceiptBy;
                #endregion

                #region Fill Page Number Info
                //sheetDetail.PageSetup.LeftFooter = @"Phát hành theo biểu mẫu WH-08-03 đã được đăng kí"; // This worked correctly
                sheetDetail.PageSetup.RightFooter = @"Page &P of &N"; // This worked correctly
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.DeliveryTicketCode + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    //ProcessStartInfo startInfo;
                    //startInfo = new ProcessStartInfo(exportPath);
                    //startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    //Process newProcess = new Process();
                    //newProcess.StartInfo = startInfo;
                    //newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    printFromExcel(exportPath);

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully + ": " + header.DeliveryTicketCode, Utility.MessageType.Information);
                }
            }
        }

        #region ExcelPrint
        // Module-level "dummy object" variable for optional parameters
        public static object optionalParameter = Type.Missing;

        public static void printFromExcel(string yourFileName)
        {
            Microsoft.Office.Interop.Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();
            xl.Workbooks.Open(yourFileName, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);
            xl.Worksheets.PrintOut(optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

            if (xl != null)
            {
                xl.Quit();
            }
        }
        #endregion

        public async Task Confirm()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var header = _mainView.DeliveryTicketHeader;
            bool isSuccess = false;

            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                SqlParameter param1 = new SqlParameter("Code", header.DeliveryTicketCode);
                listParam.Add(param1);
                SqlParameter param2 = new SqlParameter("DOImportCode", string.Empty);
                listParam.Add(param2);
                SqlParameter param3 = new SqlParameter("Type", "DeliveryTicket");
                listParam.Add(param3);
                SqlParameter param4 = new SqlParameter("Status", DeliveryNoteHeader.status.Complete);
                listParam.Add(param4);

                await _deliveryTicketHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _deliveryTicketHeaderRepository = _mainPresenter.Resolve(_deliveryTicketHeaderRepository.GetType()) as IDeliveryTicketHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_ConfirmSucessfully, Utility.MessageType.Information);

                    await LoadDeliveryTicketHeader();
                    //await LoadDeliveryTicketDetails(header.DeliveryTicketCode);
                }
            }
        }
    }
}
