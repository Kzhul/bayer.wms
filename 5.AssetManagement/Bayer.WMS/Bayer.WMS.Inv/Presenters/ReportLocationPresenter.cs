﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IReportLocationView : IBaseView
    {
        System.Data.DataTable ReportZones { get; set; }
        System.Data.DataTable ReportLines { get; set; }
        System.Data.DataTable ReportLocations { get; set; }

        System.Data.DataTable ReportChart1 { get; set; }
        System.Data.DataTable ReportChartPie1 { get; set; }
        System.Data.DataTable ReportChartPie2 { get; set; }

        System.Data.DataTable ReportZoneChart { get; set; }
        System.Data.DataTable ReportTopCustomer { get; set; }
        System.Data.DataTable ReportPalletReady { get; set; }
    }

    public interface IReportLocationPresenter : IBasePresenter
    {
        void Search(string product, string productLot,string user, string warehouse, string zone, string line, DateTime fromDate, DateTime todate, int searchDate);

        Task Print();
    }

    public class ReportLocationPresenter : BasePresenter, IReportLocationPresenter
    {
        private IReportLocationView _mainView;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository;

        public ReportLocationPresenter(IReportLocationView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryOrderHeaderRepository = deliveryOrderHeaderRepository;
            _mainView.ReportZones = new System.Data.DataTable();
            _mainView.ReportLines = new System.Data.DataTable();
            _mainView.ReportLocations = new System.Data.DataTable();
            _mainView.ReportChart1 = new System.Data.DataTable();
            _mainView.ReportChartPie1 = new System.Data.DataTable();
            _mainView.ReportChartPie2 = new System.Data.DataTable();
            _mainView.ReportZoneChart = new System.Data.DataTable();
            _mainView.ReportTopCustomer = new System.Data.DataTable();
            _mainView.ReportPalletReady = new System.Data.DataTable();
        }

        public async void Search(string product, string productLot, string user, string warehouse, string zone, string line, DateTime fromDate, DateTime todate, int searchDate)
        {
            _mainView.ReportLocations = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportLocation4",
                    new SqlParameter { ParameterName = "Product", SqlDbType = SqlDbType.VarChar, Value = product },
                    new SqlParameter { ParameterName = "ProductLot", SqlDbType = SqlDbType.VarChar, Value = productLot },
                    new SqlParameter { ParameterName = "User", SqlDbType = SqlDbType.VarChar, Value = user },
                    new SqlParameter { ParameterName = "Warehouse", SqlDbType = SqlDbType.VarChar, Value = warehouse },
                    new SqlParameter { ParameterName = "Zone", SqlDbType = SqlDbType.VarChar, Value = zone },
                    new SqlParameter { ParameterName = "Line", SqlDbType = SqlDbType.VarChar, Value = line },
                    new SqlParameter { ParameterName = "FromDate", SqlDbType = SqlDbType.Date, Value = fromDate },
                    new SqlParameter { ParameterName = "ToDate", SqlDbType = SqlDbType.Date, Value = todate },
                    new SqlParameter { ParameterName = "SearchDate", SqlDbType = SqlDbType.Int, Value = searchDate }
                    );

            LoadData();
        }

        private async void LoadData()
        {
            using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;

                _mainView.ReportZones = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportLocation1");
                _mainView.ReportLines = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportLocation2");
                //_mainView.ReportLocations = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportLocation3");
                _mainView.ReportPalletReady = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_Pallet_ProductWaitingForConfirm");

                //DateTime dt = DateTime.Today;
                //DateTime lastDateOfMonth = DateTime.Today.GetLastDayOfMonth();
                //_mainView.ReportChart1 = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_DeliveryOrderDetails_ReportMonthly",
                //    new SqlParameter { ParameterName = "FromDate", SqlDbType = SqlDbType.Date, Value = dt },
                //    new SqlParameter { ParameterName = "ToDate", SqlDbType = SqlDbType.Date, Value = lastDateOfMonth });


                //_mainView.ReportChartPie1 = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportLocationChartPie",
                //    new SqlParameter { ParameterName = "ZoneCode", SqlDbType = SqlDbType.VarChar, Value = "Kho_1" });
                //_mainView.ReportChartPie2 = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportLocationChartPie",
                //    new SqlParameter { ParameterName = "ZoneCode", SqlDbType = SqlDbType.VarChar, Value = "Kho_2" });

                //_mainView.ReportZoneChart = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportLocation1");
                //_mainView.ReportTopCustomer = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportLocation_TopCustomer",
                //    new SqlParameter { ParameterName = "FromDate", SqlDbType = SqlDbType.Date, Value = dt },
                //    new SqlParameter { ParameterName = "ToDate", SqlDbType = SqlDbType.Date, Value = lastDateOfMonth });
                
            }
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var details = _mainView.ReportLocations;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheet1 = null;
            //_Worksheet sheet2 = null;
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\ReportLocation.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";

            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 2;
            int cellColumnIndex = 1;
            string strDeliveryDate = string.Empty;
            string strCompanyName = string.Empty;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheet1 = (Worksheet)workbook.ActiveSheet;
                #region Fill Data Detail1
                if (details.Rows.Count > 0)
                {
                    int indexNBR = 0;
                    for (int i = 0; i < details.Rows.Count; i++)
                    {
                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ZoneName"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["LineName"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["LocationName"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["PalletCode"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductLot"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductCode"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductName"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["UOM"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["PackSize"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CartonQuantity"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["Quantity"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["Description"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ManufacturingDate"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ExpiryDate"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["StrDriverReceived"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["StrDriverReceivedDate"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["StrWarehouseKeeper"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["StrWarehouseVerifyDate"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["WarehouseVerifyNote"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["StrDriver"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["StrLocationPutDate"];
                        cellColumnIndex++;
                        
                        //End of Row
                        cellColumnIndex = 1;
                        cellRowIndex++;
                    }
                }
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "BaoCaoViTriKho" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion
                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }
    }
}
