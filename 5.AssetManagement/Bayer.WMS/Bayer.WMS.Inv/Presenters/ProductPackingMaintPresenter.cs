﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IProductPackingMaintView : IBaseView
    {
        System.Data.DataTable ProductsForCMB { set; }

        IList<Product> Products { set; }

        IList<ProductPacking> ProductPackings { get; set; }

        IList<ProductPacking> DeletedProductPackings { get; set; }

        string PackingType { get; }

        int ProductID { get; }
    }

    public interface IProductPackingMaintPresenter : IBasePresenter
    {
        Task LoadProducts();

        Task ImportExcel(string filePath);

        void DeleteProductPacking(ProductPacking prodPacking);
    }

    public class ProductPackingMaintPresenter : BasePresenter, IProductPackingMaintPresenter
    {
        private IProductPackingMaintView _mainView;
        private IProductPackingRepository _productPackingRepository;
        private IProductRepository _productRepository;


        public ProductPackingMaintPresenter(IProductPackingMaintView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IProductPackingRepository productPackingRepository, 
            IProductRepository productRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _productPackingRepository = productPackingRepository;
            _productRepository = productRepository;

            _mainView.ProductPackings = new List<ProductPacking>();
            _mainView.DeletedProductPackings = new List<ProductPacking>();
        }

        /// <summary>
        /// Get products from database to combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadProducts()
        {
            try
            {
                var products = await _productRepository.GetAsync(p => !p.IsDeleted);
                _mainView.ProductsForCMB = products.ToDataTable();
                products.Insert(0, new Product());
                _mainView.Products = products;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get product packing from database
        /// </summary>
        /// <returns></returns>
        public override async Task LoadMainData()
        {
            try
            {
                Expression<Func<ProductPacking, bool>> predicate = null;
                string type = _mainView.PackingType;

                if (type == ProductPacking.type.Carton)
                    predicate = p => p.Type == ProductPacking.type.Carton;
                else if (type == ProductPacking.type.Pallet)
                    predicate = p => p.Type == ProductPacking.type.Pallet;

                if (_mainView.ProductID > 0)
                    predicate = predicate == null ? p => p.ProductID == _mainView.ProductID : PredicateBuilder.And<ProductPacking>(predicate, p => p.ProductID == _mainView.ProductID);

                _mainView.ProductPackings = await _productPackingRepository.GetAsync(predicate);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public void DeleteProductPacking(ProductPacking prodPacking)
        {
            try
            {
                _mainView.DeletedProductPackings.Add(prodPacking);
                _mainView.ProductPackings.Remove(prodPacking);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Upload data product packings from excel file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task ImportExcel(string filePath)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            string start = "A2";
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Application excelApp = new ApplicationClass();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            bool isSuccess = false;
            try
            {
                workbook = workbooks.Open(filePath, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                //// get a range to work with
                range = sheetDetail.get_Range(start, Missing.Value);

                //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                range = range.get_End(XlDirection.xlDown);

                //// get the address of the bottom cell
                string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                //// specific end column
                end = downAddress.Remove(0, 1);

                //// throw exception to prevent loop through large data
                if (int.Parse(end) > 1000000)
                    throw new WrappedException(Messages.Validate_ExcelFile_Large);

                end = "G" + end;

                //// Get the range, then values from start to end
                range = sheetDetail.get_Range(start, end);
                var values = (object[,])range.Value2;
                int count = values.GetLength(0);

                var products = await _productRepository.GetAsync(p => !p.IsDeleted);

                for (int i = 1; i <= count; i++)
                {
                    try
                    {
                        string type = $"{values[i, 1]}";
                        string productCode = $"{values[i, 2]}";
                        decimal quantity;
                        decimal.TryParse($"{values[i, 4]}", out quantity);
                        string size = $"{values[i, 5]}";
                        string weight = $"{values[i, 6]}";
                        string note = $"{values[i, 7]}";

                        //// validate product code
                        if (String.IsNullOrWhiteSpace(productCode))
                            throw new Exception(Messages.Validate_Required_Product);

                        //// Validate product
                        var product = products.FirstOrDefault(p => p.ProductCode == productCode);
                        if (product == null)
                            throw new WrappedException(Messages.Validate_Required_Product);

                        //// validate packing type
                        if (String.IsNullOrWhiteSpace(type))
                            throw new WrappedException(Messages.Validate_Required_ProductPackingType);

                        //// validate size
                        if (quantity == 0)
                            throw new WrappedException(Messages.Validate_Required_ProductPackingQuantity);

                        //// validate size must be greater zero
                        if (quantity < 0)
                            throw new WrappedException(Messages.Validate_Range_PackingQuantity);

                        var prodPacking = new ProductPacking(product.ProductID, type, quantity, size, weight, note);
                        prodPacking.GenerateProductPackingCode();
                        prodPacking.IsRecordAuditTrail = true;

                        //// insert new or update existing product packing
                        await _productPackingRepository.InsertOrUpdate(prodPacking, new object[] { prodPacking.ProductPackingCode, product.ProductID });

                        await _productPackingRepository.Commit();
                        isSuccess = true;
                    }
                    catch (Exception ex) //// bypass error line
                    {
                        errorLine.Add(i + 1, ex.Message);
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();

                _productPackingRepository = _mainPresenter.Resolve(_productPackingRepository.GetType()) as IProductPackingRepository;

                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                    await LoadMainData();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(String.Format(Messages.Information_ImportSucessfullyWithError, String.Join(",", errorLine)), Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
            }
        }

        /// <summary>
        /// Save product packing to database, 2 cases:
        ///     Case 1: insert new product
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var prodPackings = _mainView.ProductPackings;
            var deletedProdPackings = _mainView.DeletedProductPackings;

            bool isSuccess = false;
            try
            {
                //// foreach deleted product packing and delete it
                foreach (var deletedProdPacking in deletedProdPackings.Where(p => !String.IsNullOrWhiteSpace(p.ProductPackingCode)))
                {
                    deletedProdPacking.IsRecordAuditTrail = true;
                    await _productPackingRepository.Delete(new object[] { deletedProdPacking.ProductPackingCode, deletedProdPacking.ProductID });
                }

                //// foreach new or udpated product packings and insert/update to database
                //// determine product packing is deleted to delete it
                foreach (var prodPacking in prodPackings.Where(p => p.ProductID > 0 && !String.IsNullOrWhiteSpace(p.Type) && p.Quantity.HasValue && p.State == EntityState.Modified))
                {
                    string oldProdPckCode = prodPacking.ProductPackingCode;

                    //prodPacking.ProductID = product.ProductID;
                    prodPacking.IsRecordAuditTrail = true;
                    prodPacking.GenerateProductPackingCode();

                    //// delete old product packing if new product packing code is different old product packing code
                    //// and then insert new one
                    //// this case occurs when user edit quantity/type on existing product packing
                    //Sy VN Remove
                    if (oldProdPckCode != prodPacking.ProductPackingCode && !String.IsNullOrWhiteSpace(oldProdPckCode))
                    {
                        await _productPackingRepository.Delete(new object[] { oldProdPckCode, prodPacking.ProductID });
                    }

                    await _productPackingRepository.InsertOrUpdate(prodPacking, new object[] { prodPacking.ProductPackingCode, prodPacking.ProductID });
                }

                await _productPackingRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _productPackingRepository = _mainPresenter.Resolve(_productPackingRepository.GetType()) as IProductPackingRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadMainData();
                }
            }
        }
    }
}
