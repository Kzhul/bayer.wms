﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IProductMaintView : IBaseView
    {
        DataTable Products { set; }

        IList<Category> Categories { set; }

        IList<UOM> UOMs { set; }

        IList<ProductPacking> ProductPackings { get; set; }

        IList<ProductPacking> DeletedProductPackings { get; set; }

        Product Product { get; set; }

        int SampleQty { get; }

        decimal PrintLabelPercentage { get; }
    }

    public interface IProductMaintPresenter : IBasePresenter
    {
        Task LoadProducts();

        Task LoadCategories();

        Task LoadUOMs();

        void DeleteProductPacking(ProductPacking prodPacking);
    }

    public class ProductMaintPresenter : BasePresenter, IProductMaintPresenter
    {
        private IProductMaintView _mainView;
        private IProductRepository _productRepository;
        private ICategoryRepository _categoryRepository;
        private IUOMRepository _uomRepository;
        private IProductPackingRepository _productPackingRepository;

        public ProductMaintPresenter(IProductMaintView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IProductRepository productRepository,
            ICategoryRepository categoryRepository,
            IUOMRepository uomRepository,
            IProductPackingRepository productPackingRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _uomRepository = uomRepository;
            _productPackingRepository = productPackingRepository;

            _mainView.ProductPackings = new List<ProductPacking>();
            _mainView.DeletedProductPackings = new List<ProductPacking>();
        }

        /// <summary>
        /// Event trigger when user select item in combobox
        /// </summary>
        /// <param name="selectedItem"></param>
        public async Task OnProductCodeSelectChange(DataRow selectedItem)
        {
            try
            {
                _mainView.ProductPackings = new List<ProductPacking>();
                _mainView.DeletedProductPackings = new List<ProductPacking>();

                var product = selectedItem.ToEntity<Product>();
                await LoadProductPackings(product.ProductID);
                product.State = EntityState.Unchanged;
                _mainView.Product = product;

            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get products from database to combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadProducts()
        {
            try
            {
                var products = await _productRepository.GetIncludeCategoryAsync(p => !p.IsDeleted);
                _mainView.Products = products.OrderBy(a => a.Description).ToList().ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get categories from database for combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadCategories()
        {
            try
            {
                var categories = await _categoryRepository.GetAsync(p => !p.IsDeleted);
                categories.Insert(0, new Category());
                _mainView.Categories = categories;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get UOMs from database for combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadUOMs()
        {
            try
            {
                var uoms = await _uomRepository.GetAsync(p => !p.IsDeleted);
                uoms.Insert(0, new UOM());
                _mainView.UOMs = uoms;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get Product Packing from database for datagridview
        /// </summary>
        /// <returns></returns>
        public async Task LoadProductPackings(int productID)
        {
            try
            {
                _mainView.ProductPackings = await _productPackingRepository.GetAsync(p => p.ProductID == productID);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public void DeleteProductPacking(ProductPacking prodPacking)
        {
            try
            {
                _mainView.DeletedProductPackings.Add(prodPacking);
                _mainView.ProductPackings.Remove(prodPacking);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override void Insert()
        {
            _mainView.Product = new Product();
            _mainView.ProductPackings = new List<ProductPacking>();
            _mainView.DeletedProductPackings = new List<ProductPacking>();
        }

        /// <summary>
        /// Save product to database, 2 cases:
        ///     Case 1: insert new product
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var product = _mainView.Product;
            var prodPackings = _mainView.ProductPackings;
            var deletedProdPackings = _mainView.DeletedProductPackings;

            product.SampleQty = _mainView.SampleQty;
            product.PrintLabelPercentage = _mainView.PrintLabelPercentage;

            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _productRepository = unitOfWork.Register(_productRepository.GetType()) as IProductRepository;
                    _productPackingRepository = unitOfWork.Register(_productPackingRepository.GetType()) as IProductPackingRepository;

                    //// check product id to determine next action (insert or update)
                    if (!product.CategoryID.HasValue || product.CategoryID == 0)
                    {
                        product.CategoryID = 1;
                    }

                    product.IsRecordAuditTrail = true;
                    if (product.ProductID == 0)
                    {
                        await _productRepository.Insert(product);

                        //// commit to get new product id
                        await _productRepository.Commit();
                    }
                    else if (product.State == EntityState.Modified)
                    {
                        await _productRepository.Update(product, new object[] { product.ProductID });
                    }

                    //// foreach deleted product packing and delete it
                    foreach (var prodPacking in deletedProdPackings.Where(p => !String.IsNullOrWhiteSpace(p.ProductPackingCode)))
                    {
                        prodPacking.IsRecordAuditTrail = true;
                        prodPacking.ProductID = product.ProductID;
                        await _productPackingRepository.Delete(new object[] { prodPacking.ProductPackingCode, prodPacking.ProductID });
                    }

                    //// foreach new or udpated product packings and insert/update to database
                    //// determine product packing is deleted to delete it
                    foreach (var prodPacking in prodPackings.Where(p => 
                                !String.IsNullOrWhiteSpace(p.Type) 
                                && p.Quantity.HasValue 
                                && (p.State == EntityState.Modified || p.State == EntityState.Deleted)
                                )
                    )
                    {
                        string oldProdPckCode = prodPacking.ProductPackingCode;

                        prodPacking.ProductID = product.ProductID;
                        prodPacking.IsRecordAuditTrail = true;
                        prodPacking.GenerateProductPackingCode();

                        //// delete old product packing if new product packing code is different old product packing code
                        //// and then insert new one
                        //// this case occurs when user edit quantity/type on existing product packing
                        //Sy VN Remove
                        if (oldProdPckCode != prodPacking.ProductPackingCode && !String.IsNullOrWhiteSpace(oldProdPckCode))
                        {
                            await _productPackingRepository.Delete(new object[] { oldProdPckCode, prodPacking.ProductID });
                        }

                        await _productPackingRepository.InsertOrUpdate(prodPacking, new object[] { prodPacking.ProductPackingCode, prodPacking.ProductID });
                    }

                    await unitOfWork.Commit();
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            case 2627: //// Duplicate unique key
                                message = await ResolveDuplicate(product);
                                break;
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _productRepository = _mainPresenter.Resolve(_productRepository.GetType()) as IProductRepository;
                _productPackingRepository = _mainPresenter.Resolve(_productPackingRepository.GetType()) as IProductPackingRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadProducts();

                    _mainView.Product = product;
                }
            }
        }

        /// <summary>
        /// Delete logical
        /// </summary>
        /// <returns></returns>
        public override async Task Delete()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var product = _mainView.Product;
            bool isSuccess = false;
            try
            {
                product.IsDeleted = true;
                product.IsRecordAuditTrail = true;

                await _productRepository.Update(product, new string[] { "IsDeleted" }, new object[] { product.ProductID });
                await _productRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                {
                    await LoadProducts();
                    Insert();
                }
            }
        }

        /// <summary>
        /// Resolve when insert new product which product code is already existed, 2 cases:
        ///     Case 1: existing product is not deleted, throw exception.
        ///     Case 2: existing product is deleted, ask user to undelete it
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public async Task<string> ResolveDuplicate(Product product)
        {
            string message = String.Empty;
            var existProduct = await _productRepository.GetSingleAsync(p => p.ProductCode == product.ProductCode);
            if (!existProduct.IsDeleted)
            {
                message = Messages.Error_ProductCodeExisted;
            }
            else if (_mainView.GetConfirm(Messages.Question_RecoverDeleted))
            {
                product.ProductID = existProduct.ProductID;
                product.RowVersion = existProduct.RowVersion;
                await _productRepository.Update(product, new object[] { product.ProductID });
                await _productRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
            }

            return message;
        }
    }
}
