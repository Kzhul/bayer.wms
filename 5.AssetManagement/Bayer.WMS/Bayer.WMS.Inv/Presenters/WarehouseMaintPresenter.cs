﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IWarehouseMaintView : IBaseView
    {
        DataTable Warehouses { set; }

        Warehouse Warehouse { get; set; }
    }

    public interface IWarehouseMaintPresenter : IBasePresenter
    {
        Task LoadWarehouses();
    }

    public class WarehouseMaintPresenter : BasePresenter, IWarehouseMaintPresenter
    {
        private IWarehouseMaintView _mainView;
        private IWarehouseRepository _warehouseRepository;

        public WarehouseMaintPresenter(IWarehouseMaintView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IWarehouseRepository warehouseRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _warehouseRepository = warehouseRepository;
        }

        /// <summary>
        /// Event trigger when user select item in combobox
        /// </summary>
        /// <param name="selectedItem"></param>
        public async Task OnWarehouseCodeSelectChange(DataRow selectedItem)
        {
            try
            {
                var warehouse = selectedItem.ToEntity<Warehouse>();

                warehouse.State = EntityState.Unchanged;
                _mainView.Warehouse = warehouse;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get products from database to combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadWarehouses()
        {
            try
            {
                var warehouses = await _warehouseRepository.GetAsync(p => !p.IsDeleted);
                _mainView.Warehouses = warehouses.OrderBy(a => a.Description).ToList().ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override void Insert()
        {
            _mainView.Warehouse = new Warehouse();
        }

        /// <summary>
        /// Save product to database, 2 cases:
        ///     Case 1: insert new product
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var warehouse = _mainView.Warehouse;

            bool isSuccess = false;
            try
            {
                //// check warehouse id to determine next action (insert or update)
                warehouse.IsRecordAuditTrail = true;
                if (warehouse.WarehouseID == 0)
                    await _warehouseRepository.Insert(warehouse);
                else if (warehouse.State == EntityState.Modified)
                    await _warehouseRepository.Update(warehouse, new object[] { warehouse.WarehouseID });

                await _warehouseRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            case 2627: //// Duplicate unique key
                                message = await ResolveDuplicate(warehouse);
                                break;
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadWarehouses();

                    _mainView.Warehouse = warehouse;
                }
            }
        }

        /// <summary>
        /// Delete logical
        /// </summary>
        /// <returns></returns>
        public override async Task Delete()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var warehouse = _mainView.Warehouse;
            bool isSuccess = false;
            try
            {
                warehouse.IsDeleted = true;
                warehouse.IsRecordAuditTrail = true;

                await _warehouseRepository.Update(warehouse, new string[] { "IsDeleted" }, new object[] { warehouse.WarehouseID });
                await _warehouseRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                {
                    await LoadWarehouses();
                    Insert();
                }
            }
        }

        /// <summary>
        /// Resolve when insert new product which product code is already existed, 2 cases:
        ///     Case 1: existing product is not deleted, throw exception.
        ///     Case 2: existing product is deleted, ask user to undelete it
        /// </summary>
        /// <param name="warehouse"></param>
        /// <returns></returns>
        public async Task<string> ResolveDuplicate(Warehouse warehouse)
        {
            string message = String.Empty;
            var existProduct = await _warehouseRepository.GetSingleAsync(p => p.WarehouseCode == warehouse.WarehouseCode);
            if (!existProduct.IsDeleted)
            {
                message = Messages.Error_ProductCodeExisted;
            }
            else if (_mainView.GetConfirm(Messages.Question_RecoverDeleted))
            {
                warehouse.WarehouseID = existProduct.WarehouseID;
                warehouse.RowVersion = existProduct.RowVersion;
                await _warehouseRepository.Update(warehouse, new object[] { warehouse.WarehouseID });
                await _warehouseRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
            }

            return message;
        }
    }
}
