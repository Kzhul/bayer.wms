﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using System.IO;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IReportBay4sView : IBaseView
    {
        System.Data.DataTable ReportBay4sDetails { get; set; }
        System.Data.DataTable ReportBay4sTotals { get; set; }
    }

    public interface IReportBay4sPresenter : IBasePresenter
    {
        Task LoadHeader();
        Task LoadDetail(string productCode, string productLot);
        Task ImportExcel(string fileName, string updateNote);
        Task Print();
    }

    public class ReportBay4sPresenter : BasePresenter, IReportBay4sPresenter
    {
        private IReportBay4sView _mainView;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository;

        public ReportBay4sPresenter(IReportBay4sView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryOrderHeaderRepository = deliveryOrderHeaderRepository;
            _mainView.ReportBay4sTotals = new System.Data.DataTable();
            _mainView.ReportBay4sDetails = new System.Data.DataTable();
            LoadHeader();
        }

        public async Task LoadHeader()
        {
            _mainView.ReportBay4sTotals = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_Bay4SData_ReportTotal");
        }

        public async Task LoadDetail(string productCode, string productLot)
        {
            List<SqlParameter> listParam = new List<SqlParameter>();
            SqlParameter paramProductCode = new SqlParameter("ProductCode", productCode);
            listParam.Add(paramProductCode);
            SqlParameter paramProductLot = new SqlParameter("ProductLot", productLot);
            listParam.Add(paramProductLot);
            _mainView.ReportBay4sDetails = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_Bay4SData_ReportDetail", listParam.ToArray());
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var details = _mainView.ReportBay4sTotals;

            List<SqlParameter> listParam = new List<SqlParameter>();
            SqlParameter paramProductCode = new SqlParameter("ProductCode", string.Empty);
            listParam.Add(paramProductCode);
            SqlParameter paramProductLot = new SqlParameter("ProductLot", string.Empty);
            listParam.Add(paramProductLot);
            var inventoryByPallets = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_Bay4SData_ReportDetail", listParam.ToArray());

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\ReportBay4S.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            bool isSuccess = false;
            int cellRowIndex = 2;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.Sheets[2];
                int n = inventoryByPallets.Rows.Count;
                #region Fill Data Detail
                for (int i = 0; i < n; i++)
                {
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["ProductCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["ProductName"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["BatchCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["BatchCodeDistributor"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["UOM"];
                    cellColumnIndex++;
                    
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["ZoneName"];
                    cellColumnIndex++;
                                        
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["LocationName"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["PalletCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["PackSize"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = inventoryByPallets.Rows[i]["Quantity"];
                    cellColumnIndex++;
                    
                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                #endregion


                //////////////////////////////////////////////
                sheetDetail = (Worksheet)workbook.Sheets[1];
                cellRowIndex = 2;
                cellColumnIndex = 1;
                #region Fill Data Detail
                n = details.Rows.Count;
                for (int i = 0; i < n; i++)
                {
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductCode"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductName"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["BatchCode"];
                    cellColumnIndex++;                    

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["UOM"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["Unrestricted"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["InQualityInsp"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["Blocked"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["SumQuantity"];
                    cellColumnIndex++; 

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["Quantity"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DifQuantity"];
                    cellColumnIndex++;                    

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ManufacturingDate"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ExpiredDate"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["PackSize"];
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["BatchCodeDistributor"];
                    cellColumnIndex++;


                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "ReportBay4s" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion
                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        public async Task ImportExcel(string fileName, string updateNote)
        {
            #region Param prepare
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            string start = "A1";
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            //_Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            bool isSuccess = false;
            int lineNBR = 0;
            int totalErrorCount = 0;
            int errorCount = 0;
            string errorMessage = string.Empty;
            #endregion

            try
            {
                workbook = excelApp.Workbooks.Open(fileName);
                //sheetDetail = (Worksheet)workbook.Worksheets[1];
                foreach (Worksheet sheetDetail in workbook.Worksheets)
                {
                    #region Prepare for import sheet
                    if (sheetDetail.AutoFilter != null)
                    {
                        sheetDetail.AutoFilterMode = false;
                    }

                    //// get a range to work with
                    range = sheetDetail.get_Range(start, Missing.Value);
                    //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                    range = range.get_End(XlDirection.xlDown);
                    //// get the address of the bottom cell
                    string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                    //// specific end column
                    end = downAddress.Remove(0, 1);
                    int countRows = sheetDetail.UsedRange.Rows.Count;
                    //// throw exception to prevent loop through large data
                    if (countRows > 1000000)
                        throw new WrappedException(Messages.Validate_ExcelFile_Large);
                    end = "L" + countRows;
                    //// Get the range, then values from start to end
                    range = sheetDetail.get_Range(start, end);

                    var values = (object[,])range.Value2;
                    int count = values.GetLength(0);
                    #endregion

                    int maxLineNbr = 1;
                    int beginImport = maxLineNbr + 1;

                    #region ClearData
                    await _deliveryOrderHeaderRepository.ExecuteNonQuery("proc_Bay4SData_Clear");
                    #endregion

                    #region Import
                    for (int i = beginImport; i <= count; i++)
                    {
                        errorCount = 0;
                        errorMessage = string.Empty;
                        try
                        {                            
                            string productCode = $"{values[i, 3]}".Trim();
                            string productName = $"{values[i, 4]}".Trim();
                            string batchCode = $"{values[i, 5]}".Trim();
                            string batchCodeDistributor = $"{values[i, 6]}".Trim();
                            string uom = $"{values[i, 7]}".Trim();
                            
                            string qtyUnrestricted = $"{values[i, 9]}".Trim();
                            string qtyInQualityInsp = $"{values[i, 10]}".Trim();
                            string qtyBlocked = $"{values[i, 11]}".Trim();

                            DateTime expiredDate = DateTime.Today;
                            try
                            {
                                double d_ExpiredDate;
                                double.TryParse($"{values[i, 8]}", out d_ExpiredDate);
                                string str_ExpiredDate = $"{values[i, 8]}";
                                expiredDate = d_ExpiredDate > 0 ? DateTime.FromOADate(d_ExpiredDate) : DateTime.Parse(str_ExpiredDate);
                            }
                            catch
                            {

                            }

                            DateTime manufacturingDate = DateTime.Today;
                            try
                            {
                                double d_ManufacturingDate;
                                double.TryParse($"{values[i, 12]}", out d_ManufacturingDate);
                                string str_ManufacturingDate = $"{values[i, 12]}";
                                manufacturingDate = d_ManufacturingDate > 0 ? DateTime.FromOADate(d_ManufacturingDate) : DateTime.Parse(str_ManufacturingDate);
                            }
                            catch
                            {

                            }

                            List<SqlParameter> listParam = new List<SqlParameter>();
                            SqlParameter sqlProductCode = new SqlParameter("ProductCode", productCode); listParam.Add(sqlProductCode);
                            SqlParameter sqlProductName = new SqlParameter("ProductName", productName); listParam.Add(sqlProductName);
                            SqlParameter sqlBatchCode = new SqlParameter("BatchCode", batchCode); listParam.Add(sqlBatchCode);
                            SqlParameter sqlBatchCodeDistributor = new SqlParameter("BatchCodeDistributor", batchCodeDistributor); listParam.Add(sqlBatchCodeDistributor);
                            SqlParameter sqlUOM = new SqlParameter("UOM", uom); listParam.Add(sqlUOM);
                            SqlParameter sqlExpiredDate = new SqlParameter("ExpiredDate", expiredDate); listParam.Add(sqlExpiredDate);
                            SqlParameter sqlUnrestricted = new SqlParameter("Unrestricted", qtyUnrestricted); listParam.Add(sqlUnrestricted);
                            SqlParameter sqlInQualityInsp = new SqlParameter("InQualityInsp", qtyInQualityInsp); listParam.Add(sqlInQualityInsp);
                            SqlParameter sqlBlocked = new SqlParameter("Blocked", qtyBlocked); listParam.Add(sqlBlocked);
                            SqlParameter sqlManufacturingDate = new SqlParameter("ManufacturingDate", manufacturingDate); listParam.Add(sqlManufacturingDate);
                            SqlParameter sqlUserID = new SqlParameter("UserID", LoginInfo.UserID); listParam.Add(sqlUserID);
                            await _deliveryOrderHeaderRepository.ExecuteNonQuery("proc_Bay4SData_Insert", listParam.ToArray());

                            isSuccess = true;
                            }
                            catch (WrappedException ex)
                            {
                                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Information);
                                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                            }
                            catch (Exception ex)
                            {
                                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Error);
                            }
                            finally
                            {
                                _mainPresenter.SetMessage("Đang xử lý: " + sheetDetail.Name + ", Dòng: " + i.ToString(), Utility.MessageType.Information);

                                if (errorCount > 0)
                                {
                                    totalErrorCount += errorCount;
                                    //Tô màu cả dòng
                                    Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A" + i.ToString(), "L" + i.ToString());
                                    foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                                    {
                                        //cell.Borders.Color = Color.Red;
                                        //cell.Borders.Weight = 2;
                                        cell.Font.Color = Color.Red;
                                    }
                                    //Ghi ra dòng lỗi
                                    sheetDetail.Cells[i, 12] = errorMessage;
                                }
                                else
                                {
                                    //Nếu không có lỗi gì, reset lại dòng lỗi và tô màu trước đó, nếu có
                                    //Tô màu cả dòng
                                    Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A" + i.ToString(), "L" + i.ToString());
                                    foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                                    {
                                        //cell.Borders.Color = null;
                                        //cell.Borders.Weight = 0;
                                        cell.Font.Color = Color.Black;
                                    }
                                    //Ghi ra dòng lỗi
                                    sheetDetail.Cells[i, 12] = "";
                                }
                            }
                    }
                    #endregion
                }
            }
            catch (WrappedException ex)
            {
                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Information);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", Messages.Error_Common));
            }
            finally
            {
                #region Garbage Collection
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                #endregion

                #region Message and Load Data
                if (isSuccess)
                {
                    if (totalErrorCount == 0)
                    {
                        //_mainView.DeliveryOrderDetails = listModel;
                        await LoadHeader();
                        _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
                    }
                }
                #endregion
            }
        }
    }
}
