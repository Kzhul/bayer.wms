﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IDeliveryNoteView : IBaseView
    {
        DeliveryNoteHeader DeliveryNoteHeader { get; set; }

        System.Data.DataTable DeliveryNoteHeaders { set; }

        IList<DeliveryNoteDetail> DeliveryNoteDetails { get; set; }

        IList<DeliveryNoteDetail> DeletedDeliveryNoteDetails { get; set; }

        System.Data.DataTable ReportLocationSuggest { get; set; }

        System.Data.DataTable ReportPalletPick { get; set; }
    }

    public interface IDeliveryNotePresenter : IBasePresenter
    {
        Task LoadDeliveryNoteHeader();

        void DeleteDeliveryNoteDetail(DeliveryNoteDetail item);

        Task Print();

        Task Confirm();

        Task ExportSuggestion();

        Task ImportExcel(string fileName);

        Task WarehouseManagerConfirm();

        Task DeletePalletSuggestion(string palletCode);

        Task SavePalletSuggestion(string palletCode);

        Task LoadReportLocationSuggest(string search);
    }

    public class DeliveryNotePresenter : BasePresenter, IDeliveryNotePresenter
    {
        private IDeliveryNoteView _mainView;
        private IDeliveryNoteHeaderRepository _deliveryNoteHeaderRepository;
        private IDeliveryNoteDetailRepository _deliveryNoteDetailRepository;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository;
        private IDeliveryOrderDetailRepository _deliveryOrderDetailRepository;

        public DeliveryNotePresenter(IDeliveryNoteView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryNoteHeaderRepository deliveryNoteHeaderRepository
            , IDeliveryNoteDetailRepository deliverynoteDetailRepository
            , IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository
            , IDeliveryOrderDetailRepository deliveryOrderDetailRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryNoteHeaderRepository = deliveryNoteHeaderRepository;
            _deliveryNoteDetailRepository = deliverynoteDetailRepository;
            _deliveryOrderHeaderRepository = deliveryOrderHeaderRepository;
            _deliveryOrderDetailRepository = deliveryOrderDetailRepository;

            _mainView.DeliveryNoteDetails = new List<DeliveryNoteDetail>();
            _mainView.DeletedDeliveryNoteDetails = new List<DeliveryNoteDetail>();
            _mainView.ReportLocationSuggest = new System.Data.DataTable();
            _mainView.ReportPalletPick = new System.Data.DataTable();
        }

        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            var model = selectedItem.ToEntity<DeliveryNoteHeader>();
            _mainView.DeliveryNoteHeader = model;
            await LoadDeliveryNoteDetails(model.ExportCode);
        }

        public async Task LoadDeliveryNoteHeader()
        {
            var model = await _deliveryNoteHeaderRepository.GetAsync(p => !p.IsDeleted);
            _mainView.DeliveryNoteHeaders = model.OrderByDescending(a => a.CreatedDateTime).ToList().ToDataTable();
        }
        public async Task SavePalletSuggestion(string palletCode)
        {
            var header = _mainView.DeliveryNoteHeader;
            await _deliveryNoteDetailRepository.ExecuteNonQuery("proc_DeliveryNoteDetailSplits_Insert",
                    new SqlParameter
                    {
                        ParameterName = "PalletCode",
                        SqlDbType = SqlDbType.VarChar,
                        Value = palletCode
                    }
                    , new SqlParameter
                    {
                        ParameterName = "DOImportCode",
                        SqlDbType = SqlDbType.VarChar,
                        Value = header.DOImportCode
                    }
                    , new SqlParameter
                    {
                        ParameterName = "ReferenceNbr",
                        SqlDbType = SqlDbType.VarChar,
                        Value = header.ExportCode
                    }
                    , new SqlParameter
                    {
                        ParameterName = "UserID",
                        SqlDbType = SqlDbType.Int,
                        Value = LoginInfo.UserID
                    }
            );
            await LoadDeliveryNoteDetails(header.ExportCode);
        }

        public async Task DeletePalletSuggestion(string palletCode)
        {
            var header = _mainView.DeliveryNoteHeader;
            await _deliveryNoteDetailRepository.ExecuteNonQuery("proc_DeliveryNoteDetailSplits_Delete",
                    new SqlParameter
                    {
                        ParameterName = "PalletCode",
                        SqlDbType = SqlDbType.VarChar,
                        Value = palletCode
                    }
                    , new SqlParameter
                    {
                        ParameterName = "DOImportCode",
                        SqlDbType = SqlDbType.VarChar,
                        Value = header.DOImportCode
                    }
                    , new SqlParameter
                    {
                        ParameterName = "ReferenceNbr",
                        SqlDbType = SqlDbType.VarChar,
                        Value = header.ExportCode
                    }
                    , new SqlParameter
                    {
                        ParameterName = "UserID",
                        SqlDbType = SqlDbType.Int,
                        Value = LoginInfo.UserID
                    }
            );
            await LoadDeliveryNoteDetails(header.DOImportCode);
        }

        public async Task LoadDeliveryNoteDetails(string exportCode)
        {
            _mainView.DeliveryNoteDetails = await _deliveryNoteDetailRepository.GetIncludeProductAndPalletAsync(p => p.ExportCode == exportCode);

            _mainView.ReportLocationSuggest = await _deliveryNoteDetailRepository.ExecuteDataTable("proc_PalletStatuses_SuggestionForDO",
                        new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = _mainView.DeliveryNoteHeader.DOImportCode },
                        new SqlParameter { ParameterName = "Search", SqlDbType = SqlDbType.VarChar, Value = string.Empty }
                        );

            _mainView.ReportPalletPick = await _deliveryNoteDetailRepository.ExecuteDataTable("proc_PalletStatuses_PalletPick",
                        new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = _mainView.DeliveryNoteHeader.DOImportCode });
        }

        public async Task LoadReportLocationSuggest(string search)
        {
            _mainView.ReportLocationSuggest = await _deliveryNoteDetailRepository.ExecuteDataTable("proc_PalletStatuses_SuggestionForDO",
                      new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = _mainView.DeliveryNoteHeader.DOImportCode },
                      new SqlParameter { ParameterName = "Search", SqlDbType = SqlDbType.VarChar, Value = search }
                      );
        }

        public void DeleteDeliveryNoteDetail(DeliveryNoteDetail item)
        {
            try
            {
                _mainView.DeletedDeliveryNoteDetails.Add(item);
                _mainView.DeliveryNoteDetails.Remove(item);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryNoteHeader;
            var details = _mainView.DeliveryNoteDetails;
            var deletedDetails = _mainView.DeletedDeliveryNoteDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _deliveryNoteHeaderRepository = unitOfWork.Register(_deliveryNoteHeaderRepository.GetType()) as IDeliveryNoteHeaderRepository;
                    _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                    _deliveryNoteDetailRepository = unitOfWork.Register(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;
                    _deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;

                    var listOrderDetail = _deliveryOrderDetailRepository.Get(a => a.DOImportCode == header.DOImportCode).ToList();
                    var listAnotherNoteDetails = _deliveryNoteDetailRepository.Get(a => a.DOCode == header.DOImportCode && a.ExportCode != header.ExportCode).ToList();

                    #region VerifyDetailQuantity
                    try
                    {
                        string productErr = string.Empty;
                        if (listOrderDetail != null && listOrderDetail.Count > 0 && details.Count > 0)
                        {
                            //List<string, string> listProduct = listOrderDetail.Select(a => a.ProductCode).Distinct().ToList();
                            var listMaterialCodeBatchCode = details.Select(a => new { a.ProductID, a.BatchCode }).Distinct().ToList();

                            foreach (var item in listMaterialCodeBatchCode)
                            {
                                decimal totalNoteQuantity = 0;
                                decimal? totalAnotherNoteQuantity = listAnotherNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                                totalAnotherNoteQuantity = totalAnotherNoteQuantity ?? totalAnotherNoteQuantity;
                                decimal? totalThisNoteQuantity = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                                totalThisNoteQuantity = totalThisNoteQuantity ?? totalThisNoteQuantity;
                                totalNoteQuantity = totalThisNoteQuantity.Value + totalAnotherNoteQuantity.Value;

                                decimal? totalOrderQuantity = listOrderDetail.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                                totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                                if (totalNoteQuantity > totalOrderQuantity)
                                {
                                    decimal? totalNoteQuantityValid = totalOrderQuantity - totalAnotherNoteQuantity;
                                    var firstDetailErr = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).FirstOrDefault();
                                    productErr = firstDetailErr.ProductCode + "----" + firstDetailErr.BatchCode + "----O:" + totalOrderQuantity.ToString() + "----A:" + totalAnotherNoteQuantity.ToString() + "----N:" + totalThisNoteQuantity.ToString();
                                    //throw new WrappedException(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr);
                                    _mainPresenter.SetMessage(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr, Utility.MessageType.Error);
                                    return;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }
                    #endregion

                    #region Save DN Header and Details
                    if (String.IsNullOrWhiteSpace(header.ExportCode))
                    {
                        string code = await _deliveryNoteHeaderRepository.GetNextNBR();
                        header.DeliveryDate = DateTime.ParseExact(header.StrDeliveryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        header.ExportCode = code;
                        header.Status = DeliveryOrderHeader.status.InProgress;
                        header.IsDeleted = false;
                        await _deliveryNoteHeaderRepository.Insert(header);
                    }
                    else
                    {
                        await _deliveryNoteHeaderRepository.Update(header, new object[] { header.ExportCode });
                    }

                    //// foreach deleted DO details and delete it
                    foreach (var item in deletedDetails)
                    {
                        await _deliveryNoteDetailRepository.Delete(new object[] { item.DOCode, item.ExportCode, item.ProductID, item.BatchCode });
                    }

                    foreach (var item in details)
                    {
                        item.ExportCode = header.ExportCode;
                        item.Status = DeliveryOrderHeader.status.InProgress;
                        await _deliveryNoteDetailRepository.InsertOrUpdate(item, new object[] { item.DOCode, item.ExportCode, item.ProductID, item.BatchCode });
                    }
                    #endregion

                    await unitOfWork.Commit();
                }

                #region Update DO Status
                {
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("Code", header.ExportCode);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("DOImportCode", header.DOImportCode);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("Type", "DeliveryNote");
                    listParam.Add(param3);
                    SqlParameter param4 = new SqlParameter("Status", DeliveryNoteHeader.status.InProgress);
                    listParam.Add(param4);

                    await _deliveryNoteHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                    isSuccess = true;
                }
                #endregion

                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _deliveryNoteHeaderRepository = _mainPresenter.Resolve(_deliveryNoteHeaderRepository.GetType()) as IDeliveryNoteHeaderRepository;
                _deliveryNoteDetailRepository = _mainPresenter.Resolve(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);

                    await LoadDeliveryNoteHeader();
                    //await LoadDeliveryNoteDetails(header.ExportCode);
                }
            }

        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryNoteHeader;
            var details = _mainView.DeliveryNoteDetails;
            var deletedDetails = _mainView.DeletedDeliveryNoteDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuXuatHang.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\PhieuXuatHang\";

            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            //config Template Element
            string positionDelivertDate = "C6";
            string positionExportNote = "C7";
            string positionExportNoteBarCode = "G7";

            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                int cellRowIndex = 10;
                int cellColumnIndex = 1;
                int productIndex = 1;

                var listProduct = details.Select(a => a.ProductID).Distinct().ToList();

                #region Fill Data Detail
                //Loop through each row and read value from each column. 
                foreach (int productID in listProduct)
                {
                    #region Process for ProductName Row
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = productIndex.ToString();
                    productIndex++;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.FirstOrDefault(a => a.ProductID == productID).ProductDescription;
                    cellColumnIndex++;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(details.Where(a => a.ProductID == productID && a.Quantity.HasValue).Sum(a => a.Quantity), 2);
                    //End of Row
                    cellColumnIndex = 1;
                    cellRowIndex++;
                    #endregion

                    var listDetailByProduct = details.Where(a => a.ProductID == productID).OrderBy(a => a.BatchCode).Distinct().ToList();
                    #region Process for BatchCode Row
                    foreach (var item in listDetailByProduct)
                    {
                        //Begin of Row
                        cellColumnIndex++;
                        cellColumnIndex++;

                        if (!string.IsNullOrEmpty(item.BatchCodeDistributor))
                        {
                            item.BatchCode += "/ " + item.BatchCodeDistributor;
                        }
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCode;
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(item.Quantity);
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(item.PackQuantity);
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.StrPackType;
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(item.ProductQuantity);
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Shipper1;
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.StrReceiver1;
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Shipper2;
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.StrReceiver2;
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Shipper3;
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.StrReceiver3;

                        cellColumnIndex++;

                        //End of Row
                        cellColumnIndex = 1;
                        cellRowIndex++;


                        //Set border and word wrap
                    }
                    #endregion
                }

                //People Sign

                //sheetDetail.Cells[cellRowIndex, 2]. .Style.Alignment.WrapText = true;
                //sheetDetail.get_Range("A" + cellRowIndex.ToString(), "M" + cellRowIndex.ToString()).b = false;
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A9", "M" + (cellRowIndex - 1).ToString());
                //foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                //{
                //    cell.BorderAround();
                //}
                cellRowIndex++;
                sheetDetail.get_Range("A" + cellRowIndex.ToString(), "M" + cellRowIndex.ToString()).WrapText = false;
                sheetDetail.Cells[cellRowIndex, 2] = "Người giao hàng kí tên";
                sheetDetail.Cells[cellRowIndex, 9] = "Người nhận hàng kí tên";


                #endregion

                #region Fill Data Header
                sheetDetail.get_Range(positionDelivertDate, positionDelivertDate).Cells[0] = header.StrDeliveryDate;
                sheetDetail.get_Range(positionExportNote, positionExportNote).Cells[0] = header.ExportCode;
                sheetDetail.get_Range(positionExportNoteBarCode, positionExportNoteBarCode).Cells[0] = "*" + header.ExportCode + "*";
                #endregion

                #region Fill Page Number Info
                sheetDetail.PageSetup.LeftFooter = "Phát hành theo biểu mẫu WH-55-03 đã được đăng kí"; // This worked correctly
                sheetDetail.PageSetup.RightFooter = "Page &P of &N"; // This worked correctly
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.ExportCode + ".xlsx";

                //Set Print Area
                sheetDetail.PageSetup.PrintArea = ("A1:M" + cellRowIndex.ToString());

                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    //printFromExcel(exportPath);

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully + ": " + header.ExportCode, Utility.MessageType.Information);
                }
            }
        }

        #region ExcelPrint
        // Module-level "dummy object" variable for optional parameters
        public static object optionalParameter = Type.Missing;

        public static void printFromExcel(string yourFileName)
        {
            Microsoft.Office.Interop.Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();

            xl.Workbooks.Open(yourFileName, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

            xl.Worksheets.PrintOut(optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

            if (xl != null)
                xl.Quit();
        }
        #endregion

        public async Task Confirm()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var header = _mainView.DeliveryNoteHeader;
            bool isSuccess = false;

            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                SqlParameter param1 = new SqlParameter("Code", header.ExportCode);
                listParam.Add(param1);
                SqlParameter param2 = new SqlParameter("DOImportCode", header.DOImportCode);
                listParam.Add(param2);
                SqlParameter param3 = new SqlParameter("Type", "DeliveryNote");
                listParam.Add(param3);
                SqlParameter param4 = new SqlParameter("Status", DeliveryNoteHeader.status.Complete);
                listParam.Add(param4);

                await _deliveryNoteHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _deliveryNoteHeaderRepository = _mainPresenter.Resolve(_deliveryNoteHeaderRepository.GetType()) as IDeliveryNoteHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_ConfirmSucessfully, Utility.MessageType.Information);

                    await LoadDeliveryNoteHeader();
                    //await LoadDeliveryNoteDetails(header.ExportCode);
                }
            }
        }

        public async Task WarehouseManagerConfirm()
        {
            //proc_DeliveryNoteDetailSplits_WarehouseKeeperConfirm
            var header = _mainView.DeliveryNoteHeader;
            List<SqlParameter> listParam = new List<SqlParameter>();
            SqlParameter param1 = new SqlParameter("DOImportCode", header.DOImportCode);
            listParam.Add(param1);
            SqlParameter param2 = new SqlParameter("ReferenceNbr", header.ExportCode);
            listParam.Add(param2);
            SqlParameter param3 = new SqlParameter("UserID", LoginInfo.UserID);
            listParam.Add(param3);

            await _deliveryNoteHeaderRepository.ExecuteNonQuery("proc_DeliveryNoteDetailSplits_WarehouseKeeperConfirm", listParam.ToArray());
            _mainPresenter.SetMessage(Messages.Information_ConfirmSucessfully, Utility.MessageType.Information);
        }

        public async Task ExportSuggestion()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryNoteHeader;
            //var details = _mainView.ReportDOProcess;
            var details = await _deliveryNoteDetailRepository.ExecuteDataTable("proc_PalletStatuses_SuggestionForDO",
                         new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = header.DOImportCode });

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\ExportSuggestion.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 2;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                //int i = 0;
                int rowCount = details.Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductCode"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductName"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductLot"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["PalletCode"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DOQuantity"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["PalletQuantity"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["LocationName"].ToString();
                    cellColumnIndex++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ZoneName"].ToString();
                    cellColumnIndex++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["LineName"].ToString();
                    cellColumnIndex++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["LengthID"].ToString();
                    cellColumnIndex++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["LevelID"].ToString();
                    cellColumnIndex++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["LocationCode"].ToString();
                    cellColumnIndex++;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A2", "K" + (cellRowIndex - 1).ToString());
                //foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                //{
                //    cell.BorderAround();
                //}
                #endregion

                #region Fill Data Header
                //string positionDelivertDate = "C4";
                //string positionExportNote = "C5";
                //sheetDetail.get_Range(positionDelivertDate, positionDelivertDate).Cells[0] = header.DeliveryDate;
                //sheetDetail.get_Range(positionExportNote, positionExportNote).Cells[0] = header.DOImportCode;
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.ExportCode + "Pallet_GoiY" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        public async Task ImportExcel(string fileName)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            string start = "A1";
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            try
            {
                workbook = workbooks.Open(fileName);
                sheetDetail = (Worksheet)workbook.ActiveSheet;
                //// get a range to work with
                range = sheetDetail.get_Range(start, Missing.Value);

                //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                range = range.get_End(XlDirection.xlDown);

                //// get the address of the bottom cell
                string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                //// specific end column
                end = downAddress.Remove(0, 1);

                end = "A" + end;
                end = "A5000";

                //// Get the range, then values from start to end
                range = sheetDetail.get_Range(start, end);
                var values = (object[,])range.Value2;
                int count = values.GetLength(0);

                var header = _mainView.DeliveryNoteHeader;
                int beginImport = 2;
                for (int i = beginImport; i <= count; i++)
                {
                    string palletCode = $"{values[i, 1]}";
                    if (!string.IsNullOrEmpty(palletCode))
                    {
                        await _deliveryNoteDetailRepository.ExecuteNonQuery("proc_DeliveryNoteDetailSplits_Insert",
                            new SqlParameter { ParameterName = "PalletCode", SqlDbType = SqlDbType.VarChar, Value = palletCode }
                            , new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = header.DOImportCode }
                            , new SqlParameter { ParameterName = "ReferenceNbr", SqlDbType = SqlDbType.VarChar, Value = header.ExportCode }
                            , new SqlParameter { ParameterName = "UserID", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID }
                            );
                    }
                }

                await LoadDeliveryNoteDetails(header.ExportCode);
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(String.Format(Messages.Information_ImportSucessfullyWithError, String.Join(",", errorLine)), Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
            }
        }
    }
}
