﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IWarehouseVisualizationView : IBaseView
    {
        Warehouse Warehouse { get; set; }
        DataTable Warehouses { set; }
        IList<Zone> Zones { get; set; }
        IList<ZoneLine> ZoneLines { get; set; }
        IList<ZoneLocation> ZoneLocations { get; set; }
        IList<WarehouseVisualization> WarehouseVisualizations { get; set; }
    }

    public interface IWarehouseVisualizationPresenter : IBasePresenter
    {
        Task LoadWarehouses();

        Task LoadWarehouseVisualizations();
    }

    public class WarehouseVisualizationPresenter : BasePresenter, IWarehouseVisualizationPresenter
    {
        private IWarehouseVisualizationView _mainView;
        private IZoneRepository _zoneRepository;
        private IZoneLineRepository _zoneLineRepository;
        private IZoneLocationRepository _zoneLocationRepository;
        private IWarehouseVisualizationRepository _warehouseVisuliazationRepository;
        private IWarehouseRepository _warehouseRepository;

        public WarehouseVisualizationPresenter(IWarehouseVisualizationView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IZoneRepository zoneRepository,
            IZoneLineRepository zoneLineRepository,
            IZoneLocationRepository zoneLocationRepository,
            IWarehouseVisualizationRepository warehouseVisuliazationRepository,
            IWarehouseRepository warehouseRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _zoneRepository = zoneRepository;
            _zoneLineRepository = zoneLineRepository;
            _zoneLocationRepository = zoneLocationRepository;
            _warehouseVisuliazationRepository = warehouseVisuliazationRepository;
            _warehouseRepository = warehouseRepository;
        }

        public async Task OnWarehouseCodeSelectChange(DataRow selectedItem)
        {
            try
            {
                var warehouse = selectedItem.ToEntity<Warehouse>();

                await LoadZoneLines(warehouse.WarehouseID);
                await LoadZoneLocations(warehouse.WarehouseID);
                await LoadZones(warehouse.WarehouseID);

                _mainView.Warehouse = warehouse;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadWarehouses()
        {
            try
            {
                var warehouses = await _warehouseRepository.GetAsync(p => !p.IsDeleted);
                _mainView.Warehouses = warehouses.OrderBy(a => a.Description).ToList().ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadZones(int warehouseID)
        {
            try
            {
                _mainView.Zones = await _zoneRepository.GetAsync(p => p.WarehouseID == warehouseID && !p.IsDeleted);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadZoneLines(int warehouseID)
        {
            try
            {
                _mainView.ZoneLines = await _zoneLineRepository.GetByWarehouseID(warehouseID);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadZoneLocations(int warehouseID)
        {
            try
            {
                _mainView.ZoneLocations = await _zoneLocationRepository.GetByWarehouseID(warehouseID);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadWarehouseVisualizations()
        {
            try
            {
                _mainView.WarehouseVisualizations = await _warehouseVisuliazationRepository.GetAsync(p => p.UserID == LoginInfo.UserID);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async Task Save()
        {
            var visualizatons = _mainView.WarehouseVisualizations;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _warehouseVisuliazationRepository = unitOfWork.Register(_warehouseVisuliazationRepository.GetType()) as IWarehouseVisualizationRepository;

                    foreach (var item in visualizatons)
                    {
                        await _warehouseVisuliazationRepository.InsertOrUpdate(item, new object[] { item.UserID, item.Control });
                    }

                    await unitOfWork.Commit();
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _warehouseVisuliazationRepository = _mainPresenter.Resolve(_warehouseVisuliazationRepository.GetType()) as IWarehouseVisualizationRepository;
                await LoadWarehouseVisualizations();
            }
        }
    }
}
