namespace Bayer.WMS.Launcher
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AppSetting
    {
        [Key]
        [StringLength(255)]
        public string SettingID { get; set; }

        [StringLength(255)]
        public string SettingValue { get; set; }
    }
}
