﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Launcher
{
    static class Program
    {
        private static byte[] bytes = Encoding.ASCII.GetBytes("BayerWMS");

        public static string ConnStr;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LauncherView());
        }

        public static string DescryptConnString(string cryptedConnStr)
        {
            try
            {
                string[] connStrComponent = cryptedConnStr.Split(';');

                string dataSource = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("data source")).Split('=')[1].Trim();
                string initialCatalog = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("initial catalog")).Split('=')[1].Trim();
                string[] splitUserID = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("user id")).Split('=');
                string[] splitPwd = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("pwd")).Split('=');

                string connStr = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Pwd={3};MultipleActiveResultSets=True;Persist Security Info=True;",
                    dataSource, initialCatalog, AESDecrypt(splitUserID[1].Trim() + (splitUserID.Length == 3 ? "=" : splitUserID.Length == 4 ? "==" : String.Empty)), AESDecrypt(splitPwd[1].Trim() + (splitPwd.Length == 3 ? "=" : splitPwd.Length == 4 ? "==" : String.Empty)));

                return connStr;
            }
            catch (Exception ex)
            {
                //Do Nothing here
            }
            return string.Empty;
        }

        public static string AESEncrypt(string planString)
        {
            if (String.IsNullOrEmpty(planString))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);

            var writer = new StreamWriter(cryptoStream);
            writer.Write(planString);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();

            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        public static string AESDecrypt(string cipherText)
        {
            try
            {
                if (String.IsNullOrEmpty(cipherText))
                    return String.Empty;

                var cryptoProvider = new DESCryptoServiceProvider();
                var memoryStream = new MemoryStream(Convert.FromBase64String(cipherText));
                var cryptoStream = new CryptoStream(memoryStream,
                    cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
                var reader = new StreamReader(cryptoStream);

                return reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                return cipherText;
                //Do Nothing here
            }
            return string.Empty;
        }
    }
}
