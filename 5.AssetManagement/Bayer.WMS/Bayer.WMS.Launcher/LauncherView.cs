﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Launcher
{
    public partial class LauncherView : Form
    {
        public LauncherView()
        {
            InitializeComponent();
        }

        private void LauncherView_Load(object sender, EventArgs e)
        {
            Thread thread = new Thread(delegate ()
            {
                ExeConfigurationFileMap configFile = new ExeConfigurationFileMap();
                configFile.ExeConfigFilename = "Bayer.WMS.exe.config";
                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFile, ConfigurationUserLevel.None);
                bool updated = false;
                try
                {
                    Program.ConnStr = Program.DescryptConnString(config.ConnectionStrings.ConnectionStrings["BayerWMSContext"].ConnectionString);
                    AppSetting newVersion, updatePath;
                    string currentVersion;
                    using (var model = new BayerWMSContext())
                    {
                        currentVersion = ConfigurationManager.AppSettings["Version"];

                        newVersion = model.AppSettings.FirstOrDefault(p => p.SettingID == "Version");
                        updatePath = model.AppSettings.FirstOrDefault(p => p.SettingID == "UpdatePath");
                    }

                    string StrUpdatePath = updatePath.SettingValue;

                    //// check current version with version store in database
                    if (newVersion != null && currentVersion != newVersion.SettingValue)
                    {
                        //string[] updatedFiles = Directory.GetFiles(updatePath.SettingValue);

                        //this.BeginInvoke((MethodInvoker)delegate ()
                        //{
                        //    progressBar1.Maximum = updatedFiles.Length;

                        //    this.Refresh();
                        //});

                        //int index = 0;
                        //while (index < updatedFiles.Length)
                        //{
                        //    FileInfo fileInfo = new FileInfo(updatedFiles[index++]);
                        //    File.Copy(fileInfo.FullName, Path.Combine(Application.StartupPath, fileInfo.Name), true);
                        //    this.BeginInvoke((MethodInvoker)delegate ()
                        //    {
                        //        progressBar1.PerformStep();

                        //        this.Refresh();
                        //    });
                        //}

                        foreach (var process in Process.GetProcessesByName("Bayer.WMS"))
                        {
                            process.Kill();
                        }

                        //Now Create all of the directories
                        foreach (string dirPath in Directory.GetDirectories(StrUpdatePath, "*",
                            SearchOption.AllDirectories))
                            Directory.CreateDirectory(dirPath.Replace(StrUpdatePath, Application.StartupPath));

                        //Copy all the files & Replaces any files with the same name
                        foreach (string newPath in Directory.GetFiles(StrUpdatePath, "*.*",
                            SearchOption.AllDirectories))
                            File.Copy(newPath, newPath.Replace(StrUpdatePath, Application.StartupPath), true);

                        Configuration versionCfg = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        versionCfg.AppSettings.Settings["Version"].Value = newVersion.SettingValue;
                        versionCfg.Save(ConfigurationSaveMode.Modified, true);

                        ConfigurationManager.RefreshSection("appSettings");
                        updated = true;

                        MessageBox.Show("Chương trình đã cập nhật phiên bản mới nhất " + newVersion.SettingValue + ". Xin cảm ơn !");
                    }
                    else
                    {
                        MessageBox.Show("Bạn đang sử dụng phiên bản mới nhất " + currentVersion + " của chương trình. Xin cảm ơn !");

                        //this.BeginInvoke((MethodInvoker)delegate ()
                        //{
                        //    progressBar1.Step = progressBar1.Maximum;
                        //    progressBar1.PerformStep();

                        //    this.Refresh();
                        //});
                    }
                }
                catch (Exception ex)
                {
                    using (var view = new ConnectionView())
                    {
                        if (DialogResult.OK != view.ShowDialog())
                        {
                            Application.Exit();
                            return;
                        }
                    }
                }

                if (updated)
                {
                    Process.Start(Path.Combine(Application.StartupPath, "Bayer.WMS.exe"));
                }
                Application.Exit();
            });
            thread.Start();
        }
    }
}
