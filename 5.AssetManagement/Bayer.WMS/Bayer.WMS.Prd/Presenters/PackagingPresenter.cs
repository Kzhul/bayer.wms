﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Bayer.WMS.Prd.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bayer.WMS.Prd.Presenters
{
    public interface IPackagingView : IBaseView
    {
        string Executor { set; }

        string StepHint { get; set; }

        string PackagingLine { get; }

        string PreStepHint { get; set; }

        int State { get; set; }

        User CurrentUser { get; set; }

        Device CurrentDevice { get; set; }

        Product CurrentProduct { get; set; }

        Pallet CurrentPallet { get; set; }

        CartonBarcode CurrentCartonBarcode { get; set; }

        ProductBarcode CurrentProductBarcode { get; set; }

        BarcodeBank CurrentBarcode { get; set; }

        IList<ProductBarcode> ProductBarcodes { get; set; }

        IList<CartonBarcode> CartonBarcodes { get; set; }

        IList<PackagingLog> PackagingLogs { get; set; }

        IList<PackagingSample> PackagingSamples { get; set; }

        ProductionPlan ProducionPlan { get; set; }

        PackagingViewModel PackagingVM { get; set; }

        void GetConfirmVoid(string message, int type);

        bool ProcessForceDestroyEnd(out int userID, out string note);

        void ShowErrorReasonBox(int type);

        void ShowQuantityCheck(DataTable dt);

        decimal Quantity { get; set; }

        int ProductLotPackagingType { get; set; }
    }

    public interface IPackagingPresenter : IBasePresenter
    {
        Task<string> ProcessBarcode(string barcode);

        int ConfirmType { set; }

        bool IsGetConfirm { set; }

        bool IsConfirm { set; }

        string Reason { set; }

        string Barcode { get; }


    }

    public class PackagingPresenter : BasePresenter, IPackagingPresenter
    {
        private readonly int _modePackaging = 0;
        private readonly int _modeDestroy = 1;
        private readonly int _modeTakeProductSample = 2;
        private readonly int _modeTakeCartonSample = 3;

        private readonly int _destroyProductUnPackagedConfirm = 1;
        private readonly int _destroyProductErrorReasonConfirm = 2;
        private readonly int _destroyCartonUnPackagedConfirm = 3;
        private readonly int _destroyCartonErrorReasonConfirm = 4;
        private readonly int _processEndConfirm = 5;

        private string _currentBarcode;
        private bool _isGetConfirm = false;
        private bool _isConfirm = false;
        private int _confirmType = -1;
        private string _reason = String.Empty;

        int _ProductLotPackagingType = 0;// = 1 QR Code, = 2 No QR Code, = 0 NOT SET

        private IPackagingView _mainView;
        private IPackagingLogRepository _packagingLogRepository;
        private IPackagingErrorRepository _packagingErrorRepository;
        private IPackagingSampleRepository _packagingSampleRepository;
        private IPackagingForceEndRepository _packagingForceEndRepository;
        private IProductionPlanRepository _productionPlanRepository;
        private IProductRepository _productRepository;
        private IProductPackingRepository _productPackingRepository;
        private ICartonBarcodeRepository _cartonBarcodeRepository;
        private IProductBarcodeRepository _productBarcodeRepository;
        private IPalletRepository _palletRepository;
        private IPalletStatusRepository _palletStatusRepository;
        private IDeviceRepository _deviceRepository;
        private IUserRepository _userRepository;
        private ISitemapRepository _sitemapRepository;
        private IBarcodeBankRepository _barcodeBankRepository;

        public PackagingPresenter(IPackagingView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IPackagingLogRepository packagingLogRepository,
            IPackagingErrorRepository packagingErrorRepository,
            IPackagingSampleRepository packagingSampleRepository,
            IPackagingForceEndRepository packagingForceEndRepository,
            IProductionPlanRepository productionPlanRepository,
            IProductRepository productRepository,
            IProductPackingRepository productPackingRepository,
            ICartonBarcodeRepository cartonBarcodeRepository,
            IProductBarcodeRepository productBarcodeRepository,
            IPalletRepository palletRepository,
            IPalletStatusRepository palletStatusRepository,
            IDeviceRepository deviceRepository,
            IUserRepository userRepository,
            ISitemapRepository sitemapRepository,
            IBarcodeBankRepository barcodeBankRepository)
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _packagingLogRepository = packagingLogRepository;
            _packagingErrorRepository = packagingErrorRepository;
            _packagingSampleRepository = packagingSampleRepository;
            _packagingForceEndRepository = packagingForceEndRepository;
            _productionPlanRepository = productionPlanRepository;
            _productRepository = productRepository;
            _productPackingRepository = productPackingRepository;
            _cartonBarcodeRepository = cartonBarcodeRepository;
            _productBarcodeRepository = productBarcodeRepository;
            _palletRepository = palletRepository;
            _palletStatusRepository = palletStatusRepository;
            _deviceRepository = deviceRepository;
            _userRepository = userRepository;
            _sitemapRepository = sitemapRepository;
            _barcodeBankRepository = barcodeBankRepository;
        }

        /// <summary>
        /// Validate validity of employee barcode
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private async Task<User> ValidateEmployeeBarcode(string barcode)
        {
            try
            {
                var user = await _userRepository.GetSingleAsync(p => p.Username == barcode && !p.IsDeleted);
                if (user == null)
                    throw new WrappedException(Messages.Error_EmployeeCodeNotExisted);

                return user;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Validate validity of product lot barcode
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private async Task<ProductionPlan> ValidateProductLotBarcode(string barcode)
        {
            try
            {
                //// check employee has been scanned yet?
                if (_mainView.CurrentUser == null)
                    throw new WrappedException(Messages.Error_ScanEmployeeCodeFirst);

                //// at moment, system is destroy mode
                if (_mainView.State == _modeDestroy)
                    throw new WrappedException(Messages.Error_DestroyLabelMode);

                //// at moment, system is take sample mode
                if (_mainView.State == _modeTakeProductSample || _mainView.State == _modeTakeCartonSample)
                    throw new WrappedException(Messages.Error_TakeSampleMode);

                //// don't do anything if scanned barcode is same current product lot
                if (_mainView.ProducionPlan != null && _mainView.ProducionPlan.ProductLot == barcode)
                    throw new WrappedException(Messages.Error_ScanDuplicateBarcode);

                //// check current production plan has been finished yet?
                if (_mainView.ProducionPlan != null && _mainView.ProducionPlan.Status != ProductionPlan.status.Completed
                    && (_mainView.PackagingSamples.Count > 0 || _mainView.PackagingLogs.Count > 0))
                    throw new WrappedException(Messages.Error_ProductLotNotFinishPackaging);

                var prodPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == barcode);
                if (prodPlan == null)
                {
                    _mainView.ProducionPlan = new ProductionPlan();
                    throw new WrappedException(Messages.Error_ProductLotNotExisted);
                }

                //TEST
                if (!Utility.debugMode)
                {
                    if (prodPlan.Device != _mainView.PackagingLine)
                        throw new WrappedException(Messages.Error_ProductLotPackagingIncorrectLine);
                }

                if (prodPlan.Status == ProductionPlan.status.Hold)
                    throw new WrappedException(Messages.Error_ProductLotIsHold);

                //if (!prodPlan.IsGenCartonBarcode && !prodPlan.IsGenProductBarcode)
                //    throw new WrappedException(Messages.Error_ProductLotNotYetGenBarcode);

                //// check production plan date
                //TEST
                if (!Utility.debugMode)
                {
                    if (prodPlan.PlanDate != DateTime.Today)
                        throw new WrappedException(Messages.Error_ProductionPlanDateIsInvalid);
                }

                return prodPlan;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Validate validity of product lot barcode
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private async Task<ProductionPlan> ValidateProductLotBarcodeNoQR(string barcode)
        {
            try
            {
                //// check employee has been scanned yet?
                if (_mainView.CurrentUser == null)
                    throw new WrappedException(Messages.Error_ScanEmployeeCodeFirst);

                //// at moment, system is destroy mode
                if (_mainView.State == _modeDestroy)
                    throw new WrappedException(Messages.Error_DestroyLabelMode);

                //// at moment, system is take sample mode
                if (_mainView.State == _modeTakeProductSample || _mainView.State == _modeTakeCartonSample)
                    throw new WrappedException(Messages.Error_TakeSampleMode);

                //// don't do anything if scanned barcode is same current product lot
                if (_mainView.ProducionPlan != null && _mainView.ProducionPlan.ProductLot == barcode)
                    throw new WrappedException(Messages.Error_ScanDuplicateBarcode);

                //// check current production plan has been finished yet?
                if (_mainView.ProducionPlan != null && _mainView.ProducionPlan.Status != ProductionPlan.status.Completed
                    && (_mainView.PackagingSamples.Count > 0 || _mainView.PackagingLogs.Count > 0))
                    throw new WrappedException(Messages.Error_ProductLotNotFinishPackaging);

                var prodPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == barcode);
                if (prodPlan == null)
                {
                    _mainView.ProducionPlan = new ProductionPlan();
                    throw new WrappedException(Messages.Error_ProductLotNotExisted);
                }

                //TEST
                if (!Utility.debugMode)
                {
                    if (prodPlan.Device != _mainView.PackagingLine)
                        throw new WrappedException(Messages.Error_ProductLotPackagingIncorrectLine);
                }

                if (prodPlan.Status == ProductionPlan.status.Hold)
                    throw new WrappedException(Messages.Error_ProductLotIsHold);

                //if (!prodPlan.IsGenCartonBarcode && !prodPlan.IsGenProductBarcode)
                //    throw new WrappedException(Messages.Error_ProductLotNotYetGenBarcode);

                //// check production plan date
                //TEST
                if (!Utility.debugMode)
                {
                    if (prodPlan.PlanDate != DateTime.Today)
                        throw new WrappedException(Messages.Error_ProductionPlanDateIsInvalid);
                }

                return prodPlan;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Validate validity of pallet barcode
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private async Task<Pallet> ValidatePalletBarcode(string barcode)
        {
            try
            {
                //// at moment, system is destroy mode
                if (_mainView.State == _modeDestroy)
                    throw new WrappedException(Messages.Error_DestroyLabelMode);

                //// at moment, system is take sample mode
                if (_mainView.State == _modeTakeProductSample || _mainView.State == _modeTakeCartonSample)
                    throw new WrappedException(Messages.Error_TakeSampleMode);

                //// don't do anything if scanned barcode is same current pallet barcode
                if (_mainView.CurrentPallet != null && _mainView.CurrentPallet.PalletCode == barcode)
                    throw new WrappedException(Messages.Error_ScanDuplicateBarcode);

                //// check product lot has been scanned yet?
                var prodPlan = _mainView.ProducionPlan;
                if (prodPlan == null)
                    throw new WrappedException(Messages.Error_ScanProductLotFirst);

                //// check pallet barcode exists in system
                var pallet = await _palletRepository.GetSingleAsync(p => p.PalletCode == barcode);
                if (pallet == null)
                    throw new WrappedException(Messages.Error_BarcodeIsNotExist);

                //// check pallet type with packaging room and capacity
                if (!_mainView.CurrentDevice.PalletType.Contains(pallet.Type))
                    throw new WrappedException(Messages.Error_PalletTypeIsInvalid);

                //// throw exception if packaging on this pallet hasn't yet finished
                if (_mainView.CurrentCartonBarcode != null)
                {
                    var packagingVM = _mainView.PackagingVM;
                    if (packagingVM.ActualPackingQtyInCarton > 0 && packagingVM.ActualPackingQtyInCarton != packagingVM.CartonPackingQty)
                        throw new WrappedException(Messages.Error_CartonPackagingHasNotYetFinished);
                }

                return pallet;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Validate validity of carton barcode
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private CartonBarcode ValidateCartonBarcode(string barcode)
        {
            try
            {
                //// at moment, system is take sample mode
                if (_mainView.State == _modeTakeProductSample)
                    throw new WrappedException(Messages.Error_TakeSampleMode);

                //// don't do anything if scanned barcode is same current carton barcode
                if (_mainView.CurrentCartonBarcode != null && _mainView.CurrentCartonBarcode.Barcode == barcode)
                    throw new WrappedException(Messages.Error_ScanDuplicateBarcode);

                //// check product lot has been scanned yet?
                var prodPlan = _mainView.ProducionPlan;
                if (prodPlan == null)
                    throw new WrappedException(Messages.Error_ScanProductLotFirst);

                //// check pallet barcode has been scanned yet?
                if (_mainView.State == _modePackaging && _mainView.CurrentPallet == null)
                    throw new WrappedException(Messages.Error_ScanPalletBarcodeFirst);

                //// check product packing type
                if (_mainView.CurrentProduct.PackingType != Product.packingType.Carton)
                    throw new WrappedException(Messages.Error_ProductPackingTypeIsPallet);

                //// check carton barcode exists in system
                var cartonBarcode = _mainView.CartonBarcodes.FirstOrDefault(p => p.Barcode == barcode); //await _cartonBarcodeRepository.GetSingleAsync(p => p.Barcode == barcode);
                if (cartonBarcode == null)
                    throw new WrappedException(Messages.Error_BarcodeIsNotExist);

                if (cartonBarcode == null || cartonBarcode.ProductLot != prodPlan.ProductLot)
                    throw new WrappedException(Messages.Error_CartonBarcodeNotExisted);

                //// check carton barcode is cancelled or destroyed
                if (cartonBarcode.Status == CartonBarcode.status.Cancelled || cartonBarcode.Status == CartonBarcode.status.Destroyed)
                    throw new WrappedException(Messages.Error_CartonBarcodeIsCancelledOrDestroyed);

                //// check carton barcode is cancelled or destroyed
                if (cartonBarcode.Status == CartonBarcode.status.TookSample)
                    throw new WrappedException(Messages.Error_CartonBarcodeIsTookSample);

                //// check product barcode is cancelled or destroyed
                if (_mainView.State == _modeDestroy && !String.IsNullOrWhiteSpace(cartonBarcode.PalletCode))
                    throw new WrappedException(Messages.Error_CartonBarcodeIsPackaging);


                //// throw exception if packaging on this carton hasn't yet finished
                if (_mainView.State == _modePackaging)
                {
                    var packagingVM = _mainView.PackagingVM;
                    if (_mainView.CurrentCartonBarcode != null && packagingVM.ActualPackingQtyInCarton != packagingVM.CartonPackingQty)
                        throw new WrappedException(Messages.Error_CartonPackagingHasNotYetFinished);
                }

                return cartonBarcode;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Validate validity of product barcode
        /// </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private ProductBarcode ValidateProductBarcode(string barcode)
        {
            try
            {
                //// check product lot has been scanned yet?
                var prodPlan = _mainView.ProducionPlan;
                if (prodPlan == null)
                    throw new WrappedException(Messages.Error_ScanProductLotFirst);

                //// only check status of production plan is packaged when screen mode is packaging or take sample
                if (prodPlan.Status != ProductionPlan.status.New && prodPlan.Status != ProductionPlan.status.InProgress
                    && (_mainView.State == _modePackaging || _mainView.State == _modeTakeProductSample || _mainView.State == _modeTakeCartonSample))
                    throw new WrappedException(Messages.Error_ProductLotIsPackaged);

                //// check product barcode is belong to production plan
                var productBarcode = _mainView.ProductBarcodes.FirstOrDefault(p => p.Barcode == barcode); //await _productBarcodeRepository.GetSingleAsync(p => p.Barcode == barcode);

                //// check product barcode is belong to production plan
                if (productBarcode == null || productBarcode.ProductLot != prodPlan.ProductLot)
                    throw new WrappedException(Messages.Error_ProductBarcodeNotExisted);

                //// check product barcode is cancelled or destroyed
                if (productBarcode.Status == ProductBarcode.status.Cancelled
                    || productBarcode.Status == ProductBarcode.status.Destroyed)
                    throw new WrappedException(Messages.Error_ProductBarcodeIsCancelledOrDestroyed);

                //// check product barcode is cancelled or destroyed
                if (productBarcode.Status == ProductBarcode.status.TookSample)
                    throw new WrappedException(Messages.Error_ProductBarcodeIsTookSample);

                //// only check status of product barcode is packaged when screen mode is packaging or take sample or destroy after packaged completely
                if (_mainView.State == _modePackaging || _mainView.State == _modeTakeProductSample
                    || (_mainView.State == _modeDestroy && prodPlan.Status == ProductionPlan.status.Destroying))
                {
                    //// check product barcode is packed or not
                    if (productBarcode.Status == ProductBarcode.status.Packaged)
                        throw new WrappedException(Messages.Error_ProductBarcodeIsPacked);
                }

                if (_mainView.State == _modeTakeProductSample && prodPlan.TookProductSampleQty == prodPlan.ProductSampleQty)
                    throw new WrappedException(Messages.Error_ProductLotTakeEnoughSample);

                return productBarcode;
            }
            catch
            {
                throw;
            }
        }

        private async Task<BarcodeBank> ValidateProductBarcode2(string barcode)
        {
            try
            {
                //// check product lot has been scanned yet?
                var prodPlan = _mainView.ProducionPlan;
                if (prodPlan == null)
                    throw new WrappedException(Messages.Error_ScanProductLotFirst);

                //// only check status of production plan is packaged when screen mode is packaging or take sample
                if (prodPlan.Status != ProductionPlan.status.New && prodPlan.Status != ProductionPlan.status.InProgress
                    && (_mainView.State == _modePackaging || _mainView.State == _modeTakeProductSample || _mainView.State == _modeTakeCartonSample))
                    throw new WrappedException(Messages.Error_ProductLotIsPackaged);

                //// check product barcode is exist
                var barcodeBank = await _barcodeBankRepository.GetSingleAsync(p => p.Barcode == barcode); //await _productBarcodeRepository.GetSingleAsync(p => p.Barcode == barcode);
                if (barcodeBank == null)
                    throw new WrappedException(Messages.Error_BarcodeIsNotExist);

                ////// check product barcode is cancelled or destroyed
                //if (barcodeBank.Status == ProductBarcode.status.Cancelled
                //    || barcodeBank.Status == ProductBarcode.status.Destroyed)
                //    throw new WrappedException(Messages.Error_ProductBarcodeIsCancelledOrDestroyed);

                ////// check product barcode is cancelled or destroyed
                //if (barcodeBank.Status == ProductBarcode.status.TookSample)
                //    throw new WrappedException(Messages.Error_ProductBarcodeIsTookSample);

                ////// only check status of product barcode is packaged when screen mode is packaging or take sample or destroy after packaged completely
                //if (_mainView.State == _modePackaging || _mainView.State == _modeTakeProductSample
                //    || (_mainView.State == _modeDestroy && prodPlan.Status == ProductionPlan.status.Destroying))
                //{
                //    //// check product barcode is packed or not
                //    if (barcodeBank.Status == ProductBarcode.status.Packaged)
                //        throw new WrappedException(Messages.Error_ProductBarcodeIsPacked);
                //}

                if (_mainView.State == _modeTakeProductSample && prodPlan.TookProductSampleQty == prodPlan.ProductSampleQty)
                    throw new WrappedException(Messages.Error_ProductLotTakeEnoughSample);

                return barcodeBank;
            }
            catch
            {
                throw;
            }
        }

        private bool IsPackagingComplete()
        {
            var packagingVM = _mainView.PackagingVM;
            return packagingVM.ActualPackingQty == packagingVM.PlanPackingQty;
        }

        private async Task<bool> IsExistUnuseLabel(string productLot)
        {
            try
            {
                var productBarcode = await _productBarcodeRepository.GetSingleAsync(p => p.ProductLot == productLot && p.Status == ProductBarcode.status.New);
                var cartonBarcode = await _cartonBarcodeRepository.GetSingleAsync(p => p.ProductLot == productLot && p.Status == ProductBarcode.status.New && p.PalletCode == null);

                //// Auto change  to destroy mode if produc lot still exist label
                return productBarcode != null || cartonBarcode != null;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessEmployeeBarcode(string barcode)
        {
            try
            {
                var user = await ValidateEmployeeBarcode(barcode);

                _mainView.Executor = $"{(String.IsNullOrWhiteSpace(user.LastName) ? String.Empty : (user.LastName + " "))}{user.FirstName}";
                _mainView.CurrentUser = user;

                //// set hint for next step
                _mainView.StepHint = Messages.Information_Packaging_ScanProdPlanStep;

                RecordAuditTrail(_mainView.CurrentUser == null ? 0 : _mainView.CurrentUser.UserID, AuditTrail.action.ScanEmployee, MethodBase.GetCurrentMethod(), barcode);
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser == null ? 0 : _mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{barcode} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser == null ? 0 : _mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{barcode} - {ex.Message}");
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessProductLotBarcode(string barcode)
        {
            try
            {
                //// check the validity of barcode
                var prodPlan = await ValidateProductLotBarcode(barcode);

                //// Get packaging logs
                var taskGetPackagingLogs = _packagingLogRepository.GetAsync(p => p.ProductLot == prodPlan.ProductLot);

                //// Get packaging samples
                var taskGetPackagingSamples = _packagingSampleRepository.GetAsync(p => p.ProductLot == prodPlan.ProductLot);

                //// Get destroyed or cancelled label
                var taskGetDestroyProductBarcodes = _productBarcodeRepository.GetAsync(p => p.ProductLot == prodPlan.ProductLot && (p.Status == ProductBarcode.status.Cancelled || p.Status == ProductBarcode.status.Destroyed));
                var taskGetDestroyCartonBarcodes = _cartonBarcodeRepository.GetAsync(p => p.ProductLot == prodPlan.ProductLot && (p.Status == CartonBarcode.status.Cancelled || p.Status == CartonBarcode.status.Destroyed));

                //// Get device or packaging room of production plan
                var taskGetDevice = _deviceRepository.GetSingleAsync(p => p.DeviceCode == prodPlan.Device);

                //// Get product information of production plan
                var taskGetProduct = _productRepository.GetSingleAsync(p => p.ProductID == prodPlan.ProductID.Value);

                //// Get packing size of productin production plan
                var taskGetProdPackings = _productPackingRepository.GetAsync(p => p.ProductID == prodPlan.ProductID.Value);

                //// Load information of production plan
                await Task.WhenAll(taskGetPackagingLogs, taskGetPackagingSamples, taskGetDestroyProductBarcodes, taskGetDestroyCartonBarcodes,
                    taskGetDevice, taskGetProduct, taskGetProdPackings);

                //// Get product barcodes of this lot
                var taskGetProdBarcodes = _productBarcodeRepository.GetAsync(a => a.ProductLot == prodPlan.ProductLot);

                //// Get carton barcodes of this lot
                var taskGetCartonBarcodes = _cartonBarcodeRepository.GetAsync(a => a.ProductLot == prodPlan.ProductLot);

                await Task.WhenAll(taskGetProdBarcodes, taskGetCartonBarcodes);

                //// Get some extra information to help employee
                _mainView.CurrentDevice = taskGetDevice.Result;
                _mainView.CurrentProduct = taskGetProduct.Result;
                _mainView.PackagingLogs = taskGetPackagingLogs.Result;
                _mainView.PackagingSamples = taskGetPackagingSamples.Result;
                _mainView.ProductBarcodes = taskGetProdBarcodes.Result;
                _mainView.CartonBarcodes = taskGetCartonBarcodes.Result;
                //// reset all
                _mainView.CurrentPallet = null;
                _mainView.CurrentCartonBarcode = null;
                _mainView.CurrentProductBarcode = null;
                _mainView.CurrentBarcode = null;

                var destroyProductBarcodes = taskGetDestroyProductBarcodes.Result;
                var destroyCartonBarcodes = taskGetDestroyCartonBarcodes.Result;
                var prodPackings = taskGetProdPackings.Result;
                var cartonPacking = prodPackings.FirstOrDefault(p => p.Type == ProductPacking.type.Carton) ?? new ProductPacking();
                var palletPacking = prodPackings.FirstOrDefault(p => p.Type == ProductPacking.type.Pallet) ?? new ProductPacking();

                //// Display production plan information on screen
                prodPlan.ProductCode = _mainView.CurrentProduct.ProductCode;
                prodPlan.ProductDescription = _mainView.CurrentProduct.Description;
                prodPlan.ProductPackingType = _mainView.CurrentProduct.PackingType;

                if (_mainView.ProductLotPackagingType == 1)
                {
                    prodPlan.ActualPackingQty = _mainView.PackagingLogs.Count;
                }
                else
                {
                    prodPlan.ActualPackingQty = _mainView.PackagingLogs.Sum(a => a.Qty);
                }
                prodPlan.CartonPackingQty = cartonPacking.Quantity;
                prodPlan.PalletPackingQty = palletPacking.Quantity;
                prodPlan.CartonWeight = cartonPacking.Weight;
                prodPlan.ProductSampleQty = _mainView.CurrentProduct.SampleQty ?? 0;
                prodPlan.TookProductSampleQty = _mainView.PackagingSamples.Count(p => p.Type == PackagingSample.type.Product);
                prodPlan.TookCartonSampleQty = _mainView.CurrentProduct.PackingType == Product.packingType.Carton ?
                    _mainView.PackagingSamples.Count(p => p.Type == PackagingSample.type.Carton) : -1;
                if (_mainView.PackagingLogs.Count == 0 && _mainView.PackagingSamples.Count == 0)
                    prodPlan.PackagingStartTime = DateTime.Now;

                var packagingVM = new PackagingViewModel(prodPlan.ProductLot, _mainView.CurrentProduct.ProductCode, _mainView.CurrentProduct.Description,
                    prodPlan.PackageQuantity, cartonPacking.Quantity, palletPacking.Quantity, prodPlan.ActualPackingQty,
                    destroyProductBarcodes.Count(p => p.Status == ProductBarcode.status.Cancelled) + destroyCartonBarcodes.Count(p => p.Status == CartonBarcode.status.Cancelled),
                    destroyProductBarcodes.Count(p => p.Status == ProductBarcode.status.Destroyed) + destroyCartonBarcodes.Count(p => p.Status == CartonBarcode.status.Destroyed),
                    prodPlan.UOM, prodPlan.UOM
                    );



                _mainView.ProducionPlan = prodPlan;
                _mainView.PackagingVM = packagingVM;

                //// process take sample if not enough
                if (prodPlan.TookProductSampleQty < prodPlan.ProductSampleQty)
                    ProcessTakeProductSampleBegin();
                else if (prodPlan.TookCartonSampleQty < 1 && _mainView.CurrentProduct.PackingType == Product.packingType.Carton)
                    ProcessTakeCartonSampleBegin();
                else
                    _mainView.StepHint = Messages.Information_Packaging_ScanPalletStep;

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanProductLot, MethodBase.GetCurrentMethod(), barcode);
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{barcode} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{barcode} - {ex.Message}");
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessPalletBarcode(string barcode)
        {
            try
            {
                //// check the validity of barcode
                var pallet = await ValidatePalletBarcode(barcode);

                DataTable dtAnotherProductLot = await _palletRepository.ExecuteDataTable("proc_Pallets_CheckAnotherProductLot",
                   new SqlParameter { ParameterName = "@PalletCode", SqlDbType = SqlDbType.VarChar, Value = pallet.PalletCode },
                   new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = _mainView.ProducionPlan.ProductLot });

                if (dtAnotherProductLot.Rows.Count > 0)
                {
                    string mes = "Pallet này đang chứa hàng khác: " + dtAnotherProductLot.Rows[0]["ProductName"].ToString() + " - Số lô: " + dtAnotherProductLot.Rows[0]["ProductLot"].ToString() + ". Vui lòng sử dụng pallet khác.";
                    throw new WrappedException(mes);
                }

                var packagingVM = _mainView.PackagingVM;
                packagingVM.PalletBarcode = pallet.PalletCode;

                if (_mainView.ProductLotPackagingType == 1)
                {
                    packagingVM.ActualPackingQtyOnPallet = _mainView.PackagingLogs.Count(p => p.PalletBarcode == pallet.PalletCode);
                }
                else
                {
                    decimal? temp = _mainView.PackagingLogs.Where(p => p.PalletBarcode == pallet.PalletCode).ToList().Sum(a => a.Qty);
                    packagingVM.ActualPackingQtyOnPallet = temp.HasValue ? temp.Value : 0;
                }

                _mainView.PackagingVM = packagingVM;
                _mainView.CurrentPallet = pallet;

                //Check pallet is contain another product lot
                //await _palletRepository.ExecuteNonQuery("proc_Pallets_Clear",
                //    new SqlParameter { ParameterName = "@PalletCode", SqlDbType = SqlDbType.VarChar, Value = pallet.PalletCode },
                //    new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = String.Empty },
                //    new SqlParameter { ParameterName = "@NotProductLot", SqlDbType = SqlDbType.VarChar, Value = _mainView.ProducionPlan.ProductLot });

                //// set hint for next step
                if (_mainView.ProductLotPackagingType == 1)
                {
                    _mainView.StepHint = _mainView.CurrentProduct.PackingType == Product.packingType.Carton ? Messages.Information_Packaging_ScanCartonStep : Messages.Information_Packaging_ScanProductStep;
                }
                else
                {
                    _mainView.StepHint = Messages.Information_Packaging_ScanQuantityForNoQRCode;
                }

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanPallet, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode}");
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessCartonBarcode(string barcode)
        {
            try
            {
                //// check the validity of barcode
                var cartonBarcode = ValidateCartonBarcode(barcode);

                if (_mainView.State == _modeDestroy)
                    await ProcessDestroyCartonLabel(cartonBarcode);
                else if (_mainView.State == _modeTakeCartonSample)
                    await ProcessTakeCartonSample(cartonBarcode);
                else
                {
                    //// throw exception if packaging on this carton hasn't yet finished
                    var packagingVM = _mainView.PackagingVM;

                    decimal packageCartonQty = Math.Floor(packagingVM.PalletPackingQty / packagingVM.CartonPackingQty);
                    var cartonList = _mainView.PackagingLogs.Where(p => p.PalletBarcode == _mainView.CurrentPallet.PalletCode).Select(p => p.CartonBarcode).ToList();
                    cartonList.Add(cartonBarcode.Barcode);
                    int actualCartonQty = cartonList.Distinct().Count();

                    Utility.LogEx("actualCartonQty:" + actualCartonQty.ToString(), "ProcessCartonBarcode");
                    Utility.LogEx("packageCartonQty:" + packageCartonQty.ToString(), "ProcessCartonBarcode");

                    if (actualCartonQty > packageCartonQty)
                        throw new WrappedException(Messages.Error_CartonOnPalletOverSpec);

                    packagingVM.CartonBarcode = cartonBarcode.Barcode;
                    packagingVM.ActualPackingQtyInCarton = _mainView.PackagingLogs.Count(p => p.CartonBarcode == cartonBarcode.Barcode);

                    _mainView.PackagingVM = packagingVM;
                    _mainView.CurrentCartonBarcode = cartonBarcode;

                    //// set hint for next step
                    _mainView.StepHint = Messages.Information_Packaging_ScanProductStep;

                    RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanCarton, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode}");
                }
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessProductBarcode(string barcode)
        {
            try
            {
                //// check the validity of barcode then set status of product barcode
                var productBarcode = ValidateProductBarcode(barcode);



                //// at moment, system is destroy mode
                if (_mainView.State == _modeDestroy)
                    await ProcessDestroyProductLabel(productBarcode);
                else if (_mainView.State == _modeTakeProductSample)
                    await ProcessTakeProductSample(productBarcode);
                else
                {
                    //// don't do anything if scanned barcode is same current carton barcode
                    if (_mainView.CurrentProductBarcode != null && _mainView.CurrentProductBarcode.Barcode == barcode)
                        throw new WrappedException(Messages.Error_ScanDuplicateBarcode);

                    //// check pallet barcode has been scanned yet?
                    if (_mainView.CurrentPallet == null)
                        throw new WrappedException(Messages.Error_ScanPalletBarcodeFirst);

                    //// check carton barcode has been scanned yet?
                    if (_mainView.CurrentProduct.PackingType == Product.packingType.Carton && _mainView.CurrentCartonBarcode == null)
                        throw new WrappedException(Messages.Error_ScanCartonBarcodeFirst);

                    //// throw exception if packaging on this carton hasn't yet finished
                    var packagingVM = _mainView.PackagingVM;

                    if (_mainView.CurrentProduct.PackingType == Product.packingType.Carton && packagingVM.ActualPackingQtyInCarton + 1 > packagingVM.CartonPackingQty)
                        throw new WrappedException(Messages.Error_CartonPackagingHadFinished);

                    //// throw exception if packaging on this pallet hasn't yet finished
                    if (packagingVM.ActualPackingQtyOnPallet + 1 > packagingVM.PalletPackingQty)
                        throw new WrappedException(Messages.Error_PalletPackagingHadFinished);

                    var prodPlan = _mainView.ProducionPlan;

                    //// save packaging log and update information of production plan
                    var packagingLog = new PackagingLog(productBarcode.Barcode,
                        productBarcode.EncryptedBarcode,
                        productBarcode.ProductLot,
                        productBarcode.ProductID,
                        DateTime.Now,
                        _mainView.CurrentProduct.PackingType == Product.packingType.Carton ? _mainView.CurrentCartonBarcode.Barcode : String.Empty,
                        _mainView.CurrentPallet.PalletCode,
                        _mainView.CurrentUser.UserID,
                        prodPlan.Device);

                    if (await SavePackagingLog(packagingLog, prodPlan))
                    {
                        //// increase qty in carton and pallet
                        packagingVM.ActualPackingQty += 1;
                        if (_mainView.CurrentProduct.PackingType == Product.packingType.Carton)
                        {
                            try
                            {
                                packagingVM.ActualPackingQtyInCarton = Utility.DecimalParse(await _palletRepository.ExecuteScalar("proc_Packaging_CountQuantityInCarton",
                           new SqlParameter { ParameterName = "@CartonBarcode", SqlDbType = SqlDbType.VarChar, Value = _mainView.CurrentCartonBarcode.Barcode }));
                            }
                            catch
                            {
                                //Do Nothing here
                            }
                        }

                        try
                        {
                            packagingVM.ActualPackingQtyOnPallet = Utility.DecimalParse(await _palletRepository.ExecuteScalar("proc_Packaging_CountQuantityInPallet",
                       new SqlParameter { ParameterName = "@PalletBarcode", SqlDbType = SqlDbType.VarChar, Value = _mainView.CurrentPallet.PalletCode }));
                        }
                        catch
                        {
                            //Do Nothing here
                        }

                        //// update information of production plan on screen
                        prodPlan.Status = ProductionPlan.status.InProgress;
                        prodPlan.ActualPackingQty += 1;

                        //// set status for carton
                        if (_mainView.CurrentCartonBarcode != null)
                        {
                            _mainView.CurrentCartonBarcode.PalletCode = packagingLog.PalletBarcode;
                            _mainView.CurrentCartonBarcode.Status = packagingVM.ActualPackingQtyInCarton == packagingVM.CartonPackingQty ? CartonBarcode.status.Packaged : CartonBarcode.status.New;
                        }

                        //// set status for product barcode
                        productBarcode.PalletCode = packagingLog.PalletBarcode;
                        productBarcode.CartonBarcode = packagingLog.CartonBarcode;
                        productBarcode.Status = ProductBarcode.status.Packaged;

                        //// reload packaging logs
                        _mainView.PackagingLogs.Add(packagingLog);

                        _mainView.ProducionPlan = prodPlan;
                        _mainView.PackagingVM = packagingVM;
                        _mainView.CurrentProductBarcode = productBarcode;

                        //// check packaging progress with plan
                        if (IsPackagingComplete())
                        {
                            _currentBarcode = "ket_thuc";
                            _mainView.GetConfirmVoid(Messages.Question_ContinuePackaging, _processEndConfirm);
                            return;
                        }
                        else if (_mainView.CurrentPallet != null && packagingVM.ActualPackingQtyOnPallet == packagingVM.PalletPackingQty)
                            _mainView.StepHint = Messages.Information_Packaging_ScanPalletStep;
                        else if (_mainView.CurrentCartonBarcode != null && packagingVM.ActualPackingQtyInCarton == packagingVM.CartonPackingQty)
                            _mainView.StepHint = Messages.Information_Packaging_ScanCartonStep;

                        RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanProduct, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode}");
                    }
                }
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                //throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessProductBarcode2(string barcode)
        {
            try
            {
                //// check the validity of barcode then set status of product barcode
                //var barcodeBank = await ValidateProductBarcode2(barcode);

                //// at moment, system is destroy mode
                if (_mainView.State == _modeDestroy)
                {
                    await ProcessDestroyProductLabel2(barcode);
                }
                else if (_mainView.State == _modeTakeProductSample)
                {
                    var barcodeBank = await ValidateProductBarcode2(barcode);
                    await ProcessTakeProductSample2(barcodeBank);
                }
                else
                {
                    var barcodeBank = await ValidateProductBarcode2(barcode);

                    //// don't do anything if scanned barcode is same current carton barcode
                    if (_mainView.CurrentBarcode != null && _mainView.CurrentBarcode.Barcode == barcode)
                        throw new WrappedException(Messages.Error_ScanDuplicateBarcode);

                    //// check pallet barcode has been scanned yet?
                    if (_mainView.CurrentPallet == null)
                        throw new WrappedException(Messages.Error_ScanPalletBarcodeFirst);

                    //// check carton barcode has been scanned yet?
                    if (_mainView.CurrentProduct.PackingType == Product.packingType.Carton && _mainView.CurrentCartonBarcode == null)
                        throw new WrappedException(Messages.Error_ScanCartonBarcodeFirst);

                    //// throw exception if packaging on this carton hasn't yet finished
                    var packagingVM = _mainView.PackagingVM;
                    if (_mainView.CurrentProduct.PackingType == Product.packingType.Carton && packagingVM.ActualPackingQtyInCarton + 1 > packagingVM.CartonPackingQty)
                        throw new WrappedException(Messages.Error_CartonPackagingHadFinished);

                    //// throw exception if packaging on this pallet hasn't yet finished
                    if (packagingVM.ActualPackingQtyOnPallet + 1 > packagingVM.PalletPackingQty)
                        throw new WrappedException(Messages.Error_PalletPackagingHadFinished);

                    var prodPlan = _mainView.ProducionPlan;

                    //// save packaging log and update information of production plan
                    var packagingLog = new PackagingLog(barcodeBank.Barcode,
                        Utility.AESEncrypt(barcodeBank.Barcode),
                        prodPlan.ProductLot,
                        prodPlan.ProductID,
                        DateTime.Now,
                        _mainView.CurrentProduct.PackingType == Product.packingType.Carton ? _mainView.CurrentCartonBarcode.Barcode : String.Empty,
                        _mainView.CurrentPallet.PalletCode,
                        _mainView.CurrentUser.UserID,
                        prodPlan.Device);

                    if (await SavePackagingLog2(packagingLog, prodPlan))
                    {


                        //// increase qty in carton and pallet
                        packagingVM.ActualPackingQty += 1;
                        if (_mainView.CurrentProduct.PackingType == Product.packingType.Carton)
                        {
                            try
                            {
                                packagingVM.ActualPackingQtyInCarton = Utility.DecimalParse(await _palletRepository.ExecuteScalar("proc_Packaging_CountQuantityInCarton",
                           new SqlParameter { ParameterName = "@CartonBarcode", SqlDbType = SqlDbType.VarChar, Value = _mainView.CurrentCartonBarcode.Barcode }));
                            }
                            catch
                            {
                                //Do Nothing here
                            }
                        }

                        try
                        {
                            packagingVM.ActualPackingQtyOnPallet = Utility.DecimalParse(await _palletRepository.ExecuteScalar("proc_Packaging_CountQuantityInPallet",
                       new SqlParameter { ParameterName = "@PalletBarcode", SqlDbType = SqlDbType.VarChar, Value = _mainView.CurrentPallet.PalletCode }));
                        }
                        catch
                        {
                            //Do Nothing here
                        }

                        //// update information of production plan on screen
                        prodPlan.Status = ProductionPlan.status.InProgress;
                        prodPlan.ActualPackingQty += 1;

                        //// set status for carton
                        if (_mainView.CurrentCartonBarcode != null)
                        {
                            _mainView.CurrentCartonBarcode.PalletCode = packagingLog.PalletBarcode;
                            _mainView.CurrentCartonBarcode.Status = packagingVM.ActualPackingQtyInCarton == packagingVM.CartonPackingQty ? CartonBarcode.status.Packaged : CartonBarcode.status.New;
                        }

                        ////// set status for product barcode
                        //productBarcode.PalletCode = packagingLog.PalletBarcode;
                        //productBarcode.CartonBarcode = packagingLog.CartonBarcode;
                        //productBarcode.Status = ProductBarcode.status.Packaged;

                        //// reload packaging logs
                        _mainView.PackagingLogs.Add(packagingLog);

                        _mainView.ProducionPlan = prodPlan;
                        _mainView.PackagingVM = packagingVM;
                        _mainView.CurrentBarcode = barcodeBank;

                        //// check packaging progress with plan
                        if (IsPackagingComplete())
                        {
                            _currentBarcode = "ket_thuc";
                            _mainView.GetConfirmVoid(Messages.Question_ContinuePackaging, _processEndConfirm);
                            return;
                        }
                        else if (_mainView.CurrentPallet != null && packagingVM.ActualPackingQtyOnPallet == packagingVM.PalletPackingQty)
                            _mainView.StepHint = Messages.Information_Packaging_ScanPalletStep;
                        else if (_mainView.CurrentCartonBarcode != null && packagingVM.ActualPackingQtyInCarton == packagingVM.CartonPackingQty)
                            _mainView.StepHint = Messages.Information_Packaging_ScanCartonStep;

                        RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanProduct, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode}");
                    }
                }
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                //throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// process error when problem occur
        /// </summary>
        /// <param name="packagingLog"></param>
        /// <param name="note"></param>
        /// <returns></returns>
        private async Task ProcessError(ProductBarcode productBarcode, PackagingLog packagingLog, int? userID, string note)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                var packagingError = new PackagingError(productBarcode.Barcode,
                    productBarcode.EncryptedBarcode,
                    PackagingError.type.Product,
                    productBarcode.ProductLot,
                    productBarcode.ProductID,
                    packagingLog == null ? null : packagingLog.Date,
                    DateTime.Now, userID, prodPlan.Device, note);

                if (packagingLog != null)
                {
                    await SavePackagingError(packagingError, packagingLog);

                    prodPlan.ActualPackingQty -= 1;

                    //// decrease qty in carton and pallet
                    var packagingVM = _mainView.PackagingVM;
                    packagingVM.ActualPackingQty -= 1;

                    if (_mainView.CurrentCartonBarcode != null && packagingLog.CartonBarcode == _mainView.CurrentCartonBarcode.Barcode)
                        packagingVM.ActualPackingQtyInCarton -= 1;
                    if (_mainView.CurrentPallet != null && packagingLog.PalletBarcode == _mainView.CurrentPallet.PalletCode)
                        packagingVM.ActualPackingQtyOnPallet -= 1;

                    _mainView.ProducionPlan = prodPlan;
                    _mainView.PackagingVM = packagingVM;

                    //// reload packaging logs
                    _mainView.PackagingLogs.Remove(packagingLog);
                }
                else
                    await SaveUnPackagingError(packagingError);
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessError2(string barcode, PackagingLog packagingLog, int? userID, string note)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                var packagingError = new PackagingError(barcode,
                    Utility.AESEncrypt(barcode),
                    PackagingError.type.Product,
                    prodPlan.ProductLot,
                    prodPlan.ProductID,
                    packagingLog == null ? null : packagingLog.Date,
                    DateTime.Now, userID, prodPlan.Device, note);

                if (packagingLog != null)
                {
                    await SavePackagingError2(packagingError, packagingLog);

                    prodPlan.ActualPackingQty -= 1;

                    //// decrease qty in carton and pallet
                    var packagingVM = _mainView.PackagingVM;
                    packagingVM.ActualPackingQty -= 1;

                    if (_mainView.CurrentCartonBarcode != null && packagingLog.CartonBarcode == _mainView.CurrentCartonBarcode.Barcode)
                        packagingVM.ActualPackingQtyInCarton -= 1;
                    if (_mainView.CurrentPallet != null && packagingLog.PalletBarcode == _mainView.CurrentPallet.PalletCode)
                        packagingVM.ActualPackingQtyOnPallet -= 1;

                    _mainView.ProducionPlan = prodPlan;
                    _mainView.PackagingVM = packagingVM;

                    //// reload packaging logs
                    _mainView.PackagingLogs.Remove(packagingLog);
                }
                else
                    await SaveUnPackagingError2(packagingError);
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessError(CartonBarcode cartonBarcode, int? userID, string note)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                var packagingError = new PackagingError(cartonBarcode.Barcode,
                    cartonBarcode.Barcode,
                    PackagingError.type.Carton,
                    cartonBarcode.ProductLot,
                    null, null, DateTime.Now, userID, prodPlan.Device, note);

                await SaveUnPackagingError(packagingError);
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessDestroyProductLabel(ProductBarcode productBarcode)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                var packagingVM = _mainView.PackagingVM;

                //// find packaging log by barcode
                var packagingLog = _mainView.PackagingLogs.FirstOrDefault(p => p.Barcode == productBarcode.Barcode);
                if (!_isGetConfirm && packagingLog == null && prodPlan.Status == ProductionPlan.status.InProgress)
                {
                    _currentBarcode = productBarcode.Barcode;
                    _mainView.GetConfirmVoid(Messages.Question_DestroyUnPackaged, _destroyProductUnPackagedConfirm);
                    return;
                }

                if (_isGetConfirm && _confirmType == _destroyProductUnPackagedConfirm && !_isConfirm)
                {
                    //// if user don't want to destroy unpackaged label
                    //// force stop destroy mode to prevent mistake of user
                    await ProcessDestroyEnd();
                    return;
                }

                if (_confirmType != _destroyProductErrorReasonConfirm)
                    _isGetConfirm = false;

                if (!_isGetConfirm)
                {
                    _currentBarcode = productBarcode.Barcode;
                    _mainView.ShowErrorReasonBox(_destroyProductErrorReasonConfirm);
                    return;
                }

                if (_isGetConfirm && _confirmType == _destroyProductErrorReasonConfirm && !_isConfirm)
                    return;

                _confirmType = 0;
                _isGetConfirm = false;
                _isConfirm = false;

                //// destroy unused label if product lot packaged completely
                //// other way, cancel label
                string status = productBarcode.Status;
                productBarcode.Status = prodPlan.Status == ProductionPlan.status.Packaged || prodPlan.Status == ProductionPlan.status.Destroying ? ProductBarcode.status.Destroyed : ProductBarcode.status.Cancelled;

                //// if destroy product barcode, only update status of product barcode is destroyed
                if (prodPlan.Status == ProductionPlan.status.Packaged || prodPlan.Status == ProductionPlan.status.Destroying)
                {
                    prodPlan.Status = ProductionPlan.status.Destroying;
                    productBarcode.CartonBarcode = null;
                    productBarcode.PalletCode = null;
                    productBarcode.Status = ProductBarcode.status.Destroyed;

                    using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                    {
                        _productionPlanRepository = unitOfWork.Register(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                        _productBarcodeRepository = unitOfWork.Register(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;
                        _packagingLogRepository = unitOfWork.Register(_packagingLogRepository.GetType()) as IPackagingLogRepository;
                        _palletStatusRepository = unitOfWork.Register(_palletStatusRepository.GetType()) as IPalletStatusRepository;

                        await _productionPlanRepository.Update(prodPlan, new string[] { "Status" }, new object[] { prodPlan.ProductLot });
                        await _productBarcodeRepository.Update(productBarcode, new string[] { "CartonBarcode", "PalletCode", "Status" }, new object[] { productBarcode.Barcode });

                        if (status == ProductionPlan.status.Packaged)
                        {
                            await _packagingLogRepository.Delete(new object[] { productBarcode.Barcode });
                            await _palletStatusRepository.Delete(new object[] { productBarcode.Barcode });
                        }

                        await unitOfWork.Commit2();
                    }

                    packagingVM.DestroyLabel += 1;

                    //if (!await IsExistUnuseLabel(prodPlan.ProductLot))
                    //{
                    //    await CompletePackaging();
                    //}
                }
                //// else, if cancel while packaging, follow process error way
                else
                {
                    await ProcessError(productBarcode, packagingLog, _mainView.CurrentUser.UserID, _reason);
                    packagingVM.CancelLabel += 1;
                }

                _mainView.PackagingVM = packagingVM;
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanProduct, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {productBarcode.Barcode} - {_reason}");
            }
            catch
            {
                throw;
            }
        }

        private async Task ProcessDestroyProductLabel2(string barcode)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                var packagingVM = _mainView.PackagingVM;

                //// find packaging log by barcode
                var packagingLog = _mainView.PackagingLogs.FirstOrDefault(p => p.Barcode == barcode);
                if (!_isGetConfirm && packagingLog == null && prodPlan.Status == ProductionPlan.status.InProgress)
                {
                    _currentBarcode = barcode;
                    _mainView.GetConfirmVoid(Messages.Question_DestroyUnPackaged, _destroyProductUnPackagedConfirm);
                    return;
                }

                if (_isGetConfirm && _confirmType == _destroyProductUnPackagedConfirm && !_isConfirm)
                {
                    //// if user don't want to destroy unpackaged label
                    //// force stop destroy mode to prevent mistake of user
                    await ProcessDestroyEnd();
                    return;
                }

                if (_confirmType != _destroyProductErrorReasonConfirm)
                    _isGetConfirm = false;

                if (!_isGetConfirm)
                {
                    _currentBarcode = barcode;
                    _mainView.ShowErrorReasonBox(_destroyProductErrorReasonConfirm);
                    return;
                }

                if (_isGetConfirm && _confirmType == _destroyProductErrorReasonConfirm && !_isConfirm)
                    return;

                _confirmType = 0;
                _isGetConfirm = false;
                _isConfirm = false;

                //// destroy unused label if product lot packaged completely
                //// other way, cancel label
                string status = prodPlan.Status == ProductionPlan.status.Packaged || prodPlan.Status == ProductionPlan.status.Destroying ? ProductBarcode.status.Destroyed : ProductBarcode.status.Cancelled;

                //// if destroy product barcode, only update status of product barcode is destroyed
                if (prodPlan.Status == ProductionPlan.status.Packaged || prodPlan.Status == ProductionPlan.status.Destroying)
                {
                    prodPlan.Status = ProductionPlan.status.Destroying;
                    var productBarcode = new ProductBarcode
                    {
                        Barcode = barcode,
                        EncryptedBarcode = Utility.AESDecrypt(barcode),
                        ProductLot = prodPlan.ProductLot,
                        ProductID = prodPlan.ProductID,
                        Status = status
                    };

                    using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                    {
                        _productionPlanRepository = unitOfWork.Register(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                        _productBarcodeRepository = unitOfWork.Register(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;
                        _packagingLogRepository = unitOfWork.Register(_packagingLogRepository.GetType()) as IPackagingLogRepository;
                        _palletStatusRepository = unitOfWork.Register(_palletStatusRepository.GetType()) as IPalletStatusRepository;
                        _barcodeBankRepository = unitOfWork.Register(_barcodeBankRepository.GetType()) as IBarcodeBankRepository;

                        await _productionPlanRepository.Update(prodPlan, new string[] { "Status" }, new object[] { prodPlan.ProductLot });
                        await _productBarcodeRepository.Insert(productBarcode);
                        await _barcodeBankRepository.Delete(new object[] { barcode });
                        await _packagingLogRepository.Delete(new object[] { barcode });
                        await _palletStatusRepository.Delete(new object[] { barcode });

                        await unitOfWork.Commit2();
                    }

                    packagingVM.DestroyLabel += 1;
                }
                //// else, if cancel while packaging, follow process error way
                else
                {
                    await ProcessError2(barcode, packagingLog, _mainView.CurrentUser.UserID, _reason);
                    packagingVM.CancelLabel += 1;
                }

                _mainView.PackagingVM = packagingVM;
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanProduct, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {_reason}");
            }
            catch
            {
                throw;
            }
        }

        private async Task ProcessDestroyCartonLabel(CartonBarcode cartonBarcode)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                var packagingVM = _mainView.PackagingVM;

                //// find packaging log by barcode
                var packagingLog = _mainView.PackagingLogs.FirstOrDefault(p => p.CartonBarcode == cartonBarcode.Barcode);
                if (!_isGetConfirm && packagingLog == null && prodPlan.Status == ProductionPlan.status.InProgress)
                {
                    _currentBarcode = cartonBarcode.Barcode;
                    _mainView.GetConfirmVoid(Messages.Question_DestroyUnPackaged, _destroyCartonUnPackagedConfirm);
                    return;
                }

                if (_isGetConfirm && _confirmType == _destroyCartonUnPackagedConfirm && _isConfirm)
                {
                    //// if user don't want to destroy unpackaged label
                    //// force stop destroy mode to prevent mistake of user
                    await ProcessDestroyEnd();
                    return;
                }

                if (_confirmType != _destroyCartonErrorReasonConfirm)
                    _isGetConfirm = false;

                if (!_isGetConfirm)
                {
                    _currentBarcode = cartonBarcode.Barcode;
                    _mainView.ShowErrorReasonBox(_destroyCartonErrorReasonConfirm);
                    return;
                }

                if (_isGetConfirm && _confirmType == _destroyCartonErrorReasonConfirm && !_isConfirm)
                    return;

                _confirmType = 0;
                _isGetConfirm = false;
                _isConfirm = false;

                //// destroy unused label if product lot packaged completely
                //// other way, cancel label
                cartonBarcode.Status = prodPlan.Status == ProductionPlan.status.Packaged || prodPlan.Status == ProductionPlan.status.Destroying ? ProductBarcode.status.Destroyed : ProductBarcode.status.Cancelled;

                //// if destroy product barcode, only update status of product barcode is destroyed
                if (prodPlan.Status == ProductionPlan.status.Packaged || prodPlan.Status == ProductionPlan.status.Destroying)
                {
                    prodPlan.Status = ProductionPlan.status.Destroying;
                    cartonBarcode.Status = CartonBarcode.status.Destroyed;


                    using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                    {
                        _productionPlanRepository = unitOfWork.Register(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                        _cartonBarcodeRepository = unitOfWork.Register(_cartonBarcodeRepository.GetType()) as ICartonBarcodeRepository;

                        await _productionPlanRepository.Update(prodPlan, new string[] { "Status" }, new object[] { prodPlan.ProductLot });
                        await _cartonBarcodeRepository.Update(cartonBarcode, new string[] { "Status" }, new object[] { cartonBarcode.Barcode });

                        await unitOfWork.Commit2();
                    }

                    packagingVM.DestroyLabel += 1;

                    //if (!await IsExistUnuseLabel(prodPlan.ProductLot))
                    //{
                    //    await CompletePackaging();
                    //}
                }
                //// else, if cancel while packaging, follow process error way
                else if (packagingLog == null)
                {
                    await ProcessError(cartonBarcode, _mainView.CurrentUser.UserID, _reason);

                    packagingVM.CancelLabel += 1;
                }

                _mainView.PackagingVM = packagingVM;
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanCarton, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {cartonBarcode.Barcode} - {_reason}");
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanCarton, MethodBase.GetCurrentMethod(), ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessTakeProductSample(ProductBarcode productBarcode)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;

                productBarcode.Status = ProductBarcode.status.TookSample;

                var packagingSample = new PackagingSample(productBarcode.Barcode,
                    productBarcode.EncryptedBarcode,
                    PackagingSample.type.Product,
                    productBarcode.ProductLot,
                    productBarcode.ProductID,
                    DateTime.Now,
                    _mainView.CurrentUser.UserID,
                    prodPlan.Device);

                await SavePackagingSample(packagingSample, productBarcode);

                prodPlan.TookProductSampleQty += 1;
                _mainView.ProducionPlan = prodPlan;

                if (prodPlan.TookProductSampleQty == prodPlan.ProductSampleQty)
                    ProcessTakeProductSampleEnd();

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanProduct, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {productBarcode.Barcode}");
            }
            catch
            {
                throw;
            }
        }

        private async Task ProcessTakeProductSample2(BarcodeBank barcodeBank)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;

                var packagingSample = new PackagingSample(barcodeBank.Barcode,
                    Utility.AESEncrypt(barcodeBank.Barcode),
                    PackagingSample.type.Product,
                    prodPlan.ProductLot,
                    prodPlan.ProductID,
                    DateTime.Now,
                    _mainView.CurrentUser.UserID,
                    prodPlan.Device);

                await SavePackagingSample2(packagingSample, barcodeBank);

                prodPlan.TookProductSampleQty += 1;
                _mainView.ProducionPlan = prodPlan;

                if (prodPlan.TookProductSampleQty == prodPlan.ProductSampleQty)
                    ProcessTakeProductSampleEnd();

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanProduct, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcodeBank.Barcode}");
            }
            catch
            {
                throw;
            }
        }

        private async Task ProcessTakeCartonSample(CartonBarcode cartonBarcode)
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;

                cartonBarcode.Status = CartonBarcode.status.TookSample;

                var packagingSample = new PackagingSample(cartonBarcode.Barcode,
                    String.Empty,
                    PackagingSample.type.Carton,
                    cartonBarcode.ProductLot,
                    0,
                    DateTime.Now,
                    _mainView.CurrentUser.UserID,
                    prodPlan.Device);

                await SavePackagingSample(packagingSample, cartonBarcode);

                prodPlan.TookCartonSampleQty += 1;
                _mainView.ProducionPlan = prodPlan;

                if (prodPlan.TookCartonSampleQty == 1)
                    ProcessTakeCartonSampleEnd();

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanCarton, MethodBase.GetCurrentMethod(), cartonBarcode.Barcode);
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanCarton, MethodBase.GetCurrentMethod(), ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessEnd()
        {
            try
            {
                //// at moment, system is destroy mode
                if (_mainView.State != _modePackaging)
                    throw new WrappedException(Messages.Error_ProductionPlanModeInvalid);

                await CompletePackaging();

                //var prodPlan = _mainView.ProducionPlan;
                //prodPlan.Status = ProductionPlan.status.Completed;
                //prodPlan.PackagingEndTime = DateTime.Now;

                //await _productionPlanRepository.Update(prodPlan, new string[] { "Status", "PackagingEndTime" }, new object[] { prodPlan.ProductLot });
                //await _productionPlanRepository.Commit();

                //_mainView.ProducionPlan = prodPlan;

                //var packagingVM = _mainView.PackagingVM;
                //packagingVM.PackagingStatus = prodPlan.Status;
                //_mainView.PackagingVM = packagingVM;

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanEnd, MethodBase.GetCurrentMethod(), _mainView.ProducionPlan.ProductLot);
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {ex.Message}");
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private void ProcessDestroyBegin()
        {
            try
            {
                if (_mainView.State != _modePackaging)
                    throw new WrappedException(Messages.Error_ProductionPlanModeInvalid);

                var prodPlan = _mainView.ProducionPlan;

                if (prodPlan.Status != ProductionPlan.status.New && prodPlan.Status != ProductionPlan.status.InProgress)
                    throw new WrappedException(Messages.Error_ProductLotIsPackaged);

                if (_mainView.State != 0)
                    throw new WrappedException(Messages.Error_ProductionPlanModeInvalid);

                _mainView.State = _modeDestroy;
                _mainView.PreStepHint = _mainView.StepHint;
                _mainView.StepHint = Messages.Information_Packaging_ScanDestroyPackageStep;

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabel, MethodBase.GetCurrentMethod(), _mainView.ProducionPlan.ProductLot);
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabel, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabel, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {ex.Message}");
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessDestroyEnd()
        {
            try
            {
                if (_mainView.State != _modeDestroy)
                    throw new WrappedException(Messages.Error_ProductionPlanModeInvalid);

                var prodPlan = _mainView.ProducionPlan;
                if (prodPlan.Status == ProductionPlan.status.Destroying)//&& await IsExistUnuseLabel(prodPlan.ProductLot)
                    throw new WrappedException(Messages.Error_ProductionPlanStillExistUnuseLabel);

                _mainView.State = _modePackaging;

                //// auto complete production plan if status is destroying
                //// this mean, this is destroy package after packaging process
                if (prodPlan.Status == ProductionPlan.status.Destroying)
                {
                    await CompletePackaging();
                }
                else
                {
                    var packagingVM = _mainView.PackagingVM;
                    if (packagingVM.CartonPackingQty > 0 && packagingVM.ActualPackingQtyInCarton < packagingVM.CartonPackingQty)
                        _mainView.StepHint = Messages.Information_Packaging_ScanProductStep;
                    else if (packagingVM.ActualPackingQtyOnPallet < packagingVM.PalletPackingQty)
                        _mainView.StepHint = Messages.Information_Packaging_ScanProductStep;
                    else
                        _mainView.StepHint = _mainView.PreStepHint;
                }

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabelEnd, MethodBase.GetCurrentMethod(), _mainView.ProducionPlan.ProductLot);
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabelEnd, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabelEnd, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {ex.Message}");
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessDestroyForceEnd()
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                if (prodPlan.Status != ProductionPlan.status.Destroying)
                    throw new WrappedException(Messages.Error_ProductionPlanModeInvalid);

                int userID;
                string note;
                if (_mainView.ProcessForceDestroyEnd(out userID, out note))
                {
                    var sitemaps = await _sitemapRepository.ExecuteEntityList("proc_AccessRights_Select",
                        new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.VarChar, Value = userID });

                    if (!sitemaps.Any(p => p.SitemapID == _mainView.Sitemap.SitemapID && p.AccessRights == RoleSitemap.accessRights.Modify))
                        throw new WrappedException(Messages.Error_Permission);

                    await _packagingForceEndRepository.Insert(new PackagingForceEnd(prodPlan.ProductLot, userID, note));
                    await _packagingForceEndRepository.Commit();

                    _mainView.State = _modePackaging;

                    await CompletePackaging();
                }

                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabelForceEnd, MethodBase.GetCurrentMethod(), _mainView.ProducionPlan.ProductLot);
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabelForceEnd, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanDetroyLabelForceEnd, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {ex.Message}");
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private void ProcessTakeProductSampleBegin()
        {
            _mainView.State = _modeTakeProductSample;
            _mainView.StepHint = Messages.Information_Packaging_ScanTakeProductSampleStep;
        }

        private void ProcessTakeProductSampleEnd()
        {
            if (_mainView.CurrentProduct.PackingType == Product.packingType.Carton)
                ProcessTakeCartonSampleBegin();
            else
            {
                _mainView.State = _modePackaging;
                _mainView.StepHint = Messages.Information_Packaging_ScanPalletStep;
            }
        }

        private void ProcessTakeCartonSampleBegin()
        {
            _mainView.State = _modeTakeCartonSample;
            _mainView.StepHint = Messages.Information_Packaging_ScanTakeCartonSampleStep;
        }

        private void ProcessTakeCartonSampleEnd()
        {
            _mainView.State = _modePackaging;
            _mainView.StepHint = Messages.Information_Packaging_ScanPalletStep;
        }

        private async Task CheckPackagedQuantity()
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                var dt = await _productionPlanRepository.ExecuteDataTable("proc_Packaging_Check_PackagedQuantity",
                    new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = prodPlan.ProductLot });

                if (dt.Rows.Count > 0)
                    _mainView.ShowQuantityCheck(dt);
                else
                    _mainView.SetMessage("Tất cả đóng gói đúng quy cách", Utility.MessageType.Information, String.Empty);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Save packaging log into database
        /// Change status of pallet if it is full
        /// Change status of carton if it is packaged finish
        /// Change status of product
        /// </summary>
        /// <returns></returns>
        private async Task<bool> SavePackagingLog(PackagingLog packagingLog, ProductionPlan prodPlan)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingLog.IsRecordAuditTrail = true;

            try
            {
                var packagingVM = _mainView.PackagingVM;

                await _productionPlanRepository.ExecuteNonQuery_Trans("proc_Packaging_Insert",
                    new SqlParameter { ParameterName = "@PackagingStartTime", SqlDbType = SqlDbType.DateTime, Value = prodPlan.PackagingStartTime },
                    new SqlParameter { ParameterName = "@PackagingEndTime", SqlDbType = SqlDbType.DateTime, Value = prodPlan.PackagingEndTime },
                    new SqlParameter { ParameterName = "@ProdPlanStatus", SqlDbType = SqlDbType.Char, Value = prodPlan.Status },
                    new SqlParameter { ParameterName = "@Barcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.Barcode },
                    new SqlParameter { ParameterName = "@EncryptedBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.EncryptedBarcode },
                    new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = packagingLog.ProductLot },
                    new SqlParameter { ParameterName = "@ProductID", SqlDbType = SqlDbType.Int, Value = packagingLog.ProductID },
                    new SqlParameter { ParameterName = "@Date", SqlDbType = SqlDbType.DateTime, Value = packagingLog.Date },
                    new SqlParameter { ParameterName = "@CartonBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.CartonBarcode ?? String.Empty },
                    new SqlParameter { ParameterName = "@PalletCode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.PalletBarcode },
                    new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = packagingLog.UserID },
                    new SqlParameter { ParameterName = "@SitemapID", SqlDbType = SqlDbType.Int, Value = Utility.CurrentSitemapID },
                    new SqlParameter { ParameterName = "@Device", SqlDbType = SqlDbType.VarChar, Value = packagingLog.Device },
                    new SqlParameter { ParameterName = "@CartonStatus", SqlDbType = SqlDbType.Char, Value = packagingVM.ActualPackingQtyInCarton + 1 == packagingVM.CartonPackingQty ? CartonBarcode.status.Packaged : CartonBarcode.status.New });

                return true;
            }
            catch
            {
                throw;
            }
        }

        private async Task<bool> SavePackagingLog2(PackagingLog packagingLog, ProductionPlan prodPlan)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingLog.IsRecordAuditTrail = true;

            try
            {
                var packagingVM = _mainView.PackagingVM;

                await _productionPlanRepository.ExecuteNonQuery_Trans("proc_Packaging_Insert",
                    new SqlParameter { ParameterName = "@PackagingStartTime", SqlDbType = SqlDbType.DateTime, Value = prodPlan.PackagingStartTime },
                    new SqlParameter { ParameterName = "@PackagingEndTime", SqlDbType = SqlDbType.DateTime, Value = prodPlan.PackagingEndTime },
                    new SqlParameter { ParameterName = "@ProdPlanStatus", SqlDbType = SqlDbType.Char, Value = prodPlan.Status },
                    new SqlParameter { ParameterName = "@Barcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.Barcode },
                    new SqlParameter { ParameterName = "@EncryptedBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.EncryptedBarcode },
                    new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = packagingLog.ProductLot },
                    new SqlParameter { ParameterName = "@ProductID", SqlDbType = SqlDbType.Int, Value = packagingLog.ProductID },
                    new SqlParameter { ParameterName = "@Date", SqlDbType = SqlDbType.DateTime, Value = packagingLog.Date },
                    new SqlParameter { ParameterName = "@CartonBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.CartonBarcode ?? String.Empty },
                    new SqlParameter { ParameterName = "@PalletCode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.PalletBarcode },
                    new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = packagingLog.UserID },
                    new SqlParameter { ParameterName = "@SitemapID", SqlDbType = SqlDbType.Int, Value = Utility.CurrentSitemapID },
                    new SqlParameter { ParameterName = "@Device", SqlDbType = SqlDbType.VarChar, Value = packagingLog.Device },
                    new SqlParameter { ParameterName = "@CartonStatus", SqlDbType = SqlDbType.Char, Value = packagingVM.ActualPackingQtyInCarton + 1 == packagingVM.CartonPackingQty ? CartonBarcode.status.Packaged : CartonBarcode.status.New });

                return true;
            }
            catch
            {
                throw;
            }
        }

        private async Task<bool> SavePackagingLogNoQRCode(PackagingLog packagingLog, ProductionPlan prodPlan)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingLog.IsRecordAuditTrail = true;

            try
            {
                var packagingVM = _mainView.PackagingVM;

                await _productionPlanRepository.ExecuteNonQuery_Trans("proc_Packaging_InsertNoQRCode",
                    new SqlParameter { ParameterName = "@PackagingStartTime", SqlDbType = SqlDbType.DateTime, Value = prodPlan.PackagingStartTime },
                    new SqlParameter { ParameterName = "@PackagingEndTime", SqlDbType = SqlDbType.DateTime, Value = prodPlan.PackagingEndTime },
                    new SqlParameter { ParameterName = "@ProdPlanStatus", SqlDbType = SqlDbType.Char, Value = prodPlan.Status },
                    new SqlParameter { ParameterName = "@Barcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.Barcode },
                    new SqlParameter { ParameterName = "@EncryptedBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.EncryptedBarcode },
                    new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = packagingLog.ProductLot },
                    new SqlParameter { ParameterName = "@ProductID", SqlDbType = SqlDbType.Int, Value = packagingLog.ProductID },
                    new SqlParameter { ParameterName = "@Date", SqlDbType = SqlDbType.DateTime, Value = packagingLog.Date },
                    new SqlParameter { ParameterName = "@CartonBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.CartonBarcode ?? String.Empty },
                    new SqlParameter { ParameterName = "@PalletCode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.PalletBarcode },
                    new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = packagingLog.UserID },
                    new SqlParameter { ParameterName = "@SitemapID", SqlDbType = SqlDbType.Int, Value = Utility.CurrentSitemapID },
                    new SqlParameter { ParameterName = "@Device", SqlDbType = SqlDbType.VarChar, Value = packagingLog.Device },
                    new SqlParameter { ParameterName = "@Qty", SqlDbType = SqlDbType.VarChar, Value = packagingLog.Qty },
                    new SqlParameter { ParameterName = "@CartonStatus", SqlDbType = SqlDbType.Char, Value = packagingVM.ActualPackingQtyInCarton + 1 == packagingVM.CartonPackingQty ? CartonBarcode.status.Packaged : CartonBarcode.status.New });

                return true;
            }
            catch
            {
                throw;
            }
        }


        /// <summary>
        /// Save packaging error into database and decrease packaging progress
        /// </summary>
        /// <returns></returns>
        private async Task SavePackagingError(PackagingError packagingError, PackagingLog packagingLog)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingError.IsRecordAuditTrail = true;
            packagingLog.IsRecordAuditTrail = true;

            try
            {
                await _productionPlanRepository.ExecuteNonQuery_Trans("proc_Packaging_Destroy",
                    new SqlParameter { ParameterName = "@Barcode", SqlDbType = SqlDbType.VarChar, Value = packagingError.Barcode },
                    new SqlParameter { ParameterName = "@EncryptedBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingError.EncryptedBarcode },
                    new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.Char, Value = packagingError.Type },
                    new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = packagingError.ProductLot },
                    new SqlParameter { ParameterName = "@ProductID", SqlDbType = SqlDbType.Int, Value = packagingError.ProductID },
                    new SqlParameter { ParameterName = "@PackagingDate", SqlDbType = SqlDbType.DateTime, Value = packagingError.PackagingDate },
                    new SqlParameter { ParameterName = "@ProcessDate", SqlDbType = SqlDbType.DateTime, Value = packagingError.ProcessDate },
                    new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = packagingError.UserID },
                    new SqlParameter { ParameterName = "@SitemapID", SqlDbType = SqlDbType.Int, Value = Utility.CurrentSitemapID },
                    new SqlParameter { ParameterName = "@Device", SqlDbType = SqlDbType.VarChar, Value = packagingError.Device },
                    new SqlParameter { ParameterName = "@Note", SqlDbType = SqlDbType.NVarChar, Value = packagingError.Note },
                    new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.Char, Value = ProductBarcode.status.Cancelled },
                    new SqlParameter { ParameterName = "@CartonBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.CartonBarcode ?? String.Empty });
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task SavePackagingError2(PackagingError packagingError, PackagingLog packagingLog)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingError.IsRecordAuditTrail = true;
            packagingLog.IsRecordAuditTrail = true;

            try
            {
                await _productionPlanRepository.ExecuteNonQuery_Trans("proc_Packaging_Destroy",
                    new SqlParameter { ParameterName = "@Barcode", SqlDbType = SqlDbType.VarChar, Value = packagingError.Barcode },
                    new SqlParameter { ParameterName = "@EncryptedBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingError.EncryptedBarcode },
                    new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.Char, Value = packagingError.Type },
                    new SqlParameter { ParameterName = "@ProductLot", SqlDbType = SqlDbType.VarChar, Value = packagingError.ProductLot },
                    new SqlParameter { ParameterName = "@ProductID", SqlDbType = SqlDbType.Int, Value = packagingError.ProductID },
                    new SqlParameter { ParameterName = "@PackagingDate", SqlDbType = SqlDbType.DateTime, Value = packagingError.PackagingDate },
                    new SqlParameter { ParameterName = "@ProcessDate", SqlDbType = SqlDbType.DateTime, Value = packagingError.ProcessDate },
                    new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.Int, Value = packagingError.UserID },
                    new SqlParameter { ParameterName = "@SitemapID", SqlDbType = SqlDbType.Int, Value = Utility.CurrentSitemapID },
                    new SqlParameter { ParameterName = "@Device", SqlDbType = SqlDbType.VarChar, Value = packagingError.Device },
                    new SqlParameter { ParameterName = "@Note", SqlDbType = SqlDbType.NVarChar, Value = packagingError.Note },
                    new SqlParameter { ParameterName = "@Status", SqlDbType = SqlDbType.Char, Value = ProductBarcode.status.Cancelled },
                    new SqlParameter { ParameterName = "@CartonBarcode", SqlDbType = SqlDbType.VarChar, Value = packagingLog.CartonBarcode ?? String.Empty });
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task SaveUnPackagingError(PackagingError packagingError)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingError.IsRecordAuditTrail = true;

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                {
                    _packagingErrorRepository = unitOfWork.Register(_packagingErrorRepository.GetType()) as IPackagingErrorRepository;
                    _productBarcodeRepository = unitOfWork.Register(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;
                    _cartonBarcodeRepository = unitOfWork.Register(_cartonBarcodeRepository.GetType()) as ICartonBarcodeRepository;

                    await _packagingErrorRepository.Insert(packagingError);

                    if (packagingError.Type == PackagingError.type.Product)
                    {
                        var productBarcode = _mainView.ProductBarcodes.First(p => p.Barcode == packagingError.Barcode);
                        productBarcode.Barcode = packagingError.Barcode;
                        productBarcode.Status = ProductBarcode.status.Cancelled;
                        productBarcode.IsRecordAuditTrail = true;
                        await _productBarcodeRepository.Update(productBarcode, new string[] { "Status" }, new object[] { productBarcode.Barcode });
                    }
                    else
                    {
                        var cartonBarcode = _mainView.CartonBarcodes.First(p => p.Barcode == packagingError.Barcode);
                        cartonBarcode.Barcode = packagingError.Barcode;
                        cartonBarcode.Status = CartonBarcode.status.Cancelled;
                        cartonBarcode.IsRecordAuditTrail = true;
                        await _cartonBarcodeRepository.Update(cartonBarcode, new string[] { "Status" }, new object[] { cartonBarcode.Barcode });
                    }

                    await unitOfWork.Commit2();
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task SaveUnPackagingError2(PackagingError packagingError)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingError.IsRecordAuditTrail = true;

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                {
                    _packagingErrorRepository = unitOfWork.Register(_packagingErrorRepository.GetType()) as IPackagingErrorRepository;
                    _productBarcodeRepository = unitOfWork.Register(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;
                    _cartonBarcodeRepository = unitOfWork.Register(_cartonBarcodeRepository.GetType()) as ICartonBarcodeRepository;

                    await _packagingErrorRepository.Insert(packagingError);

                    if (packagingError.Type == PackagingError.type.Product)
                    {
                        var productBarcode = new ProductBarcode
                        {
                            Barcode = packagingError.Barcode,
                            EncryptedBarcode = packagingError.EncryptedBarcode,
                            ProductLot = packagingError.ProductLot,
                            ProductID = packagingError.ProductID,
                            Status = ProductBarcode.status.Cancelled,
                            IsRecordAuditTrail = true
                        };

                        await _productBarcodeRepository.Insert(productBarcode);
                    }
                    else
                    {
                        var cartonBarcode = _mainView.CartonBarcodes.First(p => p.Barcode == packagingError.Barcode);
                        cartonBarcode.Barcode = packagingError.Barcode;
                        cartonBarcode.Status = CartonBarcode.status.Cancelled;
                        cartonBarcode.IsRecordAuditTrail = true;

                        await _cartonBarcodeRepository.Update(cartonBarcode, new string[] { "Status" }, new object[] { cartonBarcode.Barcode });
                    }

                    await unitOfWork.Commit2();
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task SavePackagingSample(PackagingSample packagingSample, ProductBarcode productBarcode)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingSample.IsRecordAuditTrail = true;

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                {
                    _packagingSampleRepository = unitOfWork.Register(_packagingSampleRepository.GetType()) as IPackagingSampleRepository;
                    _productBarcodeRepository = unitOfWork.Register(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;

                    await _packagingSampleRepository.Insert(packagingSample);
                    await _productBarcodeRepository.Update(productBarcode, new string[] { "Status" }, new object[] { productBarcode.Barcode });

                    await unitOfWork.Commit2();
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task SavePackagingSample2(PackagingSample packagingSample, BarcodeBank barcodeBank)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingSample.IsRecordAuditTrail = true;

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                {
                    _packagingSampleRepository = unitOfWork.Register(_packagingSampleRepository.GetType()) as IPackagingSampleRepository;
                    _barcodeBankRepository = unitOfWork.Register(_barcodeBankRepository.GetType()) as IBarcodeBankRepository;

                    await _packagingSampleRepository.Insert(packagingSample);
                    await _barcodeBankRepository.Delete(new object[] { barcodeBank.Barcode });

                    await unitOfWork.Commit2();
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task SavePackagingSample(PackagingSample packagingSample, CartonBarcode cartonBarcode)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            packagingSample.IsRecordAuditTrail = true;

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                {
                    _packagingSampleRepository = unitOfWork.Register(_packagingSampleRepository.GetType()) as IPackagingSampleRepository;
                    _cartonBarcodeRepository = unitOfWork.Register(_cartonBarcodeRepository.GetType()) as ICartonBarcodeRepository;

                    await _packagingSampleRepository.Insert(packagingSample);
                    await _cartonBarcodeRepository.Update(cartonBarcode, new string[] { "Status" }, new object[] { cartonBarcode.Barcode });

                    await unitOfWork.Commit2();
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task CompletePackaging()
        {
            try
            {
                var prodPlan = _mainView.ProducionPlan;
                prodPlan.Status = ProductionPlan.status.Completed;
                prodPlan.PackagingEndTime = DateTime.Now;

                var packagingVM = _mainView.PackagingVM;
                packagingVM.PackagingStatus = ProductionPlan.status.Completed;

                await _productionPlanRepository.Update(prodPlan, new string[] { "Status", "PackagingEndTime" }, new object[] { prodPlan.ProductLot });
                await _productionPlanRepository.Commit();

                _mainView.ProducionPlan = prodPlan;
                _mainView.PackagingVM = packagingVM;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        private async Task ProcessNoQRCodeQuantity(decimal quantity)
        {
            string barcode = "ProcessNoQRCodeQuantity" + "_" + quantity.ToString();
            try
            {
                if (quantity <= 0)
                    throw new WrappedException(Messages.Error_ScanQuantityFirst);
                //// check pallet barcode has been scanned yet?
                if (_mainView.CurrentPallet == null)
                    throw new WrappedException(Messages.Error_ScanPalletBarcodeFirst);

                var prodPlan = _mainView.ProducionPlan;

                barcode = _mainView.CurrentPallet.PalletCode + "_" + prodPlan.ProductLot + "_" + quantity.ToString();

                //// save packaging log and update information of production plan
                var packagingLog = new PackagingLog(
                    string.Empty,
                    string.Empty,
                    prodPlan.ProductLot,
                    prodPlan.ProductID,
                    DateTime.Now,
                    _mainView.CurrentProduct.PackingType == Product.packingType.Carton ? _mainView.CurrentCartonBarcode.Barcode : String.Empty,
                    _mainView.CurrentPallet.PalletCode,
                    _mainView.CurrentUser.UserID,
                    prodPlan.Device,
                    quantity
                    );

                if (await SavePackagingLogNoQRCode(packagingLog, prodPlan))
                {
                    //// update information of production plan on screen
                    prodPlan.Status = ProductionPlan.status.InProgress;
                    prodPlan.ActualPackingQty += quantity;

                    //// reload packaging logs
                    _mainView.PackagingLogs.Add(packagingLog);

                    _mainView.ProducionPlan = prodPlan;

                    //// check packaging progress with plan
                    if (IsPackagingComplete())
                    {
                        _currentBarcode = "ket_thuc";
                        _mainView.GetConfirmVoid(Messages.Question_ContinuePackaging, _processEndConfirm);
                        return;
                    }
                    RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanProduct, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode}");

                    _mainView.StepHint = Messages.Information_Packaging_ScanPalletStep;
                }
            }
            catch (WrappedException ex)
            {
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                RecordAuditTrail(_mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), $"{_mainView.ProducionPlan.ProductLot} - {barcode} - {ex.Message}");
                //throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Process barcode is scanned
        /// </summary>
        /// <param name="barcode"></param>
        public async Task<string> ProcessBarcode(string barcode)
        {
            try
            {
                //// scanned barcode is employee
                if (Utility.EmployeeRegex.IsMatch(barcode))
                {
                    barcode = Utility.EmployeeRegex.Match(barcode).Captures[0].Value.Remove(0, 3);
                    await ProcessEmployeeBarcode(barcode);
                }
                //// scanned barcode is product lot
                else if (Utility.ProductLotRegex.IsMatch(barcode))
                {
                    barcode = Utility.ProductLotRegex.Match(barcode).Captures[0].Value;

                    //using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork2())
                    //{
                    //    _barcodeBankRepository = unitOfWork.Register(_barcodeBankRepository.GetType()) as IBarcodeBankRepository;

                    //1. Check Product Lot Packaging Type 
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter code = new SqlParameter("ProductLot", barcode);
                    listParam.Add(code);
                    string temp = await _productionPlanRepository.ExecuteScalar("proc_ProductionPlans_CheckPackagingQRCodeByProductLot", listParam.ToArray());
                    _ProductLotPackagingType = temp == "1" ? 1 : 2;
                    _mainView.ProductLotPackagingType = _ProductLotPackagingType;
                    //}

                    if (_mainView.ProductLotPackagingType == 1)
                    {
                        await ProcessProductLotBarcode(barcode);
                    }
                    else
                    {
                        await ProcessProductLotBarcode(barcode);
                    }
                }
                //// scanned barcode is pallet
                else if (Utility.PalletRegex.IsMatch(barcode))
                {
                    barcode = Utility.PalletRegex.Match(barcode).Captures[0].Value;
                    await ProcessPalletBarcode(barcode);
                }
                //// scanned barcode is carton
                else if (Utility.CartonRegex.IsMatch(barcode))
                {
                    barcode = Utility.CartonRegex.Match(barcode).Captures[0].Value;
                    await ProcessCartonBarcode(barcode);
                }
                else if (barcode.ToLower().EndsWith("ket_thuc_huy2"))
                {
                    await ProcessDestroyForceEnd();
                }
                else if (barcode.ToLower().EndsWith("ket_thuc_huy"))
                {
                    await ProcessDestroyEnd();
                }
                else if (barcode.ToLower().EndsWith("huy"))
                {
                    ProcessDestroyBegin();
                }
                else if (barcode.ToLower().EndsWith("ket_thuc"))
                {
                    await ProcessEnd();
                }
                else if (barcode.ToLower().EndsWith("khong"))
                {

                }
                else if (barcode.ToLower().EndsWith("kiem_tra"))
                {
                    await CheckPackagedQuantity();
                }
                else
                {
                    if (_mainView.ProductLotPackagingType == 1)
                    {
                        string decryptedBarcode;
                        try
                        {
                            //// scanned barcode is product
                            barcode = barcode.Split('|')[0];
                            barcode = barcode.StartsWith("SP") ? barcode.Remove(0, 2) : barcode;
                            decryptedBarcode = Utility.AESDecrypt(barcode.Split('|')[0]);
                        }
                        catch
                        {
                            RecordAuditTrail(_mainView.CurrentUser == null ? 0 : _mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), barcode);
                            throw new WrappedException(Messages.Error_BarcodeIsInvalid);
                        }

                        //string decryptedBarcode = barcode;
                        if (Utility.ProductRegex.IsMatch(decryptedBarcode))
                        {
                            barcode = Utility.ProductRegex.Match(decryptedBarcode).Captures[0].Value;
                            await ProcessProductBarcode(barcode);
                        }
                        else if (Utility.Product2Regex.IsMatch(decryptedBarcode))
                        {
                            barcode = Utility.Product2Regex.Match(decryptedBarcode).Captures[0].Value;
                            await ProcessProductBarcode2(barcode);
                        }
                        else
                        {
                            RecordAuditTrail(_mainView.CurrentUser == null ? 0 : _mainView.CurrentUser.UserID, AuditTrail.action.ScanBarcodeInvalid, MethodBase.GetCurrentMethod(), barcode);
                            throw new WrappedException(Messages.Error_BarcodeIsInvalid);
                        }
                    }
                    //Packaging NO QR Code
                    else if (barcode == "dong_y" && _mainView.ProductLotPackagingType == 2)
                    {
                        await ProcessNoQRCodeQuantity(_mainView.Quantity);
                    }
                }

                return barcode;
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public int ConfirmType { set => _confirmType = value; }

        public bool IsGetConfirm { set => _isGetConfirm = value; }

        public bool IsConfirm { set => _isConfirm = value; }

        public string Reason { set => _reason = value; }

        public string Barcode { get => _currentBarcode; }
    }
}
