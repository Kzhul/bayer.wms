﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Prd.Presenters
{
    public interface IPackagingReportView : IBaseView
    {
        DateTime FromDate { get; }

        DateTime ToDate { get; }

        string ProductLot { get; }

        int ProductID { get; }

        System.Data.DataTable Products { set; }

        System.Data.DataTable Reports { set; }
    }

    public interface IPackagingReportPresenter : IBasePresenter
    {
        Task LoadProducts();
    }

    public class PackagingReportPresenter : BasePresenter, IPackagingReportPresenter
    {
        private IPackagingReportView _mainView;
        private IProductionPlanRepository _productionPlanRepository;
        private IProductRepository _productRepository;

        public PackagingReportPresenter(IPackagingReportView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IProductionPlanRepository productionPlanRepository,
            IProductRepository productRepository)
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _productionPlanRepository = productionPlanRepository;
            _productRepository = productRepository;
        }

        /// <summary>
        /// Get products from database to combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadProducts()
        {
            try
            {
                var products = await _productRepository.GetAsync(p => !p.IsDeleted);
                _mainView.Products = products.ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async Task LoadMainData()
        {
            try
            {

                _mainView.Reports = await _productionPlanRepository.ExecuteDataTable("proc_ProductionPlans_Select_PackagingReport",
                    new SqlParameter { ParameterName = "FromDate", SqlDbType = SqlDbType.Date, Value = _mainView.FromDate },
                    new SqlParameter { ParameterName = "ToDate", SqlDbType = SqlDbType.Date, Value = _mainView.ToDate },
                    new SqlParameter { ParameterName = "ProductLot", SqlDbType = SqlDbType.VarChar, Value = _mainView.ProductLot },
                    new SqlParameter { ParameterName = "ProductID", SqlDbType = SqlDbType.Int, Value = _mainView.ProductID });
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }
    }
}
