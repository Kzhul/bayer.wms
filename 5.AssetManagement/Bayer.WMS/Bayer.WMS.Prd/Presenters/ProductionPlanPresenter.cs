﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Prd.Presenters
{
    public interface IProductionPlanView : IBaseView
    {
        DateTime FromDate { get; }

        DateTime ToDate { get; }

        string ProductLot { get; }

        int ProductID { get; }

        IList<Device> Devices { set; }

        System.Data.DataTable Products { set; }

        IList<ProductionPlan> ProductionPlans { get; set; }

        IList<ProductionPlan> DeletedProductionPlans { get; set; }
    }

    public interface IProductionPlanPresenter : IBasePresenter
    {
        Task LoadDevices();

        Task LoadProducts();

        Task<bool> IsExistImportedLot(DateTime fromDate, DateTime toDate);

        Task ImportExcel(DateTime fromDate, DateTime toDate, string fileName, string updateNote);

        Task ExportQRCode(List<string> productLots, bool isExportCarton, bool isExportProduct, string folderName);

        Task GenerateProductBarcode(List<string> productLots);

        Task GenerateCartonBarcode(List<string> productLots);

        Task HoldProductionPlan(List<string> productLots);

        Task UnHoldProductionPlan(List<string> productLots);

        Task GenerateAdditionProductBarcode(string productLot, int productID, int qty);

        Task GenerateAdditionCartonBarcode(string productLot, int productID, int qty);

        Task<IList<string>> GetCartonLabelToPrint(ProductionPlan productionPlan, int qty, string note);

        Task UpdateProductionPlan(string productLot, DateTime? planDate, string device, string poNumber, string supplierLot, string note);

        Task UpdateProductionPlanStatus(string productLot, string status);

        void DeleteProductionPlans(ProductionPlan prodPlans);
    }

    public class ProductionPlanPresenter : BasePresenter, IProductionPlanPresenter
    {
        private IProductionPlanView _mainView;
        private IProductionPlanRepository _productionPlanRepository;
        private IProductionPlanImportLogRepository _productionPlanImportLogRepository;
        private IProductRepository _productRepository;
        private IProductPackingRepository _productPackingRepository;
        private IProductBarcodeRepository _productBarcodeRepository;
        private ICartonBarcodeRepository _cartonBarcodeRepository;
        private IDeviceRepository _deviceRepository;
        string[] _alphabet = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

        public ProductionPlanPresenter(IProductionPlanView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IProductionPlanRepository productionPlanRepository,
            IProductionPlanImportLogRepository productionPlanImportLogRepository,
            IProductRepository productRepository,
            IProductPackingRepository productPackingRepository,
            IProductBarcodeRepository productBarcodeRepository,
            ICartonBarcodeRepository cartonBarcodeRepository,
            IDeviceRepository deviceRepository)
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _productionPlanRepository = productionPlanRepository;
            _productionPlanImportLogRepository = productionPlanImportLogRepository;
            _productRepository = productRepository;
            _productPackingRepository = productPackingRepository;
            _productBarcodeRepository = productBarcodeRepository;
            _cartonBarcodeRepository = cartonBarcodeRepository;
            _deviceRepository = deviceRepository;

            _mainView.ProductionPlans = new List<ProductionPlan>();
            _mainView.DeletedProductionPlans = new List<ProductionPlan>();
        }

        private async Task ExportQRCartonCode(string productLot, string folderName)
        {
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            try
            {
                string fileName = Path.Combine(Utility.AppPath, @"Templates\QR_Carton.xlsx");

                workbook = workbooks.Open(fileName, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                var cartonBarcodes = await _cartonBarcodeRepository.GetAsync(p => p.ProductLot == productLot);

                for (int i = 0; i < cartonBarcodes.Count; i++)
                {
                    sheetDetail.Cells[2 + i, 1] = cartonBarcodes[i].Barcode;
                }

                excelApp.DisplayAlerts = false;
                workbook.SaveAs(Path.Combine(folderName, $"{productLot}_QR_Thung.xlsx"), XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false,
                    XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);

                RecordAuditTrail(AuditTrail.action.ExportCartonBarcode, MethodBase.GetCurrentMethod(), productLot);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private async Task ExportQRProductCode(string productLot, string folderName)
        {
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            try
            {
                string fileName = Path.Combine(Utility.AppPath, @"Templates\QR_Product.xlsx");

                workbook = workbooks.Open(fileName, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                var productBarcodes = await _productBarcodeRepository.GetAsync(p => p.ProductLot == productLot);

                for (int i = 0; i < productBarcodes.Count; i++)
                {
                    sheetDetail.Cells[2 + i, 1] = productBarcodes[i].EncryptedBarcode;
                    sheetDetail.Cells[2 + i, 2] = "|";
                    sheetDetail.Cells[2 + i, 4] = int.Parse(productBarcodes[i].Barcode.Replace(productBarcodes[i].ProductLot, String.Empty));
                }

                excelApp.DisplayAlerts = false;
                workbook.SaveAs(Path.Combine(folderName, $"{productLot}_QR_SanPham.xlsx"), XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false,
                    XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);

                RecordAuditTrail(AuditTrail.action.ExportProductBarcode, MethodBase.GetCurrentMethod(), productLot);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public async Task LoadDevices()
        {
            try
            {
                _mainView.Devices = await _deviceRepository.GetAsync(p => !p.IsDeleted);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get products from database to combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadProducts()
        {
            try
            {
                var products = await _productRepository.GetAsync(p => !p.IsDeleted);
                _mainView.Products = products.ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get production plans from database
        /// </summary>
        /// <returns></returns>
        public override async Task LoadMainData()
        {
            try
            {
                Expression<Func<ProductionPlan, bool>> predicate = p => p.PlanDate >= _mainView.FromDate && p.PlanDate <= _mainView.ToDate;

                if (!String.IsNullOrWhiteSpace(_mainView.ProductLot))
                    predicate = PredicateBuilder.And<ProductionPlan>(predicate, p => p.ProductLot.Contains(_mainView.ProductLot));
                if (_mainView.ProductID > 0)
                    predicate = PredicateBuilder.And<ProductionPlan>(predicate, p => p.ProductID == _mainView.ProductID);

                _mainView.ProductionPlans = await _productionPlanRepository.GetIncludeProductAsync(predicate);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public void DeleteProductionPlans(ProductionPlan prodPlans)
        {
            try
            {
                _mainView.DeletedProductionPlans.Add(prodPlans);
                _mainView.ProductionPlans.Remove(prodPlans);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task<bool> IsExistImportedLot(DateTime fromDate, DateTime toDate)
        {
            try
            {
                var productionPlan = await _productionPlanRepository.GetSingleAsync(p => p.PlanDate >= fromDate && p.PlanDate <= toDate);
                return productionPlan != null;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Import data from excel
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task ImportExcel(DateTime fromDate, DateTime toDate, string fileName, string updateNote)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            int rowStart = 4;
            string start = "A" + rowStart;
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add();
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            bool isSuccess = false;
            try
            {
                workbook = workbooks.Open(fileName);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                //// get a range to work with
                range = sheetDetail.get_Range(start);

                //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                range = range.get_End(XlDirection.xlDown);

                //// get the address of the bottom cell
                string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                //// specific end column
                end = downAddress.Remove(0, 1);

                //// throw exception to prevent loop through large data
                //if (int.Parse(end) > 1000000)
                //{
                //    _mainPresenter.SetMessage(Messages.Validate_ExcelFile_Large, Utility.MessageType.Error);
                //    return;
                //}

                end = "X" + end;
                end = "X5000";

                //// Get the range, then values from start to end
                range = sheetDetail.get_Range(start, end);
                var values = (object[,])range.Value2;
                int count = values.GetLength(0);

                //// write import log
                await _productionPlanImportLogRepository.Insert(new ProductionPlanImportLog(LoginInfo.UserID, updateNote, fromDate, toDate, fileName));
                await _productionPlanImportLogRepository.Commit();

                var products = await _productRepository.GetAsync(p => !p.IsDeleted);
                var devices = await _deviceRepository.GetAsync(p => !p.IsDeleted);

                for (int i = 1; i <= count; i++)
                {
                    //// Validate product code
                    string productCode = $"{values[i, 5]}";
                    if (String.IsNullOrWhiteSpace(productCode))
                        break;

                    try
                    {
                        double d_planDate;
                        double.TryParse($"{values[i, 1]}", out d_planDate);

                        double d_manufacturingDate;
                        double.TryParse($"{values[i, 15]}", out d_manufacturingDate);

                        double d_expiryDate;
                        double.TryParse($"{values[i, 17]}", out d_expiryDate);

                        string str_planDate = $"{values[i, 1]}";
                        string str_manufacturingDate = $"{values[i, 15]}";
                        string str_expiryDate = $"{values[i, 17]}";

                        DateTime planDate = d_planDate > 0 ? DateTime.FromOADate(d_planDate) : DateTime.Parse(str_planDate);

                        DateTime? manufacturingDate = null;
                        if (!String.IsNullOrWhiteSpace(str_manufacturingDate))
                            manufacturingDate = d_manufacturingDate > 0 ? DateTime.FromOADate(d_manufacturingDate) : DateTime.Parse(str_manufacturingDate);

                        DateTime? expiryDate = null;
                        if (!String.IsNullOrWhiteSpace(str_expiryDate))
                            expiryDate = d_expiryDate > 0 ? DateTime.FromOADate(d_expiryDate) : DateTime.Parse(str_expiryDate);

                        if (planDate.Date >= fromDate.Date && planDate.Date <= toDate.Date)
                        {
                            //// Validate production lot
                            string productLot = values[i, 19] == null ? $"{values[i, 18]}" : $"{values[i, 19]}";
                            if (String.IsNullOrWhiteSpace(productLot))
                                throw new WrappedException(Messages.Validate_Required_ProductionLot);

                            //// Validate device code
                            string deviceCode = $"{values[i, 4]}";
                            if (String.IsNullOrWhiteSpace(deviceCode))
                                throw new WrappedException(Messages.Validate_Required_Device);

                            //// Validate device
                            var device = devices.FirstOrDefault(p => p.DeviceCode == deviceCode);
                            if (device == null)
                                throw new WrappedException(Messages.Validate_Required_Device);

                            //// Validate PO number
                            string pONumber = $"{values[i, 21]}";
                            //if (String.IsNullOrWhiteSpace(pONumber))
                            //    throw new WrappedException(Messages.Validate_Required_PONumber);

                            //// Validate product
                            var product = products.FirstOrDefault(p => p.ProductCode == productCode);
                            if (product == null)
                                throw new WrappedException(Messages.Validate_Required_Product);

                            //// Validate quantity
                            decimal quantity;
                            if (!decimal.TryParse($"{values[i, 11]}", out quantity))
                                throw new WrappedException(Messages.Validate_Required_Quantity);
                            if (quantity < 0)
                                throw new WrappedException(Messages.Validate_Range_Quantity);

                            //// Validate package quantity
                            decimal packageQty;
                            if (!decimal.TryParse($"{values[i, 12]}", out packageQty))
                                throw new WrappedException(Messages.Validate_Required_PackageQuantity);
                            if (packageQty < 0)
                                throw new WrappedException(Messages.Validate_Range_PackageQuantity);

                            //// Validate package size
                            decimal packageSize;
                            if (!decimal.TryParse($"{values[i, 7]}", out packageSize))
                                throw new WrappedException(Messages.Validate_Required_PackageSize);
                            if (packageSize < 0)
                                throw new WrappedException(Messages.Validate_Range_PackageSize);

                            //// Validate sequence
                            int sequence;
                            if (!int.TryParse($"{values[i, 2]}", out sequence))
                                throw new WrappedException(Messages.Validate_Required_Sequence);
                            if (sequence < 0)
                                throw new WrappedException(Messages.Validate_Range_Sequence);

                            string uom = $"{values[i, 8]}";
                            string supplierLot = $"{values[i, 20]}";
                            string note = $"{values[i, 22]}";

                            var productionPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == productLot);
                            //// insert new production plan if production lot is not exists
                            if (productionPlan == null)
                            {
                                productionPlan = new ProductionPlan(productLot, supplierLot, deviceCode, pONumber, product.ProductID, quantity,
                                    planDate, manufacturingDate, expiryDate, packageQty, packageSize, uom, sequence, note, false, false, 0, 0, ProductionPlan.status.New);
                                productionPlan.IsRecordAuditTrail = true;

                                await _productionPlanRepository.Insert(productionPlan);
                            }
                            //// only update production lot if status is new
                            else if (productionPlan.Status == ProductionPlan.status.New)
                            {
                                productionPlan.SupplierLot = supplierLot?.Trim();
                                productionPlan.Device = deviceCode?.Trim();
                                productionPlan.PONumber = pONumber?.Trim();
                                productionPlan.ProductID = product.ProductID;
                                productionPlan.Quantity = quantity;
                                productionPlan.PlanDate = planDate;
                                productionPlan.ManufacturingDate = manufacturingDate;
                                productionPlan.ExpiryDate = expiryDate;
                                productionPlan.PackageQuantity = packageQty;
                                productionPlan.PackageSize = packageSize;
                                productionPlan.Sequence = sequence;
                                productionPlan.Note = note;
                                productionPlan.IsRecordAuditTrail = true;

                                await _productionPlanRepository.Update(productionPlan, new object[] { productLot });
                            }
                        }

                        await _productionPlanRepository.Commit();
                        isSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        errorLine.Add(i + rowStart, ex.Message);
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                    await LoadMainData();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(String.Format(Messages.Information_ImportSucessfullyWithError, String.Join(",", errorLine)), Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
            }
        }

        public async Task ExportQRCode(List<string> productLots, bool isExportCarton, bool isExportProduct, string folderName)
        {
            try
            {
                foreach (string proLot in productLots)
                {
                    await Task.WhenAll(Task.Run(async () =>
                    {
                        if (isExportCarton)
                            await ExportQRCartonCode(proLot, folderName);
                    }), Task.Run(async () =>
                    {
                        if (isExportProduct)
                            await ExportQRProductCode(proLot, folderName);
                    }));
                }
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _mainPresenter.SetMessage(Messages.Information_ExportSucessfully, Utility.MessageType.Information);
            }
        }

        public async Task GenerateProductBarcode(List<string> productLots)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            Dictionary<int, string> errorLine = new Dictionary<int, string>();
            bool isSuccess = false;

            try
            {
                for (int i = 0; i < productLots.Count; i++)
                {
                    string productLot = productLots[i];

                    try
                    {
                        //// check production plan is existed
                        var productionPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == productLot);
                        if (productionPlan == null)
                            throw new WrappedException(Messages.Error_ProductLotNotExisted);

                        var product = await _productRepository.GetSingleAsync(p => p.ProductID == productionPlan.ProductID && !p.IsDeleted);
                        if (product == null)
                            throw new WrappedException(Messages.Error_ProducNotExisted);

                        decimal packageQty = Math.Ceiling((decimal)(productionPlan.PackageQuantity * (1.00M + ((decimal)(product.PrintLabelPercentage ?? 0) / 100))));
                        //// addition to sample qty to take sample
                        packageQty = packageQty + (product.SampleQty ?? 0);

                        using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                        {
                            _productionPlanRepository = unitOfWork.Register(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                            _productBarcodeRepository = unitOfWork.Register(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;

                            //// generate product barcode based on package quantity in production plan
                            for (int j = 0; j < packageQty; j++)
                            {
                                string barcode = $"{productionPlan.ProductLot}{(j + 1):00000}";
                                var productBarcode = new ProductBarcode(barcode, productionPlan.ProductLot, productionPlan.ProductID, ProductBarcode.status.New);
                                await _productBarcodeRepository.Insert(productBarcode);
                            }

                            //// update production plan is generated product code
                            productionPlan.IsGenProductBarcode = true;
                            await _productionPlanRepository.Update(productionPlan, new string[] { "IsGenProductBarcode" }, new object[] { productionPlan.ProductLot });

                            await unitOfWork.Commit();
                            isSuccess = true;
                        }

                        RecordAuditTrail(AuditTrail.action.ExportProductBarcode, MethodBase.GetCurrentMethod(),
                            $"{productionPlan.ProductLot}: {productionPlan.ProductLot}00001 - {productionPlan.ProductLot}{packageQty - 1:00000}");
                    }
                    catch (WrappedException ex)
                    {
                        errorLine.Add(i + 1, ex.Message);
                    }
                    catch (Exception ex)
                    {
                        errorLine.Add(i + 1, ex.Message);
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _productionPlanRepository = _mainPresenter.Resolve(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                _productRepository = _mainPresenter.Resolve(_productRepository.GetType()) as IProductRepository;
                _productBarcodeRepository = _mainPresenter.Resolve(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;

                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                    await LoadMainData();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(Messages.Information_GenerateSuccessfullyWithError, Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_GenerateSuccessfully, Utility.MessageType.Information);
            }
        }

        public async Task GenerateCartonBarcode(List<string> productLots)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            Dictionary<int, string> errorLine = new Dictionary<int, string>();
            bool isSuccess = false;

            try
            {
                for (int i = 0; i < productLots.Count; i++)
                {
                    string productLot = productLots[i];

                    try
                    {
                        //// check production plan is existed
                        var productionPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == productLot);
                        if (productionPlan == null)
                            throw new WrappedException(Messages.Error_ProductLotNotExisted);

                        //// check product packing is existed
                        var prodPacking = await _productPackingRepository.GetSingleAsync(p => p.ProductID == productionPlan.ProductID && p.Type == ProductPacking.type.Carton);
                        if (prodPacking == null)
                            throw new WrappedException(Messages.Error_ProductPackingCartonNotExisted);

                        //// Always round to larger number
                        //// after that, addition to 1 to take sample
                        decimal packageQty = Math.Ceiling((decimal)(productionPlan.PackageQuantity / prodPacking.Quantity)) + 2;
                        using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                        {
                            _productionPlanRepository = unitOfWork.Register(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                            _cartonBarcodeRepository = unitOfWork.Register(_cartonBarcodeRepository.GetType()) as ICartonBarcodeRepository;

                            //// generate product barcode based on package quantity  in production plan and product packing
                            decimal tempPackageQty = packageQty;
                            //// carton format: C001 => C999
                            for (int j = 1; j <= 999; j++)
                            {
                                string barcode = $"{productionPlan.ProductLot}C{j:000}";
                                var cartonBarcode = new CartonBarcode(barcode, productionPlan.ProductLot, CartonBarcode.status.New);
                                await _cartonBarcodeRepository.Insert(cartonBarcode);

                                tempPackageQty--;
                                if (tempPackageQty == 0)
                                    break;
                            }

                            //// carton format: CA01 => CZ99
                            if (tempPackageQty > 0)
                            {
                                for (int j = 0; j < _alphabet.Length; j++)
                                {
                                    for (int k = 1; k <= 99; k++)
                                    {
                                        string barcode = $"{productionPlan.ProductLot}C{_alphabet[j]}{k:00}";
                                        var cartonBarcode = new CartonBarcode(barcode, productionPlan.ProductLot, CartonBarcode.status.New);
                                        await _cartonBarcodeRepository.Insert(cartonBarcode);

                                        tempPackageQty--;
                                        if (tempPackageQty == 0)
                                            break;
                                    }

                                    if (tempPackageQty == 0)
                                        break;
                                }
                            }

                            //// carton format: CAA1 => CZZ9
                            if (tempPackageQty > 0)
                            {
                                for (int j = 0; j < _alphabet.Length; j++)
                                {
                                    for (int k = 0; k < _alphabet.Length; k++)
                                    {
                                        for (int l = 1; l <= 9; l++)
                                        {
                                            string barcode = $"{productionPlan.ProductLot}C{_alphabet[j]}{_alphabet[k]}{l}";
                                            var cartonBarcode = new CartonBarcode(barcode, productionPlan.ProductLot, CartonBarcode.status.New);
                                            await _cartonBarcodeRepository.Insert(cartonBarcode);

                                            tempPackageQty--;
                                            if (tempPackageQty == 0)
                                                break;
                                        }

                                        if (tempPackageQty == 0)
                                            break;
                                    }

                                    if (tempPackageQty == 0)
                                        break;
                                }
                            }

                            //// update production plan is generated carton code
                            productionPlan.IsGenCartonBarcode = true;
                            productionPlan.MaxLabelCartonBarcode = Convert.ToInt32(packageQty);
                            await _productionPlanRepository.Update(productionPlan, new string[] { "IsGenCartonBarcode", "MaxLabelCartonBarcode" }, new object[] { productionPlan.ProductLot });

                            await unitOfWork.Commit();
                            isSuccess = true;
                        }

                        RecordAuditTrail(AuditTrail.action.ExportCartonBarcode, MethodBase.GetCurrentMethod(),
                            $"{productionPlan.ProductLot}: {productionPlan.ProductLot}C001 - {productionPlan.ProductLot}C{packageQty - 1:000}");
                    }
                    catch (WrappedException ex)
                    {
                        errorLine.Add(i + 1, ex.Message);
                    }
                    catch (Exception ex)
                    {
                        errorLine.Add(i + 1, ex.Message);
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _productionPlanRepository = _mainPresenter.Resolve(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                _cartonBarcodeRepository = _mainPresenter.Resolve(_cartonBarcodeRepository.GetType()) as ICartonBarcodeRepository;

                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                    await LoadMainData();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(Messages.Information_GenerateSuccessfullyWithError, Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_GenerateSuccessfully, Utility.MessageType.Information);
            }
        }

        public async Task HoldProductionPlan(List<string> productLots)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            Dictionary<int, string> errorLine = new Dictionary<int, string>();
            bool isSuccess = false;

            try
            {
                for (int i = 0; i < productLots.Count; i++)
                {
                    string productLot = productLots[i];

                    try
                    {
                        var prodPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == productLot);
                        if (prodPlan.Status != ProductionPlan.status.New)
                            throw new WrappedException(Messages.Error_ProductionPlanStatusIsInvalid);

                        prodPlan.Status = ProductionPlan.status.Hold;
                        prodPlan.IsRecordAuditTrail = true;

                        await _productionPlanRepository.Update(prodPlan, new string[] { "Status" }, new object[] { productLot });
                        await _productionPlanRepository.Commit();

                        isSuccess = true;
                    }
                    catch (WrappedException ex)
                    {
                        errorLine.Add(i + 1, ex.Message);
                    }
                    catch (Exception ex)
                    {
                        errorLine.Add(i + 1, ex.Message);
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                    await LoadMainData();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(Messages.Information_HoldSuccessfully, Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_HoldSuccessfullyWithError, Utility.MessageType.Information);
            }
        }

        public async Task UnHoldProductionPlan(List<string> productLots)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            Dictionary<int, string> errorLine = new Dictionary<int, string>();
            bool isSuccess = false;

            try
            {
                for (int i = 0; i < productLots.Count; i++)
                {
                    string productLot = productLots[i];

                    try
                    {
                        var prodPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == productLot);
                        if (prodPlan.Status != ProductionPlan.status.Hold)
                            throw new WrappedException(Messages.Error_ProductionPlanStatusIsInvalid);

                        prodPlan.Status = ProductionPlan.status.New;
                        prodPlan.IsRecordAuditTrail = true;

                        await _productionPlanRepository.Update(prodPlan, new string[] { "Status" }, new object[] { productLot });
                        await _productionPlanRepository.Commit();

                        isSuccess = true;
                    }
                    catch (WrappedException ex)
                    {
                        errorLine.Add(i + 1, ex.Message);
                    }
                    catch (Exception ex)
                    {
                        errorLine.Add(i + 1, ex.Message);
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                    await LoadMainData();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(Messages.Information_HoldSuccessfully, Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_HoldSuccessfullyWithError, Utility.MessageType.Information);
            }
        }

        public async Task GenerateAdditionProductBarcode(string productLot, int productID, int qty)
        {
            var lastProductBarcode = await _productBarcodeRepository.GetLastAsync(p => p.Barcode, p => p.ProductLot == productLot);
            int lastNumber = int.Parse(lastProductBarcode.Barcode.Replace(productLot, String.Empty));

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _productBarcodeRepository = unitOfWork.Register(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;

                    for (int i = 0; i < qty; i++)
                    {
                        string barcode = $"{productLot}{(lastNumber + i + 1):00000}";
                        var productBarcode = new ProductBarcode(barcode, productLot, productID, ProductBarcode.status.New);
                        await _productBarcodeRepository.Insert(productBarcode);
                    }

                    await unitOfWork.Commit();
                }

                RecordAuditTrail(AuditTrail.action.ExportProductBarcode, MethodBase.GetCurrentMethod(),
                    $"{productLot}: {productLot}00001 - {productLot}{qty - 1:00000}");

                _mainPresenter.SetMessage(Messages.Information_GenerateSuccessfully, Utility.MessageType.Information);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _productBarcodeRepository = _mainPresenter.Resolve(_productBarcodeRepository.GetType()) as IProductBarcodeRepository;

                await LoadMainData();
            }
        }

        public async Task GenerateAdditionCartonBarcode(string productLot, int productID, int qty)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var lastCartonBarcode = await _cartonBarcodeRepository.GetLastAsync(p => p.Barcode, p => p.ProductLot == productLot);
            string sequenceNbr = lastCartonBarcode.Barcode.Replace($"{productLot}C", String.Empty);
            int lastNumber;
            int nextSequence = 1;
            int firstCharacterIndex = -1;
            int secondChacterIndex = -1;

            if (int.TryParse(sequenceNbr, out lastNumber)) { }
            else if (int.TryParse(sequenceNbr.Substring(1, 2), out lastNumber))
            {
                firstCharacterIndex = Array.IndexOf(_alphabet, sequenceNbr.Substring(0, 1));
                nextSequence = int.Parse(sequenceNbr.Substring(1, 2)) + 1;
                lastNumber = (firstCharacterIndex + 1) * 99 + 999;
            }
            else
            {
                firstCharacterIndex = Array.IndexOf(_alphabet, sequenceNbr.Substring(0, 1));
                secondChacterIndex = Array.IndexOf(_alphabet, sequenceNbr.Substring(1, 1));
                nextSequence = int.Parse(sequenceNbr.Substring(2, 1)) + 1;
                lastNumber = (firstCharacterIndex + 1) * (secondChacterIndex + 1) * 9 + 3573;
            }

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _productionPlanRepository = unitOfWork.Register(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                    _cartonBarcodeRepository = unitOfWork.Register(_cartonBarcodeRepository.GetType()) as ICartonBarcodeRepository;

                    //// generate product barcode based on package quantity  in production plan and product packing
                    int tempPackageQty = qty;
                    if (lastNumber <= 999)
                    {
                        //// carton format: C001 => C999
                        for (int i = lastNumber + 1; i <= 999; i++)
                        {
                            string barcode = $"{productLot}C{i:000}";
                            var cartonBarcode = new CartonBarcode(barcode, productLot, CartonBarcode.status.New);
                            await _cartonBarcodeRepository.Insert(cartonBarcode);

                            tempPackageQty--;
                            if (tempPackageQty == 0)
                                break;
                        }

                        firstCharacterIndex = 0;
                    }

                    //// carton format: CA01 => CZ99
                    if (tempPackageQty > 0 && secondChacterIndex != -1)
                    {
                        for (int i = firstCharacterIndex; i < _alphabet.Length; i++)
                        {
                            for (int j = nextSequence; j <= 99; j++)
                            {
                                string barcode = $"{productLot}C{_alphabet[i]}{j:00}";
                                var cartonBarcode = new CartonBarcode(barcode, productLot, CartonBarcode.status.New);
                                await _cartonBarcodeRepository.Insert(cartonBarcode);

                                tempPackageQty--;
                                if (tempPackageQty == 0)
                                    break;
                            }

                            nextSequence = 1;
                            if (tempPackageQty == 0)
                                break;
                        }

                        firstCharacterIndex = 0;
                        secondChacterIndex = 0;
                    }

                    //// carton format: CAA1 => CZZ9
                    if (tempPackageQty > 0)
                    {
                        for (int i = firstCharacterIndex; i < _alphabet.Length; i++)
                        {
                            for (int j = secondChacterIndex; j < _alphabet.Length; j++)
                            {
                                for (int k = nextSequence; k <= 9; k++)
                                {
                                    string barcode = $"{productLot}C{_alphabet[i]}{_alphabet[j]}{k}";
                                    var cartonBarcode = new CartonBarcode(barcode, productLot, CartonBarcode.status.New);
                                    await _cartonBarcodeRepository.Insert(cartonBarcode);

                                    tempPackageQty--;
                                    if (tempPackageQty == 0)
                                        break;
                                }

                                nextSequence = 1;
                                if (tempPackageQty == 0)
                                    break;
                            }

                            if (tempPackageQty == 0)
                                break;
                        }
                    }

                    var productionPlan = new ProductionPlan();
                    productionPlan.ProductLot = productLot;
                    productionPlan.MaxLabelCartonBarcode = lastNumber + qty;
                    await _productionPlanRepository.Update(productionPlan, new string[] { "MaxLabelCartonBarcode" }, new object[] { productionPlan.ProductLot });

                    await unitOfWork.Commit();
                }

                RecordAuditTrail(AuditTrail.action.ExportCartonBarcode, MethodBase.GetCurrentMethod(),
                    $"{productLot}: {productLot}C001 - {productLot}C{qty - 1:000}");

                _mainPresenter.SetMessage(Messages.Information_GenerateSuccessfully, Utility.MessageType.Information);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _productionPlanRepository = _mainPresenter.Resolve(_productionPlanRepository.GetType()) as IProductionPlanRepository;
                _cartonBarcodeRepository = _mainPresenter.Resolve(_cartonBarcodeRepository.GetType()) as ICartonBarcodeRepository;

                await LoadMainData();
            }
        }

        public async Task<IList<string>> GetCartonLabelToPrint(ProductionPlan productionPlan, int qty, string note)
        {
            try
            {
                var taskGetProductPacking = _productPackingRepository.GetSingleAsync(p => p.ProductID == productionPlan.ProductID && p.Type == ProductPacking.type.Carton);
                var taskGetCartonBarcode = _cartonBarcodeRepository.GetCartonBarcodeNotYetPrinted(productionPlan.ProductLot, productionPlan.CurrentLabelCartonBarcode ?? 0);

                await Task.WhenAll(taskGetProductPacking, taskGetCartonBarcode);

                var productPacking = taskGetProductPacking.Result;
                var cartonBarcodes = taskGetCartonBarcode.Result;

                string template;
                using (var sr = new StreamReader(Path.Combine(Utility.AppPath, @"Templates\CartonLabelTemplate.txt")))
                {
                    template = await sr.ReadToEndAsync();

                    char separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
                    string format = "{0:N0}";
                    if (productionPlan.PackageSize.ToString().Contains(separator))
                    {
                        string tmp = productionPlan.PackageSize.ToString().Split(separator)[1];
                        while (tmp.EndsWith("0"))
                            tmp = tmp.Substring(0, tmp.Length - 1);
                        if (tmp.Length == 1)
                            format = "{0:N1}";
                        else if (tmp.Length == 2)
                            format = "{0:N2}";
                        else if (tmp.Length == 3)
                            format = "{0:N3}";
                        else if (tmp.Length == 4)
                            format = "{0:N4}";
                    }

                    template = template.Replace("{FontScale}", productionPlan.ProductDescription.Length <= 25 ? "100" : (Math.Round(((decimal)25 / productionPlan.ProductDescription.Length) * 100)).ToString());
                    template = template.Replace("{ProductName}", productionPlan.ProductDescription);
                    template = template.Replace("{ProductLot}", productionPlan.ProductLot + (String.IsNullOrWhiteSpace(productionPlan.SupplierLot)?String.Empty: ($" / {productionPlan.SupplierLot}")));
                    template = template.Replace("{PackageSize}", String.Format(format, productionPlan.PackageSize));
                    template = template.Replace("{UOM}", productionPlan.UOM);
                    template = template.Replace("{ProductPacking}", $"{productPacking.Quantity:N0}");
                    template = template.Replace("{ManufacturingDate}", $"{productionPlan.ManufacturingDate:dd/MM/yyyy}");
                    template = template.Replace("{ExpiryDate}", $"{productionPlan.ExpiryDate:dd/MM/yyyy}");
                }

                var list = new List<string>();
                int temp = qty;
                foreach (var barcode in cartonBarcodes)
                {
                    if (temp == 0)
                        break;

                    string text = template;
                    text = text.Replace("{CartonBarcode}", barcode.Barcode);

                    list.Add(text);
                    temp--;
                }

                productionPlan.CurrentLabelCartonBarcode = (productionPlan.CurrentLabelCartonBarcode ?? 0) + qty;

                await _productionPlanRepository.Update(productionPlan, new string[] { "CurrentLabelCartonBarcode" }, new object[] { productionPlan.ProductLot });
                await _productionPlanRepository.Commit();

                RecordAuditTrail(AuditTrail.action.Print, MethodBase.GetCurrentMethod(), note);

                return list;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                await LoadMainData();
            }
        }

        public async Task UpdateProductionPlan(string productLot, DateTime? planDate, string device, string poNumber, string supplierLot, string note)
        {
            try
            {
                var productionPlan = new ProductionPlan();
                productionPlan.ProductLot = productLot;
                productionPlan.PlanDate = planDate;
                productionPlan.Device = device;
                productionPlan.PONumber = poNumber;
                productionPlan.SupplierLot = supplierLot;

                await _productionPlanRepository.Update(productionPlan, new string[] { "PlanDate", "Device", "PONumber", "SupplierLot" }, new object[] { productLot });
                await _productionPlanRepository.Commit();

                productionPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == productLot);

                RecordAuditTrail(productionPlan.ToXML(), AuditTrail.action.Update, MethodBase.GetCurrentMethod(), note);

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                await LoadMainData();
            }
        }

        public async Task UpdateProductionPlanStatus(string productLot, string status)
        {
            try
            {
                var productionPlan = new ProductionPlan();
                productionPlan.ProductLot = productLot;
                productionPlan.Status = status;

                await _productionPlanRepository.Update(productionPlan, new string[] { "Status" }, new object[] { productLot });
                await _productionPlanRepository.Commit();

                productionPlan = await _productionPlanRepository.GetSingleAsync(p => p.ProductLot == productLot);

                RecordAuditTrail(productionPlan.ToXML(), AuditTrail.action.Update, MethodBase.GetCurrentMethod());

                await LoadMainData();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                await LoadMainData();
            }
        }

        /// <summary>
        /// Save product packing to database, 2 cases:
        ///     Case 1: insert new product
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var deletedProdPlans = _mainView.DeletedProductionPlans;

            bool isSuccess = false;
            try
            {
                //// foreach deleted product packing and delete it
                foreach (var deletedProdPlan in deletedProdPlans)
                {
                    deletedProdPlan.IsRecordAuditTrail = true;
                    await _productionPlanRepository.Delete(new object[] { deletedProdPlan.ProductLot });
                }

                await _productionPlanRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadMainData();
                }
            }
        }
    }
}
