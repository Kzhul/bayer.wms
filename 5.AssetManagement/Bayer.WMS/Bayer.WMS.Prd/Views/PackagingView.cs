﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs;
using Bayer.WMS.Prd.Presenters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Prd.ViewModels;
using System.Reflection;
using Bayer.WMS.CustomControls;
using System.IO.Ports;
using System.Threading;
using System.Media;
using System.IO;
using Microsoft.Practices.Unity;

namespace Bayer.WMS.Prd.Views
{
    public partial class PackagingView : BaseForm, IPackagingView
    {
        private bool _isProcess = false;
        private Queue<string> _queue;
        private SerialPort _serialPort;
        private int _confirmType;
        private string _currentBarcode;

        public PackagingView()
        {
            InitializeComponent();
            _queue = new Queue<string>();
            //// set hint for next step
            lblStepHints.Text = Messages.Information_Packaging_ScanEmployeeStep;
        }

        private void PackagingView_Load(object sender, EventArgs e)
        {
            txtBarcode.Focus();
        }

        private void PackagingView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_serialPort != null && _serialPort.IsOpen)
                _serialPort.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string message = String.Empty;
            btnOK.Enabled = false;
            try
            {
                if (!String.IsNullOrWhiteSpace(txtBarcode.Text))
                {
                    if (!IsParallel)
                    {
                        QueueProcess(txtBarcode.Text);
                    }
                    else
                    {
                        string[] temp = txtBarcode.Text.Split('_');
                        string barcode = txtBarcode.Text.Remove(0, $"{temp[0]}_".Length);
                        var view = Application.OpenForms[Sitemap.ClassName + temp[0]] as PackagingView;
                        view.QueueProcess(barcode);
                    }
                }
            }
            finally
            {
                btnOK.Enabled = true;
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string data = _serialPort.ReadExisting();
            QueueProcess(data);
        }

        private void SetProductLotDataSource()
        {
            txtPackagingStartTime.DataBindings.Clear();
            txtPackagingEndTime.DataBindings.Clear();
            txtCartonWeight.DataBindings.Clear();
            txtProductSampleQty.DataBindings.Clear();
            txtCartonSampleQty.DataBindings.Clear();

            txtPackagingStartTime.DataBindings.Add("Text", ProducionPlan, "PackagingStartTime", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "dd.MM.yyyy - HH:mm");
            txtPackagingEndTime.DataBindings.Add("Text", ProducionPlan, "PackagingEndTime", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "dd.MM.yyyy - HH:mm");
            txtCartonWeight.DataBindings.Add("Text", ProducionPlan, "CartonWeight", true, DataSourceUpdateMode.OnPropertyChanged);
            txtProductSampleQty.DataBindings.Add("Text", ProducionPlan, "StrTookProductSampleProgress", true, DataSourceUpdateMode.OnPropertyChanged);
            txtCartonSampleQty.DataBindings.Add("Text", ProducionPlan, "StrTookCartonSampleProgress", true, DataSourceUpdateMode.OnPropertyChanged);

            //// display quantity information based on product packing type
            if (_productionPlan.ProductPackingType == Product.packingType.Carton)
            {
                label3.Visible = true;
                txtCartonSampleQty.Visible = true;
                lblCarton_PackingQty.Visible = true;
                lblCarton_PackingQty_Label.Visible = true;
                lblPallet_CartonQty.Visible = true;
                lblPallet_CartonQty_Label.Visible = true;
                lblPallet_PackingQty.Visible = false;
                lblPallet_PackingQty_Label.Visible = false;
            }
            else
            {
                label3.Visible = false;
                txtCartonSampleQty.Visible = false;
                lblCarton_PackingQty.Visible = false;
                lblCarton_PackingQty_Label.Visible = false;
                lblPallet_CartonQty.Visible = false;
                lblPallet_CartonQty_Label.Visible = false;
                lblPallet_PackingQty.Visible = true;
                lblPallet_PackingQty_Label.Visible = true;
            }

            if (_productionPlan.Status == ProductionPlan.status.Completed)
            {
                lblStepHints.Text = "Lô thành phẩm đã đóng gói hoàn tất";
                BackColor = Color.FromArgb(192, 255, 192);
                lblStepHints.BackColor = Color.LimeGreen;
            }
            else
                BackColor = SystemColors.Control;
        }

        private void SetPackagingDataSource()
        {
            lblProductLot.DataBindings.Clear();
            lblProductDescription.DataBindings.Clear();
            lblCarton_PackingQty.DataBindings.Clear();
            lblPallet_CartonQty.DataBindings.Clear();
            lblPallet_PackingQty.DataBindings.Clear();
            lblPackingQty.DataBindings.Clear();
            lblCancelLabel.DataBindings.Clear();
            lblDestroyLabel.DataBindings.Clear();

            lblProductLot.DataBindings.Add("Text", PackagingVM, "ProductLot", true, DataSourceUpdateMode.OnPropertyChanged);
            lblProductDescription.DataBindings.Add("Text", PackagingVM, "ProductDescription", true, DataSourceUpdateMode.OnPropertyChanged);
            lblCarton_PackingQty.DataBindings.Add("Text", PackagingVM, "PackingQtyInCarton", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
            lblPallet_CartonQty.DataBindings.Add("Text", PackagingVM, "CartonQtyOnPallet", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
            lblPallet_PackingQty.DataBindings.Add("Text", PackagingVM, "PackingQtyOnPallet", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
            lblPackingQty.DataBindings.Add("Text", PackagingVM, "ActualPackingQtyOnPlan", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
            lblCancelLabel.DataBindings.Add("Text", PackagingVM, "CancelLabel", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
            lblDestroyLabel.DataBindings.Add("Text", PackagingVM, "DestroyLabel", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");

            //lblQuantityOnPallet.DataBindings.Add("Text", PackagingVM, "PackingQtyOnPallet", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
        }

        private async Task Dequeue()
        {
            while (_queue.Count > 0)
            {
                _isProcess = true;

                string queue = _queue.Dequeue();
                await ProcessBarcode(queue);
            }

            _isProcess = false;
        }

        public async Task QueueProcess(string data)
        {
            _queue.Enqueue(data);

            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    await Dequeue();
            }
        }

        public override bool GetConfirm(string message)
        {
            return base.GetConfirm(message);
        }

        public void GetConfirmVoid(string message, int type)
        {
            _confirmType = type;
            base.GetConfirmVoid(message, InvokeConfirmOK, InvokeConfirmCancel);
        }

        public bool ProcessForceDestroyEnd(out int userID, out string note)
        {
            userID = 0;
            note = String.Empty;
            using (var view = new PackagingDestroyForceEndView())
            {
                if (view.ShowDialog() != DialogResult.OK)
                    return false;

                userID = view.UserID;
                note = view.Note;

                return true;
            }
        }

        public void ShowErrorReasonBox(int type)
        {
            _confirmType = type;
            var view = new PackagingErrorReason();
            view.InvokeOK = InvokeReasonOK;
            view.InvokeCancel = InvokeReasonCancel;
            view.Show(this);
        }

        public void ShowQuantityCheck(DataTable dt)
        {
            var view = new PackagingQuantityCheck();
            view.Data = dt;
            view.Show(this);
        }

        public override void SetMessage(string message, Utility.MessageType messageType, string parent = "")
        {
            base.SetMessage(message, messageType, this.Name);

            var thread = new Thread(delegate ()
            {
                string path = Path.Combine(Application.StartupPath, @"Resources\Error_Sound.wav");
                using (var player = new SoundPlayer(path))
                {
                    player.PlaySync();
                }
            });

            thread.Start();
        }

        public void CreateCOM(string portName, int baudRate, string parity, int dataBits, string stopBits)
        {
            Parity p = Parity.None;
            switch (parity)
            {
                case "Even":
                    p = Parity.Even;
                    break;
                case "Mark":
                    p = Parity.Mark;
                    break;
                case "Odd":
                    p = Parity.Odd;
                    break;
                case "Space":
                    p = Parity.Space;
                    break;
                default:
                    break;
            }

            StopBits sb = StopBits.None;
            switch (stopBits)
            {
                case "1":
                    sb = StopBits.One;
                    break;
                case "2":
                    sb = StopBits.Two;
                    break;
                case "1.5":
                    sb = StopBits.OnePointFive;
                    break;
                default:
                    break;
            }

            _serialPort = new SerialPort(portName, baudRate, p, dataBits, sb);
            _serialPort.ReadTimeout = 1000;
            _serialPort.WriteTimeout = 1000;
            _serialPort.DataReceived += _serialPort_DataReceived;
            _serialPort.Open();
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            var packagingPresenter = _presenter as IPackagingPresenter;
        }

        public async void InvokeConfirmOK(CustomConfirmBox confirmBox)
        {
            var presenter = _presenter as IPackagingPresenter;

            presenter.ConfirmType = _confirmType;
            presenter.IsGetConfirm = true;
            presenter.IsConfirm = true;

            confirmBox.Close();

            if (_confirmType != 5)
                await (_presenter as IPackagingPresenter).ProcessBarcode(_currentBarcode);
        }

        public async void InvokeConfirmCancel(CustomConfirmBox confirmBox)
        {
            var presenter = _presenter as IPackagingPresenter;

            presenter.IsConfirm = false;

            confirmBox.Close();

            if (_confirmType == 5)
                await (_presenter as IPackagingPresenter).ProcessBarcode(_currentBarcode);
        }

        public async void InvokeReasonOK(PackagingErrorReason errorReason)
        {
            var presenter = _presenter as IPackagingPresenter;

            presenter.ConfirmType = _confirmType;
            presenter.IsGetConfirm = true;
            presenter.IsConfirm = true;
            presenter.Reason = errorReason.Reason;

            errorReason.Close();

            await (_presenter as IPackagingPresenter).ProcessBarcode(_currentBarcode);
        }

        public void InvokeReasonCancel(PackagingErrorReason errorReason)
        {
            var presenter = _presenter as IPackagingPresenter;

            presenter.IsConfirm = false;
            errorReason.Close();
        }

        public async Task ProcessBarcode(string barcode)
        {
            int n;
            bool isNumeric = int.TryParse(barcode, out n);
            var presenter = _presenter as IPackagingPresenter;
            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);

                var messageBox = Application.OpenForms[$"{Name}_CustomMessageBox"] as CustomMessageBox;
                if (messageBox != null && messageBox.Visible)
                {
                    if (barcode.ToLower() == "dong_y")
                        messageBox.Close();

                    return;
                }

                var confirmBox = Application.OpenForms[$"{Name}_CustomConfirmBox"] as CustomConfirmBox;
                if (confirmBox != null && confirmBox.Visible)
                {
                    if (barcode.ToLower() == "dong_y")
                        confirmBox.PressOk();
                    else if (barcode.ToLower() == "khong")
                        confirmBox.PressCancel();

                    return;
                }

                var errorReason = Application.OpenForms[$"{Name}_PackagingErrorReason"] as PackagingErrorReason;
                if (errorReason != null && errorReason.Visible)
                {
                    if (barcode.ToLower() == "dong_y")
                        errorReason.PressDestroy();
                    else if (barcode.ToLower() == "khong")
                        errorReason.PressCancel();

                    return;
                }

                var qtyCheck = Application.OpenForms[$"{Name}_PackagingQuantityCheck"] as PackagingQuantityCheck;
                if (qtyCheck != null && qtyCheck.Visible)
                {
                    if (barcode.ToLower() == "dong_y")
                        qtyCheck.Close();

                    return;
                }

                if (!isNumeric)
                {
                    txtBarcode.Text = barcode;
                    barcode = await (_presenter as IPackagingPresenter).ProcessBarcode(barcode);
                    //if (ProductLotPackagingType != 2)
                    //{
                    //    txtBarcode.Text = barcode;
                    //    barcode = await (_presenter as IPackagingPresenter).ProcessBarcode(barcode);
                    //}
                    //else if (barcode.ToLower() == "dong_y" && ProductLotPackagingType == 2)
                    //{
                    //    txtBarcode.Text = barcode;
                    //    barcode = await (_presenter as IPackagingPresenter).ProcessBarcode(barcode);
                    //}
                }
                else
                {
                    Quantity = Utility.IntParse(txtBarcode.Text);
                    lblStepHints.Text = Quantity.ToString() + ". Vui lòng quét mã Đồng ý";
                }

            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error, Name);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error, Name);
            }
            finally
            {
                _currentBarcode = presenter.Barcode;

                var _mainPresenter = presenter.GetMainPresenter();
                _presenter = _mainPresenter.Resolve(_presenter.GetType(),
                    new ParameterOverride("view", this),
                    new ParameterOverride("mainPresenter", _mainPresenter)) as IBasePresenter;

                if (!isNumeric)
                {
                    txtBarcode.Focus();
                    txtBarcode.SelectAll();
                }
            }
        }
        
        public string Executor { set { txtExecutor.Text = value; } }

        public string StepHint
        {
            get { return lblStepHints.Text; }
            set { lblStepHints.Text = value; }
        }

        public User CurrentUser { get; set; }

        public Device CurrentDevice { get; set; }

        public Product CurrentProduct { get; set; }

        private Pallet _currentPallet;

        public Pallet CurrentPallet
        {
            get { return _currentPallet; }
            set
            {
                _currentPallet = value;
                txtPallet.Text = value == null ? String.Empty : value.PalletCode;
            }
        }

        private CartonBarcode _currentCarton;

        public CartonBarcode CurrentCartonBarcode
        {
            get { return _currentCarton; }
            set
            {
                _currentCarton = value;
                txtCarton.Text = value == null ? String.Empty : value.Barcode;
            }
        }

        public ProductBarcode CurrentProductBarcode { get; set; }

        public BarcodeBank CurrentBarcode { get; set; }

        public IList<ProductBarcode> ProductBarcodes { get; set; }

        public IList<CartonBarcode> CartonBarcodes { get; set; }

        public IList<PackagingLog> PackagingLogs { get; set; }

        public IList<PackagingSample> PackagingSamples { get; set; }

        private ProductionPlan _productionPlan;

        public ProductionPlan ProducionPlan
        {
            get { return _productionPlan; }
            set
            {
                _productionPlan = value;

                txtPackagingStartTime.DataBindings.Clear();
                txtPackagingEndTime.DataBindings.Clear();
                txtCartonWeight.DataBindings.Clear();
                txtProductSampleQty.DataBindings.Clear();
                txtCartonSampleQty.DataBindings.Clear();

                txtPackagingStartTime.DataBindings.Add("Text", ProducionPlan, "PackagingStartTime", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "dd.MM.yyyy - HH:mm");
                txtPackagingEndTime.DataBindings.Add("Text", ProducionPlan, "PackagingEndTime", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "dd.MM.yyyy - HH:mm");
                txtCartonWeight.DataBindings.Add("Text", ProducionPlan, "CartonWeight", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProductSampleQty.DataBindings.Add("Text", ProducionPlan, "StrTookProductSampleProgress", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCartonSampleQty.DataBindings.Add("Text", ProducionPlan, "StrTookCartonSampleProgress", true, DataSourceUpdateMode.OnPropertyChanged);

                //// display quantity information based on product packing type
                if (_productionPlan.ProductPackingType == Product.packingType.Carton)
                {
                    label3.Visible = true;
                    txtCartonSampleQty.Visible = true;
                    lblCarton_PackingQty.Visible = true;
                    lblCarton_PackingQty_Label.Visible = true;
                    lblPallet_CartonQty.Visible = true;
                    lblPallet_CartonQty_Label.Visible = true;
                    lblPallet_PackingQty.Visible = false;
                    lblPallet_PackingQty_Label.Visible = false;
                }
                else
                {
                    label3.Visible = false;
                    txtCartonSampleQty.Visible = false;
                    lblCarton_PackingQty.Visible = false;
                    lblCarton_PackingQty_Label.Visible = false;
                    lblPallet_CartonQty.Visible = false;
                    lblPallet_CartonQty_Label.Visible = false;
                    lblPallet_PackingQty.Visible = true;
                    lblPallet_PackingQty_Label.Visible = true;
                }



                if (_productionPlan.Status == ProductionPlan.status.Completed)
                {
                    lblStepHints.Text = "Lô thành phẩm đã đóng gói hoàn tất";
                    BackColor = Color.FromArgb(192, 255, 192);
                    lblStepHints.BackColor = Color.LimeGreen;
                }
                else
                    BackColor = SystemColors.Control;
            }
        }

        private PackagingViewModel _packagingVM;

        public PackagingViewModel PackagingVM
        {
            get { return _packagingVM; }
            set
            {
                _packagingVM = value;

                lblProductLot.DataBindings.Clear();
                lblProductDescription.DataBindings.Clear();
                lblCarton_PackingQty.DataBindings.Clear();
                lblPallet_CartonQty.DataBindings.Clear();
                lblPallet_PackingQty.DataBindings.Clear();
                lblPackingQty.DataBindings.Clear();
                lblCancelLabel.DataBindings.Clear();
                lblDestroyLabel.DataBindings.Clear();
                lblQuantityOnPallet.DataBindings.Clear();
                lblUOMPallet.DataBindings.Clear();
                lblUOMProductLot.DataBindings.Clear();

                lblProductLot.DataBindings.Add("Text", PackagingVM, "ProductLot", true, DataSourceUpdateMode.OnPropertyChanged);
                lblProductDescription.DataBindings.Add("Text", PackagingVM, "ProductDescription", true, DataSourceUpdateMode.OnPropertyChanged);
                lblCarton_PackingQty.DataBindings.Add("Text", PackagingVM, "PackingQtyInCarton", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
                lblPallet_CartonQty.DataBindings.Add("Text", PackagingVM, "CartonQtyOnPallet", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
                lblPallet_PackingQty.DataBindings.Add("Text", PackagingVM, "PackingQtyOnPallet", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
                lblPackingQty.DataBindings.Add("Text", PackagingVM, "ActualPackingQtyOnPlan", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
                lblCancelLabel.DataBindings.Add("Text", PackagingVM, "CancelLabel", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
                lblDestroyLabel.DataBindings.Add("Text", PackagingVM, "DestroyLabel", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
                lblQuantityOnPallet.DataBindings.Add("Text", PackagingVM, "ActualPerPallet", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty, "N0");
                lblUOMPallet.DataBindings.Add("Text", PackagingVM, "UOMPallet", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty);
                lblUOMProductLot.DataBindings.Add("Text", PackagingVM, "UOMProductLot", true, DataSourceUpdateMode.OnPropertyChanged, String.Empty);
            }
        }

        public bool IsParallel { get; set; }

        public decimal Quantity { get; set; }

        public int ProductLotPackagingType { get; set; }

        public string PackagingLine
        {
            get { return txtPackagingLine.Text; }
            set { txtPackagingLine.Text = value; }
        }

        private int _state;

        public int State
        {
            get { return _state; }
            set
            {
                _state = value;

                if (_state == 1)
                    pnlDestroy.BringToFront();
                else
                    pnlPackaging.BringToFront();

                lblStepHints.BackColor = _state == 1 ? Color.Red : Color.LimeGreen;
            }
        }

        public string PreStepHint { get; set; }
    }
}
