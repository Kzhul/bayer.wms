﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class ProductionPlanExportView : BaseForm
    {
        public ProductionPlanExportView()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() != DialogResult.OK)
                    return;

                txtFolderName.Text = fbd.SelectedPath;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(txtFolderName.Text))
                DialogResult = DialogResult.OK;
            else
                MessageBox.Show(Messages.Error_ExportFolderNotSelected, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public bool IsExportCarton
        {
            get { return chkCarton.Checked; }
        }

        public bool IsExportProduct
        {
            get { return chkProduct.Checked; }
        }

        public string FolderName
        {
            get { return txtFolderName.Text; }
        }
    }
}
