﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class GenAdditionBarcode : BaseForm
    {
        public GenAdditionBarcode()
        {
            InitializeComponent();
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public string ProductLot
        {
            set { txtProductLot.Text = value; }
        }

        public string ProductPackingType
        {
            set
            {
                if (value != Product.packingType.Carton)
                    rdbCarton.Enabled = false;
                rdbProduct.Checked = true;
            }
            //get
            //{
            //    if (rdbProduct.Checked)
            //        return "P";
            //    else
            //        return Product.packingType.Carton;
            //}
        }

        public int Qty => (int)nudQty.Value;

        public string BarcodeType => rdbProduct.Checked ? "P" : "C";
    }
}
