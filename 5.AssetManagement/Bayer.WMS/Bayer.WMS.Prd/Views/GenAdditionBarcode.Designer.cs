﻿namespace Bayer.WMS.Prd.Views
{
    partial class GenAdditionBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnGen = new System.Windows.Forms.Button();
            this.nudQty = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.rdbProduct = new System.Windows.Forms.RadioButton();
            this.rdbCarton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProductLot = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudQty)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(159, 94);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Thoát";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnGen
            // 
            this.btnGen.BackColor = System.Drawing.Color.Green;
            this.btnGen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGen.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnGen.Location = new System.Drawing.Point(78, 94);
            this.btnGen.Name = "btnGen";
            this.btnGen.Size = new System.Drawing.Size(75, 30);
            this.btnGen.TabIndex = 3;
            this.btnGen.Text = "Tạo";
            this.btnGen.UseVisualStyleBackColor = false;
            this.btnGen.Click += new System.EventHandler(this.btnGen_Click);
            // 
            // nudQty
            // 
            this.nudQty.Location = new System.Drawing.Point(102, 66);
            this.nudQty.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudQty.Name = "nudQty";
            this.nudQty.Size = new System.Drawing.Size(90, 22);
            this.nudQty.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 68);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 16);
            this.label5.TabIndex = 41;
            this.label5.Text = "S.lượng:";
            // 
            // rdbProduct
            // 
            this.rdbProduct.AutoSize = true;
            this.rdbProduct.Location = new System.Drawing.Point(191, 40);
            this.rdbProduct.Name = "rdbProduct";
            this.rdbProduct.Size = new System.Drawing.Size(101, 20);
            this.rdbProduct.TabIndex = 1;
            this.rdbProduct.TabStop = true;
            this.rdbProduct.Text = "Thành phẩm";
            this.rdbProduct.UseVisualStyleBackColor = true;
            // 
            // rdbCarton
            // 
            this.rdbCarton.AutoSize = true;
            this.rdbCarton.Location = new System.Drawing.Point(102, 40);
            this.rdbCarton.Name = "rdbCarton";
            this.rdbCarton.Size = new System.Drawing.Size(64, 20);
            this.rdbCarton.TabIndex = 0;
            this.rdbCarton.TabStop = true;
            this.rdbCarton.Text = "Thùng";
            this.rdbCarton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 44;
            this.label1.Text = "Loại mã QR:";
            // 
            // txtProductLot
            // 
            this.txtProductLot.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtProductLot.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtProductLot.Location = new System.Drawing.Point(102, 12);
            this.txtProductLot.Name = "txtProductLot";
            this.txtProductLot.ReadOnly = true;
            this.txtProductLot.Size = new System.Drawing.Size(150, 22);
            this.txtProductLot.TabIndex = 45;
            this.txtProductLot.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 46;
            this.label2.Text = "Mã lô:";
            // 
            // GenAdditionBarcode
            // 
            this.AcceptButton = this.btnGen;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(313, 134);
            this.Controls.Add(this.txtProductLot);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rdbCarton);
            this.Controls.Add(this.rdbProduct);
            this.Controls.Add(this.nudQty);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnGen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "GenAdditionBarcode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tạo mã QR bổ sung";
            ((System.ComponentModel.ISupportInitialize)(this.nudQty)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnGen;
        private System.Windows.Forms.NumericUpDown nudQty;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rdbProduct;
        private System.Windows.Forms.RadioButton rdbCarton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProductLot;
        private System.Windows.Forms.Label label2;
    }
}