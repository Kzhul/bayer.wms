﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class ProductionPlanImportView : BaseForm
    {
        public ProductionPlanImportView()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Filter = "Excel Files|*.xls;*.xlsx";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    txtFileName.Text = ofd.FileName;
                }
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(txtFileName.Text))
                DialogResult = DialogResult.OK;
            else
                MessageBox.Show(Messages.Error_ImportFileNotSelected, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public DateTime FromDate
        {
            get { return dtpFromDate.Value; }
        }

        public DateTime ToDate
        {
            get { return dtpToDate.Value; }
        }

        public string FileName
        {
            get { return txtFileName.Text; }
        }
    }
}
