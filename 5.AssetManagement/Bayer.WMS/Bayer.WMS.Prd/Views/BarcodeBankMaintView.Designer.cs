﻿namespace Bayer.WMS.Prd.Views
{
    partial class BarcodeBankMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsPallet = new System.Windows.Forms.BindingSource(this.components);
            this.btnGenBarcode = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTotalNotYetUsed = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nudExportQty = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTotalExported = new System.Windows.Forms.Label();
            this.lblNotYetExport = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbProductCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtgTotal = new System.Windows.Forms.DataGridView();
            this.colProductLot1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCartonOddQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExportDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExportQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotal)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGenBarcode
            // 
            this.btnGenBarcode.BackColor = System.Drawing.Color.Green;
            this.btnGenBarcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenBarcode.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnGenBarcode.Location = new System.Drawing.Point(896, 9);
            this.btnGenBarcode.Name = "btnGenBarcode";
            this.btnGenBarcode.Size = new System.Drawing.Size(110, 30);
            this.btnGenBarcode.TabIndex = 1;
            this.btnGenBarcode.TabStop = false;
            this.btnGenBarcode.Text = "Tạo Barcode";
            this.btnGenBarcode.UseVisualStyleBackColor = false;
            this.btnGenBarcode.Click += new System.EventHandler(this.btnGenBarcode_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Số lượng có thể sử dụng:";
            // 
            // lblTotalNotYetUsed
            // 
            this.lblTotalNotYetUsed.AutoSize = true;
            this.lblTotalNotYetUsed.Location = new System.Drawing.Point(171, 9);
            this.lblTotalNotYetUsed.Name = "lblTotalNotYetUsed";
            this.lblTotalNotYetUsed.Size = new System.Drawing.Size(15, 16);
            this.lblTotalNotYetUsed.TabIndex = 15;
            this.lblTotalNotYetUsed.Text = "0";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(493, 93);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 30);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Thoát";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(387, 93);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(100, 30);
            this.btnExport.TabIndex = 2;
            this.btnExport.Text = "Xuất excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(255, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "Số lượng xuất excel:";
            // 
            // nudExportQty
            // 
            this.nudExportQty.Location = new System.Drawing.Point(387, 7);
            this.nudExportQty.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudExportQty.Name = "nudExportQty";
            this.nudExportQty.Size = new System.Drawing.Size(100, 22);
            this.nudExportQty.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 16);
            this.label3.TabIndex = 19;
            this.label3.Text = "Số lượng đã xuất excel:";
            // 
            // lblTotalExported
            // 
            this.lblTotalExported.AutoSize = true;
            this.lblTotalExported.Location = new System.Drawing.Point(171, 30);
            this.lblTotalExported.Name = "lblTotalExported";
            this.lblTotalExported.Size = new System.Drawing.Size(15, 16);
            this.lblTotalExported.TabIndex = 20;
            this.lblTotalExported.Text = "0";
            // 
            // lblNotYetExport
            // 
            this.lblNotYetExport.AutoSize = true;
            this.lblNotYetExport.Location = new System.Drawing.Point(171, 52);
            this.lblNotYetExport.Name = "lblNotYetExport";
            this.lblNotYetExport.Size = new System.Drawing.Size(15, 16);
            this.lblNotYetExport.TabIndex = 22;
            this.lblNotYetExport.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "Số lượng chưa xuất excel:";
            // 
            // cmbProductCode
            // 
            this.cmbProductCode.Columns = null;
            this.cmbProductCode.DropDownHeight = 1;
            this.cmbProductCode.DropDownWidth = 500;
            this.cmbProductCode.FormattingEnabled = true;
            this.cmbProductCode.IntegralHeight = false;
            this.cmbProductCode.Location = new System.Drawing.Point(387, 35);
            this.cmbProductCode.MaxLength = 255;
            this.cmbProductCode.Name = "cmbProductCode";
            this.cmbProductCode.PageSize = 0;
            this.cmbProductCode.PresenterInfo = null;
            this.cmbProductCode.Size = new System.Drawing.Size(400, 24);
            this.cmbProductCode.Source = null;
            this.cmbProductCode.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(255, 38);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 16);
            this.label4.TabIndex = 24;
            this.label4.Text = "Mã sản phẩm:";
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(387, 65);
            this.txtProductDescription.MaxLength = 255;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(400, 22);
            this.txtProductDescription.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(255, 68);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 16);
            this.label6.TabIndex = 26;
            this.label6.Text = "Mô tả:";
            // 
            // dtgTotal
            // 
            this.dtgTotal.AllowUserToAddRows = false;
            this.dtgTotal.AllowUserToDeleteRows = false;
            this.dtgTotal.AllowUserToOrderColumns = true;
            this.dtgTotal.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgTotal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgTotal.AutoGenerateColumns = false;
            this.dtgTotal.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTotal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductLot1,
            this.colQuantity1,
            this.colCartonOddQuantity,
            this.colTo,
            this.colExportDate,
            this.colDeliveryDate,
            this.colCompanyCode,
            this.colCompanyName,
            this.colProductDescription,
            this.colFileName});
            this.dtgTotal.DataSource = this.bdsPallet;
            this.dtgTotal.GridColor = System.Drawing.SystemColors.Control;
            this.dtgTotal.Location = new System.Drawing.Point(12, 129);
            this.dtgTotal.Name = "dtgTotal";
            this.dtgTotal.Size = new System.Drawing.Size(1140, 371);
            this.dtgTotal.TabIndex = 27;
            // 
            // colProductLot1
            // 
            this.colProductLot1.DataPropertyName = "Year";
            this.colProductLot1.HeaderText = "Năm";
            this.colProductLot1.Name = "colProductLot1";
            this.colProductLot1.ReadOnly = true;
            this.colProductLot1.Width = 50;
            // 
            // colQuantity1
            // 
            this.colQuantity1.DataPropertyName = "Quantity";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colQuantity1.DefaultCellStyle = dataGridViewCellStyle2;
            this.colQuantity1.HeaderText = "Số Lượng";
            this.colQuantity1.Name = "colQuantity1";
            this.colQuantity1.ReadOnly = true;
            this.colQuantity1.Width = 70;
            // 
            // colCartonOddQuantity
            // 
            this.colCartonOddQuantity.DataPropertyName = "SequenceFrom";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colCartonOddQuantity.DefaultCellStyle = dataGridViewCellStyle3;
            this.colCartonOddQuantity.HeaderText = "Từ";
            this.colCartonOddQuantity.Name = "colCartonOddQuantity";
            this.colCartonOddQuantity.ReadOnly = true;
            // 
            // colTo
            // 
            this.colTo.DataPropertyName = "SequenceTo";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colTo.DefaultCellStyle = dataGridViewCellStyle4;
            this.colTo.HeaderText = "Đến";
            this.colTo.Name = "colTo";
            this.colTo.ReadOnly = true;
            // 
            // colExportDate
            // 
            this.colExportDate.DataPropertyName = "StrExportDate";
            this.colExportDate.HeaderText = "Ngày Xuất";
            this.colExportDate.Name = "colExportDate";
            this.colExportDate.ReadOnly = true;
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.DataPropertyName = "UserExport";
            this.colDeliveryDate.HeaderText = "Người xuất";
            this.colDeliveryDate.Name = "colDeliveryDate";
            this.colDeliveryDate.ReadOnly = true;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.DataPropertyName = "ProductCode";
            this.colCompanyCode.HeaderText = "Mã Sản Phẩm";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.ReadOnly = true;
            // 
            // colCompanyName
            // 
            this.colCompanyName.DataPropertyName = "ProductDescription";
            this.colCompanyName.HeaderText = "Tên Sản Phẩm";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.ReadOnly = true;
            this.colCompanyName.Width = 200;
            // 
            // colProductDescription
            // 
            this.colProductDescription.DataPropertyName = "Description";
            this.colProductDescription.HeaderText = "Ghi chú";
            this.colProductDescription.Name = "colProductDescription";
            this.colProductDescription.ReadOnly = true;
            this.colProductDescription.Width = 200;
            // 
            // colFileName
            // 
            this.colFileName.DataPropertyName = "FileName";
            this.colFileName.HeaderText = "File";
            this.colFileName.Name = "colFileName";
            this.colFileName.ReadOnly = true;
            this.colFileName.Width = 150;
            // 
            // BarcodeBankMaintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1164, 512);
            this.Controls.Add(this.dtgTotal);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbProductCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblNotYetExport);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblTotalExported);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudExportQty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblTotalNotYetUsed);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnGenBarcode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BarcodeBankMaintView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ngân hàng barcode";
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExportQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsPallet;
        private System.Windows.Forms.Button btnGenBarcode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTotalNotYetUsed;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudExportQty;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTotalExported;
        private System.Windows.Forms.Label lblNotYetExport;
        private System.Windows.Forms.Label label5;
        private CustomControls.MultiColumnComboBox cmbProductCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dtgTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonOddQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExportDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFileName;
    }
}