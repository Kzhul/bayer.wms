﻿namespace Bayer.WMS.Prd.Views
{
    partial class ProductionPlanView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductionPlanView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgProductionPlan = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiChangeStatusInProgress = new System.Windows.Forms.ToolStripMenuItem();
            this.bdsProductionPlans = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.btnUpload = new System.Windows.Forms.Button();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProductionLot = new System.Windows.Forms.TextBox();
            this.cmbProduct = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.btnGenBarcode = new System.Windows.Forms.Button();
            this.btnGenAdditionBarcode = new System.Windows.Forms.Button();
            this.btnExportExcel = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnHold = new System.Windows.Forms.Button();
            this.btnUnHold = new System.Windows.Forms.Button();
            this.colProductionPlan_CurrentLabelCartonBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_MaxLabelCartonBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Print = new System.Windows.Forms.DataGridViewImageColumn();
            this.colProductionPlan_IsGenProductBarcode = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colProductionPlan_IsGenCartonBarcode = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colProductionPlan_PlanDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Device = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_ProductPackingType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PackageQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_MFG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_EXP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_PONumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSupplierLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlan_Note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductionPlan)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductionPlans)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgProductionPlan
            // 
            this.dtgProductionPlan.AllowUserToAddRows = false;
            this.dtgProductionPlan.AllowUserToOrderColumns = true;
            this.dtgProductionPlan.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductionPlan.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductionPlan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgProductionPlan.AutoGenerateColumns = false;
            this.dtgProductionPlan.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductionPlan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductionPlan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductionPlan_CurrentLabelCartonBarcode,
            this.colProductionPlan_MaxLabelCartonBarcode,
            this.colProductionPlan_Print,
            this.colProductionPlan_IsGenProductBarcode,
            this.colProductionPlan_IsGenCartonBarcode,
            this.colProductionPlan_PlanDate,
            this.colProductionPlan_ProductLot,
            this.colProductionPlan_Device,
            this.colProductionPlan_ProductID,
            this.colProductionPlan_ProductCode,
            this.colProductionPlan_ProductDescription,
            this.colProductionPlan_ProductPackingType,
            this.colProductionPlan_Quantity,
            this.colProductionPlan_Status,
            this.colProductionPlan_PackageSize,
            this.colProductionPlan_UOM,
            this.colProductionPlan_PackageQuantity,
            this.colProductionPlan_MFG,
            this.colProductionPlan_EXP,
            this.colProductionPlan_PONumber,
            this.colSupplierLot,
            this.colProductionPlan_Note});
            this.dtgProductionPlan.ContextMenuStrip = this.contextMenuStrip1;
            this.dtgProductionPlan.DataSource = this.bdsProductionPlans;
            this.dtgProductionPlan.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductionPlan.Location = new System.Drawing.Point(12, 114);
            this.dtgProductionPlan.Name = "dtgProductionPlan";
            this.dtgProductionPlan.ReadOnly = true;
            this.dtgProductionPlan.Size = new System.Drawing.Size(974, 315);
            this.dtgProductionPlan.TabIndex = 11;
            this.dtgProductionPlan.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgProductionPlan_CellDoubleClick);
            this.dtgProductionPlan.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtgProductionPlan_MouseDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiChangeStatusInProgress});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(260, 26);
            // 
            // tsmiChangeStatusInProgress
            // 
            this.tsmiChangeStatusInProgress.Name = "tsmiChangeStatusInProgress";
            this.tsmiChangeStatusInProgress.Size = new System.Drawing.Size(259, 22);
            this.tsmiChangeStatusInProgress.Text = "Chuyển trạng thái - Đang đóng gói";
            this.tsmiChangeStatusInProgress.Click += new System.EventHandler(this.tsmiChangeStatusInProgress_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 29;
            this.label3.Text = "Ngày:";
            // 
            // btnUpload
            // 
            this.btnUpload.Image = ((System.Drawing.Image)(resources.GetObject("btnUpload.Image")));
            this.btnUpload.Location = new System.Drawing.Point(12, 78);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(50, 30);
            this.btnUpload.TabIndex = 5;
            this.btnUpload.TabStop = false;
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd.MM.yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(63, 12);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(90, 22);
            this.dtpFromDate.TabIndex = 0;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd.MM.yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(173, 12);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(90, 22);
            this.dtpToDate.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(158, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 16);
            this.label1.TabIndex = 34;
            this.label1.Text = "-";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(765, 37);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 30);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 36;
            this.label2.Text = "Mã lô:";
            // 
            // txtProductionLot
            // 
            this.txtProductionLot.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtProductionLot.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtProductionLot.Location = new System.Drawing.Point(63, 40);
            this.txtProductionLot.Name = "txtProductionLot";
            this.txtProductionLot.Size = new System.Drawing.Size(200, 22);
            this.txtProductionLot.TabIndex = 2;
            // 
            // cmbProduct
            // 
            this.cmbProduct.Columns = null;
            this.cmbProduct.DropDownHeight = 1;
            this.cmbProduct.DropDownWidth = 500;
            this.cmbProduct.FormattingEnabled = true;
            this.cmbProduct.IntegralHeight = false;
            this.cmbProduct.Location = new System.Drawing.Point(359, 40);
            this.cmbProduct.MaxLength = 255;
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.PageSize = 0;
            this.cmbProduct.PresenterInfo = null;
            this.cmbProduct.Size = new System.Drawing.Size(400, 24);
            this.cmbProduct.Source = null;
            this.cmbProduct.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 43);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 39;
            this.label4.Text = "Sản phẩm:";
            // 
            // btnGenBarcode
            // 
            this.btnGenBarcode.Location = new System.Drawing.Point(124, 78);
            this.btnGenBarcode.Name = "btnGenBarcode";
            this.btnGenBarcode.Size = new System.Drawing.Size(120, 30);
            this.btnGenBarcode.TabIndex = 7;
            this.btnGenBarcode.TabStop = false;
            this.btnGenBarcode.Text = "Tạo mã QR";
            this.btnGenBarcode.UseVisualStyleBackColor = true;
            this.btnGenBarcode.Click += new System.EventHandler(this.btnGenBarcode_Click);
            // 
            // btnGenAdditionBarcode
            // 
            this.btnGenAdditionBarcode.Location = new System.Drawing.Point(250, 78);
            this.btnGenAdditionBarcode.Name = "btnGenAdditionBarcode";
            this.btnGenAdditionBarcode.Size = new System.Drawing.Size(170, 30);
            this.btnGenAdditionBarcode.TabIndex = 8;
            this.btnGenAdditionBarcode.TabStop = false;
            this.btnGenAdditionBarcode.Text = "Tạo mã QR bổ sung";
            this.btnGenAdditionBarcode.UseVisualStyleBackColor = true;
            this.btnGenAdditionBarcode.Click += new System.EventHandler(this.btnGenAdditionBarcode_Click);
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.Location = new System.Drawing.Point(68, 78);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(50, 30);
            this.btnExportExcel.TabIndex = 6;
            this.btnExportExcel.TabStop = false;
            this.btnExportExcel.UseVisualStyleBackColor = true;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnHold
            // 
            this.btnHold.Location = new System.Drawing.Point(426, 78);
            this.btnHold.Name = "btnHold";
            this.btnHold.Size = new System.Drawing.Size(100, 30);
            this.btnHold.TabIndex = 9;
            this.btnHold.TabStop = false;
            this.btnHold.Text = "Hold KHSX";
            this.btnHold.UseVisualStyleBackColor = true;
            this.btnHold.Click += new System.EventHandler(this.btnHold_Click);
            // 
            // btnUnHold
            // 
            this.btnUnHold.Location = new System.Drawing.Point(532, 78);
            this.btnUnHold.Name = "btnUnHold";
            this.btnUnHold.Size = new System.Drawing.Size(100, 30);
            this.btnUnHold.TabIndex = 10;
            this.btnUnHold.TabStop = false;
            this.btnUnHold.Text = "UnHold KHSX";
            this.btnUnHold.UseVisualStyleBackColor = true;
            this.btnUnHold.Click += new System.EventHandler(this.btnUnHold_Click);
            // 
            // colProductionPlan_CurrentLabelCartonBarcode
            // 
            this.colProductionPlan_CurrentLabelCartonBarcode.DataPropertyName = "CurrentLabelCartonBarcode";
            this.colProductionPlan_CurrentLabelCartonBarcode.HeaderText = "colProductionPlan_CurrentLabelCartonBarcode";
            this.colProductionPlan_CurrentLabelCartonBarcode.Name = "colProductionPlan_CurrentLabelCartonBarcode";
            this.colProductionPlan_CurrentLabelCartonBarcode.ReadOnly = true;
            this.colProductionPlan_CurrentLabelCartonBarcode.Visible = false;
            // 
            // colProductionPlan_MaxLabelCartonBarcode
            // 
            this.colProductionPlan_MaxLabelCartonBarcode.DataPropertyName = "MaxLabelCartonBarcode";
            this.colProductionPlan_MaxLabelCartonBarcode.HeaderText = "colProductionPlan_MaxLabelCartonBarcode";
            this.colProductionPlan_MaxLabelCartonBarcode.Name = "colProductionPlan_MaxLabelCartonBarcode";
            this.colProductionPlan_MaxLabelCartonBarcode.ReadOnly = true;
            this.colProductionPlan_MaxLabelCartonBarcode.Visible = false;
            // 
            // colProductionPlan_Print
            // 
            this.colProductionPlan_Print.HeaderText = "";
            this.colProductionPlan_Print.Image = ((System.Drawing.Image)(resources.GetObject("colProductionPlan_Print.Image")));
            this.colProductionPlan_Print.Name = "colProductionPlan_Print";
            this.colProductionPlan_Print.ReadOnly = true;
            this.colProductionPlan_Print.Width = 50;
            // 
            // colProductionPlan_IsGenProductBarcode
            // 
            this.colProductionPlan_IsGenProductBarcode.DataPropertyName = "IsGenProductBarcode";
            this.colProductionPlan_IsGenProductBarcode.HeaderText = "QR thành phẩm";
            this.colProductionPlan_IsGenProductBarcode.Name = "colProductionPlan_IsGenProductBarcode";
            this.colProductionPlan_IsGenProductBarcode.ReadOnly = true;
            this.colProductionPlan_IsGenProductBarcode.Width = 70;
            // 
            // colProductionPlan_IsGenCartonBarcode
            // 
            this.colProductionPlan_IsGenCartonBarcode.DataPropertyName = "IsGenCartonBarcode";
            this.colProductionPlan_IsGenCartonBarcode.HeaderText = "QR thùng";
            this.colProductionPlan_IsGenCartonBarcode.Name = "colProductionPlan_IsGenCartonBarcode";
            this.colProductionPlan_IsGenCartonBarcode.ReadOnly = true;
            this.colProductionPlan_IsGenCartonBarcode.Width = 70;
            // 
            // colProductionPlan_PlanDate
            // 
            this.colProductionPlan_PlanDate.DataPropertyName = "PlanDate";
            dataGridViewCellStyle2.Format = "dd.MM.yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.colProductionPlan_PlanDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.colProductionPlan_PlanDate.HeaderText = "Ngày";
            this.colProductionPlan_PlanDate.Name = "colProductionPlan_PlanDate";
            this.colProductionPlan_PlanDate.ReadOnly = true;
            this.colProductionPlan_PlanDate.Width = 80;
            // 
            // colProductionPlan_ProductLot
            // 
            this.colProductionPlan_ProductLot.DataPropertyName = "ProductLot";
            this.colProductionPlan_ProductLot.HeaderText = "Mã lô";
            this.colProductionPlan_ProductLot.Name = "colProductionPlan_ProductLot";
            this.colProductionPlan_ProductLot.ReadOnly = true;
            this.colProductionPlan_ProductLot.Width = 80;
            // 
            // colProductionPlan_Device
            // 
            this.colProductionPlan_Device.DataPropertyName = "Device";
            this.colProductionPlan_Device.HeaderText = "Thiết bị";
            this.colProductionPlan_Device.Name = "colProductionPlan_Device";
            this.colProductionPlan_Device.ReadOnly = true;
            this.colProductionPlan_Device.Width = 80;
            // 
            // colProductionPlan_ProductID
            // 
            this.colProductionPlan_ProductID.DataPropertyName = "ProductID";
            this.colProductionPlan_ProductID.HeaderText = "ProductID";
            this.colProductionPlan_ProductID.Name = "colProductionPlan_ProductID";
            this.colProductionPlan_ProductID.ReadOnly = true;
            this.colProductionPlan_ProductID.Visible = false;
            // 
            // colProductionPlan_ProductCode
            // 
            this.colProductionPlan_ProductCode.DataPropertyName = "ProductCode";
            this.colProductionPlan_ProductCode.HeaderText = "Mã sản phẩm";
            this.colProductionPlan_ProductCode.Name = "colProductionPlan_ProductCode";
            this.colProductionPlan_ProductCode.ReadOnly = true;
            // 
            // colProductionPlan_ProductDescription
            // 
            this.colProductionPlan_ProductDescription.DataPropertyName = "ProductDescription";
            this.colProductionPlan_ProductDescription.HeaderText = "Mô tả";
            this.colProductionPlan_ProductDescription.Name = "colProductionPlan_ProductDescription";
            this.colProductionPlan_ProductDescription.ReadOnly = true;
            this.colProductionPlan_ProductDescription.Width = 300;
            // 
            // colProductionPlan_ProductPackingType
            // 
            this.colProductionPlan_ProductPackingType.DataPropertyName = "ProductPackingType";
            this.colProductionPlan_ProductPackingType.HeaderText = "Loại đóng gói";
            this.colProductionPlan_ProductPackingType.Name = "colProductionPlan_ProductPackingType";
            this.colProductionPlan_ProductPackingType.ReadOnly = true;
            this.colProductionPlan_ProductPackingType.Visible = false;
            // 
            // colProductionPlan_Quantity
            // 
            this.colProductionPlan_Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            this.colProductionPlan_Quantity.DefaultCellStyle = dataGridViewCellStyle3;
            this.colProductionPlan_Quantity.HeaderText = "Số lượng";
            this.colProductionPlan_Quantity.Name = "colProductionPlan_Quantity";
            this.colProductionPlan_Quantity.ReadOnly = true;
            this.colProductionPlan_Quantity.Width = 70;
            // 
            // colProductionPlan_Status
            // 
            this.colProductionPlan_Status.DataPropertyName = "StatusDisplay";
            this.colProductionPlan_Status.HeaderText = "Trạng thái";
            this.colProductionPlan_Status.Name = "colProductionPlan_Status";
            this.colProductionPlan_Status.ReadOnly = true;
            // 
            // colProductionPlan_PackageSize
            // 
            this.colProductionPlan_PackageSize.DataPropertyName = "PackageSize";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N3";
            this.colProductionPlan_PackageSize.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductionPlan_PackageSize.HeaderText = "Kích cỡ";
            this.colProductionPlan_PackageSize.Name = "colProductionPlan_PackageSize";
            this.colProductionPlan_PackageSize.ReadOnly = true;
            this.colProductionPlan_PackageSize.Width = 50;
            // 
            // colProductionPlan_UOM
            // 
            this.colProductionPlan_UOM.DataPropertyName = "UOM";
            this.colProductionPlan_UOM.HeaderText = "ĐVT";
            this.colProductionPlan_UOM.Name = "colProductionPlan_UOM";
            this.colProductionPlan_UOM.ReadOnly = true;
            this.colProductionPlan_UOM.Width = 50;
            // 
            // colProductionPlan_PackageQuantity
            // 
            this.colProductionPlan_PackageQuantity.DataPropertyName = "PackageQuantity";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colProductionPlan_PackageQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.colProductionPlan_PackageQuantity.HeaderText = "Số lượng theo bao bì";
            this.colProductionPlan_PackageQuantity.Name = "colProductionPlan_PackageQuantity";
            this.colProductionPlan_PackageQuantity.ReadOnly = true;
            // 
            // colProductionPlan_MFG
            // 
            this.colProductionPlan_MFG.DataPropertyName = "ManufacturingDate";
            dataGridViewCellStyle6.Format = "dd.MM.yyyy";
            dataGridViewCellStyle6.NullValue = null;
            this.colProductionPlan_MFG.DefaultCellStyle = dataGridViewCellStyle6;
            this.colProductionPlan_MFG.HeaderText = "Ngày sản xuất";
            this.colProductionPlan_MFG.Name = "colProductionPlan_MFG";
            this.colProductionPlan_MFG.ReadOnly = true;
            // 
            // colProductionPlan_EXP
            // 
            this.colProductionPlan_EXP.DataPropertyName = "ExpiryDate";
            dataGridViewCellStyle7.Format = "dd.MM.yyyy";
            this.colProductionPlan_EXP.DefaultCellStyle = dataGridViewCellStyle7;
            this.colProductionPlan_EXP.HeaderText = "Ngày hết hạn";
            this.colProductionPlan_EXP.Name = "colProductionPlan_EXP";
            this.colProductionPlan_EXP.ReadOnly = true;
            // 
            // colProductionPlan_PONumber
            // 
            this.colProductionPlan_PONumber.DataPropertyName = "PONumber";
            this.colProductionPlan_PONumber.HeaderText = "Số PO";
            this.colProductionPlan_PONumber.Name = "colProductionPlan_PONumber";
            this.colProductionPlan_PONumber.ReadOnly = true;
            // 
            // colSupplierLot
            // 
            this.colSupplierLot.DataPropertyName = "SupplierLot";
            this.colSupplierLot.HeaderText = "Số lô NCC";
            this.colSupplierLot.Name = "colSupplierLot";
            this.colSupplierLot.ReadOnly = true;
            // 
            // colProductionPlan_Note
            // 
            this.colProductionPlan_Note.DataPropertyName = "Note";
            this.colProductionPlan_Note.HeaderText = "Ghi chú";
            this.colProductionPlan_Note.Name = "colProductionPlan_Note";
            this.colProductionPlan_Note.ReadOnly = true;
            this.colProductionPlan_Note.Width = 200;
            // 
            // ProductionPlanView
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 441);
            this.Controls.Add(this.btnUnHold);
            this.Controls.Add(this.btnHold);
            this.Controls.Add(this.btnExportExcel);
            this.Controls.Add(this.btnGenAdditionBarcode);
            this.Controls.Add(this.btnGenBarcode);
            this.Controls.Add(this.cmbProduct);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtProductionLot);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgProductionPlan);
            this.Name = "ProductionPlanView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductionPlan)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductionPlans)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgProductionPlan;
        private System.Windows.Forms.BindingSource bdsProductionPlans;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProductionLot;
        private CustomControls.MultiColumnComboBox cmbProduct;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnGenBarcode;
        private System.Windows.Forms.Button btnGenAdditionBarcode;
        private System.Windows.Forms.Button btnExportExcel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnHold;
        private System.Windows.Forms.Button btnUnHold;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiChangeStatusInProgress;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_CurrentLabelCartonBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_MaxLabelCartonBarcode;
        private System.Windows.Forms.DataGridViewImageColumn colProductionPlan_Print;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colProductionPlan_IsGenProductBarcode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colProductionPlan_IsGenCartonBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PlanDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Device;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_ProductPackingType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PackageQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_MFG;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_EXP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_PONumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSupplierLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlan_Note;
    }
}