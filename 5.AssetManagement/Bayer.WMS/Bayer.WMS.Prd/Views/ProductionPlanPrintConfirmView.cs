﻿using Bayer.WMS.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class ProductionPlanPrintConfirmView : BaseForm
    {
        public ProductionPlanPrintConfirmView()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public int Quantity
        {
            get { return Convert.ToInt32(nudQuantity.Value); }
            set
            {
                nudQuantity.Maximum = value;
                nudQuantity.Value = value;
                
            }
        }

        public string Note
        {
            get { return txtNote.Text; }
        }
    }
}
