﻿using Bayer.WMS.Base;
using Bayer.WMS.Prd.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.IO;

namespace Bayer.WMS.Prd.Views
{
    public partial class BarcodeBankMaintView : BaseForm, IBarcodeBankMaintView
    {
        public BarcodeBankMaintView()
        {
            InitializeComponent();
            InitializeComboBox();
            SetDoubleBuffered(dtgTotal);
        }

        public override void InitializeComboBox()
        {
            cmbProductCode.DropDownWidth = 700;
            cmbProductCode.PageSize = 20;
            cmbProductCode.ValueMember = "ProductCode";
            cmbProductCode.DisplayMember = "ProductCode - Description";
            cmbProductCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("ProductCode", "Mã sản phẩm", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("CategoryDescription", "Nhóm sản phẩm", 150),
                new MultiColumnComboBox.ComboBoxColumn("TypeDisplay", "Loại sản phẩm", 150),
                new MultiColumnComboBox.ComboBoxColumn("UOM", "ĐVT", 80),
                new MultiColumnComboBox.ComboBoxColumn("PackingTypeDisplay", "Loại đóng gói", 150),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
        }

        private async void btnGenBarcode_Click(object sender, EventArgs e)
        {
            btnGenBarcode.Enabled = false;
            try
            {
                await (_presenter as IBarcodeBankMaintPresenter).GenerateBarcode();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnGenBarcode.Enabled = true;
            }
        }

        private async void btnExport_Click(object sender, EventArgs e)
        {
            btnExport.Enabled = false;
            try
            {
                if (nudExportQty.Value <= 0)
                    throw new WrappedException(Messages.Validate_Required_ExportQty);

                string fileName;
                string filePath;
                string productCode = string.Empty;
                string productName = string.Empty;
                using (var sfd = new SaveFileDialog())
                {
                    sfd.Filter = "Data Files (*.xlsx)|*.xlsx";
                    sfd.DefaultExt = "xlsx";
                    sfd.AddExtension = true;

                    //mySaveFileDialog.DefaultFileName = myDataGridView.SelectedCells[2].Value.ToString();
                    if (cmbProductCode.SelectedItem != null)
                    {
                        productCode = cmbProductCode.SelectedItem["ProductCode"].ToString();
                        productName = cmbProductCode.SelectedItem["ProductCode"].ToString() + "_" + cmbProductCode.SelectedItem["Description"].ToString();
                    }
                    productName = Utility.CleanFileName(productName);
                    sfd.FileName = "QRCode" + "_" + productName + "_" + nudExportQty.Value.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";

                    if (sfd.ShowDialog() != DialogResult.OK)
                        return;

                    filePath = sfd.FileName;
                    fileName = Path.GetFileName(sfd.FileName);
                }

                await (_presenter as IBarcodeBankMaintPresenter).ExportQRProductCode((int)nudExportQty.Value, filePath, fileName, productCode, txtProductDescription.Text);


            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnExport.Enabled = true;
                MessageBox.Show("Xuất Excel thành công.");
            }
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                var palletPresenter = _presenter as IBarcodeBankMaintPresenter;

                await palletPresenter.LoadMainData();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DataTable Products
        {
            set { cmbProductCode.Source = value; }
        }

        private DataTable _ReportHistory;

        public DataTable ReportHistory
        {
            get { return _ReportHistory; }
            set
            {
                _ReportHistory = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsPallet.DataSource = _ReportHistory;
                    });
                }
                else
                {
                    bdsPallet.DataSource = _ReportHistory;
                }
            }
        }

        public int TotalNotYetUsed { set { lblTotalNotYetUsed.Text = $"{value:N0}"; } }

        public int TotalExported { set { lblTotalExported.Text = $"{value:N0}"; } }

        public int TotalNotYetExport { set { lblNotYetExport.Text = $"{value:N0}"; } }
    }
}
