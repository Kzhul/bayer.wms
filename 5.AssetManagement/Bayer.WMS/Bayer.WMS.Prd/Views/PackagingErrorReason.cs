﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class PackagingErrorReason : BaseForm
    {
        private Action<PackagingErrorReason> _invokeOK;
        private Action<PackagingErrorReason> _invokeCancel;

        public PackagingErrorReason()
        {
            InitializeComponent();
        }

        private void btnDestroy_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtReason.Text))
                lblMessage.Text = "Vui lòng nhập lý do lỗi.";
            else
                _invokeOK(this);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _invokeCancel(this);
        }

        public void Show(Control parent)
        {
            Top = (parent.Height - Height) / 2;
            Left = (parent.Width - Width) / 2 + parent.Left;

            if (parent != null)
                Name = $"{parent.Name}_PackagingErrorReason";

            Show();
            BringToFront();
        }

        public void PressDestroy()
        {
            btnDestroy.PerformClick();
        }

        public void PressCancel()
        {
            btnCancel.PerformClick();
        }

        public string Reason { get => txtReason.Text; }

        public Action<PackagingErrorReason> InvokeOK { set => _invokeOK = value; }

        public Action<PackagingErrorReason> InvokeCancel { set => _invokeCancel = value; }
    }
}
