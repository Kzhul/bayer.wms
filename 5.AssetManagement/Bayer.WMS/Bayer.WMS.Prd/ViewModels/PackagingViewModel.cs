﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Prd.ViewModels
{
    public class PackagingViewModel
    {
        public PackagingViewModel()
        {

        }

        public PackagingViewModel(string productLot, string productCode, string productDescription, decimal? planPackingQty,
            decimal? cartonPackingQty, decimal? palletPackingQty, decimal? actualPackingQty, decimal cancelLabel, decimal destroyLabel,
            string uomPallet, string uomProdutLot
            )
        {
            ProductLot = productLot;
            ProductCode = productCode;
            ProductDescription = productDescription;
            PlanPackingQty = planPackingQty ?? 0;
            CartonPackingQty = cartonPackingQty ?? 0;
            PalletPackingQty = palletPackingQty ?? 0;
            ActualPackingQty = actualPackingQty ?? 0;
            CancelLabel = cancelLabel;
            DestroyLabel = destroyLabel;
            UOMPallet = uomPallet;
            UOMProductLot = uomProdutLot;
        }

        public string UOMPallet { get; set; }

        public string UOMProductLot { get; set; }

        public string ProductLot { get; set; }

        public string ProductCode { get; set; }

        public string ProductDescription { get; set; }

        public string CartonBarcode { get; set; }

        public string PalletBarcode { get; set; }

        public decimal PlanPackingQty { get; set; }

        public decimal ActualPackingQty { get; set; }

        public decimal ActualPackingQtyInCarton { get; set; }

        public decimal ActualPackingQtyOnPallet { get; set; }

        public decimal CartonPackingQty { get; set; }

        public decimal PalletPackingQty { get; set; }

        public string PackingQtyInCarton
        {
            get
            {
                if (String.IsNullOrWhiteSpace(CartonBarcode))
                    return "0 / 0";
                else
                    return $"{ActualPackingQtyInCarton:N0} / {CartonPackingQty:N0}";
            }
        }

        public string PackingQtyOnPallet
        {
            get
            {
                if (String.IsNullOrWhiteSpace(PalletBarcode))
                    return "0 / 0";
                else
                    return $"{ActualPackingQtyOnPallet:N0} / {PalletPackingQty:N0}";
            }
        }

        public string ActualPerPallet
        {
            get
            {
                if (String.IsNullOrWhiteSpace(PalletBarcode))
                    return "0 / 0";
                else
                    return $"SL trên Pallet: {ActualPackingQtyOnPallet:N0} / {PalletPackingQty:N0}";
            }
        }

        public string CartonQtyOnPallet
        {
            get
            {
                if (String.IsNullOrWhiteSpace(PalletBarcode) || CartonPackingQty == 0)
                    return "0 / 0";
                else
                {
                    decimal actualCartonQty = string.IsNullOrWhiteSpace(PackagingStatus) || PackagingStatus == ProductionPlan.status.New || PackagingStatus == ProductionPlan.status.InProgress
                        ? Math.Floor(ActualPackingQtyOnPallet / CartonPackingQty) : Math.Ceiling(ActualPackingQtyOnPallet / CartonPackingQty);
                    decimal planCartonQty = Math.Floor(PlanPackingQty / CartonPackingQty);
                    decimal packageCartonQty = Math.Floor(PalletPackingQty / CartonPackingQty);
                    return $"{actualCartonQty:N0} / {(planCartonQty > packageCartonQty ? packageCartonQty : planCartonQty):N0}";
                }
            }
        }

        public string ActualPackingQtyOnPlan
        {
            get { return $"{ActualPackingQty:N0} / {PlanPackingQty:N0}"; }
        }

        public decimal CancelLabel { get; set; }

        public decimal DestroyLabel { get; set; }

        public string PackagingStatus { get; set; }
    }
}
