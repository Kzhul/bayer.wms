﻿namespace Bayer.WMS.Handheld.Views
{
    partial class WarehouseManagerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPallet = new System.Windows.Forms.Label();
            this.bdsPalletSummary = new System.Windows.Forms.BindingSource(this.components);
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnClearPallet = new System.Windows.Forms.Button();
            this.btnMoveProductPallet = new System.Windows.Forms.Button();
            this.btnVeirifyProductPallet = new System.Windows.Forms.Button();
            this.btnReceiveMaterial = new System.Windows.Forms.Button();
            this.btnMaterialMove = new System.Windows.Forms.Button();
            this.btnExportProduct = new System.Windows.Forms.Button();
            this.btnProductReturn = new System.Windows.Forms.Button();
            this.btnStockReturn = new System.Windows.Forms.Button();
            this.btnMaterialAudit = new System.Windows.Forms.Button();
            this.btnConfirmMoveMaterial = new System.Windows.Forms.Button();
            this.btnProductWaiting = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bdsPalletWaiting = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPalletWaiting = new System.Windows.Forms.DataGrid();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgPallet = new System.Windows.Forms.DataGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.bdsPalletDone = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPalletDone = new System.Windows.Forms.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletWaiting)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDone)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 21);
            this.label1.Text = "Pallet:";
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(49, 27);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(188, 21);
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã Pallet để bắt đầu";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(0, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 185);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(this.btnClearPallet);
            this.tabPage3.Controls.Add(this.btnMoveProductPallet);
            this.tabPage3.Controls.Add(this.btnVeirifyProductPallet);
            this.tabPage3.Controls.Add(this.btnReceiveMaterial);
            this.tabPage3.Controls.Add(this.btnMaterialMove);
            this.tabPage3.Controls.Add(this.btnExportProduct);
            this.tabPage3.Controls.Add(this.btnProductReturn);
            this.tabPage3.Controls.Add(this.btnStockReturn);
            this.tabPage3.Controls.Add(this.btnMaterialAudit);
            this.tabPage3.Controls.Add(this.btnConfirmMoveMaterial);
            this.tabPage3.Controls.Add(this.btnProductWaiting);
            this.tabPage3.Location = new System.Drawing.Point(0, 0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(240, 162);
            this.tabPage3.Text = "1.Công việc";
            // 
            // btnClearPallet
            // 
            this.btnClearPallet.Location = new System.Drawing.Point(5, 256);
            this.btnClearPallet.Name = "btnClearPallet";
            this.btnClearPallet.Size = new System.Drawing.Size(219, 25);
            this.btnClearPallet.TabIndex = 10;
            this.btnClearPallet.Text = "Clear Pallet";
            this.btnClearPallet.Click += new System.EventHandler(this.btnClearPallet_Click);
            // 
            // btnMoveProductPallet
            // 
            this.btnMoveProductPallet.Location = new System.Drawing.Point(5, 225);
            this.btnMoveProductPallet.Name = "btnMoveProductPallet";
            this.btnMoveProductPallet.Size = new System.Drawing.Size(219, 25);
            this.btnMoveProductPallet.TabIndex = 9;
            this.btnMoveProductPallet.Text = "San hàng thành phẩm";
            this.btnMoveProductPallet.Click += new System.EventHandler(this.btnMoveProductPallet_Click);
            // 
            // btnVeirifyProductPallet
            // 
            this.btnVeirifyProductPallet.Location = new System.Drawing.Point(5, 194);
            this.btnVeirifyProductPallet.Name = "btnVeirifyProductPallet";
            this.btnVeirifyProductPallet.Size = new System.Drawing.Size(219, 25);
            this.btnVeirifyProductPallet.TabIndex = 8;
            this.btnVeirifyProductPallet.Text = "Kiểm kê thành phẩm";
            this.btnVeirifyProductPallet.Click += new System.EventHandler(this.btnVeirifyProductPallet_Click);
            // 
            // btnReceiveMaterial
            // 
            this.btnReceiveMaterial.Location = new System.Drawing.Point(5, 71);
            this.btnReceiveMaterial.Name = "btnReceiveMaterial";
            this.btnReceiveMaterial.Size = new System.Drawing.Size(219, 25);
            this.btnReceiveMaterial.TabIndex = 7;
            this.btnReceiveMaterial.Text = "Nhập NL vào kho";
            this.btnReceiveMaterial.Click += new System.EventHandler(this.btnReceiveMaterial_Click);
            // 
            // btnMaterialMove
            // 
            this.btnMaterialMove.Location = new System.Drawing.Point(5, 316);
            this.btnMaterialMove.Name = "btnMaterialMove";
            this.btnMaterialMove.Size = new System.Drawing.Size(219, 25);
            this.btnMaterialMove.TabIndex = 6;
            this.btnMaterialMove.Text = "San hàng nguyên liệu";
            this.btnMaterialMove.Click += new System.EventHandler(this.btnMaterialMove_Click);
            // 
            // btnExportProduct
            // 
            this.btnExportProduct.Location = new System.Drawing.Point(5, 9);
            this.btnExportProduct.Name = "btnExportProduct";
            this.btnExportProduct.Size = new System.Drawing.Size(219, 25);
            this.btnExportProduct.TabIndex = 5;
            this.btnExportProduct.Text = "Xuất thành phẩm theo DO";
            this.btnExportProduct.Click += new System.EventHandler(this.btnExportProduct_Click);
            // 
            // btnProductReturn
            // 
            this.btnProductReturn.Location = new System.Drawing.Point(5, 163);
            this.btnProductReturn.Name = "btnProductReturn";
            this.btnProductReturn.Size = new System.Drawing.Size(219, 25);
            this.btnProductReturn.TabIndex = 4;
            this.btnProductReturn.Text = "Trả hàng cho KH";
            this.btnProductReturn.Click += new System.EventHandler(this.btnProductReturn_Click);
            // 
            // btnStockReturn
            // 
            this.btnStockReturn.Location = new System.Drawing.Point(5, 132);
            this.btnStockReturn.Name = "btnStockReturn";
            this.btnStockReturn.Size = new System.Drawing.Size(219, 25);
            this.btnStockReturn.TabIndex = 3;
            this.btnStockReturn.Text = "SX Trả NL";
            this.btnStockReturn.Click += new System.EventHandler(this.btnStockReturn_Click);
            // 
            // btnMaterialAudit
            // 
            this.btnMaterialAudit.Location = new System.Drawing.Point(5, 286);
            this.btnMaterialAudit.Name = "btnMaterialAudit";
            this.btnMaterialAudit.Size = new System.Drawing.Size(219, 25);
            this.btnMaterialAudit.TabIndex = 2;
            this.btnMaterialAudit.Text = "Kiểm kê nguyên liệu";
            this.btnMaterialAudit.Click += new System.EventHandler(this.btnMaterialAudit_Click);
            // 
            // btnConfirmMoveMaterial
            // 
            this.btnConfirmMoveMaterial.Location = new System.Drawing.Point(5, 101);
            this.btnConfirmMoveMaterial.Name = "btnConfirmMoveMaterial";
            this.btnConfirmMoveMaterial.Size = new System.Drawing.Size(219, 25);
            this.btnConfirmMoveMaterial.TabIndex = 1;
            this.btnConfirmMoveMaterial.Text = "Chuyển NL-SX";
            this.btnConfirmMoveMaterial.Click += new System.EventHandler(this.btnConfirmMoveMaterial_Click);
            // 
            // btnProductWaiting
            // 
            this.btnProductWaiting.Location = new System.Drawing.Point(5, 40);
            this.btnProductWaiting.Name = "btnProductWaiting";
            this.btnProductWaiting.Size = new System.Drawing.Size(219, 25);
            this.btnProductWaiting.TabIndex = 0;
            this.btnProductWaiting.Text = "Nhập TP vào kho";
            this.btnProductWaiting.Click += new System.EventHandler(this.btnProductWaiting_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgPalletWaiting);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(232, 159);
            this.tabPage2.Text = "2.DS Pallet chờ";
            // 
            // dtgPalletWaiting
            // 
            this.dtgPalletWaiting.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPalletWaiting.DataSource = this.bdsPalletWaiting;
            this.dtgPalletWaiting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPalletWaiting.Location = new System.Drawing.Point(0, 0);
            this.dtgPalletWaiting.Name = "dtgPalletWaiting";
            this.dtgPalletWaiting.RowHeadersVisible = false;
            this.dtgPalletWaiting.Size = new System.Drawing.Size(232, 159);
            this.dtgPalletWaiting.TabIndex = 1;
            this.dtgPalletWaiting.DoubleClick += new System.EventHandler(this.dtgPalletWaiting_DoubleClick);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgPallet);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(232, 159);
            this.tabPage1.Text = "3.Hàng";
            // 
            // dtgPallet
            // 
            this.dtgPallet.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPallet.DataSource = this.bdsPalletSummary;
            this.dtgPallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallet.Location = new System.Drawing.Point(0, 0);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.RowHeadersVisible = false;
            this.dtgPallet.Size = new System.Drawing.Size(232, 159);
            this.dtgPallet.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dtgPalletDone);
            this.tabPage4.Location = new System.Drawing.Point(0, 0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(232, 159);
            this.tabPage4.Text = "4.DS đã làm";
            // 
            // dtgPalletDone
            // 
            this.dtgPalletDone.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPalletDone.DataSource = this.bdsPalletDone;
            this.dtgPalletDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPalletDone.Location = new System.Drawing.Point(0, 0);
            this.dtgPalletDone.Name = "dtgPalletDone";
            this.dtgPalletDone.RowHeadersVisible = false;
            this.dtgPalletDone.Size = new System.Drawing.Size(232, 159);
            this.dtgPalletDone.TabIndex = 2;
            // 
            // WarehouseManagerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.lblPallet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "WarehouseManagerView";
            this.Text = "Thủ kho";
            this.Closed += new System.EventHandler(this.ReceiveProductConfirmView_Closed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ReceiveProductConfirmView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletWaiting)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDone)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsPalletSummary;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGrid dtgPallet;
        private System.Windows.Forms.DataGrid dtgPalletWaiting;
        private System.Windows.Forms.BindingSource bdsPalletWaiting;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnProductWaiting;
        private System.Windows.Forms.Button btnConfirmMoveMaterial;
        private System.Windows.Forms.Button btnMaterialAudit;
        private System.Windows.Forms.Button btnStockReturn;
        private System.Windows.Forms.Button btnProductReturn;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGrid dtgPalletDone;
        private System.Windows.Forms.BindingSource bdsPalletDone;
        private System.Windows.Forms.Button btnExportProduct;
        private System.Windows.Forms.Button btnMaterialMove;
        private System.Windows.Forms.Button btnReceiveMaterial;
        private System.Windows.Forms.Button btnVeirifyProductPallet;
        private System.Windows.Forms.Button btnClearPallet;
        private System.Windows.Forms.Button btnMoveProductPallet;
    }
}