﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.IO.Ports;
using Bayer.WMS.Delivery.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class PrintPalletLabelView : Form
    {
        private bool _isPrintPalletLabel;
        private string _doImportCode;
        private string _companyCode;
        private string _companyName;
        private string _executor;
        private string _deliveryDate;
        private BarcodeReader _barcodeReader;
        private readonly SerialPort _serialPort;

        public PrintPalletLabelView()
        {
            InitializeComponent();
            InitDataGridView();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);


            Parity parity = Parity.None;
            switch (Utility.COM_Parity)
            {
                case "Even":
                    parity = Parity.Even;
                    break;
                case "Mark":
                    parity = Parity.Mark;
                    break;
                case "Odd":
                    parity = Parity.Odd;
                    break;
                case "Space":
                    parity = Parity.Space;
                    break;
                default:
                    break;
            }

            StopBits sb = StopBits.None;
            switch (Utility.COM_StopBits)
            {
                case "1":
                    sb = StopBits.One;
                    break;
                case "1.5":
                    sb = StopBits.OnePointFive;
                    break;
                case "2":
                    sb = StopBits.Two;
                    break;
                default:
                    break;
            }

            _serialPort = new SerialPort(Utility.COM_PortName, Utility.COM_BaudRate, parity, Utility.COM_DataBits, sb);
        }

        public void PrintPalletLabelView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Loại", "StrPackingType", 100, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lượng", "Quantity", 60, "N0", null));

            dtgConfirmQty.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                //if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else
                {
                    var dtSum = Pallet.LoadPalletStatusGroupByPackingType(barcode, Utility.PalletStatusPrepared);
                    dtSum.TableName = "PalletSummary";
                    bdsPalletSummary.DataSource = dtSum;

                    //Auto confirm this pallet to the customer
                    try
                    {
                        Pallet _currentPallet = new Pallet();
                        _currentPallet = Pallet.Load(barcode);
                        if (_currentPallet != null)
                        {
                            //Verify Pallet Quanlity
                            DataTable dt = Company.VerifyPreparedQuantity(_doImportCode, _companyCode, barcode);
                            if (dt.Rows.Count > 0)
                            {
                                string mes = string.Empty;

                                mes =
                                        "Vượt quá số lượng yêu cầu" + Environment.NewLine
                                        + "SL: " + dt.Rows[0]["NextQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                                        + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                                        + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                                        ;

                                Utility.PlayErrorSound();
                                MessageBox.Show(mes, "Lỗi");
                                return;
                            }

                            //Approved Pallet To This Customer
                            Pallet.UpdatePallet_Status(barcode, _doImportCode, _doImportCode, _companyCode, "V", "PrintPallet");

                            //Auto set location for prepare area
                            Pallet.SetLocation(_currentPallet.PalletCode, Utility.PrepareLocationCode, "SH");

                            lblStepHints.Text = "Tự động xác nhận pallet: " + barcode + " cho khách hàng: " + _companyName;
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.PlayErrorSound();
                        MessageBox.Show(ex.Message, "Lỗi");
                    }

                    try
                    {
                        Print(barcode, dtSum);
                    }
                    catch (Exception ex)
                    {
                        Utility.PlayErrorSound();
                        MessageBox.Show(ex.Message, "Lỗi");
                    }

                    _isPrintPalletLabel = true;
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        public void Print(string palletCode, DataTable dt)
        {
            try
            {
                if (!_serialPort.IsOpen)
                    _serialPort.Open();

                string template = Utility.PalletLabelTemplate;
                var quantities = new List<string>();
                string strCartonQuantity = string.Empty;
                string strBagQuantity = string.Empty;
                string strShoveQuantity = string.Empty;
                string strCanQuantity = string.Empty;
                foreach (DataRow row in dt.Rows)
                {
                    string packginType = row["PackingType"].ToString();
                    switch (packginType)
                    {
                        case "C":
                            packginType = "T";
                            strCartonQuantity = row["Quantity"].ToString();
                            break;
                        case "B":
                            packginType = "B";
                            strBagQuantity = row["Quantity"].ToString();
                            break;
                        case "S":
                            packginType = "X";
                            strShoveQuantity = row["Quantity"].ToString();
                            break;
                        case "A":
                            packginType = "C";
                            strCanQuantity = row["Quantity"].ToString();
                            break;
                        default:
                            break;
                    }
                    quantities.Add(String.Format("{0} {1}", row["Quantity"], packginType));
                }

                string text = template.Replace("{PalletCode}", palletCode);
                text = text.Replace("{CompanyName}", _companyName);
                text = text.Replace("{DeliveryDate}", _deliveryDate);
                text = text.Replace("{UserFullName}", _executor);
                text = text.Replace("{Carton}", strCartonQuantity);
                text = text.Replace("{Bag}", strBagQuantity);
                text = text.Replace("{Shove}", strShoveQuantity);
                text = text.Replace("{Can}", strCanQuantity);

                byte[] b = Encoding.UTF8.GetBytes(text);
                _serialPort.Write(b, 0, b.Length);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();
            }
        }

        public bool IsPrintPalletLabel
        {
            get { return _isPrintPalletLabel; }
        }

        public string CompanyName
        {
            set { _companyName = value; }
        }

        public string CompanyCode
        {
            set { _companyCode = value; }
        }

        public string DOImportCode
        {
            set { _doImportCode = value; }
        }

        public string Executor
        {
            set { _executor = value; }
        }

        public string DeliveryDate
        {
            set { _deliveryDate = value; }
        }

        private void PrintPalletLabelView_Load(object sender, EventArgs e)
        {
            lblCurrentActionInfo.Text = "KH: " + _companyName;
        }
    }
}