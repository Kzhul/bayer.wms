﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.IO.Ports;
using Bayer.WMS.Delivery.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Views
{
    public partial class PINView : Form
    {
        private string _strAction;//ChangePIN,ResetPIN
        private string _currentPIN;

        public PINView()
        {
            InitializeComponent();
        }

        public void PINView_Closing(object sender, CancelEventArgs e)
        {

        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //1.If change after next login = 1, ResetPIN 
                //Only show new PIN

                //2.If User want to change their PIN
                //Show All, verify OLD PIN

                //1.
                if (_strAction == "ResetPIN")
                {
                    if (txtNewPIN.Text.Length != 4)
                    {
                        lblStepHints.Text = "Mã PIN phải bao gồm 4 kí tự số";
                        return;
                    }

                    try
                    {
                        int intNewPin = Convert.ToInt32(txtNewPIN.Text);
                    }
                    catch
                    {
                        lblStepHints.Text = "Mã PIN phải bao gồm 4 kí tự số";
                        return;
                    }

                    if (txtNewPIN.Text != txtConfirmPIN.Text)
                    {
                        lblStepHints.Text = "Mã PIN và nhập lại mã PIN không khớp";
                        return;
                    }
                }
                else if (_strAction == "ChangePIN")
                {
                    if (txtCurrentPIN.Text != Utility.UserPIN)
                    {
                        lblStepHints.Text = "Mã PIN hiện tại không đúng";
                        txtCurrentPIN.Focus();
                        txtCurrentPIN.SelectAll();
                        return;
                    }

                    if (txtNewPIN.Text.Length != 4)
                    {
                        lblStepHints.Text = "Mã PIN phải bao gồm 4 kí tự số";
                        return;
                    }

                    try
                    {
                        int intNewPin = Convert.ToInt32(txtNewPIN.Text);
                    }
                    catch
                    {
                        lblStepHints.Text = "Mã PIN phải bao gồm 4 kí tự số";
                        return;
                    }

                    if (txtNewPIN.Text != txtConfirmPIN.Text)
                    {
                        lblStepHints.Text = "Mã PIN và nhập lại mã PIN không khớp";
                        return;
                    }               
                }

                if (Utility.debugMode)
                {
                    Utility.WriteLog(Utility.UserPIN);
                    Utility.WriteLog(txtNewPIN.Text);
                    Utility.WriteLog(Utility.UserID.ToString());
                }

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("CurrentPIN", Utility.UserPIN));
                listParam.Add(new SqlParameter("NewPIN", txtNewPIN.Text));
                listParam.Add(new SqlParameter("UserID", Utility.UserID));
                listParam.Add(new SqlParameter("SitemapID", 0));
                listParam.Add(new SqlParameter("Method", "ChangePIN"));
                Utility.LoadDataFromStore("proc_Users_ChangePIN", listParam);
                lblStepHints.Text = "Cập nhật mã PIN thành công";
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                txtNewPIN.Focus();
                txtNewPIN.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public string StrAction
        {
            set { _strAction = value; }
        }

        public string CurrentPIN
        {
            set { _currentPIN = value; }
        }

        private void PINView_Load(object sender, EventArgs e)
        {
            //1.If change after next login = 1, ResetPIN 
            //Only show new PIN

            //2.If User want to change their PIN
            //Show All, verify OLD PIN

            //1.
            if (_strAction == "ResetPIN")
            {
                txtCurrentPIN.Visible = false;
                lblCurrentPIN.Visible = false;
            }
        }
    }
}