﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Threading;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ProductReturnView : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        private bool _isProcess = false;
        private Queue<string> _queue;

        private DataTable dtProduct;
        private string _documentNBR = string.Empty;
        private string _userInfo = "Trả hàng TP cho khách hành";
        #endregion

        #region InitForm
        public ProductReturnView()
        {
            InitializeComponent();
            InitDataGridView();
            dtProduct = new DataTable();
            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã phiếu",                                
                "Quét mã Thùng/SP để trả cho KH",
            };
            lblStepHints.Text = _stepHints[1];
            _userInfo = "Trả sản phẩm: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            lblStepHints.Text = _stepHints[0];

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void ProductReturnView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(dtProduct.Rows[e.Row]["ReturnQty"].ToString());
            decimal exportedQty = decimal.Parse(dtProduct.Rows[e.Row]["DeliveredQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, CheckCellEquals));            
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL yêu cầu", "ReturnQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL đã trả", "DeliveredQty", 60, "N0", CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "BatchCode", 60, String.Empty, CheckCellEquals));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCellEquals));            
            dtgPallet.TableStyles.Add(dtgStyle);


            var dtgStyleProduct = new DataGridTableStyle { MappingName = "ListProduct" };
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Mã Thùng", "CartonBarcode", 60, "N3", CheckCellEquals));        
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Mã SP Lẻ", "ProductBarcode", 60, "N3", CheckCellEquals));
            //dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("SL", "DeliveredQty", 60, "N3", CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, String.Empty, CheckCellEquals)); 
            dtgProduct.TableStyles.Add(dtgStyleProduct);
        }
        #endregion

        #region BarcodeReader
        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;
                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);
            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }
        #endregion

        #region ActionOnScreen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                QueueProcess(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        private void dtgPallet_DoubleClick(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }
        #endregion

        #region ProcessBarcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (string.IsNullOrEmpty(_documentNBR))
                {
                    _documentNBR = barcode;
                    lblCurrentTopic.Text = _documentNBR;
                    ProcessDocumentBarcode(barcode);
                }
                else
                {
                    ProcessProductBarcode(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;

                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void ProcessDocumentBarcode(string barcode)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", barcode));
                DataTable dt = Utility.LoadDataFromStore("proc_ProductReturn_SelectProduct_ByDocument", listParam);
                dt.TableName = "PalletSummary";
                bdsPallet.DataSource = null;
                bdsPallet.DataSource = dt;
                dtProduct = dt;

                DataTable dt2 = Utility.LoadDataFromStore("proc_ProductReturn_SelectProductDelivered_ByDocument", listParam);
                dt2.TableName = "ListProduct";
                bdsProduct.DataSource = null;
                bdsProduct.DataSource = dt2;

                lblStepHints.Text = _stepHints[1];
                tabControl1.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void ProcessProductBarcode(string barcode)
        {
            try
            {
                if (string.IsNullOrEmpty(_documentNBR))
                {
                    throw new Exception("Vui lòng quét mã phiếu.");
                }

                #region Verify Prepared Quantity
                DataTable dt = Company.VerifyProductReturnQuantity(_documentNBR, barcode);
                if (dt.Rows.Count > 0)
                {
                    string mes = string.Empty;

                    if (dtProduct.Select(String.Format("ProductID = '{0}'", dt.Rows[0]["ProductID"].ToString())).Length == 0)
                    {
                        mes = "Sản phẩm " + dt.Rows[0]["ProductName"].ToString() + " không yêu cầu trả";
                    }
                    else
                    {
                        mes =
                            "Vượt quá số lượng yêu cầu" + Environment.NewLine
                            + "SL: " + dt.Rows[0]["PreparedQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                            + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                            + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                            ;
                    }

                    Utility.PlayErrorSound();
                    MessageBox.Show(mes, "Lỗi");
                    return;
                }
                #endregion

                #region InsertData
                try
                {
                    string encryptedBarcode = barcode;
                    string type;
                    barcode = Barcode.Process(encryptedBarcode, false, true, true, out type);
                    var method = this.GetType().GetMethod("ProcessBarcode");
                    Pallet.ProductReturnUpdateDeliveredProduct(_documentNBR, barcode, "ProcessBarcode");
                }
                catch
                {
                    throw new Exception("Mã không hợp lệ");
                }
                #endregion

                #region Reload Data
                ProcessDocumentBarcode(_documentNBR);
                #endregion
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }
        #endregion

        private void ProductReturnView_Load(object sender, EventArgs e)
        {
            if (Utility.debugMode)
            {
                //ProcessBarcode("SR18012101");
                //ProcessBarcode("ZW1_PZ_PZ1-01");
                //ProcessBarcode("180121R02");
                //ProcessBarcode("PW071700347");
            }
        }
    }
}