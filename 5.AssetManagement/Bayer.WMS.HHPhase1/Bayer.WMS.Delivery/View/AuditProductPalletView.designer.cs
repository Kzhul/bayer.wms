﻿namespace Bayer.WMS.Handheld.Views
{
    partial class AuditProductPalletView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPallet = new System.Windows.Forms.Label();
            this.bdsPalletSummary = new System.Windows.Forms.BindingSource(this.components);
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.lblProductLot = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgPallet = new System.Windows.Forms.DataGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bdsPalletDetailProduct = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPalletDetailProduct = new System.Windows.Forms.DataGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.bdsPalletDone = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPalletDone = new System.Windows.Forms.DataGrid();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDetailProduct)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDone)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 21);
            this.label1.Text = "Pallet:";
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(49, 27);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(188, 21);
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã Pallet để bắt đầu\r\nQuét mã vị trí để xác nhận";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.menuItem3);
            this.menuItem1.MenuItems.Add(this.menuItem4);
            this.menuItem1.MenuItems.Add(this.menuItem5);
            this.menuItem1.Text = "Ghi nhận";
            // 
            // menuItem3
            // 
            this.menuItem3.Text = "1.Sai số lô, số lượng";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Text = "2.Cần kiểm kê lại sau";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Xác nhận đúng";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 21);
            this.label2.Text = "Vị trí:";
            // 
            // lblProductLot
            // 
            this.lblProductLot.Location = new System.Drawing.Point(49, 48);
            this.lblProductLot.Name = "lblProductLot";
            this.lblProductLot.Size = new System.Drawing.Size(188, 21);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(0, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 167);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgPallet);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(240, 144);
            this.tabPage1.Text = "1.Hàng";
            // 
            // dtgPallet
            // 
            this.dtgPallet.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPallet.DataSource = this.bdsPalletSummary;
            this.dtgPallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallet.Location = new System.Drawing.Point(0, 0);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.RowHeadersVisible = false;
            this.dtgPallet.Size = new System.Drawing.Size(240, 144);
            this.dtgPallet.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgPalletDetailProduct);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(232, 141);
            this.tabPage2.Text = "2.DS barcode";
            // 
            // dtgPalletDetailProduct
            // 
            this.dtgPalletDetailProduct.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPalletDetailProduct.DataSource = this.bdsPalletDetailProduct;
            this.dtgPalletDetailProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPalletDetailProduct.Location = new System.Drawing.Point(0, 0);
            this.dtgPalletDetailProduct.Name = "dtgPalletDetailProduct";
            this.dtgPalletDetailProduct.RowHeadersVisible = false;
            this.dtgPalletDetailProduct.Size = new System.Drawing.Size(232, 141);
            this.dtgPalletDetailProduct.TabIndex = 2;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dtgPalletDone);
            this.tabPage4.Location = new System.Drawing.Point(0, 0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(232, 141);
            this.tabPage4.Text = "4.DS đã làm";
            // 
            // dtgPalletDone
            // 
            this.dtgPalletDone.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPalletDone.DataSource = this.bdsPalletDone;
            this.dtgPalletDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPalletDone.Location = new System.Drawing.Point(0, 0);
            this.dtgPalletDone.Name = "dtgPalletDone";
            this.dtgPalletDone.RowHeadersVisible = false;
            this.dtgPalletDone.Size = new System.Drawing.Size(232, 141);
            this.dtgPalletDone.TabIndex = 3;
            // 
            // menuItem5
            // 
            this.menuItem5.Text = "3.Cần in lại nhãn pallet";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // AuditProductPalletView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblProductLot);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.lblPallet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "AuditProductPalletView";
            this.Text = "Xe nâng";
            this.Closed += new System.EventHandler(this.SetupLocationView_Closed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.SetupLocationView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDetailProduct)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletDone)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsPalletSummary;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblProductLot;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGrid dtgPallet;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGrid dtgPalletDone;
        private System.Windows.Forms.BindingSource bdsPalletDone;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.BindingSource bdsPalletDetailProduct;
        private System.Windows.Forms.DataGrid dtgPalletDetailProduct;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem5;
    }
}