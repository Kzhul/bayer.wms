﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Threading;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Bayer.WMS.Handheld.Views
{
    public partial class StockReturnMaterialView : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        private bool _isProcess = false;
        private Queue<string> _queue;

        private DataTable dtProduct;
        private string _documentNBR = string.Empty;
        private int _lineNBR = 0;
        private string _locationCode = string.Empty;
        private string _locationName = string.Empty;
        private string _palletCode = string.Empty;
        private string _userInfo = "Nhập nguyên liệu trả";
        #endregion

        #region InitForm
        public StockReturnMaterialView()
        {
            InitializeComponent();
            InitDataGridView();
            dtProduct = new DataTable();
            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã phiếu",    
                "Quét mã Pallet",
                "Quét mã vị trí tạm",
                "Bấm xác nhận để nhập kho"
            };
            lblStepHints.Text = _stepHints[1];
            _userInfo = "Nhập hàng trả: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            lblStepHints.Text = _stepHints[0];

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void StockReturnView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(dtProduct.Rows[e.Row]["ReturnQty"].ToString());
            decimal exportedQty = decimal.Parse(dtProduct.Rows[e.Row]["ReceivedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 80, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Vị trí", "LocationCode", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "PalletQuantity", 60, "N3", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgPallet.TableStyles.Add(dtgStyle);


            var dtgStyleProduct = new DataGridTableStyle { MappingName = "ListProduct" };
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("SL trả", "ReturnQty", 60, "N3", CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("SL đã lấy", "ReceivedQty", 60, "N3", CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, String.Empty, CheckCellEquals));            
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Trạng thái", "Status", 60, String.Empty, CheckCellEquals));
            dtgProduct.TableStyles.Add(dtgStyleProduct);
        }
        #endregion

        #region BarcodeReader
        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;
                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);
            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }
        #endregion

        #region ActionOnScreen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                QueueProcess(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        private void dtgPallet_DoubleClick(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }
        #endregion

        #region ProcessBarcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (string.IsNullOrEmpty(_documentNBR))
                {
                    ProcessDocumentBarcode(barcode);
                }               
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                {
                    ProcessPalletBarcode(barcode);
                }
                else if (string.IsNullOrEmpty(_locationCode))
                {
                    ProcessLocationBarcode(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;

                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void ProcessDocumentBarcode(string barcode)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", barcode));
                DataTable dt = Utility.LoadDataFromStore("proc_StockReturn_SelectPallet_ByDocument", listParam);
                dt.TableName = "PalletSummary";
                bdsPallet.DataSource = null;
                bdsPallet.DataSource = dt;

                _documentNBR = barcode;

                if (string.IsNullOrEmpty(_locationName))
                {
                    lblCurrentTopic.Text = _documentNBR;
                }
                else
                {
                    lblCurrentTopic.Text = _documentNBR + " ,VT: " + _locationName;
                }
                lblStepHints.Text = _stepHints[1];
                tabControl1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void ProcessLocationBarcode(string barcode)
        {
            try
            {
                if (string.IsNullOrEmpty(_documentNBR))
                {
                    throw new Exception("Vui lòng quét mã phiếu.");
                }

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("LocationCode", barcode));

                DataTable dt = Utility.LoadDataFromStore("proc_ZoneLocations_Select", listParam);
                if (dt.Rows.Count == 0)
                {
                    throw new Exception("Mã vị trí không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }
                else
                {
                    _locationCode = barcode;
                    _locationName = dt.Rows[0]["LocationName"].ToString();
                    lblCurrentTopic.Text = _documentNBR + " ,VT: " + _locationName;
                    lblStepHints.Text = _stepHints[2];
                    tabControl1.SelectedIndex = 1;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                if (string.IsNullOrEmpty(_documentNBR))
                {
                    throw new Exception("Vui lòng quét mã phiếu.");
                }

                _palletCode = barcode;
                var pl = Pallet.Load(barcode);

                //Load data theo Pallet, lấy từ trong phiếu
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", _documentNBR));
                listParam.Add(new SqlParameter("PalletCode", barcode));

                dtProduct = Utility.LoadDataFromStore("proc_StockReturn_SelectPalletDetail", listParam);
                dtProduct.TableName = "ListProduct";
                bdsProduct.DataSource = null;
                bdsProduct.DataSource = dtProduct;

                if (dtProduct.Rows.Count == 0)
                {
                    throw new Exception("Mã phiếu không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }
        #endregion

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (string.IsNullOrEmpty(_documentNBR))
                {
                    throw new Exception("Vui lòng quét mã phiếu.");
                }
                if (string.IsNullOrEmpty(_palletCode))
                {
                    throw new Exception("Vui lòng quét mã pallet.");
                }
                if (string.IsNullOrEmpty(_locationCode))
                {
                    throw new Exception("Vui lòng quét vị trí tạm.");
                }

                //Update
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", _documentNBR));
                listParam.Add(new SqlParameter("LocationCode", _locationCode));
                listParam.Add(new SqlParameter("PalletCode", _palletCode));
                listParam.Add(new SqlParameter("UserID", Utility.UserID));
                listParam.Add(new SqlParameter("SitemapID", Utility.SitemapID));
                listParam.Add(new SqlParameter("Method", "StockReturn_ConfirmPallet"));

                DataTable dt = Utility.LoadDataFromStore("proc_StockReturn_ConfirmPallet", listParam);

                //Reload Data
                ProcessDocumentBarcode(_documentNBR);
                lblStepHints.Text = "Đã xác nhận " + _palletCode.ToString() + ". Quét mã Pallet để làm tiếp";
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void StockReturnView_Load(object sender, EventArgs e)
        {
            if (Utility.debugMode)
            {
                //ProcessBarcode("SR18012101");
                //ProcessBarcode("ZW1_PZ_PZ1-01");
                //ProcessBarcode("180121R02");
                //ProcessBarcode("PW071700347");
            }
        }
    }
}