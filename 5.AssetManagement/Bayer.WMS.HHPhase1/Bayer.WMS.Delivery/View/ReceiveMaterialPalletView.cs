﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Threading;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ReceiveMaterialPalletView : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        private bool _isProcess = false;
        private Queue<string> _queue;

        private DataTable dtProduct;
        private DataTable _dtDO;
        private string _documentNBR = string.Empty;
        private int _lineNBR = 0;
        private string _locationCode = string.Empty;
        private string _locationName = string.Empty;
        private string _palletCode = string.Empty;
        private string _productLot = string.Empty;
        private string _productLotNbr = string.Empty;
        private int _productID = 0;
        private int _pickedQuantity = 0;
        private int _requestQuantity = 0;
        private int _packageQuantity = 0;
        private int _quantity = 0;
        private string _userInfo = "Nhập hàng";
        #endregion

        #region InitForm
        public ReceiveMaterialPalletView()
        {
            InitializeComponent();
            InitDataGridView();
            dtProduct = new DataTable();
            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Quét mã phiếu",     
                "Quét mã vị trí",
                "Quét mã Lô",
                "Quét mã Pallet",
                "Điền số lượng. Bấm xác nhận để nhập kho"
            };
            lblStepHints.Text = _stepHints[1];
            _userInfo = "Nhập hàng: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            lblStepHints.Text = _stepHints[0];

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            LoadListDocument();
        }

        public void ReceiveMaterialPalletView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(dtProduct.Rows[e.Row]["RequestQty"].ToString());
            decimal exportedQty = decimal.Parse(dtProduct.Rows[e.Row]["PickedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckDOCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtDO.Rows[e.Row]["QuantityPlanning"].ToString());
            decimal exportedQty = decimal.Parse(_dtDO.Rows[e.Row]["QuantityReceived"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "ID", 25, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 80, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Vị trí", "LocationCode", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "PalletQuantity", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgPallet.TableStyles.Add(dtgStyle);


            var dtgStyleProduct = new DataGridTableStyle { MappingName = "ListProduct" };
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("#", "ID", 25, String.Empty, null));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("SL cần", "NeedQty", 60, "N0", CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, String.Empty, CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("SL YC", "RequestQty", 60, "N0", CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("SL đã lấy", "PickedQty", 60, "N0", CheckCellEquals));
            dtgStyleProduct.GridColumnStyles.Add(Utility.AddColumn("Trạng thái", "Status", 60, String.Empty, CheckCellEquals));
            dtgProduct.TableStyles.Add(dtgStyleProduct);

            #region dtgDO
            var dtgDOStyle = new DataGridTableStyle { MappingName = "DocumentList" };
            dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Ngày giao", "DeliveryDate", 80, String.Empty, CheckDOCellEquals));
            dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Mã phiếu", "StockReceivingCode", 100, String.Empty, CheckDOCellEquals));
            dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Công ty", "CompanyName", 100, String.Empty, CheckDOCellEquals));
            dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("SL yêu cầu", "QuantityPlanning", 80, String.Empty, CheckDOCellEquals));
            dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("SL đã nhập", "QuantityReceived", 80, String.Empty, CheckDOCellEquals));
            dtgListDocument.TableStyles.Add(dtgDOStyle);
            #endregion
        }
        #endregion

        #region BarcodeReader
        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;
                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);
            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }
        #endregion

        #region ActionOnScreen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                QueueProcess(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        private void dtgPallet_DoubleClick(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }
        #endregion

        #region ProcessBarcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (Utility.ReceivingMaterialDocumentRegex.IsMatch(barcode))
                {
                    ProcessDocumentBarcode(barcode);
                }
                else if (Utility.LocationCodeRegex.IsMatch(barcode))
                {
                    ProcessLocationBarcode(barcode);
                }
                //else if (Utility.ProductLotRegex.IsMatch(barcode))
                //{
                //    _productLotNbr = string.Empty;
                //    ProcessProductLotBarcode(barcode);
                //}
                else if (Utility.ProductLot4Regex.IsMatch(barcode))
                {
                    _productLotNbr = string.Empty;
                    _productLotNbr = barcode;
                    Match match = Regex.Match(barcode, @"\d{6}\w\d{2}");
                    if (match.Success)
                    {
                        barcode = match.Value;
                    }
                    ProcessProductLotBarcode(barcode);
                }
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                {
                    ProcessPalletBarcode(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;

                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void ProcessDocumentBarcode(string barcode)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", barcode));

                dtProduct = Utility.LoadDataFromStore("proc_StockReceiving_Select_ByDocument", listParam);
                dtProduct.TableName = "ListProduct";
                bdsProduct.DataSource = null;
                bdsProduct.DataSource = dtProduct;

                if (dtProduct.Rows.Count == 0)
                {
                    throw new Exception("Mã phiếu không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }

                DataTable dt = Utility.LoadDataFromStore("proc_StockReceiving_SelectPallet_ByDocument", listParam);
                dt.TableName = "PalletSummary";
                bdsPallet.DataSource = null;
                bdsPallet.DataSource = dt;

                _documentNBR = barcode;

                if (string.IsNullOrEmpty(_locationName))
                {
                    lblCurrentTopic.Text = _documentNBR;
                }
                else
                {
                    lblCurrentTopic.Text = _documentNBR + " ,VT: " + _locationName;
                }
                lblStepHints.Text = _stepHints[1];
                tabControl1.SelectedIndex = 1;

                lblProductLot.Text = "Mã lô: ";
                lblProduct.Text = "Sản phẩm: ";
                lblPallet.Text = "Pallet: ";
                txtQuantity.Text = "";
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void ProcessLocationBarcode(string barcode)
        {
            try
            {
                if (string.IsNullOrEmpty(_documentNBR))
                {
                    throw new Exception("Vui lòng quét mã phiếu.");
                }

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("LocationCode", barcode));

                DataTable dt = Utility.LoadDataFromStore("proc_ZoneLocations_Select", listParam);
                if (dt.Rows.Count == 0)
                {
                    throw new Exception("Mã vị trí không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }
                else
                {
                    _locationCode = barcode;
                    _locationName = dt.Rows[0]["LocationName"].ToString();
                    lblCurrentTopic.Text = _documentNBR + " ,VT: " + _locationName;
                    lblStepHints.Text = _stepHints[2];
                    tabControl1.SelectedIndex = 1;

                    lblProductLot.Text = "Mã lô: ";
                    lblProduct.Text = "Sản phẩm: ";
                    lblPallet.Text = "Pallet: ";
                    txtQuantity.Text = "";

                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void ProcessProductLotBarcode(string barcode)
        {
            try
            {
                if (string.IsNullOrEmpty(_documentNBR))
                {
                    throw new Exception("Vui lòng quét mã phiếu.");
                }

                //if (!string.IsNullOrEmpty(_locationCode))
                //{
                //    if (MessageBox.Show("Bạn có chắc lô hàng đang nằm ở vị trí: " + _locationName,"", MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button1) == DialogResult.No)
                //    {
                //        _locationCode = string.Empty;
                //        _locationName = string.Empty;
                //        lblCurrentTopic.Text = _documentNBR + " ,VT: "; 
                //    }
                //}

                _productLot = barcode;
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", _documentNBR));
                listParam.Add(new SqlParameter("ProductLot", _productLot));
                listParam.Add(new SqlParameter("ProductLotNbr", _productLotNbr));
                string strCheck = Utility.ExecuteScalarFromStore("proc_StockReceiving_CheckProductLot_ByDocument", listParam);

                if (strCheck == "-1")
                {
                    throw new Exception("Mã lô với số thứ tự " + _productLotNbr + " đã được quét.");
                }
                else if (strCheck == "-2")
                {
                    throw new Exception("Mã lô không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }

                listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", _documentNBR));
                listParam.Add(new SqlParameter("ProductLot", _productLot));
                DataTable dt = Utility.LoadDataFromStore("proc_StockReceiving_SelectProductLot_ByDocument", listParam);

                _packageQuantity = Convert.ToInt32(dt.Rows[0]["PackageQuantity"].ToString());
                _productID = Convert.ToInt32(dt.Rows[0]["ProductID"].ToString());
                _lineNBR = Convert.ToInt32(dt.Rows[0]["LineNbr"].ToString());
                _pickedQuantity = Convert.ToInt32(dt.Rows[0]["PickedQty"].ToString());
                _requestQuantity = Convert.ToInt32(dt.Rows[0]["RequestQty"].ToString());

                lblProductLot.Text = "Mã lô: " + _productLot + "-" + _productLotNbr;
                lblProduct.Text = "Sản phẩm: " + dt.Rows[0]["ProductCode"].ToString() + "- " + dt.Rows[0]["ProductName"].ToString()
                    + Environment.NewLine + "SL đã lấy: " + dt.Rows[0]["PickedQty"].ToString() + " / " + dt.Rows[0]["RequestQty"].ToString()
                    + Environment.NewLine + "QC pallet: " + dt.Rows[0]["PackageQuantity"].ToString();
                lblStepHints.Text = _stepHints[3];

                tabControl1.SelectedIndex = 0;
                lblPallet.Text = "Pallet: ";
                txtQuantity.Text = "";
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                if (string.IsNullOrEmpty(_documentNBR))
                {
                    throw new Exception("Vui lòng quét mã phiếu.");
                }
                if (string.IsNullOrEmpty(_productLot))
                {
                    throw new Exception("Vui lòng quét mã lô.");
                }

                var pl = Pallet.Load(barcode);
                if (pl.Status != "N" && pl.Status != "B")
                {
                    throw new Exception("Không phải Pallet trống");
                }
                else
                {
                    _palletCode = barcode;
                    lblPallet.Text = "Pallet: " + _palletCode;

                    //Số lượng chưa lấy lớn hơn số lượng đóng gói
                    //Mặc định hiển thị số lượng đóng gói
                    if ((_requestQuantity - _pickedQuantity) > _packageQuantity)
                    {
                        txtQuantity.Text = _packageQuantity.ToString();
                    }
                    //Số lượng chưa lấy nhỏ hơn số lượng đóng gói
                    //Hiển thị số lượng còn lại ở pallet cuối này
                    else if (_requestQuantity > _pickedQuantity)
                    {
                        txtQuantity.Text = (_requestQuantity - _pickedQuantity).ToString();
                    }
                    //Số lượng đã lấy lớn hơn số lượng yêu cầu
                    //Nhắc user bạn đã lấy đủ số lượng, bạn có muốn tiếp tục
                    else if (_requestQuantity < _pickedQuantity)
                    {
                        if (MessageBox.Show("Bạn đã nhập đủ số lượng, bạn có muốn tiếp tục ?", "Quá số lượng", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            txtQuantity.Text = _packageQuantity.ToString();
                        }
                        else
                        {
                            txtQuantity.Text = string.Empty;
                        }
                    }

                    mniConfirm.Enabled = true;
                    lblStepHints.Text = _stepHints[4];
                    tabControl1.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void LoadListDocument()
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                bdsListDocument.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_StockReceiving_SelectDocument", listParam);
                a.TableName = "DocumentList";
                _dtDO = a;
                bdsListDocument.DataSource = a;
                tabControl1.SelectedIndex = 3;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (string.IsNullOrEmpty(_documentNBR))
                {
                    throw new Exception("Vui lòng quét mã phiếu.");
                }
                if (string.IsNullOrEmpty(_productLot))
                {
                    throw new Exception("Vui lòng quét mã lô.");
                }
                if (string.IsNullOrEmpty(_palletCode))
                {
                    throw new Exception("Vui lòng quét mã pallet.");
                }
                if (string.IsNullOrEmpty(_locationCode))
                {
                    throw new Exception("Vui lòng quét vị trí.");
                }

                //Get value
                _quantity = Convert.ToInt32(txtQuantity.Text);
                if (_quantity > _packageQuantity)
                {
                    throw new Exception("Không được chất hàng quá số lượng");
                }

                Pallet.SetLocation(_palletCode, _locationCode, string.Empty);

                //Update
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DocumentNbr", _documentNBR));
                listParam.Add(new SqlParameter("LineNbr", _lineNBR));
                listParam.Add(new SqlParameter("LocationCode", _locationCode));
                listParam.Add(new SqlParameter("PalletCode", _palletCode));
                listParam.Add(new SqlParameter("ProductLot", _productLot));
                listParam.Add(new SqlParameter("ProductLotNbr", _productLotNbr));
                listParam.Add(new SqlParameter("ProductID", _productID));
                listParam.Add(new SqlParameter("Quantity", _quantity));
                listParam.Add(new SqlParameter("UserID", Utility.UserID));
                listParam.Add(new SqlParameter("SitemapID", Utility.SitemapID));
                listParam.Add(new SqlParameter("Method", "StockReceiving_ConfirmPallet"));

                DataTable dt = Utility.LoadDataFromStore("proc_StockReceiving_ConfirmPallet_2", listParam);

                //Reload Data
                ProcessDocumentBarcode(_documentNBR);
                //ProcessProductLotBarcode(_productLot);

                lblStepHints.Text = "Đã xác nhận " + _palletCode.ToString() + " ,Số lô " + _productLotNbr + " ,Số lượng " + _quantity.ToString() + ". Quét mã lô để làm tiếp";
                _productLot = string.Empty;
                _palletCode = string.Empty;
                lblProductLot.Text = "Mã lô: ";
                lblProduct.Text = "Sản phẩm: ";
                lblPallet.Text = "Pallet: ";


                mniConfirm.Enabled = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ReceiveMaterialPalletView_Load(object sender, EventArgs e)
        {

            if (Utility.debugMode)
            {
                //ProcessBarcode("SR18041003");
                //ProcessBarcode("TO2_T1_T11-01");
                //ProcessBarcode("180410R04-001");
                //ProcessBarcode("PW071700347");
            }
        }

        private void dtgListDocument_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgListDocument[dtgListDocument.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
    }
}