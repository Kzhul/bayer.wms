﻿namespace Bayer.WMS.Handheld.Views
{
    partial class PrintPalletLabelView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsPalletSummary = new System.Windows.Forms.BindingSource(this.components);
            this.dtgConfirmQty = new System.Windows.Forms.DataGrid();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.lblCurrentActionInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgConfirmQty
            // 
            this.dtgConfirmQty.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgConfirmQty.DataSource = this.bdsPalletSummary;
            this.dtgConfirmQty.Location = new System.Drawing.Point(0, 53);
            this.dtgConfirmQty.Name = "dtgConfirmQty";
            this.dtgConfirmQty.RowHeadersVisible = false;
            this.dtgConfirmQty.Size = new System.Drawing.Size(240, 165);
            this.dtgConfirmQty.TabIndex = 5;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 5);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 11;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 221);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 47);
            this.lblStepHints.Text = "Quét mã Pallet để in nhãn";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.miExit);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = " ";
            // 
            // miExit
            // 
            this.miExit.Text = "Thoát";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // lblCurrentActionInfo
            // 
            this.lblCurrentActionInfo.Location = new System.Drawing.Point(3, 29);
            this.lblCurrentActionInfo.Name = "lblCurrentActionInfo";
            this.lblCurrentActionInfo.Size = new System.Drawing.Size(234, 21);
            // 
            // PrintPalletLabelView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblCurrentActionInfo);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.dtgConfirmQty);
            this.Menu = this.mainMenu1;
            this.Name = "PrintPalletLabelView";
            this.Text = "In nhãn pallet";
            this.Load += new System.EventHandler(this.PrintPalletLabelView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.PrintPalletLabelView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid dtgConfirmQty;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsPalletSummary;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.Label lblCurrentActionInfo;
    }
}