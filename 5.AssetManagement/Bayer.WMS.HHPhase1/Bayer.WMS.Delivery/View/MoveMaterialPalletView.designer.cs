﻿namespace Bayer.WMS.Handheld.Views
{
    partial class MoveMaterialPalletView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblPallet = new System.Windows.Forms.Label();
            this.bdsListAsset = new System.Windows.Forms.BindingSource(this.components);
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.mniConfirm = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.bdsListStatus = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAssetName = new System.Windows.Forms.Label();
            this.lblAssetCode = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgListAsset = new System.Windows.Forms.DataGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bdsListInventory = new System.Windows.Forms.BindingSource(this.components);
            this.dtgListInventory = new System.Windows.Forms.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.bdsListAsset)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsListStatus)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bdsListInventory)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 14;
            // 
            // lblPallet
            // 
            this.lblPallet.Location = new System.Drawing.Point(3, 27);
            this.lblPallet.Name = "lblPallet";
            this.lblPallet.Size = new System.Drawing.Size(234, 21);
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 221);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 47);
            this.lblStepHints.Text = "Quét mã tài sản";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.mniConfirm);
            this.mainMenu1.MenuItems.Add(this.mniExit);
            // 
            // mniConfirm
            // 
            this.mniConfirm.Text = "Xác nhận";
            this.mniConfirm.Click += new System.EventHandler(this.mniConfirm_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniCancel_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.tabControl1.Location = new System.Drawing.Point(0, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(240, 167);
            this.tabControl1.TabIndex = 25;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.comboBox1);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.lblAssetName);
            this.tabPage3.Controls.Add(this.lblAssetCode);
            this.tabPage3.Location = new System.Drawing.Point(0, 0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(240, 144);
            this.tabPage3.Text = "Chi tiết tài sản";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.bdsListStatus;
            this.comboBox1.Location = new System.Drawing.Point(71, 119);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(165, 22);
            this.comboBox1.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 21);
            this.label1.Text = "Tình trạng:";
            // 
            // lblAssetName
            // 
            this.lblAssetName.Location = new System.Drawing.Point(1, 21);
            this.lblAssetName.Name = "lblAssetName";
            this.lblAssetName.Size = new System.Drawing.Size(237, 95);
            this.lblAssetName.Text = "Tên:";
            // 
            // lblAssetCode
            // 
            this.lblAssetCode.Location = new System.Drawing.Point(3, -3);
            this.lblAssetCode.Name = "lblAssetCode";
            this.lblAssetCode.Size = new System.Drawing.Size(237, 21);
            this.lblAssetCode.Text = "Mã tài sản:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgListAsset);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(232, 141);
            this.tabPage1.Text = "1.Danh sách kiểm kê";
            // 
            // dtgListAsset
            // 
            this.dtgListAsset.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgListAsset.DataSource = this.bdsListAsset;
            this.dtgListAsset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgListAsset.Location = new System.Drawing.Point(0, 0);
            this.dtgListAsset.Name = "dtgListAsset";
            this.dtgListAsset.RowHeadersVisible = false;
            this.dtgListAsset.Size = new System.Drawing.Size(232, 141);
            this.dtgListAsset.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgListInventory);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(232, 141);
            this.tabPage2.Text = "2.Kỳ kiểm kê";
            // 
            // dtgListInventory
            // 
            this.dtgListInventory.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgListInventory.DataSource = this.bdsListInventory;
            this.dtgListInventory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgListInventory.Location = new System.Drawing.Point(0, 0);
            this.dtgListInventory.Name = "dtgListInventory";
            this.dtgListInventory.RowHeadersVisible = false;
            this.dtgListInventory.Size = new System.Drawing.Size(232, 141);
            this.dtgListInventory.TabIndex = 1;
            this.dtgListInventory.DoubleClick += new System.EventHandler(this.dtgListInventory_DoubleClick);
            // 
            // MoveMaterialPalletView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.lblPallet);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "MoveMaterialPalletView";
            this.Text = "Kiểm kê";
            this.Load += new System.EventHandler(this.MoveMaterialPalletView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MovePalletView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsListAsset)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsListStatus)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bdsListInventory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblPallet;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsListAsset;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem mniConfirm;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGrid dtgListAsset;
        private System.Windows.Forms.DataGrid dtgListInventory;
        private System.Windows.Forms.BindingSource bdsListInventory;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label lblAssetName;
        private System.Windows.Forms.Label lblAssetCode;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource bdsListStatus;
        private System.Windows.Forms.Label label1;
    }
}