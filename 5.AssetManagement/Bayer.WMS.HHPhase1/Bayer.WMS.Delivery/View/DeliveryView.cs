﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading;
using Bayer.WMS.Delivery.Models;
using System.IO;

namespace Bayer.WMS.Handheld.Views
{
    public partial class DeliveryView : Form
    {
        #region Param
        protected BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;

        private bool _isProcess = false;
        private Queue<string> _queue;
        private Company _company;
        private Pallet _currentPallet;

        private bool dataChanged = false;
        private DataTable _dt;
        private DataTable _dtProductByCompany;
        private DataTable _dtProductByPallet;
        private DataTable _dtCompany;
        private DataTable _dtDO;
        private string _userInfo = "Giao hàng";
        private string _palletInfo = string.Empty;
        private string _doImportCode = string.Empty;
        #endregion

        #region InitForm
        public DeliveryView()
        {
            InitializeComponent();
            InitDataGridView();
            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên",
                "Quét mã khách hàng",
                "Quét mã pallet và bấm xác nhận để xuất hàng",
            };
            _company = new Company();
            _currentPallet = new Pallet();
            _dt = new DataTable();
            _dtProductByCompany = new DataTable();
            _dtProductByPallet = new DataTable();
            _dtCompany = new DataTable();
            _dtDO = new DataTable();
            _queue = new Queue<string>();

            _userInfo = "GH: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            lblStepHints.Text = _stepHints[1];
            LoadListDO();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);

            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isProcess)
                    QueueProcess(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }

        private void DeliveryView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }
        #endregion

        #region MENU BUTTON
        public void mniExit_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        //Xác nhận giao hàng pallet này cho KH
        private void menuItem3_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_currentPallet.PalletCode))
            {
                //if (_currentPallet.Status != "P")
                //{
                //    Utility.PlayErrorSound();
                //    MessageBox.Show("Trạng thái pallet không đúng.");
                //    return;
                //}

                if (_currentPallet.CompanyCode != _company.CompanyCode)
                {
                    Utility.PlayErrorSound();
                    MessageBox.Show("Pallet này không thuộc về khách hàng đang giao.");
                    return;
                }

                #region Verify Prepared Quantity
                DataTable dt = Company.VerifyPreparedQuantity(_currentPallet.PalletCode, _currentPallet.PalletCode);
                if (dt.Rows.Count > 0)
                {
                    string mes = string.Empty;

                    if (_dtProductByCompany.Select(String.Format("ProductID = '{0}'", dt.Rows[0]["ProductID"].ToString())).Length == 0)
                    {
                        mes = "Sản phẩm " + dt.Rows[0]["ProductName"].ToString() + " không yêu cầu trong DO";
                    }
                    else if (_dtProductByCompany.Select(String.Format("ProductLot = '{0}'", dt.Rows[0]["ProductLot"].ToString())).Length == 0)
                    {
                        mes = "Số lô " + dt.Rows[0]["ProductLot"].ToString() + " không yêu cầu trong DO";
                    }
                    else
                    {
                        mes =
                            "Vượt quá số lượng yêu cầu" + Environment.NewLine
                            + "SL: " + dt.Rows[0]["NextQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                            + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                            + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                            ;
                    }

                    Utility.PlayErrorSound();
                    MessageBox.Show(mes, "Lỗi");
                    return;
                }
                #endregion

                Pallet.UpdateUpdate_Delivery2(_currentPallet.PalletCode, "ConfirmDeliveredPallet");
            }

            #region Load lại data 2 table
            ProcessCustomerBarcode(_company.CompanyCode);
            #endregion

            lblStepHints.Text = _stepHints[2];
        }
        #endregion

        #region Init
        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["DeliveredQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckPalletCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtProductByPallet.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtProductByPallet.Rows[e.Row]["DeliveredQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckDOCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtDO.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtDO.Rows[e.Row]["DeliveredQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckCompanyCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtCompany.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtCompany.Rows[e.Row]["DeliveredQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            try
            {
                #region dtgPallets
                var dtgStyle = new DataGridTableStyle { MappingName = "PalletList" };
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã", "PalletCode", 75, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("TT", "StrStatus", 100, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Người dùng", "UserFullName", 150, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Ngày giao", "StrDeliveryDate", 100, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng", "CartonQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Bao", "BagQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Xô", "ShoveQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Can", "CanQuantity", 40, String.Empty, null));
                dtgPallets.TableStyles.Add(dtgStyle);
                #endregion

                #region dtgProducts
                var dtgProductStyle = new DataGridTableStyle { MappingName = "ProductList" };
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL(DO)", "DOQuantity", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL_còn_lại", "NeedPrepareQuantity", 60, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 20, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCellEquals));
                dtgProducts.TableStyles.Add(dtgProductStyle);
                #endregion

                #region dtgPalletInfo
                var dtgPalletInfoStyle = new DataGridTableStyle { MappingName = "CartonList" };
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã", "CartonBarcode", 90, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 40, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
                dtgPalletInfo.TableStyles.Add(dtgPalletInfoStyle);
                #endregion

                #region dtgPalletDetailProduct
                var dtgPalletDetailProductStyle = new DataGridTableStyle { MappingName = "PalletDetailProduct" };
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 40, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL_còn_lại", "NeedPrepareQuantity", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProduct.TableStyles.Add(dtgPalletDetailProductStyle);
                #endregion

                #region dtgCustomer
                var dtgCustomerStyle = new DataGridTableStyle { MappingName = "CustomerList" };
                dtgCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Mã Cty", "CompanyCode", 80, String.Empty, CheckCompanyCellEquals));
                dtgCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Tên Cty", "CompanyName", 200, String.Empty, CheckCompanyCellEquals));
                dtgCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Ngày", "StrDeliveryDate", 80, String.Empty, CheckCompanyCellEquals));
                dtgCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("DO", "DOQuantity", 50, String.Empty, CheckCompanyCellEquals));
                dtgCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Soạn", "PreparedQty", 50, String.Empty, CheckCompanyCellEquals));
                dtgCustomerStyle.GridColumnStyles.Add(Utility.AddColumn("Giao", "DeliveredQty", 50, String.Empty, CheckCompanyCellEquals));
                dtgCustomer.TableStyles.Add(dtgCustomerStyle);
                #endregion

                #region dtgDO
                var dtgDOStyle = new DataGridTableStyle { MappingName = "DOList" };
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Ngày giao", "DeliveryDate", 80, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Mã DO", "DOImportCode", 100, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("SL_DO", "DOQuantity", 50, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Soạn", "PreparedQty", 50, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Giao", "DeliveredQty", 50, String.Empty, CheckDOCellEquals));
                dtgDO.TableStyles.Add(dtgDOStyle);
                #endregion

                ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ProcessBarcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (string.IsNullOrEmpty(_doImportCode))
                {
                    _doImportCode = barcode;
                    LoadListCompany(_doImportCode);
                    ShowHideTab();
                    this.Text = "GH: " + _doImportCode;
                }
                else if (Utility.CustomerRegex.IsMatch(barcode))
                {
                    barcode = barcode.Remove(0, 3);
                    ProcessCustomerBarcode(barcode);
                }
                else if (Utility.DeliveryRegex.IsMatch(barcode))
                {
                    ProcessDeliveryBarcode(barcode);
                }
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessCustomerBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                dataChanged = false;

                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");

                _company = Company.Load(barcode);
                lblCurrentActionInfo.Text = _company.CompanyName;

                LoadPalletByCompany();
                LoadProductByCompany();

                bdsPalletInfo.DataSource = null;
                bdsPalletDetailProduct.DataSource = null;

                tpPrepareInfo.SelectedIndex = 1;

                lblStepHints.Text = "Quét mã pallet / thùng / lẻ";
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessDeliveryBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                dataChanged = false;

                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");

                _company = Company.SelectCompany(_doImportCode, string.Empty, barcode);
                lblCurrentActionInfo.Text = _company.CompanyName;

                LoadPalletByCompany();
                LoadProductByCompany();

                bdsPalletInfo.DataSource = null;
                bdsPalletDetailProduct.DataSource = null;

                tpPrepareInfo.SelectedIndex = 1;

                lblStepHints.Text = "Quét mã pallet / thùng / lẻ";
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");

                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");

                //// get quantity details of pallet
                _currentPallet = Pallet.Load(barcode);

                if (_currentPallet.Status != "V")
                {
                    MessageBox.Show(
                        "Trạng thái pallet không đúng, bạn chỉ được giao pallet Đã Soạn");
                    _currentPallet = null;
                    return;
                }


                //Validate Current Customer vs Pallet Customer
                if (!string.IsNullOrEmpty(_currentPallet.CompanyCode) && (_currentPallet.CompanyCode != _company.CompanyCode))
                {
                    MessageBox.Show(
                        "Pallet này không thuộc về khách hàng đang giao."
                        + Environment.NewLine
                        + "Vui lòng chọn đúng pallet."
                    );
                    _currentPallet = null;
                    return;
                }

               
                LoadPalletInfo(barcode);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void LoadPalletByCompany()
        {
            dataChanged = false;

            List<SqlParameter> listParam = new List<SqlParameter>();
            listParam.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
            listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
            bdsPallets.DataSource = null;
            DataTable a = Utility.LoadDataFromStore("proc_Pallets_SelectDeliveryByCompany", listParam);
            a.TableName = "PalletList";
            bdsPallets.DataSource = a;
        }

        public void LoadProductByCompany()
        {
            dataChanged = false;

            List<SqlParameter> listParam = new List<SqlParameter>();
            listParam.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
            listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
            bdsProduct.DataSource = null;
            _dtProductByCompany = new DataTable();
            _dtProductByCompany = Utility.LoadDataFromStore("proc_PalletStatuses_SelectDeliveredByCompany_GroupByBarCode", listParam);
            _dtProductByCompany.TableName = "ProductList";
            bdsProduct.DataSource = _dtProductByCompany;
        }

        public void LoadPalletInfo(string barcode)
        {
            if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
            {
                //Tạm thời bỏ ra để xem được pallet đã giao hàng

                //if (_currentPallet.Status != "P")
                //{
                //    Utility.PlayErrorSound();
                //    MessageBox.Show("Trạng thái pallet không đúng.");
                //    return;
                //}

                if (_currentPallet.CompanyCode != _company.CompanyCode)
                {
                    Utility.PlayErrorSound();
                    MessageBox.Show("Pallet này không thuộc về khách hàng đang giao.");
                    return;
                }

                #region bdsPalletInfo
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("Barcode", barcode));
                listParam.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
                listParam.Add(new SqlParameter("Type", "P"));
                bdsPalletInfo.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_PalletStatuses_SelectDeliveryByPallet", listParam);
                a.TableName = "CartonList";
                bdsPalletInfo.DataSource = a;
                _dt = a;
                #endregion


                #region bdsPalletDetailProduct
                List<SqlParameter> listParam2 = new List<SqlParameter>();
                listParam2.Add(new SqlParameter("Barcode", barcode));
                listParam2.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                listParam2.Add(new SqlParameter("DOImportCode", _doImportCode));
                bdsPalletDetailProduct.DataSource = null;
                DataTable b = Utility.LoadDataFromStore("proc_PalletStatuses_SelectDeliveryByPallet_GroupByBarCode", listParam2);
                b.TableName = "PalletDetailProduct";
                bdsPalletDetailProduct.DataSource = b;
                _dtProductByPallet = b;
                #endregion

                #region ThungBaoXoCan
                try
                {
                    List<SqlParameter> listParam3 = new List<SqlParameter>();
                    listParam3.Add(new SqlParameter("Barcode", barcode));
                    listParam3.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                    listParam3.Add(new SqlParameter("DOImportCode", _doImportCode));
                    DataTable c = Utility.LoadDataFromStore("proc_Pallets_SelectDeliveryByPallet_SumByPackingType", listParam3);
                    _palletInfo = c.Rows[0]["StrQuantity"].ToString();
                }
                catch
                {

                }
                #endregion

                //Nếu đây là pallet mới và có SP
                if (_currentPallet != null && _currentPallet.Status == "N" && b.Rows.Count > 0)
                {
                    lblStepHints.Text = _stepHints[3];
                }

                tpPrepareInfo.SelectedIndex = 2;
            }
        }

        public void LoadListCompany(string barcode)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("DOImportCode", barcode));
                bdsCustomer.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_Prepare_SelectByCompany", listParam);
                a.TableName = "CustomerList";
                _dtCompany = a;
                bdsCustomer.DataSource = a;

                tpPrepareInfo.SelectedIndex = 4;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ActionOnScreen
        private void dtgPallets_DoubleClick(object sender, EventArgs e)
        {
            //MessageBox.Show(dtgPallets[dtgPallets.CurrentRowIndex, 0].ToString());
            txtBarcode.Text = dtgPallets[dtgPallets.CurrentRowIndex, 1].ToString();
            QueueProcess(txtBarcode.Text);
        }

        private void tpPrepareInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region ForReloadData
            if (dataChanged)
            {
                //Nếu check vào 2 tab tổng của KH, load lại list pallet và sl
                if (tpPrepareInfo.SelectedIndex <= 1)
                {
                    dataChanged = false;

                    #region Load lại data 2 table
                    ProcessCustomerBarcode(_company.CompanyCode);
                    #endregion
                }
            }
            #endregion

            if (tpPrepareInfo.SelectedIndex <= 1)
            {
                if (_company != null && !string.IsNullOrEmpty(_company.CompanyName))
                {
                    lblCurrentActionInfo.Text = "KH:" + _company.CompanyName;
                    this.Text = _userInfo;
                }
            }
            else
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    lblCurrentActionInfo.Text = _currentPallet.PalletCode + ": " + _palletInfo;
                    //this.Text = _palletInfo;
                }
            }
        }

        private void dtgCustomer_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = "KH_" + dtgCustomer[dtgCustomer.CurrentRowIndex, 0].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        private void dtgDO_DoubleClick(object sender, EventArgs e)
        {
            txtBarcode.Text = dtgDO[dtgDO.CurrentRowIndex, 1].ToString();
            ProcessBarcode(txtBarcode.Text);
        }

        private void DeliveryView_Load(object sender, EventArgs e)
        {
            ShowHideTab();
        }

        private void ShowHideTab()
        {
            if (string.IsNullOrEmpty(_doImportCode))
            {
                //tpDO.Show();
                //tpListPallets.Hide();
                //tpPallet.Hide();
                //tpPalletProduct.Hide();
                //tpPrepareInfo.Hide();
                //tpProducts.Hide();
                //tpPrepareInfo.TabPages[5].Show();
                //tpPrepareInfo.Refresh();
            }
            else
            {
                //tpDO.Hide();
                //tpListPallets.Show();
                //tpPallet.Show();
                //tpPalletProduct.Show();
                //tpPrepareInfo.Show();
                //tpProducts.Show();
                //tpPrepareInfo.TabPages[5].Hide();
                tpPrepareInfo.Controls.Remove(this.tpDO);
                tpPrepareInfo.Refresh();
                //this.ResumeLayout(false);
            }
        }

        public void LoadListDO()
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                bdsDO.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_Prepare_SelectDO", listParam);
                a.TableName = "DOList";
                _dtDO = a;
                bdsDO.DataSource = a;

                tpPrepareInfo.SelectedIndex = 5;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
    }
}