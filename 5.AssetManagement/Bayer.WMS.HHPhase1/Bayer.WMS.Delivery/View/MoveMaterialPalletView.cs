﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Threading;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Views
{
    public partial class MoveMaterialPalletView : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;
        private bool _isProcess = false;
        private Queue<string> _queue;
        public string _InventoryCode = string.Empty;
        public string _AssetCode = string.Empty;
        public string _Status = string.Empty;
        private string _userInfo = "Kiểm kê";
        #endregion

        #region InitForm
        public MoveMaterialPalletView()
        {
            InitializeComponent();
            InitDataGridView();
            _queue = new Queue<string>();
            _stepHints = new List<string> 
            { 
                "Chọn kỳ kiểm kê",
                "Quét mã tài sản",
                "Chọn lý do và bấm xác nhận",
            };
            lblStepHints.Text = _stepHints[0];
            _userInfo = "Kiểm kê: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            lblStepHints.Text = _stepHints[0];
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void MovePalletView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void InitDataGridView()
        {
            var dtgStyleAssetList = new DataGridTableStyle { MappingName = "AssetList" };
            dtgStyleAssetList.GridColumnStyles.Add(Utility.AddColumn("#", "LineNbr", 25, String.Empty, null));
            dtgStyleAssetList.GridColumnStyles.Add(Utility.AddColumn("Mã tài sản", "AssetCode", 80, String.Empty, null));
            dtgStyleAssetList.GridColumnStyles.Add(Utility.AddColumn("Tên", "AssetName", 150, string.Empty, null));
            dtgStyleAssetList.GridColumnStyles.Add(Utility.AddColumn("CostCenter", "CostCenter", 80, String.Empty, null));
            dtgStyleAssetList.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "Description", 80, String.Empty, null));
            dtgStyleAssetList.GridColumnStyles.Add(Utility.AddColumn("Ngày kiểm", "StrCheckDate", 80, String.Empty, null));
            dtgStyleAssetList.GridColumnStyles.Add(Utility.AddColumn("Người kiểm", "StrCheckBy", 100, String.Empty, null));
            dtgStyleAssetList.GridColumnStyles.Add(Utility.AddColumn("Trạng thái", "StrStatus", 100, String.Empty, null));
            dtgListAsset.TableStyles.Add(dtgStyleAssetList);

            var dtgStyleInventoryList = new DataGridTableStyle { MappingName = "InventoryList" };
            dtgStyleInventoryList.GridColumnStyles.Add(Utility.AddColumn("Mã kỳ kiểm kê", "InventoryCode", 80, String.Empty, null));
            dtgStyleInventoryList.GridColumnStyles.Add(Utility.AddColumn("Tên", "InventoryName", 150, string.Empty, null));
            dtgStyleInventoryList.GridColumnStyles.Add(Utility.AddColumn("Từ ngày", "StrFromDate", 60, String.Empty, null));
            dtgStyleInventoryList.GridColumnStyles.Add(Utility.AddColumn("Đến ngày", "StrToDate", 60, String.Empty, null));
            dtgListInventory.TableStyles.Add(dtgStyleInventoryList);
        }
        #endregion

        #region BarcodeReader
        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;
                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);
            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }
        #endregion

        #region ActionOnScreen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                List<SqlParameter> listParam2 = new List<SqlParameter>();
                listParam2.Add(new SqlParameter("InventoryCode", _InventoryCode));
                listParam2.Add(new SqlParameter("AssetCode", _AssetCode));
                listParam2.Add(new SqlParameter("MachineCode", Utility.MachineCode));
                listParam2.Add(new SqlParameter("Status", comboBox1.SelectedValue.ToString()));
                listParam2.Add(new SqlParameter("UserID", Utility.UserID));
                listParam2.Add(new SqlParameter("SitemapID", "0"));
                listParam2.Add(new SqlParameter("Method ", "Mobile_CheckDuplicate"));
                Utility.LoadDataFromStore("proc_Inventory_InventoryManagement_InsertCheckResult", listParam2);

                lblStepHints.Text = "Lưu thành công. Quét mã tài sản để làm tiếp";
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }
        #endregion

        #region ProcessBarcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else
                {
                    _AssetCode = barcode;
                    LoadAsset();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;

                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }
        #endregion

        public void ClearScreen()
        {
            bdsListAsset.DataSource = null;
            bdsListInventory.DataSource = null;
            lblPallet.Text = string.Empty;
            lblStepHints.Text = _stepHints[0];
        }

        public void LoadAsset()
        {
            #region CheckDuplicate
            List<SqlParameter> listParam = new List<SqlParameter>();
            listParam.Add(new SqlParameter("InventoryCode", _InventoryCode));
            listParam.Add(new SqlParameter("AssetCode", _AssetCode));
            listParam.Add(new SqlParameter("UserID", Utility.UserID));
            listParam.Add(new SqlParameter("SitemapID", "0"));
            listParam.Add(new SqlParameter("Method ", "Mobile_CheckDuplicate"));
            DataTable dt = Utility.LoadDataFromStore("proc_Inventory_InventoryManagement_CheckDuplicate", listParam);
            if (dt.Rows.Count > 0)
            {
                Utility.PlayErrorSound();

                MessageBox.Show("Tài sản đã được kiểm kê:"
                    + Environment.NewLine + "Mã tài sản: " + dt.Rows[0]["AssetCode"].ToString()
                    + Environment.NewLine + "Tên: " + dt.Rows[0]["AssetName"].ToString()
                    + Environment.NewLine + "Người kiểm: " + dt.Rows[0]["StrCheckBy"].ToString()
                    + Environment.NewLine + "Ngày kiểm: " + dt.Rows[0]["StrCheckDate"].ToString()
                    + Environment.NewLine + "Tình trạng: " + dt.Rows[0]["StrStatus"].ToString()
                    );

                return;
            }
            #endregion

            #region LoadDataToScreen
            List<SqlParameter> listParam2 = new List<SqlParameter>();
            listParam2.Add(new SqlParameter("AssetCode", _AssetCode));
            listParam2.Add(new SqlParameter("UserID", Utility.UserID));
            listParam2.Add(new SqlParameter("SitemapID", "0"));
            listParam2.Add(new SqlParameter("Method ", "Mobile_LoadAsset"));
            DataTable b = Utility.LoadDataFromStore("proc_Inventory_InventoryManagement_GetAssetDetail", listParam2);
            if (b.Rows.Count > 0)
            {
                lblAssetCode.Text = "Mã tài sản: " + b.Rows[0]["AssetCode"].ToString();
                lblAssetName.Text = "Tên: " + b.Rows[0]["AssetName"].ToString()
                                    + Environment.NewLine + "Cost Center: " + b.Rows[0]["CostCenter"].ToString()
                                    + Environment.NewLine + b.Rows[0]["Description"].ToString();
                comboBox1.SelectedIndex = 0;
                tabControl1.SelectedIndex = 0;
                lblStepHints.Text = _stepHints[2];
            }
            else
            {
                lblStepHints.Text = "Mã tài sản không tồn tại. Vui lòng kiểm tra lại danh sách kiểm kê.";
                Utility.PlayErrorSound();
            }
            #endregion
        }

        public void LoadListAsset()
        {
            try
            {
                if (!string.IsNullOrEmpty(_InventoryCode))
                {
                    List<SqlParameter> listParam2 = new List<SqlParameter>();
                    listParam2.Add(new SqlParameter("InventoryCode", _InventoryCode));
                    listParam2.Add(new SqlParameter("MachineCode", Utility.MachineCode));
                    listParam2.Add(new SqlParameter("UserID", Utility.UserID));
                    listParam2.Add(new SqlParameter("SitemapID", "0"));
                    listParam2.Add(new SqlParameter("Method ", "Mobile_LoadListAsset"));
                    DataTable b = Utility.LoadDataFromStore("proc_Inventory_InventoryManagement_GetAssets", listParam2);
                    b.TableName = "AssetList";
                    bdsListAsset.DataSource = null;
                    bdsListAsset.DataSource = b;

                    //tabControl1.SelectedIndex = 1;
                    lblStepHints.Text = _stepHints[1];
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        private void MoveMaterialPalletView_Load(object sender, EventArgs e)
        {
            LoadDataCombobox();
            LoadListDO();
        }

        private void LoadDataCombobox()
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                DataTable dt = Utility.LoadDataFromStore("proc_Inventory_InventoryManagement_SelectStatus", listParam);
                bdsListStatus.DataSource = null;
                bdsListStatus.DataSource = dt;

                comboBox1.DisplayMember = "RefName";
                comboBox1.ValueMember = "RefValue";
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void LoadListDO()
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                DataTable a = Utility.LoadDataFromStore("proc_Inventory_InventoryManagement_SelectInventory", listParam);
                a.TableName = "InventoryList";
                bdsListInventory.DataSource = null;
                bdsListInventory.DataSource = a;
                tabControl1.SelectedIndex = 2;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void ShowHideTab()
        {
            if (string.IsNullOrEmpty(_InventoryCode))
            {

            }
            else
            {
                tabControl1.Controls.Remove(this.tabPage2);
                tabControl1.Refresh();
            }
        }

        private void dtgListInventory_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgListInventory[dtgListInventory.CurrentRowIndex, 0].ToString();
                _InventoryCode = txtBarcode.Text;
                LoadListAsset();
                ShowHideTab();
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tabControl1.SelectedIndex == 1)
                {
                    LoadListAsset();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }
    }
}