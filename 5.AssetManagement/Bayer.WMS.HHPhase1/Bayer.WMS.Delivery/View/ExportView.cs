﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading;
using Bayer.WMS.Delivery.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ExportView : Form
    {
        #region Param
        protected BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;

        private bool _isProcess = false;
        private Queue<string> _queue;
        private Company _company;
        private Pallet _currentPallet;
        private DataTable _dt;
        private DataTable _dtProductByCompany;
        private DataTable _dtProductByPallet;
        private DataTable _dtCompany;
        private DataTable _dtDO;
        private bool dataChanged = false;
        private string _userInfo = "Xuất hàng theo DO";
        private string _palletInfo = string.Empty;
        private string _doImportCode = string.Empty;
        #endregion

        #region InitForm
        public ExportView()
        {
            try
            {
                InitializeComponent();
                InitDataGridView();
                this.KeyPreview = true;
                _queue = new Queue<string>();
                _stepHints = new List<string> 
                { 
                    "Quét mã nhân viên",
                    "Quét mã khách hàng",
                    "Quét mã pallet có hàng để xuất chẳn" + Environment.NewLine + "Quét mã pallet trống để xuất lẻ",
                    "Bấm xác nhận để xuất pallet này cho khách hàng",
                };

                lblStepHints.Text = _stepHints[1];
                _company = new Company();
                _currentPallet = new Pallet();
                _dt = new DataTable();
                _dtProductByCompany = new DataTable();
                _dtProductByPallet = new DataTable();
                _dtCompany = new DataTable();
                _dtDO = new DataTable();
                _queue = new Queue<string>();

                _userInfo = "SH_DO: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
                this.Text = _userInfo;

                LoadListDO();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);

            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isProcess)
                    QueueProcess(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }
        #endregion

        #region MENU BUTTON
        public void mniClearPallet_Click(object sender, EventArgs e)
        {
            clearPallet();
        }

        public void clearPallet()
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.ClearPalletView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[2];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergePallet_Click(object sender, EventArgs e)
        {
            moveMaterialPallet();
        }

        public void moveMaterialPallet()
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveMaterialPalletView())
                {
                    view.ShowDialog();
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[2];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniMergeCarton_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                //using (var view = new Bayer.WMS.Handheld.Views.MergeCartonView())
                //{
                //    view.ShowDialog();
                //}

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

                lblStepHints.Text = _stepHints[2];
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        public void mniExit_Click(object sender, EventArgs e)
        {
            exitForm();
        }

        public void exitForm()
        {
            try
            {
                DisposeBarcodeReader();
                this.Close();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        //Xác nhận xuất lấy luôn pallet này cho KH
        private void menuConfirmPallet_Click(object sender, EventArgs e)
        {
            confirmPallet();
        }

        private void confirmPallet()
        {
            try
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    if (_currentPallet.Status == "A" || _currentPallet.Status == "N")
                    {
                        #region Verify Prepared Quantity
                        DataTable dt = Company.VerifyExportedQuantity(_doImportCode, _company.CompanyCode, _currentPallet.PalletCode);
                        if (dt.Rows.Count > 0)
                        {
                            string mes = string.Empty;

                            if (_dtProductByCompany.Select(String.Format("ProductID = '{0}'", dt.Rows[0]["ProductID"].ToString())).Length == 0)
                            {
                                mes = "Sản phẩm " + dt.Rows[0]["ProductName"].ToString() + " không yêu cầu trong DO";
                            }
                            else if (_dtProductByCompany.Select(String.Format("ProductLot = '{0}'", dt.Rows[0]["ProductLot"].ToString())).Length == 0)
                            {
                                mes = "Số lô " + dt.Rows[0]["ProductLot"].ToString() + " không yêu cầu trong DO";
                            }
                            else
                            {
                                mes =
                                    "Vượt quá số lượng yêu cầu" + Environment.NewLine
                                    + "SL: " + dt.Rows[0]["NextQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                                    + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                                    + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                                    ;
                            }

                            Utility.PlayErrorSound();
                            MessageBox.Show(mes, "Lỗi");
                            return;
                        }
                        #endregion

                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_ExportStatus(_currentPallet.PalletCode, _doImportCode, _doImportCode, _company.CompanyCode, "X", method.Name);

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                    else
                    {
                        MessageBox.Show("Trạng thái pallet không đúng, không thể xác nhận.");
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void menuPreparePallet_Click(object sender, EventArgs e)
        {
            try
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    if (_currentPallet.Status == "N")
                    {
                        #region Verify Prepared Quantity
                        DataTable dt = Company.VerifyExportedQuantity(_doImportCode, _company.CompanyCode, _currentPallet.PalletCode);
                        if (dt.Rows.Count > 0)
                        {
                            string mes = string.Empty;

                            if (_dtProductByCompany.Select(String.Format("ProductID = '{0}'", dt.Rows[0]["ProductID"].ToString())).Length == 0)
                            {
                                mes = "Sản phẩm " + dt.Rows[0]["ProductName"].ToString() + " không yêu cầu trong DO";
                            }
                            else if (_dtProductByCompany.Select(String.Format("ProductLot = '{0}'", dt.Rows[0]["ProductLot"].ToString())).Length == 0)
                            {
                                mes = "Số lô " + dt.Rows[0]["ProductLot"].ToString() + " không yêu cầu trong DO";
                            }
                            else
                            {
                                mes =
                                    "Vượt quá số lượng yêu cầu" + Environment.NewLine
                                    + "SL: " + dt.Rows[0]["ExportedQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                                    + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                                    + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                                    ;
                            }

                            Utility.PlayErrorSound();
                            MessageBox.Show(mes, "Lỗi");
                            return;
                        }
                        #endregion

                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_ExportStatus(_currentPallet.PalletCode, _doImportCode, _doImportCode, _company.CompanyCode, "A", method.Name);

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                    else
                    {
                        MessageBox.Show("Trạng thái pallet không đúng, không thể xác nhận.");
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        //Trả lại pallet
        private void menuReturnPallet_Click(object sender, EventArgs e)
        {
            returnPallet();
        }

        private void returnPallet()
        {
            try
            {
                //Check trạng thái
                // && (_currentPallet.Status == "P" || _currentPallet.Status == "T")
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    //Xác nhận có muốn thực hiện hay ko
                    if (DialogResult.Yes == MessageBox.Show("Bạn có chắc muốn thu hồi lại pallet: " + _currentPallet.PalletCode + " ?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_ExportStatus(_currentPallet.PalletCode, string.Empty, string.Empty, string.Empty, "N", method.Name);

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        #endregion

                        lblStepHints.Text = _stepHints[2];
                    }
                }
                else
                {
                    MessageBox.Show("Trạng thái pallet không đúng, không thể thu hồi.");
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void menuPackaging_Click(object sender, EventArgs e)
        {
            packaging();
        }

        private void packaging()
        {
            try
            {
                DisposeBarcodeReader();
                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");
                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");
                if (String.IsNullOrEmpty(_currentPallet.PalletCode))
                    throw new Exception("Vui lòng quét mã pallet.");

                string cartonBarcode = Utility.GenCartonWarehouseBarcode();

                if (Utility.debugMode)
                {
                    MessageBox.Show(cartonBarcode);
                }

                if (!string.IsNullOrEmpty(cartonBarcode))
                {
                    using (var view = new PackagingView())
                    {
                        view.OddQty = 0;// _process.OddQty;
                        view.PrepareCode = "XH" + DateTime.Today.ToString("yyMMdd") + "01";//_process.ReferenceNbr;SH17092701
                        view.Pallet = _currentPallet;
                        view.DOImportCode = _doImportCode;
                        view.CartonBarcode = cartonBarcode;
                        view.CompanyCode = _company.CompanyCode;
                        view.CompanyName = _company.CompanyName;
                        view.Executor = Utility.CurrentUser.LastName + " " + Utility.CurrentUser.FirstName;
                        view.DeliveryDate = DateTime.Today;
                        view.ShowDialog();
                    }
                }

                #region Load lại data 2 table
                ProcessCustomerBarcode(_company.CompanyCode);
                #endregion

            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuQRCode_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.BarcodeTrackingView())
                {
                    view.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                DisposeBarcodeReader();

                using (var view = new Bayer.WMS.Handheld.Views.MoveProductPallet())
                {
                    view.ShowDialog();
                }
            }
            finally
            {
                ReInitBarcodeReader();
            }
        }
        #endregion

        #region Init
        public void CheckCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtProductByCompany.Rows[e.Row]["ExportedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckPalletCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtProductByPallet.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtProductByPallet.Rows[e.Row]["ExportedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void CheckDOCellEquals(object sender, DataGridEnableEventArgs e)
        {
            decimal requestQty = decimal.Parse(_dtDO.Rows[e.Row]["DOQuantity"].ToString());
            decimal exportedQty = decimal.Parse(_dtDO.Rows[e.Row]["ExportedQty"].ToString());

            if (exportedQty == 0)
                e.MeetsCriteria = 0;
            else if (exportedQty < requestQty)
                e.MeetsCriteria = 1;
            else if (exportedQty > requestQty)
                e.MeetsCriteria = 3;
            else
                e.MeetsCriteria = 2;
        }

        public void InitDataGridView()
        {
            try
            {
                #region dtgPallets
                var dtgStyle = new DataGridTableStyle { MappingName = "PalletList" };
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mã", "PalletCode", 75, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Trạng thái", "StrStatus", 100, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Người dùng", "UserFullName", 150, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng", "CartonQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Bao", "BagQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Xô", "ShoveQuantity", 40, String.Empty, null));
                dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Can", "CanQuantity", 40, String.Empty, null));

                dtgPallets.TableStyles.Add(dtgStyle);
                #endregion

                #region dtgProducts
                var dtgProductStyle = new DataGridTableStyle { MappingName = "ProductList" };
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 80, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL_còn_lại", "NeedExportQuantity", 60, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL(DO)", "DOQuantity", 40, String.Empty, CheckCellEquals));                           
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 20, String.Empty, CheckCellEquals));
                dtgProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckCellEquals));
                dtgProducts.TableStyles.Add(dtgProductStyle);
                #endregion

                #region dtgPalletInfo
                var dtgPalletInfoStyle = new DataGridTableStyle { MappingName = "CartonList" };
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 80, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã", "CartonBarcode", 90, String.Empty, null));
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 40, String.Empty, null));
                
                dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
                dtgPalletInfo.TableStyles.Add(dtgPalletInfoStyle);
                #endregion

                #region dtgPalletDetailProduct
                var dtgPalletDetailProductStyle = new DataGridTableStyle { MappingName = "PalletDetailProduct" };
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 80, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, CheckPalletCellEquals));                
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 40, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("SL_còn_lại", "NeedExportQuantity", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Thùng/ lẻ", "CartonOddQuantity", 40, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("QC", "PackingType", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProductStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, CheckPalletCellEquals));
                dtgPalletDetailProduct.TableStyles.Add(dtgPalletDetailProductStyle);
                #endregion

                #region dtgDO
                var dtgDOStyle = new DataGridTableStyle { MappingName = "DOList" };
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Ngày giao", "DeliveryDate", 80, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Mã DO", "DOImportCode", 100, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("SL_DO", "DOQuantity", 50, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Xuất", "ExportedQty", 50, String.Empty, CheckDOCellEquals));
                dtgDOStyle.GridColumnStyles.Add(Utility.AddColumn("Nhận", "ReceivedQty", 50, String.Empty, CheckDOCellEquals));
                dtgDO.TableStyles.Add(dtgDOStyle);
                #endregion

                ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
                _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ProcessBarcode
        public string currentBarCode = string.Empty;
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                currentBarCode = barcode;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                } 
                else if (string.IsNullOrEmpty(_doImportCode))
                {
                    _doImportCode = currentBarCode;
                    this.Text = "XH_DO: " + _doImportCode;
                    
                    ShowHideTab();

                    ProcessCustomerBarcode("9999999");
                }
                //// step 2: scan export note
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                    ProcessPalletBarcode(barcode);
                //// step 3: scan pallet/carton/product
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                string descr = String.Format("{0} - {1} - {2}", lblCurrentActionInfo.Text, barcode, ex.Message);
                Utility.PlayErrorSound();
                Utility.WriteLog(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessCustomerBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                dataChanged = false;

                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");

                _company = Company.Load(barcode);
                lblCurrentActionInfo.Text = _company.CompanyName;

                LoadPalletByCompany();
                LoadProductByCompany();

                bdsPalletInfo.DataSource = null;
                bdsPalletDetailProduct.DataSource = null;

                tpPrepareInfo.SelectedIndex = 1;

                lblStepHints.Text = "Quét mã pallet / thùng / lẻ";
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");

                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");

                //// get quantity details of pallet
                _currentPallet = Pallet.Load(barcode);

                //Validate Current Customer vs Pallet Customer
                if (!string.IsNullOrEmpty(_currentPallet.CompanyCode) && (_currentPallet.CompanyCode != _company.CompanyCode))
                {
                    MessageBox.Show(
                        "Pallet này không thuộc về khách hàng đang xuất."
                        + Environment.NewLine
                        + "Vui lòng chọn đúng pallet."
                    );
                    _currentPallet = null;
                    return;
                }

                LoadPalletInfo(barcode);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessProductBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                #region Validate Data
                if (String.IsNullOrEmpty(_doImportCode))
                    throw new Exception("Vui lòng quét mã DO.");
                if (String.IsNullOrEmpty(_company.CompanyCode))
                    throw new Exception("Vui lòng quét mã khách hàng.");
                if (String.IsNullOrEmpty(_currentPallet.PalletCode))
                    throw new Exception("Vui lòng quét mã pallet.");
                if (_dt.Select(String.Format("CartonBarcode = '{0}'", barcode)).Length > 0)
                    throw new Exception("Mã QR đã được quét, vui lòng kiểm tra lại");     
          
                #endregion

                #region Verify Prepared Quantity
                if (_currentPallet.Status != "N" && !string.IsNullOrEmpty(_currentPallet.CompanyCode))
                {
                    DataTable dt = Company.VerifyExportedQuantity(_doImportCode, _company.CompanyCode, barcode);
                    if (dt.Rows.Count > 0)
                    {
                        string mes = string.Empty;

                        if (_dtProductByCompany.Select(String.Format("ProductID = '{0}'", dt.Rows[0]["ProductID"].ToString())).Length == 0)
                        {
                            mes = "Sản phẩm " + dt.Rows[0]["ProductName"].ToString() + " không yêu cầu trong DO";
                        }
                        else if (_dtProductByCompany.Select(String.Format("ProductLot = '{0}'", dt.Rows[0]["ProductLot"].ToString())).Length == 0)
                        {
                            mes = "Số lô " + dt.Rows[0]["ProductLot"].ToString() + " không yêu cầu trong DO";
                        }
                        else
                        {
                            mes =
                                "Vượt quá số lượng yêu cầu" + Environment.NewLine
                                + "SL Quét: " + dt.Rows[0]["NextQuantity"].ToString() + Environment.NewLine
                                + "SL Cần: " + dt.Rows[0]["ExportedQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                                + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                                + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                                ;
                        }

                        Utility.PlayErrorSound();
                        MessageBox.Show(mes, "Lỗi");
                        return;
                    }
                }
                #endregion

                #region InsertData
                if (
                     Utility.CartonBarcode3Regex.IsMatch(barcode)
                    || Utility.CartonBarcode2Regex.IsMatch(barcode)
                    || Utility.CartonBarcodeRegex.IsMatch(barcode)
                    )
                {
                    var method = this.GetType().GetMethod("ProcessBarcode");
                    Pallet.UpdatePallet_ExportProduct(_currentPallet.PalletCode, barcode, "C", method.Name);
                }
                else
                {
                    try
                    {
                        string encryptedBarcode = barcode;
                        string type;
                        barcode = Barcode.Process(encryptedBarcode, false, false, true, out type);

                        //if (_dt.Select(String.Format("ProductBarcode = '{0}'", barcode)).Length > 0)
                        //    throw new Exception("Mã QR đã được quét, vui lòng kiểm tra lại");

                        var method = this.GetType().GetMethod("ProcessBarcode");
                        Pallet.UpdatePallet_ExportProduct(_currentPallet.PalletCode, barcode, "E", method.Name);
                    }
                    catch
                    {
                        throw new Exception("Mã không hợp lệ");
                    }
                }
                #endregion

                #region Reload Data
                LoadPalletInfo(_currentPallet.PalletCode);
                tpPrepareInfo.SelectedIndex = 2;

                dataChanged = true;
                #endregion
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void LoadPalletByCompany()
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
                bdsPallets.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_Pallets_SelectByCompany", listParam);
                a.TableName = "PalletList";
                bdsPallets.DataSource = a;

                tpPrepareInfo.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadProductByCompany()
        {
            try
            {
                dataChanged = false;

                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                listParam.Add(new SqlParameter("DOImportCode", _doImportCode));
                bdsProduct.DataSource = null;
                _dtProductByCompany = new DataTable();
                _dtProductByCompany = Utility.LoadDataFromStore("proc_Export_SelectProduct", listParam);
                _dtProductByCompany.TableName = "ProductList";
                bdsProduct.DataSource = _dtProductByCompany;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadPalletInfo(string barcode)
        {
            try
            {
                if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                {
                    #region bdsPalletInfo
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    listParam.Add(new SqlParameter("Barcode", barcode));
                    listParam.Add(new SqlParameter("Type", "P"));
                    bdsPalletInfo.DataSource = null;
                    DataTable a = Utility.LoadDataFromStore("proc_PalletStatuses_Select2", listParam);
                    a.TableName = "CartonList";
                    bdsPalletInfo.DataSource = a;
                    _dt = a;
                    #endregion
                    
                    #region bdsPalletDetailProduct
                    List<SqlParameter> listParam2 = new List<SqlParameter>();
                    listParam2.Add(new SqlParameter("Barcode", barcode));
                    listParam2.Add(new SqlParameter("CompanyCode", _company.CompanyCode));
                    listParam2.Add(new SqlParameter("DOImportCode", _doImportCode));
                    bdsPalletDetailProduct.DataSource = null;
                    DataTable b = Utility.LoadDataFromStore("proc_PalletStatuses_SelectByPallet_ForExport", listParam2);
                    b.TableName = "PalletDetailProduct";
                    bdsPalletDetailProduct.DataSource = b;
                    _dtProductByPallet = b;
                    #endregion

                    #region ThungBaoXoCan
                    try
                    {
                        List<SqlParameter> listParam3 = new List<SqlParameter>();
                        listParam3.Add(new SqlParameter("Barcode", barcode));
                        DataTable c = Utility.LoadDataFromStore("proc_Pallets_Select_SumByPackingType", listParam3);
                        _palletInfo = c.Rows[0]["StrQuantity"].ToString();
                    }
                    catch
                    {

                    }
                    #endregion

                    //Nếu đây là pallet mới và có SP
                    if (_currentPallet != null && _currentPallet.Status == "N" && b.Rows.Count > 0)
                    {
                        lblStepHints.Text = _stepHints[3];
                    }

                    tpPrepareInfo.SelectedIndex = 2;
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void LoadListDO()
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                bdsDO.DataSource = null;
                DataTable a = Utility.LoadDataFromStore("proc_Export_SelectDO", listParam);
                a.TableName = "DOList";
                _dtDO = a;
                bdsDO.DataSource = a;

                tpPrepareInfo.SelectedIndex = 4;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        #region ActionOnScreen
        private void dtgDO_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgDO[dtgDO.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void dtgPallets_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgPallets[dtgPallets.CurrentRowIndex, 1].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void tpPrepareInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                #region ForReloadData
                if (dataChanged)
                {
                    //Nếu check vào 2 tab tổng của KH, load lại list pallet và sl
                    if (tpPrepareInfo.SelectedIndex <= 1)
                    {
                        dataChanged = false;

                        #region Load lại data 2 table
                        ProcessCustomerBarcode(_company.CompanyCode);
                        #endregion
                    }
                }
                #endregion

                if (tpPrepareInfo.SelectedIndex <= 1)
                {
                    if (_company != null && !string.IsNullOrEmpty(_company.CompanyName))
                    {
                        lblCurrentActionInfo.Text = "KH:" + _company.CompanyName;
                        //this.Text = _userInfo;
                    }
                }
                else
                {
                    if (_currentPallet != null && !string.IsNullOrEmpty(_currentPallet.PalletCode))
                    {
                        lblCurrentActionInfo.Text = _currentPallet.PalletCode + ": " + _palletInfo;
                        //this.Text = _palletInfo;
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }
        #endregion

        private void ExportView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        private void ExportView_Load(object sender, EventArgs e)
        {
            ShowHideTab();
        }

        private void ShowHideTab()
        {
            if (string.IsNullOrEmpty(_doImportCode))
            {
                //tpDO.Show();
                //tpListPallets.Hide();
                //tpPallet.Hide();
                //tpPalletProduct.Hide();
                //tpPrepareInfo.Hide();
                //tpProducts.Hide();
                //tpPrepareInfo.TabPages[5].Show();
                //tpPrepareInfo.Refresh();
            }
            else
            {
                //tpDO.Hide();
                //tpListPallets.Show();
                //tpPallet.Show();
                //tpPalletProduct.Show();
                //tpPrepareInfo.Show();
                //tpProducts.Show();
                //tpPrepareInfo.TabPages[5].Hide();
                tpPrepareInfo.Controls.Remove(this.tpDO);
                tpPrepareInfo.Refresh();
                //this.ResumeLayout(false);
            }
        }
    }
}