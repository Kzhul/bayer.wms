﻿namespace Bayer.WMS.Handheld.Views
{
    partial class MergeCartonView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblCarton = new System.Windows.Forms.Label();
            this.bdsJoinCarton = new System.Windows.Forms.BindingSource(this.components);
            this.dtgPackaging = new System.Windows.Forms.DataGrid();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mniFinish = new System.Windows.Forms.MenuItem();
            this.mniDelete = new System.Windows.Forms.MenuItem();
            this.mniEndDelete = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblQty = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bdsJoinCarton)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 21);
            this.label1.Text = "Thùng:";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 18;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 17;
            // 
            // lblCarton
            // 
            this.lblCarton.Location = new System.Drawing.Point(59, 27);
            this.lblCarton.Name = "lblCarton";
            this.lblCarton.Size = new System.Drawing.Size(178, 21);
            // 
            // dtgPackaging
            // 
            this.dtgPackaging.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPackaging.DataSource = this.bdsJoinCarton;
            this.dtgPackaging.Location = new System.Drawing.Point(0, 72);
            this.dtgPackaging.Name = "dtgPackaging";
            this.dtgPackaging.RowHeadersVisible = false;
            this.dtgPackaging.Size = new System.Drawing.Size(240, 158);
            this.dtgPackaging.TabIndex = 23;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.miExit);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.mniFinish);
            this.menuItem1.MenuItems.Add(this.mniDelete);
            this.menuItem1.MenuItems.Add(this.mniEndDelete);
            this.menuItem1.Text = "Chức năng";
            // 
            // mniFinish
            // 
            this.mniFinish.Text = "Xác nhận";
            this.mniFinish.Click += new System.EventHandler(this.mniFinish_Click);
            // 
            // mniDelete
            // 
            this.mniDelete.Text = "Hủy";
            this.mniDelete.Click += new System.EventHandler(this.mniDelete_Click);
            // 
            // mniEndDelete
            // 
            this.mniEndDelete.Text = "Kết thúc hủy";
            this.mniEndDelete.Click += new System.EventHandler(this.mniEndDelete_Click);
            // 
            // miExit
            // 
            this.miExit.Text = "Thoát";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã thùng lẻ để thực hiện nhập thùng";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 21);
            this.label4.Text = "SL:";
            // 
            // lblQty
            // 
            this.lblQty.Location = new System.Drawing.Point(59, 48);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(178, 21);
            this.lblQty.Text = "0";
            // 
            // MergeCartonView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblQty);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.dtgPackaging);
            this.Controls.Add(this.lblCarton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBarcode);
            this.Menu = this.mainMenu1;
            this.Name = "MergeCartonView";
            this.Text = "Dồn thùng";
            this.Load += new System.EventHandler(this.MergeCartonView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MergeCartonView_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.bdsJoinCarton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblCarton;
        private System.Windows.Forms.DataGrid dtgPackaging;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.BindingSource bdsJoinCarton;
        private System.Windows.Forms.MenuItem mniDelete;
        private System.Windows.Forms.MenuItem mniEndDelete;
        private System.Windows.Forms.MenuItem mniFinish;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblQty;
    }
}