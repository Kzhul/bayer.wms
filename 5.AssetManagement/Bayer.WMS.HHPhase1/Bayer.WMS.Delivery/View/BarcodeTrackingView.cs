﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;

namespace Bayer.WMS.Handheld.Views
{
    public partial class BarcodeTrackingView : Form
    {
        private BarcodeReader _barcodeReader;

        public BarcodeTrackingView()
        {
            InitializeComponent();
            InitDataGridView();

            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);
        }

        public void BarcodeTrackingView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void miExit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public void InitDataGridView()
        {
            #region dtgInfo

            var dtgInfoStyle = new DataGridTableStyle { MappingName = "Info" };

            dtgInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Thông tin", "InfoName", 150, String.Empty, null));
            dtgInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Giá trị", "InfoValue", 200, String.Empty, null));

            dtgInfo.TableStyles.Add(dtgInfoStyle);

            #endregion
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                txtBarcode.Text = barcode;
                var dtInfo = Barcode.CheckAssetDetail(barcode);
                dtInfo.TableName = "Info";
                bdsInfo.DataSource = dtInfo;
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }
    }
}