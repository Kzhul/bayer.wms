﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Views
{
    public partial class AuditProductPalletView : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;

        public bool isPalletScanned = false;
        public bool isLocationScanned = false;
        private string _userInfo = "Kiểm kho TP";
        private string _currentWork = string.Empty;
        private string _locationCode = string.Empty;
        private string _locationName = string.Empty;
        private string _locationSuggestionCode = string.Empty;
        private string _locationSuggestionName = string.Empty;
        #endregion

        #region Form Init
        public AuditProductPalletView()
        {
            InitializeComponent();
            InitDataGridView();

            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên để bắt đầu",
                "Quét mã Pallet",
                "Pallet đã rời vị trí. Quét mã vị trí để đưa pallet lên kệ",
                "Bấm xác nhận"
            };
            lblStepHints.Text = _stepHints[1];
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);

            _userInfo = "Kiểm kho TP: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;
            LoadPalletDoneByUser();
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "IDS", 25, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL Thùng", "CartonQuantity", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Quy Cách", "PackageSize", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, string.Empty, null));
            dtgPallet.TableStyles.Add(dtgStyle);

            var dtgPalletInfoStyle = new DataGridTableStyle { MappingName = "CartonList" };
            dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
            dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã", "CartonBarcode", 90, String.Empty, null));
            dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 40, String.Empty, null));
            dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgPalletInfoStyle.GridColumnStyles.Add(Utility.AddColumn("Mã SP", "ProductCode", 60, String.Empty, null));
            dtgPalletDetailProduct.TableStyles.Add(dtgPalletInfoStyle);

            var dtgStyleDone = new DataGridTableStyle { MappingName = "PalletDone" };
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("#", "IDS", 25, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("SL Thùng", "CartonQuantity", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Vị trí", "LocationName", 90, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Dãy", "LineName", 60, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Khu vực", "ZoneName", 150, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Quy Cách", "PackageSize", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, string.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Thời Gian", "StrTime", 100, string.Empty, null));
            dtgPalletDone.TableStyles.Add(dtgStyleDone);
        }

        public void SetupLocationView_Closed(object sender, EventArgs e)
        {
            lblStepHints.Text = _stepHints[0];
        }

        public void SetupLocationView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }
        #endregion

        #region Action on screen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Confirm();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }
        #endregion

        #region Process Barcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                {
                    ProcessPalletBarcode(barcode);
                }
                else if (Utility.LocationCodeRegex.IsMatch(barcode))
                {
                    if (!isPalletScanned)
                    {
                        Utility.PlayErrorSound();
                        lblStepHints.Text = "Vui lòng quét mã pallet trước.";
                        isPalletScanned = false;
                        isLocationScanned = false;
                        return;
                    }

                    ProcessLocationBarcode(barcode);
                }
                else
                {
                    ProcessPalletBarcode(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            _locationCode = string.Empty;
            _locationName = string.Empty;
            _locationSuggestionCode = string.Empty;
            _locationSuggestionName = string.Empty;

            string type;
            bdsPalletSummary.DataSource = null;
            barcode = Barcode.Process(barcode, true, true, true, out type);
            DataTable dt = Pallet.LoadRequirePalletStatusLocation(barcode, type);
            dt.TableName = "PalletSummary";
            bdsPalletSummary.DataSource = dt;
            barcode = dt.Rows[0]["PalletCode"].ToString();
            lblPallet.Text = dt.Rows[0]["PalletCode"].ToString();

            List<SqlParameter> listParam2 = new List<SqlParameter>();
            listParam2.Add(new SqlParameter("Barcode", barcode));
            listParam2.Add(new SqlParameter("Type", "P"));
            bdsPalletDetailProduct.DataSource = null;
            DataTable b = Utility.LoadDataFromStore("proc_PalletStatuses_Select2", listParam2);
            b.TableName = "CartonList";
            bdsPalletDetailProduct.DataSource = b;

            _locationCode = dt.Rows[0]["LocationCode"].ToString();
            _locationSuggestionCode = dt.Rows[0]["LocationSuggestion"].ToString();
            _locationName = dt.Rows[0]["LocationName"].ToString();
            _locationSuggestionName = dt.Rows[0]["LocationSuggestionName"].ToString();

            lblProductLot.Text = string.Empty;
            if (string.IsNullOrEmpty(_locationCode))
            {
                lblProductLot.Text = "______,Gợi ý: " + _locationSuggestionName;
            }
            else
            {
                lblProductLot.Text = _locationName + ",Gợi ý: " + _locationSuggestionName;
            }

            lblStepHints.Text = _stepHints[2];
            isPalletScanned = true;
            tabControl1.SelectedIndex = 0;
        }

        public void ProcessLocationBarcode(string barcode)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("LocationCode", barcode));

                DataTable dt = Utility.LoadDataFromStore("proc_ZoneLocations_Select", listParam);
                if (dt.Rows.Count == 0)
                {
                    throw new Exception("Mã vị trí không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }
                else
                {
                    _locationCode = barcode;
                    _locationName = dt.Rows[0]["LocationName"].ToString();
                    lblProductLot.Text = _locationName;
                    isLocationScanned = true;
                    Confirm();
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void Confirm()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (Pallet.SetLocation(lblPallet.Text, _locationCode, _currentWork))
                {
                    lblStepHints.Text = "Đã đưa pallet " + lblPallet.Text + " lên kệ " + lblProductLot.Text + ". Quét pallet để làm tiếp.";
                    ////Play sound set vị trí thành công                    
                    //bdsPalletSummary.DataSource = null;
                    //isPalletScanned = false;
                    //isLocationScanned = false;
                    //lblPallet.Text = string.Empty;
                    //lblProductLot.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        private void menuItem2_Click(object sender, EventArgs e)
        {
            UpdateVerifyStatus("OK");
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            UpdateVerifyStatus("WrongLotQuantity");
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            UpdateVerifyStatus("WrongUpdateLater");
        }

        public void UpdateVerifyStatus(string status)
        {
            try
            {
                if (!isPalletScanned)
                {
                    Utility.PlayErrorSound();
                    lblStepHints.Text = "Vui lòng quét mã pallet trước.";
                    isPalletScanned = false;
                    isLocationScanned = false;
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;
                if (Pallet.UpdateVerifyStatus(lblPallet.Text, status))
                {
                    lblStepHints.Text = "Đã ghi nhận trạng thái pallet " + lblPallet.Text + ". Quét pallet để làm tiếp.";
                    //Play sound set vị trí thành công                    
                    bdsPalletSummary.DataSource = null;
                    isPalletScanned = false;
                    isLocationScanned = false;
                    lblPallet.Text = string.Empty;
                    lblProductLot.Text = string.Empty;
                }

                LoadPalletDoneByUser();
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void LoadPalletDoneByUser()
        {
            try
            {
                bdsPalletDone.DataSource = null;
                DataTable dt2 = Pallet.LoadPalletDoneByUser(string.Empty);
                dt2.TableName = "PalletDone";
                bdsPalletDone.DataSource = dt2;
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
        }

        private void menuItem5_Click(object sender, EventArgs e)
        {
            UpdateVerifyStatus("MustPrintPallet");
        }
    }
}