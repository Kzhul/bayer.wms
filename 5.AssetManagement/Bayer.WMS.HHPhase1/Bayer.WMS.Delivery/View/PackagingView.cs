﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Data.SqlClient;
using Bayer.WMS.Delivery.Models;
using System.IO.Ports;
using System.Threading;
using System.Globalization;
using System.IO;

namespace Bayer.WMS.Handheld.Views
{
    public partial class PackagingView : Form
    {
        private string _storeHints;
        private int _oddQty;
        private string _prepareCode;
        private Pallet _pallet;
        private string _cartonBarcode;
        private string _companyCode;
        private string _companyName;
        private string _doImportCode;
        private string _executor;
        private DateTime _deliveryDate;
        private DataTable _dt;
        private BarcodeReader _barcodeReader;
        private readonly SerialPort _serialPort;
        private bool _isProcess = false;
        private Queue<string> _queue;

        public PackagingView()
        {
            InitializeComponent();
            InitDataGridView();

            _queue = new Queue<string>();
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);

            _pallet = new Pallet();
            _oddQty = 0;

            #region DataTable
            _dt = new DataTable();
            _dt.Columns.Add("CartonBarcode", typeof(string));
            _dt.Columns.Add("ProductBarcode", typeof(string));
            _dt.Columns.Add("EncryptedProductBarcode", typeof(string));
            _dt.Columns.Add("ProductLot", typeof(string));
            _dt.Columns.Add("ProductID", typeof(int));
            _dt.Columns.Add("ProductCode", typeof(string));
            _dt.Columns.Add("ProductDescription", typeof(string));

            var dtPerCarton = _dt.Clone();
            dtPerCarton.TableName = "Packaging";
            bdsPackaging.DataSource = dtPerCarton;
            #endregion

            #region SerialPort
            Parity parity = Parity.None;
            switch (Utility.COM_Parity)
            {
                case "Even":
                    parity = Parity.Even;
                    break;
                case "Mark":
                    parity = Parity.Mark;
                    break;
                case "Odd":
                    parity = Parity.Odd;
                    break;
                case "Space":
                    parity = Parity.Space;
                    break;
                default:
                    break;
            }

            StopBits sb = StopBits.None;
            switch (Utility.COM_StopBits)
            {
                case "1":
                    sb = StopBits.One;
                    break;
                case "1.5":
                    sb = StopBits.OnePointFive;
                    break;
                case "2":
                    sb = StopBits.Two;
                    break;
                default:
                    break;
            }

            _serialPort = new SerialPort(Utility.COM_PortName, Utility.COM_BaudRate, parity, Utility.COM_DataBits, sb);
            #endregion
        }

        public void PackagingView_Load(object sender, EventArgs e)
        {
            lblCompany.Text = _companyName;
            lblPallet.Text = "Pallet: " + _pallet.PalletCode;
            lblCarton.Text = "Thùng: " + _cartonBarcode;

            lblStepHints.Text = "Quét mã đơn vị lẻ để thực hiện đóng gói";
        }

        public void PackagingView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                if (bre != null)
                {
                    string barcode = bre.strDataBuffer;
                    if (!_isProcess)
                        QueueProcess(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                //txtBarcode.Focus();
                //txtBarcode.SelectAll();
            }
        }

        private void Dequeue()
        {
            try
            {
                while (_queue.Count > 0)
                {
                    _isProcess = true;

                    string queue = _queue.Dequeue();
                    ProcessBarcode(queue);
                }

                _isProcess = false;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void QueueProcess(string data)
        {
            _queue.Enqueue(data);

            if (!_isProcess)
            {
                if (InvokeRequired)
                {
                    Invoke((ThreadStart)delegate
                    {
                        Dequeue();
                    });
                }
                else
                    Dequeue();
            }
        }

        public void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isProcess)
                    QueueProcess(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniFinish_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isProcess)
                    QueueProcess("ket_thuc");
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        public void mniCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "Packaging" };
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "#", 25, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("QR", "EncryptedProductBarcode", 100, String.Empty, null));

            dtgPackaging.TableStyles.Add(dtgStyle);
        }

        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (barcode == "ket_thuc")
                    ProcessPackagingEnd();
                else if (
                     Utility.CartonBarcode3Regex.IsMatch(barcode)
                    || Utility.CartonBarcode2Regex.IsMatch(barcode)
                    || Utility.CartonBarcodeRegex.IsMatch(barcode)
                    )
                    ProcessCartonBarcode(barcode);
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                Utility.WriteLog(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPackagingEnd()
        {
            Cursor.Current = Cursors.Default;

            decimal weight;
            using (var view = new UpdateWeightView())
            {
                if (view.ShowDialog() != DialogResult.OK)
                    return;

                weight = view.Weight;
            }
            Print(bdsPackaging.DataSource as DataTable, weight);
            lblStepHints.Text = "Đóng gói hoàn tất";
        }

        public void ProcessCartonBarcode(string barcode)
        {
            if (MessageBox.Show("Bạn có chắc muốn san thùng ?", "San thùng", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                Utility.UpdatePalletStatus_Packaging3(_pallet.PalletCode, barcode, _cartonBarcode);
                DataTable details = Pallet.LoadPalletStatuses(_cartonBarcode, "C");
                _dt = details;
                details.TableName = "Packaging";
                bdsPackaging.DataSource = details;
                _oddQty = details.Rows.Count;
                lblQty.Text = "SL: " + (_oddQty).ToString();
            }
        }

        public void ProcessProductBarcode(string barcode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string encryptedBarcode = barcode; //barcode.Split('|')[0];
                string type;
                barcode = Barcode.Process(encryptedBarcode, false, false, true, out type);

                if (_dt.Select(String.Format("ProductBarcode = '{0}'", barcode)).Length > 0)
                    throw new Exception("Mã đã đóng gói rồi, vui lòng kiểm tra lại dữ liệu.");

                #region Verify Prepared Quantity
                if (_pallet.Status != "N" && !string.IsNullOrEmpty(_pallet.CompanyCode))
                {
                    DataTable dt = Company.VerifyPreparedQuantity(_pallet.PalletCode, barcode);
                    if (dt.Rows.Count > 0)
                    {
                        string mes = string.Empty;
                        mes =
                            "Vượt quá số lượng yêu cầu" + Environment.NewLine
                            + "SL: " + dt.Rows[0]["NextQuantity"].ToString() + " / " + dt.Rows[0]["DOQuantity"].ToString() + Environment.NewLine
                            + "Lô: " + dt.Rows[0]["ProductLot"].ToString() + Environment.NewLine
                            + "SP: " + dt.Rows[0]["ProductName"].ToString() + Environment.NewLine
                            ;
                        Utility.PlayErrorSound();
                        MessageBox.Show(mes, "Lỗi");
                        return;
                    }
                }
                #endregion
                _oddQty++;

                Utility.UpdatePalletStatus_Packaging2(_pallet.PalletCode, _cartonBarcode, barcode);

                DataTable details = Pallet.LoadPalletStatuses(_cartonBarcode, "C");
                _dt = details;
                details.TableName = "Packaging";
                bdsPackaging.DataSource = details;
                _oddQty = details.Rows.Count;
                lblQty.Text = "SL: " + (_oddQty).ToString();

                lblStepHints.Text = "Quét mã đơn vị lẻ để thực hiện đóng gói";
                lblQty.Text = "SL: " + _oddQty.ToString();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void Print(DataTable packagingData, decimal weight)
        {
            try
            {
                var products = new List<string>();
                var qties = new Dictionary<string, int>();
                int maxLength = 0;
                foreach (DataRow row in packagingData.Rows)
                {
                    string productLot = row["ProductLot"].ToString();
                    string productCode = row["ProductCode"].ToString();
                    string productDescription = row["ProductDescription"].ToString();

                    maxLength = productDescription.Length > maxLength ? productDescription.Length : maxLength;

                    string tmp = String.Format("{0}?{1}?{2}", productLot, productCode, productDescription);
                    if (!products.Exists(p => p == tmp))
                    {
                        products.Add(tmp);
                        qties.Add(tmp, 1);
                    }
                    else
                        qties[tmp] += 1;
                }

                if (!_serialPort.IsOpen)
                    _serialPort.Open();

                string text = String.Empty;
                if (products.Count == 1)
                {
                    var productionPlan = ProductionPlan.LoadProductionPlan(products[0].Split('?')[0]);

                    char separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
                    string format = GetNumberFormat(productionPlan.PackageSize);

                    text = Utility.CartonLabelTemplate;
                    text = text.Replace("{FontScale}", productionPlan.ProductDescription.Length <= 25 ? "100" : (Math.Round(((decimal)25 / productionPlan.ProductDescription.Length) * 100)).ToString());
                    text = text.Replace("{CompanyName}", _companyName);
                    text = text.Replace("{DeliveryDate}", _deliveryDate.ToString("dd/MM/yyyy"));
                    text = text.Replace("{ProductName}", productionPlan.ProductDescription);
                    text = text.Replace("{ProductLot}", productionPlan.ProductLot);
                    text = text.Replace("{ManufacturingDate}", productionPlan.MFD.ToString("dd/MM/yyyy"));
                    text = text.Replace("{ExpiryDate}", productionPlan.EXP.ToString("dd/MM/yyyy"));
                    text = text.Replace("{CartonBarcode}", _cartonBarcode);
                    text = text.Replace("{Weight}", String.Format("{0:N2}", weight));
                    text = text.Replace("{Executor}", _executor);
                    text = text.Replace("{PackageSize}", String.Format(format, productionPlan.PackageSize));
                    text = text.Replace("{UOM}", productionPlan.UOM);
                    text = text.Replace("{ProductPacking}", String.Format("{0:N0}", qties[products[0]]));
                }
                else
                {
                    text = Utility.CartonLabelForMultiProductTemplate;
                    text = text.Replace("{FontScale}", maxLength <= 20 ? "80" : (Math.Round(((decimal)25 / maxLength) * 80)).ToString());
                    text = text.Replace("{CompanyName}", _companyName);
                    text = text.Replace("{DeliveryDate}", _deliveryDate.ToString("dd/MM/yyyy"));
                    text = text.Replace("{CartonBarcode}", _cartonBarcode);
                    text = text.Replace("{Weight}", String.Format("{0:N2}", weight));
                    text = text.Replace("{Executor}", _executor);

                    for (int i = 1; i <= 6; i++)
                    {
                        if (products.Count >= i)
                        {
                            string[] temp = products[i - 1].Split('?');

                            text = text.Replace("{ProductLot" + i + "}", temp[0]);
                            text = text.Replace("{ProductName" + i + "}", temp[2]);
                            text = text.Replace("{Qty" + i + "}", String.Format("{0:N0}", qties[products[i - 1]]));
                        }
                        else
                        {
                            text = text.Replace("{ProductLot" + i + "}", String.Empty);
                            text = text.Replace("{ProductName" + i + "}", String.Empty);
                            text = text.Replace("{Qty" + i + "}", String.Empty);
                        }
                    }
                }
                byte[] b = Encoding.UTF8.GetBytes(text);
                _serialPort.Write(b, 0, b.Length);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
                throw;

            }
            finally
            {
                if (_serialPort.IsOpen)
                    _serialPort.Close();
            }
        }

        public string GetNumberFormat(decimal number)
        {
            char separator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
            string format = "{0:N0}";
            if (number.ToString().Contains(separator))
            {
                string tmp = number.ToString().Split(separator)[1];
                while (tmp.EndsWith("0"))
                    tmp = tmp.Substring(0, tmp.Length - 1);
                if (tmp.Length == 1)
                    format = "{0:N1}";
                else if (tmp.Length == 2)
                    format = "{0:N2}";
                else if (tmp.Length == 3)
                    format = "{0:N3}";
                else if (tmp.Length == 4)
                    format = "{0:N4}";
            }

            return format;
        }

        public string PrepareCode
        {
            set { _prepareCode = value; }
        }

        public string CompanyName
        {
            set { _companyName = value; }
        }

        public string Executor
        {
            set { _executor = value; }
        }

        public DateTime DeliveryDate
        {
            set { _deliveryDate = value; }
        }

        public int OddQty
        {
            get { return _oddQty; }
            set { _oddQty = value; }
        }

        public Pallet Pallet
        {
            set { _pallet = value; }
        }

        public string CartonBarcode
        {
            set { _cartonBarcode = value; }
        }

        public string CompanyCode
        {
            set { _companyCode = value; }
        }

        public string DOImportCode
        {
            set { _doImportCode = value; }
        }
    }
}