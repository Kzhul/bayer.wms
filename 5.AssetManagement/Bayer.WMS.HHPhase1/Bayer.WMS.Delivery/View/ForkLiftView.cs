﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using Bayer.WMS.Delivery.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Handheld.Views
{
    public partial class ForkLiftView : Form
    {
        #region Param
        private BarcodeReader _barcodeReader;
        protected List<string> _scannedValue;
        protected List<string> _stepHints;

        public bool isPalletScanned = false;
        public bool isLocationScanned = false;
        private string _userInfo = "Xe nâng";
        private string _currentWork = string.Empty;
        private string _locationCode = string.Empty;
        private string _locationName = string.Empty;
        private string _locationSuggestionCode = string.Empty;
        private string _locationSuggestionName = string.Empty;
        #endregion

        #region Form Init
        public ForkLiftView()
        {
            InitializeComponent();
            InitDataGridView();

            _stepHints = new List<string> 
            { 
                "Quét mã nhân viên để bắt đầu",
                "Quét mã Pallet",
                "Pallet đã rời vị trí. Quét mã vị trí để đưa pallet lên kệ",
                "Bấm xác nhận"
            };
            lblStepHints.Text = _stepHints[1];
            _barcodeReader = Utility.InitBarcodeReader(_barcodeReader_BarcodeRead);

            _userInfo = "Xe nâng: " + String.Format("{0} {1}", Utility.CurrentUser.LastName, Utility.CurrentUser.FirstName);
            this.Text = _userInfo;

        }

        private void SetupLocationView_Load(object sender, EventArgs e)
        {
            btnProductWaiting.Text = "TP SX->Kho";
        }

        public void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "PalletSummary" };

            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("#", "IDS", 25, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductDescription", 150, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL Thùng", "CartonQuantity", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("Quy Cách", "PackageSize", 60, "N0", null));
            dtgStyle.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, string.Empty, null));            
            dtgPallet.TableStyles.Add(dtgStyle);


            var dtgStyleReady = new DataGridTableStyle { MappingName = "PalletReady" };
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("#", "IDS", 25, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));            
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("VT gợi ý", "LocationSuggestionName", 60, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("Thủ kho duyệt", "StrTime", 100, string.Empty, null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("SL Thùng", "CartonQuantity", 60, "N0", null));
            dtgStyleReady.GridColumnStyles.Add(Utility.AddColumn("SL", "Quantity", 60, "N0", null));            
            dtgPalletWaiting.TableStyles.Add(dtgStyleReady);

            var dtgStyleDone = new DataGridTableStyle { MappingName = "PalletDone" };
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("#", "IDS", 25, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Mô tả", "ProductName", 150, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Số lô", "ProductLot", 60, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Vị trí", "LocationName", 90, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Thời Gian", "StrTime", 100, string.Empty, null));            
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Dãy", "LineName", 60, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Khu vực", "ZoneName", 150, String.Empty, null));            
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Pallet", "PalletCode", 90, String.Empty, null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("SL Thùng", "CartonQuantity", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("SL", "ProductQty", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("Quy Cách", "PackageSize", 60, "N0", null));
            dtgStyleDone.GridColumnStyles.Add(Utility.AddColumn("ĐVT", "UOM", 60, string.Empty, null));
            
            dtgPalletDone.TableStyles.Add(dtgStyleDone);
        }

        public void SetupLocationView_Closed(object sender, EventArgs e)
        {
            lblStepHints.Text = _stepHints[0];
        }

        public void SetupLocationView_Closing(object sender, CancelEventArgs e)
        {
            if (_barcodeReader != null)
                _barcodeReader.Dispose();
        }

        public virtual void DisposeBarcodeReader()
        {
            try
            {
                if (_barcodeReader != null)
                    _barcodeReader.Dispose();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public virtual void ReInitBarcodeReader()
        {
            try
            {
                _barcodeReader = Utility.ReInitBarcodeReader();
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }
        #endregion

        #region Action on screen
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessBarcode(txtBarcode.Text);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                Confirm();
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void mniCancel_Click(object sender, EventArgs e)
        {
            DisposeBarcodeReader();
            this.Close();
        }

        private void btnProductWaiting_Click(object sender, EventArgs e)
        {
            //Lấy danh sách pallet đã sản xuất xong để mang về kho
            _currentWork = "N";
            LoadPalletWaiting(_currentWork);
        }

        private void dtgPalletWaiting_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtBarcode.Text = dtgPalletWaiting[dtgPalletWaiting.CurrentRowIndex, 5].ToString();
                ProcessBarcode(txtBarcode.Text);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        private void btnProductForLocation_Click(object sender, EventArgs e)
        {
            //Lấy danh sách pallet đã được thủ kho xác nhận chờ đưa lên kệ
            _currentWork = "C";
            LoadPalletWaiting(_currentWork);
        }

        private void btnMaterialForLocation_Click(object sender, EventArgs e)
        {
            //Lấy danh sách pallet đã được thủ kho xác nhận chờ đưa lên kệ
            _currentWork = "B";
            LoadPalletWaiting(_currentWork);
        }

        private void btnDropMaterial_Click(object sender, EventArgs e)
        {
            //Lấy danh sách pallet đã được thủ kho xác nhận chờ rớt hàng nguyên liệu
            _currentWork = "BM";
            LoadPalletWaiting(_currentWork);
        }

        private void btnMoveMaterial_Click(object sender, EventArgs e)
        {
            //Lấy danh sách pallet đã được thủ kho xác nhận chờ chuyển xuống SX
            _currentWork = "MM";
            LoadPalletWaiting(_currentWork);
        }
        #endregion

        #region Process Barcode
        public void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Cursor.Current = Cursors.WaitCursor;

                if (barcode == "0")
                {
                    if (Utility.debugMode)
                    {
                        DataTable dt = Company.Barcode_Test();
                        int n = dt.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            ProcessBarcode(dt.Rows[i]["Barcode"].ToString());
                        }
                    }
                }
                else if (Utility.PalletBarcodeRegex.IsMatch(barcode))
                {
                    ProcessPalletBarcode(barcode);
                }
                else if (Utility.LocationCodeRegex.IsMatch(barcode))
                {
                    if (!isPalletScanned)
                    {
                        Utility.PlayErrorSound();
                        lblStepHints.Text = "Vui lòng quét mã pallet trước.";
                        isPalletScanned = false;
                        isLocationScanned = false;
                        return;
                    }

                    ProcessLocationBarcode(barcode);
                }
                else
                {
                    ProcessPalletBarcode(barcode);
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
                Cursor.Current = Cursors.Default;
            }
        }

        public void ProcessPalletBarcode(string barcode)
        {
            string type;
            bdsPalletSummary.DataSource = null;
            barcode = Barcode.Process(barcode, true, true, true, out type);
            DataTable dt = Pallet.LoadRequirePalletStatusLocation(barcode, type);

            //Đưa hàng lên kệ
            //Nếu thủ kho chưa xác nhận
            //Xe nâng quét vào sẽ hiển thị thông báo
            if (_currentWork == "C")
            {
                if (dt.Rows[0]["WarehouseKeeper"].ToString() == "0")
                {
                    MessageBox.Show(barcode + Environment.NewLine + "Trạng Thái: " + dt.Rows[0]["StrStatus"].ToString() + Environment.NewLine + "Thủ kho chưa xác nhận pallet này.");
                    return;
                }
            }

            _locationCode = string.Empty;
            _locationName = string.Empty;
            _locationSuggestionCode = string.Empty;
            _locationSuggestionName = string.Empty;

            dt.TableName = "PalletSummary";
            bdsPalletSummary.DataSource = dt;
            barcode = dt.Rows[0]["PalletCode"].ToString();
            lblPallet.Text = dt.Rows[0]["PalletCode"].ToString();

            _locationCode = dt.Rows[0]["LocationCode"].ToString();
            _locationSuggestionCode = dt.Rows[0]["LocationSuggestion"].ToString();
            _locationName = dt.Rows[0]["LocationName"].ToString();
            _locationSuggestionName = dt.Rows[0]["LocationSuggestionName"].ToString();

            lblProductLot.Text = string.Empty;
            if (string.IsNullOrEmpty(_locationCode))
            {
                lblProductLot.Text = "______,Gợi ý: " + _locationSuggestionName;
            }
            else
            {
                lblProductLot.Text = _locationName + ",Gợi ý: " + _locationSuggestionName;
            }

            lblStepHints.Text = _stepHints[2];
            isPalletScanned = true;
            tabControl1.SelectedIndex = 2;
        }

        public void ProcessLocationBarcode(string barcode)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("LocationCode", barcode));

                DataTable dt = Utility.LoadDataFromStore("proc_ZoneLocations_Select", listParam);
                if (dt.Rows.Count == 0)
                {
                    throw new Exception("Mã vị trí không tồn tại, vui lòng kiểm tra lại dữ liệu.");
                }
                else
                {
                    _locationCode = barcode;
                    _locationName = dt.Rows[0]["LocationName"].ToString();
                    lblProductLot.Text = _locationName;
                    isLocationScanned = true;
                    //lblStepHints.Text = _stepHints[3];

                    //Xe nâng không cần bấm xác nhận, hệ thống xác nhận tự động khi quét mã vị trí
                    //Trường hợp chất hàng lên kệ, xe nâng cầm bấm xác nhận

                    //C: chất hàng TP lên kệ
                    //B: chất hàng nguyên liệu lên kệ
                    //N: pallet đã sản xuất xong, chờ mang về kho
                    //BM: rớt hàng nguyên liệu
                    //DP: rớt hàng thành phẩm
                    //if (_currentWork == "N" || _currentWork == "DP" || _currentWork == "BM")
                    {
                        Confirm();
                    }

                    //Utility.PlayLocationLevelSound(dt.Rows[0]["LevelID"].ToString());
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogWithSound(ex);
            }
        }

        public void LoadPalletWaiting(string type)
        {
            try
            {
                bdsPalletWaiting.DataSource = null;
                DataTable dt = Pallet.LoadPalletReady(type);
                dt.TableName = "PalletReady";
                bdsPalletWaiting.DataSource = dt;


                bdsPalletDone.DataSource = null;
                DataTable dt2 = Pallet.LoadPalletDoneByUser(type);
                dt2.TableName = "PalletDone";
                bdsPalletDone.DataSource = dt2;

                tabControl1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
            }
        }

        public void Confirm()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (Pallet.SetLocation(lblPallet.Text, _locationCode, _currentWork))
                {
                    //Nếu công việc hiện tại là Nhập Kho Nguyên Liệu
                    if (_currentWork == "B")
                    {
                        //
                        //Update
                        List<SqlParameter> listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("PalletCode", lblPallet.Text));
                        listParam.Add(new SqlParameter("LocationCode", _locationCode));
                        listParam.Add(new SqlParameter("UserID", Utility.UserID));
                        listParam.Add(new SqlParameter("SitemapID", Utility.SitemapID));
                        listParam.Add(new SqlParameter("Method", "ForkLift_SetLocation"));
                        DataTable dt = Utility.LoadDataFromStore("proc_StockReceiving_ConfirmPalletLocationByForkLift", listParam);

                        lblStepHints.Text = "Đã đưa pallet " + lblPallet.Text + " lên kệ " + lblProductLot.Text + ". Quét pallet để làm tiếp.";
                    }
                    //Nếu công việc hiện tại là Rớt hàng nguyên liệu xuống cho Sản Xuất
                    else if (_currentWork == "BM")
                    {
                        //
                        //Update
                        List<SqlParameter> listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("PalletCode", lblPallet.Text));
                        listParam.Add(new SqlParameter("UserID", Utility.UserID));
                        listParam.Add(new SqlParameter("SitemapID", Utility.SitemapID));
                        listParam.Add(new SqlParameter("Method", "ForkLift_SetLocation"));
                        DataTable dt = Utility.LoadDataFromStore("proc_RequestMaterial_DriverPickedPallet", listParam);

                        lblStepHints.Text = "Đã rớt pallet " + lblPallet.Text + " xuống vị trí tạm " + lblProductLot.Text + ". Quét pallet để làm tiếp.";
                    }
                    //Nếu công việc hiện tại là chuyển hàng nguyên liệu xuống cho Sản Xuất
                    else if (_currentWork == "MM")
                    {
                        //
                        //Update
                        List<SqlParameter> listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("PalletCode", lblPallet.Text));
                        listParam.Add(new SqlParameter("UserID", Utility.UserID));
                        listParam.Add(new SqlParameter("SitemapID", Utility.SitemapID));
                        listParam.Add(new SqlParameter("Method", "ForkLift_SetLocation"));
                        DataTable dt = Utility.LoadDataFromStore("proc_RequestMaterial_DriverMoveMaterial", listParam);

                        lblStepHints.Text = "Đã rớt pallet " + lblPallet.Text + " xuống vị trí tạm " + lblProductLot.Text + ". Quét pallet để làm tiếp.";
                    }//Nếu công việc hiện tại là chuyển hàng nguyên liệu xuống cho Sản Xuất
                    else if (_currentWork == "DP")
                    {
                        //
                        //Update
                        List<SqlParameter> listParam = new List<SqlParameter>();
                        listParam.Add(new SqlParameter("PalletCode", lblPallet.Text));
                        listParam.Add(new SqlParameter("UserID", Utility.UserID));
                        listParam.Add(new SqlParameter("SitemapID", Utility.SitemapID));
                        listParam.Add(new SqlParameter("Method", "ForkLift_SetLocation"));
                        DataTable dt = Utility.LoadDataFromStore("proc_DeliveryNoteDetailSplits_Driver", listParam);

                        lblStepHints.Text = "Đã rớt pallet " + lblPallet.Text + " xuống vị trí tạm " + lblProductLot.Text + ". Quét pallet để làm tiếp.";
                    }
                    else
                    {
                        lblStepHints.Text = "Đã đưa pallet " + lblPallet.Text + " lên kệ " + lblProductLot.Text + ". Quét pallet để làm tiếp.";
                    }

                    //Play sound set vị trí thành công                    
                    //bdsPalletSummary.DataSource = null;
                    //isPalletScanned = false;
                    isLocationScanned = false;
                    //lblPallet.Text = string.Empty;
                    lblProductLot.Text = string.Empty;
                    LoadPalletWaiting(_currentWork);
                }
            }
            catch (Exception ex)
            {
                Utility.PlayErrorSound();
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        private void btnExportProductPallet_Click(object sender, EventArgs e)
        {
            //Lấy danh sách pallet đã sản xuất xong để mang về kho
            _currentWork = "DP";
            LoadPalletWaiting(_currentWork);
        }
    }
}