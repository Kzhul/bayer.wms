﻿namespace Bayer.WMS.Handheld.Views
{
    partial class PINView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblCurrentPIN = new System.Windows.Forms.Label();
            this.txtCurrentPIN = new System.Windows.Forms.TextBox();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNewPIN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtConfirmPIN = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(3, 103);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(234, 39);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Lưu";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblCurrentPIN
            // 
            this.lblCurrentPIN.Location = new System.Drawing.Point(3, 5);
            this.lblCurrentPIN.Name = "lblCurrentPIN";
            this.lblCurrentPIN.Size = new System.Drawing.Size(126, 21);
            this.lblCurrentPIN.Text = "Mã PIN hiện tại:";
            // 
            // txtCurrentPIN
            // 
            this.txtCurrentPIN.Location = new System.Drawing.Point(135, 5);
            this.txtCurrentPIN.MaxLength = 100;
            this.txtCurrentPIN.Name = "txtCurrentPIN";
            this.txtCurrentPIN.Size = new System.Drawing.Size(102, 21);
            this.txtCurrentPIN.TabIndex = 11;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 221);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 47);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.miExit);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = " ";
            // 
            // miExit
            // 
            this.miExit.Text = "Thoát";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 21);
            this.label1.Text = "Mã PIN mới:";
            // 
            // txtNewPIN
            // 
            this.txtNewPIN.Location = new System.Drawing.Point(135, 49);
            this.txtNewPIN.MaxLength = 100;
            this.txtNewPIN.Name = "txtNewPIN";
            this.txtNewPIN.Size = new System.Drawing.Size(102, 21);
            this.txtNewPIN.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 21);
            this.label2.Text = "Nhập lại mã PIN mới:";
            // 
            // txtConfirmPIN
            // 
            this.txtConfirmPIN.Location = new System.Drawing.Point(135, 76);
            this.txtConfirmPIN.MaxLength = 100;
            this.txtConfirmPIN.Name = "txtConfirmPIN";
            this.txtConfirmPIN.Size = new System.Drawing.Size(102, 21);
            this.txtConfirmPIN.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(234, 21);
            this.label4.Text = "------------------------------------------------------";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PINView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtConfirmPIN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNewPIN);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblCurrentPIN);
            this.Controls.Add(this.txtCurrentPIN);
            this.Menu = this.mainMenu1;
            this.Name = "PINView";
            this.Text = "Cập nhật mã PIN";
            this.Load += new System.EventHandler(this.PINView_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.PINView_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblCurrentPIN;
        private System.Windows.Forms.TextBox txtCurrentPIN;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNewPIN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtConfirmPIN;
        private System.Windows.Forms.Label label4;
    }
}