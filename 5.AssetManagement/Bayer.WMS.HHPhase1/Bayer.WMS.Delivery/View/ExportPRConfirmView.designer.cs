﻿namespace Bayer.WMS.Handheld.Views
{
    partial class ExportPRConfirmView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
           
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuFunction = new System.Windows.Forms.MenuItem();
            this.menuConfirmPallet = new System.Windows.Forms.MenuItem();
            this.menuQRCode = new System.Windows.Forms.MenuItem();
            this.mniExit = new System.Windows.Forms.MenuItem();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.lblCurrentActionInfo = new System.Windows.Forms.Label();
            this.tpPrepareInfo = new System.Windows.Forms.TabControl();
            this.tpListPallets = new System.Windows.Forms.TabPage();
            this.dtgPallets = new System.Windows.Forms.DataGrid();
            this.tbProducts = new System.Windows.Forms.TabPage();
            this.dtgProducts = new System.Windows.Forms.DataGrid();
            this.tpPallet = new System.Windows.Forms.TabPage();
            this.dtgPalletInfo = new System.Windows.Forms.DataGrid();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGrid2 = new System.Windows.Forms.DataGrid();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGrid3 = new System.Windows.Forms.DataGrid();
            bdsPallets = new System.Windows.Forms.BindingSource(this.components);
            bdsProduct = new System.Windows.Forms.BindingSource(this.components);
            bdsPalletInfo = new System.Windows.Forms.BindingSource(this.components);
            bdsPalletDetailProduct = new System.Windows.Forms.BindingSource(this.components);
            this.tpPrepareInfo.SuspendLayout();
            this.tpListPallets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(bdsPallets)).BeginInit();
            this.tbProducts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(bdsProduct)).BeginInit();
            this.tpPallet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(bdsPalletInfo)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(bdsPalletDetailProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuFunction);
            // 
            // menuFunction
            // 
            this.menuFunction.MenuItems.Add(this.menuConfirmPallet);
            this.menuFunction.MenuItems.Add(this.menuQRCode);
            this.menuFunction.MenuItems.Add(this.mniExit);
            this.menuFunction.Text = "F1.Chức năng";
            // 
            // menuConfirmPallet
            // 
            this.menuConfirmPallet.Text = "1.Xác nhận Pallet";
            this.menuConfirmPallet.Click += new System.EventHandler(this.menuConfirmPallet_Click);
            // 
            // menuQRCode
            // 
            this.menuQRCode.Text = "2.Truy vết QRCode";
            this.menuQRCode.Click += new System.EventHandler(this.menuQRCode_Click);
            // 
            // mniExit
            // 
            this.mniExit.Text = "0.Thoát";
            this.mniExit.Click += new System.EventHandler(this.mniExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 21);
            this.label1.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(34, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(147, 21);
            this.txtBarcode.TabIndex = 0;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 233);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(240, 35);
            this.lblStepHints.Text = "Quét mã pallet có hàng để xuất nhiều\r\nQuét mã pallet trống để xuất lẻ";
            // 
            // lblCurrentActionInfo
            // 
            this.lblCurrentActionInfo.Location = new System.Drawing.Point(3, 27);
            this.lblCurrentActionInfo.Name = "lblCurrentActionInfo";
            this.lblCurrentActionInfo.Size = new System.Drawing.Size(234, 21);
            // 
            // tpPrepareInfo
            // 
            this.tpPrepareInfo.Controls.Add(this.tpListPallets);
            this.tpPrepareInfo.Controls.Add(this.tbProducts);
            this.tpPrepareInfo.Controls.Add(this.tpPallet);
            this.tpPrepareInfo.Dock = System.Windows.Forms.DockStyle.None;
            this.tpPrepareInfo.Location = new System.Drawing.Point(0, 51);
            this.tpPrepareInfo.Name = "tpPrepareInfo";
            this.tpPrepareInfo.SelectedIndex = 0;
            this.tpPrepareInfo.Size = new System.Drawing.Size(240, 179);
            this.tpPrepareInfo.TabIndex = 7;
            this.tpPrepareInfo.SelectedIndexChanged += new System.EventHandler(this.tpPrepareInfo_SelectedIndexChanged);
            // 
            // tpListPallets
            // 
            this.tpListPallets.Controls.Add(this.dtgPallets);
            this.tpListPallets.Location = new System.Drawing.Point(0, 0);
            this.tpListPallets.Name = "tpListPallets";
            this.tpListPallets.Size = new System.Drawing.Size(240, 156);
            this.tpListPallets.Text = "DS PL";
            // 
            // dtgPallets
            // 
            this.dtgPallets.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPallets.DataSource = bdsPallets;
            this.dtgPallets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallets.Location = new System.Drawing.Point(0, 0);
            this.dtgPallets.Name = "dtgPallets";
            this.dtgPallets.RowHeadersVisible = false;
            this.dtgPallets.Size = new System.Drawing.Size(240, 156);
            this.dtgPallets.TabIndex = 0;
            this.dtgPallets.DoubleClick += new System.EventHandler(this.dtgPallets_DoubleClick);
            // 
            // tbProducts
            // 
            this.tbProducts.Controls.Add(this.dtgProducts);
            this.tbProducts.Location = new System.Drawing.Point(0, 0);
            this.tbProducts.Name = "tbProducts";
            this.tbProducts.Size = new System.Drawing.Size(232, 153);
            this.tbProducts.Text = "DS SP";
            // 
            // dtgProducts
            // 
            this.dtgProducts.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgProducts.DataSource = bdsProduct;
            this.dtgProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgProducts.Location = new System.Drawing.Point(0, 0);
            this.dtgProducts.Name = "dtgProducts";
            this.dtgProducts.RowHeadersVisible = false;
            this.dtgProducts.Size = new System.Drawing.Size(232, 153);
            this.dtgProducts.TabIndex = 1;
            // 
            // tpPallet
            // 
            this.tpPallet.Controls.Add(this.dtgPalletInfo);
            this.tpPallet.Location = new System.Drawing.Point(0, 0);
            this.tpPallet.Name = "tpPallet";
            this.tpPallet.Size = new System.Drawing.Size(232, 153);
            this.tpPallet.Text = "Tìm kiếm";
            // 
            // dtgPalletInfo
            // 
            this.dtgPalletInfo.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgPalletInfo.DataSource = bdsPalletInfo;
            this.dtgPalletInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPalletInfo.Location = new System.Drawing.Point(0, 0);
            this.dtgPalletInfo.Name = "dtgPalletInfo";
            this.dtgPalletInfo.RowHeadersVisible = false;
            this.dtgPalletInfo.Size = new System.Drawing.Size(232, 153);
            this.dtgPalletInfo.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGrid1);
            this.tabPage1.Location = new System.Drawing.Point(0, 0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(240, 156);
            this.tabPage1.Text = "DS PL";
            // 
            // dataGrid1
            // 
            this.dataGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid1.DataSource = bdsPallets;
            this.dataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid1.Location = new System.Drawing.Point(0, 0);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.RowHeadersVisible = false;
            this.dataGrid1.Size = new System.Drawing.Size(240, 156);
            this.dataGrid1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGrid2);
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(232, 153);
            this.tabPage2.Text = "DS SP";
            // 
            // dataGrid2
            // 
            this.dataGrid2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid2.DataSource = bdsProduct;
            this.dataGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid2.Location = new System.Drawing.Point(0, 0);
            this.dataGrid2.Name = "dataGrid2";
            this.dataGrid2.RowHeadersVisible = false;
            this.dataGrid2.Size = new System.Drawing.Size(232, 153);
            this.dataGrid2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(0, 0);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(240, 156);
            this.tabPage3.Text = "Pallet SP";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGrid3);
            this.tabPage4.Location = new System.Drawing.Point(0, 0);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(240, 156);
            this.tabPage4.Text = "Pallet";
            // 
            // dataGrid3
            // 
            this.dataGrid3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid3.DataSource = bdsPalletInfo;
            this.dataGrid3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid3.Location = new System.Drawing.Point(0, 0);
            this.dataGrid3.Name = "dataGrid3";
            this.dataGrid3.RowHeadersVisible = false;
            this.dataGrid3.Size = new System.Drawing.Size(240, 156);
            this.dataGrid3.TabIndex = 2;
            // 
            // ExportPRConfirmView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tpPrepareInfo);
            this.Controls.Add(this.lblCurrentActionInfo);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBarcode);
            this.KeyPreview = true;
            this.Menu = this.mainMenu1;
            this.Name = "ExportPRConfirmView";
            this.Text = "Soạn hàng theo DO";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ExportPRConfirmView_Closing);
            this.tpPrepareInfo.ResumeLayout(false);
            this.tpListPallets.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(bdsPallets)).EndInit();
            this.tbProducts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(bdsProduct)).EndInit();
            this.tpPallet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(bdsPalletInfo)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(bdsPalletDetailProduct)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.MenuItem menuFunction;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.Label lblCurrentActionInfo;
        private System.Windows.Forms.TabControl tpPrepareInfo;
        private System.Windows.Forms.TabPage tpListPallets;
        private System.Windows.Forms.TabPage tbProducts;
        private System.Windows.Forms.DataGrid dtgPallets;
        private System.Windows.Forms.DataGrid dtgProducts;
        private System.Windows.Forms.TabPage tpPallet;
        private System.Windows.Forms.DataGrid dtgPalletInfo;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGrid dataGrid1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGrid dataGrid2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGrid dataGrid3;
        private System.Windows.Forms.MenuItem mniExit;
        private System.Windows.Forms.MenuItem menuQRCode;
        private System.Windows.Forms.MenuItem menuConfirmPallet;
        private System.Windows.Forms.BindingSource bdsPallets;
        private System.Windows.Forms.BindingSource bdsProduct;
        private System.Windows.Forms.BindingSource bdsPalletInfo;
        private System.Windows.Forms.BindingSource bdsPalletDetailProduct;
    }
}