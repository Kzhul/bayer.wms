﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Bayer.WMS.Delivery.Models
{
    public class Barcode
    {
        public static string Process(string barcode, bool isAcceptPallet, bool isAcceptCarton, bool isAcceptProduct, out string type)
        {
            type = String.Empty;
            if (isAcceptPallet && Utility.PalletBarcodeRegex.IsMatch(barcode))
                type = "P";
            else if (isAcceptPallet && Utility.ProductLot4Regex.IsMatch(barcode))
                type = "Y";
            else if (isAcceptPallet && Utility.ProductLotRegex.IsMatch(barcode))
                type = "L";
            else if (isAcceptCarton
                && (Utility.CartonBarcodeRegex.IsMatch(barcode)
                    || Utility.CartonBarcode2Regex.IsMatch(barcode)
                    || Utility.CartonBarcode3Regex.IsMatch(barcode)
                    || Utility.CartonBarcodeTempRegex.IsMatch(barcode)))
                type = "C";
            else
            {
                try
                {
                    Utility.WriteLog(barcode);
                    string decryptedBarcode = string.Empty;
                    try
                    {
                        //// scanned barcode is product
                        barcode = barcode.Split('|')[0];
                        barcode = barcode.StartsWith("SP") ? barcode.Remove(0, 2) : barcode;
                        decryptedBarcode = Utility.AESDecrypt(barcode.Split('|')[0]);
                    }
                    catch
                    {

                    }

                    Utility.WriteLog(decryptedBarcode);
                    //string decryptedBarcode = barcode;
                    if (Utility.ProductRegex.IsMatch(decryptedBarcode))
                    {
                        barcode = Utility.ProductRegex.Match(decryptedBarcode).Captures[0].Value;
                    }
                    else if (Utility.Product2Regex.IsMatch(decryptedBarcode))
                    {
                        barcode = Utility.Product2Regex.Match(decryptedBarcode).Captures[0].Value;
                    }
                    else if (Utility.Product3Regex.IsMatch(barcode))
                    {
                        barcode = Utility.Product3Regex.Match(barcode).Captures[0].Value;
                    }

                    if (isAcceptProduct && (Utility.ProductRegex.IsMatch(barcode) || Utility.Product2Regex.IsMatch(barcode) || Utility.Product3Regex.IsMatch(barcode)))
                        type = "E";
                    else
                        throw new Exception();
                }
                catch
                {
                    throw new Exception("Mã không hợp lệ, vui lòng kiểm tra lại dữ liệu.");
                }
            }

            return barcode;
        }

        public static DataTable GetBarcodeTracking(string barcode, string type)
        {
            var dt = new DataTable();

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open();
                try
                {
                    using (var cmd = new SqlCommand("proc_Barcode_Tracking_Mobile", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch(Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable GetBarcodeTrackingInfo(string barcode, string type)
        {
            var dt = new DataTable();

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open(); 
                try
                {
                    using (var cmd = new SqlCommand("proc_Barcode_TrackingInfo_Mobile", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@Barcode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@Type", type));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        public static DataTable CheckAssetDetail(string barcode)
        {
            var dt = new DataTable();

            using (var conn = new SqlConnection(Utility.ConnStr))
            {
                conn.Open(); 
                try
                {
                    using (var cmd = new SqlCommand("proc_Inventory_InventoryManagement_CheckAssetDetail", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter("@AssetCode", barcode));
                        cmd.Parameters.Add(new SqlParameter("@MachineCode", Utility.MachineCode));
                        cmd.Parameters.Add(new SqlParameter("@UserID", Utility.UserID));
                        cmd.Parameters.Add(new SqlParameter("@SitemapID", "0"));
                        cmd.Parameters.Add(new SqlParameter("@Method", "Mobile_CheckAssetDetail"));

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            adapt.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteLog(ex);
                    throw new Exception("Lỗi kết nối đến hệ thống");
                }
                finally
                {
                    conn.Close();
                }
            }

            return dt;
        }

        
    }
}
