﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;
using Bayer.WMS.Delivery.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Delivery
{
    public partial class DeliveryView : Form
    {
        private bool _isScanPallet = false;
        private string _temp;
        private bool _isOffline = false;
        private byte[] _bytes = Encoding.ASCII.GetBytes("BayerWMS");
        private DataTable _dt;
        private BarcodeReader _barcodeReader;
        private User _user;
        private Company _company;
        private Regex _employeeBarcodeRegex = new Regex(@"^NV_", RegexOptions.IgnoreCase);
        private Regex _productLotRegex = new Regex(@"\d{6}[FVABR]\d{2}$", RegexOptions.IgnoreCase);
        private Regex _palletBarcodeRegex = new Regex(@"^P[A-Z][01]\d\d{2}\d{5}$", RegexOptions.IgnoreCase);
        private Regex _cartonBarcodeRegex = new Regex(@"^\d{6}\w\d{2}C\d{3}$", RegexOptions.IgnoreCase);
        private Regex _productBarcodeRegex = new Regex(@"^\d{6}\w\d{2}\d{5}", RegexOptions.IgnoreCase);
        private Regex _customerBarcodeRegex = new Regex(@"^KH_", RegexOptions.IgnoreCase);

        public DeliveryView()
        {
            InitializeComponent();
            InitDataGridView();
            LoadConfig();
            CheckConnection();

            try
            {
                _barcodeReader = new BarcodeReader();
                _barcodeReader.ThreadedRead(true);
                _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
            catch
            {
                _barcodeReader = null;
            }

            _dt = new DataTable();
            _dt.TableName = "Delivery";
            _dt.Columns.Add("Barcode", typeof(string));
            _dt.Columns.Add("Type", typeof(string));
            _dt.Columns.Add("DisplayType", typeof(string));

            bdsDelivery.DataSource = _dt;
        }

        private void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ProcessBarcode(txtBarcode.Text);
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "Delivery" };

            var colBarcode = new ColumnStyle(1) { Width = 170, HeaderText = "Barcode", MappingName = "Barcode" };
            dtgStyle.GridColumnStyles.Add(colBarcode);

            var colType = new ColumnStyle(1) { Width = 80, HeaderText = "Loại", MappingName = "DisplayType" };
            dtgStyle.GridColumnStyles.Add(colType);

            dtgDelivery.TableStyles.Add(dtgStyle);
        }

        private void LoadConfig()
        {
            Program.AppPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().GetName().CodeBase);

            string configPath = Path.Combine(Program.AppPath, "AppConfig.txt");

            var config = new Dictionary<string, string>();
            using (var sr = new StreamReader(configPath))
            {
                while (!sr.EndOfStream)
                {
                    string[] tmp = sr.ReadLine().Split(':');
                    if (tmp.Length == 2)
                        config.Add(tmp[0].Trim(), tmp[1].Trim());
                }
            }

            string[] connStrComponent = config["ConnectionString"].Split(';');
            string server = connStrComponent.FirstOrDefault(p => p.StartsWith("Server")).Replace("Server=", String.Empty).Trim();
            string database = connStrComponent.FirstOrDefault(p => p.StartsWith("Database")).Replace("Database=", String.Empty).Trim();
            string userid = connStrComponent.FirstOrDefault(p => p.StartsWith("Uid")).Replace("Uid=", String.Empty).Trim();
            string pwd = connStrComponent.FirstOrDefault(p => p.StartsWith("Pwd")).Replace("Pwd=", String.Empty).Trim();
            string connTimeout = connStrComponent.FirstOrDefault(p => p.StartsWith("Connection Timeout")).Replace("Connection Timeout=", String.Empty).Trim();

            //string connStr = String.Format("Server={0};Database={1};Uid={2};pwd={3};",
            //    server, database, AESDecrypt(userid), AESDecrypt(pwd));
            Program.ConnStr = config["ConnectionString"];
        }

        

        private void CheckConnection()
        {
            Utility.WriteLog(Program.ConnStr);
            using (var conn = new SqlConnection(Program.ConnStr))
            {
                try
                {
                    conn.Open();
                    conn.Close();
                }
                catch
                {
                    MessageBox.Show("Không kết nối được đến server. Chuyển sang chế độ offline mode.");
                    _isOffline = true;
                }
            }
        }

        private string AESDecrypt(string cipherText)
        {
            if (String.IsNullOrEmpty(cipherText))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream(Convert.FromBase64String(cipherText));
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(_bytes, _bytes), CryptoStreamMode.Read);
            var reader = new StreamReader(cryptoStream);

            return reader.ReadToEnd();
            //return cipherText;
        }

        #region ProcessBarCode
        private void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;
                Utility.WriteLog("ProcessBarcode: " + barcode);

                if (String.IsNullOrEmpty(barcode.Trim()))
                    return;

                //// step 1: scan employee
                if (_employeeBarcodeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                //// step 2: scan customer
                else if (_customerBarcodeRegex.IsMatch(barcode))
                    ProcessCustomerBarcode(barcode);
                //// step 3.1: scan product lot
                else if (_productLotRegex.IsMatch(barcode))
                    ProcessProductBarcode(barcode);
                //// step 3: scan pallet/carton/product
                else
                    ProcessProductBarcode(barcode);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
        }

        private void ProcessEmployeeBarcode(string barcode)
        {
            if (_isScanPallet)
                throw new Exception("Quét không đúng bước");

            barcode = barcode.Remove(0, 3);
            if (!_isOffline)
            {
                Utility.WriteLog("ProcessEmployeeBarcode: " + barcode);
                _user = User.Load(barcode);
                Utility.WriteLog(_user.LastName + " - " + _user.FirstName);
                lblEmployeeName.Text = String.Format("{0} {1}", _user.LastName, _user.FirstName);
            }
            else
                lblEmployeeName.Text = barcode;

            lblHints.Text = "Quét mã khách hàng";
        }

        private void ProcessCustomerBarcode(string barcode)
        {
            if (_isScanPallet)
                throw new Exception("Quét không đúng bước");

            barcode = barcode.Remove(0, 3);
            if (!_isOffline)
            {
                Utility.WriteLog("ProcessCustomerBarcode: " + barcode);
                _company = Company.Load(barcode);
                lblCustomerCode.Text = _company.CompanyName;
            }
            else
                lblCustomerCode.Text = barcode;

            _dt.Rows.Clear();
            lblHints.Text = "Quét mã pallet / thùng / lẻ";
        }

        private void ProcessProductLotBarcode(string barcode)
        {
            if (!_isScanPallet)
                throw new Exception("Quét không đúng bước");

            _isScanPallet = false;
            _dt.Rows.Add(_temp, "P", "Pallet");

            Utility.WriteLog("ProcessProductLotBarcode: " + barcode);
            using (var sw = new StreamWriter(Path.Combine(Program.AppPath, "Delivery.txt"), true))
            {
                sw.WriteLine(String.Format("{0}-{1}-{2}-{3}-{4}", lblEmployeeName.Text, lblCustomerCode.Text, _temp, "P", barcode));
            }

            lblHints.Text = "Quét mã pallet / thùng / lẻ";
        }

        private void ProcessProductBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblCustomerCode.Text) || lblCustomerCode.Text == "[Tên khách hàng]")
                throw new Exception("Chưa quét mã khách hàng");

            string type = String.Empty;
            Utility.WriteLog("ProcessProductBarcode: " + barcode);
            if (!_isScanPallet)
            {
                if (_palletBarcodeRegex.IsMatch(barcode))
                {
                    Utility.WriteLog("_palletBarcodeRegex: " + barcode);
                    type = "P";
                    _temp = barcode;
                    _isScanPallet = true;

                    lblHints.Text = "Quét mã bao / xô / can đại diện";
                }
                else if (_cartonBarcodeRegex.IsMatch(barcode))
                {
                    type = "C";
                    Utility.WriteLog("_cartonBarcodeRegex: " + barcode);
                }
                else
                {
                    try
                    {
                        barcode = AESDecrypt(barcode.Split('|')[0].Remove(0, 2));
                        if (_productBarcodeRegex.IsMatch(barcode))
                        {
                            type = "E";
                            Utility.WriteLog("_productBarcodeRegex: " + barcode);
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                    catch
                    {
                        throw new Exception("Mã không hợp lệ");
                    }
                }

                if (_dt.Select(String.Format("Barcode = '{0}' AND Type = '{1}'", barcode, type)).Length > 0)
                    throw new Exception("Mã QR đã được quét, vui lòng kiểm tra lại");
                else
                {
                    if (type != "P")
                    {
                        if (!_isOffline)
                        {
                            Utility.WriteLog("Delivery.Insert: " + barcode + ", " + type + ", " + _company + ", " + _user);
                            Delivery.Models.Delivery.Insert(barcode, type, _company, _user);
                        }
                        else
                        {
                            Utility.WriteLog("Delivery.txt: " + barcode + ", " + type + ", " + _company + ", " + _user);
                            using (var sw = new StreamWriter(Path.Combine(Program.AppPath, "Delivery.txt"), true))
                            {
                                sw.WriteLine(String.Format("{0}-{1}-{2}-{3}-{4:yyyy.MM.dd}", lblEmployeeName.Text, lblCustomerCode.Text, barcode, type, DateTime.Today));
                            }
                        }

                        if (type == "C")
                            lblCarton.Text = (int.Parse(lblCarton.Text) + 1).ToString();
                        else
                            lblEach.Text = (int.Parse(lblEach.Text) + 1).ToString();

                        _dt.Rows.Add(barcode, type, type == "C" ? "Thùng" : "Lẻ");
                    }
                }
            }
            else
            {
                try
                {
                    barcode = AESDecrypt(barcode.Split('|')[0].Remove(0, 2));
                    if (!_productBarcodeRegex.IsMatch(barcode))
                        throw new Exception();

                    barcode = _palletBarcodeRegex.Match(barcode).Value;
                }
                catch
                {
                    throw new Exception("Mã không hợp lệ");
                }

                Utility.WriteLog("Delivery.txt: " + barcode + ", " + _temp + ", " + lblCustomerCode.Text + ", " + lblEmployeeName.Text);
                using (var sw = new StreamWriter(Path.Combine(Program.AppPath, "Delivery.txt"), true))
                {
                    sw.WriteLine(String.Format("{0}-{1}-{2}-{3}-{4}-{5:yyyy.MM.dd}", lblEmployeeName.Text, lblCustomerCode.Text, _temp, "P", barcode, DateTime.Today));
                }

                _isScanPallet = false;
                _dt.Rows.Add(_temp, "P", "Pallet");
                lblHints.Text = "Quét mã pallet / thùng / lẻ";
            }
        }
        #endregion

        private void menuItem1_Click(object sender, EventArgs e)
        {

        }
    }
}