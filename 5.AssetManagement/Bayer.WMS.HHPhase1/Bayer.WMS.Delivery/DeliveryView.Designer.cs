﻿namespace Bayer.WMS.Delivery
{
    partial class DeliveryView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeliveryView));
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.miExit = new System.Windows.Forms.MenuItem();
            this.bdsDelivery = new System.Windows.Forms.BindingSource(this.components);
            this.dtgDelivery = new System.Windows.Forms.DataGrid();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblCustomerCode = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCarton = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEach = new System.Windows.Forms.Label();
            this.lblHints = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDelivery)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(187, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 21);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Tìm";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 21);
            this.label1.Text = "QR:";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(39, 3);
            this.txtBarcode.MaxLength = 100;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(142, 21);
            this.txtBarcode.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 21);
            this.label3.Text = "NV:";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(3, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 21);
            this.label4.Text = "KH:";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.miExit);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Xác nhận";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // miExit
            // 
            this.miExit.Text = "Thoát";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // dtgDelivery
            // 
            this.dtgDelivery.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dtgDelivery.DataSource = this.bdsDelivery;
            this.dtgDelivery.Location = new System.Drawing.Point(0, 93);
            this.dtgDelivery.Name = "dtgDelivery";
            this.dtgDelivery.RowHeadersVisible = false;
            this.dtgDelivery.Size = new System.Drawing.Size(240, 152);
            this.dtgDelivery.TabIndex = 32;
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Location = new System.Drawing.Point(39, 27);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(198, 21);
            this.lblEmployeeName.Text = "[Tên nhân viên]";
            // 
            // lblCustomerCode
            // 
            this.lblCustomerCode.Location = new System.Drawing.Point(39, 48);
            this.lblCustomerCode.Name = "lblCustomerCode";
            this.lblCustomerCode.Size = new System.Drawing.Size(198, 21);
            this.lblCustomerCode.Text = "[Tên khách hàng]";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 21);
            this.label2.Text = "Thùng:";
            // 
            // lblCarton
            // 
            this.lblCarton.Location = new System.Drawing.Point(56, 69);
            this.lblCarton.Name = "lblCarton";
            this.lblCarton.Size = new System.Drawing.Size(47, 21);
            this.lblCarton.Text = "0";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(125, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 21);
            this.label6.Text = "Lẻ:";
            // 
            // lblEach
            // 
            this.lblEach.Location = new System.Drawing.Point(178, 69);
            this.lblEach.Name = "lblEach";
            this.lblEach.Size = new System.Drawing.Size(47, 21);
            this.lblEach.Text = "0";
            // 
            // lblHints
            // 
            this.lblHints.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblHints.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblHints.Location = new System.Drawing.Point(0, 248);
            this.lblHints.Name = "lblHints";
            this.lblHints.Size = new System.Drawing.Size(240, 20);
            this.lblHints.Text = "Quét mã nhân viên";
            this.lblHints.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DeliveryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblHints);
            this.Controls.Add(this.lblEach);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblCarton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCustomerCode);
            this.Controls.Add(this.lblEmployeeName);
            this.Controls.Add(this.dtgDelivery);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBarcode);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Menu = this.mainMenu1;
            this.Name = "DeliveryView";
            this.Text = "Soạn hàng";
            ((System.ComponentModel.ISupportInitialize)(this.bdsDelivery)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem miExit;
        private System.Windows.Forms.DataGrid dtgDelivery;
        private System.Windows.Forms.BindingSource bdsDelivery;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblCustomerCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCarton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblEach;
        private System.Windows.Forms.Label lblHints;
    }
}

