﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Net;
using OpenNETCF.Net.Ftp;
using System.Threading;

namespace UpdateApp
{
    public partial class UpdateApp : Form
    {
        public static string fromFolder = string.Empty;
        public static string toFolder = string.Empty;
        public static string strAppPath = string.Empty;
        public static int fileCopied = 0;
        public static int fileFailedCopied = 0;
        public static int folderCopied = 0;

        public UpdateApp()
        {
            InitializeComponent();
            strAppPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().GetName().CodeBase);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                LoadConfig();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void LoadConfig()
        {

            string configPath = Path.Combine(strAppPath, "AppConfig.txt");

            try
            {
                var config = new Dictionary<string, string>();
                using (var sr = new StreamReader(configPath))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] tmp = sr.ReadLine().Split(':');
                        if (tmp.Length == 2)
                            config.Add(tmp[0].Trim(), tmp[1].Trim());
                    }
                }

                fromFolder = config["FromFolder"];
                toFolder = config["ToFolder"];

                txtServer.Text = fromFolder;
                txtCK3.Text = toFolder;
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            if (Directory.Exists(@txtServer.Text))
            {
                try
                {
                    //MessageBox.Show("NetworkConnection OK");
                    lblMessage.Text += "NetworkConnection OK" + Environment.NewLine;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    MessageBox.Show(ex.StackTrace);
                }
            }

            if (Directory.Exists(@txtCK3.Text))
            {
                //MessageBox.Show("CK3 OK");
                lblMessage.Text += "CK3 OK" + Environment.NewLine;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                fromFolder = txtServer.Text;
                toFolder = txtCK3.Text;

                fileCopied = 0;
                folderCopied = 0;
                fileFailedCopied = 0;
                Copy(fromFolder, toFolder);

                if (fileFailedCopied == 0)
                {
                    lblMessage.Text = "Copy Thành Công " + folderCopied.ToString() + " folders," + fileCopied.ToString() + " files !!!";
                }
                else
                {
                    lblMessage.Text = "Copy \"Không\" Thành Công " + fileFailedCopied.ToString() + " files !!!";
                }
                //MessageBox.Show("NetworkConnection Start");
                //NetworkConnection conn;
                //NetworkCredential cred = new NetworkCredential(@"BAYER-CNB\VN414311", @"Bayer123");
                //using (conn = new NetworkConnection(fromFolder, cred))
                //{
                //    MessageBox.Show("NetworkConnection Connected");
                //    Copy(fromFolder, toFolder);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Copy(string sourceDir, string targetDir)
        {
            Directory.CreateDirectory(targetDir);
            folderCopied++;

            foreach (var file in Directory.GetFiles(sourceDir))
            {
                try
                {
                    lblMessage.Text = "Copying File: " + file;
                    File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)), true);
                    fileCopied++;
                }
                catch (Exception ex)
                {
                    fileFailedCopied++;
                    MessageBox.Show(ex.Message);
                }
            }

            foreach (var directory in Directory.GetDirectories(sourceDir))
            {
                lblMessage.Text = "Copying Folder: " + directory;
                Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCopyFTP_Click(object sender, EventArgs e)
        {
            DownloadFileFTP();
        }

        private FTP m_ftp;
        string ftphost = "syvn7154.dynu.net";
        string ftpfilepath = "/CK3/Bayer.WMS.Delivery.exe";
        string ftpuser = "syvn7154";
        string ftppass = "syVn7154";
        
        private void DownloadFileFTP()
        {
            try
            {
                m_ftp = new FTP(ftphost,17154);
                m_ftp.Connected += new FTPConnectedHandler(m_ftp_Connected);
                m_ftp.BeginConnect(ftpuser, ftppass);
                int i = 0;
                while (!m_ftp.IsConnected)
                {
                    i++;
                    Thread.Sleep(1000);
                    this.Text = "BeginConnect: " + i;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void m_ftp_Connected(FTP source)
        {
            MessageBox.Show("Connected");
            string localfilepath = toFolder + "Bayer.WMS.Handheld/Bayer.WMS.Delivery.exe";
            source.GetFile(ftphost + ftpfilepath, localfilepath, true);
            MessageBox.Show("Get file OK");
        }
    }
}