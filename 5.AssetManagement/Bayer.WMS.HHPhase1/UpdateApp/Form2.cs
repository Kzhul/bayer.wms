﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Net;
using OpenNETCF.Net.Ftp;
using System.Threading;
using System.Diagnostics;

namespace UpdateApp
{
    public partial class Form2 : Form
    {
        public static string fromFolder = string.Empty;
        public static string toFolder = string.Empty;
        public static string strAppPath = string.Empty;
        public static bool debugMode = false;
        public Form2()
        {
            InitializeComponent();
            strAppPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().GetName().CodeBase);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                Thread.Sleep(5000);
                LoadConfig();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void LoadConfig()
        {
            string configPath = Path.Combine(strAppPath, "UpdateAppConfig.txt");

            try
            {
                var config = new Dictionary<string, string>();
                using (var sr = new StreamReader(configPath))
                {
                    int i = 0;
                    while (!sr.EndOfStream)
                    {
                        i++;
                        string[] tmp = sr.ReadLine().Split('#');
                        if (tmp.Length == 2)
                            config.Add(tmp[0].Trim(), tmp[1].Trim());
                    }
                    if (i >= 3)
                    {
                        debugMode = true;
                    }
                }

                fromFolder = config["FromFolder"];
                toFolder = config["ToFolder"];

                //foreach (var process in Process.GetProcessById("Bayer.WMS.Delivery"))
                //{
                //    process.Kill();
                //}

                Copy(fromFolder, toFolder);

                MessageBox.Show("Bạn đã cập nhật phiên bản mới nhất của chương trình");

                ProcessStartInfo deliveryApp = new ProcessStartInfo(Path.Combine(strAppPath, "Bayer.WMS.Delivery.exe"),string.Empty);
                System.Diagnostics.Process.Start(deliveryApp);
                Application.Exit();
            }
            catch (FileNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
            catch (DirectoryNotFoundException)
            {
                throw new Exception("Không tìm thấy file config.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Copy(string sourceDir, string targetDir)
        {
            try
            {
                if (debugMode)
                {
                    MessageBox.Show(sourceDir);
                    MessageBox.Show(targetDir);
                }
                Directory.CreateDirectory(targetDir);

                foreach (var file in Directory.GetFiles(sourceDir))
                {
                    try
                    {
                        if (debugMode)
                        {
                            MessageBox.Show(Path.GetFileName(file));
                        }
                        File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)), true);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                foreach (var directory in Directory.GetDirectories(sourceDir))
                {
                    Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}