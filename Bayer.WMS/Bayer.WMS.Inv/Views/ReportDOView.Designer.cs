﻿namespace Bayer.WMS.Inv.Views
{
    partial class ReportDOView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.colDOImportCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEXStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSPStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPRStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDHStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExportCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplitCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrepareCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeliveryTicketCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToAddRows = false;
            this.dtgProductPacking.AllowUserToDeleteRows = false;
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductPacking.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDOImportCode,
            this.colDeliveryDate,
            this.colDOStatus,
            this.colEXStatus,
            this.colSPStatus,
            this.colPRStatus,
            this.colDHStatus,
            this.colExportCode,
            this.colSplitCode,
            this.colPrepareCode,
            this.colDeliveryTicketCode});
            this.dtgProductPacking.DataSource = this.bdsDetails;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(12, 55);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.ReadOnly = true;
            this.dtgProductPacking.Size = new System.Drawing.Size(974, 586);
            this.dtgProductPacking.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã:";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(70, 12);
            this.txtCode.MaxLength = 255;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(200, 22);
            this.txtCode.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(442, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 16);
            this.label2.TabIndex = 41;
            this.label2.Text = "-";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd.MM.yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(456, 10);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(90, 22);
            this.dtpToDate.TabIndex = 39;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd.MM.yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(346, 10);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(90, 22);
            this.dtpFromDate.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 40;
            this.label3.Text = "Ngày:";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Location = new System.Drawing.Point(562, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 42;
            this.btnSave.Text = "Tìm";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // colDOImportCode
            // 
            this.colDOImportCode.DataPropertyName = "DOImportCode";
            this.colDOImportCode.HeaderText = "Mã DOImport";
            this.colDOImportCode.Name = "colDOImportCode";
            this.colDOImportCode.ReadOnly = true;
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.DataPropertyName = "DeliveryDate";
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.colDeliveryDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.colDeliveryDate.HeaderText = "Ngày giao hàng";
            this.colDeliveryDate.Name = "colDeliveryDate";
            this.colDeliveryDate.ReadOnly = true;
            // 
            // colDOStatus
            // 
            this.colDOStatus.DataPropertyName = "DOStatus";
            this.colDOStatus.HeaderText = "Trạng thái DO";
            this.colDOStatus.Name = "colDOStatus";
            this.colDOStatus.ReadOnly = true;
            // 
            // colEXStatus
            // 
            this.colEXStatus.DataPropertyName = "EXStatus";
            this.colEXStatus.HeaderText = "Trạng thái xuất hàng";
            this.colEXStatus.Name = "colEXStatus";
            this.colEXStatus.ReadOnly = true;
            // 
            // colSPStatus
            // 
            this.colSPStatus.DataPropertyName = "SPStatus";
            this.colSPStatus.HeaderText = "Trạng thái chia hàng";
            this.colSPStatus.Name = "colSPStatus";
            this.colSPStatus.ReadOnly = true;
            // 
            // colPRStatus
            // 
            this.colPRStatus.DataPropertyName = "PRStatus";
            this.colPRStatus.HeaderText = "Trạng thái Soạn Hàng";
            this.colPRStatus.Name = "colPRStatus";
            this.colPRStatus.ReadOnly = true;
            // 
            // colDHStatus
            // 
            this.colDHStatus.DataPropertyName = "DHStatus";
            this.colDHStatus.HeaderText = "Trạng thái Giao Hàng";
            this.colDHStatus.Name = "colDHStatus";
            this.colDHStatus.ReadOnly = true;
            // 
            // colExportCode
            // 
            this.colExportCode.DataPropertyName = "ExportCode";
            this.colExportCode.HeaderText = "Phiếu Xuất Hàng";
            this.colExportCode.Name = "colExportCode";
            this.colExportCode.ReadOnly = true;
            // 
            // colSplitCode
            // 
            this.colSplitCode.DataPropertyName = "SplitCode";
            this.colSplitCode.HeaderText = "Phiếu Chia Hàng";
            this.colSplitCode.Name = "colSplitCode";
            this.colSplitCode.ReadOnly = true;
            // 
            // colPrepareCode
            // 
            this.colPrepareCode.DataPropertyName = "PrepareCode";
            this.colPrepareCode.HeaderText = "Phiếu Soạn Hàng";
            this.colPrepareCode.Name = "colPrepareCode";
            this.colPrepareCode.ReadOnly = true;
            // 
            // colDeliveryTicketCode
            // 
            this.colDeliveryTicketCode.DataPropertyName = "DeliveryTicketCode";
            this.colDeliveryTicketCode.HeaderText = "Phiếu Giao Hàng";
            this.colDeliveryTicketCode.Name = "colDeliveryTicketCode";
            this.colDeliveryTicketCode.ReadOnly = true;
            // 
            // ReportDOView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 653);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgProductPacking);
            this.Name = "ReportDOView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.BindingSource bdsDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOImportCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEXStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSPStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPRStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDHStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExportCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplitCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrepareCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryTicketCode;
    }
}