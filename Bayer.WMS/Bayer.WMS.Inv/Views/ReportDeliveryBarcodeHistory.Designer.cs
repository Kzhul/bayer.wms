﻿namespace Bayer.WMS.Inv.Views
{
    partial class ReportDeliveryBarcodeHistoryView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbCompanyCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.StrDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCartonBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgTotal = new System.Windows.Forms.DataGridView();
            this.bdsTotal = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnLoadQRCode = new System.Windows.Forms.Button();
            this.colDeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOImportCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductLot1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCartonOddQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dtgPallet = new System.Windows.Forms.DataGridView();
            this.bdsPalletList = new System.Windows.Forms.BindingSource(this.components);
            this.colStrDeliveryDate3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDOImportCode3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsTotal)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletList)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(293, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã:";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd.MM.yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(64, 17);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(90, 22);
            this.dtpFromDate.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 19);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 40;
            this.label3.Text = "Ngày:";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Location = new System.Drawing.Point(656, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 42;
            this.btnSave.Text = "Tìm";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbCompanyCode
            // 
            this.cmbCompanyCode.Columns = null;
            this.cmbCompanyCode.DropDownHeight = 1;
            this.cmbCompanyCode.DropDownWidth = 500;
            this.cmbCompanyCode.FormattingEnabled = true;
            this.cmbCompanyCode.IntegralHeight = false;
            this.cmbCompanyCode.Location = new System.Drawing.Point(330, 15);
            this.cmbCompanyCode.MaxLength = 255;
            this.cmbCompanyCode.Name = "cmbCompanyCode";
            this.cmbCompanyCode.PageSize = 0;
            this.cmbCompanyCode.PresenterInfo = null;
            this.cmbCompanyCode.Size = new System.Drawing.Size(281, 24);
            this.cmbCompanyCode.Source = null;
            this.cmbCompanyCode.TabIndex = 43;
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd.MM.yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(160, 17);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(90, 22);
            this.dtpToDate.TabIndex = 44;
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToAddRows = false;
            this.dtgProductPacking.AllowUserToDeleteRows = false;
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductPacking.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StrDeliveryDate,
            this.CompanyCode,
            this.CompanyName,
            this.colDescription,
            this.colProductLot,
            this.colQuantity,
            this.colCartonBarcode,
            this.colProductBarcode});
            this.dtgProductPacking.DataSource = this.bdsDetails;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(3, 40);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.ReadOnly = true;
            this.dtgProductPacking.Size = new System.Drawing.Size(967, 314);
            this.dtgProductPacking.TabIndex = 8;
            // 
            // StrDeliveryDate
            // 
            this.StrDeliveryDate.DataPropertyName = "StrDeliveryDate";
            this.StrDeliveryDate.HeaderText = "Ngày Giao Hàng";
            this.StrDeliveryDate.Name = "StrDeliveryDate";
            this.StrDeliveryDate.ReadOnly = true;
            // 
            // CompanyCode
            // 
            this.CompanyCode.DataPropertyName = "CompanyCode";
            this.CompanyCode.HeaderText = "Mã Công Ty";
            this.CompanyCode.Name = "CompanyCode";
            this.CompanyCode.ReadOnly = true;
            // 
            // CompanyName
            // 
            this.CompanyName.DataPropertyName = "CompanyName";
            this.CompanyName.HeaderText = "Tên Công Ty";
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.ReadOnly = true;
            this.CompanyName.Width = 300;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "ProductDescription";
            this.colDescription.HeaderText = "Sản phẩm";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            this.colDescription.Width = 300;
            // 
            // colProductLot
            // 
            this.colProductLot.DataPropertyName = "ProductLot";
            dataGridViewCellStyle2.NullValue = null;
            this.colProductLot.DefaultCellStyle = dataGridViewCellStyle2;
            this.colProductLot.HeaderText = "Số lô";
            this.colProductLot.Name = "colProductLot";
            this.colProductLot.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.colQuantity.DefaultCellStyle = dataGridViewCellStyle3;
            this.colQuantity.HeaderText = "Số lượng";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.ReadOnly = true;
            // 
            // colCartonBarcode
            // 
            this.colCartonBarcode.DataPropertyName = "CartonBarcode";
            this.colCartonBarcode.HeaderText = "Mã QR thùng";
            this.colCartonBarcode.Name = "colCartonBarcode";
            this.colCartonBarcode.ReadOnly = true;
            this.colCartonBarcode.Width = 150;
            // 
            // colProductBarcode
            // 
            this.colProductBarcode.DataPropertyName = "ProductBarcode";
            this.colProductBarcode.HeaderText = "Mã QR sản phẩm";
            this.colProductBarcode.Name = "colProductBarcode";
            this.colProductBarcode.ReadOnly = true;
            this.colProductBarcode.Width = 150;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 48);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(981, 386);
            this.tabControl1.TabIndex = 45;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgTotal);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(973, 357);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách sản phẩm";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgTotal
            // 
            this.dtgTotal.AllowUserToAddRows = false;
            this.dtgTotal.AllowUserToDeleteRows = false;
            this.dtgTotal.AllowUserToOrderColumns = true;
            this.dtgTotal.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Azure;
            this.dtgTotal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dtgTotal.AutoGenerateColumns = false;
            this.dtgTotal.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgTotal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTotal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colDeliveryDate,
            this.colDOImportCode,
            this.colCompanyCode,
            this.colCompanyName,
            this.colProductDescription,
            this.colProductLot1,
            this.colDOQuantity,
            this.colQuantity1,
            this.colCartonOddQuantity});
            this.dtgTotal.DataSource = this.bdsTotal;
            this.dtgTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgTotal.GridColor = System.Drawing.SystemColors.Control;
            this.dtgTotal.Location = new System.Drawing.Point(3, 3);
            this.dtgTotal.Name = "dtgTotal";
            this.dtgTotal.Size = new System.Drawing.Size(967, 351);
            this.dtgTotal.TabIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnLoadQRCode);
            this.tabPage2.Controls.Add(this.dtgProductPacking);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(973, 357);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Danh sách QRCode";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnLoadQRCode
            // 
            this.btnLoadQRCode.BackColor = System.Drawing.Color.Green;
            this.btnLoadQRCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoadQRCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadQRCode.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnLoadQRCode.Location = new System.Drawing.Point(6, 4);
            this.btnLoadQRCode.Name = "btnLoadQRCode";
            this.btnLoadQRCode.Size = new System.Drawing.Size(75, 30);
            this.btnLoadQRCode.TabIndex = 46;
            this.btnLoadQRCode.Text = "Tìm";
            this.btnLoadQRCode.UseVisualStyleBackColor = false;
            this.btnLoadQRCode.Click += new System.EventHandler(this.btnLoadQRCode_Click);
            // 
            // colDeliveryDate
            // 
            this.colDeliveryDate.DataPropertyName = "StrDeliveryDate";
            this.colDeliveryDate.HeaderText = "Ngày Giao Hàng";
            this.colDeliveryDate.Name = "colDeliveryDate";
            this.colDeliveryDate.ReadOnly = true;
            // 
            // colDOImportCode
            // 
            this.colDOImportCode.DataPropertyName = "DOImportCode";
            this.colDOImportCode.HeaderText = "DOImportCode";
            this.colDOImportCode.Name = "colDOImportCode";
            this.colDOImportCode.ReadOnly = true;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.DataPropertyName = "CompanyCode";
            this.colCompanyCode.HeaderText = "Mã Công Ty";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.ReadOnly = true;
            // 
            // colCompanyName
            // 
            this.colCompanyName.DataPropertyName = "CompanyName";
            this.colCompanyName.HeaderText = "Tên Công Ty";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.ReadOnly = true;
            this.colCompanyName.Width = 300;
            // 
            // colProductDescription
            // 
            this.colProductDescription.DataPropertyName = "ProductDescription";
            this.colProductDescription.HeaderText = "Sản Phẩm";
            this.colProductDescription.Name = "colProductDescription";
            this.colProductDescription.ReadOnly = true;
            this.colProductDescription.Width = 300;
            // 
            // colProductLot1
            // 
            this.colProductLot1.DataPropertyName = "ProductLot";
            this.colProductLot1.HeaderText = "Số Lô";
            this.colProductLot1.Name = "colProductLot1";
            this.colProductLot1.ReadOnly = true;
            // 
            // colDOQuantity
            // 
            this.colDOQuantity.DataPropertyName = "DOQuantity";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colDOQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.colDOQuantity.HeaderText = "Số lượng theo DO";
            this.colDOQuantity.Name = "colDOQuantity";
            this.colDOQuantity.ReadOnly = true;
            // 
            // colQuantity1
            // 
            this.colQuantity1.DataPropertyName = "Quantity";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colQuantity1.DefaultCellStyle = dataGridViewCellStyle6;
            this.colQuantity1.HeaderText = "Số Lượng";
            this.colQuantity1.Name = "colQuantity1";
            this.colQuantity1.ReadOnly = true;
            // 
            // colCartonOddQuantity
            // 
            this.colCartonOddQuantity.DataPropertyName = "CartonOddQuantity";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colCartonOddQuantity.DefaultCellStyle = dataGridViewCellStyle7;
            this.colCartonOddQuantity.HeaderText = "Số Thùng";
            this.colCartonOddQuantity.Name = "colCartonOddQuantity";
            this.colCartonOddQuantity.ReadOnly = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dtgPallet);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(973, 357);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Danh sách pallet đang được giữ bởi nhóm soạn hàng";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dtgPallet
            // 
            this.dtgPallet.AllowUserToAddRows = false;
            this.dtgPallet.AllowUserToDeleteRows = false;
            this.dtgPallet.AllowUserToOrderColumns = true;
            this.dtgPallet.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Azure;
            this.dtgPallet.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgPallet.AutoGenerateColumns = false;
            this.dtgPallet.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPallet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPallet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colStrDeliveryDate3,
            this.colDOImportCode3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn27});
            this.dtgPallet.DataSource = this.bdsPalletList;
            this.dtgPallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallet.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPallet.Location = new System.Drawing.Point(0, 0);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.ReadOnly = true;
            this.dtgPallet.Size = new System.Drawing.Size(973, 357);
            this.dtgPallet.TabIndex = 11;
            // 
            // colStrDeliveryDate3
            // 
            this.colStrDeliveryDate3.DataPropertyName = "StrDeliveryDate";
            this.colStrDeliveryDate3.HeaderText = "Ngày giao hàng";
            this.colStrDeliveryDate3.Name = "colStrDeliveryDate3";
            this.colStrDeliveryDate3.ReadOnly = true;
            // 
            // colDOImportCode3
            // 
            this.colDOImportCode3.DataPropertyName = "DOImportCode";
            this.colDOImportCode3.HeaderText = "DOImportCode";
            this.colDOImportCode3.Name = "colDOImportCode3";
            this.colDOImportCode3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PalletCode";
            this.dataGridViewTextBoxColumn4.HeaderText = "Mã Pallet";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "StrStatus";
            this.dataGridViewTextBoxColumn5.HeaderText = "Trạng thái";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "UserFullName";
            this.dataGridViewTextBoxColumn10.HeaderText = "Người sử dụng";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 130;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "CompanyName";
            this.dataGridViewTextBoxColumn11.HeaderText = "Tên Công Ty";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 250;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn16.HeaderText = "Mã SP";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "ProductDescription";
            this.dataGridViewTextBoxColumn17.HeaderText = "Tên SP";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 200;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "ProductLot";
            this.dataGridViewTextBoxColumn18.HeaderText = "Số lô";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "Quantity";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            this.dataGridViewTextBoxColumn27.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn27.HeaderText = "Số lượng";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            // 
            // ReportDeliveryBarcodeHistoryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 446);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.cmbCompanyCode);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "ReportDeliveryBarcodeHistoryView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsTotal)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPalletList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSave;
        private CustomControls.MultiColumnComboBox cmbCompanyCode;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgTotal;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.BindingSource bdsTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn StrDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductBarcode;
        private System.Windows.Forms.Button btnLoadQRCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOImportCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonOddQuantity;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dtgPallet;
        private System.Windows.Forms.BindingSource bdsPalletList;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrDeliveryDate3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDOImportCode3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
    }
}