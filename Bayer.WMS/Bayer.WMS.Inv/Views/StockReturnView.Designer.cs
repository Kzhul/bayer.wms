﻿namespace Bayer.WMS.Inv.Views
{
    partial class StockReturnView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StockReturnView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtCreatedBy = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnDetail_Delete = new System.Windows.Forms.Button();
            this.btnDetail_Upload = new System.Windows.Forms.Button();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtImportStatus = new System.Windows.Forms.TextBox();
            this.btnDownload = new System.Windows.Forms.Button();
            this.dtgDetail = new System.Windows.Forms.DataGridView();
            this.colLineNbr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackageSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReturnQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceivedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReturnedFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReceivedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQA_QCCheck = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDetail_Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbCode
            // 
            this.cmbCode.Columns = null;
            this.cmbCode.DropDownHeight = 1;
            this.cmbCode.DropDownWidth = 500;
            this.cmbCode.FormattingEnabled = true;
            this.cmbCode.IntegralHeight = false;
            this.cmbCode.Location = new System.Drawing.Point(125, 12);
            this.cmbCode.MaxLength = 255;
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.PageSize = 0;
            this.cmbCode.PresenterInfo = null;
            this.cmbCode.Size = new System.Drawing.Size(400, 24);
            this.cmbCode.Source = null;
            this.cmbCode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã:";
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Location = new System.Drawing.Point(125, 72);
            this.txtCreatedBy.MaxLength = 255;
            this.txtCreatedBy.Multiline = true;
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.ReadOnly = true;
            this.txtCreatedBy.Size = new System.Drawing.Size(400, 24);
            this.txtCreatedBy.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Người tạo phiếu:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(584, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 16);
            this.label7.TabIndex = 25;
            this.label7.Text = "Ngày trả hàng:";
            // 
            // btnDetail_Delete
            // 
            this.btnDetail_Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDetail_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btnDetail_Delete.Image")));
            this.btnDetail_Delete.Location = new System.Drawing.Point(928, 112);
            this.btnDetail_Delete.Name = "btnDetail_Delete";
            this.btnDetail_Delete.Size = new System.Drawing.Size(50, 30);
            this.btnDetail_Delete.TabIndex = 27;
            this.btnDetail_Delete.TabStop = false;
            this.btnDetail_Delete.UseVisualStyleBackColor = true;
            this.btnDetail_Delete.Click += new System.EventHandler(this.btnDetail_Delete_Click);
            // 
            // btnDetail_Upload
            // 
            this.btnDetail_Upload.Image = ((System.Drawing.Image)(resources.GetObject("btnDetail_Upload.Image")));
            this.btnDetail_Upload.Location = new System.Drawing.Point(13, 112);
            this.btnDetail_Upload.Name = "btnDetail_Upload";
            this.btnDetail_Upload.Size = new System.Drawing.Size(105, 30);
            this.btnDetail_Upload.TabIndex = 7;
            this.btnDetail_Upload.TabStop = false;
            this.btnDetail_Upload.UseVisualStyleBackColor = true;
            this.btnDetail_Upload.Click += new System.EventHandler(this.btnDetail_Upload_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(125, 42);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(400, 24);
            this.txtDescription.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 45);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 16);
            this.label12.TabIndex = 50;
            this.label12.Text = "Ghi chú:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(584, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 81;
            this.label4.Text = "Trạng thái:";
            // 
            // dtpDeliveryDate
            // 
            this.dtpDeliveryDate.CustomFormat = "dd.MM.yyyy";
            this.dtpDeliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDeliveryDate.Location = new System.Drawing.Point(720, 12);
            this.dtpDeliveryDate.Name = "dtpDeliveryDate";
            this.dtpDeliveryDate.Size = new System.Drawing.Size(90, 22);
            this.dtpDeliveryDate.TabIndex = 4;
            // 
            // txtImportStatus
            // 
            this.txtImportStatus.Location = new System.Drawing.Point(720, 42);
            this.txtImportStatus.MaxLength = 255;
            this.txtImportStatus.Multiline = true;
            this.txtImportStatus.Name = "txtImportStatus";
            this.txtImportStatus.ReadOnly = true;
            this.txtImportStatus.Size = new System.Drawing.Size(150, 24);
            this.txtImportStatus.TabIndex = 83;
            // 
            // btnDownload
            // 
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownload.Location = new System.Drawing.Point(125, 112);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(108, 30);
            this.btnDownload.TabIndex = 86;
            this.btnDownload.TabStop = false;
            this.btnDownload.Text = "2.Download";
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // dtgDetail
            // 
            this.dtgDetail.AllowUserToAddRows = false;
            this.dtgDetail.AllowUserToDeleteRows = false;
            this.dtgDetail.AllowUserToOrderColumns = true;
            this.dtgDetail.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgDetail.AutoGenerateColumns = false;
            this.dtgDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLineNbr,
            this.colDetail_ProductCode,
            this.colDetail_ProductDescription,
            this.colPackageSize,
            this.colDetail_UOM,
            this.colBatchCode,
            this.colReturnQty,
            this.colReceivedQty,
            this.colPalletCode,
            this.colReturnedFrom,
            this.colReceivedBy,
            this.colQA_QCCheck,
            this.colDetail_Description,
            this.colDetail_Status});
            this.dtgDetail.DataSource = this.bdsDetails;
            this.dtgDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dtgDetail.Location = new System.Drawing.Point(12, 148);
            this.dtgDetail.Name = "dtgDetail";
            this.dtgDetail.ReadOnly = true;
            this.dtgDetail.Size = new System.Drawing.Size(966, 443);
            this.dtgDetail.TabIndex = 87;
            // 
            // colLineNbr
            // 
            this.colLineNbr.DataPropertyName = "LineNbr";
            this.colLineNbr.Frozen = false;
            this.colLineNbr.HeaderText = "#";
            this.colLineNbr.Name = "colLineNbr";
            this.colLineNbr.ReadOnly = true;
            // 
            // colDetail_ProductCode
            // 
            this.colDetail_ProductCode.DataPropertyName = "ProductCode";
            this.colDetail_ProductCode.Frozen = false;
            this.colDetail_ProductCode.HeaderText = "Mã hàng";
            this.colDetail_ProductCode.Name = "colDetail_ProductCode";
            this.colDetail_ProductCode.ReadOnly = true;
            // 
            // colDetail_ProductDescription
            // 
            this.colDetail_ProductDescription.DataPropertyName = "ProductDescription";
            dataGridViewCellStyle2.NullValue = null;
            this.colDetail_ProductDescription.DefaultCellStyle = dataGridViewCellStyle2;
            this.colDetail_ProductDescription.Frozen = false;
            this.colDetail_ProductDescription.HeaderText = "Tên hàng";
            this.colDetail_ProductDescription.Name = "colDetail_ProductDescription";
            this.colDetail_ProductDescription.ReadOnly = true;
            this.colDetail_ProductDescription.Width = 200;
            // 
            // colPackageSize
            // 
            this.colPackageSize.DataPropertyName = "PackageSize";
            this.colPackageSize.Frozen = false;
            this.colPackageSize.HeaderText = "Quy Cách";
            this.colPackageSize.Name = "colPackageSize";
            this.colPackageSize.ReadOnly = true;
            // 
            // colDetail_UOM
            // 
            this.colDetail_UOM.DataPropertyName = "UOM";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colDetail_UOM.DefaultCellStyle = dataGridViewCellStyle3;
            this.colDetail_UOM.Frozen = false;
            this.colDetail_UOM.HeaderText = "ĐVT";
            this.colDetail_UOM.Name = "colDetail_UOM";
            this.colDetail_UOM.ReadOnly = true;
            this.colDetail_UOM.Width = 50;
            // 
            // colBatchCode
            // 
            this.colBatchCode.DataPropertyName = "BatchCode";
            this.colBatchCode.Frozen = false;
            this.colBatchCode.HeaderText = "Số lô";
            this.colBatchCode.Name = "colBatchCode";
            this.colBatchCode.ReadOnly = true;
            // 
            // colReturnQty
            // 
            this.colReturnQty.DataPropertyName = "ReturnQty";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N3";
            this.colReturnQty.DefaultCellStyle = dataGridViewCellStyle4;
            this.colReturnQty.Frozen = false;
            this.colReturnQty.HeaderText = "Số lượng trả hàng";
            this.colReturnQty.Name = "colReturnQty";
            this.colReturnQty.ReadOnly = true;
            // 
            // colReceivedQty
            // 
            this.colReceivedQty.DataPropertyName = "ReceivedQty";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colReceivedQty.DefaultCellStyle = dataGridViewCellStyle5;
            this.colReceivedQty.Frozen = false;
            this.colReceivedQty.HeaderText = "Số lượng đã nhận";
            this.colReceivedQty.Name = "colReceivedQty";
            this.colReceivedQty.ReadOnly = true;
            // 
            // colPalletCode
            // 
            this.colPalletCode.DataPropertyName = "PalletCode";
            this.colPalletCode.Frozen = false;
            this.colPalletCode.HeaderText = "Mã Pallet";
            this.colPalletCode.Name = "colPalletCode";
            this.colPalletCode.ReadOnly = true;
            // 
            // colReturnedFrom
            // 
            this.colReturnedFrom.DataPropertyName = "ReturnedFrom";
            this.colReturnedFrom.Frozen = false;
            this.colReturnedFrom.HeaderText = "Người trả";
            this.colReturnedFrom.Name = "colReturnedFrom";
            this.colReturnedFrom.ReadOnly = true;
            // 
            // colReceivedBy
            // 
            this.colReceivedBy.DataPropertyName = "StrReceivedBy";
            this.colReceivedBy.Frozen = false;
            this.colReceivedBy.HeaderText = "Người nhận";
            this.colReceivedBy.Name = "colReceivedBy";
            this.colReceivedBy.ReadOnly = true;
            this.colReceivedBy.Width = 200;
            // 
            // colQA_QCCheck
            // 
            this.colQA_QCCheck.DataPropertyName = "QA_QCCheck";
            this.colQA_QCCheck.Frozen = false;
            this.colQA_QCCheck.HeaderText = "Kiểm tra bởi QA/QC";
            this.colQA_QCCheck.Name = "colQA_QCCheck";
            this.colQA_QCCheck.ReadOnly = true;
            // 
            // colDetail_Description
            // 
            this.colDetail_Description.DataPropertyName = "Description";
            this.colDetail_Description.HeaderText = "Ghi chú";
            this.colDetail_Description.Name = "colDetail_Description";
            this.colDetail_Description.ReadOnly = true;
            this.colDetail_Description.Width = 200;
            // 
            // colDetail_Status
            // 
            this.colDetail_Status.DataPropertyName = "StatusDisplay";
            this.colDetail_Status.HeaderText = "Trạng thái";
            this.colDetail_Status.Name = "colDetail_Status";
            this.colDetail_Status.ReadOnly = true;
            this.colDetail_Status.Width = 150;
            // 
            // StockReturnView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 633);
            this.Controls.Add(this.dtgDetail);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.btnDetail_Delete);
            this.Controls.Add(this.btnDetail_Upload);
            this.Controls.Add(this.txtImportStatus);
            this.Controls.Add(this.dtpDeliveryDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCreatedBy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCode);
            this.Name = "StockReturnView";
            this.Text = "ProductPackingMaintView";
            this.Load += new System.EventHandler(this.StockReturnView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsDetails;
        private CustomControls.MultiColumnComboBox cmbCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCreatedBy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDeliveryDate;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnDetail_Upload;
        private System.Windows.Forms.TextBox txtImportStatus;
        private System.Windows.Forms.Button btnDetail_Delete;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.DataGridView dtgDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineNbr;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackageSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReturnQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceivedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReturnedFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReceivedBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQA_QCCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDetail_Status;
    }
}