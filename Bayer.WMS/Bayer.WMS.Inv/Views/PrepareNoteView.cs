﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class PrepareNoteView : BaseForm, IPrepareNoteView
    {
        public PrepareNoteView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductPacking);
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "PrepareCode";
            cmbCode.DisplayMember = "PrepareCode - Description";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {

                new MultiColumnComboBox.ComboBoxColumn("StrDeliveryDate", "Ngày giao hàng", 130),
                new MultiColumnComboBox.ComboBoxColumn("PrepareCode", "Mã", 130),
                new MultiColumnComboBox.ComboBoxColumn("CompanyCode", "Mã NPP", 130),
                new MultiColumnComboBox.ComboBoxColumn("CompanyName", "Tên NPP", 300),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 130),
                new MultiColumnComboBox.ComboBoxColumn("DOImportCode", "Mã DO", 130)
                
            };
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "GetUsers", "OnCodeSelectChange");
            cmbCode.DropDownWidth = 800;
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            InitializeComboBox();

            var doPresenter = _presenter as IPrepareNotePresenter;
            await Task.WhenAll(doPresenter.LoadPrepareNoteHeader());

            if (!isRefresh)
                doPresenter.Insert();
        }

        public string DOImportCode
        {
            get
            {
                return cmbCode.SelectedItem["PrepareCode"].ToString();
            }
        }

        public DataTable PrepareNoteHeaders
        {
            set { cmbCode.Source = value; }
        }

        private PrepareNoteHeader _PrepareNoteHeader;

        public PrepareNoteHeader PrepareNoteHeader
        {
            get { return _PrepareNoteHeader; }
            set
            {
                _PrepareNoteHeader = value;

                cmbCode.DataBindings.Clear();
                txtProductDescription.DataBindings.Clear();
                txtStatusDisplay.DataBindings.Clear();
                txtDeliveryDate.DataBindings.Clear();
                txtDOImportCode.DataBindings.Clear();
                txtCompanyCode.DataBindings.Clear();
                txtCompanyName.DataBindings.Clear();
                txtProvince.DataBindings.Clear();
                txtUserCreateFullName.DataBindings.Clear();
                txtUserWareHouse.DataBindings.Clear();
                txtUserReview.DataBindings.Clear();


                cmbCode.DataBindings.Add("Text", PrepareNoteHeader, "PrepareCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProductDescription.DataBindings.Add("Text", PrepareNoteHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStatusDisplay.DataBindings.Add("Text", PrepareNoteHeader, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDeliveryDate.DataBindings.Add("Text", PrepareNoteHeader, "StrDeliveryDate", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDOImportCode.DataBindings.Add("Text", PrepareNoteHeader, "DOImportCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCompanyCode.DataBindings.Add("Text", PrepareNoteHeader, "CompanyCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCompanyName.DataBindings.Add("Text", PrepareNoteHeader, "CompanyName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProvince.DataBindings.Add("Text", PrepareNoteHeader, "Province", true, DataSourceUpdateMode.OnPropertyChanged);
                txtUserCreateFullName.DataBindings.Add("Text", PrepareNoteHeader, "UserCreateFullName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtUserWareHouse.DataBindings.Add("Text", PrepareNoteHeader, "UserWareHouse", true, DataSourceUpdateMode.OnPropertyChanged);
                txtUserReview.DataBindings.Add("Text", PrepareNoteHeader, "UserReview", true, DataSourceUpdateMode.OnPropertyChanged);

                //if (!string.IsNullOrEmpty(value.PrepareCode))
                //{
                //    btnConfirm.Visible = value.Status != DeliveryNoteHeader.status.Complete;
                //}

                txtStatusDisplay.ForeColor = Utility.StatusToColor(this.PrepareNoteHeader.Status);
            }
        }

        private IList<PrepareNoteDetail> _PrepareNoteDetail;

        public IList<PrepareNoteDetail> PrepareNoteDetails
        {
            get { return _PrepareNoteDetail; }
            set
            {
                _PrepareNoteDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _PrepareNoteDetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _PrepareNoteDetail;
                }
            }
        }

        private IList<PrepareNoteDetail> _deletedPrepareNoteDetails;

        public IList<PrepareNoteDetail> DeletedPrepareNoteDetails
        {
            get { return _deletedPrepareNoteDetails; }
            set { _deletedPrepareNoteDetails = value; }
        }


        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }


        //protected BayerWMSContext _context;
        //protected SqlConnection _conn;

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                BayerWMSContext db = new BayerWMSContext();

                if (this.PrepareNoteDetails.Count == 0)
                {
                    this.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                    return;
                }

                #region VerifyDetailQuantity
                //Get by DO Code and CompanyCode
                var listOrderDetail = db.DeliveryOrderDetails.Where(a => a.DOImportCode == this.PrepareNoteHeader.DOImportCode).ToList();
                //Get by DO Code and CompanyCode
                //var listAnotherNoteDetails = db.PrepareNoteDetails.Where(a => a.DOCode == this.PrepareNoteHeader.DOImportCode && a.PrepareCode != this.PrepareNoteHeader.PrepareCode).ToList();
                var listAnotherNoteDetails = (from a in db.PrepareNoteDetails
                                              join b in db.PrepareNoteHeaders
                                              on new { a.DOCode, a.PrepareCode } equals new { DOCode = b.DOImportCode, b.PrepareCode }
                                              where a.DOCode == this.PrepareNoteHeader.DOImportCode
                                              && b.PrepareCode != this.PrepareNoteHeader.PrepareCode
                                              select a).Distinct().ToList();

                string productErr = string.Empty;
                if (listOrderDetail != null && listOrderDetail.Count > 0)
                {
                    foreach (var item in listOrderDetail)
                    {
                        #region Total Prepare Quantity of this DO detail item
                        decimal totalNoteQuantity = 0;
                        decimal? totalAnotherNoteQuantity = listAnotherNoteDetails.Where(a =>
                                                                a.ProductID == item.ProductID
                                                                && a.BatchCode == item.BatchCode
                                                                && a.DeliveryCode == item.Delivery
                                                                && a.Quantity.HasValue
                                                            ).Sum(a => a.Quantity);
                        totalAnotherNoteQuantity = totalAnotherNoteQuantity ?? totalAnotherNoteQuantity;
                        decimal? totalThisNoteQuantity = this.PrepareNoteDetails.Where(a =>
                                                                a.ProductID == item.ProductID
                                                                && a.BatchCode == item.BatchCode
                                                                && a.DeliveryCode == item.Delivery
                                                                && a.Quantity.HasValue
                                                            ).Sum(a => a.Quantity);
                        totalThisNoteQuantity = totalThisNoteQuantity ?? totalThisNoteQuantity;

                        totalNoteQuantity = totalThisNoteQuantity.Value + totalAnotherNoteQuantity.Value;
                        #endregion

                        decimal? totalOrderQuantity = item.Quantity ?? 0;

                        totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                        if (totalNoteQuantity > totalOrderQuantity)
                        {
                            decimal? totalNoteQuantityValid = totalOrderQuantity - totalAnotherNoteQuantity;
                            var firstDetailErr = this.PrepareNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).FirstOrDefault();
                            productErr = firstDetailErr.ProductCode + "----" + firstDetailErr.BatchCode + "----O:" + totalOrderQuantity.ToString() + "----A:" + totalAnotherNoteQuantity.ToString() + "----N:" + totalNoteQuantityValid.ToString();
                            //throw new WrappedException(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr);
                            this.SetMessage(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr, Utility.MessageType.Error);
                            break;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(productErr))
                {
                    return;
                }
                #endregion

                #region Save DN Header and Details
                string lastestNumber = string.Empty;
                //Insert
                if (string.IsNullOrEmpty(this.PrepareNoteHeader.PrepareCode))
                {
                    var model = db.PrepareNoteHeaders.Where(a => a.CreatedDateTime.HasValue && a.CreatedDateTime.Value >= DateTime.Today.Date).OrderByDescending(a => a.PrepareCode).FirstOrDefault();
                    if (model != null)
                    {
                        lastestNumber = model.PrepareCode;
                        string a = lastestNumber.Substring(lastestNumber.Length - 2); ;
                        int NextDONumber = Convert.ToInt32(a) + 1;
                        lastestNumber = DateTime.Today.ToString("ddMMyy") + "P" + NextDONumber.ToString("00");
                    }
                    else
                    {
                        int NextDONumber = 1;
                        lastestNumber = DateTime.Today.ToString("ddMMyy") + "P" + NextDONumber.ToString("00");
                    }

                    this.PrepareNoteHeader.PrepareCode = lastestNumber;
                    this.PrepareNoteHeader.Status = DeliveryNoteHeader.status.New;
                    this.PrepareNoteHeader.IsDeleted = false;
                    this.PrepareNoteHeader.CreatedBy = LoginInfo.UserID;
                    this.PrepareNoteHeader.CreatedBySitemapID = LoginInfo.SitemapID;
                    this.PrepareNoteHeader.CreatedDateTime = DateTime.Now;
                    this.PrepareNoteHeader.UpdatedBy = LoginInfo.UserID;
                    this.PrepareNoteHeader.UpdatedBySitemapID = LoginInfo.SitemapID;
                    this.PrepareNoteHeader.UpdatedDateTime = DateTime.Now;

                    db.PrepareNoteHeaders.Add(this.PrepareNoteHeader);
                }
                //Update
                else
                {
                    var modelUpdate = db.PrepareNoteHeaders.Where(a => a.PrepareCode == this.PrepareNoteHeader.PrepareCode).FirstOrDefault();
                    modelUpdate.Description = this.PrepareNoteHeader.Description;
                    modelUpdate.DeliveryDate = this.PrepareNoteHeader.DeliveryDate;
                    modelUpdate.UpdatedBy = LoginInfo.UserID;
                    modelUpdate.UpdatedBySitemapID = LoginInfo.SitemapID;
                    modelUpdate.UpdatedDateTime = DateTime.Now;
                }


                //Detail
                ////Delete all and insert new
                var listDetailOld = db.PrepareNoteDetails.Where(a => a.PrepareCode == this.PrepareNoteHeader.PrepareCode).ToList();
                foreach (PrepareNoteDetail item in listDetailOld)
                {
                    db.PrepareNoteDetails.Remove(item);
                }

                foreach (PrepareNoteDetail item in this.PrepareNoteDetails)
                {
                    item.PrepareCode = this.PrepareNoteHeader.PrepareCode;
                    item.Status = DeliveryNoteHeader.status.New;
                    item.CreatedBy = LoginInfo.UserID;
                    item.CreatedBySitemapID = LoginInfo.SitemapID;
                    item.CreatedDateTime = DateTime.Now;
                    item.UpdatedBy = LoginInfo.UserID;
                    item.UpdatedBySitemapID = LoginInfo.SitemapID;
                    item.UpdatedDateTime = DateTime.Now;
                    db.PrepareNoteDetails.Add(item);
                }

                db.SaveChanges();
                #endregion

                #region Update DO Status
                bool isPrepareFull = true;
                var listNoteDetails = db.PrepareNoteDetails.Where(a => a.DOCode == this.PrepareNoteHeader.DOImportCode).ToList();
                if (listNoteDetails != null && listNoteDetails.Count > 0)
                {
                    foreach (var item in listOrderDetail)
                    {
                        decimal? totalNoteQuantity = listNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue && a.DeliveryCode == item.Delivery).Sum(a => a.Quantity);
                        totalNoteQuantity = totalNoteQuantity ?? totalNoteQuantity;

                        decimal? totalOrderQuantity = listOrderDetail.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue && a.Delivery == item.Delivery).Sum(a => a.Quantity);
                        totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                        //if not yet export full of one product, can create
                        if (totalNoteQuantity < totalOrderQuantity)
                        {
                            isPrepareFull = false;
                            break;
                        }
                    }
                }
                else
                {
                    isPrepareFull = false;
                }

                var doHeader = db.DeliveryOrderHeaders.Where(a => a.DOImportCode == this.PrepareNoteHeader.DOImportCode && !a.IsDeleted).FirstOrDefault();
                //doHeader.IsPrepareFull = isPrepareFull;
                db.SaveChanges();
                #endregion
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                //await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    //isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                //_PrepareNoteHeaderRepository = _mainPresenter.Resolve(_PrepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;
                //_PrepareNoteDetailRepository = _mainPresenter.Resolve(_PrepareNoteDetailRepository.GetType()) as IPrepareNoteDetailRepository;

                ////// if insert or update sucessfully, refresh combobox
                //if (isSuccess)
                //{
                //    await LoadPrepareNoteHeader();

                //    _mainView.PrepareNoteHeader = header;
                //    _mainView.PrepareNoteDetails = details;
                //}
                MessageBox.Show("Lưu thành công !");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public async void Print()
        {
            var doPresenter = _presenter as IPrepareNotePresenter;
            await doPresenter.Print();
        }

        private async void btnConfirm_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IPrepareNotePresenter;
            await doPresenter.Confirm();
        }
    }
}
