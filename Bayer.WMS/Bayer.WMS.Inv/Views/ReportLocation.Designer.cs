﻿namespace Bayer.WMS.Inv.Views
{
    partial class ReportLocationView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsZone = new System.Windows.Forms.BindingSource(this.components);
            this.bdsTopCustomer = new System.Windows.Forms.BindingSource(this.components);
            this.bdsZoneChart = new System.Windows.Forms.BindingSource(this.components);
            this.bdsLine = new System.Windows.Forms.BindingSource(this.components);
            this.bdsLocation = new System.Windows.Forms.BindingSource(this.components);
            this.bdsPallet = new System.Windows.Forms.BindingSource(this.components);
            this.txtProduct = new System.Windows.Forms.TextBox();
            this.txtProductLot = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgZone = new System.Windows.Forms.DataGridView();
            this.colZoneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTotal1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCheckedLocation1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmptyLocation1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dtgLine = new System.Windows.Forms.DataGridView();
            this.colZoneName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLineName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTotal2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCheckedLocation2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEmptyLocation2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dtgLocation = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dtgPallet = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationCode4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductLot4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductionPlanNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabChart = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtgTopCustomer = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgZoneList = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chartZone2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartZone1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.rdbWarehouseVerifyDate = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.btnDownload = new System.Windows.Forms.Button();
            this.txtLineName = new System.Windows.Forms.TextBox();
            this.txtWarehouseName = new System.Windows.Forms.TextBox();
            this.txtZoneName = new System.Windows.Forms.TextBox();
            this.colZoneName3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLineName3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationCode3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCartonQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colManufacturingDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrDriverReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrDriverReceivedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrWarehouseKeeper = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrWarehouseVerifyDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWarehouseVerifyNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrDriver = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrLocationPutDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLastLocationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsTopCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallet)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgZone)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgLine)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgLocation)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallet)).BeginInit();
            this.tabChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTopCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartZone2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartZone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtProduct
            // 
            this.txtProduct.Location = new System.Drawing.Point(113, 9);
            this.txtProduct.Name = "txtProduct";
            this.txtProduct.Size = new System.Drawing.Size(216, 22);
            this.txtProduct.TabIndex = 48;
            // 
            // txtProductLot
            // 
            this.txtProductLot.Location = new System.Drawing.Point(113, 40);
            this.txtProductLot.Name = "txtProductLot";
            this.txtProductLot.Size = new System.Drawing.Size(216, 22);
            this.txtProductLot.TabIndex = 47;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 46;
            this.label2.Text = "Số lô:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabChart);
            this.tabControl1.Location = new System.Drawing.Point(12, 104);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1196, 664);
            this.tabControl1.TabIndex = 45;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgZone);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1188, 635);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách khu vực";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgZone
            // 
            this.dtgZone.AllowUserToAddRows = false;
            this.dtgZone.AllowUserToDeleteRows = false;
            this.dtgZone.AllowUserToOrderColumns = true;
            this.dtgZone.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgZone.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgZone.AutoGenerateColumns = false;
            this.dtgZone.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgZone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgZone.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneName,
            this.colTotal1,
            this.colCheckedLocation1,
            this.colEmptyLocation1,
            this.colDescription1});
            this.dtgZone.DataSource = this.bdsZone;
            this.dtgZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgZone.GridColor = System.Drawing.SystemColors.Control;
            this.dtgZone.Location = new System.Drawing.Point(3, 3);
            this.dtgZone.Name = "dtgZone";
            this.dtgZone.Size = new System.Drawing.Size(1182, 629);
            this.dtgZone.TabIndex = 7;
            // 
            // colZoneName
            // 
            this.colZoneName.DataPropertyName = "ZoneName";
            this.colZoneName.HeaderText = "Khu vực";
            this.colZoneName.Name = "colZoneName";
            this.colZoneName.ReadOnly = true;
            // 
            // colTotal1
            // 
            this.colTotal1.DataPropertyName = "Total";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colTotal1.DefaultCellStyle = dataGridViewCellStyle2;
            this.colTotal1.HeaderText = "Tổng vị trí";
            this.colTotal1.Name = "colTotal1";
            this.colTotal1.ReadOnly = true;
            // 
            // colCheckedLocation1
            // 
            this.colCheckedLocation1.DataPropertyName = "CheckedLocation";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colCheckedLocation1.DefaultCellStyle = dataGridViewCellStyle3;
            this.colCheckedLocation1.HeaderText = "Đang chứa hàng";
            this.colCheckedLocation1.Name = "colCheckedLocation1";
            this.colCheckedLocation1.ReadOnly = true;
            // 
            // colEmptyLocation1
            // 
            this.colEmptyLocation1.DataPropertyName = "EmptyLocation";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colEmptyLocation1.DefaultCellStyle = dataGridViewCellStyle4;
            this.colEmptyLocation1.HeaderText = "Trống";
            this.colEmptyLocation1.Name = "colEmptyLocation1";
            this.colEmptyLocation1.ReadOnly = true;
            // 
            // colDescription1
            // 
            this.colDescription1.DataPropertyName = "Description";
            this.colDescription1.HeaderText = "Ghi chú";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.ReadOnly = true;
            this.colDescription1.Width = 200;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgLine);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1188, 635);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Chi tiết khu vực";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dtgLine
            // 
            this.dtgLine.AllowUserToAddRows = false;
            this.dtgLine.AllowUserToDeleteRows = false;
            this.dtgLine.AllowUserToOrderColumns = true;
            this.dtgLine.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Azure;
            this.dtgLine.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dtgLine.AutoGenerateColumns = false;
            this.dtgLine.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgLine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgLine.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneName2,
            this.colLineName2,
            this.colLevel,
            this.colLength,
            this.colTotal2,
            this.colCheckedLocation2,
            this.colEmptyLocation2,
            this.colDescription2});
            this.dtgLine.DataSource = this.bdsLine;
            this.dtgLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgLine.GridColor = System.Drawing.SystemColors.Control;
            this.dtgLine.Location = new System.Drawing.Point(3, 3);
            this.dtgLine.Name = "dtgLine";
            this.dtgLine.Size = new System.Drawing.Size(1182, 629);
            this.dtgLine.TabIndex = 8;
            // 
            // colZoneName2
            // 
            this.colZoneName2.DataPropertyName = "ZoneName";
            this.colZoneName2.HeaderText = "Khu vực";
            this.colZoneName2.Name = "colZoneName2";
            this.colZoneName2.ReadOnly = true;
            // 
            // colLineName2
            // 
            this.colLineName2.DataPropertyName = "LineName";
            this.colLineName2.HeaderText = "Dãy";
            this.colLineName2.Name = "colLineName2";
            this.colLineName2.ReadOnly = true;
            // 
            // colLevel
            // 
            this.colLevel.DataPropertyName = "Level";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colLevel.DefaultCellStyle = dataGridViewCellStyle6;
            this.colLevel.HeaderText = "Tầng";
            this.colLevel.Name = "colLevel";
            this.colLevel.ReadOnly = true;
            // 
            // colLength
            // 
            this.colLength.DataPropertyName = "Length";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colLength.DefaultCellStyle = dataGridViewCellStyle7;
            this.colLength.HeaderText = "Dài";
            this.colLength.Name = "colLength";
            this.colLength.ReadOnly = true;
            // 
            // colTotal2
            // 
            this.colTotal2.DataPropertyName = "Total";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colTotal2.DefaultCellStyle = dataGridViewCellStyle8;
            this.colTotal2.HeaderText = "Số vị trí";
            this.colTotal2.Name = "colTotal2";
            this.colTotal2.ReadOnly = true;
            // 
            // colCheckedLocation2
            // 
            this.colCheckedLocation2.DataPropertyName = "CheckedLocation";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colCheckedLocation2.DefaultCellStyle = dataGridViewCellStyle9;
            this.colCheckedLocation2.HeaderText = "Đang chứa hàng";
            this.colCheckedLocation2.Name = "colCheckedLocation2";
            this.colCheckedLocation2.ReadOnly = true;
            // 
            // colEmptyLocation2
            // 
            this.colEmptyLocation2.DataPropertyName = "EmptyLocation";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colEmptyLocation2.DefaultCellStyle = dataGridViewCellStyle10;
            this.colEmptyLocation2.HeaderText = "Trống";
            this.colEmptyLocation2.Name = "colEmptyLocation2";
            this.colEmptyLocation2.ReadOnly = true;
            // 
            // colDescription2
            // 
            this.colDescription2.DataPropertyName = "Description";
            this.colDescription2.HeaderText = "Ghi chú";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.ReadOnly = true;
            this.colDescription2.Width = 200;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dtgLocation);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1188, 635);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Danh sách vị trí";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dtgLocation
            // 
            this.dtgLocation.AllowUserToAddRows = false;
            this.dtgLocation.AllowUserToDeleteRows = false;
            this.dtgLocation.AllowUserToOrderColumns = true;
            this.dtgLocation.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Azure;
            this.dtgLocation.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dtgLocation.AutoGenerateColumns = false;
            this.dtgLocation.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgLocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgLocation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneName3,
            this.colLineName3,
            this.colLocationCode3,
            this.colPalletCode,
            this.colProductLot,
            this.colProductCode,
            this.colProductName,
            this.colUOM,
            this.colPackSize,
            this.colCartonQuantity,
            this.colQuantity,
            this.colDescription3,
            this.colManufacturingDate,
            this.colExpiryDate,
            this.colStrDriverReceived,
            this.colStrDriverReceivedDate,
            this.colStrWarehouseKeeper,
            this.colStrWarehouseVerifyDate,
            this.colWarehouseVerifyNote,
            this.colStrDriver,
            this.colStrLocationPutDate,
            this.colLastLocationName});
            this.dtgLocation.DataSource = this.bdsLocation;
            this.dtgLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgLocation.GridColor = System.Drawing.SystemColors.Control;
            this.dtgLocation.Location = new System.Drawing.Point(0, 0);
            this.dtgLocation.Name = "dtgLocation";
            this.dtgLocation.Size = new System.Drawing.Size(1188, 635);
            this.dtgLocation.TabIndex = 9;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dtgPallet);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1188, 635);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Danh sách pallet thành phẩm chờ nhập kho";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dtgPallet
            // 
            this.dtgPallet.AllowUserToAddRows = false;
            this.dtgPallet.AllowUserToDeleteRows = false;
            this.dtgPallet.AllowUserToOrderColumns = true;
            this.dtgPallet.AllowUserToResizeRows = false;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.Azure;
            this.dtgPallet.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dtgPallet.AutoGenerateColumns = false;
            this.dtgPallet.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPallet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPallet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.colLocationCode4,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.colProductLot4,
            this.colProductionPlanNote,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19});
            this.dtgPallet.DataSource = this.bdsPallet;
            this.dtgPallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgPallet.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPallet.Location = new System.Drawing.Point(0, 0);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.Size = new System.Drawing.Size(1188, 635);
            this.dtgPallet.TabIndex = 10;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PalletCode";
            this.dataGridViewTextBoxColumn6.HeaderText = "Mã Pallet";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // colLocationCode4
            // 
            this.colLocationCode4.DataPropertyName = "LocationCode";
            this.colLocationCode4.HeaderText = "Mã vị trí gợi ý";
            this.colLocationCode4.Name = "colLocationCode4";
            this.colLocationCode4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "ManufacturingDate";
            this.dataGridViewTextBoxColumn21.HeaderText = "Ngày sản xuất";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "ExpiryDate";
            this.dataGridViewTextBoxColumn22.HeaderText = "Ngày hết hạn";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn13.HeaderText = "Mã SP";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "ProductName";
            this.dataGridViewTextBoxColumn14.HeaderText = "Tên SP";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 200;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "UOM";
            this.dataGridViewTextBoxColumn16.HeaderText = "ĐVT";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "PackageSize";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn17.HeaderText = "Quy cách";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // colProductLot4
            // 
            this.colProductLot4.DataPropertyName = "ProductLot";
            this.colProductLot4.HeaderText = "Số lô";
            this.colProductLot4.Name = "colProductLot4";
            this.colProductLot4.ReadOnly = true;
            // 
            // colProductionPlanNote
            // 
            this.colProductionPlanNote.DataPropertyName = "Note";
            this.colProductionPlanNote.HeaderText = "Ghi chú";
            this.colProductionPlanNote.Name = "colProductionPlanNote";
            this.colProductionPlanNote.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "CartonQuantity";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn18.HeaderText = "Số thùng";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "Quantity";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn19.HeaderText = "Số lượng SP";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // tabChart
            // 
            this.tabChart.Controls.Add(this.label6);
            this.tabChart.Controls.Add(this.label5);
            this.tabChart.Controls.Add(this.label4);
            this.tabChart.Controls.Add(this.label3);
            this.tabChart.Controls.Add(this.dtgTopCustomer);
            this.tabChart.Controls.Add(this.dtgZoneList);
            this.tabChart.Controls.Add(this.chartZone2);
            this.tabChart.Controls.Add(this.chartZone1);
            this.tabChart.Controls.Add(this.chart1);
            this.tabChart.Location = new System.Drawing.Point(4, 25);
            this.tabChart.Name = "tabChart";
            this.tabChart.Size = new System.Drawing.Size(1188, 635);
            this.tabChart.TabIndex = 3;
            this.tabChart.Text = "DashBoard";
            this.tabChart.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(999, 177);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 16);
            this.label6.TabIndex = 52;
            this.label6.Text = "Kho Thành Phẩm";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(688, 177);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 16);
            this.label5.TabIndex = 51;
            this.label5.Text = "Kho Nguyên Liệu";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 345);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(323, 16);
            this.label4.TabIndex = 50;
            this.label4.Text = "Biểu đồ tỉ lệ giao hàng thành công trong tháng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(291, 16);
            this.label3.TabIndex = 49;
            this.label3.Text = "Danh sách top 10 khách hàng trong tháng";
            // 
            // dtgTopCustomer
            // 
            this.dtgTopCustomer.AllowUserToAddRows = false;
            this.dtgTopCustomer.AllowUserToDeleteRows = false;
            this.dtgTopCustomer.AllowUserToOrderColumns = true;
            this.dtgTopCustomer.AllowUserToResizeRows = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.Azure;
            this.dtgTopCustomer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dtgTopCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgTopCustomer.AutoGenerateColumns = false;
            this.dtgTopCustomer.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgTopCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTopCustomer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.colCompanyName,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dtgTopCustomer.DataSource = this.bdsTopCustomer;
            this.dtgTopCustomer.GridColor = System.Drawing.SystemColors.Control;
            this.dtgTopCustomer.Location = new System.Drawing.Point(3, 28);
            this.dtgTopCustomer.Name = "dtgTopCustomer";
            this.dtgTopCustomer.Size = new System.Drawing.Size(596, 299);
            this.dtgTopCustomer.TabIndex = 11;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CompanyCode";
            this.dataGridViewTextBoxColumn2.HeaderText = "Mã khách hàng";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // colCompanyName
            // 
            this.colCompanyName.DataPropertyName = "CompanyName";
            this.colCompanyName.HeaderText = "Tên khách hàng";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.ReadOnly = true;
            this.colCompanyName.Width = 200;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "OrderQuantity";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewTextBoxColumn3.HeaderText = "Số lượng mua hàng (DO)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DeliveryQuantity";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn4.HeaderText = "Số lượng đã giao";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "DeliveryPercent";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewTextBoxColumn5.HeaderText = "% giao hàng thành công";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dtgZoneList
            // 
            this.dtgZoneList.AllowUserToAddRows = false;
            this.dtgZoneList.AllowUserToDeleteRows = false;
            this.dtgZoneList.AllowUserToOrderColumns = true;
            this.dtgZoneList.AllowUserToResizeRows = false;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.Azure;
            this.dtgZoneList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle23;
            this.dtgZoneList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgZoneList.AutoGenerateColumns = false;
            this.dtgZoneList.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgZoneList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgZoneList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn15});
            this.dtgZoneList.DataSource = this.bdsZoneChart;
            this.dtgZoneList.GridColor = System.Drawing.SystemColors.Control;
            this.dtgZoneList.Location = new System.Drawing.Point(646, 196);
            this.dtgZoneList.Name = "dtgZoneList";
            this.dtgZoneList.Size = new System.Drawing.Size(452, 131);
            this.dtgZoneList.TabIndex = 10;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ZoneName";
            this.dataGridViewTextBoxColumn1.HeaderText = "Khu vực";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Total";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn10.HeaderText = "Tổng";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 70;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "CheckedLocation";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn11.HeaderText = "Đang chứa hàng";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "EmptyLocation";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn15.HeaderText = "Trống";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 70;
            // 
            // chartZone2
            // 
            this.chartZone2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chartZone2.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartZone2.Legends.Add(legend1);
            this.chartZone2.Location = new System.Drawing.Point(907, 3);
            this.chartZone2.Name = "chartZone2";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartZone2.Series.Add(series1);
            this.chartZone2.Size = new System.Drawing.Size(278, 196);
            this.chartZone2.TabIndex = 2;
            this.chartZone2.Text = "Kho 2";
            // 
            // chartZone1
            // 
            this.chartZone1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chartZone1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartZone1.Legends.Add(legend2);
            this.chartZone1.Location = new System.Drawing.Point(605, 3);
            this.chartZone1.Name = "chartZone1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartZone1.Series.Add(series2);
            this.chartZone1.Size = new System.Drawing.Size(278, 196);
            this.chartZone1.TabIndex = 1;
            this.chartZone1.Text = "Kho 1";
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(3, 364);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(1188, 327);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Green;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSave.Location = new System.Drawing.Point(946, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 42;
            this.btnSave.Text = "Tìm";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Sản phẩm:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(342, 12);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 16);
            this.label7.TabIndex = 50;
            this.label7.Text = "Kho:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(342, 42);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 16);
            this.label8.TabIndex = 52;
            this.label8.Text = "Khu vực:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(342, 72);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 16);
            this.label9.TabIndex = 54;
            this.label9.Text = "Dãy:";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(113, 71);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(216, 22);
            this.txtUser.TabIndex = 56;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 74);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 16);
            this.label10.TabIndex = 55;
            this.label10.Text = "Người thực hiện:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(615, 45);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 16);
            this.label11.TabIndex = 58;
            this.label11.Text = "Từ ngày:";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd/MM/yyyy";
            this.dtpFromDate.Enabled = false;
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(682, 40);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(110, 22);
            this.dtpFromDate.TabIndex = 57;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(791, 45);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 16);
            this.label12.TabIndex = 60;
            this.label12.Text = "đến:";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd/MM/yyyy";
            this.dtpToDate.Enabled = false;
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(824, 40);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(110, 22);
            this.dtpToDate.TabIndex = 59;
            // 
            // rdbWarehouseVerifyDate
            // 
            this.rdbWarehouseVerifyDate.AutoSize = true;
            this.rdbWarehouseVerifyDate.Location = new System.Drawing.Point(682, 14);
            this.rdbWarehouseVerifyDate.Name = "rdbWarehouseVerifyDate";
            this.rdbWarehouseVerifyDate.Size = new System.Drawing.Size(107, 20);
            this.rdbWarehouseVerifyDate.TabIndex = 61;
            this.rdbWarehouseVerifyDate.TabStop = true;
            this.rdbWarehouseVerifyDate.Text = "Thủ kho nhập";
            this.rdbWarehouseVerifyDate.UseVisualStyleBackColor = true;
            this.rdbWarehouseVerifyDate.CheckedChanged += new System.EventHandler(this.rdbWarehouseVerifyDate_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(824, 13);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(103, 20);
            this.radioButton1.TabIndex = 62;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Xe nâng chất";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.rdbWarehouseVerifyDate_CheckedChanged);
            // 
            // btnDownload
            // 
            this.btnDownload.BackColor = System.Drawing.Color.Green;
            this.btnDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownload.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDownload.Location = new System.Drawing.Point(946, 36);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 30);
            this.btnDownload.TabIndex = 63;
            this.btnDownload.Text = "Export Excel";
            this.btnDownload.UseVisualStyleBackColor = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // txtLineName
            // 
            this.txtLineName.Location = new System.Drawing.Point(400, 71);
            this.txtLineName.Name = "txtLineName";
            this.txtLineName.Size = new System.Drawing.Size(216, 22);
            this.txtLineName.TabIndex = 66;
            // 
            // txtWarehouseName
            // 
            this.txtWarehouseName.Location = new System.Drawing.Point(400, 9);
            this.txtWarehouseName.Name = "txtWarehouseName";
            this.txtWarehouseName.Size = new System.Drawing.Size(216, 22);
            this.txtWarehouseName.TabIndex = 65;
            // 
            // txtZoneName
            // 
            this.txtZoneName.Location = new System.Drawing.Point(400, 40);
            this.txtZoneName.Name = "txtZoneName";
            this.txtZoneName.Size = new System.Drawing.Size(216, 22);
            this.txtZoneName.TabIndex = 64;
            // 
            // colZoneName3
            // 
            this.colZoneName3.DataPropertyName = "ZoneName";
            this.colZoneName3.HeaderText = "Khu vực";
            this.colZoneName3.Name = "colZoneName3";
            this.colZoneName3.ReadOnly = true;
            // 
            // colLineName3
            // 
            this.colLineName3.DataPropertyName = "LineName";
            this.colLineName3.HeaderText = "Dãy";
            this.colLineName3.Name = "colLineName3";
            this.colLineName3.ReadOnly = true;
            // 
            // colLocationCode3
            // 
            this.colLocationCode3.DataPropertyName = "LocationName";
            this.colLocationCode3.HeaderText = "Vị trí";
            this.colLocationCode3.Name = "colLocationCode3";
            this.colLocationCode3.ReadOnly = true;
            // 
            // colPalletCode
            // 
            this.colPalletCode.DataPropertyName = "PalletCode";
            this.colPalletCode.HeaderText = "Pallet";
            this.colPalletCode.Name = "colPalletCode";
            this.colPalletCode.ReadOnly = true;
            // 
            // colProductLot
            // 
            this.colProductLot.DataPropertyName = "ProductLot";
            this.colProductLot.HeaderText = "Số lô";
            this.colProductLot.Name = "colProductLot";
            this.colProductLot.ReadOnly = true;
            // 
            // colProductCode
            // 
            this.colProductCode.DataPropertyName = "ProductCode";
            this.colProductCode.HeaderText = "Mã SP";
            this.colProductCode.Name = "colProductCode";
            this.colProductCode.ReadOnly = true;
            // 
            // colProductName
            // 
            this.colProductName.DataPropertyName = "ProductName";
            this.colProductName.HeaderText = "Tên SP";
            this.colProductName.Name = "colProductName";
            this.colProductName.ReadOnly = true;
            this.colProductName.Width = 200;
            // 
            // colUOM
            // 
            this.colUOM.DataPropertyName = "UOM";
            this.colUOM.HeaderText = "ĐVT";
            this.colUOM.Name = "colUOM";
            this.colUOM.ReadOnly = true;
            // 
            // colPackSize
            // 
            this.colPackSize.DataPropertyName = "PackSize";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colPackSize.DefaultCellStyle = dataGridViewCellStyle12;
            this.colPackSize.HeaderText = "Quy cách";
            this.colPackSize.Name = "colPackSize";
            this.colPackSize.ReadOnly = true;
            // 
            // colCartonQuantity
            // 
            this.colCartonQuantity.DataPropertyName = "CartonQuantity";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colCartonQuantity.DefaultCellStyle = dataGridViewCellStyle13;
            this.colCartonQuantity.HeaderText = "Số thùng";
            this.colCartonQuantity.Name = "colCartonQuantity";
            this.colCartonQuantity.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colQuantity.DefaultCellStyle = dataGridViewCellStyle14;
            this.colQuantity.HeaderText = "Số lượng SP";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.ReadOnly = true;
            // 
            // colDescription3
            // 
            this.colDescription3.DataPropertyName = "Description";
            this.colDescription3.HeaderText = "Ghi chú";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.ReadOnly = true;
            this.colDescription3.Width = 200;
            // 
            // colManufacturingDate
            // 
            this.colManufacturingDate.DataPropertyName = "ManufacturingDate";
            this.colManufacturingDate.HeaderText = "Ngày sản xuất";
            this.colManufacturingDate.Name = "colManufacturingDate";
            this.colManufacturingDate.ReadOnly = true;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.DataPropertyName = "ExpiryDate";
            this.colExpiryDate.HeaderText = "Ngày hết hạn";
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.ReadOnly = true;
            // 
            // colStrDriverReceived
            // 
            this.colStrDriverReceived.DataPropertyName = "StrDriverReceived";
            this.colStrDriverReceived.HeaderText = "Xe nâng chuyển";
            this.colStrDriverReceived.Name = "colStrDriverReceived";
            this.colStrDriverReceived.ReadOnly = true;
            this.colStrDriverReceived.Width = 150;
            // 
            // colStrDriverReceivedDate
            // 
            this.colStrDriverReceivedDate.DataPropertyName = "StrDriverReceivedDate";
            this.colStrDriverReceivedDate.HeaderText = "Ngày chuyển kho";
            this.colStrDriverReceivedDate.Name = "colStrDriverReceivedDate";
            this.colStrDriverReceivedDate.ReadOnly = true;
            this.colStrDriverReceivedDate.Width = 120;
            // 
            // colStrWarehouseKeeper
            // 
            this.colStrWarehouseKeeper.DataPropertyName = "StrWarehouseKeeper";
            this.colStrWarehouseKeeper.HeaderText = "Thủ kho";
            this.colStrWarehouseKeeper.Name = "colStrWarehouseKeeper";
            this.colStrWarehouseKeeper.ReadOnly = true;
            this.colStrWarehouseKeeper.Width = 150;
            // 
            // colStrWarehouseVerifyDate
            // 
            this.colStrWarehouseVerifyDate.DataPropertyName = "StrWarehouseVerifyDate";
            this.colStrWarehouseVerifyDate.HeaderText = "Ngày thủ kho xác nhận";
            this.colStrWarehouseVerifyDate.Name = "colStrWarehouseVerifyDate";
            this.colStrWarehouseVerifyDate.ReadOnly = true;
            this.colStrWarehouseVerifyDate.Width = 120;
            // 
            // colWarehouseVerifyNote
            // 
            this.colWarehouseVerifyNote.DataPropertyName = "WarehouseVerifyNote";
            this.colWarehouseVerifyNote.HeaderText = "Ghi chú kiểm kê";
            this.colWarehouseVerifyNote.Name = "colWarehouseVerifyNote";
            this.colWarehouseVerifyNote.ReadOnly = true;
            // 
            // colStrDriver
            // 
            this.colStrDriver.DataPropertyName = "StrDriver";
            this.colStrDriver.HeaderText = "Xe nâng lên kệ";
            this.colStrDriver.Name = "colStrDriver";
            this.colStrDriver.ReadOnly = true;
            this.colStrDriver.Width = 150;
            // 
            // colStrLocationPutDate
            // 
            this.colStrLocationPutDate.DataPropertyName = "StrLocationPutDate";
            this.colStrLocationPutDate.HeaderText = "Ngày lên kệ";
            this.colStrLocationPutDate.Name = "colStrLocationPutDate";
            this.colStrLocationPutDate.ReadOnly = true;
            this.colStrLocationPutDate.Width = 120;
            // 
            // colLastLocationName
            // 
            this.colLastLocationName.DataPropertyName = "LastLocationName";
            this.colLastLocationName.HeaderText = "Vị trí trước đó";
            this.colLastLocationName.Name = "colLastLocationName";
            this.colLastLocationName.ReadOnly = true;
            // 
            // ReportLocationView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 780);
            this.Controls.Add(this.txtLineName);
            this.Controls.Add(this.txtWarehouseName);
            this.Controls.Add(this.txtZoneName);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.rdbWarehouseVerifyDate);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtProduct);
            this.Controls.Add(this.txtProductLot);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label12);
            this.Name = "ReportLocationView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsTopCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallet)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgZone)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgLine)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgLocation)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallet)).EndInit();
            this.tabChart.ResumeLayout(false);
            this.tabChart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTopCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartZone2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartZone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsZone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dtgZone;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.BindingSource bdsLine;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProductLot;
        private System.Windows.Forms.TextBox txtProduct;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTotal1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCheckedLocation1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmptyLocation1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription1;
        private System.Windows.Forms.BindingSource bdsLocation;
        private System.Windows.Forms.DataGridView dtgLine;
        private System.Windows.Forms.DataGridView dtgLocation;
        private System.Windows.Forms.TabPage tabChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartZone1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartZone2;
        private System.Windows.Forms.DataGridView dtgZoneList;
        private System.Windows.Forms.BindingSource bdsZoneChart;
        private System.Windows.Forms.DataGridView dtgTopCustomer;
        private System.Windows.Forms.BindingSource bdsTopCustomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTotal2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCheckedLocation2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEmptyLocation2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dtgPallet;
        private System.Windows.Forms.BindingSource bdsPallet;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationCode4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductionPlanNote;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.RadioButton rdbWarehouseVerifyDate;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.TextBox txtLineName;
        private System.Windows.Forms.TextBox txtWarehouseName;
        private System.Windows.Forms.TextBox txtZoneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneName3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineName3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationCode3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colManufacturingDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrDriverReceived;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrDriverReceivedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrWarehouseKeeper;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrWarehouseVerifyDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWarehouseVerifyNote;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrDriver;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrLocationPutDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLastLocationName;
    }
}