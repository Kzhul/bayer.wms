﻿using Bayer.WMS.Objs;

namespace Bayer.WMS.Inv.Views
{
    partial class ZoneMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsZoneLine = new System.Windows.Forms.BindingSource(this.components);
            this.bdsZoneLocation = new System.Windows.Forms.BindingSource(this.components);
            this.bdsZoneCategory = new System.Windows.Forms.BindingSource(this.components);
            this.bdsZoneTemperature = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbZoneCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.cmbWarehouseID = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.splitAll = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtgZoneLine = new System.Windows.Forms.DataGridView();
            this.colZoneLine_LineCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_LineName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_Type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colZoneLine_Length = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_Level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_Order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_Status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colZoneLine_Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitAll)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneLine)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(306, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 16);
            this.label4.TabIndex = 45;
            this.label4.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 16);
            this.label3.TabIndex = 43;
            this.label3.Text = "Kho:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(624, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 16);
            this.label7.TabIndex = 40;
            this.label7.Text = "*";
            // 
            // cmbZoneCode
            // 
            this.cmbZoneCode.Columns = null;
            this.cmbZoneCode.DropDownHeight = 1;
            this.cmbZoneCode.DropDownWidth = 500;
            this.cmbZoneCode.FormattingEnabled = true;
            this.cmbZoneCode.IntegralHeight = false;
            this.cmbZoneCode.Location = new System.Drawing.Point(418, 12);
            this.cmbZoneCode.MaxLength = 3;
            this.cmbZoneCode.Name = "cmbZoneCode";
            this.cmbZoneCode.PageSize = 0;
            this.cmbZoneCode.PresenterInfo = null;
            this.cmbZoneCode.Size = new System.Drawing.Size(200, 24);
            this.cmbZoneCode.Source = null;
            this.cmbZoneCode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(333, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 16);
            this.label1.TabIndex = 37;
            this.label1.Text = "Mã khu vực:";
            // 
            // cmbWarehouseID
            // 
            this.cmbWarehouseID.Columns = null;
            this.cmbWarehouseID.DropDownHeight = 1;
            this.cmbWarehouseID.DropDownWidth = 500;
            this.cmbWarehouseID.FormattingEnabled = true;
            this.cmbWarehouseID.IntegralHeight = false;
            this.cmbWarehouseID.Location = new System.Drawing.Point(100, 12);
            this.cmbWarehouseID.MaxLength = 255;
            this.cmbWarehouseID.Name = "cmbWarehouseID";
            this.cmbWarehouseID.PageSize = 0;
            this.cmbWarehouseID.PresenterInfo = null;
            this.cmbWarehouseID.Size = new System.Drawing.Size(200, 24);
            this.cmbWarehouseID.Source = null;
            this.cmbWarehouseID.TabIndex = 4;
            // 
            // splitAll
            // 
            this.splitAll.Location = new System.Drawing.Point(12, 42);
            this.splitAll.Name = "splitAll";
            // 
            // splitAll.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer1);
            // 
            // splitAll.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitAll.Size = new System.Drawing.Size(1012, 529);
            this.splitAll.SplitterDistance = 502;
            this.splitAll.TabIndex = 46;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(502, 529);
            this.splitContainer1.SplitterDistance = 245;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.splitContainer2.Size = new System.Drawing.Size(506, 529);
            this.splitContainer2.SplitterDistance = 243;
            this.splitContainer2.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtgZoneLine);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(502, 245);
            this.panel1.TabIndex = 0;
            // 
            // dtgZoneLine
            // 
            this.dtgZoneLine.AllowUserToOrderColumns = true;
            this.dtgZoneLine.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgZoneLine.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgZoneLine.AutoGenerateColumns = false;
            this.dtgZoneLine.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgZoneLine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgZoneLine.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneLine_LineCode,
            this.colZoneLine_LineName,
            this.colZoneLine_Type,
            this.colZoneLine_Length,
            this.colZoneLine_Level,
            this.colZoneLine_Order,
            this.colZoneLine_Status,
            this.colZoneLine_Description});
            this.dtgZoneLine.DataSource = this.bdsZoneLine;
            this.dtgZoneLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgZoneLine.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgZoneLine.GridColor = System.Drawing.SystemColors.Control;
            this.dtgZoneLine.Location = new System.Drawing.Point(0, 0);
            this.dtgZoneLine.Name = "dtgZoneLine";
            this.dtgZoneLine.Size = new System.Drawing.Size(502, 245);
            this.dtgZoneLine.TabIndex = 3;
            // 
            // colZoneLine_LineCode
            // 
            this.colZoneLine_LineCode.DataPropertyName = "LineCode";
            this.colZoneLine_LineCode.HeaderText = "Mã";
            this.colZoneLine_LineCode.Name = "colZoneLine_LineCode";
            // 
            // colZoneLine_LineName
            // 
            this.colZoneLine_LineName.DataPropertyName = "LineName";
            this.colZoneLine_LineName.HeaderText = "Tên";
            this.colZoneLine_LineName.Name = "colZoneLine_LineName";
            // 
            // colZoneLine_Type
            // 
            this.colZoneLine_Type.DataPropertyName = "Type";
            this.colZoneLine_Type.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colZoneLine_Type.HeaderText = "Loại";
            this.colZoneLine_Type.Name = "colZoneLine_Type";
            this.colZoneLine_Type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colZoneLine_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // colZoneLine_Length
            // 
            this.colZoneLine_Length.DataPropertyName = "Length";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            this.colZoneLine_Length.DefaultCellStyle = dataGridViewCellStyle2;
            this.colZoneLine_Length.HeaderText = "Dài";
            this.colZoneLine_Length.Name = "colZoneLine_Length";
            this.colZoneLine_Length.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // colZoneLine_Level
            // 
            this.colZoneLine_Level.DataPropertyName = "Level";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            this.colZoneLine_Level.DefaultCellStyle = dataGridViewCellStyle3;
            this.colZoneLine_Level.HeaderText = "Cao";
            this.colZoneLine_Level.Name = "colZoneLine_Level";
            // 
            // colZoneLine_Order
            // 
            this.colZoneLine_Order.DataPropertyName = "Order";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "N0";
            this.colZoneLine_Order.DefaultCellStyle = dataGridViewCellStyle4;
            this.colZoneLine_Order.HeaderText = "Thứ tự";
            this.colZoneLine_Order.Name = "colZoneLine_Order";
            // 
            // colZoneLine_Status
            // 
            this.colZoneLine_Status.DataPropertyName = "Status";
            this.colZoneLine_Status.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colZoneLine_Status.HeaderText = "Trạng thái";
            this.colZoneLine_Status.Name = "colZoneLine_Status";
            this.colZoneLine_Status.Width = 150;
            // 
            // colZoneLine_Description
            // 
            this.colZoneLine_Description.DataPropertyName = "Description";
            this.colZoneLine_Description.HeaderText = "Ghi chú";
            this.colZoneLine_Description.Name = "colZoneLine_Description";
            this.colZoneLine_Description.Width = 300;
            // 
            // ZoneMaintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 583);
            this.Controls.Add(this.splitAll);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbZoneCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbWarehouseID);
            this.Name = "ZoneMaintView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneTemperature)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitAll)).EndInit();
            this.splitAll.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneLine)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label7;
        private CustomControls.MultiColumnComboBox cmbZoneCode;
        private System.Windows.Forms.Label label1;
        private CustomControls.MultiColumnComboBox cmbWarehouseID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.BindingSource bdsZoneLine;
        private System.Windows.Forms.BindingSource bdsZoneLocation;
        private System.Windows.Forms.BindingSource bdsZoneCategory;
        private System.Windows.Forms.BindingSource bdsZoneTemperature;
        private System.Windows.Forms.SplitContainer splitAll;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dtgZoneLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_LineCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_LineName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colZoneLine_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_Length;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_Level;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_Order;
        private System.Windows.Forms.DataGridViewComboBoxColumn colZoneLine_Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_Description;
    }
}