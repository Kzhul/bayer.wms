﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;
using System.IO.Ports;

namespace Bayer.WMS.Inv.Views
{
    public partial class BarcodeTrackingView : BaseForm, IBarcodeTrackingView
    {
        private SerialPort _serialPort;
        public BarcodeTrackingView()
        {
            InitializeComponent();

            SetDoubleBuffered(dtgInfo);
            SetDoubleBuffered(dtgBarCode);
        }

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdbAll.Checked)
                {
                    await (_presenter as IBarcodeTrackingPresenter).Search(txtCode.Text);
                }
                else
                {
                    string type = "E";
                    //P,C,K,E,L
                    if (rdbPallet.Checked)
                    {
                        type = "P";
                    }
                    else if (rdbCarton.Checked)
                    {
                        type = "C";
                    }
                    else if (rdbProductLot.Checked)
                    {
                        type = "L";
                    }
                    else if (rdbProductCode.Checked)
                    {
                        type = "M";
                    }

                    await (_presenter as IBarcodeTrackingPresenter).Search(txtCode.Text, type);
                }

            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error, Name);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error, Name);
            }
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);
        }

        private DataTable _barcodeTrackingDetails;

        public DataTable BarcodeTrackingDetails
        {
            get { return _barcodeTrackingDetails; }
            set
            {
                _barcodeTrackingDetails = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsBarCode.DataSource = _barcodeTrackingDetails;
                    });
                }
                else
                {
                    bdsBarCode.DataSource = _barcodeTrackingDetails;
                }
            }
        }

        private DataTable _barcodeTrackingInfo;

        public DataTable BarcodeTrackingInfo
        {
            get { return _barcodeTrackingInfo; }
            set
            {
                _barcodeTrackingInfo = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _barcodeTrackingInfo;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _barcodeTrackingInfo;
                }
            }
        }

        private void BarcodeTrackingView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_serialPort != null && _serialPort.IsOpen)
                _serialPort.Close();
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string data = _serialPort.ReadExisting();
            txtCode.Text = data;
        }

        public void CreateCOM(string portName, int baudRate, string parity, int dataBits, string stopBits)
        {
            Parity p = Parity.None;
            switch (parity)
            {
                case "Even":
                    p = Parity.Even;
                    break;
                case "Mark":
                    p = Parity.Mark;
                    break;
                case "Odd":
                    p = Parity.Odd;
                    break;
                case "Space":
                    p = Parity.Space;
                    break;
                default:
                    break;
            }

            StopBits sb = StopBits.None;
            switch (stopBits)
            {
                case "1":
                    sb = StopBits.One;
                    break;
                case "2":
                    sb = StopBits.Two;
                    break;
                case "1.5":
                    sb = StopBits.OnePointFive;
                    break;
                default:
                    break;
            }

            _serialPort = new SerialPort(portName, baudRate, p, dataBits, sb);
            _serialPort.ReadTimeout = 1000;
            _serialPort.WriteTimeout = 1000;
            _serialPort.DataReceived += _serialPort_DataReceived;
            _serialPort.Open();
        }
    }
}
