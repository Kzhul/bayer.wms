﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Reflection;
using Microsoft.Practices.Unity;
using System.IO;
using System.Collections;
using System.Diagnostics;
using Microsoft.Office.Interop.Excel;
using System.Threading;
using System.Runtime.InteropServices;

namespace Bayer.WMS.Inv.Views
{
    public partial class ZoneMaintView : BaseForm, IZoneMaintView
    {
        public ZoneMaintView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgZoneCategory);
            SetDoubleBuffered(dtgZoneLine);
        }

        private void btnZoneLine_Insert_Click(object sender, EventArgs e)
        {
            bdsZoneLine.AddNew();
        }

        private void btnZoneLine_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtgZoneLine.SelectedRows)
                {
                    var zoneLine = bdsZoneLine[row.Index] as ZoneLine;

                    bdsZoneLine.Remove(zoneLine);
                    (_presenter as IZoneMaintPresenter).DeleteZoneLine(zoneLine);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        private void btnZoneLocation_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtgZoneLocation.SelectedRows)
                {
                    var zoneLocation = bdsZoneLocation[row.Index] as ZoneLocation;

                    bdsZoneLocation.Remove(zoneLocation);
                    (_presenter as IZoneMaintPresenter).DeleteZoneLocation(zoneLocation);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        private void dtgZoneLine_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex == -1) return;
            //if (e.ColumnIndex == colZoneLine_Type.Index)
            //{
            //    string value = dtgZoneLine[e.ColumnIndex, e.RowIndex].Value == null ? String.Empty : dtgZoneLine[e.ColumnIndex, e.RowIndex].Value.ToString();
            //    if (value == ZoneLine.type.Line)
            //    {
            //        dtgZoneLine[colZoneLine_Length.Index, e.RowIndex].ReadOnly = false;
            //        dtgZoneLine[colZoneLine_Level.Index, e.RowIndex].ReadOnly = false;
            //    }
            //    else
            //    {
            //        dtgZoneLine[colZoneLine_Length.Index, e.RowIndex].ReadOnly = true;
            //        dtgZoneLine[colZoneLine_Level.Index, e.RowIndex].ReadOnly = true;
            //    }
            //}
        }

        private void dtgZoneLine_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dtgZoneLine.CurrentCell.ColumnIndex == colZoneLine_Type.Index)
            {
                dtgZoneLine.EndEdit();
                if (dtgZoneLine.IsCurrentCellDirty)
                {
                    dtgZoneLine.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
        }

        private void dtgZoneCategory_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dtgZoneCategory.EndEdit();
            if (dtgZoneCategory.IsCurrentCellDirty)
            {
                dtgZoneCategory.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dtgZoneTemperature_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dtgZoneTemperature.EndEdit();
            if (dtgZoneTemperature.IsCurrentCellDirty)
            {
                dtgZoneTemperature.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        public override Task Save()
        {
            dtgZoneLine.EndEdit();
            dtgZoneLocation.EndEdit();
            dtgZoneCategory.EndEdit();
            dtgZoneTemperature.EndEdit();

            return base.Save();
        }

        public override void InitializeComboBox()
        {
            cmbStatus.ValueMember = Product.status.ValueMember;
            cmbStatus.DisplayMember = Product.status.DisplayMember;
            cmbStatus.DataSource = Product.status.Get();

            cmbZoneCode.DropDownWidth = 700;
            cmbZoneCode.PageSize = 20;
            cmbZoneCode.ValueMember = "ZoneCode";
            cmbZoneCode.DisplayMember = "ZoneCode";
            cmbZoneCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("ZoneCode", "Mã kho", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("WarehouseDescription", "Kho", 200),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbZoneCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadZones", "OnZoneCodeSelectChange");

            cmbWarehouseID.DropDownWidth = 700;
            cmbWarehouseID.PageSize = 20;
            cmbWarehouseID.ValueMember = "WarehouseID";
            cmbWarehouseID.DisplayMember = "WarehouseCode - Description";
            cmbWarehouseID.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("WarehouseCode", "Mã kho", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbWarehouseID.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadWarehouses", "OnWarehouseCodeSelectChange");

            colZoneLine_Type.ValueMember = ZoneLine.type.ValueMember;
            colZoneLine_Type.DisplayMember = ZoneLine.type.DisplayMember;
            colZoneLine_Type.DataSource = ZoneLine.type.Get();

            colZoneLine_Status.ValueMember = ZoneLine.status.ValueMember;
            colZoneLine_Status.DisplayMember = ZoneLine.status.DisplayMember;
            colZoneLine_Status.DataSource = ZoneLine.status.Get();

            colZoneLocation_Status.ValueMember = ZoneLocation.status.ValueMember;
            colZoneLocation_Status.DisplayMember = ZoneLocation.status.DisplayMember;
            colZoneLocation_Status.DataSource = ZoneLocation.status.Get();
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var zoneMaintPresenter = _presenter as IZoneMaintPresenter;

                await Task.WhenAll(zoneMaintPresenter.LoadZones(), zoneMaintPresenter.LoadWarehouses());

                if (!isRefresh)
                    zoneMaintPresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public System.Data.DataTable Zones { set => cmbZoneCode.Source = value; }

        public System.Data.DataTable Warehouses { set => cmbWarehouseID.Source = value; }

        private Warehouse _warehouse;

        public Warehouse Warehouse { get => _warehouse; set => _warehouse = value; }

        private IList<ZoneLine> _zoneLines;

        public IList<ZoneLine> ZoneLines
        {
            get => _zoneLines;
            set
            {
                _zoneLines = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsZoneLine.DataSource = _zoneLines;
                    });
                }
                else
                {
                    bdsZoneLine.DataSource = _zoneLines;
                }
            }
        }

        private IList<ZoneLine> _deletedZoneLines;

        public IList<ZoneLine> DeletedZoneLines { get => _deletedZoneLines; set => _deletedZoneLines = value; }

        private IList<ZoneLocation> _zoneLocations;

        public IList<ZoneLocation> ZoneLocations
        {
            get => _zoneLocations;
            set
            {
                _zoneLocations = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsZoneLocation.DataSource = value;
                    });
                }
                else
                {
                    bdsZoneLocation.DataSource = value;
                }
            }
        }

        private IList<ZoneLocation> _deletedZoneLocations;

        public IList<ZoneLocation> DeletedZoneLocations { get => _deletedZoneLocations; set => _deletedZoneLocations = value; }

        private IList<ZoneCategory> _zoneCategories;

        public IList<ZoneCategory> ZoneCategories
        {
            get => _zoneCategories;
            set
            {
                _zoneCategories = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsZoneCategory.DataSource = value;
                    });
                }
                else
                {
                    bdsZoneCategory.DataSource = value;
                }
            }
        }

        private IList<ZoneTemperature> _zoneTemperatures;

        public IList<ZoneTemperature> ZoneTemperatures
        {
            get => _zoneTemperatures;
            set
            {
                _zoneTemperatures = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsZoneTemperature.DataSource = value;
                    });
                }
                else
                {
                    bdsZoneTemperature.DataSource = value;
                }
            }
        }

        private Zone _zone;

        public Zone Zone
        {
            get { return _zone; }
            set
            {
                _zone = value;

                cmbZoneCode.DataBindings.Clear();
                txtDescription.DataBindings.Clear();
                cmbWarehouseID.DataBindings.Clear();
                cmbStatus.DataBindings.Clear();
                txtZoneName.DataBindings.Clear();

                cmbZoneCode.DataBindings.Add("Text", Zone, "ZoneCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtZoneName.DataBindings.Add("Text", Zone, "ZoneName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", Zone, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbWarehouseID.DataBindings.Add("SelectedValue", Zone, "WarehouseID", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbStatus.DataBindings.Add("SelectedValue", Zone, "Status", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    txtZoneName.ReadOnly = true;
                    txtDescription.ReadOnly = true;
                    txtWarehouseID_ReadOnly.BringToFront();
                    txtStatus_ReadOnly.BringToFront();

                    btnZoneLine_Insert.Enabled = false;
                    btnZoneLine_Delete.Enabled = false;

                    btnZoneLocation_Delete.Enabled = false;

                    dtgZoneLine.ReadOnly = true;
                    dtgZoneLocation.ReadOnly = true;
                    dtgZoneCategory.ReadOnly = true;
                    dtgZoneTemperature.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void btnExportQRCode_Click_0(object sender, EventArgs e)
        {
            //Create directory to save file
            //1 for Vertical
            //2 for Horizontal
            //Foreach Line
            //Foreach 2 Zone Location
            string seletedLineCode = string.Empty;
            seletedLineCode = dtgZoneLine.SelectedCells[0].OwningRow.Cells["colZoneLine_LineCode"].Value.ToString();

            string savePath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + cmbZoneCode.SelectedValue.ToString() + "\\" + seletedLineCode;
            string savePathVertial = savePath + "\\" + "Vertical";
            string savePathHorizontal = savePath + "\\" + "Horizontal";
            Utility.CreateDirectory(savePathVertial);
            Utility.CreateDirectory(savePathHorizontal);
            Process.Start(savePathVertial);
            Process.Start(savePathHorizontal);

            ZoneLine zl = this.ZoneLines.FirstOrDefault(a => a.LineCode == seletedLineCode);
            //foreach (ZoneLine zl in this.ZoneLines)
            {
                //Lấy ra danh sách từ 1 tới 27 hoặc 29
                var listLocationInLine = (from a in this.ZoneLocations where a.LineCode == zl.LineCode select a.LengthID).Distinct().OrderBy(a => a).ToList().GetEnumerator();

                //di chuyển từng vị trí từ 1 tới 27
                while (listLocationInLine.MoveNext())
                {
                    {
                        var current = listLocationInLine.Current;
                        if (listLocationInLine.MoveNext())
                        {
                            var afterCurrent = listLocationInLine.Current;

                            //here you can access "current" & "afterCurrent"
                            List<ZoneLocation> listLocationCurrent = this.ZoneLocations.Where(a =>
                                                            a.LineCode == zl.LineCode
                                                            && a.LengthID == current
                                                        ).OrderBy(a => a.LevelID).ToList();
                            List<ZoneLocation> listLocationAfterCurrent = this.ZoneLocations.Where(a =>
                                                            a.LineCode == zl.LineCode
                                                            && a.LengthID == afterCurrent
                                                        ).OrderBy(a => a.LevelID).ToList();

                            List<Bitmap> listQRCodeCurrent = new List<Bitmap>();
                            foreach (ZoneLocation zlQRCurrent in listLocationCurrent)
                            {
                                listQRCodeCurrent.Add(QRCoder.Utility.RenderQrCode(zlQRCurrent.LocationCode, zlQRCurrent.LocationName));
                            }

                            List<Bitmap> listQRCodeAfterCurrent = new List<Bitmap>();
                            foreach (ZoneLocation zlQRCurrent in listLocationAfterCurrent)
                            {
                                listQRCodeAfterCurrent.Add(QRCoder.Utility.RenderQrCode(zlQRCurrent.LocationCode, zlQRCurrent.LocationName));
                            }

                            string bigImageVerticalName = savePathVertial + "\\" + listLocationCurrent[0].LocationName + ".png";
                            string bigImageHorizontalName = savePathHorizontal + "\\" + listLocationCurrent[0].LocationName + ".png";
                            QRCoder.Utility.createVerticalBigImage(listQRCodeCurrent, listQRCodeAfterCurrent, bigImageVerticalName);
                            QRCoder.Utility.createHorizontalBigImage(listQRCodeCurrent, listQRCodeAfterCurrent, bigImageHorizontalName);
                        }
                        else
                        {
                            //here you can only access "current", there is no item after this.
                            List<ZoneLocation> listLocationCurrent = this.ZoneLocations.Where(a =>
                                                            a.LineCode == zl.LineCode
                                                            && a.LengthID == current
                                                        ).OrderBy(a => a.LevelID).ToList();

                            List<Bitmap> listQRCodeCurrent = new List<Bitmap>();
                            foreach (ZoneLocation zlQRCurrent in listLocationCurrent)
                            {
                                listQRCodeCurrent.Add(QRCoder.Utility.RenderQrCode(zlQRCurrent.LocationCode, zlQRCurrent.LocationName));
                            }

                            string bigImageVerticalName = savePathVertial + "\\" + listLocationCurrent[0].LocationName + ".png";
                            string bigImageHorizontalName = savePathHorizontal + "\\" + listLocationCurrent[0].LocationName + ".png";
                            QRCoder.Utility.createVerticalBigImage(listQRCodeCurrent, null, bigImageVerticalName);
                            QRCoder.Utility.createHorizontalBigImage(listQRCodeCurrent, null, bigImageHorizontalName);
                        }
                    }
                }
            }
        }

        string savePathHorizontal = string.Empty;
        private void btnExportQRCode_Click(object sender, EventArgs e)
        {
            //Create directory to save file
            //1 for Vertical
            //2 for Horizontal
            //Foreach Line
            //Foreach 2 Zone Location
            string seletedLineCode = string.Empty;
            seletedLineCode = dtgZoneLine.SelectedCells[0].OwningRow.Cells["colZoneLine_LineCode"].Value.ToString();

            string savePath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\_ZoneLocationQRCode\\" + cmbZoneCode.SelectedValue.ToString() + "\\" + seletedLineCode;
            //string savePathVertial = savePath + "\\" + "Vertical";
            savePathHorizontal = savePath + "\\" + "Horizontal";
            //Utility.CreateDirectory(savePathVertial);
            Utility.CreateDirectory(savePathHorizontal);
            //Process.Start(savePathVertial);
            Process.Start(savePathHorizontal);

            ZoneLine zl = this.ZoneLines.FirstOrDefault(a => a.LineCode == seletedLineCode);

            #region Create Excel here
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\LocationPrint.xlsx";
            workbook = excelApp.Workbooks.Open(templatePath);
            sheetDetail = (Worksheet)workbook.ActiveSheet;
            #endregion

            //foreach (ZoneLine zl in this.ZoneLines)
            {
                //Lấy ra danh sách từ 1 tới 27 hoặc 29
                var listLocationInLine = (from a in this.ZoneLocations where a.LineCode == zl.LineCode select a.LengthID).Distinct().OrderBy(a => a).ToList().GetEnumerator();

                //di chuyển từng vị trí từ 1 tới 27
                while (listLocationInLine.MoveNext())
                {
                    var current = listLocationInLine.Current;
                    {
                        //here you can only access "current", there is no item after this.
                        List<ZoneLocation> listLocationCurrent = this.ZoneLocations.Where(a =>
                                                        a.LineCode == zl.LineCode
                                                        && a.LengthID == current
                                                    ).OrderBy(a => a.LevelID).ToList();


                        int maxLevelID = listLocationCurrent.Max(a => a.LevelID);
                        string lastLocationBarcode = string.Empty;
                        List<Bitmap> listQRCodeCurrent = new List<Bitmap>();
                        foreach (ZoneLocation zlQRCurrent in listLocationCurrent)
                        {
                            listQRCodeCurrent.Add(QRCoder.Utility.RenderQrCodeNoText(zlQRCurrent.LocationCode));

                            string locationBarCode = "A1";
                            if (zlQRCurrent.LevelID == 1)
                            {
                                locationBarCode = "A1";
                                sheetDetail.Cells[2, 1] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 2)
                            {
                                locationBarCode = "C3";
                                sheetDetail.Cells[2, 3] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 3)
                            {
                                locationBarCode = "E1";
                                sheetDetail.Cells[2, 5] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 4)
                            {
                                locationBarCode = "G3";
                                sheetDetail.Cells[2, 7] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 5)
                            {
                                locationBarCode = "I1";
                                sheetDetail.Cells[2, 9] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 6)
                            {
                                locationBarCode = "K3";
                                sheetDetail.Cells[2, 11] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 7)
                            {
                                locationBarCode = "M1";
                                sheetDetail.Cells[2, 13] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 8)
                            {
                                locationBarCode = "O3";
                                sheetDetail.Cells[2, 15] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 9)
                            {
                                locationBarCode = "Q1";
                                sheetDetail.Cells[2, 17] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 10)
                            {
                                locationBarCode = "S3";
                                sheetDetail.Cells[2, 19] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 11)
                            {
                                locationBarCode = "U1";
                                sheetDetail.Cells[2, 21] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 12)
                            {
                                locationBarCode = "W3";
                                sheetDetail.Cells[2, 23] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 13)
                            {
                                locationBarCode = "Y1";
                                sheetDetail.Cells[2, 25] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 14)
                            {
                                locationBarCode = "AA3";
                                sheetDetail.Cells[2, 27] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 15)
                            {
                                locationBarCode = "AC1";
                                sheetDetail.Cells[2, 29] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 16)
                            {
                                locationBarCode = "AE3";
                                sheetDetail.Cells[2, 31] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 17)
                            {
                                locationBarCode = "AG1";
                                sheetDetail.Cells[2, 33] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 18)
                            {
                                locationBarCode = "AI3";
                                sheetDetail.Cells[2, 35] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 19)
                            {
                                locationBarCode = "AK1";
                                sheetDetail.Cells[2, 37] = zlQRCurrent.LocationName;
                            }
                            else if (zlQRCurrent.LevelID == 20)
                            {
                                locationBarCode = "AM3";
                                sheetDetail.Cells[2, 39] = zlQRCurrent.LocationName;
                            }
                            CopyBarcodeToExcelCell(locationBarCode, sheetDetail, zlQRCurrent.LocationCode);
                            lastLocationBarcode = locationBarCode;
                        }

                        #region Input to Excel here
                        try
                        {
                            #region Export range to picture
                            lastLocationBarcode = lastLocationBarcode.Substring(0, lastLocationBarcode.Length - 1);
                            lastLocationBarcode = lastLocationBarcode + "3";
                            Microsoft.Office.Interop.Excel.Range r = sheetDetail.get_Range("A1", lastLocationBarcode);
                            r.CopyPicture(Microsoft.Office.Interop.Excel.XlPictureAppearance.xlScreen,
                                           Microsoft.Office.Interop.Excel.XlCopyPictureFormat.xlBitmap);

                            if (Clipboard.GetDataObject() != null)
                            {
                                IDataObject data = Clipboard.GetDataObject();

                                if (data.GetDataPresent(DataFormats.Bitmap))
                                {
                                    Image image = (Image)data.GetData(DataFormats.Bitmap, true);
                                    string bigImageVerticalName = savePathHorizontal + "\\" + listLocationCurrent[0].LocationName + ".png";
                                    image.Save(bigImageVerticalName, System.Drawing.Imaging.ImageFormat.Jpeg);
                                }
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                            throw new WrappedException(Messages.Error_Common);
                        }
                        finally
                        {

                        }
                        #endregion

                        #region After Save, clean the template for next LenghID
                        foreach (Microsoft.Office.Interop.Excel.Shape sh in sheetDetail.Shapes)
                        {
                            sh.Delete();
                        }
                        for (int i = 1; i <= 40; i++)
                        {
                            sheetDetail.Cells[2, i] = string.Empty;
                        }
                        #endregion
                    }
                }
            }

            //After create all location image, merge to have A4
            mergeTwoImage();

            #region Clean Excel Object
            try
            {
                foreach (Process proc in System.Diagnostics.Process.GetProcessesByName("EXCEL"))
                {
                    proc.Kill();
                }

                workbook.Close();
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);

                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();


            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            #endregion
        }

        private void CopyBarcodeToExcelCell(string excelCell, _Worksheet sheetDetail, string locationCode)
        {
            object barcodeCell = excelCell;
            Microsoft.Office.Interop.Excel.Range rangeBitmap = sheetDetail.get_Range(barcodeCell, barcodeCell);
            Bitmap img = QRCoder.Utility.RenderQrCodeNoText(locationCode);
            Bitmap resizedImg = new Bitmap(200, 200);

            double ratioX = (double)resizedImg.Width / (double)img.Width;
            double ratioY = (double)resizedImg.Height / (double)img.Height;
            double ratio = ratioX < ratioY ? ratioX : ratioY;

            int newHeight = Convert.ToInt32(img.Height * ratio);
            int newWidth = Convert.ToInt32(img.Width * ratio);

            using (Graphics g = Graphics.FromImage(resizedImg))
            {
                g.DrawImage(img, 0, 0, newWidth, newHeight);
            }
            Clipboard.SetDataObject(resizedImg);
            //Paste the barcode image
            rangeBitmap.Select();
            //Interop params
            object oMissing = System.Reflection.Missing.Value;
            sheetDetail.Paste(oMissing, oMissing);
        }

        private void mergeTwoImage()
        {
            try
            {
                var files = Directory.GetFiles(savePathHorizontal).ToList().GetEnumerator();
                Utility.CreateDirectory(savePathHorizontal + "\\A4Full\\");
                while (files.MoveNext())
                {
                    var current = files.Current;
                    if (files.MoveNext())
                    {
                        var afterCurrent = files.Current;
                        Image frame;
                        try
                        {
                            frame = Image.FromFile(current);
                        }
                        catch (Exception ex)
                        {
                            frame = null;
                            Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                        }

                        Image nextframe;
                        try
                        {
                            nextframe = Image.FromFile(afterCurrent);
                        }
                        catch (Exception ex)
                        {
                            nextframe = null;
                            Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                        }

                        FileInfo f = new FileInfo(current);
                        Bitmap bigImage = new Bitmap(frame.Width, frame.Height * 2, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
                        using (Graphics graph = Graphics.FromImage(bigImage))
                        {
                            System.Drawing.Rectangle ImageSize = new System.Drawing.Rectangle(0, 0, frame.Width, frame.Height * 2);
                            graph.FillRectangle(Brushes.White, ImageSize);
                        }

                        using (Graphics gr = Graphics.FromImage(bigImage))
                        {
                            gr.DrawImage(frame, new System.Drawing.Rectangle(0, 0, frame.Width, frame.Height));
                            gr.DrawImage(nextframe, new System.Drawing.Rectangle(0, frame.Height + 1, nextframe.Width, nextframe.Height));
                        }

                        string bigImageVerticalName = savePathHorizontal + "\\A4Full\\" + f.Name;
                        bigImage.Save(bigImageVerticalName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    else
                    {
                        Image frame;
                        try
                        {
                            frame = Image.FromFile(current);
                        }
                        catch (Exception ex)
                        {
                            frame = null;
                            Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                        }
                        FileInfo f = new FileInfo(current);
                        string bigImageVerticalName = savePathHorizontal + "\\A4Full\\" + f.Name;
                        frame.Save(bigImageVerticalName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }

        private void btnMergeImage_Click(object sender, EventArgs e)
        {

        }
    }
}