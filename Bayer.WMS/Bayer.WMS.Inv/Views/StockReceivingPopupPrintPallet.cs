﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Inv.Views
{
    public partial class StockReceivingPopupPrintPallet : Form
    {
        public bool bolOKToPrint { get; set; }
        public string strPrinterName { get; set; }
        public int intNumberToPrint { get; set; }
        string defaultPrinterName { get; set; }

        public StockReceivingPopupPrintPallet()
        {
            InitializeComponent();
            bolOKToPrint = false;
            strPrinterName = string.Empty;
            intNumberToPrint = 0;
            PrinterSettings settings = new PrinterSettings();
            defaultPrinterName = settings.PrinterName;
        }

        private async void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                btnPrint.Enabled = false;

                await Print();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                btnPrint.Enabled = true;
            }
        }

        private async Task Print()
        {
            try
            {
                intNumberToPrint = Convert.ToInt32(txtNumberToPrint.Text);
                strPrinterName = cmbListPrinter.Text;
                bolOKToPrint = true;

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                //To Nothing Here
            }
        }

        private void StockReceivingPopupPrintPallet_Load(object sender, EventArgs e)
        {
            var printers = System.Drawing.Printing.PrinterSettings.InstalledPrinters;   
            foreach (String s in printers)
            {
                cmbListPrinter.Items.Add(s);
            }

            cmbListPrinter.SelectedItem = defaultPrinterName;
        }

        private async void txtNumberToPrint_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                try
                {
                    btnPrint.Enabled = false;

                    await Print();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    btnPrint.Enabled = true;
                }
            }
        }
    }
}
