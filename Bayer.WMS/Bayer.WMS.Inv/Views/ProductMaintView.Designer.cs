﻿namespace Bayer.WMS.Inv.Views
{
    partial class ProductMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductMaintView));
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.cmbUOM = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbCategoryID = new System.Windows.Forms.ComboBox();
            this.cmbProductCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.colProductPacking_Type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colProductPacking_Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsProductPacking = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.btnProductPacking_Delete = new System.Windows.Forms.Button();
            this.btnProductPacking_Insert = new System.Windows.Forms.Button();
            this.txtCategoryID_ReadOnly = new System.Windows.Forms.TextBox();
            this.txtType_ReadOnly = new System.Windows.Forms.TextBox();
            this.txtUOM_ReadOnly = new System.Windows.Forms.TextBox();
            this.txtStatus_ReadOnly = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbPackingType = new System.Windows.Forms.ComboBox();
            this.txtPackingType_ReadOnly = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.nudSampleQty = new System.Windows.Forms.NumericUpDown();
            this.nudPrintLabelPercentage = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.nudPalletSize = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.nudQuantityByUnit = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.nudQuantityByCarton = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.cmbHeatProtection = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductPacking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSampleQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrintLabelPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPalletSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantityByUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantityByCarton)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(129, 246);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(200, 24);
            this.cmbStatus.TabIndex = 8;
            this.cmbStatus.SelectedValueChanged += new System.EventHandler(this.cmbStatus_SelectedValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 249);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 17;
            this.label5.Text = "Trạng thái:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 133);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "Đơn vị tính:";
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(129, 42);
            this.txtProductDescription.MaxLength = 255;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(624, 22);
            this.txtProductDescription.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "Loại sản phẩm:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 11;
            this.label2.Text = "Mô tả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 16);
            this.label1.TabIndex = 9;
            this.label1.Text = "Mã sản phẩm:";
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(129, 100);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(200, 24);
            this.cmbType.TabIndex = 3;
            this.cmbType.SelectedValueChanged += new System.EventHandler(this.cmbType_SelectedValueChanged);
            // 
            // cmbUOM
            // 
            this.cmbUOM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbUOM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbUOM.FormattingEnabled = true;
            this.cmbUOM.Location = new System.Drawing.Point(129, 130);
            this.cmbUOM.Name = "cmbUOM";
            this.cmbUOM.Size = new System.Drawing.Size(200, 24);
            this.cmbUOM.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 73);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "Nhóm sản phẩm:";
            // 
            // cmbCategoryID
            // 
            this.cmbCategoryID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbCategoryID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCategoryID.FormattingEnabled = true;
            this.cmbCategoryID.IntegralHeight = false;
            this.cmbCategoryID.Location = new System.Drawing.Point(129, 70);
            this.cmbCategoryID.Name = "cmbCategoryID";
            this.cmbCategoryID.Size = new System.Drawing.Size(200, 24);
            this.cmbCategoryID.TabIndex = 2;
            // 
            // cmbProductCode
            // 
            this.cmbProductCode.Columns = null;
            this.cmbProductCode.DropDownHeight = 1;
            this.cmbProductCode.DropDownWidth = 500;
            this.cmbProductCode.FormattingEnabled = true;
            this.cmbProductCode.IntegralHeight = false;
            this.cmbProductCode.Location = new System.Drawing.Point(129, 12);
            this.cmbProductCode.MaxLength = 255;
            this.cmbProductCode.Name = "cmbProductCode";
            this.cmbProductCode.PageSize = 0;
            this.cmbProductCode.PresenterInfo = null;
            this.cmbProductCode.Size = new System.Drawing.Size(624, 24);
            this.cmbProductCode.Source = null;
            this.cmbProductCode.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(759, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 16);
            this.label7.TabIndex = 22;
            this.label7.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(759, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 16);
            this.label10.TabIndex = 25;
            this.label10.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(335, 133);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 16);
            this.label9.TabIndex = 26;
            this.label9.Text = "*";
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgProductPacking.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colProductPacking_Type,
            this.colProductPacking_Size});
            this.dtgProductPacking.DataSource = this.bdsProductPacking;
            this.dtgProductPacking.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(12, 338);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.Size = new System.Drawing.Size(760, 280);
            this.dtgProductPacking.TabIndex = 11;
            this.dtgProductPacking.CurrentCellDirtyStateChanged += new System.EventHandler(this.dtgProductPacking_CurrentCellDirtyStateChanged);
            // 
            // colProductPacking_Type
            // 
            this.colProductPacking_Type.DataPropertyName = "Type";
            this.colProductPacking_Type.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colProductPacking_Type.HeaderText = "Loại";
            this.colProductPacking_Type.Name = "colProductPacking_Type";
            this.colProductPacking_Type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colProductPacking_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colProductPacking_Type.Width = 200;
            // 
            // colProductPacking_Size
            // 
            this.colProductPacking_Size.DataPropertyName = "Quantity";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = null;
            this.colProductPacking_Size.DefaultCellStyle = dataGridViewCellStyle4;
            this.colProductPacking_Size.HeaderText = "Quy cách";
            this.colProductPacking_Size.Name = "colProductPacking_Size";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(13, 283);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 16);
            this.label8.TabIndex = 28;
            this.label8.Text = "Quy cách đóng gói";
            // 
            // btnProductPacking_Delete
            // 
            this.btnProductPacking_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btnProductPacking_Delete.Image")));
            this.btnProductPacking_Delete.Location = new System.Drawing.Point(68, 302);
            this.btnProductPacking_Delete.Name = "btnProductPacking_Delete";
            this.btnProductPacking_Delete.Size = new System.Drawing.Size(50, 30);
            this.btnProductPacking_Delete.TabIndex = 10;
            this.btnProductPacking_Delete.TabStop = false;
            this.btnProductPacking_Delete.UseVisualStyleBackColor = true;
            this.btnProductPacking_Delete.Click += new System.EventHandler(this.btnProductPacking_Delete_Click);
            // 
            // btnProductPacking_Insert
            // 
            this.btnProductPacking_Insert.Image = ((System.Drawing.Image)(resources.GetObject("btnProductPacking_Insert.Image")));
            this.btnProductPacking_Insert.Location = new System.Drawing.Point(12, 302);
            this.btnProductPacking_Insert.Name = "btnProductPacking_Insert";
            this.btnProductPacking_Insert.Size = new System.Drawing.Size(50, 30);
            this.btnProductPacking_Insert.TabIndex = 9;
            this.btnProductPacking_Insert.TabStop = false;
            this.btnProductPacking_Insert.UseVisualStyleBackColor = true;
            this.btnProductPacking_Insert.Click += new System.EventHandler(this.btnProductPacking_Insert_Click);
            // 
            // txtCategoryID_ReadOnly
            // 
            this.txtCategoryID_ReadOnly.Location = new System.Drawing.Point(129, 70);
            this.txtCategoryID_ReadOnly.MaxLength = 255;
            this.txtCategoryID_ReadOnly.Multiline = true;
            this.txtCategoryID_ReadOnly.Name = "txtCategoryID_ReadOnly";
            this.txtCategoryID_ReadOnly.ReadOnly = true;
            this.txtCategoryID_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtCategoryID_ReadOnly.TabIndex = 31;
            // 
            // txtType_ReadOnly
            // 
            this.txtType_ReadOnly.Location = new System.Drawing.Point(129, 100);
            this.txtType_ReadOnly.MaxLength = 255;
            this.txtType_ReadOnly.Multiline = true;
            this.txtType_ReadOnly.Name = "txtType_ReadOnly";
            this.txtType_ReadOnly.ReadOnly = true;
            this.txtType_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtType_ReadOnly.TabIndex = 32;
            // 
            // txtUOM_ReadOnly
            // 
            this.txtUOM_ReadOnly.Location = new System.Drawing.Point(129, 130);
            this.txtUOM_ReadOnly.MaxLength = 255;
            this.txtUOM_ReadOnly.Multiline = true;
            this.txtUOM_ReadOnly.Name = "txtUOM_ReadOnly";
            this.txtUOM_ReadOnly.ReadOnly = true;
            this.txtUOM_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtUOM_ReadOnly.TabIndex = 33;
            // 
            // txtStatus_ReadOnly
            // 
            this.txtStatus_ReadOnly.Location = new System.Drawing.Point(129, 246);
            this.txtStatus_ReadOnly.MaxLength = 255;
            this.txtStatus_ReadOnly.Multiline = true;
            this.txtStatus_ReadOnly.Name = "txtStatus_ReadOnly";
            this.txtStatus_ReadOnly.ReadOnly = true;
            this.txtStatus_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtStatus_ReadOnly.TabIndex = 34;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 163);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 16);
            this.label11.TabIndex = 36;
            this.label11.Text = "Loại đóng gói:";
            // 
            // cmbPackingType
            // 
            this.cmbPackingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPackingType.FormattingEnabled = true;
            this.cmbPackingType.Location = new System.Drawing.Point(129, 160);
            this.cmbPackingType.Name = "cmbPackingType";
            this.cmbPackingType.Size = new System.Drawing.Size(200, 24);
            this.cmbPackingType.TabIndex = 5;
            this.cmbPackingType.SelectedValueChanged += new System.EventHandler(this.cmbPackingType_SelectedValueChanged);
            // 
            // txtPackingType_ReadOnly
            // 
            this.txtPackingType_ReadOnly.Location = new System.Drawing.Point(129, 160);
            this.txtPackingType_ReadOnly.MaxLength = 255;
            this.txtPackingType_ReadOnly.Multiline = true;
            this.txtPackingType_ReadOnly.Name = "txtPackingType_ReadOnly";
            this.txtPackingType_ReadOnly.ReadOnly = true;
            this.txtPackingType_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtPackingType_ReadOnly.TabIndex = 37;
            // 
            // nudSampleQty
            // 
            this.nudSampleQty.Location = new System.Drawing.Point(129, 190);
            this.nudSampleQty.Name = "nudSampleQty";
            this.nudSampleQty.Size = new System.Drawing.Size(200, 22);
            this.nudSampleQty.TabIndex = 6;
            // 
            // nudPrintLabelPercentage
            // 
            this.nudPrintLabelPercentage.DecimalPlaces = 1;
            this.nudPrintLabelPercentage.Location = new System.Drawing.Point(129, 218);
            this.nudPrintLabelPercentage.Name = "nudPrintLabelPercentage";
            this.nudPrintLabelPercentage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nudPrintLabelPercentage.Size = new System.Drawing.Size(200, 22);
            this.nudPrintLabelPercentage.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 192);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 16);
            this.label12.TabIndex = 40;
            this.label12.Text = "Số lượng mẫu:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 220);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 16);
            this.label13.TabIndex = 41;
            this.label13.Text = "% Nhãn in thêm:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(335, 220);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 16);
            this.label15.TabIndex = 44;
            this.label15.Text = "%";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(361, 162);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(185, 16);
            this.label14.TabIndex = 46;
            this.label14.Text = "Số lượng đóng gói theo pallet:";
            this.label14.Visible = false;
            // 
            // nudPalletSize
            // 
            this.nudPalletSize.Location = new System.Drawing.Point(553, 159);
            this.nudPalletSize.Name = "nudPalletSize";
            this.nudPalletSize.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nudPalletSize.Size = new System.Drawing.Size(200, 22);
            this.nudPalletSize.TabIndex = 45;
            this.nudPalletSize.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(376, 104);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(170, 16);
            this.label16.TabIndex = 48;
            this.label16.Text = "Khối lượng theo đơn vị (KG):";
            // 
            // nudQuantityByUnit
            // 
            this.nudQuantityByUnit.DecimalPlaces = 2;
            this.nudQuantityByUnit.Location = new System.Drawing.Point(553, 102);
            this.nudQuantityByUnit.Name = "nudQuantityByUnit";
            this.nudQuantityByUnit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nudQuantityByUnit.Size = new System.Drawing.Size(200, 22);
            this.nudQuantityByUnit.TabIndex = 47;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(380, 135);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(166, 16);
            this.label17.TabIndex = 50;
            this.label17.Text = "Khối lượng theo thùng (KG):";
            // 
            // nudQuantityByCarton
            // 
            this.nudQuantityByCarton.DecimalPlaces = 2;
            this.nudQuantityByCarton.Location = new System.Drawing.Point(553, 132);
            this.nudQuantityByCarton.Name = "nudQuantityByCarton";
            this.nudQuantityByCarton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.nudQuantityByCarton.Size = new System.Drawing.Size(200, 22);
            this.nudQuantityByCarton.TabIndex = 49;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(425, 75);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(121, 16);
            this.label18.TabIndex = 52;
            this.label18.Text = "Nhiệt độ bảo quản:";
            // 
            // cmbHeatProtection
            // 
            this.cmbHeatProtection.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbHeatProtection.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbHeatProtection.FormattingEnabled = true;
            this.cmbHeatProtection.IntegralHeight = false;
            this.cmbHeatProtection.Location = new System.Drawing.Point(553, 72);
            this.cmbHeatProtection.Name = "cmbHeatProtection";
            this.cmbHeatProtection.Size = new System.Drawing.Size(200, 24);
            this.cmbHeatProtection.TabIndex = 51;
            // 
            // ProductMaintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 630);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.cmbHeatProtection);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.nudQuantityByCarton);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.nudQuantityByUnit);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.nudPalletSize);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.nudPrintLabelPercentage);
            this.Controls.Add(this.nudSampleQty);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cmbPackingType);
            this.Controls.Add(this.btnProductPacking_Insert);
            this.Controls.Add(this.btnProductPacking_Delete);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dtgProductPacking);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbProductCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtProductDescription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbUOM);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.txtType_ReadOnly);
            this.Controls.Add(this.txtUOM_ReadOnly);
            this.Controls.Add(this.txtStatus_ReadOnly);
            this.Controls.Add(this.txtPackingType_ReadOnly);
            this.Controls.Add(this.cmbCategoryID);
            this.Controls.Add(this.txtCategoryID_ReadOnly);
            this.Name = "ProductMaintView";
            this.Text = "ProductMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsProductPacking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSampleQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrintLabelPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPalletSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantityByUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantityByCarton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomControls.MultiColumnComboBox cmbProductCode;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtProductDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.ComboBox cmbUOM;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbCategoryID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.BindingSource bdsProductPacking;
        private System.Windows.Forms.Button btnProductPacking_Delete;
        private System.Windows.Forms.Button btnProductPacking_Insert;
        private System.Windows.Forms.DataGridViewComboBoxColumn colProductPacking_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductPacking_Size;
        private System.Windows.Forms.TextBox txtCategoryID_ReadOnly;
        private System.Windows.Forms.TextBox txtType_ReadOnly;
        private System.Windows.Forms.TextBox txtUOM_ReadOnly;
        private System.Windows.Forms.TextBox txtStatus_ReadOnly;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbPackingType;
        private System.Windows.Forms.TextBox txtPackingType_ReadOnly;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.NumericUpDown nudSampleQty;
        private System.Windows.Forms.NumericUpDown nudPrintLabelPercentage;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudPalletSize;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudQuantityByUnit;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown nudQuantityByCarton;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cmbHeatProtection;
    }
}