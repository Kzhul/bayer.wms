﻿namespace Bayer.WMS.Inv.Views
{
    partial class PalletMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PalletMaintView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgPallet = new System.Windows.Forms.DataGridView();
            this.bdsPallet = new System.Windows.Forms.BindingSource(this.components);
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPalletCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnGenPallet = new System.Windows.Forms.Button();
            this.btnPrintPallet = new System.Windows.Forms.Button();
            this.colPallet_PalletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPallet_Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrintDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUserPrintFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrintedTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPallet_StartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPallet_ExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrIsDeleted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLineName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUpdatedDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUserFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCartonQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrLocationPutDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrDriver = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrWarehouseVerifyDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStrWarehouseKeeper = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallet)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgPallet
            // 
            this.dtgPallet.AllowUserToAddRows = false;
            this.dtgPallet.AllowUserToOrderColumns = true;
            this.dtgPallet.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgPallet.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgPallet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPallet.AutoGenerateColumns = false;
            this.dtgPallet.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPallet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPallet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPallet_PalletCode,
            this.colPallet_Type,
            this.colPrintDateTime,
            this.colUserPrintFullName,
            this.colPrintedTime,
            this.colPallet_StartDate,
            this.colPallet_ExpiryDate,
            this.colStrStatus,
            this.colStrIsDeleted,
            this.colZoneName,
            this.colLineName,
            this.colLocationName,
            this.colUpdatedDateTime,
            this.colUserFullName,
            this.colCartonQuantity,
            this.colStrLocationPutDate,
            this.colStrDriver,
            this.colStrWarehouseVerifyDate,
            this.colStrWarehouseKeeper,
            this.colLocationCode});
            this.dtgPallet.DataSource = this.bdsPallet;
            this.dtgPallet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgPallet.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPallet.Location = new System.Drawing.Point(12, 76);
            this.dtgPallet.Name = "dtgPallet";
            this.dtgPallet.ReadOnly = true;
            this.dtgPallet.Size = new System.Drawing.Size(983, 527);
            this.dtgPallet.TabIndex = 4;
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(57, 12);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(200, 24);
            this.cmbType.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 16);
            this.label3.TabIndex = 31;
            this.label3.Text = "Loại:";
            // 
            // txtPalletCode
            // 
            this.txtPalletCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtPalletCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtPalletCode.Location = new System.Drawing.Point(347, 12);
            this.txtPalletCode.Name = "txtPalletCode";
            this.txtPalletCode.Size = new System.Drawing.Size(200, 22);
            this.txtPalletCode.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 16);
            this.label2.TabIndex = 38;
            this.label2.Text = "Mã pallet:";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(553, 9);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 30);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnGenPallet
            // 
            this.btnGenPallet.Location = new System.Drawing.Point(16, 39);
            this.btnGenPallet.Name = "btnGenPallet";
            this.btnGenPallet.Size = new System.Drawing.Size(100, 30);
            this.btnGenPallet.TabIndex = 3;
            this.btnGenPallet.TabStop = false;
            this.btnGenPallet.Text = "Tạo Pallet";
            this.btnGenPallet.UseVisualStyleBackColor = true;
            this.btnGenPallet.Click += new System.EventHandler(this.btnGenPallet_Click);
            // 
            // btnPrintPallet
            // 
            this.btnPrintPallet.Location = new System.Drawing.Point(347, 39);
            this.btnPrintPallet.Name = "btnPrintPallet";
            this.btnPrintPallet.Size = new System.Drawing.Size(100, 30);
            this.btnPrintPallet.TabIndex = 39;
            this.btnPrintPallet.TabStop = false;
            this.btnPrintPallet.Text = "In Pallet";
            this.btnPrintPallet.UseVisualStyleBackColor = true;
            this.btnPrintPallet.Click += new System.EventHandler(this.btnPrintPallet_Click);
            // 
            // colPallet_PalletCode
            // 
            this.colPallet_PalletCode.DataPropertyName = "PalletCode";
            this.colPallet_PalletCode.HeaderText = "Mã Pallet";
            this.colPallet_PalletCode.Name = "colPallet_PalletCode";
            this.colPallet_PalletCode.ReadOnly = true;
            // 
            // colPallet_Type
            // 
            this.colPallet_Type.DataPropertyName = "PalletType";
            this.colPallet_Type.HeaderText = "Loại";
            this.colPallet_Type.Name = "colPallet_Type";
            this.colPallet_Type.ReadOnly = true;
            // 
            // colPrintDateTime
            // 
            this.colPrintDateTime.DataPropertyName = "PrintDateTime";
            this.colPrintDateTime.HeaderText = "Ngày in nhãn";
            this.colPrintDateTime.Name = "colPrintDateTime";
            this.colPrintDateTime.ReadOnly = true;
            this.colPrintDateTime.Width = 130;
            // 
            // colUserPrintFullName
            // 
            this.colUserPrintFullName.DataPropertyName = "UserPrintFullName";
            this.colUserPrintFullName.HeaderText = "Người in";
            this.colUserPrintFullName.Name = "colUserPrintFullName";
            this.colUserPrintFullName.ReadOnly = true;
            this.colUserPrintFullName.Width = 150;
            // 
            // colPrintedTime
            // 
            this.colPrintedTime.DataPropertyName = "PrintedTime";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colPrintedTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.colPrintedTime.HeaderText = "Số lần đã in";
            this.colPrintedTime.Name = "colPrintedTime";
            this.colPrintedTime.ReadOnly = true;
            // 
            // colPallet_StartDate
            // 
            this.colPallet_StartDate.DataPropertyName = "StartDate";
            this.colPallet_StartDate.HeaderText = "Ngày đưa vào sử dụng";
            this.colPallet_StartDate.Name = "colPallet_StartDate";
            this.colPallet_StartDate.ReadOnly = true;
            this.colPallet_StartDate.Width = 110;
            // 
            // colPallet_ExpiryDate
            // 
            this.colPallet_ExpiryDate.DataPropertyName = "ExpiryDate";
            this.colPallet_ExpiryDate.HeaderText = "Ngày hết hạn sử dụng";
            this.colPallet_ExpiryDate.Name = "colPallet_ExpiryDate";
            this.colPallet_ExpiryDate.ReadOnly = true;
            this.colPallet_ExpiryDate.Width = 110;
            // 
            // colStrStatus
            // 
            this.colStrStatus.DataPropertyName = "StrStatus";
            this.colStrStatus.HeaderText = "Trạng thái";
            this.colStrStatus.Name = "colStrStatus";
            this.colStrStatus.ReadOnly = true;
            // 
            // colStrIsDeleted
            // 
            this.colStrIsDeleted.DataPropertyName = "StrIsDeleted";
            this.colStrIsDeleted.HeaderText = "Tình trạng sử dụng";
            this.colStrIsDeleted.Name = "colStrIsDeleted";
            this.colStrIsDeleted.ReadOnly = true;
            // 
            // colZoneName
            // 
            this.colZoneName.DataPropertyName = "ZoneName";
            this.colZoneName.HeaderText = "Khu vực";
            this.colZoneName.Name = "colZoneName";
            this.colZoneName.ReadOnly = true;
            this.colZoneName.Width = 130;
            // 
            // colLineName
            // 
            this.colLineName.DataPropertyName = "LineName";
            this.colLineName.HeaderText = "Dãy";
            this.colLineName.Name = "colLineName";
            this.colLineName.ReadOnly = true;
            // 
            // colLocationName
            // 
            this.colLocationName.DataPropertyName = "LocationName";
            this.colLocationName.HeaderText = "Vị trí";
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.ReadOnly = true;
            // 
            // colUpdatedDateTime
            // 
            this.colUpdatedDateTime.DataPropertyName = "UpdatedDateTime";
            this.colUpdatedDateTime.HeaderText = "Ngày sử dụng gần nhất";
            this.colUpdatedDateTime.Name = "colUpdatedDateTime";
            this.colUpdatedDateTime.ReadOnly = true;
            this.colUpdatedDateTime.Width = 130;
            // 
            // colUserFullName
            // 
            this.colUserFullName.DataPropertyName = "UserFullName";
            this.colUserFullName.HeaderText = "Người sử dụng";
            this.colUserFullName.Name = "colUserFullName";
            this.colUserFullName.ReadOnly = true;
            this.colUserFullName.Width = 150;
            // 
            // colCartonQuantity
            // 
            this.colCartonQuantity.DataPropertyName = "CartonQuantity";
            this.colCartonQuantity.HeaderText = "Hàng hóa";
            this.colCartonQuantity.Name = "colCartonQuantity";
            this.colCartonQuantity.ReadOnly = true;
            // 
            // colStrLocationPutDate
            // 
            this.colStrLocationPutDate.DataPropertyName = "StrLocationPutDate";
            this.colStrLocationPutDate.HeaderText = "Ngày đặt vị trí";
            this.colStrLocationPutDate.Name = "colStrLocationPutDate";
            this.colStrLocationPutDate.ReadOnly = true;
            this.colStrLocationPutDate.Width = 130;
            // 
            // colStrDriver
            // 
            this.colStrDriver.DataPropertyName = "StrDriver";
            this.colStrDriver.HeaderText = "Xe nâng";
            this.colStrDriver.Name = "colStrDriver";
            this.colStrDriver.ReadOnly = true;
            this.colStrDriver.Width = 150;
            // 
            // colStrWarehouseVerifyDate
            // 
            this.colStrWarehouseVerifyDate.DataPropertyName = "StrWarehouseVerifyDate";
            this.colStrWarehouseVerifyDate.HeaderText = "Ngày thủ kho duyệt";
            this.colStrWarehouseVerifyDate.Name = "colStrWarehouseVerifyDate";
            this.colStrWarehouseVerifyDate.ReadOnly = true;
            this.colStrWarehouseVerifyDate.Width = 130;
            // 
            // colStrWarehouseKeeper
            // 
            this.colStrWarehouseKeeper.DataPropertyName = "StrWarehouseKeeper";
            this.colStrWarehouseKeeper.HeaderText = "Thủ kho";
            this.colStrWarehouseKeeper.Name = "colStrWarehouseKeeper";
            this.colStrWarehouseKeeper.ReadOnly = true;
            this.colStrWarehouseKeeper.Width = 150;
            // 
            // colLocationCode
            // 
            this.colLocationCode.DataPropertyName = "LocationCode";
            this.colLocationCode.HeaderText = "Mã vị trí";
            this.colLocationCode.Name = "colLocationCode";
            this.colLocationCode.ReadOnly = true;
            // 
            // PalletMaintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 615);
            this.Controls.Add(this.btnPrintPallet);
            this.Controls.Add(this.btnGenPallet);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtPalletCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgPallet);
            this.Name = "PalletMaintView";
            this.Text = "PalletMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgPallet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPallet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgPallet;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPalletCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource bdsPallet;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnGenPallet;
        private System.Windows.Forms.Button btnPrintPallet;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPallet_PalletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPallet_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrintDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserPrintFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrintedTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPallet_StartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPallet_ExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrIsDeleted;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUpdatedDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrLocationPutDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrDriver;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrWarehouseVerifyDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStrWarehouseKeeper;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationCode;
    }
}