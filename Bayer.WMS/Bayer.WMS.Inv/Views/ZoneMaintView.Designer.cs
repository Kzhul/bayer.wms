﻿using Bayer.WMS.Objs;

namespace Bayer.WMS.Inv.Views
{
    partial class ZoneMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZoneMaintView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsZoneLine = new System.Windows.Forms.BindingSource(this.components);
            this.bdsZoneLocation = new System.Windows.Forms.BindingSource(this.components);
            this.bdsZoneCategory = new System.Windows.Forms.BindingSource(this.components);
            this.bdsZoneTemperature = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txtZoneName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbZoneCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbpLine = new System.Windows.Forms.TabPage();
            this.btnZoneLine_Insert = new System.Windows.Forms.Button();
            this.btnZoneLine_Delete = new System.Windows.Forms.Button();
            this.dtgZoneLine = new System.Windows.Forms.DataGridView();
            this.colZoneLine_LineCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_LineName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_Type = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colZoneLine_Length = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_Level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_Order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLine_Status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colZoneLine_Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbpLocation = new System.Windows.Forms.TabPage();
            this.btnZoneLocation_Delete = new System.Windows.Forms.Button();
            this.dtgZoneLocation = new System.Windows.Forms.DataGridView();
            this.colZoneLocation_LocationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLocation_LineCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLocation_LevelID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLocation_LengthID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneLocation_Status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colZoneLocation_Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbpProductCategory = new System.Windows.Forms.TabPage();
            this.dtgZoneCategory = new System.Windows.Forms.DataGridView();
            this.colZoneCategory_Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colZoneCategory_CategoryDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbpTemperature = new System.Windows.Forms.TabPage();
            this.dtgZoneTemperature = new System.Windows.Forms.DataGridView();
            this.colZoneTemperature_Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colZoneTemperature_ReferenceDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cmbWarehouseID = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.txtWarehouseID_ReadOnly = new System.Windows.Forms.TextBox();
            this.txtStatus_ReadOnly = new System.Windows.Forms.TextBox();
            this.btnExportQRCode = new System.Windows.Forms.Button();
            this.btnMergeImage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneTemperature)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tbpLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneLine)).BeginInit();
            this.tbpLocation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneLocation)).BeginInit();
            this.tbpProductCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneCategory)).BeginInit();
            this.tbpTemperature.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneTemperature)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(500, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 16);
            this.label8.TabIndex = 48;
            this.label8.Text = "*";
            // 
            // txtZoneName
            // 
            this.txtZoneName.Location = new System.Drawing.Point(94, 42);
            this.txtZoneName.MaxLength = 255;
            this.txtZoneName.Name = "txtZoneName";
            this.txtZoneName.Size = new System.Drawing.Size(400, 22);
            this.txtZoneName.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 45);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 16);
            this.label6.TabIndex = 47;
            this.label6.Text = "Tên:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(822, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 16);
            this.label4.TabIndex = 45;
            this.label4.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(535, 45);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 16);
            this.label3.TabIndex = 43;
            this.label3.Text = "Kho:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(500, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 16);
            this.label10.TabIndex = 41;
            this.label10.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(500, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 16);
            this.label7.TabIndex = 40;
            this.label7.Text = "*";
            // 
            // cmbZoneCode
            // 
            this.cmbZoneCode.Columns = null;
            this.cmbZoneCode.DropDownHeight = 1;
            this.cmbZoneCode.DropDownWidth = 500;
            this.cmbZoneCode.FormattingEnabled = true;
            this.cmbZoneCode.IntegralHeight = false;
            this.cmbZoneCode.Location = new System.Drawing.Point(94, 12);
            this.cmbZoneCode.MaxLength = 3;
            this.cmbZoneCode.Name = "cmbZoneCode";
            this.cmbZoneCode.PageSize = 0;
            this.cmbZoneCode.PresenterInfo = null;
            this.cmbZoneCode.Size = new System.Drawing.Size(400, 24);
            this.cmbZoneCode.Source = null;
            this.cmbZoneCode.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(535, 15);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 39;
            this.label5.Text = "Trạng thái:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 73);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 38;
            this.label2.Text = "Mô tả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 16);
            this.label1.TabIndex = 37;
            this.label1.Text = "Mã khu vực:";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(616, 12);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(200, 24);
            this.cmbStatus.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tbpLine);
            this.tabControl1.Controls.Add(this.tbpLocation);
            this.tabControl1.Controls.Add(this.tbpProductCategory);
            this.tabControl1.Controls.Add(this.tbpTemperature);
            this.tabControl1.Location = new System.Drawing.Point(4, 98);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1030, 550);
            this.tabControl1.TabIndex = 5;
            // 
            // tbpLine
            // 
            this.tbpLine.Controls.Add(this.btnZoneLine_Insert);
            this.tbpLine.Controls.Add(this.btnZoneLine_Delete);
            this.tbpLine.Controls.Add(this.dtgZoneLine);
            this.tbpLine.Location = new System.Drawing.Point(4, 25);
            this.tbpLine.Name = "tbpLine";
            this.tbpLine.Padding = new System.Windows.Forms.Padding(3);
            this.tbpLine.Size = new System.Drawing.Size(1022, 521);
            this.tbpLine.TabIndex = 1;
            this.tbpLine.Text = "Dãy & Đường đi";
            this.tbpLine.UseVisualStyleBackColor = true;
            // 
            // btnZoneLine_Insert
            // 
            this.btnZoneLine_Insert.Image = ((System.Drawing.Image)(resources.GetObject("btnZoneLine_Insert.Image")));
            this.btnZoneLine_Insert.Location = new System.Drawing.Point(3, 3);
            this.btnZoneLine_Insert.Name = "btnZoneLine_Insert";
            this.btnZoneLine_Insert.Size = new System.Drawing.Size(50, 25);
            this.btnZoneLine_Insert.TabIndex = 0;
            this.btnZoneLine_Insert.TabStop = false;
            this.btnZoneLine_Insert.UseVisualStyleBackColor = true;
            this.btnZoneLine_Insert.Click += new System.EventHandler(this.btnZoneLine_Insert_Click);
            // 
            // btnZoneLine_Delete
            // 
            this.btnZoneLine_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btnZoneLine_Delete.Image")));
            this.btnZoneLine_Delete.Location = new System.Drawing.Point(59, 3);
            this.btnZoneLine_Delete.Name = "btnZoneLine_Delete";
            this.btnZoneLine_Delete.Size = new System.Drawing.Size(50, 25);
            this.btnZoneLine_Delete.TabIndex = 1;
            this.btnZoneLine_Delete.TabStop = false;
            this.btnZoneLine_Delete.UseVisualStyleBackColor = true;
            this.btnZoneLine_Delete.Click += new System.EventHandler(this.btnZoneLine_Delete_Click);
            // 
            // dtgZoneLine
            // 
            this.dtgZoneLine.AllowUserToOrderColumns = true;
            this.dtgZoneLine.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgZoneLine.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgZoneLine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgZoneLine.AutoGenerateColumns = false;
            this.dtgZoneLine.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgZoneLine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgZoneLine.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneLine_LineCode,
            this.colZoneLine_LineName,
            this.colZoneLine_Type,
            this.colZoneLine_Length,
            this.colZoneLine_Level,
            this.colZoneLine_Order,
            this.colZoneLine_Status,
            this.colZoneLine_Description});
            this.dtgZoneLine.DataSource = this.bdsZoneLine;
            this.dtgZoneLine.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgZoneLine.GridColor = System.Drawing.SystemColors.Control;
            this.dtgZoneLine.Location = new System.Drawing.Point(0, 34);
            this.dtgZoneLine.Name = "dtgZoneLine";
            this.dtgZoneLine.Size = new System.Drawing.Size(1022, 487);
            this.dtgZoneLine.TabIndex = 2;
            this.dtgZoneLine.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgZoneLine_CellValueChanged);
            this.dtgZoneLine.CurrentCellDirtyStateChanged += new System.EventHandler(this.dtgZoneLine_CurrentCellDirtyStateChanged);
            // 
            // colZoneLine_LineCode
            // 
            this.colZoneLine_LineCode.DataPropertyName = "LineCode";
            this.colZoneLine_LineCode.HeaderText = "Mã";
            this.colZoneLine_LineCode.Name = "colZoneLine_LineCode";
            // 
            // colZoneLine_LineName
            // 
            this.colZoneLine_LineName.DataPropertyName = "LineName";
            this.colZoneLine_LineName.HeaderText = "Tên";
            this.colZoneLine_LineName.Name = "colZoneLine_LineName";
            // 
            // colZoneLine_Type
            // 
            this.colZoneLine_Type.DataPropertyName = "Type";
            this.colZoneLine_Type.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colZoneLine_Type.HeaderText = "Loại";
            this.colZoneLine_Type.Name = "colZoneLine_Type";
            this.colZoneLine_Type.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colZoneLine_Type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // colZoneLine_Length
            // 
            this.colZoneLine_Length.DataPropertyName = "Length";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            this.colZoneLine_Length.DefaultCellStyle = dataGridViewCellStyle2;
            this.colZoneLine_Length.HeaderText = "Dài";
            this.colZoneLine_Length.Name = "colZoneLine_Length";
            this.colZoneLine_Length.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // colZoneLine_Level
            // 
            this.colZoneLine_Level.DataPropertyName = "Level";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            this.colZoneLine_Level.DefaultCellStyle = dataGridViewCellStyle3;
            this.colZoneLine_Level.HeaderText = "Cao";
            this.colZoneLine_Level.Name = "colZoneLine_Level";
            // 
            // colZoneLine_Order
            // 
            this.colZoneLine_Order.DataPropertyName = "Order";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "N0";
            this.colZoneLine_Order.DefaultCellStyle = dataGridViewCellStyle4;
            this.colZoneLine_Order.HeaderText = "Thứ tự";
            this.colZoneLine_Order.Name = "colZoneLine_Order";
            // 
            // colZoneLine_Status
            // 
            this.colZoneLine_Status.DataPropertyName = "Status";
            this.colZoneLine_Status.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colZoneLine_Status.HeaderText = "Trạng thái";
            this.colZoneLine_Status.Name = "colZoneLine_Status";
            this.colZoneLine_Status.Width = 150;
            // 
            // colZoneLine_Description
            // 
            this.colZoneLine_Description.DataPropertyName = "Description";
            this.colZoneLine_Description.HeaderText = "Ghi chú";
            this.colZoneLine_Description.Name = "colZoneLine_Description";
            this.colZoneLine_Description.Width = 300;
            // 
            // tbpLocation
            // 
            this.tbpLocation.Controls.Add(this.btnZoneLocation_Delete);
            this.tbpLocation.Controls.Add(this.dtgZoneLocation);
            this.tbpLocation.Location = new System.Drawing.Point(4, 25);
            this.tbpLocation.Name = "tbpLocation";
            this.tbpLocation.Size = new System.Drawing.Size(1022, 521);
            this.tbpLocation.TabIndex = 2;
            this.tbpLocation.Text = "Vị trí";
            this.tbpLocation.UseVisualStyleBackColor = true;
            // 
            // btnZoneLocation_Delete
            // 
            this.btnZoneLocation_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btnZoneLocation_Delete.Image")));
            this.btnZoneLocation_Delete.Location = new System.Drawing.Point(3, 3);
            this.btnZoneLocation_Delete.Name = "btnZoneLocation_Delete";
            this.btnZoneLocation_Delete.Size = new System.Drawing.Size(50, 25);
            this.btnZoneLocation_Delete.TabIndex = 0;
            this.btnZoneLocation_Delete.TabStop = false;
            this.btnZoneLocation_Delete.UseVisualStyleBackColor = true;
            this.btnZoneLocation_Delete.Click += new System.EventHandler(this.btnZoneLocation_Delete_Click);
            // 
            // dtgZoneLocation
            // 
            this.dtgZoneLocation.AllowUserToAddRows = false;
            this.dtgZoneLocation.AllowUserToDeleteRows = false;
            this.dtgZoneLocation.AllowUserToOrderColumns = true;
            this.dtgZoneLocation.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Azure;
            this.dtgZoneLocation.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dtgZoneLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgZoneLocation.AutoGenerateColumns = false;
            this.dtgZoneLocation.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgZoneLocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgZoneLocation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneLocation_LocationName,
            this.colZoneLocation_LineCode,
            this.colZoneLocation_LevelID,
            this.colZoneLocation_LengthID,
            this.colZoneLocation_Status,
            this.colZoneLocation_Description,
            this.colLocationCode});
            this.dtgZoneLocation.DataSource = this.bdsZoneLocation;
            this.dtgZoneLocation.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgZoneLocation.GridColor = System.Drawing.SystemColors.Control;
            this.dtgZoneLocation.Location = new System.Drawing.Point(0, 34);
            this.dtgZoneLocation.Name = "dtgZoneLocation";
            this.dtgZoneLocation.Size = new System.Drawing.Size(1022, 455);
            this.dtgZoneLocation.TabIndex = 1;
            // 
            // colZoneLocation_LocationName
            // 
            this.colZoneLocation_LocationName.DataPropertyName = "LocationName";
            this.colZoneLocation_LocationName.HeaderText = "Tên vị trí";
            this.colZoneLocation_LocationName.Name = "colZoneLocation_LocationName";
            // 
            // colZoneLocation_LineCode
            // 
            this.colZoneLocation_LineCode.DataPropertyName = "LineCode";
            this.colZoneLocation_LineCode.HeaderText = "Mã dãy";
            this.colZoneLocation_LineCode.Name = "colZoneLocation_LineCode";
            this.colZoneLocation_LineCode.ReadOnly = true;
            // 
            // colZoneLocation_LevelID
            // 
            this.colZoneLocation_LevelID.DataPropertyName = "LevelID";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            this.colZoneLocation_LevelID.DefaultCellStyle = dataGridViewCellStyle6;
            this.colZoneLocation_LevelID.HeaderText = "Tầng";
            this.colZoneLocation_LevelID.Name = "colZoneLocation_LevelID";
            this.colZoneLocation_LevelID.ReadOnly = true;
            // 
            // colZoneLocation_LengthID
            // 
            this.colZoneLocation_LengthID.DataPropertyName = "LengthID";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Format = "N0";
            this.colZoneLocation_LengthID.DefaultCellStyle = dataGridViewCellStyle7;
            this.colZoneLocation_LengthID.HeaderText = "Thứ tự";
            this.colZoneLocation_LengthID.Name = "colZoneLocation_LengthID";
            this.colZoneLocation_LengthID.ReadOnly = true;
            // 
            // colZoneLocation_Status
            // 
            this.colZoneLocation_Status.DataPropertyName = "Status";
            this.colZoneLocation_Status.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colZoneLocation_Status.HeaderText = "Trạng thái";
            this.colZoneLocation_Status.Name = "colZoneLocation_Status";
            this.colZoneLocation_Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colZoneLocation_Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colZoneLocation_Status.Width = 150;
            // 
            // colZoneLocation_Description
            // 
            this.colZoneLocation_Description.DataPropertyName = "Description";
            this.colZoneLocation_Description.HeaderText = "Ghi chú";
            this.colZoneLocation_Description.Name = "colZoneLocation_Description";
            this.colZoneLocation_Description.Width = 300;
            // 
            // colLocationCode
            // 
            this.colLocationCode.DataPropertyName = "LocationCode";
            this.colLocationCode.HeaderText = "Mã vị trí";
            this.colLocationCode.Name = "colLocationCode";
            this.colLocationCode.ReadOnly = true;
            this.colLocationCode.Width = 150;
            // 
            // tbpProductCategory
            // 
            this.tbpProductCategory.Controls.Add(this.dtgZoneCategory);
            this.tbpProductCategory.Location = new System.Drawing.Point(4, 25);
            this.tbpProductCategory.Name = "tbpProductCategory";
            this.tbpProductCategory.Padding = new System.Windows.Forms.Padding(3);
            this.tbpProductCategory.Size = new System.Drawing.Size(1022, 521);
            this.tbpProductCategory.TabIndex = 0;
            this.tbpProductCategory.Text = "Loại sản phẩm";
            this.tbpProductCategory.UseVisualStyleBackColor = true;
            // 
            // dtgZoneCategory
            // 
            this.dtgZoneCategory.AllowUserToAddRows = false;
            this.dtgZoneCategory.AllowUserToOrderColumns = true;
            this.dtgZoneCategory.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Azure;
            this.dtgZoneCategory.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dtgZoneCategory.AutoGenerateColumns = false;
            this.dtgZoneCategory.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgZoneCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgZoneCategory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneCategory_Check,
            this.colZoneCategory_CategoryDescription});
            this.dtgZoneCategory.DataSource = this.bdsZoneCategory;
            this.dtgZoneCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgZoneCategory.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgZoneCategory.GridColor = System.Drawing.SystemColors.Control;
            this.dtgZoneCategory.Location = new System.Drawing.Point(3, 3);
            this.dtgZoneCategory.Name = "dtgZoneCategory";
            this.dtgZoneCategory.Size = new System.Drawing.Size(1016, 515);
            this.dtgZoneCategory.TabIndex = 2;
            this.dtgZoneCategory.CurrentCellDirtyStateChanged += new System.EventHandler(this.dtgZoneCategory_CurrentCellDirtyStateChanged);
            // 
            // colZoneCategory_Check
            // 
            this.colZoneCategory_Check.DataPropertyName = "Checked";
            this.colZoneCategory_Check.HeaderText = "";
            this.colZoneCategory_Check.Name = "colZoneCategory_Check";
            this.colZoneCategory_Check.Width = 50;
            // 
            // colZoneCategory_CategoryDescription
            // 
            this.colZoneCategory_CategoryDescription.DataPropertyName = "CategoryDescription";
            this.colZoneCategory_CategoryDescription.HeaderText = "Nhóm sản phẩm";
            this.colZoneCategory_CategoryDescription.Name = "colZoneCategory_CategoryDescription";
            this.colZoneCategory_CategoryDescription.ReadOnly = true;
            this.colZoneCategory_CategoryDescription.Width = 300;
            // 
            // tbpTemperature
            // 
            this.tbpTemperature.Controls.Add(this.dtgZoneTemperature);
            this.tbpTemperature.Location = new System.Drawing.Point(4, 25);
            this.tbpTemperature.Name = "tbpTemperature";
            this.tbpTemperature.Padding = new System.Windows.Forms.Padding(3);
            this.tbpTemperature.Size = new System.Drawing.Size(1022, 521);
            this.tbpTemperature.TabIndex = 3;
            this.tbpTemperature.Text = "Nhiệt độ bảo quản";
            this.tbpTemperature.UseVisualStyleBackColor = true;
            // 
            // dtgZoneTemperature
            // 
            this.dtgZoneTemperature.AllowUserToAddRows = false;
            this.dtgZoneTemperature.AllowUserToOrderColumns = true;
            this.dtgZoneTemperature.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Azure;
            this.dtgZoneTemperature.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dtgZoneTemperature.AutoGenerateColumns = false;
            this.dtgZoneTemperature.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgZoneTemperature.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgZoneTemperature.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colZoneTemperature_Check,
            this.colZoneTemperature_ReferenceDescription});
            this.dtgZoneTemperature.DataSource = this.bdsZoneTemperature;
            this.dtgZoneTemperature.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgZoneTemperature.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgZoneTemperature.GridColor = System.Drawing.SystemColors.Control;
            this.dtgZoneTemperature.Location = new System.Drawing.Point(3, 3);
            this.dtgZoneTemperature.Name = "dtgZoneTemperature";
            this.dtgZoneTemperature.Size = new System.Drawing.Size(1016, 515);
            this.dtgZoneTemperature.TabIndex = 3;
            this.dtgZoneTemperature.CurrentCellDirtyStateChanged += new System.EventHandler(this.dtgZoneTemperature_CurrentCellDirtyStateChanged);
            // 
            // colZoneTemperature_Check
            // 
            this.colZoneTemperature_Check.DataPropertyName = "Checked";
            this.colZoneTemperature_Check.HeaderText = "";
            this.colZoneTemperature_Check.Name = "colZoneTemperature_Check";
            this.colZoneTemperature_Check.Width = 50;
            // 
            // colZoneTemperature_ReferenceDescription
            // 
            this.colZoneTemperature_ReferenceDescription.DataPropertyName = "ReferenceDescription";
            this.colZoneTemperature_ReferenceDescription.HeaderText = "Nhiệt độ bảo quản";
            this.colZoneTemperature_ReferenceDescription.Name = "colZoneTemperature_ReferenceDescription";
            this.colZoneTemperature_ReferenceDescription.ReadOnly = true;
            this.colZoneTemperature_ReferenceDescription.Width = 300;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(94, 70);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(400, 22);
            this.txtDescription.TabIndex = 2;
            // 
            // cmbWarehouseID
            // 
            this.cmbWarehouseID.Columns = null;
            this.cmbWarehouseID.DropDownHeight = 1;
            this.cmbWarehouseID.DropDownWidth = 500;
            this.cmbWarehouseID.FormattingEnabled = true;
            this.cmbWarehouseID.IntegralHeight = false;
            this.cmbWarehouseID.Location = new System.Drawing.Point(616, 42);
            this.cmbWarehouseID.MaxLength = 255;
            this.cmbWarehouseID.Name = "cmbWarehouseID";
            this.cmbWarehouseID.PageSize = 0;
            this.cmbWarehouseID.PresenterInfo = null;
            this.cmbWarehouseID.Size = new System.Drawing.Size(200, 24);
            this.cmbWarehouseID.Source = null;
            this.cmbWarehouseID.TabIndex = 4;
            // 
            // txtWarehouseID_ReadOnly
            // 
            this.txtWarehouseID_ReadOnly.Location = new System.Drawing.Point(616, 42);
            this.txtWarehouseID_ReadOnly.Multiline = true;
            this.txtWarehouseID_ReadOnly.Name = "txtWarehouseID_ReadOnly";
            this.txtWarehouseID_ReadOnly.ReadOnly = true;
            this.txtWarehouseID_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtWarehouseID_ReadOnly.TabIndex = 49;
            // 
            // txtStatus_ReadOnly
            // 
            this.txtStatus_ReadOnly.Location = new System.Drawing.Point(616, 12);
            this.txtStatus_ReadOnly.Multiline = true;
            this.txtStatus_ReadOnly.Name = "txtStatus_ReadOnly";
            this.txtStatus_ReadOnly.ReadOnly = true;
            this.txtStatus_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtStatus_ReadOnly.TabIndex = 50;
            // 
            // btnExportQRCode
            // 
            this.btnExportQRCode.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExportQRCode.Location = new System.Drawing.Point(616, 72);
            this.btnExportQRCode.Name = "btnExportQRCode";
            this.btnExportQRCode.Size = new System.Drawing.Size(200, 31);
            this.btnExportQRCode.TabIndex = 51;
            this.btnExportQRCode.Text = "Xuất QR Code vị trí";
            this.btnExportQRCode.UseVisualStyleBackColor = true;
            this.btnExportQRCode.Click += new System.EventHandler(this.btnExportQRCode_Click);
            // 
            // btnMergeImage
            // 
            this.btnMergeImage.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnMergeImage.Location = new System.Drawing.Point(822, 72);
            this.btnMergeImage.Name = "btnMergeImage";
            this.btnMergeImage.Size = new System.Drawing.Size(135, 31);
            this.btnMergeImage.TabIndex = 52;
            this.btnMergeImage.Text = "Gom QR Code vị trí";
            this.btnMergeImage.UseVisualStyleBackColor = true;
            this.btnMergeImage.Visible = false;
            this.btnMergeImage.Click += new System.EventHandler(this.btnMergeImage_Click);
            // 
            // ZoneMaintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 653);
            this.Controls.Add(this.btnMergeImage);
            this.Controls.Add(this.btnExportQRCode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtZoneName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbZoneCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.cmbWarehouseID);
            this.Controls.Add(this.txtWarehouseID_ReadOnly);
            this.Controls.Add(this.txtStatus_ReadOnly);
            this.Name = "ZoneMaintView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsZoneTemperature)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tbpLine.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneLine)).EndInit();
            this.tbpLocation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneLocation)).EndInit();
            this.tbpProductCategory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneCategory)).EndInit();
            this.tbpTemperature.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgZoneTemperature)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbpProductCategory;
        private System.Windows.Forms.DataGridView dtgZoneCategory;
        private System.Windows.Forms.TabPage tbpLine;
        private System.Windows.Forms.DataGridView dtgZoneLine;
        private System.Windows.Forms.TabPage tbpLocation;
        private System.Windows.Forms.DataGridView dtgZoneLocation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private CustomControls.MultiColumnComboBox cmbZoneCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbStatus;
        private CustomControls.MultiColumnComboBox cmbWarehouseID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tbpTemperature;
        private System.Windows.Forms.Button btnZoneLine_Insert;
        private System.Windows.Forms.Button btnZoneLine_Delete;
        private System.Windows.Forms.BindingSource bdsZoneLine;
        private System.Windows.Forms.BindingSource bdsZoneLocation;
        private System.Windows.Forms.Button btnZoneLocation_Delete;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colZoneCategory_Check;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneCategory_CategoryDescription;
        private System.Windows.Forms.BindingSource bdsZoneCategory;
        private System.Windows.Forms.DataGridView dtgZoneTemperature;
        private System.Windows.Forms.BindingSource bdsZoneTemperature;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colZoneTemperature_Check;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneTemperature_ReferenceDescription;
        private System.Windows.Forms.TextBox txtZoneName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtWarehouseID_ReadOnly;
        private System.Windows.Forms.TextBox txtStatus_ReadOnly;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_LineCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_LineName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colZoneLine_Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_Length;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_Level;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_Order;
        private System.Windows.Forms.DataGridViewComboBoxColumn colZoneLine_Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLine_Description;
        private System.Windows.Forms.Button btnExportQRCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLocation_LocationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLocation_LineCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLocation_LevelID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLocation_LengthID;
        private System.Windows.Forms.DataGridViewComboBoxColumn colZoneLocation_Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneLocation_Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationCode;
        private System.Windows.Forms.Button btnMergeImage;
    }
}