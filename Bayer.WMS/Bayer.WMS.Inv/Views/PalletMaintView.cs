﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Inv.Views
{
    public partial class PalletMaintView : BaseForm, IPalletMaintView
    {
        public PalletMaintView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgPallet);
        }

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            try
            {
                var presenter = _presenter as IPalletMaintPresenter;
                await presenter.Search(PalletType, PalletCode);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnSearch.Enabled = true;
            }
        }

        private async void btnGenPallet_Click(object sender, EventArgs e)
        {
            btnGenPallet.Enabled = false;
            try
            {
                string type;
                int usageTime, qty;
                DateTime startDate, expiryDate;
                string fileName = String.Empty, note = String.Empty;
                using (var view = new GenPalletView())
                {
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    type = view.PalletType;
                    usageTime = view.UsageTime;
                    qty = view.Qty;
                    startDate = view.StartDate;
                    expiryDate = view.ExpiryDate;
                }

                await (_presenter as IPalletMaintPresenter).GeneratePallet(type, usageTime, startDate, expiryDate, qty);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnGenPallet.Enabled = true;
            }
        }

        public override void InitializeComboBox()
        {
            cmbType.ValueMember = Pallet.type.ValueMember;
            cmbType.DisplayMember = Pallet.type.DisplayMember;
            cmbType.DataSource = Pallet.type.GetWithAll();
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var palletPresenter = _presenter as IPalletMaintPresenter;

                await palletPresenter.LoadMainData();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public string PalletType => cmbType.SelectedValue == null ? String.Empty : cmbType.SelectedValue.ToString();

        public string PalletCode => txtPalletCode.Text;

        private System.Data.DataTable _pallets;

        public System.Data.DataTable Pallets
        {
            get { return _pallets; }
            set
            {
                _pallets = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsPallet.DataSource = _pallets;
                    });
                }
                else
                {
                    bdsPallet.DataSource = _pallets;
                }
            }
        }

        private IList<Pallet> _deletedPallets;

        public IList<Pallet> DeletedPallets
        {
            get { return _deletedPallets; }
            set { _deletedPallets = value; }
        }

        private void btnPrintPallet_Click(object sender, EventArgs e)
        {
            //Create directory to save file
            //1 for Vertical
            //2 for Horizontal
            //Foreach Line
            //Foreach 2 Zone Location
            List<string> seletedPallet = new List<string>();
            //seletedPallet.Add("PA071700001");
            //seletedPallet.Add("PA071700002");
            //seletedPallet.Add("PA071700003");
            //seletedPallet.Add("PA071700004");
            //seletedPallet.Add("PA071700005");
            //seletedPallet.Add("PA071700006");
            //seletedPallet.Add("PA071700007");
            //seletedPallet.Add("PA071700008");
            //seletedPallet.Add("PA071700009");
            //seletedPallet.Add("PA071700010");

            
            foreach (DataGridViewRow r in dtgPallet.SelectedRows)
            {
                seletedPallet.Add(dtgPallet[0, r.Index].Value.ToString());
            }

            foreach (DataGridViewCell r in dtgPallet.SelectedCells)
            {
                seletedPallet.Add(dtgPallet[0, r.RowIndex].Value.ToString());
            }

            seletedPallet = seletedPallet.Distinct().ToList();

            string savePath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Pallets";
            Utility.CreateDirectory(savePath);
            Process.Start(savePath);
            
            #region Create Excel here
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PalletPrint.xlsx";
            workbook = excelApp.Workbooks.Open(templatePath);
            sheetDetail = (Worksheet)workbook.ActiveSheet;
            #endregion

            var palletPresenter = _presenter as IPalletMaintPresenter;
            foreach (string currentPallet in seletedPallet)
            {
                string locationBarCode = "A1";
                CopyBarcodeToExcelCell(locationBarCode, sheetDetail,currentPallet);
                locationBarCode = "B1";
                CopyBarcodeToExcelCell(locationBarCode, sheetDetail, currentPallet);
                locationBarCode = "A4";
                CopyBarcodeToExcelCell(locationBarCode, sheetDetail, currentPallet);
                locationBarCode = "B4";
                CopyBarcodeToExcelCell(locationBarCode, sheetDetail, currentPallet);

                sheetDetail.Cells[2, 1] = currentPallet;
                sheetDetail.Cells[2, 2] = currentPallet;
                sheetDetail.Cells[5, 1] = currentPallet;
                sheetDetail.Cells[5, 2] = currentPallet;

                string palletType = string.Empty;
                if (currentPallet.Contains("PW"))
                {
                    palletType = @"Pallet GỖ";
                }
                else if (currentPallet.Contains("PF"))
                {
                    palletType = @"Pallet Nhựa Lỗ";
                }
                else if (currentPallet.Contains("PA"))
                {
                    palletType = @"Pallet Nhôm";
                }
                else if (currentPallet.Contains("PP"))
                {
                    palletType = @"Pallet Nhựa Phẳng";
                }

                sheetDetail.Cells[3, 1] = palletType;
                sheetDetail.Cells[3, 2] = palletType;
                sheetDetail.Cells[6, 1] = palletType;
                sheetDetail.Cells[6, 2] = palletType;


                #region Input to Excel here
                try
                {
                    #region Export range to picture
                    Microsoft.Office.Interop.Excel.Range r = sheetDetail.get_Range("A1", "B6");
                    r.CopyPicture(Microsoft.Office.Interop.Excel.XlPictureAppearance.xlScreen,
                                    Microsoft.Office.Interop.Excel.XlCopyPictureFormat.xlBitmap);

                    if (Clipboard.GetDataObject() != null)
                    {
                        IDataObject data = Clipboard.GetDataObject();

                        if (data.GetDataPresent(DataFormats.Bitmap))
                        {
                            Image image = (Image)data.GetData(DataFormats.Bitmap, true);
                            string bigImageVerticalName = savePath + "\\" + currentPallet + ".png";
                            image.Save(bigImageVerticalName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                    }
                    #endregion

                    #region Save info printed to data
                    palletPresenter.PrintPallet(currentPallet);
                    #endregion
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(Messages.Error_Common);
                }
                finally
                {

                }
                #endregion

                #region After Save, clean the template for next LenghID
                foreach (Microsoft.Office.Interop.Excel.Shape sh in sheetDetail.Shapes)
                {
                    sh.Delete();
                }
                for (int i = 1; i <= 9; i++)
                {
                    sheetDetail.Cells[2, i] = string.Empty;
                }
                #endregion
            }

            palletPresenter.LoadMainData();

            #region Clean Excel Object
            try
            {
                foreach (Process proc in System.Diagnostics.Process.GetProcessesByName("EXCEL"))
                {
                    proc.Kill();
                }

                workbook.Close();
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);

                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            #endregion
        }

        private void CopyBarcodeToExcelCell(string excelCell, _Worksheet sheetDetail, string locationCode)
        {
            object barcodeCell = excelCell;
            Microsoft.Office.Interop.Excel.Range rangeBitmap = sheetDetail.get_Range(barcodeCell, barcodeCell);
            Bitmap img = QRCoder.Utility.RenderQrCodeNoText(locationCode);
            Bitmap resizedImg = new Bitmap(250, 250);

            double ratioX = (double)resizedImg.Width / (double)img.Width;
            double ratioY = (double)resizedImg.Height / (double)img.Height;
            double ratio = ratioX < ratioY ? ratioX : ratioY;

            int newHeight = Convert.ToInt32(img.Height * ratio);
            int newWidth = Convert.ToInt32(img.Width * ratio);

            using (Graphics g = Graphics.FromImage(resizedImg))
            {
                g.DrawImage(img, 0, 0, newWidth, newHeight);
            }
            Clipboard.SetDataObject(resizedImg);
            //Paste the barcode image
            rangeBitmap.Select();
            //Interop params
            object oMissing = System.Reflection.Missing.Value;
            sheetDetail.Paste(oMissing, oMissing);
        }
    }
}
