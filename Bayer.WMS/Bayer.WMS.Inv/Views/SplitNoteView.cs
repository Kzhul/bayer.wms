﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class SplitNoteView : BaseForm, ISplitNoteView
    {
        public SplitNoteView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductPacking);
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "SplitCode";
            cmbCode.DisplayMember = "SplitCode - Description";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {

                new MultiColumnComboBox.ComboBoxColumn("StrDeliveryDate", "Ngày giao hàng", 130),
                new MultiColumnComboBox.ComboBoxColumn("SplitCode", "Mã", 130),                
                new MultiColumnComboBox.ComboBoxColumn("DOImportCode", "Mã DO", 130),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 130)
            };
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "GetUsers", "OnCodeSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);

            InitializeComboBox();

            var doPresenter = _presenter as ISplitNotePresenter;
            await Task.WhenAll(doPresenter.LoadSplitNoteHeader());

            if (!isRefresh)
                doPresenter.Insert();
        }

        public string DOImportCode
        {
            get
            {
                return cmbCode.SelectedItem["SplitCode"].ToString();
            }
        }

        public DataTable SplitNoteHeaders
        {
            set { cmbCode.Source = value; }
        }

        private SplitNoteHeader _SplitNoteHeader;

        public SplitNoteHeader SplitNoteHeader
        {
            get { return _SplitNoteHeader; }
            set
            {
                _SplitNoteHeader = value;

                cmbCode.DataBindings.Clear();
                txtProductDescription.DataBindings.Clear();
                txtStatusDisplay.DataBindings.Clear();
                txtDeliveryDate.DataBindings.Clear();
                txtDOImportCode.DataBindings.Clear();

                cmbCode.DataBindings.Add("Text", SplitNoteHeader, "SplitCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProductDescription.DataBindings.Add("Text", SplitNoteHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                txtStatusDisplay.DataBindings.Add("Text", SplitNoteHeader, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDeliveryDate.DataBindings.Add("Text", SplitNoteHeader, "StrDeliveryDate", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDOImportCode.DataBindings.Add("Text", SplitNoteHeader, "DOImportCode", true, DataSourceUpdateMode.OnPropertyChanged);

                txtStatusDisplay.ForeColor = Utility.StatusToColor(this.SplitNoteHeader.Status);
            }
        }

        private IList<SplitNoteDetail> _SplitNoteDetail;

        public IList<SplitNoteDetail> SplitNoteDetails
        {
            get { return _SplitNoteDetail; }
            set
            {
                _SplitNoteDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _SplitNoteDetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _SplitNoteDetail;
                }
            }
        }

        private IList<SplitNoteDetail> _deletedSplitNoteDetails;

        public IList<SplitNoteDetail> DeletedSplitNoteDetails
        {
            get { return _deletedSplitNoteDetails; }
            set { _deletedSplitNoteDetails = value; }
        }


        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }


        //protected BayerWMSContext _context;
        //protected SqlConnection _conn;

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool isSucess = false;
            string lastestNumber = string.Empty;
            try
            {
                using (BayerWMSContext db = new BayerWMSContext())
                {
                    if (this.SplitNoteDetails.Count == 0)
                    {
                        this.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                        return;
                    }

                    #region VerifyDetailQuantity
                    //Get by DO Code and CompanyCode
                    var listOrderDetail = db.DeliveryOrderDetails.Where(a => a.DOImportCode == this.SplitNoteHeader.DOImportCode).ToList();
                    //Get by DO Code and CompanyCode
                    //var listAnotherNoteDetails = db.SplitNoteDetails.Where(a => a.DOCode == this.SplitNoteHeader.DOImportCode && a.SplitCode != this.SplitNoteHeader.SplitCode).ToList();
                    var listAnotherNoteDetails = (from a in db.SplitNoteDetails
                                                  join b in db.SplitNoteHeaders
                                                  on new { a.DOCode, a.SplitCode } equals new { DOCode = b.DOImportCode, b.SplitCode }
                                                  where a.DOCode == this.SplitNoteHeader.DOImportCode
                                                  && b.SplitCode != this.SplitNoteHeader.SplitCode
                                                  select a).Distinct().ToList();

                    string productErr = string.Empty;
                    if (listOrderDetail != null && listOrderDetail.Count > 0)
                    {
                        var listMaterialCodeBatchCode = listOrderDetail.Select(a => new { a.ProductID, a.BatchCode, a.Delivery }).Distinct().ToList();
                        foreach (var item in listMaterialCodeBatchCode)
                        {
                            #region Total Split Quantity of this DO detail item
                            decimal totalNoteQuantity = 0;
                            decimal? totalAnotherNoteQuantity = listAnotherNoteDetails.Where(a =>
                                                                    a.ProductID == item.ProductID
                                                                    && a.BatchCode == item.BatchCode
                                                                    && a.DeliveryCode == item.Delivery
                                                                    && a.Quantity.HasValue
                                                                ).Sum(a => a.Quantity);
                            totalAnotherNoteQuantity = totalAnotherNoteQuantity ?? totalAnotherNoteQuantity;
                            decimal? totalThisNoteQuantity = this.SplitNoteDetails.Where(a =>
                                                                    a.ProductID == item.ProductID
                                                                    && a.BatchCode == item.BatchCode
                                                                    && a.DeliveryCode == item.Delivery
                                                                    && a.Quantity.HasValue
                                                                ).Sum(a => a.Quantity);
                            totalThisNoteQuantity = totalThisNoteQuantity ?? totalThisNoteQuantity;

                            totalNoteQuantity = totalThisNoteQuantity.Value + totalAnotherNoteQuantity.Value;
                            #endregion

                            decimal? totalOrderQuantity = listOrderDetail.Where(a =>
                                                                    a.ProductID == item.ProductID
                                                                    && a.BatchCode == item.BatchCode
                                                                    && a.Delivery == item.Delivery
                                                                    && a.Quantity.HasValue
                                                                ).Sum(a => a.Quantity);

                            totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                            if (totalNoteQuantity > totalOrderQuantity)
                            {
                                decimal? totalNoteQuantityValid = totalOrderQuantity - totalAnotherNoteQuantity;
                                var firstDetailErr = this.SplitNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).FirstOrDefault();
                                productErr = firstDetailErr.ProductCode + "----" + firstDetailErr.BatchCode + "----Đặt:" + totalOrderQuantity.ToString() + "----Khác:" + totalAnotherNoteQuantity.ToString() + "----Hiện tại:" + totalThisNoteQuantity.ToString();
                                //throw new WrappedException(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr);
                                this.SetMessage(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr, Utility.MessageType.Error);
                                break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(productErr))
                    {
                        return;
                    }
                    #endregion

                    #region Save DN Header and Details

                    //Insert                  
                    var modelUpdate = db.SplitNoteHeaders.Where(a => a.SplitCode == this.SplitNoteHeader.SplitCode).FirstOrDefault();

                    if (string.IsNullOrEmpty(this.SplitNoteHeader.SplitCode) || modelUpdate == null)
                    {
                        var model = db.SplitNoteHeaders.Where(a => a.SplitDate >= DateTime.Today.Date).OrderByDescending(a => a.SplitCode).FirstOrDefault();
                        int nextDONumber = 1;
                        if (model != null)
                        {
                            nextDONumber = int.Parse(model.SplitCode.Substring(model.SplitCode.Length - 2)) + 1;
                        }

                        lastestNumber = $"CH{DateTime.Today:yyMMdd}{nextDONumber:00}";

                        this.SplitNoteHeader.SplitCode = lastestNumber;
                        this.SplitNoteHeader.SplitDate = DateTime.Today;
                        this.SplitNoteHeader.Status = DeliveryNoteHeader.status.New;
                        this.SplitNoteHeader.IsDeleted = false;
                        this.SplitNoteHeader.CreatedBy = LoginInfo.UserID;
                        this.SplitNoteHeader.CreatedBySitemapID = LoginInfo.SitemapID;
                        this.SplitNoteHeader.CreatedDateTime = DateTime.Now;
                        this.SplitNoteHeader.UpdatedBy = LoginInfo.UserID;
                        this.SplitNoteHeader.UpdatedBySitemapID = LoginInfo.SitemapID;
                        this.SplitNoteHeader.UpdatedDateTime = DateTime.Now;

                        db.SplitNoteHeaders.Add(this.SplitNoteHeader);
                    }
                    //Update
                    else
                    {
                        //var modelUpdate = db.SplitNoteHeaders.Where(a => a.SplitCode == this.SplitNoteHeader.SplitCode).FirstOrDefault();
                        modelUpdate.Description = this.SplitNoteHeader.Description;
                        modelUpdate.DeliveryDate = this.SplitNoteHeader.DeliveryDate;
                        modelUpdate.UpdatedBy = LoginInfo.UserID;
                        modelUpdate.UpdatedBySitemapID = LoginInfo.SitemapID;
                        modelUpdate.UpdatedDateTime = DateTime.Now;
                    }


                    //Detail
                    ////Delete all and insert new
                    var listDetailOld = db.SplitNoteDetails.Where(a => a.SplitCode == this.SplitNoteHeader.SplitCode).ToList();
                    foreach (SplitNoteDetail item in listDetailOld)
                    {
                        db.SplitNoteDetails.Remove(item);
                    }

                    foreach (SplitNoteDetail item in this.SplitNoteDetails)
                    {
                        item.SplitCode = this.SplitNoteHeader.SplitCode;
                        item.Status = DeliveryNoteHeader.status.New;
                        item.CreatedBy = LoginInfo.UserID;
                        item.CreatedBySitemapID = LoginInfo.SitemapID;
                        item.CreatedDateTime = DateTime.Now;
                        item.UpdatedBy = LoginInfo.UserID;
                        item.UpdatedBySitemapID = LoginInfo.SitemapID;
                        item.UpdatedDateTime = DateTime.Now;
                        db.SplitNoteDetails.Add(item);
                    }

                    db.SaveChanges();
                }
                #endregion

                #region Update DO Status
                using (BayerWMSContext db = new BayerWMSContext())
                {
                    db.Database.ExecuteSqlCommand("dbo.proc_UpdateStatusAllDocument @Code, @DOImportCode, @Type, @Status"
                    , new System.Data.SqlClient.SqlParameter("Code", this.SplitNoteHeader.SplitCode)
                    , new System.Data.SqlClient.SqlParameter("DOImportCode", this.SplitNoteHeader.DOImportCode)
                    , new System.Data.SqlClient.SqlParameter("Type", "SplitNote")
                    , new System.Data.SqlClient.SqlParameter("Status", DeliveryNoteHeader.status.New)
                    );
                    db.SaveChanges();
                }

                isSucess = true;
                #endregion
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                //await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                //_SplitNoteHeaderRepository = _mainPresenter.Resolve(_SplitNoteHeaderRepository.GetType()) as ISplitNoteHeaderRepository;
                //_SplitNoteDetailRepository = _mainPresenter.Resolve(_SplitNoteDetailRepository.GetType()) as ISplitNoteDetailRepository;

                ////// if insert or update sucessfully, refresh combobox
                //if (isSuccess)
                //{
                //    await LoadSplitNoteHeader();

                //    _mainView.SplitNoteHeader = header;
                //    _mainView.SplitNoteDetails = details;
                //}
                if (isSucess) { MessageBox.Show("Lưu thành công "+ lastestNumber + " !"); this.Close(); }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public async void Print()
        {
            var doPresenter = _presenter as ISplitNotePresenter;
            await doPresenter.Print();
        }

        private async void btnConfirm_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as ISplitNotePresenter;
            await doPresenter.Confirm();
        }
    }
}
