﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class PalletTrackingView : BaseForm, IPalletTrackingView
    {
        public PalletTrackingView()
        {
            InitializeComponent();

            SetDoubleBuffered(dtgInfo);
            SetDoubleBuffered(dtgBarCode);
        }

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                    await (_presenter as IPalletTrackingPresenter).Search();
                
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error, Name);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error, Name);
            }
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);
        }

        private DataTable _PalletTrackingDetails;

        public DataTable PalletTrackingDetails
        {
            get { return _PalletTrackingDetails; }
            set
            {
                _PalletTrackingDetails = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsBarCode.DataSource = _PalletTrackingDetails;
                    });
                }
                else
                {
                    bdsBarCode.DataSource = _PalletTrackingDetails;
                }
            }
        }

        private DataTable _PalletTrackingInfo;

        public DataTable PalletTrackingInfo
        {
            get { return _PalletTrackingInfo; }
            set
            {
                _PalletTrackingInfo = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _PalletTrackingInfo;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _PalletTrackingInfo;
                }
            }
        }
    }
}
