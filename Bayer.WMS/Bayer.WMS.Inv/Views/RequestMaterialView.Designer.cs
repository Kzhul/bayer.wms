﻿namespace Bayer.WMS.Inv.Views
{
    partial class RequestMaterialView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RequestMaterialView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsRequestMaterial = new System.Windows.Forms.BindingSource(this.components);
            this.bdsRequestMaterialSplit = new System.Windows.Forms.BindingSource(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnImportLineSplit = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.tbpRequestMaterial_Detail = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dtgRequestMaterial = new System.Windows.Forms.DataGridView();
            this.colRequestMaterial_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequestMaterial_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequestMaterial_RequestQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBookedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPickedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequestMaterial_TransferedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequestMaterial_UOM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequestMaterial_Note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequestMaterial_Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnPrintExportPallet = new System.Windows.Forms.Button();
            this.btnConfirmExport = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnSuggest = new System.Windows.Forms.Button();
            this.dtgSplit = new System.Windows.Forms.DataGridView();
            this.colSplit_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplit_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplit_ProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplit_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colActualQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplit_Note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSplit_Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDriver = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDriverExportDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWarehouseKeeper = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWarehouseVerifyDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDriverToProduction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDriverToProductionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDocumentNbr = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnSearchSugestion = new System.Windows.Forms.Button();
            this.txtSugestionSearch = new System.Windows.Forms.TextBox();
            this.dtgPalletSuggest = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCodeSuggest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colZoneName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLineName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLengthID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLevelID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRequestMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRequestMaterialSplit)).BeginInit();
            this.tbpRequestMaterial_Detail.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgRequestMaterial)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgSplit)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPalletSuggest)).BeginInit();
            this.SuspendLayout();
            // 
            // btnUpload
            // 
            this.btnUpload.Image = ((System.Drawing.Image)(resources.GetObject("btnUpload.Image")));
            this.btnUpload.Location = new System.Drawing.Point(12, 6);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(50, 30);
            this.btnUpload.TabIndex = 0;
            this.btnUpload.TabStop = false;
            this.toolTip1.SetToolTip(this.btnUpload, "Import");
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnImportLineSplit
            // 
            this.btnImportLineSplit.Image = ((System.Drawing.Image)(resources.GetObject("btnImportLineSplit.Image")));
            this.btnImportLineSplit.Location = new System.Drawing.Point(653, 6);
            this.btnImportLineSplit.Name = "btnImportLineSplit";
            this.btnImportLineSplit.Size = new System.Drawing.Size(50, 30);
            this.btnImportLineSplit.TabIndex = 27;
            this.btnImportLineSplit.TabStop = false;
            this.toolTip1.SetToolTip(this.btnImportLineSplit, "Import");
            this.btnImportLineSplit.UseVisualStyleBackColor = true;
            this.btnImportLineSplit.Visible = false;
            this.btnImportLineSplit.Click += new System.EventHandler(this.btnImportLineSplit_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(621, 12);
            this.txtStatus.MaxLength = 255;
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(200, 24);
            this.txtStatus.TabIndex = 84;
            // 
            // tbpRequestMaterial_Detail
            // 
            this.tbpRequestMaterial_Detail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbpRequestMaterial_Detail.Controls.Add(this.tabPage1);
            this.tbpRequestMaterial_Detail.Controls.Add(this.tabPage3);
            this.tbpRequestMaterial_Detail.Controls.Add(this.tabPage2);
            this.tbpRequestMaterial_Detail.Location = new System.Drawing.Point(0, 100);
            this.tbpRequestMaterial_Detail.Name = "tbpRequestMaterial_Detail";
            this.tbpRequestMaterial_Detail.SelectedIndex = 0;
            this.tbpRequestMaterial_Detail.Size = new System.Drawing.Size(947, 516);
            this.tbpRequestMaterial_Detail.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dtgRequestMaterial);
            this.tabPage1.Controls.Add(this.btnUpload);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(939, 487);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách nguyên liệu yêu cầu";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dtgRequestMaterial
            // 
            this.dtgRequestMaterial.AllowUserToAddRows = false;
            this.dtgRequestMaterial.AllowUserToOrderColumns = true;
            this.dtgRequestMaterial.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgRequestMaterial.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgRequestMaterial.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgRequestMaterial.AutoGenerateColumns = false;
            this.dtgRequestMaterial.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgRequestMaterial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgRequestMaterial.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colRequestMaterial_ProductCode,
            this.colRequestMaterial_ProductDescription,
            this.colRequestMaterial_RequestQty,
            this.colBookedQty,
            this.colPickedQty,
            this.colRequestMaterial_TransferedQty,
            this.colRequestMaterial_UOM,
            this.colRequestMaterial_Note,
            this.colRequestMaterial_Status});
            this.dtgRequestMaterial.DataSource = this.bdsRequestMaterial;
            this.dtgRequestMaterial.GridColor = System.Drawing.SystemColors.Control;
            this.dtgRequestMaterial.Location = new System.Drawing.Point(12, 42);
            this.dtgRequestMaterial.Name = "dtgRequestMaterial";
            this.dtgRequestMaterial.ReadOnly = true;
            this.dtgRequestMaterial.Size = new System.Drawing.Size(919, 439);
            this.dtgRequestMaterial.TabIndex = 2;
            // 
            // colRequestMaterial_ProductCode
            // 
            this.colRequestMaterial_ProductCode.DataPropertyName = "ProductCode";
            this.colRequestMaterial_ProductCode.HeaderText = "Mã hàng";
            this.colRequestMaterial_ProductCode.Name = "colRequestMaterial_ProductCode";
            this.colRequestMaterial_ProductCode.ReadOnly = true;
            // 
            // colRequestMaterial_ProductDescription
            // 
            this.colRequestMaterial_ProductDescription.DataPropertyName = "ProductDescription";
            this.colRequestMaterial_ProductDescription.HeaderText = "Tên hàng";
            this.colRequestMaterial_ProductDescription.Name = "colRequestMaterial_ProductDescription";
            this.colRequestMaterial_ProductDescription.ReadOnly = true;
            this.colRequestMaterial_ProductDescription.Width = 300;
            // 
            // colRequestMaterial_RequestQty
            // 
            this.colRequestMaterial_RequestQty.DataPropertyName = "RequestQty";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            this.colRequestMaterial_RequestQty.DefaultCellStyle = dataGridViewCellStyle2;
            this.colRequestMaterial_RequestQty.HeaderText = "S.lượng yêu cầu";
            this.colRequestMaterial_RequestQty.Name = "colRequestMaterial_RequestQty";
            this.colRequestMaterial_RequestQty.ReadOnly = true;
            // 
            // colBookedQty
            // 
            this.colBookedQty.DataPropertyName = "BookedQty";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colBookedQty.DefaultCellStyle = dataGridViewCellStyle3;
            this.colBookedQty.HeaderText = "S.lượng thủ kho yêu cầu lấy";
            this.colBookedQty.Name = "colBookedQty";
            this.colBookedQty.ReadOnly = true;
            // 
            // colPickedQty
            // 
            this.colPickedQty.DataPropertyName = "PickedQty";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.colPickedQty.DefaultCellStyle = dataGridViewCellStyle4;
            this.colPickedQty.HeaderText = "S.lượng đã rớt hàng";
            this.colPickedQty.Name = "colPickedQty";
            this.colPickedQty.ReadOnly = true;
            // 
            // colRequestMaterial_TransferedQty
            // 
            this.colRequestMaterial_TransferedQty.DataPropertyName = "TransferedQty";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N0";
            this.colRequestMaterial_TransferedQty.DefaultCellStyle = dataGridViewCellStyle5;
            this.colRequestMaterial_TransferedQty.HeaderText = "S.lượng đã chuyển kho";
            this.colRequestMaterial_TransferedQty.Name = "colRequestMaterial_TransferedQty";
            this.colRequestMaterial_TransferedQty.ReadOnly = true;
            // 
            // colRequestMaterial_UOM
            // 
            this.colRequestMaterial_UOM.DataPropertyName = "UOM";
            this.colRequestMaterial_UOM.HeaderText = "ĐVT";
            this.colRequestMaterial_UOM.Name = "colRequestMaterial_UOM";
            this.colRequestMaterial_UOM.ReadOnly = true;
            this.colRequestMaterial_UOM.Width = 50;
            // 
            // colRequestMaterial_Note
            // 
            this.colRequestMaterial_Note.DataPropertyName = "Note";
            this.colRequestMaterial_Note.HeaderText = "Ghi chú";
            this.colRequestMaterial_Note.Name = "colRequestMaterial_Note";
            this.colRequestMaterial_Note.ReadOnly = true;
            this.colRequestMaterial_Note.Width = 300;
            // 
            // colRequestMaterial_Status
            // 
            this.colRequestMaterial_Status.DataPropertyName = "StatusDisplay";
            this.colRequestMaterial_Status.HeaderText = "Trạng thái";
            this.colRequestMaterial_Status.Name = "colRequestMaterial_Status";
            this.colRequestMaterial_Status.ReadOnly = true;
            this.colRequestMaterial_Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colRequestMaterial_Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnPrintExportPallet);
            this.tabPage2.Controls.Add(this.btnConfirmExport);
            this.tabPage2.Controls.Add(this.btnDownload);
            this.tabPage2.Controls.Add(this.btnSuggest);
            this.tabPage2.Controls.Add(this.btnImportLineSplit);
            this.tabPage2.Controls.Add(this.dtgSplit);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(939, 487);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Danh sách pallet rớt hàng, xuất hàng";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnPrintExportPallet
            // 
            this.btnPrintExportPallet.Location = new System.Drawing.Point(493, 6);
            this.btnPrintExportPallet.Name = "btnPrintExportPallet";
            this.btnPrintExportPallet.Size = new System.Drawing.Size(150, 30);
            this.btnPrintExportPallet.TabIndex = 33;
            this.btnPrintExportPallet.Text = "In phiếu rớt hàng";
            this.btnPrintExportPallet.UseVisualStyleBackColor = true;
            this.btnPrintExportPallet.Visible = false;
            this.btnPrintExportPallet.Click += new System.EventHandler(this.btnPrintExportPallet_Click);
            // 
            // btnConfirmExport
            // 
            this.btnConfirmExport.Location = new System.Drawing.Point(168, 6);
            this.btnConfirmExport.Name = "btnConfirmExport";
            this.btnConfirmExport.Size = new System.Drawing.Size(150, 30);
            this.btnConfirmExport.TabIndex = 32;
            this.btnConfirmExport.Text = "Thủ kho xác nhận";
            this.btnConfirmExport.UseVisualStyleBackColor = true;
            this.btnConfirmExport.Click += new System.EventHandler(this.btnConfirmExport_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownload.Location = new System.Drawing.Point(709, 6);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(108, 30);
            this.btnDownload.TabIndex = 31;
            this.btnDownload.TabStop = false;
            this.btnDownload.Text = "2.Download";
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Visible = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnSuggest
            // 
            this.btnSuggest.Location = new System.Drawing.Point(12, 6);
            this.btnSuggest.Name = "btnSuggest";
            this.btnSuggest.Size = new System.Drawing.Size(150, 30);
            this.btnSuggest.TabIndex = 28;
            this.btnSuggest.Text = "Tự động lấy hàng";
            this.btnSuggest.UseVisualStyleBackColor = true;
            this.btnSuggest.Click += new System.EventHandler(this.btnSuggest_Click);
            // 
            // dtgSplit
            // 
            this.dtgSplit.AllowUserToAddRows = false;
            this.dtgSplit.AllowUserToOrderColumns = true;
            this.dtgSplit.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Azure;
            this.dtgSplit.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dtgSplit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgSplit.AutoGenerateColumns = false;
            this.dtgSplit.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgSplit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgSplit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSplit_ProductCode,
            this.colSplit_ProductDescription,
            this.colSplit_ProductLot,
            this.colSplit_Quantity,
            this.colPalletCode,
            this.colActualQuantity,
            this.colSplit_Note,
            this.colSplit_Status,
            this.colLocationName,
            this.colDriver,
            this.colDriverExportDate,
            this.colWarehouseKeeper,
            this.colWarehouseVerifyDate,
            this.colDriverToProduction,
            this.colDriverToProductionDate});
            this.dtgSplit.DataSource = this.bdsRequestMaterialSplit;
            this.dtgSplit.GridColor = System.Drawing.SystemColors.Control;
            this.dtgSplit.Location = new System.Drawing.Point(11, 42);
            this.dtgSplit.Name = "dtgSplit";
            this.dtgSplit.ReadOnly = true;
            this.dtgSplit.Size = new System.Drawing.Size(920, 441);
            this.dtgSplit.TabIndex = 24;
            // 
            // colSplit_ProductCode
            // 
            this.colSplit_ProductCode.DataPropertyName = "ProductCode";
            this.colSplit_ProductCode.HeaderText = "Mã sản phẩm";
            this.colSplit_ProductCode.Name = "colSplit_ProductCode";
            this.colSplit_ProductCode.ReadOnly = true;
            // 
            // colSplit_ProductDescription
            // 
            this.colSplit_ProductDescription.DataPropertyName = "ProductDescription";
            this.colSplit_ProductDescription.HeaderText = "Mô tả";
            this.colSplit_ProductDescription.Name = "colSplit_ProductDescription";
            this.colSplit_ProductDescription.ReadOnly = true;
            this.colSplit_ProductDescription.Width = 300;
            // 
            // colSplit_ProductLot
            // 
            this.colSplit_ProductLot.DataPropertyName = "ProductLot";
            this.colSplit_ProductLot.HeaderText = "Số lô";
            this.colSplit_ProductLot.Name = "colSplit_ProductLot";
            this.colSplit_ProductLot.ReadOnly = true;
            // 
            // colSplit_Quantity
            // 
            this.colSplit_Quantity.DataPropertyName = "PalletQuantity";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "N0";
            this.colSplit_Quantity.DefaultCellStyle = dataGridViewCellStyle11;
            this.colSplit_Quantity.HeaderText = "Số lượng";
            this.colSplit_Quantity.Name = "colSplit_Quantity";
            this.colSplit_Quantity.ReadOnly = true;
            // 
            // colPalletCode
            // 
            this.colPalletCode.DataPropertyName = "PalletCode";
            this.colPalletCode.HeaderText = "Pallet";
            this.colPalletCode.Name = "colPalletCode";
            this.colPalletCode.ReadOnly = true;
            // 
            // colActualQuantity
            // 
            this.colActualQuantity.DataPropertyName = "ActualQuantity";
            this.colActualQuantity.HeaderText = "Số lượng thực xuất";
            this.colActualQuantity.Name = "colActualQuantity";
            this.colActualQuantity.ReadOnly = true;
            // 
            // colSplit_Note
            // 
            this.colSplit_Note.DataPropertyName = "Note";
            this.colSplit_Note.HeaderText = "Ghi chú";
            this.colSplit_Note.Name = "colSplit_Note";
            this.colSplit_Note.ReadOnly = true;
            this.colSplit_Note.Width = 300;
            // 
            // colSplit_Status
            // 
            this.colSplit_Status.DataPropertyName = "StatusDisplay";
            this.colSplit_Status.HeaderText = "Trạng thái";
            this.colSplit_Status.Name = "colSplit_Status";
            this.colSplit_Status.ReadOnly = true;
            this.colSplit_Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colSplit_Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colLocationName
            // 
            this.colLocationName.DataPropertyName = "LocationName";
            this.colLocationName.HeaderText = "Vị trí";
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.ReadOnly = true;
            // 
            // colDriver
            // 
            this.colDriver.DataPropertyName = "StrDriver";
            this.colDriver.HeaderText = "Tài xế rớt hàng";
            this.colDriver.Name = "colDriver";
            this.colDriver.ReadOnly = true;
            // 
            // colDriverExportDate
            // 
            this.colDriverExportDate.DataPropertyName = "StrDriverExportDate";
            this.colDriverExportDate.HeaderText = "Thời gian rớt hàng";
            this.colDriverExportDate.Name = "colDriverExportDate";
            this.colDriverExportDate.ReadOnly = true;
            // 
            // colWarehouseKeeper
            // 
            this.colWarehouseKeeper.DataPropertyName = "StrWarehouseKeeper";
            this.colWarehouseKeeper.HeaderText = "Thủ kho";
            this.colWarehouseKeeper.Name = "colWarehouseKeeper";
            this.colWarehouseKeeper.ReadOnly = true;
            // 
            // colWarehouseVerifyDate
            // 
            this.colWarehouseVerifyDate.DataPropertyName = "StrWarehouseVerifyDate";
            this.colWarehouseVerifyDate.HeaderText = "Thời gian xác nhận";
            this.colWarehouseVerifyDate.Name = "colWarehouseVerifyDate";
            this.colWarehouseVerifyDate.ReadOnly = true;
            // 
            // colDriverToProduction
            // 
            this.colDriverToProduction.DataPropertyName = "StrDriverToProduction";
            this.colDriverToProduction.HeaderText = "Tài xế chuyển hàng";
            this.colDriverToProduction.Name = "colDriverToProduction";
            this.colDriverToProduction.ReadOnly = true;
            // 
            // colDriverToProductionDate
            // 
            this.colDriverToProductionDate.DataPropertyName = "StrDriverToProductionDate";
            this.colDriverToProductionDate.HeaderText = "Thời gian chuyển hàng";
            this.colDriverToProductionDate.Name = "colDriverToProductionDate";
            this.colDriverToProductionDate.ReadOnly = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(543, 15);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 40;
            this.label5.Text = "Trạng thái:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 77);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 21;
            this.label3.Text = "Ngày:";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd/MM/yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(88, 72);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(110, 22);
            this.dtpDate.TabIndex = 2;
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(88, 42);
            this.txtNote.MaxLength = 255;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(400, 24);
            this.txtNote.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 16);
            this.label2.TabIndex = 19;
            this.label2.Text = "Ghi chú:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 17;
            this.label1.Text = "Mã phiếu:";
            // 
            // cmbDocumentNbr
            // 
            this.cmbDocumentNbr.Columns = null;
            this.cmbDocumentNbr.DropDownHeight = 1;
            this.cmbDocumentNbr.DropDownWidth = 500;
            this.cmbDocumentNbr.FormattingEnabled = true;
            this.cmbDocumentNbr.IntegralHeight = false;
            this.cmbDocumentNbr.Location = new System.Drawing.Point(88, 12);
            this.cmbDocumentNbr.MaxLength = 255;
            this.cmbDocumentNbr.Name = "cmbDocumentNbr";
            this.cmbDocumentNbr.PageSize = 0;
            this.cmbDocumentNbr.PresenterInfo = null;
            this.cmbDocumentNbr.Size = new System.Drawing.Size(400, 24);
            this.cmbDocumentNbr.Source = null;
            this.cmbDocumentNbr.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnSearchSugestion);
            this.tabPage3.Controls.Add(this.txtSugestionSearch);
            this.tabPage3.Controls.Add(this.dtgPalletSuggest);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(939, 487);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Danh sách vị trí gợi ý";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnSearchSugestion
            // 
            this.btnSearchSugestion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchSugestion.Location = new System.Drawing.Point(208, -1);
            this.btnSearchSugestion.Name = "btnSearchSugestion";
            this.btnSearchSugestion.Size = new System.Drawing.Size(217, 30);
            this.btnSearchSugestion.TabIndex = 96;
            this.btnSearchSugestion.TabStop = false;
            this.btnSearchSugestion.Text = "Tìm kiếm";
            this.btnSearchSugestion.UseVisualStyleBackColor = true;
            // 
            // txtSugestionSearch
            // 
            this.txtSugestionSearch.Location = new System.Drawing.Point(3, 3);
            this.txtSugestionSearch.MaxLength = 255;
            this.txtSugestionSearch.Name = "txtSugestionSearch";
            this.txtSugestionSearch.Size = new System.Drawing.Size(199, 22);
            this.txtSugestionSearch.TabIndex = 95;
            // 
            // dtgPalletSuggest
            // 
            this.dtgPalletSuggest.AllowUserToAddRows = false;
            this.dtgPalletSuggest.AllowUserToDeleteRows = false;
            this.dtgPalletSuggest.AllowUserToOrderColumns = true;
            this.dtgPalletSuggest.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Azure;
            this.dtgPalletSuggest.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dtgPalletSuggest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPalletSuggest.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPalletSuggest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPalletSuggest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.colPalletCodeSuggest,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn5,
            this.colZoneName,
            this.colLineName,
            this.colLengthID,
            this.colLevelID,
            this.colLocationCode});
            this.dtgPalletSuggest.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPalletSuggest.Location = new System.Drawing.Point(3, 31);
            this.dtgPalletSuggest.Name = "dtgPalletSuggest";
            this.dtgPalletSuggest.ReadOnly = true;
            this.dtgPalletSuggest.Size = new System.Drawing.Size(933, 453);
            this.dtgPalletSuggest.TabIndex = 94;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DeliveryDate";
            dataGridViewCellStyle7.Format = "dd/MM/yyyy";
            dataGridViewCellStyle7.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn1.HeaderText = "Ngày giao hàng";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ProductCode";
            this.dataGridViewTextBoxColumn2.HeaderText = "Mã sản phẩm";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ProductName";
            this.dataGridViewTextBoxColumn3.HeaderText = "Tên sản phẩm";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 200;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ProductLot";
            this.dataGridViewTextBoxColumn4.HeaderText = "Số lô";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // colPalletCodeSuggest
            // 
            this.colPalletCodeSuggest.DataPropertyName = "PalletCode";
            this.colPalletCodeSuggest.HeaderText = "Mã pallet gợi ý";
            this.colPalletCodeSuggest.Name = "colPalletCodeSuggest";
            this.colPalletCodeSuggest.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "DOQuantity";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N0";
            dataGridViewCellStyle8.NullValue = null;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn10.HeaderText = "Số lượng DO yêu cầu";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "PalletQuantity";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.Format = "N0";
            dataGridViewCellStyle9.NullValue = null;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn11.HeaderText = "Số lượng pallet hiện có";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "LocationName";
            this.dataGridViewTextBoxColumn5.HeaderText = "Tên vị trí";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // colZoneName
            // 
            this.colZoneName.DataPropertyName = "ZoneName";
            this.colZoneName.HeaderText = "Khu vực";
            this.colZoneName.Name = "colZoneName";
            this.colZoneName.ReadOnly = true;
            // 
            // colLineName
            // 
            this.colLineName.DataPropertyName = "LineName";
            this.colLineName.HeaderText = "Dãy";
            this.colLineName.Name = "colLineName";
            this.colLineName.ReadOnly = true;
            // 
            // colLengthID
            // 
            this.colLengthID.DataPropertyName = "LengthID";
            this.colLengthID.HeaderText = "Kệ";
            this.colLengthID.Name = "colLengthID";
            this.colLengthID.ReadOnly = true;
            // 
            // colLevelID
            // 
            this.colLevelID.DataPropertyName = "LevelID";
            this.colLevelID.HeaderText = "Tầng";
            this.colLevelID.Name = "colLevelID";
            this.colLevelID.ReadOnly = true;
            // 
            // colLocationCode
            // 
            this.colLocationCode.DataPropertyName = "LocationCode";
            this.colLocationCode.HeaderText = "Mã vị trí gợi ý";
            this.colLocationCode.Name = "colLocationCode";
            this.colLocationCode.ReadOnly = true;
            // 
            // RequestMaterialView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 628);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.tbpRequestMaterial_Detail);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbDocumentNbr);
            this.Name = "RequestMaterialView";
            this.Text = "RequestMaterialView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsRequestMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRequestMaterialSplit)).EndInit();
            this.tbpRequestMaterial_Detail.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgRequestMaterial)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgSplit)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPalletSuggest)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CustomControls.MultiColumnComboBox cmbDocumentNbr;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.DataGridView dtgRequestMaterial;
        private System.Windows.Forms.BindingSource bdsRequestMaterial;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tbpRequestMaterial_Detail;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dtgSplit;
        private System.Windows.Forms.BindingSource bdsRequestMaterialSplit;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Button btnSuggest;
        private System.Windows.Forms.Button btnImportLineSplit;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnConfirmExport;
        private System.Windows.Forms.Button btnPrintExportPallet;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestMaterial_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestMaterial_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestMaterial_RequestQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBookedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPickedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestMaterial_TransferedQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestMaterial_UOM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestMaterial_Note;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestMaterial_Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplit_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplit_ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplit_ProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplit_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colActualQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplit_Note;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSplit_Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDriver;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDriverExportDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWarehouseKeeper;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWarehouseVerifyDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDriverToProduction;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDriverToProductionDate;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnSearchSugestion;
        private System.Windows.Forms.TextBox txtSugestionSearch;
        private System.Windows.Forms.DataGridView dtgPalletSuggest;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCodeSuggest;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn colZoneName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLineName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLengthID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLevelID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationCode;
    }
}