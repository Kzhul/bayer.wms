﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class ProductReturnView : BaseForm, IProductReturnView
    {
        public ProductReturnView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgDetailSplit);
            SetDoubleBuffered(dtgDetail);
        }

        private async void btnConfirm_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IProductReturnPresenter;
            await doPresenter.Confirm();
        }

        private async void btnDetail_Upload_Click(object sender, EventArgs e)
        {
            btnDetail_Upload.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        await (_presenter as IProductReturnPresenter).ImportExcel(ofd.FileName);
                    }
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnDetail_Upload.Enabled = true;
            }
        }

        private void btnDetail_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtgDetail.SelectedRows)
                {
                    var detail = bdsDetails[row.Index] as ProductReturnDetail;
                    bdsDetails.Remove(detail);
                    (_presenter as IProductReturnPresenter).DeleteDetail(detail);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "ProductReturnCode";
            cmbCode.DisplayMember = "ProductReturnCode";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("ProductReturnCode", "Mã", 100),
                new MultiColumnComboBox.ComboBoxColumn("StrDate", "Ngày giao hàng", 120),
                new MultiColumnComboBox.ComboBoxColumn("CompanyName", "Công ty", 120),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Ghi chú", 200)
            };
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadProductReturnHeader", "OnCodeSelectChange");
            cmbCode.DropDownWidth = 800;
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);
            InitializeComboBox();
            var doPresenter = _presenter as IProductReturnPresenter;
            await Task.WhenAll(doPresenter.LoadProductReturnHeaders());
            if (!isRefresh)
                doPresenter.Insert();
        }

        public string ProductReturnCode
        {
            get
            {
                return cmbCode.SelectedItem["ProductReturnCode"].ToString();
            }
        }

        public DataTable ProductReturnHeaders
        {
            set { cmbCode.Source = value; }
        }

        private ProductReturnHeader _stockReturnHeader;

        public ProductReturnHeader ProductReturnHeader
        {
            get => _stockReturnHeader;
            set
            {
                _stockReturnHeader = value;
                cmbCode.DataBindings.Clear();
                txtDescription.DataBindings.Clear();
                txtCreatedBy.DataBindings.Clear();
                dtpDeliveryDate.DataBindings.Clear();
                txtImportStatus.DataBindings.Clear();
                txtProvince.DataBindings.Clear();
                txtCompany.DataBindings.Clear();

                cmbCode.DataBindings.Add("Text", ProductReturnHeader, "ProductReturnCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", ProductReturnHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCreatedBy.DataBindings.Add("Text", ProductReturnHeader, "CreatedByName", true, DataSourceUpdateMode.OnPropertyChanged);
                dtpDeliveryDate.DataBindings.Add("Value", ProductReturnHeader, "Date", true, DataSourceUpdateMode.OnPropertyChanged);
                txtImportStatus.DataBindings.Add("Text", ProductReturnHeader, "StatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtProvince.DataBindings.Add("Text", ProductReturnHeader, "Province", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCompany.Text = _stockReturnHeader.CompanyCode + " - " + _stockReturnHeader.CompanyName;
            }
        }

        private IList<ProductReturnDetail> _stockReturnDetail;

        public IList<ProductReturnDetail> ProductReturnDetails
        {
            get => _stockReturnDetail;
            set
            {
                _stockReturnDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _stockReturnDetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _stockReturnDetail;
                }
            }
        }

        private DataTable _productReturnDetailSplits;

        public DataTable ProductReturnDetailSplits
        {
            get => _productReturnDetailSplits;
            set
            {
                _productReturnDetailSplits = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetailSplits.DataSource = _productReturnDetailSplits;
                    });
                }
                else
                {
                    bdsDetailSplits.DataSource = _productReturnDetailSplits;
                }
            }
        }

        private IList<ProductReturnDetail> _deletedProductReturnDetails;

        public IList<ProductReturnDetail> DeletedProductReturnDetails
        {
            get { return _deletedProductReturnDetails; }
            set { _deletedProductReturnDetails = value; }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }

        public async void Print()
        {
            bool validate = true;
            if (validate)
            {
                var doPresenter = _presenter as IProductReturnPresenter;
                await doPresenter.Print();
            }
        }

        private async void btnDownload_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IProductReturnPresenter;
            await doPresenter.Export();
        }

        private void ProductReturnView_Load(object sender, EventArgs e)
        {

        }
    }
}