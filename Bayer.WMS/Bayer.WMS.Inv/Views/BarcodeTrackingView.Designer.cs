﻿namespace Bayer.WMS.Inv.Views
{
    partial class BarcodeTrackingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BarcodeTrackingView));
            this.dtgInfo = new System.Windows.Forms.DataGridView();
            this.colInfoName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInfoValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.dtgBarCode = new System.Windows.Forms.DataGridView();
            this.colLocationCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPalletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCartonBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEncryptedProductBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsBarCode = new System.Windows.Forms.BindingSource(this.components);
            this.btnSearch = new System.Windows.Forms.Button();
            this.rdbPallet = new System.Windows.Forms.RadioButton();
            this.rdbCarton = new System.Windows.Forms.RadioButton();
            this.rdbProduct = new System.Windows.Forms.RadioButton();
            this.rdbProductLot = new System.Windows.Forms.RadioButton();
            this.rdbAll = new System.Windows.Forms.RadioButton();
            this.rdbProductCode = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dtgInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgBarCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsBarCode)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgInfo
            // 
            this.dtgInfo.AllowUserToAddRows = false;
            this.dtgInfo.AllowUserToDeleteRows = false;
            this.dtgInfo.AllowUserToOrderColumns = true;
            this.dtgInfo.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgInfo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgInfo.AutoGenerateColumns = false;
            this.dtgInfo.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colInfoName,
            this.colInfoValue});
            this.dtgInfo.DataSource = this.bdsDetails;
            this.dtgInfo.GridColor = System.Drawing.SystemColors.Control;
            this.dtgInfo.Location = new System.Drawing.Point(580, 41);
            this.dtgInfo.Name = "dtgInfo";
            this.dtgInfo.ReadOnly = true;
            this.dtgInfo.Size = new System.Drawing.Size(406, 600);
            this.dtgInfo.TabIndex = 8;
            // 
            // colInfoName
            // 
            this.colInfoName.DataPropertyName = "InfoName";
            this.colInfoName.HeaderText = "Thông tin";
            this.colInfoName.Name = "colInfoName";
            this.colInfoName.ReadOnly = true;
            this.colInfoName.Width = 200;
            // 
            // colInfoValue
            // 
            this.colInfoValue.DataPropertyName = "InfoValue";
            dataGridViewCellStyle2.NullValue = null;
            this.colInfoValue.DefaultCellStyle = dataGridViewCellStyle2;
            this.colInfoValue.HeaderText = "Giá trị";
            this.colInfoValue.Name = "colInfoValue";
            this.colInfoValue.ReadOnly = true;
            this.colInfoValue.Width = 150;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mã:";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(70, 12);
            this.txtCode.MaxLength = 255;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(200, 22);
            this.txtCode.TabIndex = 36;
            // 
            // dtgBarCode
            // 
            this.dtgBarCode.AllowUserToAddRows = false;
            this.dtgBarCode.AllowUserToDeleteRows = false;
            this.dtgBarCode.AllowUserToOrderColumns = true;
            this.dtgBarCode.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Azure;
            this.dtgBarCode.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgBarCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgBarCode.AutoGenerateColumns = false;
            this.dtgBarCode.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgBarCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgBarCode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLocationCode,
            this.colPalletCode,
            this.colCartonBarcode,
            this.colProductBarcode,
            this.colEncryptedProductBarcode,
            this.colProductLot,
            this.colProductCode,
            this.colDescription});
            this.dtgBarCode.DataSource = this.bdsBarCode;
            this.dtgBarCode.GridColor = System.Drawing.SystemColors.Control;
            this.dtgBarCode.Location = new System.Drawing.Point(12, 40);
            this.dtgBarCode.Name = "dtgBarCode";
            this.dtgBarCode.ReadOnly = true;
            this.dtgBarCode.Size = new System.Drawing.Size(562, 601);
            this.dtgBarCode.TabIndex = 43;
            // 
            // colLocationCode
            // 
            this.colLocationCode.DataPropertyName = "LocationCode";
            this.colLocationCode.HeaderText = "Mã vị trí";
            this.colLocationCode.Name = "colLocationCode";
            this.colLocationCode.ReadOnly = true;
            // 
            // colPalletCode
            // 
            this.colPalletCode.DataPropertyName = "PalletCode";
            this.colPalletCode.HeaderText = "Mã Pallet";
            this.colPalletCode.Name = "colPalletCode";
            this.colPalletCode.ReadOnly = true;
            // 
            // colCartonBarcode
            // 
            this.colCartonBarcode.DataPropertyName = "CartonBarcode";
            dataGridViewCellStyle4.NullValue = null;
            this.colCartonBarcode.DefaultCellStyle = dataGridViewCellStyle4;
            this.colCartonBarcode.HeaderText = "Mã Thùng";
            this.colCartonBarcode.Name = "colCartonBarcode";
            this.colCartonBarcode.ReadOnly = true;
            this.colCartonBarcode.Width = 120;
            // 
            // colProductBarcode
            // 
            this.colProductBarcode.DataPropertyName = "ProductBarcode";
            this.colProductBarcode.HeaderText = "Barcode sản phẩm";
            this.colProductBarcode.Name = "colProductBarcode";
            this.colProductBarcode.ReadOnly = true;
            this.colProductBarcode.Width = 150;
            // 
            // colEncryptedProductBarcode
            // 
            this.colEncryptedProductBarcode.DataPropertyName = "EncryptedProductBarcode";
            this.colEncryptedProductBarcode.HeaderText = "Barcode sản phẩm mã hóa";
            this.colEncryptedProductBarcode.Name = "colEncryptedProductBarcode";
            this.colEncryptedProductBarcode.ReadOnly = true;
            this.colEncryptedProductBarcode.Width = 150;
            // 
            // colProductLot
            // 
            this.colProductLot.DataPropertyName = "ProductLot";
            this.colProductLot.HeaderText = "Số lô";
            this.colProductLot.Name = "colProductLot";
            this.colProductLot.ReadOnly = true;
            // 
            // colProductCode
            // 
            this.colProductCode.DataPropertyName = "ProductCode";
            this.colProductCode.HeaderText = "Mã sản phẩm";
            this.colProductCode.Name = "colProductCode";
            this.colProductCode.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "ProductDescription";
            this.colDescription.HeaderText = "Tên sản phẩm";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            this.colDescription.Width = 200;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(719, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(122, 30);
            this.btnSearch.TabIndex = 44;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // rdbPallet
            // 
            this.rdbPallet.AutoSize = true;
            this.rdbPallet.Location = new System.Drawing.Point(346, 11);
            this.rdbPallet.Name = "rdbPallet";
            this.rdbPallet.Size = new System.Drawing.Size(60, 20);
            this.rdbPallet.TabIndex = 45;
            this.rdbPallet.Text = "Pallet";
            this.rdbPallet.UseVisualStyleBackColor = true;
            // 
            // rdbCarton
            // 
            this.rdbCarton.AutoSize = true;
            this.rdbCarton.Location = new System.Drawing.Point(412, 11);
            this.rdbCarton.Name = "rdbCarton";
            this.rdbCarton.Size = new System.Drawing.Size(64, 20);
            this.rdbCarton.TabIndex = 46;
            this.rdbCarton.Text = "Thùng";
            this.rdbCarton.UseVisualStyleBackColor = true;
            // 
            // rdbProduct
            // 
            this.rdbProduct.AutoSize = true;
            this.rdbProduct.Location = new System.Drawing.Point(478, 11);
            this.rdbProduct.Name = "rdbProduct";
            this.rdbProduct.Size = new System.Drawing.Size(87, 20);
            this.rdbProduct.TabIndex = 47;
            this.rdbProduct.Text = "Sản phẩm";
            this.rdbProduct.UseVisualStyleBackColor = true;
            // 
            // rdbProductLot
            // 
            this.rdbProductLot.AutoSize = true;
            this.rdbProductLot.Location = new System.Drawing.Point(571, 11);
            this.rdbProductLot.Name = "rdbProductLot";
            this.rdbProductLot.Size = new System.Drawing.Size(61, 20);
            this.rdbProductLot.TabIndex = 48;
            this.rdbProductLot.Text = "Số Lô";
            this.rdbProductLot.UseVisualStyleBackColor = true;
            // 
            // rdbAll
            // 
            this.rdbAll.AutoSize = true;
            this.rdbAll.Checked = true;
            this.rdbAll.Location = new System.Drawing.Point(276, 11);
            this.rdbAll.Name = "rdbAll";
            this.rdbAll.Size = new System.Drawing.Size(64, 20);
            this.rdbAll.TabIndex = 49;
            this.rdbAll.TabStop = true;
            this.rdbAll.Text = "Tất cả";
            this.rdbAll.UseVisualStyleBackColor = true;
            // 
            // rdbProductCode
            // 
            this.rdbProductCode.AutoSize = true;
            this.rdbProductCode.Location = new System.Drawing.Point(638, 11);
            this.rdbProductCode.Name = "rdbProductCode";
            this.rdbProductCode.Size = new System.Drawing.Size(75, 20);
            this.rdbProductCode.TabIndex = 50;
            this.rdbProductCode.Text = "Tồn kho";
            this.rdbProductCode.UseVisualStyleBackColor = true;
            // 
            // BarcodeTrackingView
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 653);
            this.Controls.Add(this.rdbProductCode);
            this.Controls.Add(this.rdbAll);
            this.Controls.Add(this.rdbProductLot);
            this.Controls.Add(this.rdbProduct);
            this.Controls.Add(this.rdbCarton);
            this.Controls.Add(this.rdbPallet);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.dtgBarCode);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "BarcodeTrackingView";
            this.Text = "ProductPackingMaintView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BarcodeTrackingView_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dtgInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgBarCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsBarCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgInfo;
        private System.Windows.Forms.BindingSource bdsDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.DataGridView dtgBarCode;
        private System.Windows.Forms.BindingSource bdsBarCode;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInfoName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInfoValue;
        private System.Windows.Forms.RadioButton rdbPallet;
        private System.Windows.Forms.RadioButton rdbCarton;
        private System.Windows.Forms.RadioButton rdbProduct;
        private System.Windows.Forms.RadioButton rdbProductLot;
        private System.Windows.Forms.RadioButton rdbAll;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEncryptedProductBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.RadioButton rdbProductCode;
    }
}