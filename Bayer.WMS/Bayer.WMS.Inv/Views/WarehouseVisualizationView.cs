﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.CustomControls;

namespace Bayer.WMS.Inv.Views
{
    public partial class WarehouseVisualizationView : BaseForm, IWarehouseVisualizationView
    {
        private Dictionary<string, WarehouseZone> _zoneVisualizations;

        private void cmbLevel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private WarehouseZone DrawZone(Zone zone, int level)
        {
            var lines = _zoneLines.Where(p => p.ZoneCode == zone.ZoneCode);
            var locations = _zoneLocations.Where(p => p.ZoneCode == zone.ZoneCode && p.LevelID == level);

            var zoneV = new WarehouseZone();
            zoneV.Name = zone.ZoneCode;
            zoneV.ZoneName = zone.ZoneName;
            zoneV.Draggable(true);

            int rowCount = _zoneLocations.Max(p => p.LengthID);

            //// Draw length of line            
            zoneV.DrawLength(rowCount);

            //// Draw line & path
            int index = 0;
            foreach (var line in lines.OrderBy(p => p.Order))
            {
                if (line.Type == ZoneLine.type.Line)
                    zoneV.DrawLine(index);
                else if (line.Type == ZoneLine.type.Path)
                    zoneV.DrawPath(index);

                index++;
            }

            zoneV.Refresh();

            foreach (var line in lines.OrderBy(p => p.Order))
            {
                if (line.Type == ZoneLine.type.Path)
                    continue;

                for (int row = 1; row <= rowCount; row++)
                {
                    var loc = locations.FirstOrDefault(p => p.LineCode == line.LineCode && p.LengthID == row);
                    if (loc != null)
                        zoneV.DrawLocation(loc.LocationCode, line.Order, row);
                }
            }

            panel1.Controls.Add(zoneV);

            var visualization = _warehouseVisualizations.FirstOrDefault(p => p.Control == zone.ZoneCode);

            if (visualization != null)
                zoneV.Location = new Point((int)visualization.X, (int)visualization.Y);

            return zoneV;
        }

        public WarehouseVisualizationView()
        {
            InitializeComponent();
            _zoneVisualizations = new Dictionary<string, WarehouseZone>();
        }

        public override void InitializeComboBox()
        {
            cmbWarehouseID.DropDownWidth = 700;
            cmbWarehouseID.PageSize = 20;
            cmbWarehouseID.ValueMember = "WarehouseID";
            cmbWarehouseID.DisplayMember = "WarehouseCode - Description";
            cmbWarehouseID.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("WarehouseCode", "Mã kho", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbWarehouseID.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadWarehouses", "OnWarehouseCodeSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var warehouseVisualizationPresenter = _presenter as IWarehouseVisualizationPresenter;

                await Task.WhenAll(warehouseVisualizationPresenter.LoadWarehouseVisualizations(), warehouseVisualizationPresenter.LoadWarehouses());

                //if (!isRefresh)
                //    userMaintPresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override Task Save()
        {
            return base.Save();
        }

        public DataTable Warehouses { set => cmbWarehouseID.Source = value; }

        private Warehouse _warehouse;

        public Warehouse Warehouse { get => _warehouse; set => _warehouse = value; }

        private IList<Zone> _zones;

        public IList<Zone> Zones
        {
            get => _zones;
            set
            {
                _zones = value;

                foreach (var zone in _zoneVisualizations)
                {
                    panel1.Controls.RemoveByKey(zone.Key);
                }

                int maxLevel = _zoneLines.Max(p => p.Level ?? 0);
                cmbLevel.SelectedIndexChanged -= cmbLevel_SelectedIndexChanged;
                cmbLevel.Items.Clear();
                for (int i = 1; i <= maxLevel; i++)
                {
                    cmbLevel.Items.Add(i);
                }
                cmbLevel.SelectedItem = 1;
                cmbLevel.SelectedIndexChanged += cmbLevel_SelectedIndexChanged;

                foreach (var zone in _zones)
                {
                    var zoneV = DrawZone(zone, 1);

                    _zoneVisualizations.Add(zone.ZoneCode, zoneV);
                }
            }
        }

        private IList<ZoneLine> _zoneLines;

        public IList<ZoneLine> ZoneLines { get => _zoneLines; set => _zoneLines = value; }

        private IList<ZoneLocation> _zoneLocations;

        public IList<ZoneLocation> ZoneLocations { get => _zoneLocations; set => _zoneLocations = value; }

        private IList<WarehouseVisualization> _warehouseVisualizations;

        public IList<WarehouseVisualization> WarehouseVisualizations
        {
            get
            {
                var warehouseVisualizations = new List<WarehouseVisualization>();

                foreach (var item in _zoneVisualizations)
                {
                    warehouseVisualizations.Add(new WarehouseVisualization
                    {
                        UserID = LoginInfo.UserID,
                        Control = item.Value.Name,
                        X = item.Value.Location.X,
                        Y = item.Value.Location.Y,
                    });
                }

                return warehouseVisualizations;
            }
            set => _warehouseVisualizations = value;
        }
    }
}
