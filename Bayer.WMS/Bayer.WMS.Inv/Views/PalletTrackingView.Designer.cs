﻿namespace Bayer.WMS.Inv.Views
{
    partial class PalletTrackingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PalletTrackingView));
            this.dtgInfo = new System.Windows.Forms.DataGridView();
            this.colInfoName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInfoValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsDetails = new System.Windows.Forms.BindingSource(this.components);
            this.dtgBarCode = new System.Windows.Forms.DataGridView();
            this.colPalletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCartonBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWarehouseKeeperName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWarehouseVerifyDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDriverName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationPutDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLocationSuggestion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colManufacturingDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colExpiryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsBarCode = new System.Windows.Forms.BindingSource(this.components);
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnPrintExportPallet = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgBarCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsBarCode)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgInfo
            // 
            this.dtgInfo.AllowUserToAddRows = false;
            this.dtgInfo.AllowUserToDeleteRows = false;
            this.dtgInfo.AllowUserToOrderColumns = true;
            this.dtgInfo.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Azure;
            this.dtgInfo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dtgInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgInfo.AutoGenerateColumns = false;
            this.dtgInfo.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colInfoName,
            this.colInfoValue});
            this.dtgInfo.DataSource = this.bdsDetails;
            this.dtgInfo.GridColor = System.Drawing.SystemColors.Control;
            this.dtgInfo.Location = new System.Drawing.Point(649, 40);
            this.dtgInfo.Name = "dtgInfo";
            this.dtgInfo.ReadOnly = true;
            this.dtgInfo.Size = new System.Drawing.Size(337, 601);
            this.dtgInfo.TabIndex = 8;
            // 
            // colInfoName
            // 
            this.colInfoName.DataPropertyName = "InfoName";
            this.colInfoName.HeaderText = "Thông tin";
            this.colInfoName.Name = "colInfoName";
            this.colInfoName.ReadOnly = true;
            this.colInfoName.Width = 200;
            // 
            // colInfoValue
            // 
            this.colInfoValue.DataPropertyName = "InfoValue";
            dataGridViewCellStyle10.NullValue = null;
            this.colInfoValue.DefaultCellStyle = dataGridViewCellStyle10;
            this.colInfoValue.HeaderText = "Giá trị";
            this.colInfoValue.Name = "colInfoValue";
            this.colInfoValue.ReadOnly = true;
            this.colInfoValue.Width = 150;
            // 
            // dtgBarCode
            // 
            this.dtgBarCode.AllowUserToAddRows = false;
            this.dtgBarCode.AllowUserToDeleteRows = false;
            this.dtgBarCode.AllowUserToOrderColumns = true;
            this.dtgBarCode.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Azure;
            this.dtgBarCode.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dtgBarCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgBarCode.AutoGenerateColumns = false;
            this.dtgBarCode.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgBarCode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgBarCode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPalletCode,
            this.colProductLot,
            this.colCartonBarcode,
            this.colDescription,
            this.colLocationCode,
            this.colWarehouseKeeperName,
            this.colWarehouseVerifyDate,
            this.colDriverName,
            this.colLocationPutDate,
            this.colLocationSuggestion,
            this.colManufacturingDate,
            this.colExpiryDate,
            this.colProductCode});
            this.dtgBarCode.DataSource = this.bdsBarCode;
            this.dtgBarCode.GridColor = System.Drawing.SystemColors.Control;
            this.dtgBarCode.Location = new System.Drawing.Point(12, 40);
            this.dtgBarCode.Name = "dtgBarCode";
            this.dtgBarCode.ReadOnly = true;
            this.dtgBarCode.Size = new System.Drawing.Size(631, 601);
            this.dtgBarCode.TabIndex = 43;
            // 
            // colPalletCode
            // 
            this.colPalletCode.DataPropertyName = "PalletCode";
            this.colPalletCode.HeaderText = "Mã Pallet";
            this.colPalletCode.Name = "colPalletCode";
            this.colPalletCode.ReadOnly = true;
            // 
            // colProductLot
            // 
            this.colProductLot.DataPropertyName = "ProductLot";
            this.colProductLot.HeaderText = "Số lô";
            this.colProductLot.Name = "colProductLot";
            this.colProductLot.ReadOnly = true;
            // 
            // colCartonBarcode
            // 
            this.colCartonBarcode.DataPropertyName = "Quantity";
            dataGridViewCellStyle12.NullValue = null;
            this.colCartonBarcode.DefaultCellStyle = dataGridViewCellStyle12;
            this.colCartonBarcode.HeaderText = "Số lượng";
            this.colCartonBarcode.Name = "colCartonBarcode";
            this.colCartonBarcode.ReadOnly = true;
            this.colCartonBarcode.Width = 120;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "ProductDescription";
            this.colDescription.HeaderText = "Tên sản phẩm";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            this.colDescription.Width = 200;
            // 
            // colLocationCode
            // 
            this.colLocationCode.DataPropertyName = "LocationName";
            this.colLocationCode.HeaderText = "Mã vị trí";
            this.colLocationCode.Name = "colLocationCode";
            this.colLocationCode.ReadOnly = true;
            // 
            // colWarehouseKeeperName
            // 
            this.colWarehouseKeeperName.DataPropertyName = "WarehouseKeeperName";
            this.colWarehouseKeeperName.HeaderText = "Thủ kho";
            this.colWarehouseKeeperName.Name = "colWarehouseKeeperName";
            this.colWarehouseKeeperName.ReadOnly = true;
            // 
            // colWarehouseVerifyDate
            // 
            this.colWarehouseVerifyDate.DataPropertyName = "strWarehouseVerifyDate";
            this.colWarehouseVerifyDate.HeaderText = "Ngày nhận";
            this.colWarehouseVerifyDate.Name = "colWarehouseVerifyDate";
            this.colWarehouseVerifyDate.ReadOnly = true;
            // 
            // colDriverName
            // 
            this.colDriverName.DataPropertyName = "DriverName";
            this.colDriverName.HeaderText = "Xe nâng";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.ReadOnly = true;
            // 
            // colLocationPutDate
            // 
            this.colLocationPutDate.DataPropertyName = "strLocationPutDate";
            this.colLocationPutDate.HeaderText = "Ngày lên kệ";
            this.colLocationPutDate.Name = "colLocationPutDate";
            this.colLocationPutDate.ReadOnly = true;
            // 
            // colLocationSuggestion
            // 
            this.colLocationSuggestion.DataPropertyName = "LocationSuggestionName";
            this.colLocationSuggestion.HeaderText = "Vị trí gợi ý";
            this.colLocationSuggestion.Name = "colLocationSuggestion";
            this.colLocationSuggestion.ReadOnly = true;
            // 
            // colManufacturingDate
            // 
            this.colManufacturingDate.DataPropertyName = "strManufacturingDate";
            this.colManufacturingDate.HeaderText = "Ngày sản xuất";
            this.colManufacturingDate.Name = "colManufacturingDate";
            this.colManufacturingDate.ReadOnly = true;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.DataPropertyName = "strExpiryDate";
            this.colExpiryDate.HeaderText = "Ngày hết hạn";
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.ReadOnly = true;
            // 
            // colProductCode
            // 
            this.colProductCode.DataPropertyName = "ProductCode";
            this.colProductCode.HeaderText = "Mã sản phẩm";
            this.colProductCode.Name = "colProductCode";
            this.colProductCode.ReadOnly = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(12, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(122, 30);
            this.btnSearch.TabIndex = 44;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnPrintExportPallet
            // 
            this.btnPrintExportPallet.Location = new System.Drawing.Point(140, 4);
            this.btnPrintExportPallet.Name = "btnPrintExportPallet";
            this.btnPrintExportPallet.Size = new System.Drawing.Size(185, 30);
            this.btnPrintExportPallet.TabIndex = 45;
            this.btnPrintExportPallet.Text = "In DS pallet cần đưa lên kệ";
            this.btnPrintExportPallet.UseVisualStyleBackColor = true;
            // 
            // PalletTrackingView
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 653);
            this.Controls.Add(this.btnPrintExportPallet);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.dtgBarCode);
            this.Controls.Add(this.dtgInfo);
            this.Name = "PalletTrackingView";
            this.Text = "ProductPackingMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgBarCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsBarCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgInfo;
        private System.Windows.Forms.BindingSource bdsDetails;
        private System.Windows.Forms.DataGridView dtgBarCode;
        private System.Windows.Forms.BindingSource bdsBarCode;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInfoName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInfoValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPalletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWarehouseKeeperName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWarehouseVerifyDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDriverName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationPutDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLocationSuggestion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colManufacturingDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colExpiryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductCode;
        private System.Windows.Forms.Button btnPrintExportPallet;
    }
}