﻿using Bayer.WMS.Base;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Reflection;

namespace Bayer.WMS.Inv.Views
{
    public partial class StockReceivingView : BaseForm, IStockReceivingView
    {
        public StockReceivingView()
        {
            InitializeComponent();

            SetDoubleBuffered(dtgDetail);
            SetDoubleBuffered(dtgDetailSplit);
        }

        private async void btnConfirm_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IStockReceivingPresenter;
            await doPresenter.Confirm();
        }

        private async void btnDetail_Upload_Click(object sender, EventArgs e)
        {
            btnDetail_Upload.Enabled = false;
            try
            {
                using (var ofd = new OpenFileDialog())
                {
                    ofd.Filter = "Excel Files|*.xls;*.xlsx";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        await (_presenter as IStockReceivingPresenter).ImportExcel(ofd.FileName);
                    }
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnDetail_Upload.Enabled = true;
            }
        }

        private void btnDetail_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dtgDetail.SelectedRows)
                {
                    var detail = bdsDetails[row.Index] as StockReceivingDetail;

                    bdsDetails.Remove(detail);
                    (_presenter as IStockReceivingPresenter).DeleteDetail(detail);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override void InitializeComboBox()
        {
            cmbCode.PageSize = 20;
            cmbCode.ValueMember = "StockReceivingCode";
            cmbCode.DisplayMember = "StockReceivingCode";
            cmbCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("StockReceivingCode", "Mã", 100),
                new MultiColumnComboBox.ComboBoxColumn("StrDeliveryDate", "Ngày giao hàng", 120),
                new MultiColumnComboBox.ComboBoxColumn("CompanyName", "Nhà cung cấp", 120),
                new MultiColumnComboBox.ComboBoxColumn("ImportStatusDisplay", "Trạng thái nhập kho", 130),
                new MultiColumnComboBox.ComboBoxColumn("VerifyStatusDisplay", "Trạng thái kiểm tra", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Ghi chú", 200)
            };
            cmbCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadStockReceivingHeader", "OnCodeSelectChange");
            cmbCode.DropDownWidth = 800;
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            await base.SetPresenter(presenter);
            InitializeComboBox();
            var doPresenter = _presenter as IStockReceivingPresenter;
            await Task.WhenAll(doPresenter.LoadStockReceivingHeaders());
            if (!isRefresh)
                doPresenter.Insert();
        }

        public string StockReceivingCode
        {
            get
            {
                return cmbCode.SelectedItem["StockReceivingCode"].ToString();
            }
        }

        public DataTable StockReceivingHeaders
        {
            set { cmbCode.Source = value; }
        }

        private StockReceivingHeader _stockReceivingHeader;

        public StockReceivingHeader StockReceivingHeader
        {
            get => _stockReceivingHeader;
            set
            {
                _stockReceivingHeader = value;
                cmbCode.Text = _stockReceivingHeader.StockReceivingCode;
                txtCompanyName.DataBindings.Clear();
                txtCreatedBy.DataBindings.Clear();
                dtpDeliveryDate.DataBindings.Clear();
                txtImportStatus.DataBindings.Clear();
                txtVerifyStatus.DataBindings.Clear();
                txtDescription.DataBindings.Clear();

                txtCompanyName.DataBindings.Add("Text", StockReceivingHeader, "CompanyName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtCreatedBy.DataBindings.Add("Text", StockReceivingHeader, "CreatedByName", true, DataSourceUpdateMode.OnPropertyChanged);
                dtpDeliveryDate.DataBindings.Add("Value", StockReceivingHeader, "DeliveryDate", true, DataSourceUpdateMode.OnPropertyChanged);
                txtImportStatus.DataBindings.Add("Text", StockReceivingHeader, "ImportStatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtVerifyStatus.DataBindings.Add("Text", StockReceivingHeader, "VerifyStatusDisplay", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", StockReceivingHeader, "Description", true, DataSourceUpdateMode.OnPropertyChanged);


            }
        }

        private IList<StockReceivingDetail> _stockReceivingDetail;

        public IList<StockReceivingDetail> StockReceivingDetails
        {
            get => _stockReceivingDetail;
            set
            {
                _stockReceivingDetail = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetails.DataSource = _stockReceivingDetail;
                    });
                }
                else
                {
                    bdsDetails.DataSource = _stockReceivingDetail;
                }
            }
        }

        private DataTable _stockReceivingDetailSplits;

        public DataTable StockReceivingDetailSplits
        {
            get => _stockReceivingDetailSplits;
            set
            {
                _stockReceivingDetailSplits = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsDetailSplits.DataSource = _stockReceivingDetailSplits;
                    });
                }
                else
                {
                    bdsDetailSplits.DataSource = _stockReceivingDetailSplits;
                }
            }
        }

        private IList<StockReceivingDetail> _deletedStockReceivingDetails;

        public IList<StockReceivingDetail> DeletedStockReceivingDetails
        {
            get { return _deletedStockReceivingDetails; }
            set { _deletedStockReceivingDetails = value; }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    break;
                default:
                    break;
            }
        }

        public async void Print()
        {
            bool validate = true;
            var doPresenter = _presenter as IStockReceivingPresenter;
            await doPresenter.Save();

            if (validate)
            {
                await doPresenter.Print();
            }
        }

        private async void btnDownload_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IStockReceivingPresenter;
            await doPresenter.Save();
            await doPresenter.Export();
        }

        private void StockReceivingView_Load(object sender, EventArgs e)
        {

        }

        private async void btnPrintPalletLabel_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IStockReceivingPresenter;
            await doPresenter.Save();

            bool bolOKPrint = false;
            string strPrinterName = string.Empty;
            string seletedLineCode = string.Empty;
            int intNumberToPrint = 0;
            seletedLineCode = dtgDetail.SelectedCells[0].OwningRow.Cells["colBatchCode"].Value.ToString();


            StockReceivingPopupPrintPallet testDialog = new StockReceivingPopupPrintPallet();

            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            if (testDialog.ShowDialog(this) == DialogResult.OK)
            {
                if (testDialog.bolOKToPrint && testDialog.intNumberToPrint > 0)
                {
                    strPrinterName = testDialog.strPrinterName;
                    intNumberToPrint = testDialog.intNumberToPrint;
                    bolOKPrint = true;
                    //MessageBox.Show(testDialog.intNumberToPrint.ToString() + testDialog.strPrinterName);
                }
            }
            else
            {

            }
            testDialog.Dispose();

            if (bolOKPrint)
            {
                await doPresenter.ExportPalletLabel(seletedLineCode, intNumberToPrint, strPrinterName);
            }
        }

        private async void btnConfirmImportToWarehouse_Click(object sender, EventArgs e)
        {
            var doPresenter = _presenter as IStockReceivingPresenter;
            await doPresenter.Save();

            if (MessageBox.Show("Bạn có chắc chắn muốn xác nhận cho xe nâng chất hàng ?", "?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //var doPresenter = _presenter as IStockReceivingPresenter;
                await doPresenter.WarehouseManagerApproveImport();
            }
        }
    }
}