﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IPrepareNoteView : IBaseView
    {
        PrepareNoteHeader PrepareNoteHeader { get; set; }

        System.Data.DataTable PrepareNoteHeaders { set; }

        IList<PrepareNoteDetail> PrepareNoteDetails { get; set; }

        IList<PrepareNoteDetail> DeletedPrepareNoteDetails { get; set; }

    }

    public interface IPrepareNotePresenter : IBasePresenter
    {
        Task LoadPrepareNoteHeader();

        void DeletePrepareNoteDetail(PrepareNoteDetail item);

        Task Print();

        Task Confirm();

        Task LoadPrepareNoteDetails(string PrepareCode);
    }

    public class PrepareNotePresenter : BasePresenter, IPrepareNotePresenter
    {
        private IPrepareNoteView _mainView;
        private IPrepareNoteHeaderRepository _prepareNoteHeaderRepository;
        private IPrepareNoteDetailRepository _prepareNoteDetailRepository;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository;
        private IDeliveryOrderDetailRepository _deliveryOrderDetailRepository;

        public PrepareNotePresenter(IPrepareNoteView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IPrepareNoteHeaderRepository prepareNoteHeaderRepository
            , IPrepareNoteDetailRepository deliverynoteDetailRepository
            , IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository
            , IDeliveryOrderDetailRepository deliveryOrderDetailRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _prepareNoteHeaderRepository = prepareNoteHeaderRepository;
            _prepareNoteDetailRepository = deliverynoteDetailRepository;
            _deliveryOrderHeaderRepository = deliveryOrderHeaderRepository;
            _deliveryOrderDetailRepository = deliveryOrderDetailRepository;

            _mainView.PrepareNoteDetails = new List<PrepareNoteDetail>();
            _mainView.DeletedPrepareNoteDetails = new List<PrepareNoteDetail>();
        }

        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            var model = selectedItem.ToEntity<PrepareNoteHeader>();
            await LoadPrepareNoteDetails(model.PrepareCode);

            _mainView.PrepareNoteHeader = model;
        }

        public async Task LoadPrepareNoteHeader()
        {
            var model = await _prepareNoteHeaderRepository.GetAsync(p => !p.IsDeleted);
            _mainView.PrepareNoteHeaders = model.OrderByDescending(a => a.CreatedDateTime).ToList().ToDataTable();
        }

        public async Task LoadPrepareNoteDetails(string PrepareCode)
        {
            _mainView.PrepareNoteDetails = await _prepareNoteDetailRepository.GetIncludeProductAndPalletAsync(p => p.PrepareCode == PrepareCode);
        }

        public void DeletePrepareNoteDetail(PrepareNoteDetail item)
        {
            try
            {
                _mainView.DeletedPrepareNoteDetails.Add(item);
                _mainView.PrepareNoteDetails.Remove(item);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.PrepareNoteHeader;
            var details = _mainView.PrepareNoteDetails;
            var deletedDetails = _mainView.DeletedPrepareNoteDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _prepareNoteHeaderRepository = unitOfWork.Register(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;
                    _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                    _prepareNoteDetailRepository = unitOfWork.Register(_prepareNoteDetailRepository.GetType()) as IPrepareNoteDetailRepository;
                    _deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;

                    var listOrderDetail = _deliveryOrderDetailRepository.Get(a => a.DOImportCode == header.DOImportCode && a.CompanyCode == header.CompanyCode).ToList();
                    var listAnotherNoteDetails = await _prepareNoteDetailRepository.GetByDOCodeAndAnotherPartnerCode(header.DOImportCode, header.CompanyCode, header.PrepareCode);
                    #region VerifyDetailQuantity
                    bool productQuantityErr = false;
                    try
                    {
                        string productErr = string.Empty;
                        if (listOrderDetail != null && listOrderDetail.Count > 0 && details.Count > 0)
                        {
                            foreach (var item in listOrderDetail)
                            {
                                decimal totalNoteQuantity = 0;
                                decimal? totalAnotherNoteQuantity = listAnotherNoteDetails.Where(a => a.DeliveryCode == item.Delivery && a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                                totalAnotherNoteQuantity = totalAnotherNoteQuantity ?? totalAnotherNoteQuantity;
                                decimal? totalThisNoteQuantity = details.Where(a => a.DeliveryCode == item.Delivery && a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                                totalThisNoteQuantity = totalThisNoteQuantity ?? totalThisNoteQuantity;
                                totalNoteQuantity = totalThisNoteQuantity.Value + totalAnotherNoteQuantity.Value;

                                decimal? totalOrderQuantity = item.Quantity;
                                totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                                if (totalNoteQuantity > totalOrderQuantity)
                                {
                                    decimal? totalNoteQuantityValid = totalOrderQuantity - totalAnotherNoteQuantity;
                                    var firstDetailErr = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).FirstOrDefault();
                                    productErr = firstDetailErr.ProductCode + "----" + firstDetailErr.BatchCode + "----O:" + totalOrderQuantity.ToString() + "----A:" + totalAnotherNoteQuantity.ToString() + "----N:" + totalThisNoteQuantity.ToString();
                                    //throw new WrappedException(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr);
                                    productQuantityErr = true;
                                    _mainPresenter.SetMessage(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr, Utility.MessageType.Error);
                                    break;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }

                    //COMMENT FOR TEST ONLY
                    //if (productQuantityErr)
                    //{
                    //    return;
                    //}
                    #endregion


                    #region Save DN Header and Details
                    if (String.IsNullOrWhiteSpace(header.PrepareCode))
                    {
                        string code = await _prepareNoteHeaderRepository.GetNextNBR();

                        header.PrepareCode = code;
                        header.Status = DeliveryOrderHeader.status.InProgress;
                        header.IsDeleted = false;
                        header.UserCreateFullName = LoginInfo.LastName + " " + LoginInfo.FirstName;
                        await _prepareNoteHeaderRepository.Insert(header);
                    }
                    else
                    {
                        await _prepareNoteHeaderRepository.Update(header, new object[] { header.PrepareCode });
                    }

                    //// foreach deleted DO details and delete it
                    foreach (var item in deletedDetails)
                    {
                        await _prepareNoteDetailRepository.Delete(new object[] { item.DOCode, item.PrepareCode, item.DeliveryCode, item.ProductID, item.BatchCode });
                    }

                    foreach (var item in details)
                    {
                        item.PrepareCode = header.PrepareCode;
                        item.Status = DeliveryOrderHeader.status.InProgress;
                        await _prepareNoteDetailRepository.InsertOrUpdate(item, new object[] { item.DOCode, item.DeliveryCode, item.PrepareCode, item.ProductID, item.BatchCode });
                    }
                    #endregion

                    await unitOfWork.Commit();
                }

                {
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("Code", header.PrepareCode);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("DOImportCode", header.DOImportCode);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("Type", "PrepareNote");
                    listParam.Add(param3);
                    SqlParameter param4 = new SqlParameter("Status", DeliveryNoteHeader.status.InProgress);
                    listParam.Add(param4);

                    await _prepareNoteHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                    isSuccess = true;
                }


                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _prepareNoteHeaderRepository = _mainPresenter.Resolve(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;
                _prepareNoteDetailRepository = _mainPresenter.Resolve(_prepareNoteDetailRepository.GetType()) as IPrepareNoteDetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);

                    await LoadPrepareNoteHeader();
                    //await LoadPrepareNoteDetails(header.PrepareCode);

                    _mainView.PrepareNoteHeader = header;
                }
            }

        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.PrepareNoteHeader;
            //var details = _mainView.PrepareNoteDetails;

            List<SqlParameter> listParam = new List<SqlParameter>();
            SqlParameter paramProductCode = new SqlParameter("DOImportCode", header.DOImportCode);
            listParam.Add(paramProductCode);
            SqlParameter paramProductLot = new SqlParameter("CompanyCode", header.CompanyCode);
            listParam.Add(paramProductLot);
            var PreparedDetails = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_PrepareNote_SelectPreparedData", listParam.ToArray());
            var details = Utility.DataTableToList<PrepareNoteDetailReport>(PreparedDetails);

            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuSoanHang.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\PhieuSoanHang\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            //config Template Element
            string positionDelivertDate = "C5";
            string positionExportNote = "C6";
            string positionExportNoteBarCode = "I6";
            string positionCompanyName = "C7";
            string positionProvince = "C8";

            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                int cellRowIndex = 10;
                int cellColumnIndex = 1;
                int productIndex = 1;

                var listProduct = details.Select(a => a.ProductID).Distinct().ToList();

                #region Fill Data Detail
                //Loop through each row and read value from each column. 
                foreach (int productID in listProduct)
                {
                    #region Process for ProductName Row
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = productIndex.ToString();
                    productIndex++;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.FirstOrDefault(a => a.ProductID == productID).ProductDescription;
                    cellColumnIndex++;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(details.Where(a => a.ProductID == productID).Sum(a => a.DOQuantity));
                    cellColumnIndex++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(details.Where(a => a.ProductID == productID).Sum(a => a.PreparedQty));

                    //End of Row
                    cellColumnIndex = 1;
                    cellRowIndex++;
                    #endregion

                    var listDetailByProduct = details.Where(a => a.ProductID == productID).OrderBy(a => a.BatchCode).Distinct().ToList();
                    #region Process for BatchCode Row
                    foreach (var item in listDetailByProduct)
                    {
                        //Begin of Row
                        cellColumnIndex++;
                        cellColumnIndex++;
                        
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCode;
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToEmptyString(item.DOQuantity);
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToEmptyString(item.PreparedQty);
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToEmptyString(item.CartonQty);
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.PackingType;
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToEmptyString(item.PackageSize);
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToEmptyString(item.OddQty);
                        cellColumnIndex++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.OddPackingType;
                        cellColumnIndex++;

                        //End of Row
                        cellColumnIndex = 1;
                        cellRowIndex++;
                        #endregion
                    }
                }

                //People Sign

                //sheetDetail.Cells[cellRowIndex, 2]. .Style.Alignment.WrapText = true;
                //sheetDetail.get_Range("A" + cellRowIndex.ToString(), "M" + cellRowIndex.ToString()).b = false;
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A10", "K" + (cellRowIndex - 1).ToString());
                //foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                //{
                //    cell.BorderAround();
                //}


                #region Fill data Summary
                #region Thùng bao xô can
                //Thùng -> C
                //Bao -> B
                //Xô -> S
                //Can -> A
                
                listParam = new List<SqlParameter>();
                SqlParameter param0 = new SqlParameter("DeliveryTicketCode", string.Empty);
                listParam.Add(param0);
                SqlParameter param1= new SqlParameter("DOImportCode", header.DOImportCode);
                listParam.Add(param1);
                SqlParameter param2 = new SqlParameter("CompanyCode", header.CompanyCode);
                listParam.Add(param2);
                SqlParameter param3 = new SqlParameter("PackagingType", "C");
                listParam.Add(param3);
                string totalBoxes = await _deliveryOrderHeaderRepository.ExecuteScalar("proc_DeliveryTicket_SelectNumberByProductPackaging", listParam.ToArray());

                listParam = new List<SqlParameter>();
                param0 = new SqlParameter("DeliveryTicketCode", string.Empty);
                listParam.Add(param0);
                param1 = new SqlParameter("DOImportCode", header.DOImportCode);
                listParam.Add(param1);
                param2 = new SqlParameter("CompanyCode", header.CompanyCode);
                listParam.Add(param2);
                param3 = new SqlParameter("PackagingType", "CSH");
                listParam.Add(param3);
                string totalOddBoxes = await _deliveryOrderHeaderRepository.ExecuteScalar("proc_DeliveryTicket_SelectNumberByProductPackaging", listParam.ToArray());

                listParam = new List<SqlParameter>();
                param0 = new SqlParameter("DeliveryTicketCode", string.Empty);
                listParam.Add(param0);
                param1 = new SqlParameter("DOImportCode", header.DOImportCode);
                listParam.Add(param1);
                param2 = new SqlParameter("CompanyCode", header.CompanyCode);
                listParam.Add(param2);
                param3 = new SqlParameter("PackagingType", "CC");
                listParam.Add(param3);
                string totalEvenBoxes = await _deliveryOrderHeaderRepository.ExecuteScalar("proc_DeliveryTicket_SelectNumberByProductPackaging", listParam.ToArray());


                decimal totalBags = details.Where(a => a.PackingType == "B").Sum(a => a.PreparedQty);
                decimal totalBuckets = details.Where(a => a.PackingType == "X").Sum(a => a.PreparedQty);
                decimal totalCans = details.Where(a => a.PackingType == "C").Sum(a => a.PreparedQty);

                cellRowIndex++;
                int beginOfDataSummary = cellRowIndex;
                sheetDetail.Cells[cellRowIndex, 4] = Utility.DecimalToString(totalBuckets);
                sheetDetail.Cells[cellRowIndex, 5] = "XÔ";

                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 4] = Utility.DecimalToString(totalCans);
                sheetDetail.Cells[cellRowIndex, 5] = "CAN";

                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 4] = Utility.DecimalToString(totalBags);
                sheetDetail.Cells[cellRowIndex, 5] = "BAO";

                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 4] = totalBoxes;
                sheetDetail.Cells[cellRowIndex, 5] = "THÙNG = "
                + totalEvenBoxes + " Thùng Chẵn" + " + "
                + totalOddBoxes + " Thùng Lẻ";
                #endregion


                #region Người lập phiếu, người kiểm
                cellRowIndex++;

                sheetDetail.Cells[cellRowIndex, 2] = @"Tổng số phiếu soạn hàng: …………………………………";
                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 2] = @"Số lượng kiểm thực tế: ……………………………………";
                cellRowIndex++;
                int beginOfDataAlignRight = cellRowIndex;
                sheetDetail.Cells[cellRowIndex, 3] = @"1.Tổng số thùng/ pallet:";
                sheetDetail.Cells[cellRowIndex, 4] = "………………………………………………………………";
                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 3] = @"Tổng số thùng:";
                sheetDetail.Cells[cellRowIndex, 4] = @"………………………………………………………………";
                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 3] = @"2.Số lượng bao/ pallet:";
                sheetDetail.Cells[cellRowIndex, 4] = @"………………………………………………………………";
                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 3] = @"Tổng số bao:";
                sheetDetail.Cells[cellRowIndex, 4] = @"………………………………………………………………";
                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 3] = @"3.Số lượng can/ pallet";
                sheetDetail.Cells[cellRowIndex, 4] = @"………………………………………………………………";
                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 3] = @"Tổng số can:";
                sheetDetail.Cells[cellRowIndex, 4] = @"………………………………………………………………";
                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 3] = @"4.Số lượng xô/ pallet:";
                sheetDetail.Cells[cellRowIndex, 4] = @"………………………………………………………………";
                cellRowIndex++;
                sheetDetail.Cells[cellRowIndex, 3] = @"Tổng số xô:";
                sheetDetail.Cells[cellRowIndex, 4] = @"………………………………………………………………";

                sheetDetail.get_Range("A" + beginOfDataAlignRight.ToString(), "C" + cellRowIndex.ToString()).Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

                cellRowIndex++;
                cellRowIndex++;

                sheetDetail.get_Range("A" + beginOfDataSummary.ToString(), "K" + cellRowIndex.ToString()).WrapText = false;

                sheetDetail.Cells[cellRowIndex, 1] = @"Người lập phiếu";
                sheetDetail.Cells[cellRowIndex, 4] = @"Nhân viên hệ thống kho kiểm trước khi Post";
                sheetDetail.Cells[cellRowIndex, 10] = @"Người kiểm";
                #endregion
                #endregion


                #endregion

                #region Fill Data Header
                sheetDetail.get_Range(positionDelivertDate, positionDelivertDate).Cells[0] = header.StrDeliveryDate;
                sheetDetail.get_Range(positionExportNote, positionExportNote).Cells[0] = header.PrepareCode;
                sheetDetail.get_Range(positionExportNoteBarCode, positionExportNoteBarCode).Cells[0] = "*" + header.PrepareCode + "*";
                sheetDetail.get_Range(positionCompanyName, positionCompanyName).Cells[0] = header.CompanyName;
                sheetDetail.get_Range(positionProvince, positionProvince).Cells[0] = header.Province;
                //sheetDetail.get_Range(positionExportNoteBarCode, positionExportNoteBarCode).Cells[0] = header.PrepareCode;
                #endregion

                #region Fill Page Number Info
                //sheetDetail.PageSetup.LeftFooter = @"Phát hành theo biểu mẫu WH-55-03 đã được đăng kí"; // This worked correctly
                sheetDetail.PageSetup.RightFooter = @"Page &P of &N"; // This worked correctly
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.PrepareCode + ".xlsx";

                //Set Print Area
                sheetDetail.PageSetup.PrintArea = ("A1:O" + cellRowIndex.ToString());

                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    //printFromExcel(exportPath);

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully + ": " + header.PrepareCode, Utility.MessageType.Information);
                }
            }
        }

        #region ExcelPrint
        // Module-level "dummy object" variable for optional parameters
        public static object optionalParameter = Type.Missing;

        public static void printFromExcel(string yourFileName)
        {
            Microsoft.Office.Interop.Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();

            xl.Workbooks.Open(yourFileName, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

            xl.Worksheets.PrintOut(optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

            if (xl != null)
                xl.Quit();
        }
        #endregion

        public async Task Confirm()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var header = _mainView.PrepareNoteHeader;
            bool isSuccess = false;

            try
            {
                {
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("Code", header.PrepareCode);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("DOImportCode", header.DOImportCode);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("Type", "PrepareNote");
                    listParam.Add(param3);
                    SqlParameter param4 = new SqlParameter("Status", PrepareNoteHeader.status.Complete);
                    listParam.Add(param4);

                    await _prepareNoteHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                    isSuccess = true;
                }
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _prepareNoteHeaderRepository = _mainPresenter.Resolve(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_ConfirmSucessfully, Utility.MessageType.Information);

                    await LoadPrepareNoteHeader();
                    //await LoadPrepareNoteDetails(header.PrepareCode);
                }
            }
        }
    }
}
