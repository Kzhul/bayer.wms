﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface ISplitNoteView : IBaseView
    {
        SplitNoteHeader SplitNoteHeader { get; set; }

        System.Data.DataTable SplitNoteHeaders { set; }

        IList<SplitNoteDetail> SplitNoteDetails { get; set; }

        IList<SplitNoteDetail> DeletedSplitNoteDetails { get; set; }

    }

    public interface ISplitNotePresenter : IBasePresenter
    {
        Task LoadSplitNoteHeader();

        void DeleteSplitNoteDetail(SplitNoteDetail item);

        Task Print();

        Task Confirm();

        Task LoadSplitNoteDetails(string SplitCode);
    }

    public class SplitNotePresenter : BasePresenter, ISplitNotePresenter
    {
        private ISplitNoteView _mainView;
        private ISplitNoteHeaderRepository _splitNoteHeaderRepository;
        private ISplitNoteDetailRepository _splitNoteDetailRepository;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository;
        private IDeliveryOrderDetailRepository _deliveryOrderDetailRepository;

        public SplitNotePresenter(ISplitNoteView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            ISplitNoteHeaderRepository splitNoteHeaderRepository
            , ISplitNoteDetailRepository deliverynoteDetailRepository
            , IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository
            , IDeliveryOrderDetailRepository deliveryOrderDetailRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _splitNoteHeaderRepository = splitNoteHeaderRepository;
            _splitNoteDetailRepository = deliverynoteDetailRepository;
            _deliveryOrderHeaderRepository = deliveryOrderHeaderRepository;
            _deliveryOrderDetailRepository = deliveryOrderDetailRepository;

            _mainView.SplitNoteDetails = new List<SplitNoteDetail>();
            _mainView.DeletedSplitNoteDetails = new List<SplitNoteDetail>();
        }

        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            var model = selectedItem.ToEntity<SplitNoteHeader>();
            await LoadSplitNoteDetails(model.SplitCode);

            _mainView.SplitNoteHeader = model;
        }

        public async Task LoadSplitNoteHeader()
        {
            var model = await _splitNoteHeaderRepository.GetAsync(p => !p.IsDeleted);
            _mainView.SplitNoteHeaders = model.OrderByDescending(a => a.CreatedDateTime).ToList().ToDataTable();
        }

        public async Task LoadSplitNoteDetails(string SplitCode)
        {
            _mainView.SplitNoteDetails = await _splitNoteDetailRepository.GetIncludeProductAndPalletAsync(p => p.SplitCode == SplitCode);
        }

        public void DeleteSplitNoteDetail(SplitNoteDetail item)
        {
            try
            {
                _mainView.DeletedSplitNoteDetails.Add(item);
                _mainView.SplitNoteDetails.Remove(item);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.SplitNoteHeader;
            var details = _mainView.SplitNoteDetails;
            var deletedDetails = _mainView.DeletedSplitNoteDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _splitNoteHeaderRepository = unitOfWork.Register(_splitNoteHeaderRepository.GetType()) as ISplitNoteHeaderRepository;
                    _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                    _splitNoteDetailRepository = unitOfWork.Register(_splitNoteDetailRepository.GetType()) as ISplitNoteDetailRepository;
                    _deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;

                    var listOrderDetail = _deliveryOrderDetailRepository.Get(a => a.DOImportCode == header.DOImportCode).ToList();
                    var listAnotherNoteDetails = await _splitNoteDetailRepository.GetByDOCodeAndAnotherPartnerCode(header.DOImportCode, string.Empty, header.SplitCode);
                    #region VerifyDetailQuantity
                    bool productQuantityErr = false;
                    try
                    {
                        string productErr = string.Empty;
                        if (listOrderDetail != null && listOrderDetail.Count > 0 && details.Count > 0)
                        {
                            //List<string, string> listProduct = listOrderDetail.Select(a => a.ProductCode).Distinct().ToList();
                            var listMaterialCodeBatchCode = details.Select(a => new { a.ProductID, a.BatchCode, a.DeliveryCode }).Distinct().ToList();

                            foreach (var item in listMaterialCodeBatchCode)
                            {
                                decimal totalNoteQuantity = 0;
                                decimal? totalAnotherNoteQuantity = listAnotherNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.DeliveryCode == item.DeliveryCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                                totalAnotherNoteQuantity = totalAnotherNoteQuantity ?? totalAnotherNoteQuantity;
                                decimal? totalThisNoteQuantity = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.DeliveryCode == item.DeliveryCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                                totalThisNoteQuantity = totalThisNoteQuantity ?? totalThisNoteQuantity;
                                totalNoteQuantity = totalThisNoteQuantity.Value + totalAnotherNoteQuantity.Value;

                                decimal? totalOrderQuantity = listOrderDetail.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Delivery == item.DeliveryCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                                totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                                if (totalNoteQuantity > totalOrderQuantity)
                                {
                                    decimal? totalNoteQuantityValid = totalOrderQuantity - totalAnotherNoteQuantity;
                                    var firstDetailErr = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).FirstOrDefault();
                                    productErr = firstDetailErr.ProductCode + "----" + firstDetailErr.BatchCode + "----O:" + totalOrderQuantity.ToString() + "----A:" + totalAnotherNoteQuantity.ToString() + "----N:" + totalThisNoteQuantity.ToString();
                                    //throw new WrappedException(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr);
                                    productQuantityErr = true;
                                    _mainPresenter.SetMessage(Messages.Error_QuantityExportNotGreaterThanQuantityOrder + productErr, Utility.MessageType.Error);
                                    break;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    }

                    //COMMENT FOR TEST ONLY
                    //if (productQuantityErr)
                    //{
                    //    return;
                    //}
                    #endregion


                    #region Save DN Header and Details
                    if (String.IsNullOrWhiteSpace(header.SplitCode))
                    {
                        string code = await _splitNoteHeaderRepository.GetNextNBR();

                        header.SplitCode = code;
                        header.Status = DeliveryOrderHeader.status.InProgress;
                        header.IsDeleted = false;
                        await _splitNoteHeaderRepository.Insert(header);
                    }
                    else
                    {
                        await _splitNoteHeaderRepository.Update(header, new object[] { header.SplitCode });
                    }

                    //// foreach deleted DO details and delete it
                    foreach (var item in deletedDetails)
                    {
                        await _splitNoteDetailRepository.Delete(new object[] { item.DOCode, item.SplitCode, item.ProductID, item.BatchCode });
                    }

                    foreach (var item in details)
                    {
                        item.SplitCode = header.SplitCode;
                        item.Status = DeliveryOrderHeader.status.InProgress;
                        await _splitNoteDetailRepository.InsertOrUpdate(item, new object[] { item.SplitCode, item.DOCode, item.DeliveryCode, item.ProductID, item.BatchCode, item.CompanyCode });
                    }
                    #endregion

                    await unitOfWork.Commit();
                }

                #region #region Update DO Status
                {
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("Code", header.SplitCode);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("DOImportCode", header.DOImportCode);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("Type", "SplitNote");
                    listParam.Add(param3);
                    SqlParameter param4 = new SqlParameter("Status", DeliveryNoteHeader.status.InProgress);
                    listParam.Add(param4);

                    await _splitNoteHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                    isSuccess = true;
                }
                #endregion


                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _splitNoteHeaderRepository = _mainPresenter.Resolve(_splitNoteHeaderRepository.GetType()) as ISplitNoteHeaderRepository;
                _splitNoteDetailRepository = _mainPresenter.Resolve(_splitNoteDetailRepository.GetType()) as ISplitNoteDetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);

                    await LoadSplitNoteHeader();
                    //await LoadSplitNoteDetails(header.SplitCode);

                    _mainView.SplitNoteHeader = header;
                }
            }

        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.SplitNoteHeader;
            var details = _mainView.SplitNoteDetails;
            var deletedDetails = _mainView.DeletedSplitNoteDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuChiaHang.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\PhieuChiaHang\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            //config Template Element
            string positionDelivertDate = "C5";
            string positionExportNote = "C6";
            string positionExportNoteBarCode = "D6";

            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                int cellRowIndex = 9;
                int cellColumnIndex = 1;
                int productIndex = 1;

                var listProduct = details.Select(a => a.ProductID).Distinct().ToList();

                #region Fill Data Detail
                //Loop through each row and read value from each column. 
                foreach (int productID in listProduct)
                {
                    //oSheet.get_Range("A1", "AS1").Merge();

                    #region Process for ProductName Row
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = productIndex.ToString();
                    productIndex++;
                    cellColumnIndex++;

                    //Merge cell
                    sheetDetail.get_Range(sheetDetail.Cells[cellRowIndex, cellColumnIndex], sheetDetail.Cells[cellRowIndex, cellColumnIndex + 2]).Merge();
                    sheetDetail.get_Range(sheetDetail.Cells[cellRowIndex, cellColumnIndex], sheetDetail.Cells[cellRowIndex, cellColumnIndex + 2]).Font.Bold = true;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.FirstOrDefault(a => a.ProductID == productID).ProductDescription;
                    cellColumnIndex++;
                    cellColumnIndex++;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(details.Where(a => a.ProductID == productID && a.Quantity.HasValue).Sum(a => a.Quantity));

                    //End of Row
                    cellColumnIndex = 1;
                    cellRowIndex++;
                    #endregion

                    var listDetailByCompany = details.Where(a => a.ProductID == productID).OrderBy(a => a.CompanyName).Select(a => new { a.CompanyCode, a.CompanyName }).Distinct().ToList();
                    #region Process for Company Row
                    foreach (var itemCompany in listDetailByCompany)
                    {
                        //Begin of Row
                        cellColumnIndex++;

                        sheetDetail.get_Range(sheetDetail.Cells[cellRowIndex, cellColumnIndex], sheetDetail.Cells[cellRowIndex, cellColumnIndex + 2]).Merge();
                        sheetDetail.get_Range(sheetDetail.Cells[cellRowIndex, cellColumnIndex], sheetDetail.Cells[cellRowIndex, cellColumnIndex + 2]).HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = itemCompany.CompanyName;



                        cellColumnIndex++;
                        cellColumnIndex++;
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(details.Where(a => a.ProductID == productID && a.CompanyCode == itemCompany.CompanyCode && a.Quantity.HasValue).Sum(a => a.Quantity));

                        //End of Row
                        cellColumnIndex = 1;
                        cellRowIndex++;

                        var listDetailByBatchCode = details.Where(a => a.ProductID == productID && a.CompanyCode == itemCompany.CompanyCode).Select(a => a.BatchCode).OrderBy(a => a).Distinct().ToList();
                        #region Process for BatchCode Row
                        foreach (var item in listDetailByBatchCode)
                        {
                            var listSumProduct = details.Where(a => a.ProductID == productID && a.CompanyCode == itemCompany.CompanyCode && a.BatchCode == item).ToList();
                            var template = listSumProduct.FirstOrDefault();

                            //Begin of Row
                            cellColumnIndex++;
                            cellColumnIndex++;
                            cellColumnIndex++;

                            if (!string.IsNullOrEmpty(template.BatchCodeDistributor))
                            {
                                template.BatchCode += "/ " + template.BatchCodeDistributor;
                            }
                            sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item;
                            cellColumnIndex++;

                            sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(listSumProduct.Sum(a => a.Quantity));
                            cellColumnIndex++;

                            sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(listSumProduct.Sum(a => a.PackQuantity));
                            cellColumnIndex++;

                            sheetDetail.Cells[cellRowIndex, cellColumnIndex] = template.StrPackType;
                            cellColumnIndex++;

                            sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(listSumProduct.Sum(a => a.ProductQuantity));
                            cellColumnIndex++;

                            //End of Row
                            cellColumnIndex = 1;
                            cellRowIndex++;
                        }
                        #endregion

                    }
                    #endregion


                }

                //People Sign

                //sheetDetail.Cells[cellRowIndex, 2]. .Style.Alignment.WrapText = true;
                //sheetDetail.get_Range("A" + cellRowIndex.ToString(), "M" + cellRowIndex.ToString()).b = false;
                //Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A9", "H" + (cellRowIndex - 1).ToString());
                //foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                //{
                //    cell.BorderAround();
                //}
                cellRowIndex++;

                //this note dont need to wrap text
                //sheetDetail.get_Range("A" + cellRowIndex.ToString(), "M" + cellRowIndex.ToString()).WrapText = false;

                sheetDetail.Cells[cellRowIndex, 2] = "Người lập phiếu";
                sheetDetail.Cells[cellRowIndex, 4] = "Người chia";


                #endregion

                #region Fill Data Header
                sheetDetail.get_Range(positionDelivertDate, positionDelivertDate).Cells[0] = header.StrDeliveryDate;
                sheetDetail.get_Range(positionExportNote, positionExportNote).Cells[0] = header.SplitCode;
                sheetDetail.get_Range(positionExportNoteBarCode, positionExportNoteBarCode).Cells[0] = "*" + header.SplitCode + "*";
                #endregion

                #region Fill Page Number Info
                //sheetDetail.PageSetup.LeftFooter = "Phát hành theo biểu mẫu WH-55-03 đã được đăng kí"; // This worked correctly
                sheetDetail.PageSetup.RightFooter = "Page &P of &N"; // This worked correctly
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.SplitCode + ".xlsx";

                //Set Print Area
                sheetDetail.PageSetup.PrintArea = ("A1:H" + cellRowIndex.ToString());

                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    //ProcessStartInfo startInfo;
                    //startInfo = new ProcessStartInfo(exportPath);
                    //startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    //Process newProcess = new Process();
                    //newProcess.StartInfo = startInfo;
                    //newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    printFromExcel(exportPath);

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully + ": " + header.SplitCode, Utility.MessageType.Information);
                }
            }
        }

        #region ExcelPrint
        // Module-level "dummy object" variable for optional parameters
        public static object optionalParameter = Type.Missing;

        public static void printFromExcel(string yourFileName)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();

                xl.Workbooks.Open(yourFileName, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

                xl.Worksheets.PrintOut(optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

                if (xl != null)
                    xl.Quit();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
        }
        #endregion

        public async Task Confirm()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var header = _mainView.SplitNoteHeader;
            bool isSuccess = false;

            try
            {
                {
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("Code", header.SplitCode);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("DOImportCode", header.DOImportCode);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("Type", "SplitNote");
                    listParam.Add(param3);
                    SqlParameter param4 = new SqlParameter("Status", SplitNoteHeader.status.Complete);
                    listParam.Add(param4);

                    await _splitNoteHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                    isSuccess = true;
                }
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _splitNoteHeaderRepository = _mainPresenter.Resolve(_splitNoteHeaderRepository.GetType()) as ISplitNoteHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_ConfirmSucessfully, Utility.MessageType.Information);

                    await LoadSplitNoteHeader();
                    //await LoadSplitNoteDetails(header.SplitCode);
                }
            }
        }
    }
}
