﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using GenCode128;
using System.IO;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IStockReceivingView : IBaseView
    {
        System.Data.DataTable StockReceivingHeaders { set; }
        System.Data.DataTable StockReceivingDetailSplits { set; }
        StockReceivingHeader StockReceivingHeader { get; set; }
        IList<StockReceivingDetail> StockReceivingDetails { get; set; }
        IList<StockReceivingDetail> DeletedStockReceivingDetails { get; set; }
    }

    public interface IStockReceivingPresenter : IBasePresenter
    {
        Task<bool> LoadStockReceivingHeaders();
        void DeleteDetail(StockReceivingDetail item);
        Task Print();
        Task Confirm();
        Task ImportExcel(string fileName);

        Task Export();

        Task ExportPalletLabel(string strProductLot, int intNumberToPrint, string strPrinterName);

        Task WarehouseManagerApproveImport();
    }

    public class StockReceivingPresenter : BasePresenter, IStockReceivingPresenter
    {
        private IStockReceivingView _mainView;
        private IStockReceivingHeaderRepository _stockReceivingHeaderRepository;
        private IStockReceivingDetailRepository _stockReceivingDetailRepository;
        private IPalletRepository _palletRepository;
        private IProductRepository _productRepository;
        private IProductPackingRepository _productPackingRepository;

        public StockReceivingPresenter(IStockReceivingView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IStockReceivingHeaderRepository stockReceivingHeaderRepository,
            IStockReceivingDetailRepository stockReceivingDetailRepository,
            IPalletRepository palletRepository,
            IProductRepository productRepository,
            IProductPackingRepository productPackingRepository)
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _stockReceivingHeaderRepository = stockReceivingHeaderRepository;
            _stockReceivingDetailRepository = stockReceivingDetailRepository;
            _palletRepository = palletRepository;
            _productRepository = productRepository;
            _productPackingRepository = productPackingRepository;

            _mainView.StockReceivingDetails = new List<StockReceivingDetail>();
            _mainView.DeletedStockReceivingDetails = new List<StockReceivingDetail>();
            //_mainView.StockReceivingHeader = new StockReceivingHeader();
            //_mainView.StockReceivingHeader.ImportStatus = StockReceivingHeader.status.New;
            //_mainView.StockReceivingHeader.VerifyStatus = StockReceivingHeader.status.New;
            _mainView.StockReceivingHeaders = new System.Data.DataTable();
            _mainView.StockReceivingDetailSplits = new System.Data.DataTable();
        }

        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            var model = selectedItem.ToEntity<StockReceivingHeader>();
            _mainView.StockReceivingHeader = model;

            var t1 = LoadStockReceivingDetails(_mainView.StockReceivingHeader.StockReceivingCode);
            var t2 = LoadStockReceivingDetailSplits(_mainView.StockReceivingHeader.StockReceivingCode);
            await Task.WhenAll(t1, t2);
        }

        public async Task<bool> LoadStockReceivingHeaders()
        {
            try
            {
                _mainView.StockReceivingHeaders = (await _stockReceivingHeaderRepository.GetIncludeCreatedByAsync(p => !p.IsDeleted)).OrderByDescending(p => p.CreatedDateTime).ToList().ToDataTable();
                //if (_mainView.StockReceivingHeader != null)
                //{
                //    var t1 = LoadStockReceivingDetails(_mainView.StockReceivingHeader.StockReceivingCode);
                //    var t2 = LoadStockReceivingDetailSplits(_mainView.StockReceivingHeader.StockReceivingCode);
                //    await Task.WhenAll(t1, t2);
                //}

                return true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task<bool> LoadStockReceivingDetails(string StockReceivingCode)
        {
            _mainView.StockReceivingDetails = await _stockReceivingDetailRepository.GetIncludeProductAsync(p => p.StockReceivingCode == StockReceivingCode);
            _mainView.StockReceivingDetailSplits = await _stockReceivingDetailRepository.ExecuteDataTable("proc_StockReceivingDetailSplit_Select_ByDocument",
                new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = StockReceivingCode });
            return true;
        }

        public async Task<bool> LoadStockReceivingDetailSplits(string StockReceivingCode)
        {
            //_mainView.StockReceivingDetailSplits = await _stockReceivingDetailRepository.ExecuteDataTable("proc_StockReceivingDetailSplit_Select_ByDocument",
            //    new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = StockReceivingCode });
            return true;
        }

        public void DeleteDetail(StockReceivingDetail item)
        {
            try
            {
                _mainView.DeletedStockReceivingDetails.Add(item);
                _mainView.StockReceivingDetails.Remove(item);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override void Insert()
        {
            _mainView.StockReceivingHeader = new StockReceivingHeader();
            _mainView.StockReceivingDetails = new List<StockReceivingDetail>();
            _mainView.DeletedStockReceivingDetails = new List<StockReceivingDetail>();
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReceivingHeader;
            var details = _mainView.StockReceivingDetails;
            var deletedDetails = _mainView.DeletedStockReceivingDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuGiaoHang.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\PhieuGiaoHang\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            //config Template Element
            string positionCompanyName = "M7";
            string positionStockReceivingCode = "AT7";
            string positionBarCodeStockReceivingCode = "AK8";
            string positionDeliveryDate = "I10";
            string positionOrderNumbers = "V10";

            string positionBoxes = "I13";
            string positionBags = "P13";
            string positionBuckets = "W13";
            string positionCans = "AC11";

            string positionFreeItems = "L16";

            string positionGrossWeight = "K18";

            string positionTruckNo = "E21";

            string positionDriver = "E24";
            string positionDriverIdentification = "AC24";
            string positionReceiptBy = "J27";

            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Header
                //sheetDetail.get_Range(positionCompanyName, positionCompanyName).Cells[0] = header.CompanyCode + " - " + header.CompanyName + " - " + header.Province;
                //sheetDetail.get_Range(positionStockReceivingCode, positionStockReceivingCode).Cells[0] = header.StockReceivingCode;
                //sheetDetail.get_Range(positionBarCodeStockReceivingCode, positionBarCodeStockReceivingCode).Cells[0] = "*" + header.StockReceivingCode + "*";

                //sheetDetail.get_Range(positionDeliveryDate, positionDeliveryDate).Cells[0] = header.StrDeliveryDate;
                //sheetDetail.get_Range(positionOrderNumbers, positionOrderNumbers).Cells[0] = header.ListOrderNumbers;

                //sheetDetail.get_Range(positionBoxes, positionBoxes).Cells[0] = Utility.DecimalToString( header.TotalBoxes);
                //sheetDetail.get_Range(positionBags, positionBags).Cells[0] = Utility.DecimalToString(header.TotalBags);
                //sheetDetail.get_Range(positionBuckets, positionBuckets).Cells[0] = Utility.DecimalToString(header.TotalBuckets);
                //sheetDetail.get_Range(positionCans, positionCans).Cells[0] = Utility.DecimalToString(header.TotalCans);

                //sheetDetail.get_Range(positionFreeItems, positionFreeItems).Cells[0] = header.ListFreeItems;

                //sheetDetail.get_Range(positionGrossWeight, positionGrossWeight).Cells[0] = header.GrossWeight;

                //sheetDetail.get_Range(positionTruckNo, positionTruckNo).Cells[0] = header.TruckNo;

                //sheetDetail.get_Range(positionDriver, positionDriver).Cells[0] = header.Driver;

                //sheetDetail.get_Range(positionDriverIdentification, positionDriverIdentification).Cells[0] = header.DriverIdentification;

                //sheetDetail.get_Range(positionReceiptBy, positionReceiptBy).Cells[0] = header.ReceiptBy;
                #endregion

                #region Fill Page Number Info
                sheetDetail.PageSetup.LeftFooter = @"Phát hành theo biểu mẫu WH-55-03 đã được đăng kí"; // This worked correctly
                sheetDetail.PageSetup.RightFooter = @"Page &P of &N"; // This worked correctly
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.StockReceivingCode + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    //ProcessStartInfo startInfo;
                    //startInfo = new ProcessStartInfo(exportPath);
                    //startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    //Process newProcess = new Process();
                    //newProcess.StartInfo = startInfo;
                    //newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    //printFromExcel(exportPath);

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully + ": " + header.StockReceivingCode, Utility.MessageType.Information);
                }
            }
        }

        public static void PrintFromExcel(string yourFileName)
        {
            //Microsoft.Office.Interop.Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();
            //xl.Workbooks.Open(yourFileName, Type., optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);
            //xl.Worksheets.PrintOut(optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

            //if (xl != null)
            //{
            //    xl.Quit();
            //}
        }

        public async Task Confirm()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var header = _mainView.StockReceivingHeader;
            bool isSuccess = false;

            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                SqlParameter param1 = new SqlParameter("Code", header.StockReceivingCode);
                listParam.Add(param1);
                SqlParameter param2 = new SqlParameter("DOImportCode", string.Empty);
                listParam.Add(param2);
                SqlParameter param3 = new SqlParameter("Type", "StockReceiving");
                listParam.Add(param3);
                SqlParameter param4 = new SqlParameter("Status", DeliveryNoteHeader.status.Complete);
                listParam.Add(param4);

                await _stockReceivingHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _stockReceivingHeaderRepository = _mainPresenter.Resolve(_stockReceivingHeaderRepository.GetType()) as IStockReceivingHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_ConfirmSucessfully, Utility.MessageType.Information);

                    await LoadStockReceivingHeaders();
                }
            }
        }

        public async Task ImportExcel(string fileName)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            string start = "A1";
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            try
            {
                workbook = workbooks.Open(fileName);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                //// get a range to work with
                range = sheetDetail.get_Range(start, Missing.Value);

                //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                range = range.get_End(XlDirection.xlDown);

                //// get the address of the bottom cell
                string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                //// specific end column
                end = downAddress.Remove(0, 1);
                Utility.LogEx(end.ToString(), "Receiving_ImportExcel");

                end = "K" + end;
                end = "K1000";

                //// Get the range, then values from start to end
                range = sheetDetail.get_Range(start, end);
                var values = (object[,])range.Value2;
                int count = values.GetLength(0);

                var products = await _productRepository.GetAsync(p => !p.IsDeleted);
                var productPackagings = await _productPackingRepository.GetAsync(p => p.Type == ProductPacking.type.Pallet);

                var header = await _stockReceivingHeaderRepository.GetSingleAsync(p => p.StockReceivingCode == _mainView.StockReceivingHeader.StockReceivingCode && !p.IsDeleted);
                IList<StockReceivingDetail> details = new List<StockReceivingDetail>();
                if (header != null)
                {
                    if (header.ImportStatus == StockReceivingHeader.status.Completed)
                        throw new WrappedException("Phiếu nhập kho này đã hoàn tất, không thể import bổ sung.");

                    details = await _stockReceivingDetailRepository.GetAsync(p => p.StockReceivingCode == _mainView.StockReceivingHeader.StockReceivingCode);
                }

                string companyName = $"{values[7, 3]}";
                header = header ?? new StockReceivingHeader();
                header.DeliveryDate = DateTime.Today;
                header.CompanyName = companyName;
                header.CreatedByName = $"{LoginInfo.LastName} {LoginInfo.FirstName}";




                #region Get current productLot Number
                //Import
                //  Get number for each type of data
                //  Increate Current ## of that type
                string strToDayProductLot = DateTime.Today.ToString("yyMMdd");
                string productLotFinishProduct = strToDayProductLot + "F";
                string productLotRawMaterial = strToDayProductLot + "R";
                string productLotPackagingMaterial = strToDayProductLot + "P";
                int intCurrentProductLotFinishProduct = 0;
                int intCurrentProductLotRawMaterial = 0;
                int intCurrentProductLotPackagingMaterial = 0;

                string strTemp = await _stockReceivingDetailRepository.ExecuteScalar("proc_StockReceivingDetail_GetLastestProductLotByType",
                                                    new SqlParameter { ParameterName = "Type", SqlDbType = SqlDbType.VarChar, Value = productLotFinishProduct });
                if (!string.IsNullOrEmpty(strTemp))
                {
                    intCurrentProductLotFinishProduct = Convert.ToInt32(strTemp.Replace(productLotFinishProduct, string.Empty));
                }
                strTemp = await _stockReceivingDetailRepository.ExecuteScalar("proc_StockReceivingDetail_GetLastestProductLotByType",
                                                    new SqlParameter { ParameterName = "Type", SqlDbType = SqlDbType.VarChar, Value = productLotRawMaterial });
                if (!string.IsNullOrEmpty(strTemp))
                {
                    intCurrentProductLotRawMaterial = Convert.ToInt32(strTemp.Replace(productLotRawMaterial, string.Empty));
                }
                strTemp = await _stockReceivingDetailRepository.ExecuteScalar("proc_StockReceivingDetail_GetLastestProductLotByType",
                                                    new SqlParameter { ParameterName = "Type", SqlDbType = SqlDbType.VarChar, Value = productLotPackagingMaterial });
                if (!string.IsNullOrEmpty(strTemp))
                {
                    intCurrentProductLotPackagingMaterial = Convert.ToInt32(strTemp.Replace(productLotPackagingMaterial, string.Empty));
                }
                #endregion

                int beginImport = 16;
                for (int i = beginImport; i <= count; i++)
                {
                    string productCode = $"{values[i, 2]}";
                    string productDescr = $"{values[i, 3]}";

                    if (String.IsNullOrWhiteSpace(productCode))
                        break;

                    int lineNbr = int.Parse($"{values[i, 1]}");

                    //// Validate product
                    var product = products.FirstOrDefault(p => p.ProductCode == productCode);
                    if (product == null)
                        throw new WrappedException(String.Format(Messages.Validate_Product_NotExist, productDescr));

                    //// Validate request quantity
                    decimal planQty;
                    if (!decimal.TryParse($"{values[i, 9]}", out planQty))
                        throw new WrappedException(Messages.Validate_Required_RequestQty);
                    if (planQty < 0)
                        throw new WrappedException(Messages.Validate_Range_RequestQty);

                    var line = details.FirstOrDefault(p => p.LineNbr == lineNbr);
                    if (line == null)
                    {
                        line = new StockReceivingDetail();
                        line.LineNbr = lineNbr;
                        line.QuantityReceived = 0;
                        line.PalletReceived = 0;

                        details.Add(line);
                    }

                    double e_date;
                    double.TryParse($"{values[i, 6]}", out e_date);
                    string str_Edate = $"{values[i, 6]}";
                    DateTime Expired_Date = e_date > 0 ? DateTime.FromOADate(e_date) : DateTime.Parse(str_Edate);

                    double m_date;
                    double.TryParse($"{values[i, 10]}", out m_date);
                    string str_Mdate = $"{values[i, 10]}";
                    DateTime ManufacturingDate = m_date > 0 ? DateTime.FromOADate(m_date) : DateTime.Parse(str_Mdate);

                    line.ProductID = product.ProductID;
                    line.ProductCode = productCode;
                    line.ProductDescription = productDescr;
                    line.CompanyName = companyName;
                    line.QuantityPlanning = planQty;
                    line.UOM = $"{values[i, 8]}";
                    line.POCode = $"{values[i, 7]}";
                    line.POLine = $"{values[i, 1]}";
                    line.DeliveryDate = DateTime.Today;
                    line.DeliveryHour = "";
                    line.Description = $"{values[i, 11]}";
                    line.Receiver = "";
                    line.ExpiryDate = Expired_Date;
                    line.ManufacturingDate = ManufacturingDate;

                    line.BatchCodeDistributor = $"{values[i, 5]}";

                    var packaging = productPackagings.FirstOrDefault(p => p.ProductID == product.ProductID);
                    if (packaging != null)
                    {
                        line.PalletPlanning = (decimal)Math.Ceiling((double)(planQty / packaging.Quantity));
                    }
                }

                //Process detail after getting all of them
                var listProductCode = details.Select(a => new { a.ProductCode, a.BatchCodeDistributor, a.ProductDescription }).Distinct().ToList();
                List<StockReceivingDetail> listSummed = new List<StockReceivingDetail>();
                int sumLineNbr = 0;
                foreach (var item in listProductCode)
                {
                    sumLineNbr++;
                    StockReceivingDetail itemBase = details.First(a => a.ProductCode == item.ProductCode && a.BatchCodeDistributor == item.BatchCodeDistributor);

                    StockReceivingDetail itemSum = new StockReceivingDetail();
                    itemSum.LineNbr = sumLineNbr;
                    itemSum.QuantityReceived = 0;
                    itemSum.PalletReceived = 0;
                    itemSum.ProductID = itemBase.ProductID;
                    itemSum.ProductCode = itemBase.ProductCode;
                    itemSum.ProductDescription = itemBase.ProductDescription;
                    itemSum.CompanyName = itemBase.CompanyName;
                    itemSum.BatchCodeDistributor = itemBase.BatchCodeDistributor;
                    itemSum.QuantityPlanning = details.Where(a => a.ProductCode == item.ProductCode && a.BatchCodeDistributor == item.BatchCodeDistributor).Sum(a => a.QuantityPlanning);
                    itemSum.UOM = itemBase.UOM;
                    itemSum.POCode = string.Join(" ;", details.Where(a => a.ProductCode == item.ProductCode && a.BatchCodeDistributor == item.BatchCodeDistributor).Select(a => a.POCode));
                    itemSum.POLine = string.Join(" ;", details.Where(a => a.ProductCode == item.ProductCode && a.BatchCodeDistributor == item.BatchCodeDistributor).Select(a => a.POLine));
                    itemSum.DeliveryDate = itemBase.DeliveryDate;
                    itemSum.DeliveryHour = "";
                    itemSum.Description = string.Join(" ;", details.Where(a => a.ProductCode == item.ProductCode && a.BatchCodeDistributor == item.BatchCodeDistributor).Select(a => a.Description));
                    itemSum.Receiver = "";
                    itemSum.ExpiryDate = itemBase.ExpiryDate;
                    itemSum.ManufacturingDate = itemBase.ManufacturingDate;
                    itemSum.PalletPlanning = details.Where(a => a.ProductCode == item.ProductCode && a.BatchCodeDistributor == item.BatchCodeDistributor).Sum(a => a.PalletPlanning);

                    var product = products.FirstOrDefault(p => p.ProductCode == itemSum.ProductCode);
                    if (product.Type == Product.type.F)
                    {
                        intCurrentProductLotFinishProduct += 1;
                        itemSum.BatchCode = productLotFinishProduct + intCurrentProductLotFinishProduct.ToString("00");
                    }
                    else if (product.Type == Product.type.M)
                    {
                        intCurrentProductLotRawMaterial += 1;
                        itemSum.BatchCode = productLotRawMaterial + intCurrentProductLotRawMaterial.ToString("00");
                    }
                    else if (product.Type == Product.type.P)
                    {
                        intCurrentProductLotPackagingMaterial += 1;
                        itemSum.BatchCode = productLotPackagingMaterial + intCurrentProductLotPackagingMaterial.ToString("00");
                    }

                    if (item.ProductCode.StartsWith("A"))
                    {
                        //Set default ProductLot for Promotion Product
                        //A0060703 -> 0060703A00
                        string temp = item.ProductCode.Replace("A", "");
                        temp = "0000" + temp;
                        temp = temp.Substring(temp.Length - 6, 6) + "Y" + "00";
                        itemSum.BatchCode = temp;
                    }
                    else if (item.ProductDescription.StartsWith("AQ"))
                    {
                        //Set default ProductLot for Promotion Product
                        //A0060703 -> 0060703A00
                        string temp = item.ProductCode.Substring(0, 6);
                        temp = temp + "Y" + item.ProductCode.Substring(item.ProductCode.Length - 2, 2);
                        itemSum.BatchCode = temp;
                    }

                    listSummed.Add(itemSum);
                }

                //Write data back to Excel
                try
                {
                    for (int i = beginImport; i <= count; i++)
                    {
                        string productCode = $"{values[i, 2]}";
                        string batchCodeDistributor = $"{values[i, 5]}";
                        batchCodeDistributor = batchCodeDistributor == null ? string.Empty : batchCodeDistributor;
                        //Ghi ra dòng batchcode tự động
                        var itemHaveBatchCode = listSummed.FirstOrDefault(a => a.ProductCode == productCode && a.BatchCodeDistributor == batchCodeDistributor);
                        if (itemHaveBatchCode != null)
                        {
                            string batchCode = itemHaveBatchCode.BatchCode;
                            sheetDetail.Cells[i, 4] = batchCode;
                        }
                    }

                    excelApp.DisplayAlerts = false;
                    workbook.SaveAs(fileName);
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }

                _mainView.StockReceivingHeader = header;
                _mainView.StockReceivingDetails = listSummed;
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(String.Format(Messages.Information_ImportSucessfullyWithError, String.Join(",", errorLine)), Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReceivingHeader;
            var details = _mainView.StockReceivingDetails;
            var deletedDetails = _mainView.DeletedStockReceivingDetails;
            bool isSuccess = false;

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _stockReceivingHeaderRepository = unitOfWork.Register(_stockReceivingHeaderRepository.GetType()) as IStockReceivingHeaderRepository;
                    _stockReceivingDetailRepository = unitOfWork.Register(_stockReceivingDetailRepository.GetType()) as IStockReceivingDetailRepository;

                    if (String.IsNullOrWhiteSpace(header.StockReceivingCode))
                    {
                        string code = await _stockReceivingHeaderRepository.GetNextNBR();
                        header.StockReceivingCode = code;
                        await _stockReceivingHeaderRepository.Insert(header);
                    }
                    else
                        await _stockReceivingHeaderRepository.Update(header, null, new string[] { "ImportStatus", "VerifyStatus" }, new object[] { header.StockReceivingCode });

                    //// foreach deleted DO details and delete it
                    foreach (var item in deletedDetails)
                    {
                        await _stockReceivingDetailRepository.Delete(new object[] { item.StockReceivingCode, item.LineNbr });
                    }

                    foreach (var item in details)
                    {
                        item.StockReceivingCode = header.StockReceivingCode;

                        await _stockReceivingDetailRepository.InsertOrUpdate(item, null, new string[] { "Status", "QuantityReceived" }, new object[] { item.StockReceivingCode, item.LineNbr });
                    }

                    await unitOfWork.Commit();
                }

                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _stockReceivingHeaderRepository = _mainPresenter.Resolve(_stockReceivingHeaderRepository.GetType()) as IStockReceivingHeaderRepository;
                _stockReceivingDetailRepository = _mainPresenter.Resolve(_stockReceivingDetailRepository.GetType()) as IStockReceivingDetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);

                    await LoadStockReceivingHeaders();
                    _mainView.StockReceivingHeader = header;
                }
            }

        }

        public override async Task Delete()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReceivingHeader;

            bool isSuccess = false;
            try
            {
                if (header.VerifyStatusDisplay != StockReceivingHeader.status.New || header.ImportStatusDisplay != StockReceivingHeader.status.New)
                {
                    if (header.VerifyStatusDisplay == StockReceivingHeader.status.Processing)
                        throw new WrappedException("Phiếu nhập kho đang được kiểm tra không thể xóa");
                    if (header.VerifyStatusDisplay == StockReceivingHeader.status.Completed)
                        throw new WrappedException("Phiếu nhập kho đã kiểm tra hoàn tất không thể xóa");
                    if (header.ImportStatusDisplay == StockReceivingHeader.status.Processing)
                        throw new WrappedException("Phiếu nhập kho đang được thực hiện không thể xóa");
                    if (header.ImportStatusDisplay == StockReceivingHeader.status.Completed)
                        throw new WrappedException("Phiếu nhập kho đã hoàn tất không thể xóa");
                }

                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _stockReceivingHeaderRepository = unitOfWork.Register(_stockReceivingHeaderRepository.GetType()) as IStockReceivingHeaderRepository;

                    await _stockReceivingHeaderRepository.ExecuteNonQuery("proc_StockReceivingHeaders_Delete",
                        new SqlParameter { ParameterName = "@StockReceivingCode", SqlDbType = SqlDbType.VarChar, Value = header.StockReceivingCode });

                    await unitOfWork.Commit();
                }


                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                _stockReceivingHeaderRepository = _mainPresenter.Resolve(_stockReceivingHeaderRepository.GetType()) as IStockReceivingHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadStockReceivingHeaders();
                    Insert();
                }
            }
        }

        public async Task Export()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReceivingHeader;
            var details = _mainView.StockReceivingDetails;

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuNhapHang.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 16;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                int i = 0;
                foreach (var item in details)
                {
                    i++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = (i).ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductDescription;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCodeDistributor;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ExpiryDate.Value.ToString("MM/dd/yyyy");
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.POCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.UOM;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.QuantityPlanning;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ManufacturingDate.Value.ToString("MM/dd/yyyy");
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Description;
                    cellColumnIndex++;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A16", "K" + (cellRowIndex - 1).ToString());
                foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                {
                    cell.BorderAround();
                }
                #endregion

                #region Fill Data Header
                sheetDetail.Cells[7, 3] = header.CompanyName;
                sheetDetail.Cells[7, 10] = header.DeliveryDate.ToString("MM/dd/yyyy");
                sheetDetail.Cells[8, 10] = header.StockReceivingCode;
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "PhieuNhapHang" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        public async Task ExportPalletLabel(string strProductLot, int intNumberToPrint, string strPrinterName)
        {
            var details = _mainView.StockReceivingDetails;
            StockReceivingDetail data = details.FirstOrDefault(a => a.BatchCode == strProductLot);

            var printers = System.Drawing.Printing.PrinterSettings.InstalledPrinters;
            int printerIndex = 0;
            foreach (String s in printers)
            {
                if (s.Equals(strPrinterName))
                {
                    break;
                }
                printerIndex++;
            }

            //I -get next number of print multiple time
            string lastestPrintedNBR = await _stockReceivingDetailRepository.ExecuteScalar("proc_StockReceivingDetail_GetLastedPrintedNbr",
                new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = data.StockReceivingCode },
                new SqlParameter { ParameterName = "BatchCode", SqlDbType = SqlDbType.VarChar, Value = data.BatchCode }
                );

            int lastestNBR = string.IsNullOrEmpty(lastestPrintedNBR) ? 0 : Convert.ToInt32(lastestPrintedNBR);

            int lastNumber = lastestNBR + intNumberToPrint;
            lastestNBR++;

            var series = Enumerable.Range(lastestNBR, lastNumber).ToList();

            string savePath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\Pallet_ProductLot_Material\\";
            Utility.CreateDirectory(savePath);
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(savePath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            for (int i = lastestNBR; i <= lastNumber; i++)
            //foreach (var i in series)
            {
                //var result = Task.Run(async () => { return await ExportPalletLabelDetail(strProductLot, i, printerIndex); }).Result;
                //Task.Run(async () => { await ExportPalletLabelDetail(strProductLot, i, printerIndex); }).Wait();
                await ExportPalletLabelDetail(strProductLot, i, printerIndex, savePath);
                Thread.Sleep(4000);
            }

            //Save printed NBR
            await _stockReceivingDetailRepository.ExecuteDataTable("proc_StockReceivingDetail_UpdatePrintedNbr",
                new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = data.StockReceivingCode },
                new SqlParameter { ParameterName = "BatchCode", SqlDbType = SqlDbType.VarChar, Value = data.BatchCode },
                new SqlParameter { ParameterName = "BatchCodePrintedNbr", SqlDbType = SqlDbType.Int, Value = lastNumber },
                new SqlParameter { ParameterName = "UserID", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                new SqlParameter { ParameterName = "MethodID", SqlDbType = SqlDbType.VarChar, Value = "PrintPalletLabel" }
                );
        }

        public async Task ExportPalletLabelDetail(string strProductLot, int nbr, int printerIndex, string savePath)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReceivingHeader;
            var details = _mainView.StockReceivingDetails;
            var products = await _productRepository.GetAsync();

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\Pallet_ProductLot_Material.xlsx";

            bool isSuccess = false;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                StockReceivingDetail data = details.FirstOrDefault(a => a.BatchCode == strProductLot);
                var product = products.Where(p => p.ProductCode == data.ProductCode).FirstOrDefault();

                sheetDetail.Cells[1, 1] = data.ProductDescription;
                sheetDetail.Cells[2, 1] = data.BatchCodeDistributor;
                sheetDetail.Cells[3, 1] = data.BatchCode;

                //Tên: HADACLEAN A -20KG BAG
                //Lô NCC:  180130R01
                //Lô:           180130R01
                //NSX:        30 / 01 / 2018 HSD: 29 / 01 / 2019
                //5745457520180130R012901191189
                //57454575 - 20 - 180130R01 - 290119 - 1189
                sheetDetail.Cells[5, 3] = "Tên: " + product.Description;
                sheetDetail.Cells[7, 3] = "Lô NCC:  " + data.BatchCodeDistributor;
                sheetDetail.Cells[8, 3] = "Lô:           " + data.BatchCode;
                string strNSX = "NSX:        " + data.ManufacturingDate.Value.ToString("dd/MM/yyyy") + " HSD: " + data.ExpiryDate.Value.ToString("dd/MM/yyyy") + "";
                sheetDetail.Cells[9, 3] = strNSX;
                product.QuantityByUnit = product.QuantityByUnit ?? 0;
                string barCodePrint = product.ProductCode + product.QuantityByUnit.Value.ToString("00") + data.BatchCode + data.ExpiryDate.Value.ToString("ddMMyy") + nbr.ToString("0000");
                sheetDetail.Cells[14, 3] = product.ProductCode + "-" + product.QuantityByUnit.Value.ToString("00") + "-" + data.BatchCode + "-" + data.ExpiryDate.Value.ToString("ddMMyy") + "-" + nbr.ToString("0000");

                //Image myimg = Code128Rendering.MakeBarcodeImage(barCodePrint, 1, false);
                BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                Image myimg = b.Encode(BarcodeLib.TYPE.CODE128, barCodePrint, Color.Black, Color.White, 222, 100);
                object barcode128PrintCell = "C10";
                Microsoft.Office.Interop.Excel.Range rangeBitmap2 = sheetDetail.get_Range(barcode128PrintCell, barcode128PrintCell);
                Clipboard.SetDataObject(myimg);
                //Paste the barcode image
                rangeBitmap2.Select();
                //Interop params
                object oMissing2 = System.Reflection.Missing.Value;
                sheetDetail.Paste(oMissing2, oMissing2);

                #region QRCodePrint
                //Find the predefined barcode cell into the worksheet
                object barcodePrintCell = "D4";
                Microsoft.Office.Interop.Excel.Range rangeBitmap1 = sheetDetail.get_Range(barcodePrintCell, barcodePrintCell);
                //Copy the barcode image into Clipboard
                Bitmap img1 = QRCoder.Utility.RenderQrCodeNoText(barCodePrint);
                Bitmap resizedImg1 = new Bitmap(120, 120);

                double ratioX1 = (double)resizedImg1.Width / (double)img1.Width;
                double ratioY1 = (double)resizedImg1.Height / (double)img1.Height;
                double ratio1 = ratioX1 < ratioY1 ? ratioX1 : ratioY1;

                int newHeight1 = Convert.ToInt32(img1.Height * ratio1);
                int newWidth1 = Convert.ToInt32(img1.Width * ratio1);

                using (Graphics g = Graphics.FromImage(resizedImg1))
                {
                    g.DrawImage(img1, 0, 0, newWidth1, newHeight1);
                }
                Clipboard.SetDataObject(resizedImg1);
                //Paste the barcode image
                rangeBitmap1.Select();
                //Interop params
                object oMissing1 = System.Reflection.Missing.Value;
                sheetDetail.Paste(oMissing1, oMissing1);
                #endregion

                #region ProductLotQRCode
                string productLotNbr = strProductLot + nbr.ToString("-000");
                //Find the predefined barcode cell into the worksheet
                object barcodeCell = "A5";
                Microsoft.Office.Interop.Excel.Range rangeBitmap = sheetDetail.get_Range(barcodeCell, barcodeCell);
                //Copy the barcode image into Clipboard
                Bitmap img = QRCoder.Utility.RenderQrCode(productLotNbr);
                Bitmap resizedImg = new Bitmap(280, 280);

                double ratioX = (double)resizedImg.Width / (double)img.Width;
                double ratioY = (double)resizedImg.Height / (double)img.Height;
                double ratio = ratioX < ratioY ? ratioX : ratioY;

                int newHeight = Convert.ToInt32(img.Height * ratio);
                int newWidth = Convert.ToInt32(img.Width * ratio);

                using (Graphics g = Graphics.FromImage(resizedImg))
                {
                    g.DrawImage(img, 0, 0, newWidth, newHeight);
                }
                Clipboard.SetDataObject(resizedImg);
                //Paste the barcode image
                rangeBitmap.Select();
                //Interop params
                object oMissing = System.Reflection.Missing.Value;
                sheetDetail.Paste(oMissing, oMissing);
                #endregion

                excelApp.DisplayAlerts = false;
                string exportPath = savePath;
                exportPath += "Pallet_ProductLot_Material" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
                sheetDetail.PageSetup.PrintArea = ("A1:D15");

                //Microsoft.Office.Interop.Excel.Range r = sheetDetail.get_Range("A1", "D15");
                //r.CopyPicture(Microsoft.Office.Interop.Excel.XlPictureAppearance.xlScreen,
                //                Microsoft.Office.Interop.Excel.XlCopyPictureFormat.xlBitmap);                

                //if (Clipboard.GetDataObject() != null)
                //{
                //    IDataObject data2 = Clipboard.GetDataObject();

                //    if (data2.GetDataPresent(DataFormats.Bitmap))
                //    {
                //        Image image = (Image)data2.GetData(DataFormats.Bitmap, true);
                //        string bigImageVerticalName = savePath + "\\" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".png";
                //        image.Save(bigImageVerticalName, System.Drawing.Imaging.ImageFormat.Jpeg);
                //    }
                //}

                //Workbook w = a.Workbooks.Open(exportPath);
                //Worksheet ws = w.Sheets["Report"];
                sheetDetail.Protect(Contents: false);
                //Range r = sheetDetail.Range["A1:D15"];
                //r.CopyPicture(XlPictureAppearance.xlScreen, XlCopyPictureFormat.xlBitmap);
                //string bigImageVerticalName = savePath + "\\" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".png";
                //Bitmap image = new Bitmap(Clipboard.GetImage());
                //image.Save(bigImageVerticalName);


                workbook.PrintOut(Type.Missing, Type.Missing, Type.Missing, Type.Missing, printerIndex, Type.Missing, Type.Missing, Type.Missing);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    //#region Open Excel File In Edit Mode
                    //ProcessStartInfo startInfo;
                    //startInfo = new ProcessStartInfo(exportPath);
                    //startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    //Process newProcess = new Process();
                    //newProcess.StartInfo = startInfo;
                    //newProcess.Start();  // <<=== here you default editor should start... 
                    //#endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        public async Task WarehouseManagerApproveImport()
        {
            await _stockReceivingDetailRepository.ExecuteDataTable("proc_StockReceiving_WarehouseManagerApproveImport",
                new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = _mainView.StockReceivingHeader.StockReceivingCode },
                new SqlParameter { ParameterName = "UserID", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                new SqlParameter { ParameterName = "MethodID", SqlDbType = SqlDbType.VarChar, Value = "WarehouseManagerApproveImport" }
                );

            _mainPresenter.SetMessage(Messages.Information_WarehouseManagerApproveImport, Utility.MessageType.Information);
        }
    }
}