﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IReportDeliveryBarcodeHistoryView : IBaseView
    {
        System.Data.DataTable ReportDeliveryBarcodeHistoryDetails { get; set; }
        System.Data.DataTable ReportDeliveryBarcodeHistoryTotals { get; set; }
        System.Data.DataTable ListCompanies { get; set; }
        System.Data.DataTable Sheet1 { get; set; }
        System.Data.DataTable Sheet2 { get; set; }
        System.Data.DataTable PalletList { get; set; }
    }

    public interface IReportDeliveryBarcodeHistoryPresenter : IBasePresenter
    {
        void Search(string Code, DateTime? fromDate, DateTime? toDate);

        void SearchDetail(string Code, DateTime? fromDate, DateTime? toDate);

        void LoadCompany(DateTime? fromDate, DateTime? toDate);

        Task Print();

        Task PrintAll();
    }

    public class ReportDeliveryBarcodeHistoryPresenter : BasePresenter, IReportDeliveryBarcodeHistoryPresenter
    {
        private IReportDeliveryBarcodeHistoryView _mainView;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository;
        private IDeliveryOrderDetailRepository _deliveryOrderDetailRepository;
        private IDeliveryNoteDetailRepository _deliveryNoteDetailRepository;

        public ReportDeliveryBarcodeHistoryPresenter(IReportDeliveryBarcodeHistoryView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository,
            IDeliveryOrderDetailRepository deliveryOrderDetailRepository,
            IDeliveryNoteDetailRepository deliveryNoteDetailRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryOrderHeaderRepository = deliveryOrderHeaderRepository;
            _deliveryOrderDetailRepository = deliveryOrderDetailRepository;
            _deliveryNoteDetailRepository = deliveryNoteDetailRepository;
            _mainView.Sheet1 = new System.Data.DataTable();
            _mainView.Sheet2 = new System.Data.DataTable();
            _mainView.ReportDeliveryBarcodeHistoryDetails = new System.Data.DataTable();
            _mainView.ReportDeliveryBarcodeHistoryTotals = new System.Data.DataTable();
            _mainView.ListCompanies = new System.Data.DataTable();
        }

        public async void Search(string Code, DateTime? fromDate, DateTime? toDate)
        {
            LoadData(Code, fromDate, toDate);
        }

        public async void SearchDetail(string Code, DateTime? fromDate, DateTime? toDate)
        {
            LoadDataDetail(Code, fromDate, toDate);
        }

        public async void LoadCompany(DateTime? fromDate, DateTime? toDate)
        {
            using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                List<SqlParameter> listParam = new List<SqlParameter>();
                SqlParameter paramFromDate = new SqlParameter("FromDate", fromDate);
                listParam.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter("ToDate", toDate);
                listParam.Add(paramToDate);
                Utility.LogEx("LoadCompany", string.Empty);
                Utility.LogEx(fromDate.ToString(), string.Empty);
                Utility.LogEx(toDate.ToString(), string.Empty);
                _mainView.ListCompanies = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_CompanyGetByDeliveryDate", listParam.ToArray());
                Utility.LogEx("ListCompanies" + _mainView.ListCompanies.Rows.Count.ToString(), string.Empty);
            }
        }

        private async void LoadData(string Code, DateTime? fromDate, DateTime? toDate)
        {
            using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                _deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;
                _deliveryNoteDetailRepository = unitOfWork.Register(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;

                List<SqlParameter> listParam = new List<SqlParameter>();
                SqlParameter code = new SqlParameter("CompanyCode", Code);
                listParam.Add(code);
                SqlParameter paramFromDate = new SqlParameter("FromDate", fromDate);
                listParam.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter("ToDate", toDate);
                listParam.Add(paramToDate);
                SqlParameter prSheetID = new SqlParameter("Sheet", 1);
                listParam.Add(prSheetID);
                _mainView.Sheet1 = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportDeliveryBarcodeHistory3", listParam.ToArray());
                _mainView.ReportDeliveryBarcodeHistoryTotals = _mainView.Sheet1;

                _mainView.PalletList = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_Pallets_SelectByDOImport",
                       new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = string.Empty });
            }
        }

        private async void LoadDataDetail(string Code, DateTime? fromDate, DateTime? toDate)
        {
            using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                _deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;
                _deliveryNoteDetailRepository = unitOfWork.Register(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;
                
                List<SqlParameter> listParam2 = new List<SqlParameter>();
                SqlParameter code2 = new SqlParameter("CompanyCode", Code);
                listParam2.Add(code2);
                SqlParameter paramFromDate2 = new SqlParameter("FromDate", fromDate);
                listParam2.Add(paramFromDate2);
                SqlParameter paramToDate2 = new SqlParameter("ToDate", toDate);
                listParam2.Add(paramToDate2);
                SqlParameter prSheetID2 = new SqlParameter("Sheet", 2);
                listParam2.Add(prSheetID2);
                _mainView.Sheet2 = await _deliveryOrderDetailRepository.ExecuteDataTable("proc_ReportDeliveryBarcodeHistory3", listParam2.ToArray());

                List<SqlParameter> listParam3 = new List<SqlParameter>();
                SqlParameter code3 = new SqlParameter("CompanyCode", Code);
                listParam3.Add(code3);
                SqlParameter paramFromDate3 = new SqlParameter("FromDate", fromDate);
                listParam3.Add(paramFromDate3);
                SqlParameter paramToDate3 = new SqlParameter("ToDate", toDate);
                listParam3.Add(paramToDate3);
                SqlParameter prSheetID3 = new SqlParameter("Sheet", 3);
                listParam3.Add(prSheetID3);
                _mainView.ReportDeliveryBarcodeHistoryDetails = await _deliveryNoteDetailRepository.ExecuteDataTable("proc_ReportDeliveryBarcodeHistory3", listParam3.ToArray());
            }
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var details = _mainView.Sheet1;
            var details2 = _mainView.Sheet2;
            var details3 = _mainView.ReportDeliveryBarcodeHistoryDetails;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheet1 = null;
            //_Worksheet sheet2 = null;
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\ReportDeliveryBarcodeHistory.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";

            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 8;
            int cellColumnIndex = 1;
            string strDeliveryDate = string.Empty;
            string strCompanyName = string.Empty;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);

                sheet1 = (Worksheet)workbook.Sheets[1];
                sheetDetail = (Worksheet)workbook.Sheets[2];

                if (details.Rows.Count > 0)
                {
                    string temp = details.Rows[0]["DeliveryDate"].ToString();
                    DateTime date = Convert.ToDateTime(temp);
                    strDeliveryDate = date.ToString("dd/MM/yyyy");
                    strCompanyName = details.Rows[0]["CompanyName"].ToString();
                }

                #region Sheet 3
                #region Fill Data Detail1
                if (details.Rows.Count > 0)
                {
                    int indexNBR = 0;
                    for (int i = 0; i < details.Rows.Count; i++)
                    {
                        indexNBR++;
                        sheet1.Cells[cellRowIndex, cellColumnIndex] = (indexNBR).ToString();
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductDescription"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductLot"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["Quantity"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CartonOddQuantity"];
                        cellColumnIndex++;

                        //End of Row
                        cellColumnIndex = 1;
                        cellRowIndex++;
                    }
                }
                #endregion                

                #region Fill Data Detail3
                cellRowIndex = 8;
                cellColumnIndex = 1;
                if (details3.Rows.Count > 0)
                {
                    int indexNBR = 0;
                    for (int i = 0; i < details3.Rows.Count; i++)
                    {
                        indexNBR++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = (indexNBR).ToString();
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["ProductDescription"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["ProductLot"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["CartonBarcode"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["EncryptedProductBarcode"];
                        cellColumnIndex++;

                        //End of Row
                        cellColumnIndex = 1;
                        cellRowIndex++;
                    }
                }
                //Microsoft.Office.Interop.Excel.Range rangeDetail3 = sheetDetail.get_Range("A8", "E" + (cellRowIndex - 1).ToString());
                //foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail3.Cells)
                //{
                //    cell.BorderAround();
                //}
                #endregion
                #endregion

                #region Fill Data Header
                sheet1.get_Range("C5", "C5").Cells[0] = strDeliveryDate;
                sheet1.get_Range("C6", "C6").Cells[0] = strCompanyName;

                //sheet2.get_Range("C5", "C5").Cells[0] = strDeliveryDate;
                //sheet2.get_Range("C6", "C6").Cells[0] = strCompanyName;

                sheetDetail.get_Range("C5", "C5").Cells[0] = strDeliveryDate;
                sheetDetail.get_Range("C6", "C6").Cells[0] = strCompanyName;
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "BangKeChiTietGiaoHang" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion
                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        public async Task PrintAll()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var details = _mainView.Sheet1;
            var details2 = _mainView.Sheet2;
            var details3 = _mainView.ReportDeliveryBarcodeHistoryDetails;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheet1 = null;
            //_Worksheet sheet2 = null;
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\ReportDeliveryBarcodeHistoryAll.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";

            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 6;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);

                sheet1 = (Worksheet)workbook.Sheets[1];
                sheetDetail = (Worksheet)workbook.Sheets[2];

                #region Sheet 3
                #region Fill Data Detail1
                if (details.Rows.Count > 0)
                {
                    int indexNBR = 0;
                    for (int i = 0; i < details.Rows.Count; i++)
                    {
                        indexNBR++;
                        sheet1.Cells[cellRowIndex, cellColumnIndex] = (indexNBR).ToString();
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["StrDeliveryDate"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CompanyCode"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CompanyName"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductDescription"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductLot"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["Quantity"];
                        cellColumnIndex++;

                        sheet1.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CartonOddQuantity"];
                        cellColumnIndex++;

                        //End of Row
                        cellColumnIndex = 1;
                        cellRowIndex++;
                    }
                }
                #endregion                

                #region Fill Data Detail3
                cellRowIndex = 6;
                cellColumnIndex = 1;
                if (details3.Rows.Count > 0)
                {
                    int indexNBR = 0;
                    for (int i = 0; i < details3.Rows.Count; i++)
                    {
                        indexNBR++;
                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = (indexNBR).ToString();
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["StrDeliveryDate"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["CompanyCode"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["CompanyName"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["ProductDescription"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["ProductLot"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["CartonBarcode"];
                        cellColumnIndex++;

                        sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details3.Rows[i]["EncryptedProductBarcode"];
                        cellColumnIndex++;

                        //End of Row
                        cellColumnIndex = 1;
                        cellRowIndex++;
                    }
                }
                #endregion
                #endregion

                #region Fill Data Header
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "BangKeChiTietGiaoHang" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion
                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }
    }
}
