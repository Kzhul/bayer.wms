﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Drawing;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IDeliveryOrderView : IBaseView
    {
        DeliveryOrderHeader DeliveryOrderHeader { get; set; }

        System.Data.DataTable DeliveryOrderHeaders { set; }

        IList<DeliveryOrderDetail> DeliveryOrderDetails { get; set; }

        IList<DeliveryOrderDetail> DeletedDeliveryOrderDetails { get; set; }

        System.Data.DataTable ReportDODetails { get; set; }

        System.Data.DataTable ReportLocationSuggest { get; set; }

        System.Data.DataTable ReportDOProcess { get; set; }

        System.Data.DataTable ReportExportProcess { get; set; }

        System.Data.DataTable PalletList { get; set; }
    }

    public interface IDeliveryOrderPresenter : IBasePresenter
    {
        Task<bool> LoadDeliveryOrderDetails(string doImportCode);

        Task LoadDeliveryOrderHeaders();

        Task ImportExcel(string fileName, string updateNote);

        Task CorrectAdditionalDO(string fileName, string updateNote);

        Task UpdateRelateDO();

        Task CreateSplitNote();

        Task CreateDeliveryNote();

        void DeleteDeliveryOrderDetail(DeliveryOrderDetail prodPacking);

        Task<DeliveryNoteFull> CreateDeliveryNoteTemp();

        Task<SplitNoteFull> CreateSplitNoteTemp(string PartnerCode);

        Task<SplitNoteFull> CreateSplitNoteTemp();

        Task<int> UpdateStatusDeliveryOrderByDeliveryNote();

        Task<List<string>> CreateSplitNoteFull();

        Task CreatePrepareNote();

        Task Print();

        Task Export();

        Task ExportProcess();

        Task<bool> ConfirmDeliveryFinish();
    }

    public class DeliveryOrderPresenter : BasePresenter, IDeliveryOrderPresenter
    {
        #region Construction
        private IDeliveryOrderView _mainView;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository;
        private IDeliveryOrderDetailRepository _deliveryOrderDetailRepository;
        private IDeliveryNoteHeaderRepository _deliveryNoteHeaderRepository;
        private IDeliveryNoteDetailRepository _deliveryNoteDetailRepository;
        private ISplitNoteHeaderRepository _splitNoteHeaderRepository;
        private ISplitNoteDetailRepository _splitNoteDetailRepository;
        private IPrepareNoteHeaderRepository _prepareNoteHeaderRepository;
        private IPrepareNoteDetailRepository _prepareNoteDetailRepository;
        private IProductRepository _productRepository;
        private ICompanyRepository _companyRepository;
        private IProductPackingRepository _productPackingRepository;
        private IUserRepository _userRepository;
        private bool dataChanged = false;

        public DeliveryOrderPresenter(IDeliveryOrderView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryOrderHeaderRepository deliveryOrderRepository,
            IDeliveryOrderDetailRepository deliveryOrderImportLogRepository,
            IDeliveryNoteHeaderRepository deliveryNoteHeaderRepository,
            IDeliveryNoteDetailRepository deliveryNoteDetailRepository,
            ISplitNoteHeaderRepository splitNoteHeaderRepository,
            ISplitNoteDetailRepository splitNoteDetailRepository,
            IPrepareNoteHeaderRepository prepareNoteHeaderRepository,
            IPrepareNoteDetailRepository prepareNoteDetailRepository,
            IProductRepository productRepository,
            ICompanyRepository companyRepository,
            IProductPackingRepository productPackingRepository,
            IUserRepository userRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryOrderHeaderRepository = deliveryOrderRepository;
            _deliveryOrderDetailRepository = deliveryOrderImportLogRepository;
            _deliveryNoteHeaderRepository = deliveryNoteHeaderRepository;
            _deliveryNoteDetailRepository = deliveryNoteDetailRepository;
            _splitNoteHeaderRepository = splitNoteHeaderRepository;
            _splitNoteDetailRepository = splitNoteDetailRepository;
            _prepareNoteHeaderRepository = prepareNoteHeaderRepository;
            _prepareNoteDetailRepository = prepareNoteDetailRepository;
            _productRepository = productRepository;
            _companyRepository = companyRepository;
            _productPackingRepository = productPackingRepository;
            _userRepository = userRepository;

            _mainView.DeliveryOrderDetails = new List<DeliveryOrderDetail>();
            _mainView.DeletedDeliveryOrderDetails = new List<DeliveryOrderDetail>();
            _mainView.ReportDODetails = new System.Data.DataTable();
            _mainView.ReportLocationSuggest = new System.Data.DataTable();
            _mainView.ReportDOProcess = new System.Data.DataTable();
            _mainView.ReportExportProcess = new System.Data.DataTable();
            _mainView.PalletList = new System.Data.DataTable();

        }
        #endregion

        #region Load Data
        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            var model = selectedItem.ToEntity<DeliveryOrderHeader>();
            _mainView.DeliveryOrderHeader = model;
            // await LoadDeliveryOrderDetails(_mainView.DeliveryOrderHeader.DOImportCode);
            var t1 = LoadDeliveryOrderDetails(_mainView.DeliveryOrderHeader.DOImportCode);
            await Task.WhenAll(t1);
        }

        public async Task LoadDeliveryOrderHeaders()
        {
            var model = await _deliveryOrderHeaderRepository.GetAsync(p => !p.IsDeleted);
            _mainView.DeliveryOrderHeaders = model.OrderByDescending(a => a.CreatedDateTime).ToList().ToDataTable();
            //if (_mainView.DeliveryOrderHeader != null)
            //{
            //    await LoadDeliveryOrderDetails(_mainView.DeliveryOrderHeader.DOImportCode);
            //}
        }

        public async Task<bool> LoadDeliveryOrderDetails(string doImportCode)
        {
            //using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                //_deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                //_deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;

                _mainView.DeliveryOrderDetails = await _deliveryOrderDetailRepository.GetIncludeProductAsync(p => p.DOImportCode == doImportCode);

                //Thread.Sleep(2000);
                //_mainView.ReportLocationSuggest = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_PalletStatuses_SuggestionForDO",
                //         new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = doImportCode });


                _mainView.ReportExportProcess = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_DeliveryOrderDetails_ReportExportProcess",
                        new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = doImportCode });
                //Thread.Sleep(2000);
                Utility.LogEx(_mainView.ReportExportProcess.Rows.Count.ToString(), "ReportExportProcess");
                _mainView.ReportDOProcess = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_DeliveryOrderDetails_ReportProcess",
                         new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = doImportCode });
                Utility.LogEx(_mainView.ReportDOProcess.Rows.Count.ToString(), "ReportDOProcess");
                //Thread.Sleep(2000);
                _mainView.PalletList = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_Pallets_SelectByDOImport",
                        new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = doImportCode });
                //Thread.Sleep(2000);
                List<SqlParameter> listParam = new List<SqlParameter>();
                SqlParameter code = new SqlParameter("DOImportCode", doImportCode);
                listParam.Add(code);
                SqlParameter paramFromDate = new SqlParameter("FromDate", DBNull.Value);
                listParam.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter("ToDate", DBNull.Value);
                listParam.Add(paramToDate);
                _mainView.ReportDODetails = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportDO", listParam.ToArray());

                return true;
            }
        }

        public async Task<bool> ConfirmDeliveryFinish()
        {
            List<SqlParameter> listParam = new List<SqlParameter>();
            SqlParameter code = new SqlParameter("DOImportCode", _mainView.DeliveryOrderHeader.DOImportCode);
            listParam.Add(code);
            SqlParameter param2 = new SqlParameter("UserID", LoginInfo.UserID);
            listParam.Add(param2);
            SqlParameter param3 = new SqlParameter("SiteMapID", 47);
            listParam.Add(param3);
            await _deliveryOrderHeaderRepository.ExecuteNonQuery("proc_DeliveryOrderHeader_ConfirmDeliveryFinish", listParam.ToArray());

            return true;
        }
                

        //public async Task LoadReportDO(string DOCode)
        //{
        //    using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
        //    {
        //        _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;

        //        List<SqlParameter> listParam = new List<SqlParameter>();
        //        SqlParameter code = new SqlParameter("DOImportCode", DOCode);
        //        listParam.Add(code);
        //        SqlParameter paramFromDate = new SqlParameter("FromDate", DBNull.Value);
        //        listParam.Add(paramFromDate);
        //        SqlParameter paramToDate = new SqlParameter("ToDate", DBNull.Value);
        //        listParam.Add(paramToDate);
        //        _mainView.ReportDODetails = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_ReportDO", listParam.ToArray());
        //    }
        //}
        #endregion

        #region CreateTicketRelated
        public async Task<DeliveryNoteFull> CreateDeliveryNoteTemp()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            DeliveryNoteFull model = new DeliveryNoteFull();

            await Task.Run(() =>
            {
                var header = _mainView.DeliveryOrderHeader;
                var details = _mainView.DeliveryOrderDetails;
                if (details.Count > 0)
                    header.DeliveryDate = details[0].DeliveryDate;

                #region Header

                //string DOImportCode = await _deliveryNoteHeaderRepository.GetNextNBR();
                DeliveryNoteHeader _DNHeader = new DeliveryNoteHeader();
                _DNHeader.ExportCode = string.Empty; //DOImportCode;
                _DNHeader.DOImportCode = header.DOImportCode;
                _DNHeader.DeliveryDate = header.DeliveryDate;
                _DNHeader.Status = DeliveryNoteHeader.status.New;
                _DNHeader.IsDeleted = false;

                model.header = _DNHeader;

                #endregion

                #region Details

                var listMaterialCodeBatchCode = details.Select(a => new { a.ProductID, a.BatchCode }).Distinct().ToList();
                var listDeliveryNoteDetailCreated = _deliveryNoteDetailRepository.Get(a => a.DOCode == header.DOImportCode);
                var listPacking = _productPackingRepository.Get();
                var listProduct = _productRepository.Get();

                model.listDetail = new List<DeliveryNoteDetail>();
                foreach (var item in listMaterialCodeBatchCode)
                {
                    DeliveryNoteDetail _DNDetail = new DeliveryNoteDetail();
                    _DNDetail.ExportCode = _DNHeader.ExportCode;
                    _DNDetail.Status = DeliveryNoteHeader.status.New;

                    _DNDetail.DOCode = header.DOImportCode;
                    _DNDetail.ProductID = item.ProductID;
                    _DNDetail.ProductCode = details.FirstOrDefault(a => a.ProductID == item.ProductID).ProductCode;
                    _DNDetail.ProductDescription = details.FirstOrDefault(a => a.ProductID == item.ProductID).ProductName;
                    _DNDetail.BatchCode = item.BatchCode;

                    decimal quantityOrder = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).Select(a => a.Quantity ?? 0).Sum();
                    decimal quantityCreated = listDeliveryNoteDetailCreated.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).Select(a => a.Quantity ?? 0).Sum();

                    _DNDetail.Quantity = quantityOrder - quantityCreated;

                    //Get list of product packing, order by carton, pallet
                    var productPacking = listPacking.Where(a => a.Type != "P" && a.ProductID == item.ProductID).OrderBy(a => a.Type).FirstOrDefault();
                    var product = listProduct.Where(a => a.ProductID == item.ProductID).FirstOrDefault();
                    if (productPacking != null && productPacking.Quantity.HasValue && productPacking.Quantity > 0)
                    {
                        _DNDetail.PackQuantity = (int)(_DNDetail.Quantity / productPacking.Quantity);
                        _DNDetail.ProductQuantity = _DNDetail.Quantity % productPacking.Quantity;
                        _DNDetail.PackType = product.PackingType;
                    }
                    else
                    {
                        _DNDetail.ProductQuantity = _DNDetail.Quantity;
                    }

                    if (_DNDetail.Quantity > 0)
                    {
                        model.listDetail.Add(_DNDetail);
                    }
                }

                #endregion
            });

            return model;
        }

        public async Task<SplitNoteFull> CreateSplitNoteTemp()
        {
            SplitNoteFull model = new SplitNoteFull();

            await Task.Run(() =>
            {
                var header = _mainView.DeliveryOrderHeader;
                var details = _mainView.DeliveryOrderDetails;
                if (details.Count > 0)
                    header.DeliveryDate = details[0].DeliveryDate;

                #region Header
                //string DOImportCode = await _splitNoteHeaderRepository.GetNextNBR();
                SplitNoteHeader modelHeader = new SplitNoteHeader();
                modelHeader.SplitCode = string.Empty; //DOImportCode;
                modelHeader.DOImportCode = header.DOImportCode;
                modelHeader.DeliveryDate = header.DeliveryDate;
                modelHeader.Status = DeliveryNoteHeader.status.New;
                modelHeader.IsDeleted = false;

                model.header = modelHeader;
                #endregion

                #region Details
                var listSplitNoteDetailCreated = _splitNoteDetailRepository.Get(a => a.DOCode == header.DOImportCode);
                var listPacking = _productPackingRepository.Get();
                var listProduct = _productRepository.Get();

                model.listDetail = new List<SplitNoteDetail>();

                var listMaterialCode = details.Select(a => new { a.ProductID, a.Delivery }).Distinct().ToList();
                foreach (var item in listMaterialCode)
                {
                    var listMaterialCodeCustomerCode = details.Where(a => a.ProductID == item.ProductID && a.Delivery == item.Delivery).Select(a => new { a.ProductID, a.Delivery, a.CompanyCode }).Distinct().ToList();
                    foreach (var itemByCustomer in listMaterialCodeCustomerCode)
                    {
                        var listMaterialCodeCustomerCodeBatchCode = details.Where(a => a.ProductID == item.ProductID && a.Delivery == item.Delivery && a.CompanyCode == itemByCustomer.CompanyCode)
                        .Select(a => new { a.ProductID, a.Delivery, a.CompanyCode, a.BatchCode }).Distinct().ToList();
                        foreach (var itemByBatchCode in listMaterialCodeCustomerCodeBatchCode)
                        {
                            SplitNoteDetail modelDetail = new SplitNoteDetail();
                            DeliveryOrderDetail orderDetail = details.FirstOrDefault(
                                                            a => a.ProductID == itemByBatchCode.ProductID
                                                            && a.BatchCode == itemByBatchCode.BatchCode
                                                            && a.CompanyCode == itemByBatchCode.CompanyCode
                                                            && a.Delivery == itemByBatchCode.Delivery);

                            //KEY
                            modelDetail.DOCode = header.DOImportCode;
                            modelDetail.SplitCode = modelHeader.SplitCode;
                            modelDetail.DeliveryCode = itemByBatchCode.Delivery;
                            modelDetail.ProductID = itemByBatchCode.ProductID;
                            modelDetail.BatchCode = itemByBatchCode.BatchCode;

                            //Property
                            modelDetail.Status = DeliveryNoteHeader.status.New;
                            modelDetail.ProductCode = details.FirstOrDefault(a => a.ProductID == itemByBatchCode.ProductID).ProductCode;
                            modelDetail.ProductDescription = details.FirstOrDefault(a => a.ProductID == itemByBatchCode.ProductID).ProductName;
                            modelDetail.CompanyCode = itemByBatchCode.CompanyCode;
                            modelDetail.CompanyName = orderDetail.CompanyName;
                            modelDetail.Province = orderDetail.ProvinceShipTo;

                            //Calculate Quantity to be Create
                            decimal? quantityOrder = details.Where(a => a.CompanyCode == itemByBatchCode.CompanyCode && a.ProductID == itemByBatchCode.ProductID && a.BatchCode == itemByBatchCode.BatchCode && a.Delivery == itemByBatchCode.Delivery && a.Quantity.HasValue).Sum(a => a.Quantity);
                            quantityOrder = quantityOrder ?? 0;
                            decimal? quantityCreated = listSplitNoteDetailCreated.Where(a =>
                                                            a.ProductID == itemByBatchCode.ProductID
                                                            && a.BatchCode == itemByBatchCode.BatchCode
                                                            && a.DeliveryCode == itemByBatchCode.Delivery
                                                            && a.CompanyCode == itemByBatchCode.CompanyCode
                                                        ).Select(a => a.Quantity ?? 0).Sum();
                            quantityCreated = quantityCreated ?? 0;
                            modelDetail.Quantity = quantityOrder - quantityCreated;

                            //ProductPacking
                            var productPacking = listPacking.Where(a => a.ProductID == itemByBatchCode.ProductID).FirstOrDefault();
                            var product = listProduct.Where(a => a.ProductID == itemByBatchCode.ProductID).FirstOrDefault();
                            if (productPacking != null && productPacking.Quantity.HasValue && productPacking.Quantity > 0)
                            {
                                modelDetail.PackQuantity = (int)(modelDetail.Quantity / productPacking.Quantity);
                                modelDetail.ProductQuantity = modelDetail.Quantity % productPacking.Quantity;
                                modelDetail.PackType = product.PackingType;
                            }
                            else
                            {
                                modelDetail.ProductQuantity = modelDetail.Quantity;
                            }

                            if (modelDetail.Quantity > 0)
                            {
                                model.listDetail.Add(modelDetail);
                            }
                        }
                    }
                }
                #endregion
            });

            return model;
        }

        public async Task CreatePrepareNote()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;
            bool isSuccess = false;
            string strUserCreate = string.Empty;
            var user = await _userRepository.GetSingleAsync(a => a.UserID == header.CreatedBy);
            if (user != null)
            {
                strUserCreate = user.LastName + " " + user.FirstName;
            }

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _productRepository = unitOfWork.Register(_productRepository.GetType()) as IProductRepository;
                    _productPackingRepository = unitOfWork.Register(_productPackingRepository.GetType()) as IProductPackingRepository;
                }

                //GROUP Header BY DeliveryDate, PartnerCode
                var listDeliveryDateCompanyCode = details.Select(a => new { a.DeliveryDate, a.CompanyCode, a.ProvinceSoldTo, a.CompanyName }).Distinct().ToList();
                var listProduct = await _productRepository.GetAsync();
                var listPacking = await _productPackingRepository.GetAsync();
                foreach (var d in listDeliveryDateCompanyCode)
                {
                    using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                    {
                        _prepareNoteHeaderRepository = unitOfWork.Register(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;
                        _prepareNoteDetailRepository = unitOfWork.Register(_prepareNoteDetailRepository.GetType()) as IPrepareNoteDetailRepository;

                        //Get Lastest DO Code
                        //Create Next DO Code
                        //Save DO Header
                        //Save DO Detail

                        string prepareCode = await _prepareNoteHeaderRepository.GetNextNBR();
                        PrepareNoteHeader modelHeader = new PrepareNoteHeader();
                        modelHeader.DOImportCode = header.DOImportCode;
                        modelHeader.PrepareCode = prepareCode;
                        modelHeader.PrepareDate = DateTime.Today;
                        modelHeader.DeliveryDate = d.DeliveryDate;
                        modelHeader.CompanyCode = d.CompanyCode;
                        modelHeader.Province = d.ProvinceSoldTo;
                        modelHeader.CompanyName = d.CompanyName;
                        modelHeader.Status = PrepareNoteHeader.status.New;

                        modelHeader.UserWareHouse = strUserCreate;
                        modelHeader.UserCreateFullName = LoginInfo.LastName + " " + LoginInfo.FirstName;


                        //GROUP BY MaterialCode, BatchCode
                        var listOrderDetail = details.Where(p => p.CompanyCode == d.CompanyCode).Select(a => new { a.ProductID, a.BatchCode, a.Delivery }).Distinct().ToList();
                        var listNoteDetailCreated = await _prepareNoteDetailRepository.GetByDOCodeAndPartnerCode(header.DOImportCode, d.CompanyCode);


                        List<PrepareNoteDetail> listDetail = new List<PrepareNoteDetail>();
                        foreach (var item in listOrderDetail)
                        {
                            PrepareNoteDetail modelDetail = new PrepareNoteDetail();
                            modelDetail.Status = DeliveryNoteHeader.status.New;

                            modelDetail.DOCode = header.DOImportCode;
                            modelDetail.PrepareCode = prepareCode;
                            modelDetail.DeliveryCode = item.Delivery;
                            modelDetail.ProductID = item.ProductID;
                            modelDetail.BatchCode = item.BatchCode;

                            decimal quantityOrder = details.Where(a => a.CompanyCode == d.CompanyCode && a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Delivery == item.Delivery).Select(a => a.Quantity ?? 0).Sum();
                            decimal quantityCreated = listNoteDetailCreated.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.DeliveryCode == item.Delivery).Select(a => a.Quantity ?? 0).Sum();

                            modelDetail.Quantity = quantityOrder - quantityCreated;

                            var productPacking = listPacking.Where(a => a.Type != "P" && a.ProductID == item.ProductID).FirstOrDefault();
                            var product = listProduct.Where(a => a.ProductID == item.ProductID).FirstOrDefault();
                            if (modelDetail.Quantity > 0 && productPacking != null && productPacking.Quantity.HasValue && productPacking.Quantity > 0)
                            {
                                modelDetail.PackQuantity = (int)(modelDetail.Quantity / productPacking.Quantity);
                                modelDetail.ProductQuantity = modelDetail.Quantity % productPacking.Quantity;
                                modelDetail.PackType = product.PackingType;

                                //AdditionInfomation
                                modelDetail.PackSize = productPacking.Quantity;
                            }
                            else
                            {
                                modelDetail.ProductQuantity = modelDetail.Quantity;
                            }

                            if (modelDetail.Quantity > 0)
                            {
                                listDetail.Add(modelDetail);
                            }

                            //AdditionInfomation
                            modelDetail.ProductCase = modelDetail.ProductQuantity > 0 ? 1 : 0;
                            modelDetail.ProductUnit = "G";
                        }

                        if (listDetail.Count > 0)
                        {
                            await _prepareNoteHeaderRepository.Insert(modelHeader);
                            foreach (var modelDetail in listDetail)
                            {
                                await _prepareNoteDetailRepository.InsertOrUpdate(modelDetail, new object[] { modelDetail.DeliveryCode, modelDetail.PrepareCode, modelDetail.DOCode, modelDetail.ProductID, modelDetail.BatchCode });
                            }
                            await unitOfWork.Commit();
                        }
                    }

                }

                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _prepareNoteHeaderRepository = unitOfWork.Register(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;

                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("Code", string.Empty);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("DOImportCode", header.DOImportCode);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("Type", "PrepareNote");
                    listParam.Add(param3);
                    SqlParameter param4 = new SqlParameter("Status", SplitNoteHeader.status.New);
                    listParam.Add(param4);

                    await _prepareNoteHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                    await unitOfWork.Commit();
                    isSuccess = true;
                }

                _mainPresenter.SetMessage(Messages.Success_CreatePrepareNotes, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                _mainPresenter.SetMessage(Messages.Error_ConcurrencyUpdate, Utility.MessageType.Error);
                await Refresh();
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                _mainPresenter.SetMessage(String.Join(" ", message), Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    _mainPresenter.SetMessage(message, Utility.MessageType.Error);
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                _prepareNoteHeaderRepository = _mainPresenter.Resolve(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;
                _prepareNoteDetailRepository = _mainPresenter.Resolve(_prepareNoteDetailRepository.GetType()) as IPrepareNoteDetailRepository;
                _productRepository = _mainPresenter.Resolve(_productRepository.GetType()) as IProductRepository;
                _productPackingRepository = _mainPresenter.Resolve(_productPackingRepository.GetType()) as IProductPackingRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {

                }
            }
        }
        #endregion

        #region OLD CODE-USE CAREFULLY
        public async Task<SplitNoteFull> CreateSplitNoteTemp(string PartnerCode)
        {
            SplitNoteFull model = new SplitNoteFull();
            using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                _splitNoteHeaderRepository = unitOfWork.Register(_splitNoteHeaderRepository.GetType()) as ISplitNoteHeaderRepository;
                _splitNoteDetailRepository = unitOfWork.Register(_splitNoteDetailRepository.GetType()) as ISplitNoteDetailRepository;

                var header = _mainView.DeliveryOrderHeader;
                var details = _mainView.DeliveryOrderDetails;
                DeliveryOrderDetail detailInfo;
                if (details.Count > 0)
                {
                    header.DeliveryDate = details[0].DeliveryDate;
                    if (string.IsNullOrEmpty(PartnerCode))
                    {
                        PartnerCode = details[0].CompanyCode;
                        detailInfo = details[0];
                    }
                    else
                    {
                        detailInfo = details.Where(a => a.CompanyCode == PartnerCode).FirstOrDefault();
                    }
                }
                else
                {
                    return null;
                }

                #region Header
                SplitNoteHeader _DNHeader = new SplitNoteHeader();
                _DNHeader.SplitCode = string.Empty;
                _DNHeader.DOImportCode = header.DOImportCode;
                _DNHeader.DeliveryDate = header.DeliveryDate;
                _DNHeader.Status = DeliveryNoteHeader.status.New;
                _DNHeader.IsDeleted = false;

                model.header = _DNHeader;
                #endregion

                #region Details
                var listMaterialCodeBatchCode = details.Where(a => a.CompanyCode == PartnerCode).Select(a => new { a.ProductID, a.BatchCode }).Distinct().ToList();
                var listSplitNoteDetailCreated = await _splitNoteDetailRepository.GetByDOCodeAndPartnerCode(header.DOImportCode, PartnerCode);

                model.listDetail = new List<SplitNoteDetail>();
                foreach (var item in listMaterialCodeBatchCode)
                {
                    SplitNoteDetail _DNDetail = new SplitNoteDetail();
                    _DNDetail.SplitCode = _DNHeader.SplitCode;
                    _DNDetail.Status = DeliveryNoteHeader.status.New;

                    _DNDetail.DOCode = header.DOImportCode;
                    _DNDetail.ProductID = item.ProductID;
                    _DNDetail.ProductCode = details.FirstOrDefault(a => a.ProductID == item.ProductID).ProductCode;
                    _DNDetail.ProductDescription = details.FirstOrDefault(a => a.ProductID == item.ProductID).ProductName;
                    _DNDetail.BatchCode = item.BatchCode;

                    decimal quantityOrder = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).Select(a => a.Quantity ?? 0).Sum();
                    decimal quantityCreated = listSplitNoteDetailCreated.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).Select(a => a.Quantity ?? 0).Sum();

                    _DNDetail.Quantity = quantityOrder - quantityCreated;
                    _DNDetail.PackQuantity = 0;// item.Quantity;
                    _DNDetail.ProductQuantity = 0;// item.ProductQuantity;

                    if (_DNDetail.Quantity > 0)
                    {
                        model.listDetail.Add(_DNDetail);
                    }
                }
                #endregion
            }

            return model;
        }

        public async Task<List<string>> CreateSplitNoteFull()
        {
            //For each delivery in OrderDetails
            //Check created SplitNote
            //If Not Yet Create Full
            //Create with remain quantity of product

            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;
            List<string> listSplitNoteCreated = new List<string>();

            List<string> listDO = details.Select(a => a.Delivery).Distinct().ToList();
            foreach (string DeliveryCode in listDO)
            {
                try
                {
                    using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                    {
                        _splitNoteHeaderRepository = unitOfWork.Register(_splitNoteHeaderRepository.GetType()) as ISplitNoteHeaderRepository;
                        _splitNoteDetailRepository = unitOfWork.Register(_splitNoteDetailRepository.GetType()) as ISplitNoteDetailRepository;

                        #region Check Remain SplitNote Not Yet Created
                        var createdSplitNoteDetails = _splitNoteDetailRepository.Get(a => a.DeliveryCode == DeliveryCode);
                        var listMaterialCodeBatchCode = details.Where(a => a.Delivery == DeliveryCode).Select(a => new { a.ProductID, a.BatchCode }).Distinct().ToList();

                        List<SplitNoteDetail> listSplitDetails = new List<SplitNoteDetail>();
                        foreach (var item in listMaterialCodeBatchCode)
                        {
                            decimal quantityOrder = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode && a.Delivery == DeliveryCode).Select(a => a.Quantity ?? 0).Sum();
                            decimal quantityCreated = 0;
                            if (createdSplitNoteDetails != null && createdSplitNoteDetails.Count > 0)
                            {
                                quantityCreated = createdSplitNoteDetails.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).Select(a => a.Quantity ?? 0).Sum();
                            }
                            decimal remainQuantity = quantityOrder - quantityCreated;
                            if (remainQuantity > 0)
                            {
                                SplitNoteDetail detail = new SplitNoteDetail()
                                {
                                    BatchCode = item.BatchCode,
                                    DeliveryCode = DeliveryCode,
                                    DOCode = header.DOImportCode,
                                    ProductCode = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).FirstOrDefault().ProductCode,
                                    ProductDescription = details.Where(a => a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).FirstOrDefault().ProductName,
                                    ProductID = item.ProductID,
                                    Quantity = remainQuantity,
                                    Status = SplitNoteHeader.status.New,
                                };
                                listSplitDetails.Add(detail);
                            }
                        }
                        #endregion


                        #region If Not Yet Created Full, Create
                        //If have remain product not yet create SplitDetail
                        //Create SplitHeader and Detail of it
                        if (listSplitDetails.Count > 0)
                        {
                            string splitCode = await _splitNoteHeaderRepository.GetNextNBR();
                            SplitNoteHeader _DNHeader = new SplitNoteHeader();
                            var detailInfo = details.Where(a => a.Delivery == DeliveryCode).FirstOrDefault();

                            _DNHeader.SplitCode = splitCode;
                            _DNHeader.DOImportCode = header.DOImportCode;
                            _DNHeader.DeliveryDate = detailInfo.DeliveryDate;
                            _DNHeader.Status = DeliveryNoteHeader.status.New;
                            _DNHeader.IsDeleted = false;

                            await _splitNoteHeaderRepository.Insert(_DNHeader);

                            foreach (var item in listSplitDetails)
                            {
                                item.SplitCode = _DNHeader.SplitCode;
                                await _splitNoteDetailRepository.InsertOrUpdate(item, new object[] { item.DOCode, item.SplitCode, item.DeliveryCode, item.ProductID, item.BatchCode });
                            }

                            await unitOfWork.Commit();
                            listSplitNoteCreated.Add(_DNHeader.SplitCode);
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(Messages.Error_Common);
                }
            }
            //_mainPresenter.SetMessage(Messages.Success_CreateSplitNotes, Utility.MessageType.Information);
            _mainPresenter.SetMessage(String.Format(Messages.Success_CreateSplitNotesFull, String.Join(", ", listSplitNoteCreated)), Utility.MessageType.Information);
            return listSplitNoteCreated;
        }

        public async Task CreateDeliveryNote()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;
            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _deliveryNoteHeaderRepository = unitOfWork.Register(_deliveryNoteHeaderRepository.GetType()) as IDeliveryNoteHeaderRepository;
                    _deliveryNoteDetailRepository = unitOfWork.Register(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;

                    //GROUP Header BY DeliveryDate
                    var listDeliveryDates = details.Select(a => a.DeliveryDate).Distinct().ToList();

                    foreach (DateTime d in listDeliveryDates)
                    {
                        //Get Lastest DO Code
                        //Create Next DO Code
                        //Save DO Header
                        //Save DO Detail

                        string DOImportCode = await _deliveryNoteHeaderRepository.GetNextNBR();
                        DeliveryNoteHeader _DNHeader = new DeliveryNoteHeader();
                        _DNHeader.ExportCode = DOImportCode;
                        _DNHeader.DeliveryDate = d;
                        _DNHeader.Status = DeliveryNoteHeader.status.New;
                        _DNHeader.IsDeleted = false;
                        await _deliveryNoteHeaderRepository.Insert(_DNHeader);

                        //GROUP BY MaterialCode, BatchCode
                        var listMaterialCodeBatchCode = details.Where(a => a.DeliveryDate == d).Select(a => new { a.ProductID, a.BatchCode }).Distinct().ToList();
                        foreach (var item in listMaterialCodeBatchCode)
                        {
                            DeliveryNoteDetail _DNDetail = new DeliveryNoteDetail();
                            _DNDetail.ExportCode = DOImportCode;
                            _DNDetail.Status = DeliveryNoteHeader.status.New;

                            _DNDetail.DOCode = string.Empty; //item.DOCode;
                            _DNDetail.ProductID = item.ProductID;
                            _DNDetail.BatchCode = item.BatchCode;
                            _DNDetail.Quantity = details.Where(a => a.DeliveryDate == d && a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).Select(a => a.Quantity ?? 0).Sum();//item.Quantity;
                            _DNDetail.PackQuantity = 0;// item.Quantity;
                            _DNDetail.ProductQuantity = 0;// item.ProductQuantity;

                            await _deliveryNoteDetailRepository.InsertOrUpdate(_DNDetail, new object[] { _DNDetail.ExportCode, _DNDetail.DOCode, _DNDetail.ProductID, _DNDetail.BatchCode, _DNDetail.Quantity });
                            await _deliveryNoteDetailRepository.Commit();
                        }

                        await unitOfWork.Commit();
                    }
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                _mainPresenter.SetMessage(Messages.Error_ConcurrencyUpdate, Utility.MessageType.Error);
                await Refresh();
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                _mainPresenter.SetMessage(String.Join(" ", message), Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    _mainPresenter.SetMessage(message, Utility.MessageType.Error);
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {

                }
            }
        }

        public async Task CreateSplitNote()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;
            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _splitNoteHeaderRepository = unitOfWork.Register(_splitNoteHeaderRepository.GetType()) as ISplitNoteHeaderRepository;
                    _splitNoteDetailRepository = unitOfWork.Register(_splitNoteDetailRepository.GetType()) as ISplitNoteDetailRepository;
                    _productRepository = unitOfWork.Register(_productRepository.GetType()) as IProductRepository;

                    //GROUP Header BY DeliveryDate, PartnerCode
                    var listDeliveryDateCompanyCode = details.Select(a => new { a.DeliveryDate, a.CompanyCode, a.ProvinceSoldTo, a.CompanyName }).Distinct().ToList();
                    var listProduct = await _productRepository.GetAsync();
                    foreach (var d in listDeliveryDateCompanyCode)
                    {
                        //Get Lastest DO Code
                        //Create Next DO Code
                        //Save DO Header
                        //Save DO Detail

                        string DOImportCode = await _splitNoteHeaderRepository.GetNextNBR();
                        SplitNoteHeader _DNHeader = new SplitNoteHeader();
                        _DNHeader.SplitCode = DOImportCode;
                        _DNHeader.DeliveryDate = d.DeliveryDate;
                        _DNHeader.Status = DeliveryNoteHeader.status.New;

                        await _splitNoteHeaderRepository.Insert(_DNHeader);

                        //GROUP BY MaterialCode, BatchCode
                        var listMaterialCodeBatchCode = details.Where(p => p.CompanyCode == d.CompanyCode).Select(p => new { p.ProductID, p.BatchCode }).Distinct().ToList();
                        foreach (var item in listMaterialCodeBatchCode)
                        {
                            SplitNoteDetail _DNDetail = new SplitNoteDetail();
                            _DNDetail.SplitCode = DOImportCode;
                            _DNDetail.Status = DeliveryNoteHeader.status.New;

                            _DNDetail.DOCode = String.Empty; //item.DOCode;
                            _DNDetail.ProductID = item.ProductID;
                            _DNDetail.BatchCode = item.BatchCode;
                            _DNDetail.Quantity = details.Where(a => a.CompanyCode == d.CompanyCode && a.ProductID == item.ProductID && a.BatchCode == item.BatchCode).Select(a => a.Quantity).Sum();
                            _DNDetail.PackQuantity = 0;// item.Quantity;
                            _DNDetail.ProductQuantity = 0;// item.ProductQuantity;

                            await _splitNoteDetailRepository.InsertOrUpdate(_DNDetail, new object[] { _DNDetail.SplitCode, _DNDetail.DOCode, _DNDetail.ProductID, _DNDetail.BatchCode, _DNDetail.Quantity });
                            await _splitNoteDetailRepository.Commit();
                        }

                        await unitOfWork.Commit();
                    }
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                _mainPresenter.SetMessage(Messages.Error_ConcurrencyUpdate, Utility.MessageType.Error);
                await Refresh();
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                _mainPresenter.SetMessage(String.Join(" ", message), Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    _mainPresenter.SetMessage(message, Utility.MessageType.Error);
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {

                }
            }
        }

        public async Task<bool> CanCreateDeliveryNote()
        {
            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;
            bool canCreate = false;

            using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                _deliveryNoteHeaderRepository = unitOfWork.Register(_deliveryNoteHeaderRepository.GetType()) as IDeliveryNoteHeaderRepository;
                _deliveryNoteDetailRepository = unitOfWork.Register(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;
                _deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;

                var listNote = await _deliveryNoteDetailRepository.GetAsync(a => a.DOCode == header.DOImportCode);
                if (listNote != null && listNote.Count > 0)
                {
                    List<string> listProduct = listNote.Select(a => a.ProductCode).Distinct().ToList();

                    foreach (string productCode in listProduct)
                    {
                        decimal? totalNoteQuantity = listNote.Where(a => a.ProductCode == productCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                        totalNoteQuantity = totalNoteQuantity ?? totalNoteQuantity;

                        decimal? totalOrderQuantity = details.Where(a => a.ProductCode == productCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                        totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                        //if not yet export full of one product, can create
                        if (totalNoteQuantity < totalOrderQuantity)
                        {
                            canCreate = true;
                            break;
                        }
                    }
                }
                else
                {
                    canCreate = true;
                }


            }
            return canCreate;
        }

        public async Task<int> UpdateStatusDeliveryOrderByDeliveryNote()
        {
            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;
            int isExportFull = 0;

            using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                _deliveryNoteHeaderRepository = unitOfWork.Register(_deliveryNoteHeaderRepository.GetType()) as IDeliveryNoteHeaderRepository;
                _deliveryNoteDetailRepository = unitOfWork.Register(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;
                _deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;

                var listNote = await _deliveryNoteDetailRepository.GetAsync(a => a.DOCode == header.DOImportCode);
                if (listNote != null && listNote.Count > 0)
                {
                    List<string> listProduct = details.Select(a => a.ProductCode).Distinct().ToList();

                    foreach (string productCode in listProduct)
                    {
                        decimal? totalNoteQuantity = listNote.Where(a => a.ProductCode == productCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                        totalNoteQuantity = totalNoteQuantity ?? totalNoteQuantity;

                        decimal? totalOrderQuantity = details.Where(a => a.ProductCode == productCode && a.Quantity.HasValue).Sum(a => a.Quantity);
                        totalOrderQuantity = totalOrderQuantity ?? totalOrderQuantity;

                        //if not yet export full of one product, can create
                        if (totalNoteQuantity < totalOrderQuantity)
                        {
                            isExportFull = 1;
                            break;
                        }
                    }
                }
                else
                {
                    isExportFull = 1;
                }

                header.IsExportFull = isExportFull;
                await _deliveryOrderHeaderRepository.Update(header, new object[] { header.DOImportCode });
            }

            return isExportFull;
        }
        #endregion

        #region Button
        public override void Insert()
        {
            _mainView.DeliveryOrderHeader = new DeliveryOrderHeader();
            _mainView.DeliveryOrderDetails = new List<DeliveryOrderDetail>();
            _mainView.DeletedDeliveryOrderDetails = new List<DeliveryOrderDetail>();
            _mainView.ReportDODetails = new System.Data.DataTable();
            _mainView.ReportLocationSuggest = new System.Data.DataTable();
            _mainView.ReportDOProcess = new System.Data.DataTable();
            _mainView.ReportExportProcess = new System.Data.DataTable();
        }
        public class CompanyDoCode
        {
            public string doCode { get; set; }
            public string companyCode { get; set; }
        }
        public async Task ImportExcel(string fileName, string updateNote)
        {
            #region Param prepare
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            string start = "A1";
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            //_Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            bool isSuccess = false;

            List<DeliveryOrderDetail> listModel = new List<DeliveryOrderDetail>();
            //Get list ProductToValidate
            var products = await _productRepository.GetAsync();
            var companies = await _companyRepository.GetAsync();
            int lineNBR = 0;
            DateTime defaultDeliveryDate = DateTime.Today;
            bool hasDefaultDate = false;
            int totalErrorCount = 0;
            int errorCount = 0;
            string errorMessage = string.Empty;
            #endregion

            try
            {
                workbook = excelApp.Workbooks.Open(fileName);
                //sheetDetail = (Worksheet)workbook.Worksheets[1];
                foreach (Worksheet sheetDetail in workbook.Worksheets)
                {
                    #region Prepare for import sheet
                    if (sheetDetail.AutoFilter != null)
                    {
                        sheetDetail.AutoFilterMode = false;
                    }

                    //// get a range to work with
                    range = sheetDetail.get_Range(start, Missing.Value);

                    //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                    range = range.get_End(XlDirection.xlDown);

                    //// get the address of the bottom cell
                    string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                    //// specific end column
                    end = downAddress.Remove(0, 1);

                    int countRows = sheetDetail.UsedRange.Rows.Count;

                    //// throw exception to prevent loop through large data
                    if (countRows > 1000000)
                        throw new WrappedException(Messages.Validate_ExcelFile_Large);

                    end = "Z" + countRows;

                    //// Get the range, then values from start to end
                    range = sheetDetail.get_Range(start, end);
                    var values = (object[,])range.Value2;
                    int count = values.GetLength(0);
                    #endregion

                    #region If Valid to import
                    //Kiểm tra xem sheet có hợp lệ để import hay ko, bắt đầu = Delivery
                    if ($"{values[1, 1]}".Trim() == "Delivery")
                    {
                        //Chỉ lấy những dòng mới trong file Excel import
                        //Ví dụ DO đang có 5 line, khi import excel, chỉ lấy data từ line thứ 6
                        //int maxLineNbr = _mainView.DeliveryOrderDetails.Count() > 0 ? _mainView.DeliveryOrderDetails.Max(p => p.LineNbr) : 0;
                        int maxLineNbr = 1;
                        int beginImport = maxLineNbr + 1;
                        List<CompanyDoCode> listdocombanyCode = new List<CompanyDoCode>();

                        #region Import
                        for (int i = beginImport; i <= count; i++)
                        {
                            errorCount = 0;
                            errorMessage = string.Empty;

                            try
                            {
                                string excelLine = (i).ToString();
                                string batchCode = $"{values[i, 8]}".Trim();
                                string productCode = $"{values[i, 6]}".Trim();
                                if (!String.IsNullOrWhiteSpace(productCode))
                                {
                                    //// Validate product code                            
                                    if (String.IsNullOrWhiteSpace(productCode))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_Product, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_Product + " " + i.ToString());
                                    }

                                    //// Validate product
                                    string productName = $"{values[i, 7]}".Trim();
                                    int productID = 0;
                                    var product = products.Where(p => p.ProductCode == productCode).FirstOrDefault();
                                    if (product == null || product.ProductID == 0)
                                    {
                                        //SaveAnyway. don't throw exception
                                        ExcelCollectError(ref errorCount, ref errorMessage, string.Format(Messages.Validate_Required_ProductNotInserted, productCode), i, sheetDetail);
                                    }
                                    else
                                    {
                                        productName = product.Description;
                                        productID = product.ProductID;
                                    }

                                    //if (item.ProductCode.StartsWith("A"))
                                    //{
                                    //    //Set default ProductLot for Promotion Product
                                    //    //A0060703 -> 0060703A00
                                    //    string temp = item.ProductCode.Replace("A", "");
                                    //    temp = "0000" + temp;
                                    //    temp = temp.Substring(temp.Length - 6, 6) + "Y" + "00";
                                    //    itemSum.BatchCode = temp;
                                    //}
                                    //else if (item.ProductDescription.StartsWith("AQ"))
                                    //{
                                    //    //Set default ProductLot for Promotion Product
                                    //    //A0060703 -> 0060703A00
                                    //    string temp = item.ProductCode.Substring(0, 6);
                                    //    temp = temp + "Y" + item.ProductCode.Substring(item.ProductCode.Length - 2, 2);
                                    //    itemSum.BatchCode = temp;
                                    //}

                                    if (productCode.StartsWith("A"))
                                    {
                                        //Set default ProductLot for Promotion Product
                                        //if (String.IsNullOrWhiteSpace(batchCode))
                                        {
                                            //A0060703 -> 0060703A00
                                            string temp = productCode.Replace("A", "");
                                            temp = "0000" + temp;
                                            temp = temp.Substring(temp.Length - 6, 6) + "Y" + "00";
                                            batchCode = temp;
                                        }
                                    }
                                    else if (productName.StartsWith("AQ"))
                                    {
                                        //Set default ProductLot for Promotion Product
                                        //if (String.IsNullOrWhiteSpace(batchCode))
                                        {
                                            //A0060703 -> 0060703A00
                                            string temp = productCode.Substring(0, 6);
                                            temp = temp + "Y" + productCode.Substring(productCode.Length - 2, 2);
                                            batchCode = temp;
                                        }
                                    }

                                    if (String.IsNullOrWhiteSpace(batchCode))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_ProductionLot, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_CustomerCode);
                                    }

                                    //// Validate Product Lot Distributor
                                    string batchCodeDistributor = $"{values[i, 12]}".Trim();

                                    //// Validate quantity
                                    decimal quantity;
                                    if (!decimal.TryParse($"{values[i, 10]}", out quantity))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_Quantity, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_Quantity);
                                    }
                                    if (quantity < 0)
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Range_Quantity, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Range_Quantity);
                                    }

                                    //// Validate customer code
                                    string companyCode = $"{values[i, 5]}".Trim();
                                    string provinceSoldTo = $"{values[i, 3]}".Trim();
                                    string provinceShipTo = $"{values[i, 4]}".Trim();
                                    if (String.IsNullOrWhiteSpace(companyCode))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_CustomerCode, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_CustomerCode);
                                    }
                                    else
                                    {
                                        var company = companies.Where(p => p.CompanyCode == companyCode).FirstOrDefault();
                                        if (company == null)
                                        {
                                            ExcelCollectError(ref errorCount, ref errorMessage, string.Format("Công ty {0} chưa có trong hệ thống.", companyCode, excelLine.ToString()), i, sheetDetail);
                                            //throw new WrappedException(string.Format("Dòng {1}: Công ty {0} chưa có trong hệ thống.", companyCode, excelLine.ToString()));
                                        }
                                        else
                                        {
                                            provinceSoldTo = company.ProvinceSoldTo;
                                            provinceShipTo = company.ProvinceShipTo;
                                        }
                                    }

                                    //// Validate customer name
                                    string companyName = $"{values[i, 2]}".Trim();
                                    if (String.IsNullOrWhiteSpace(companyName))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_CustomerName, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_CustomerName);
                                    }

                                    //ValidateDatime
                                    if (!hasDefaultDate)
                                    {
                                        try
                                        {
                                            double d_deliveryDate;
                                            double.TryParse($"{values[i, 11]}", out d_deliveryDate);
                                            string str_deliveryDate = $"{values[i, 10]}";
                                            DateTime deliveryDate = d_deliveryDate > 0 ? DateTime.FromOADate(d_deliveryDate) : DateTime.Parse(str_deliveryDate);
                                            defaultDeliveryDate = deliveryDate;
                                            hasDefaultDate = true;
                                        }
                                        catch
                                        {
                                            // Do nothing here
                                        }
                                    }

                                    if (defaultDeliveryDate < DateTime.Today)
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Default_DeliveryDate, i, sheetDetail);
                                    }

                                    lineNBR++;
                                    //// Validate device
                                    string doCode = $"{values[i, 1]}".Trim();
                                    if (String.IsNullOrWhiteSpace(doCode))
                                    {
                                        doCode = "DO_BoSung" + "_" + lineNBR.ToString();
                                        //throw new WrappedException(Messages.Validate_Required_Device);
                                    }
                                    else
                                    {
                                        //validate 1 docode inhere 1 company
                                        var isdoCode = listdocombanyCode.Where(w => w.doCode.Equals(doCode)).ToList();
                                        if (isdoCode.Count() > 0)
                                            if (isdoCode.Where(w => w.companyCode.Equals(companyCode)).Count() == 0)
                                            {
                                                ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_CompanyDoCode, i, sheetDetail);
                                                //throw new WrappedException(string.Format(Messages.Validate_Required_CompanyDoCode, i + 1));
                                            }

                                        CompanyDoCode Code = new CompanyDoCode
                                        {
                                            doCode = doCode,
                                            companyCode = companyCode
                                        };
                                        listdocombanyCode.Add(Code);
                                    }


                                    
                                    DeliveryOrderDetail modelToAdd = new DeliveryOrderDetail(lineNBR, doCode, productID, productCode, productName, batchCode, quantity, companyCode, companyName, provinceSoldTo, provinceShipTo, defaultDeliveryDate, "I", batchCodeDistributor);

                                    modelToAdd.NetWeight = Utility.DecimalParse($"{values[i, 13]}".Trim());
                                    modelToAdd.TotalWeight = Utility.DecimalParse($"{values[i, 14]}".Trim());
                                    modelToAdd.Volume = Utility.DecimalParse($"{values[i, 15]}".Trim());
                                    modelToAdd.VolumeUnit = Utility.StringParse($"{values[i, 16]}".Trim());
                                    modelToAdd.UOM = Utility.StringParse($"{values[i, 9]}".Trim());

                                    listModel.Add(modelToAdd);

                                    isSuccess = true;
                                }
                            }
                            catch (WrappedException ex)
                            {
                                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Information);
                                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                            }
                            catch (Exception ex)
                            {
                                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Error);
                            }
                            finally
                            {
                                _mainPresenter.SetMessage("Đang xử lý: " + sheetDetail.Name + ", Dòng: " + i.ToString(), Utility.MessageType.Information);

                                if (errorCount > 0)
                                {
                                    totalErrorCount += errorCount;
                                    //Tô màu cả dòng
                                    Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A" + i.ToString(), "L" + i.ToString());
                                    foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                                    {
                                        //cell.Borders.Color = Color.Red;
                                        //cell.Borders.Weight = 2;
                                        cell.Font.Color = Color.Red;
                                    }
                                    //Ghi ra dòng lỗi
                                    sheetDetail.Cells[i, 12] = errorMessage;
                                }
                                else
                                {
                                    //Nếu không có lỗi gì, reset lại dòng lỗi và tô màu trước đó, nếu có
                                    //Tô màu cả dòng
                                    Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A" + i.ToString(), "L" + i.ToString());
                                    foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                                    {
                                        //cell.Borders.Color = null;
                                        //cell.Borders.Weight = 0;
                                        cell.Font.Color = Color.Black;
                                    }
                                    //Ghi ra dòng lỗi
                                    sheetDetail.Cells[i, 12] = "";
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (WrappedException ex)
            {
                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Information);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", Messages.Error_Common));
            }
            finally
            {
                #region Save Excel If Error
                Utility.CreateDirectory(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\ExcelDOImportError\\");
                string savePath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\ExcelDOImportError\\DOImport_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                workbook.SaveAs(savePath);

                //Nếu có lỗi khi import, hiển thị thông báo và mở file Excel lỗi lên
                if (totalErrorCount > 0)
                {
                    _mainPresenter.SetMessage("Có " + totalErrorCount.ToString() + " lỗi dữ liệu. Vui lòng kiểm tra file Excel.", Utility.MessageType.Error);
                    FileInfo fi = new FileInfo(savePath);
                    if (fi.Exists)
                    {
                        System.Diagnostics.Process.Start(savePath);
                    }
                }
                #endregion

                #region Garbage Collection
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                #endregion

                #region Message and Load Data
                if (isSuccess)
                {
                    if (totalErrorCount == 0)
                    {
                        _mainView.DeliveryOrderDetails = listModel;
                        await LoadMainData();
                        _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
                    }
                }
                #endregion

                dataChanged = true;
            }
        }

        public void ExcelCollectError(ref int errorCount, ref string errorMessageOriginal, string errorMessage, int rowNumber, Worksheet sheetDetail)
        {
            errorCount++;
            errorMessageOriginal += errorMessage + " ;";
        }

        public void ExcelCollectReset(int rowNumber, Worksheet sheetDetail)
        {
            //Tô màu cả dòng
            Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A" + rowNumber.ToString(), "L" + rowNumber.ToString());
            foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
            {
                cell.Borders.Color = null;
            }
            //Ghi ra dòng lỗi
            sheetDetail.Cells[rowNumber, 12] = "";
        }


        public async Task CorrectAdditionalDO(string fileName, string updateNote)
        {
            #region Param prepare
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            string start = "A1";
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            //_Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            bool isSuccess = false;

            List<DeliveryOrderDetail> listModel = new List<DeliveryOrderDetail>();
            //Get list ProductToValidate
            var products = await _productRepository.GetAsync();
            var companies = await _companyRepository.GetAsync();
            var listCurrentDetail = _mainView.DeliveryOrderDetails;

            int lineNBR = 0;
            DateTime defaultDeliveryDate = DateTime.Today;
            bool hasDefaultDate = false;
            int totalErrorCount = 0;
            int errorCount = 0;
            string errorMessage = string.Empty;
            #endregion

            try
            {
                workbook = excelApp.Workbooks.Open(fileName);
                //sheetDetail = (Worksheet)workbook.Worksheets[1];
                foreach (Worksheet sheetDetail in workbook.Worksheets)
                {
                    #region Prepare for import sheet
                    if (sheetDetail.AutoFilter != null)
                    {
                        sheetDetail.AutoFilterMode = false;
                    }

                    //// get a range to work with
                    range = sheetDetail.get_Range(start, Missing.Value);

                    //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                    range = range.get_End(XlDirection.xlDown);

                    //// get the address of the bottom cell
                    string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                    //// specific end column
                    end = downAddress.Remove(0, 1);

                    int countRows = sheetDetail.UsedRange.Rows.Count;

                    //// throw exception to prevent loop through large data
                    if (countRows > 1000000)
                        throw new WrappedException(Messages.Validate_ExcelFile_Large);

                    end = "L" + countRows;

                    //// Get the range, then values from start to end
                    range = sheetDetail.get_Range(start, end);
                    var values = (object[,])range.Value2;
                    int count = values.GetLength(0);
                    #endregion

                    #region If Valid to import
                    //Kiểm tra xem sheet có hợp lệ để import hay ko, bắt đầu = Delivery
                    if ($"{values[1, 1]}".Trim() == "Delivery")
                    {
                        //Chỉ lấy những dòng mới trong file Excel import
                        //Ví dụ DO đang có 5 line, khi import excel, chỉ lấy data từ line thứ 6
                        //int maxLineNbr = _mainView.DeliveryOrderDetails.Count() > 0 ? _mainView.DeliveryOrderDetails.Max(p => p.LineNbr) : 0;
                        int maxLineNbr = 1;
                        int beginImport = maxLineNbr + 1;
                        List<CompanyDoCode> listdocombanyCode = new List<CompanyDoCode>();

                        #region Import
                        for (int i = beginImport; i <= count; i++)
                        {
                            errorCount = 0;
                            errorMessage = string.Empty;

                            try
                            {
                                string excelLine = (i).ToString();

                                string batchCode = $"{values[i, 8]}".Trim();
                                string productCode = $"{values[i, 6]}".Trim();
                                if (!String.IsNullOrWhiteSpace(productCode))
                                {
                                    //// Validate product code                            
                                    if (String.IsNullOrWhiteSpace(productCode))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_Product, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_Product + " " + i.ToString());
                                    }

                                    //// Validate product
                                    string productName = $"{values[i, 7]}".Trim();
                                    int productID = 0;
                                    if (!productCode.Contains("A"))
                                    {
                                        var product = products.Where(p => p.ProductCode == productCode).FirstOrDefault();
                                        if (product == null || product.ProductID == 0)
                                        {
                                            ExcelCollectError(ref errorCount, ref errorMessage, string.Format(Messages.Validate_Required_ProductNotInserted, productCode), i, sheetDetail);
                                            //throw new WrappedException(string.Format(Messages.Validate_Required_ProductNotInserted, productCode, excelLine.ToString()));
                                        }
                                        else
                                        {
                                            productName = product.Description;
                                            productID = product.ProductID;
                                        }

                                        //// Validate Product Lot
                                        if (String.IsNullOrWhiteSpace(batchCode))
                                        {
                                            ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_ProductionLot, i, sheetDetail);
                                            //throw new WrappedException(Messages.Validate_Required_ProductionLot);
                                        }
                                    }
                                    //Process for Promotion Product
                                    else
                                    {
                                        //Set default ProductLot for Promotion Product
                                        if (String.IsNullOrWhiteSpace(batchCode))
                                        {
                                            batchCode = "170000F00";
                                        }
                                    }


                                    //// Validate Product Lot Distributor
                                    string batchCodeDistributor = $"{values[i, 11]}".Trim();

                                    //// Validate quantity
                                    decimal quantity;
                                    if (!decimal.TryParse($"{values[i, 9]}", out quantity))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_Quantity, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_Quantity);
                                    }
                                    if (quantity < 0)
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Range_Quantity, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Range_Quantity);
                                    }

                                    //// Validate customer code
                                    string companyCode = $"{values[i, 5]}".Trim();
                                    string provinceSoldTo = $"{values[i, 3]}".Trim();
                                    string provinceShipTo = $"{values[i, 4]}".Trim();
                                    if (String.IsNullOrWhiteSpace(companyCode))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_CustomerCode, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_CustomerCode);
                                    }
                                    else
                                    {
                                        var company = companies.Where(p => p.CompanyCode == companyCode).FirstOrDefault();
                                        if (company == null)
                                        {
                                            ExcelCollectError(ref errorCount, ref errorMessage, string.Format("Công ty {0} chưa có trong hệ thống.", companyCode, excelLine.ToString()), i, sheetDetail);
                                            //throw new WrappedException(string.Format("Dòng {1}: Công ty {0} chưa có trong hệ thống.", companyCode, excelLine.ToString()));
                                        }
                                        else
                                        {
                                            provinceSoldTo = company.ProvinceSoldTo;
                                            provinceShipTo = company.ProvinceShipTo;
                                        }
                                    }

                                    //// Validate customer name
                                    string companyName = $"{values[i, 2]}".Trim();
                                    if (String.IsNullOrWhiteSpace(companyName))
                                    {
                                        ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_CustomerName, i, sheetDetail);
                                        //throw new WrappedException(Messages.Validate_Required_CustomerName);
                                    }

                                    //ValidateDatime
                                    if (!hasDefaultDate)
                                    {
                                        try
                                        {
                                            double d_deliveryDate;
                                            double.TryParse($"{values[i, 10]}", out d_deliveryDate);
                                            string str_deliveryDate = $"{values[i, 10]}";
                                            DateTime deliveryDate = d_deliveryDate > 0 ? DateTime.FromOADate(d_deliveryDate) : DateTime.Parse(str_deliveryDate);
                                            defaultDeliveryDate = deliveryDate;
                                            hasDefaultDate = true;
                                        }
                                        catch
                                        {
                                            // Do nothing here
                                        }
                                    }

                                    lineNBR++;
                                    //// Validate device
                                    string doCode = $"{values[i, 1]}".Trim();
                                    if (String.IsNullOrWhiteSpace(doCode))
                                    {
                                        doCode = "DO_BoSung" + "_" + lineNBR.ToString();
                                        //throw new WrappedException(Messages.Validate_Required_Device);
                                    }
                                    else
                                    {
                                        //validate 1 docode inhere 1 company
                                        var isdoCode = listdocombanyCode.Where(w => w.doCode.Equals(doCode)).ToList();
                                        if (isdoCode.Count() > 0)
                                            if (isdoCode.Where(w => w.companyCode.Equals(companyCode)).Count() == 0)
                                            {
                                                ExcelCollectError(ref errorCount, ref errorMessage, Messages.Validate_Required_CompanyDoCode, i, sheetDetail);
                                                //throw new WrappedException(string.Format(Messages.Validate_Required_CompanyDoCode, i + 1));
                                            }

                                        CompanyDoCode Code = new CompanyDoCode
                                        {
                                            doCode = doCode,
                                            companyCode = companyCode
                                        };
                                        listdocombanyCode.Add(Code);
                                    }

                                    //Sau khi có được model
                                    //1.Nếu có DO
                                    //2.Kiểm tra xem model có tồn tại trong current hay không, nếu không, ghi nhận lỗi
                                    //3.Nếu có và là update DO, cho phép Update
                                    //-----------------------------
                                    //1.Nếu có DO
                                    //if (!doCode.Contains("BoSung"))
                                    {
                                        //2.Kiểm tra xem model có tồn tại trong current hay không, nếu không, ghi nhận lỗi
                                        var modelExist = listCurrentDetail.Where(a =>
                                                            a.CompanyCode == companyCode
                                                            && a.ProductCode == productCode
                                                            && a.BatchCode == batchCode
                                                            && a.Quantity == quantity
                                                        ).ToList();
                                        if (modelExist != null && modelExist.Count > 0)
                                        {
                                            var modelDOBonusExist = listCurrentDetail.FirstOrDefault(a =>
                                                            a.CompanyCode == companyCode
                                                            && a.ProductCode == productCode
                                                            && a.BatchCode == batchCode
                                                            && a.Quantity == quantity
                                                            && a.Delivery.Contains("BoSung")
                                                        );
                                            //3.Nếu có và là update DO, cho phép Update
                                            if (modelDOBonusExist != null)
                                            {
                                                modelDOBonusExist.Delivery = doCode;
                                            }

                                        }
                                        else
                                        {
                                            ExcelCollectError(ref errorCount, ref errorMessage, "DO này không tồn tại trước đây. Cập nhật lại dữ liệu DO trước.", i, sheetDetail);
                                        }
                                    }


                                    //listModel.Add(new DeliveryOrderDetail(lineNBR, doCode, productID, productCode, productName, batchCode, quantity, companyCode, companyName, provinceSoldTo, provinceShipTo, defaultDeliveryDate, "I", batchCodeDistributor));

                                    isSuccess = true;
                                }
                            }
                            catch (WrappedException ex)
                            {
                                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Information);
                                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                            }
                            catch (Exception ex)
                            {
                                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Error);
                            }
                            finally
                            {
                                _mainPresenter.SetMessage("Đang xử lý: " + sheetDetail.Name + ", Dòng: " + i.ToString(), Utility.MessageType.Information);

                                if (errorCount > 0)
                                {
                                    totalErrorCount += errorCount;
                                    //Tô màu cả dòng
                                    Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A" + i.ToString(), "L" + i.ToString());
                                    foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                                    {
                                        //cell.Borders.Color = Color.Red;
                                        //cell.Borders.Weight = 2;
                                        cell.Font.Color = Color.Red;
                                    }
                                    //Ghi ra dòng lỗi
                                    sheetDetail.Cells[i, 12] = errorMessage;
                                }
                                else
                                {
                                    //Nếu không có lỗi gì, reset lại dòng lỗi và tô màu trước đó, nếu có
                                    //Tô màu cả dòng
                                    Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A" + i.ToString(), "L" + i.ToString());
                                    foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                                    {
                                        //cell.Borders.Color = null;
                                        //cell.Borders.Weight = 0;
                                        cell.Font.Color = Color.Black;
                                    }
                                    //Ghi ra dòng lỗi
                                    sheetDetail.Cells[i, 12] = "";
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (WrappedException ex)
            {
                _mainPresenter.SetMessage(ex.Message, Utility.MessageType.Information);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", Messages.Error_Common));
            }
            finally
            {
                #region Save Excel If Error
                Utility.CreateDirectory(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\ExcelDOImportError\\");
                string savePath = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\ExcelDOImportError\\DOImport_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                workbook.SaveAs(savePath);

                //Nếu có lỗi khi import, hiển thị thông báo và mở file Excel lỗi lên
                if (totalErrorCount > 0)
                {
                    _mainPresenter.SetMessage("Có " + totalErrorCount.ToString() + " lỗi dữ liệu. Vui lòng kiểm tra file Excel.", Utility.MessageType.Error);
                    FileInfo fi = new FileInfo(savePath);
                    if (fi.Exists)
                    {
                        System.Diagnostics.Process.Start(savePath);
                    }
                }
                #endregion

                #region Garbage Collection
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                #endregion

                #region Message and Load Data
                if (isSuccess)
                {
                    if (totalErrorCount == 0)
                    {
                        _mainView.DeliveryOrderDetails = listCurrentDetail;
                        await LoadMainData();
                        _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
                        _mainPresenter.SetMessage(Messages.Information_CorrectAdditionalDOSucessfully, Utility.MessageType.Information);
                    }
                }
                #endregion
            }
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\DOImport.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 7;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                int i = 0;
                foreach (var item in details)
                {
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = i.ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Delivery;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.CompanyName;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProvinceSoldTo;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.CompanyCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductName;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(item.Quantity);
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.StrDeliveryDate;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCodeDistributor;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A6", "K" + (cellRowIndex - 1).ToString());
                //foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                //{
                //    cell.BorderAround();
                //}
                #endregion

                #region Fill Data Header
                string positionDelivertDate = "C4";
                string positionExportNote = "C5";
                sheetDetail.get_Range(positionDelivertDate, positionDelivertDate).Cells[0] = header.DeliveryDate;
                sheetDetail.get_Range(positionExportNote, positionExportNote).Cells[0] = header.DOImportCode;
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "DOImport" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        public async Task Export()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\DOExport.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 2;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                int i = 0;
                foreach (var item in details)
                {
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Delivery;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.CompanyName;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProvinceSoldTo;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProvinceShipTo;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.CompanyCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductName;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(item.Quantity);
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.DeliveryDate;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCodeDistributor;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A2", "K" + (cellRowIndex - 1).ToString());
                //foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                //{
                //    cell.BorderAround();
                //}
                #endregion

                #region Fill Data Header
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "DOExport" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        public async Task ExportProcess()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryOrderHeader;
            //var details = _mainView.ReportDOProcess;
            var details = await _deliveryOrderHeaderRepository.ExecuteDataTable("proc_DeliveryOrderDetails_ReportProcess",
                         new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = header.DOImportCode });

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\DOProcessExport.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 2;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                //int i = 0;
                int rowCount = details.Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DeliveryDate"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CompanyCode"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["CompanyName"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductCode"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["ProductName"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["BatchCode"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DOQuantity"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["PreparedQty"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["DeliveredQty"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["NeedPrepareQty"].ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = details.Rows[i]["NeedDeliveryQty"].ToString();

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A2", "K" + (cellRowIndex - 1).ToString());
                //foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                //{
                //    cell.BorderAround();
                //}
                #endregion

                #region Fill Data Header
                //string positionDelivertDate = "C4";
                //string positionExportNote = "C5";
                //sheetDetail.get_Range(positionDelivertDate, positionDelivertDate).Cells[0] = header.DeliveryDate;
                //sheetDetail.get_Range(positionExportNote, positionExportNote).Cells[0] = header.DOImportCode;
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.DOImportCode + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }

        /// <summary>
        /// Save product packing to database, 2 cases:
        ///     Case 1: insert new product
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.DeliveryOrderHeader;
            var details = _mainView.DeliveryOrderDetails;
            var deletedDetails = _mainView.DeletedDeliveryOrderDetails;

            bool isSuccess = false;
            try
            {

                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                    _deliveryOrderDetailRepository = unitOfWork.Register(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;
                    _deliveryNoteDetailRepository = unitOfWork.Register(_deliveryNoteDetailRepository.GetType()) as IDeliveryNoteDetailRepository;
                    _splitNoteDetailRepository = unitOfWork.Register(_splitNoteDetailRepository.GetType()) as ISplitNoteDetailRepository;
                    _prepareNoteDetailRepository = unitOfWork.Register(_prepareNoteDetailRepository.GetType()) as IPrepareNoteDetailRepository;
                    _deliveryNoteHeaderRepository = unitOfWork.Register(_deliveryNoteHeaderRepository.GetType()) as IDeliveryNoteHeaderRepository;
                    _splitNoteHeaderRepository = unitOfWork.Register(_splitNoteHeaderRepository.GetType()) as ISplitNoteHeaderRepository;
                    _prepareNoteHeaderRepository = unitOfWork.Register(_prepareNoteHeaderRepository.GetType()) as IPrepareNoteHeaderRepository;
                    _productRepository = unitOfWork.Register(_productRepository.GetType()) as IProductRepository;
                    _productPackingRepository = unitOfWork.Register(_productPackingRepository.GetType()) as IProductPackingRepository;

                    var listPacking = await _productPackingRepository.GetAsync();
                    var listProduct = await _productRepository.GetAsync();



                    #region VerifyData
                    bool canContinue = true;

                    foreach (DeliveryOrderDetail item in details)
                    {
                        if (item.ChangedQuantity.HasValue)
                        {
                            //Check Reason
                            //If Have Reason
                            //Check if increase -> nothing to worry
                            //Check if decrease -> fuck this shit, check all data related to verify
                            if (string.IsNullOrWhiteSpace(item.ChangeReason))
                            {
                                _mainPresenter.SetMessage("Vui lòng nhập lý do thay đổi số lượng.", Utility.MessageType.Error);
                                canContinue = false;
                                return;
                            }

                            if (item.ChangedQuantity < 0)
                            {
                                _mainPresenter.SetMessage("Bạn nhập lung tung, chúng tôi sẽ ghi nhận và báo cáo cho cấp quản lý.", Utility.MessageType.Error);
                                canContinue = false;
                                return;
                            }


                            item.DiffQuantity = item.ChangedQuantity - item.Quantity;
                            if (item.DiffQuantity > 0)
                            {
                                //nothing to worry
                                item.Quantity = item.ChangedQuantity;
                                item.ChangedCase = 1;
                            }
                            else
                            {
                                if (item.LineNbr > 0)
                                {
                                    //Write store to check data
                                    //Verify can be continue or not
                                    var deliveryNoteDetails = await _deliveryNoteDetailRepository.GetAsync(a =>
                                                                a.DOCode == header.DOImportCode
                                                                && a.ProductID == item.ProductID
                                                                && a.BatchCode == item.BatchCode
                                                                );

                                    //Verify can be continue or not
                                    if (deliveryNoteDetails.Sum(a => a.ExportedQty) >= item.ChangedQuantity)
                                    {
                                        canContinue = true;
                                    }

                                    //Can be continue, set quantity
                                    if (canContinue)
                                    {
                                        item.Quantity = item.ChangedQuantity;
                                        item.ChangedCase = 2;
                                    }
                                }
                            }
                        }
                    }
                    #endregion


                    if (canContinue)
                    {
                        //Get Lastest DO Code
                        //Create Next DO Code
                        //Save DO Header
                        //Save DO Detail

                        #region Header
                        //Verify DeliveryDate
                        if (details.GroupBy(p => p.DeliveryDate).Count() > 1)
                        {
                            //throw new WrappedException(Messages.Error_DeliveryOrderMustSameDate);
                        }

                        if (String.IsNullOrWhiteSpace(header.DOImportCode))
                        {
                            string doImportCode = await _deliveryOrderHeaderRepository.GetNextNBR();

                            header.DOImportCode = doImportCode;
                            header.Status = DeliveryOrderHeader.status.New;

                            if (details.Count > 0)
                                header.DeliveryDate = details[0].DeliveryDate;

                            await _deliveryOrderHeaderRepository.Insert(header);
                        }
                        else
                        {
                            await _deliveryOrderHeaderRepository.Update(header, new object[] { header.DOImportCode });
                        }

                        if (dataChanged)
                        {
                            await _deliveryOrderHeaderRepository.ExecuteNonQuery("pp_DOImport_Merge_DeliveryOrderDetailHistories",
                                new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = header.DOImportCode },
                                new SqlParameter { ParameterName = "UserID", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                                new SqlParameter { ParameterName = "SitemapID", SqlDbType = SqlDbType.Int, Value = 12 },
                                new SqlParameter { ParameterName = "Method", SqlDbType = SqlDbType.VarChar, Value = AuditTrail.action.Update }
                                );
                        }
                        #endregion

                        //// foreach deleted DO details and delete it
                        foreach (var item in deletedDetails.Where(p => !String.IsNullOrWhiteSpace(p.DOImportCode) && p.LineNbr > 0))
                        {
                            await _deliveryOrderDetailRepository.Delete(new object[] { item.DOImportCode, item.LineNbr });
                        }

                        #region Process Detail
                        bool isFirstSave = false;
                        int maxLineNbr = details.Max(p => p.LineNbr);
                        if (maxLineNbr == 0)
                        {
                            isFirstSave = true;
                        }

                        for (int i = 0; i < details.Count; i++)
                        {
                            #region Save Data Detail
                            if (details[i].LineNbr == 0)
                            {
                                maxLineNbr++;
                                details[i].LineNbr = maxLineNbr;

                                //Set BeginDOQUantity
                                details[i].DOQuantity = details[i].Quantity;

                                //Not first Save, new Line
                                if (isFirstSave == false)
                                {
                                    details[i].ChangedCase = 3;
                                }
                            }
                            details[i].DOImportCode = header.DOImportCode;

                            await _deliveryOrderDetailRepository.InsertOrUpdate(details[i], new object[] { details[i].DOImportCode, details[i].LineNbr });
                            #endregion
                        }
                        #endregion
                        await unitOfWork.Commit();
                    }
                }



                #region Update Status all document related
                _deliveryOrderHeaderRepository = _mainPresenter.Resolve(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;

                    await _deliveryOrderHeaderRepository.ExecuteNonQuery("pp_DOImport_Merge_DeliveryOrderSum",
                               new SqlParameter { ParameterName = "DOImportCode", SqlDbType = SqlDbType.VarChar, Value = header.DOImportCode },
                               new SqlParameter { ParameterName = "UserID", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                               new SqlParameter { ParameterName = "SitemapID", SqlDbType = SqlDbType.Int, Value = 12 },
                               new SqlParameter { ParameterName = "Method", SqlDbType = SqlDbType.VarChar, Value = AuditTrail.action.Update }
                               );

                    //Update Status all document related
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("DOImportCode", header.DOImportCode);
                    listParam.Add(param1);
                    SqlParameter param2 = new SqlParameter("UserID", LoginInfo.UserID);
                    listParam.Add(param2);
                    SqlParameter param3 = new SqlParameter("SitemapID", 12);
                    listParam.Add(param3);
                    SqlParameter param4 = new SqlParameter("Method", AuditTrail.action.Update);
                    listParam.Add(param4);
                    await _deliveryOrderHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocumentDOImport", listParam.ToArray());
                    await unitOfWork.Commit();
                }
                #endregion

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _deliveryOrderHeaderRepository = _mainPresenter.Resolve(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                _deliveryOrderDetailRepository = _mainPresenter.Resolve(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    //await LoadDeliveryOrderHeaders();
                    //await LoadReportDO(header.DOImportCode);
                    //await LoadDeliveryOrderDetails(header.DOImportCode);
                    await Task.WhenAll(LoadDeliveryOrderHeaders());
                    _mainView.DeliveryOrderHeader = header;
                    //await LoadDeliveryOrderDetails(header.DOImportCode);
                }
            }
        }

        public override async Task Delete()
        {
            var header = _mainView.DeliveryOrderHeader;
            header.Status = DeliveryOrderHeader.status.Deleted;

            using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
            {
                _deliveryOrderHeaderRepository = unitOfWork.Register(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
                await _deliveryOrderHeaderRepository.Update(header, new object[] { header.DOImportCode });
                await unitOfWork.Commit();
            }

            _deliveryOrderHeaderRepository = _mainPresenter.Resolve(_deliveryOrderHeaderRepository.GetType()) as IDeliveryOrderHeaderRepository;
            _deliveryOrderDetailRepository = _mainPresenter.Resolve(_deliveryOrderDetailRepository.GetType()) as IDeliveryOrderDetailRepository;

            _mainPresenter.SetMessage("DO đã được xóa", Utility.MessageType.Information);

            await Task.WhenAll(LoadDeliveryOrderHeaders());

            //await LoadDeliveryOrderHeaders();
            //await LoadReportDO(header.DOImportCode);
            //await LoadDeliveryOrderDetails(header.DOImportCode);
            _mainView.DeliveryOrderHeader = header;

            //await LoadDeliveryOrderDetails(header.DOImportCode);

        }

        public void DeleteDeliveryOrderDetail(DeliveryOrderDetail prodPacking)
        {
            try
            {
                _mainView.DeletedDeliveryOrderDetails.Add(prodPacking);
                _mainView.DeliveryOrderDetails.Remove(prodPacking);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task UpdateRelateDO()
        {
            //await this.Save();
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var header = _mainView.DeliveryOrderHeader;

            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _splitNoteDetailRepository = unitOfWork.Register(_splitNoteDetailRepository.GetType()) as ISplitNoteDetailRepository;
                    List<SqlParameter> listParam = new List<SqlParameter>();
                    SqlParameter param1 = new SqlParameter("DOImportCode", header.DOImportCode);
                    listParam.Add(param1);
                    await _splitNoteDetailRepository.ExecuteNonQuery("proc_UpdateDeliveryCodeAllDocument", listParam.ToArray());

                    //await unitOfWork.Commit();
                }

                _mainPresenter.SetMessage(Messages.Information_CorrectRelateTicketDOSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }
        #endregion
    }
}