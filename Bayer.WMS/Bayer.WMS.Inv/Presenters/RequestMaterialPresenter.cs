﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IRequestMaterialView : IBaseView
    {
        System.Data.DataTable RequestMaterials { set; }
        RequestMaterial RequestMaterial { get; set; }
        IList<RequestMaterialLine> RequestMaterialLines { get; set; }
        System.Data.DataTable RequestMaterialLineSplits { get; set; }
    }

    public interface IRequestMaterialPresenter : IBasePresenter
    {
        Task LoadRequestMaterials();
        Task ImportExcel(string fileName);
        Task SuggestPickupProductFEFO();
        Task WarehouseKeeperBookPallet();
        //void DeleteSplit(RequestMaterialLineSplit split);
    }

    public class RequestMaterialPresenter : BasePresenter, IRequestMaterialPresenter
    {
        private IRequestMaterialView _mainView;
        private IRequestMaterialRepository _requestMaterialRepository;
        private IRequestMaterialLineRepository _requestMaterialLineRepository;
        private IRequestMaterialLineSplitRepository _requestMaterialLineSplitRepository;
        private IProductRepository _productRepository;
        private IPalletRepository _palletRepository;

        public RequestMaterialPresenter(IRequestMaterialView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IRequestMaterialRepository requestMaterialRepository,
            IRequestMaterialLineRepository requestMaterialLineRepository,
            IRequestMaterialLineSplitRepository requestMaterialLineSplitRepository,
            IProductRepository productRepository,
            IPalletRepository palletRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _requestMaterialRepository = requestMaterialRepository;
            _requestMaterialLineRepository = requestMaterialLineRepository;
            _requestMaterialLineSplitRepository = requestMaterialLineSplitRepository;
            _productRepository = productRepository;
            _palletRepository = palletRepository;

            _mainView.RequestMaterialLines = new List<RequestMaterialLine>();
            _mainView.RequestMaterialLineSplits = new System.Data.DataTable();
        }

        public async Task OnDocumentNbrSelectChange(DataRow selectedItem)
        {
            try
            {
                var reqMaterial = selectedItem.ToEntity<RequestMaterial>();
                await LoadRequestMaterialLines(reqMaterial.DocumentNbr);
                await LoadRequestMaterialLineSplits(reqMaterial.DocumentNbr);

                reqMaterial.State = EntityState.Unchanged;
                _mainView.RequestMaterial = reqMaterial;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadRequestMaterials()
        {
            try
            {
                _mainView.RequestMaterials = (await _requestMaterialRepository.GetAsync()).OrderByDescending(p => p.Date).ToList().ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadRequestMaterialLines(string documentNbr)
        {
            try
            {
                _mainView.RequestMaterialLines = await _requestMaterialLineRepository.GetIncludeProductAsync(p => p.DocumentNbr == documentNbr);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadRequestMaterialLineSplits(string documentNbr)
        {
            try
            {
                _mainView.RequestMaterialLineSplits = await _requestMaterialRepository.ExecuteDataTable("proc_RequestMaterialSplit_SelectPallet_ByDocument",
                new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = documentNbr });
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        //public void DeleteSplit(RequestMaterialLineSplit split)
        //{
        //    try
        //    {
        //        _mainView.DeletedRequestMaterialLineSplits.Add(split);
        //        _mainView.RequestMaterialLineSplits.Remove(split);
        //    }
        //    catch (Exception ex)
        //    {
        //        Utility.LogEx(ex, MethodBase.GetCurrentMethod());
        //        throw new WrappedException(Messages.Error_Common);
        //    }
        //}

        public override void Insert()
        {
            _mainView.RequestMaterialLines = new List<RequestMaterialLine>();
            _mainView.RequestMaterialLineSplits = new System.Data.DataTable();
        }

        public async Task ImportExcel(string fileName)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            int rowStart = 10;
            string start = "A" + rowStart;
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            try
            {
                workbook = workbooks.Open(fileName);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                //// get a range to work with
                range = sheetDetail.get_Range(start, Missing.Value);

                //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                range = range.get_End(XlDirection.xlDown);

                //// get the address of the bottom cell
                string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                //// specific end column
                end = downAddress.Remove(0, 1);

                end = "F" + end;
                end = "F5000";

                //// Get the range, then values from start to end
                range = sheetDetail.get_Range(start, end);
                var values = (object[,])range.Value2;
                int count = values.GetLength(0);

                string docNbr = $"{(sheetDetail.Cells[7, 5] as Range).Value2}";

                double d_date;
                double.TryParse($"{(sheetDetail.Cells[7, 2] as Range).Value2}", out d_date);

                string str_date = $"{(sheetDetail.Cells[7, 2] as Range).Value2}";

                DateTime date = d_date > 0 ? DateTime.FromOADate(d_date) : DateTime.Parse(str_date);

                var reqMaterial = await _requestMaterialRepository.GetSingleAsync(p => p.DocumentNbr == docNbr);
                IList<RequestMaterialLine> reqLines = new List<RequestMaterialLine>();
                if (reqMaterial != null)
                {
                    if (reqMaterial.Status == RequestMaterial.status.Completed)
                        throw new WrappedException("Phiếu yêu cầu nguyên liệu này đã hoàn tất, không thể import bổ sung.");

                    reqLines = await _requestMaterialLineRepository.GetAsync(p => p.DocumentNbr == docNbr);
                }

                reqMaterial = reqMaterial ?? new RequestMaterial();
                reqMaterial.DocumentNbr = docNbr;
                reqMaterial.Date = date;

                var products = await _productRepository.GetAsync(p => !p.IsDeleted);

                int max = reqLines.Count == 0 ? 0 : reqLines.Max(p => p.LineNbr);
                for (int i = 1; i <= count; i++)
                {
                    string productCode = $"{values[i, 2]}";
                    string productDescr = $"{values[i, 3]}";
                    if (String.IsNullOrWhiteSpace(productCode))
                        break;

                    //// Validate product
                    var product = products.FirstOrDefault(p => p.ProductCode == productCode);
                    if (product == null)
                        throw new WrappedException(String.Format(Messages.Validate_Product_NotExist, productDescr));

                    //// Validate request quantity
                    decimal requestQty;
                    if (!decimal.TryParse($"{values[i, 4]}", out requestQty))
                        throw new WrappedException(Messages.Validate_Required_RequestQty);
                    if (requestQty < 0)
                        throw new WrappedException(Messages.Validate_Range_RequestQty);

                    var line = reqLines.FirstOrDefault(p => p.ProductID == product.ProductID);
                    if (line == null)
                    {
                        line = new RequestMaterialLine();
                        line.LineNbr = max + 1;
                        line.RequestQty = 0;
                        line.TransferedQty = 0;

                        reqLines.Add(line);

                        max++;
                    }

                    line.DocumentNbr = reqMaterial.DocumentNbr;
                    line.ProductID = product.ProductID;
                    line.ProductCode = productCode;
                    line.ProductDescription = productDescr;
                    line.RequestQty = requestQty;
                    line.UOM = $"{values[i, 5]}";
                    line.Note = $"{values[i, 6]}";
                }

                _mainView.RequestMaterial = reqMaterial;
                _mainView.RequestMaterialLines = reqLines;
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(String.Format(Messages.Information_ImportSucessfullyWithError, String.Join(",", errorLine)), Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
            }
        }

        public async Task SuggestPickupProductFEFO()
        {
            try
            {
                var reqMaterial = _mainView.RequestMaterial;

                if (_requestMaterialRepository.GetSingleAsync(p => p.DocumentNbr == reqMaterial.DocumentNbr) == null)
                    throw new WrappedException("Dữ liệu chưa được lưu. Vui lòng lưu dữ liệu trước khi thực hiện chức năng này.");

                await _requestMaterialRepository.ExecuteNonQuery("proc_RequestMaterials_PickupPallet",
                new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = reqMaterial.DocumentNbr });

                await LoadRequestMaterialLines(reqMaterial.DocumentNbr);
                await LoadRequestMaterialLineSplits(reqMaterial.DocumentNbr);                
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _palletRepository = _mainPresenter.Resolve(_palletRepository.GetType()) as IPalletRepository;
                _requestMaterialLineSplitRepository = _mainPresenter.Resolve(_requestMaterialLineSplitRepository.GetType()) as IRequestMaterialLineSplitRepository;
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var reqMaterial = _mainView.RequestMaterial;
            var reqMaterialLines = _mainView.RequestMaterialLines;

            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _palletRepository = unitOfWork.Register(_palletRepository.GetType()) as IPalletRepository;
                    _requestMaterialRepository = unitOfWork.Register(_requestMaterialRepository.GetType()) as IRequestMaterialRepository;
                    _requestMaterialLineRepository = unitOfWork.Register(_requestMaterialLineRepository.GetType()) as IRequestMaterialLineRepository;
                    _requestMaterialLineSplitRepository = unitOfWork.Register(_requestMaterialLineSplitRepository.GetType()) as IRequestMaterialLineSplitRepository;

                    reqMaterial.IsRecordAuditTrail = true;
                    await _requestMaterialRepository.InsertOrUpdate(reqMaterial, new object[] { reqMaterial.DocumentNbr });

                    foreach (var line in reqMaterialLines)
                    {
                        await _requestMaterialLineRepository.InsertOrUpdate(line, new object[] { line.DocumentNbr, line.LineNbr });
                    }

                    await unitOfWork.Commit();
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _palletRepository = _mainPresenter.Resolve(_palletRepository.GetType()) as IPalletRepository;
                _requestMaterialRepository = _mainPresenter.Resolve(_requestMaterialRepository.GetType()) as IRequestMaterialRepository;
                _requestMaterialLineRepository = _mainPresenter.Resolve(_requestMaterialLineRepository.GetType()) as IRequestMaterialLineRepository;
                _requestMaterialLineSplitRepository = _mainPresenter.Resolve(_requestMaterialLineSplitRepository.GetType()) as IRequestMaterialLineSplitRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadRequestMaterials();

                    _mainView.RequestMaterial = reqMaterial;
                }
            }
        }

        public override async Task Delete()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var reqMaterial = _mainView.RequestMaterial;

            bool isSuccess = false;
            try
            {
                if (reqMaterial.Status == RequestMaterial.status.Processing)
                    throw new WrappedException("Phiếu yêu cầu nguyên liệu đang được thực hiện không thể xóa");
                if (reqMaterial.Status == RequestMaterial.status.Completed)
                    throw new WrappedException("Phiếu yêu cầu nguyên liệu đã hoàn tất không thể xóa");

                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _requestMaterialRepository = unitOfWork.Register(_requestMaterialRepository.GetType()) as IRequestMaterialRepository;

                    await _requestMaterialRepository.ExecuteNonQuery("proc_RequestMaterials_Delete",
                        new SqlParameter { ParameterName = "@DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = reqMaterial.DocumentNbr });

                    await unitOfWork.Commit();
                }


                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                _requestMaterialRepository = _mainPresenter.Resolve(_requestMaterialRepository.GetType()) as IRequestMaterialRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadRequestMaterials();
                    Insert();
                }
            }
        }

        public async Task WarehouseKeeperBookPallet()
        {
            try
            {
                var reqMaterial = _mainView.RequestMaterial;

                if (_requestMaterialRepository.GetSingleAsync(p => p.DocumentNbr == reqMaterial.DocumentNbr) == null)
                    throw new WrappedException("Dữ liệu chưa được lưu. Vui lòng lưu dữ liệu trước khi thực hiện chức năng này.");

                await _requestMaterialRepository.ExecuteNonQuery("proc_RequestMaterial_WarehouseKeeperBookPallet",
                new SqlParameter { ParameterName = "DocumentNbr", SqlDbType = SqlDbType.VarChar, Value = reqMaterial.DocumentNbr },
                new SqlParameter { ParameterName = "UserID", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                new SqlParameter { ParameterName = "SitemapID", SqlDbType = SqlDbType.Int, Value = 0 }
                );

                await LoadRequestMaterialLines(reqMaterial.DocumentNbr);
                await LoadRequestMaterialLineSplits(reqMaterial.DocumentNbr);
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _palletRepository = _mainPresenter.Resolve(_palletRepository.GetType()) as IPalletRepository;
                _requestMaterialLineSplitRepository = _mainPresenter.Resolve(_requestMaterialLineSplitRepository.GetType()) as IRequestMaterialLineSplitRepository;
            }
        }

    }
}