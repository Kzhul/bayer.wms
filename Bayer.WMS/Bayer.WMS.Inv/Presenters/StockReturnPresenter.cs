﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IStockReturnView : IBaseView
    {
        System.Data.DataTable StockReturnHeaders { set; }
        StockReturnHeader StockReturnHeader { get; set; }
        IList<StockReturnDetail> StockReturnDetails { get; set; }
        IList<StockReturnDetail> DeletedStockReturnDetails { get; set; }
    }

    public interface IStockReturnPresenter : IBasePresenter
    {
        Task LoadStockReturnHeaders();
        void DeleteDetail(StockReturnDetail item);
        Task Print();
        Task Confirm();
        Task ImportExcel(string fileName);
        Task Export();
    }

    public class StockReturnPresenter : BasePresenter, IStockReturnPresenter
    {
        private IStockReturnView _mainView;
        private IStockReturnHeaderRepository _stockReturnHeaderRepository;
        private IStockReturnDetailRepository _stockReturnDetailRepository;
        private IPalletRepository _palletRepository;
        private IProductRepository _productRepository;
        private IProductPackingRepository _productPackingRepository;

        public StockReturnPresenter(IStockReturnView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IStockReturnHeaderRepository stockReturnHeaderRepository,
            IStockReturnDetailRepository stockReturnDetailRepository,
            IPalletRepository palletRepository,
            IProductRepository productRepository,
            IProductPackingRepository productPackingRepository)
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _stockReturnHeaderRepository = stockReturnHeaderRepository;
            _stockReturnDetailRepository = stockReturnDetailRepository;
            _palletRepository = palletRepository;
            _productRepository = productRepository;
            _productPackingRepository = productPackingRepository;

            _mainView.StockReturnDetails = new List<StockReturnDetail>();
            _mainView.DeletedStockReturnDetails = new List<StockReturnDetail>();
            _mainView.StockReturnHeaders = new System.Data.DataTable();
        }

        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            var model = selectedItem.ToEntity<StockReturnHeader>();            
            _mainView.StockReturnHeader = model;
            await LoadStockReturnDetails(model.StockReturnCode);
        }

        public async Task LoadStockReturnHeaders()
        {
            try
            {
                _mainView.StockReturnHeaders = (await _stockReturnHeaderRepository.GetIncludeCreatedByAsync(p => !p.IsDeleted)).OrderByDescending(p => p.CreatedDateTime).ToList().ToDataTable();
                if (_mainView.StockReturnHeader != null)
                {
                    await LoadStockReturnDetails(_mainView.StockReturnHeader.StockReturnCode);
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadStockReturnDetails(string StockReturnCode)
        {
            _mainView.StockReturnDetails = await _stockReturnDetailRepository.GetIncludeProductAsync(p => p.StockReturnCode == StockReturnCode);
        }

        public void DeleteDetail(StockReturnDetail item)
        {
            try
            {
                _mainView.DeletedStockReturnDetails.Add(item);
                _mainView.StockReturnDetails.Remove(item);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override void Insert()
        {
            _mainView.StockReturnHeader = new StockReturnHeader();
            _mainView.StockReturnDetails = new List<StockReturnDetail>();
            _mainView.DeletedStockReturnDetails = new List<StockReturnDetail>();
        }

        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReturnHeader;
            var details = _mainView.StockReturnDetails;
            var deletedDetails = _mainView.DeletedStockReturnDetails;
            bool isSuccess = false;

            if (details.Count == 0)
            {
                _mainPresenter.SetMessage(Messages.Validate_Range_Quantity, Utility.MessageType.Error);
                return;
            }

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuGiaoHang.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\PhieuGiaoHang\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Header
                //sheetDetail.get_Range(positionCompanyName, positionCompanyName).Cells[0] = header.CompanyCode + " - " + header.CompanyName + " - " + header.Province;
                //sheetDetail.get_Range(positionStockReturnCode, positionStockReturnCode).Cells[0] = header.StockReturnCode;
                //sheetDetail.get_Range(positionBarCodeStockReturnCode, positionBarCodeStockReturnCode).Cells[0] = "*" + header.StockReturnCode + "*";

                //sheetDetail.get_Range(positionDeliveryDate, positionDeliveryDate).Cells[0] = header.StrDeliveryDate;
                //sheetDetail.get_Range(positionOrderNumbers, positionOrderNumbers).Cells[0] = header.ListOrderNumbers;

                //sheetDetail.get_Range(positionBoxes, positionBoxes).Cells[0] = Utility.DecimalToString( header.TotalBoxes);
                //sheetDetail.get_Range(positionBags, positionBags).Cells[0] = Utility.DecimalToString(header.TotalBags);
                //sheetDetail.get_Range(positionBuckets, positionBuckets).Cells[0] = Utility.DecimalToString(header.TotalBuckets);
                //sheetDetail.get_Range(positionCans, positionCans).Cells[0] = Utility.DecimalToString(header.TotalCans);

                //sheetDetail.get_Range(positionFreeItems, positionFreeItems).Cells[0] = header.ListFreeItems;

                //sheetDetail.get_Range(positionGrossWeight, positionGrossWeight).Cells[0] = header.GrossWeight;

                //sheetDetail.get_Range(positionTruckNo, positionTruckNo).Cells[0] = header.TruckNo;

                //sheetDetail.get_Range(positionDriver, positionDriver).Cells[0] = header.Driver;

                //sheetDetail.get_Range(positionDriverIdentification, positionDriverIdentification).Cells[0] = header.DriverIdentification;

                //sheetDetail.get_Range(positionReceiptBy, positionReceiptBy).Cells[0] = header.ReceiptBy;
                #endregion

                #region Fill Page Number Info
                sheetDetail.PageSetup.LeftFooter = @"Phát hành theo biểu mẫu WH-55-03 đã được đăng kí"; // This worked correctly
                sheetDetail.PageSetup.RightFooter = @"Page &P of &N"; // This worked correctly
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += header.StockReturnCode + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    //ProcessStartInfo startInfo;
                    //startInfo = new ProcessStartInfo(exportPath);
                    //startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    //Process newProcess = new Process();
                    //newProcess.StartInfo = startInfo;
                    //newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    //printFromExcel(exportPath);

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully + ": " + header.StockReturnCode, Utility.MessageType.Information);
                }
            }
        }

        public static void PrintFromExcel(string yourFileName)
        {
            //Microsoft.Office.Interop.Excel.Application xl = new Microsoft.Office.Interop.Excel.Application();
            //xl.Workbooks.Open(yourFileName, Type., optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);
            //xl.Worksheets.PrintOut(optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter, optionalParameter);

            //if (xl != null)
            //{
            //    xl.Quit();
            //}
        }

        public async Task Confirm()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());
            var header = _mainView.StockReturnHeader;
            bool isSuccess = false;

            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                SqlParameter param1 = new SqlParameter("Code", header.StockReturnCode);
                listParam.Add(param1);
                SqlParameter param2 = new SqlParameter("DOImportCode", string.Empty);
                listParam.Add(param2);
                SqlParameter param3 = new SqlParameter("Type", "StockReturn");
                listParam.Add(param3);
                SqlParameter param4 = new SqlParameter("Status", DeliveryNoteHeader.status.Complete);
                listParam.Add(param4);

                await _stockReturnHeaderRepository.ExecuteNonQuery("proc_UpdateStatusAllDocument", listParam.ToArray());
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _stockReturnHeaderRepository = _mainPresenter.Resolve(_stockReturnHeaderRepository.GetType()) as IStockReturnHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_ConfirmSucessfully, Utility.MessageType.Information);

                    await LoadStockReturnHeaders();
                }
            }
        }

        public async Task ImportExcel(string fileName)
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            int rowStart = 15;
            string start = "A" + rowStart;
            string end = String.Empty;
            Dictionary<int, string> errorLine = new Dictionary<int, string>();

            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            try
            {
                workbook = workbooks.Open(fileName);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                //// get a range to work with
                range = sheetDetail.get_Range(start, Missing.Value);

                //// get the end of values toward the bottom, looking in the last column (will stop at first empty cell)
                range = range.get_End(XlDirection.xlDown);

                //// get the address of the bottom cell
                string downAddress = range.get_Address(false, false, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                //// specific end column
                end = downAddress.Remove(0, 1);

                end = "L" + end;
                end = "L5000";

                //// Get the range, then values from start to end
                range = sheetDetail.get_Range(start, end);
                var values = (object[,])range.Value2;
                int count = values.GetLength(0);

                var header = await _stockReturnHeaderRepository.GetSingleAsync(p => p.StockReturnCode == _mainView.StockReturnHeader.StockReturnCode && !p.IsDeleted);
                IList<StockReturnDetail> details = new List<StockReturnDetail>();
                if (header != null)
                {
                    if (header.Status == StockReturnHeader.status.Completed)
                        throw new WrappedException("Phiếu nhập kho này đã hoàn tất, không thể import bổ sung.");

                    details = await _stockReturnDetailRepository.GetAsync(p => p.StockReturnCode == _mainView.StockReturnHeader.StockReturnCode);
                }

                header = header ?? new StockReturnHeader();
                header.Date = DateTime.Today;
                header.CreatedByName = $"{LoginInfo.LastName} {LoginInfo.FirstName}";

                var products = await _productRepository.GetAsync(p => !p.IsDeleted);
                var productPackagings = await _productPackingRepository.GetAsync(p => p.Type != ProductPacking.type.Pallet);

                for (int i = 1; i <= count; i++)
                {
                    string productCode = $"{values[i, 2]}";
                    string productDescr = $"{values[i, 3]}";
                    if (String.IsNullOrWhiteSpace(productCode))
                        break;

                    int lineNbr = int.Parse($"{values[i, 1]}");

                    //// Validate product
                    var product = products.FirstOrDefault(p => p.ProductCode == productCode);
                    if (product == null)
                        throw new WrappedException(String.Format(Messages.Validate_Product_NotExist, productDescr));

                    //// Validate request quantity
                    decimal planQty;
                    if (!decimal.TryParse($"{values[i, 7]}", out planQty))
                        throw new WrappedException(Messages.Validate_Required_RequestQty);
                    if (planQty < 0)
                        throw new WrappedException(Messages.Validate_Range_RequestQty);

                    var line = details.FirstOrDefault(p => p.LineNbr == lineNbr);
                    if (line == null)
                    {
                        line = new StockReturnDetail();
                        line.LineNbr = lineNbr;
                        line.ReceivedQty = 0;
                        line.ReturnQty = planQty;

                        details.Add(line);
                    }

                    line.ProductID = product.ProductID;
                    line.ProductCode = productCode;
                    line.ProductDescription = productDescr;
                    line.BatchCode = $"{values[i, 6]}";
                    line.PalletCode = $"{values[i, 8]}";
                    line.ReturnedFrom = $"{values[i, 9]}";
                    line.QA_QCCheck = $"{values[i, 11]}";
                    line.Description = $"{values[i, 12]}";

                    var packaging = productPackagings.FirstOrDefault(p => p.ProductID == product.ProductID);
                    if (packaging != null)
                    {
                        line.PackageSize = packaging.Quantity.Value;
                    }

                    line.UOM = product.UOM;
                }

                _mainView.StockReturnHeader = header;
                _mainView.StockReturnDetails = details;
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //// show error excel line
                if (errorLine.Count > 0)
                    _mainPresenter.SetMessage(String.Format(Messages.Information_ImportSucessfullyWithError, String.Join(",", errorLine)), Utility.MessageType.Information);
                else
                    _mainPresenter.SetMessage(Messages.Information_ImportSucessfully, Utility.MessageType.Information);
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReturnHeader;
            var details = _mainView.StockReturnDetails;
            var deletedDetails = _mainView.DeletedStockReturnDetails;
            bool isSuccess = false;

            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _stockReturnHeaderRepository = unitOfWork.Register(_stockReturnHeaderRepository.GetType()) as IStockReturnHeaderRepository;
                    _stockReturnDetailRepository = unitOfWork.Register(_stockReturnDetailRepository.GetType()) as IStockReturnDetailRepository;

                    if (String.IsNullOrWhiteSpace(header.StockReturnCode))
                    {
                        string code = await _stockReturnHeaderRepository.GetNextNBR();
                        header.StockReturnCode = code;
                        await _stockReturnHeaderRepository.Insert(header);
                    }
                    else
                    {
                        await _stockReturnHeaderRepository.Update(header, null, new string[] { "Status" }, new object[] { header.StockReturnCode });
                    }

                    //// foreach deleted DO details and delete it
                    foreach (var item in deletedDetails)
                    {
                        await _stockReturnDetailRepository.Delete(new object[] { item.StockReturnCode, item.LineNbr });
                    }

                    foreach (var item in details)
                    {
                        item.StockReturnCode = header.StockReturnCode;
                        await _stockReturnDetailRepository.InsertOrUpdate(item, null, new string[] { "Status", "ReceivedQty" }, new object[] { item.StockReturnCode, item.LineNbr });
                    }

                    await unitOfWork.Commit();
                }

                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _stockReturnHeaderRepository = _mainPresenter.Resolve(_stockReturnHeaderRepository.GetType()) as IStockReturnHeaderRepository;
                _stockReturnDetailRepository = _mainPresenter.Resolve(_stockReturnDetailRepository.GetType()) as IStockReturnDetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);

                    await LoadStockReturnHeaders();
                    _mainView.StockReturnHeader = header;
                }
            }

        }

        public override async Task Delete()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReturnHeader;

            bool isSuccess = false;
            try
            {
                if (header.Status != StockReturnHeader.status.New)
                {
                    if (header.Status == StockReturnHeader.status.Processing)
                        throw new WrappedException("Phiếu nhập kho đang được kiểm tra không thể xóa");
                    if (header.Status == StockReturnHeader.status.Completed)
                        throw new WrappedException("Phiếu nhập kho đã kiểm tra hoàn tất không thể xóa");
                }

                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _stockReturnHeaderRepository = unitOfWork.Register(_stockReturnHeaderRepository.GetType()) as IStockReturnHeaderRepository;

                    await _stockReturnHeaderRepository.ExecuteNonQuery("proc_StockReturnHeaders_Delete",
                        new SqlParameter { ParameterName = "@StockReturnCode", SqlDbType = SqlDbType.VarChar, Value = header.StockReturnCode });

                    await unitOfWork.Commit();
                }


                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                _stockReturnHeaderRepository = _mainPresenter.Resolve(_stockReturnHeaderRepository.GetType()) as IStockReturnHeaderRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadStockReturnHeaders();
                    Insert();
                }
            }
        }

        public async Task Export()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.StockReturnHeader;
            var details = _mainView.StockReturnDetails;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\PhieuTraHangNguyenLieu.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 11;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                int i = 0;
                foreach (var item in details)
                {
                    i++;
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = (i).ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductDescription;
                    cellColumnIndex++;
                    
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Description;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = string.Empty;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCode;
                    cellColumnIndex++;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A11", "K" + (cellRowIndex - 1).ToString());
                foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                {
                    cell.BorderAround();
                }
                #endregion

                #region Fill Data Header
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "PhieuTraHangNguyenLieu" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }
    }
}
