﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IReturnDOView : IBaseView
    {
        ReturnDOHeader ReturnDOHeader { get; set; }

        System.Data.DataTable ReturnDOHeaders { set; }

        IList<ReturnDODetail> ReturnDODetails { get; set; }

        IList<ReturnDODetail> DeletedReturnDODetails { get; set; }

        System.Data.DataTable ReportDODetails { get; set; }
    }

    public interface IReturnDOPresenter : IBasePresenter
    {
        Task LoadReturnDOHeaders();

        Task LoadDetailsByDeliveryCode(string DOCode);

        void DeleteReturnDODetail(ReturnDODetail prodPacking);

        Task Print();

        Task LoadReportDO(string ReturnCode);
    }

    public class ReturnDOPresenter : BasePresenter, IReturnDOPresenter
    {
        private IReturnDOView _mainView;
        private IReturnDOHeaderRepository _returnDOHeaderRepository;
        private IReturnDODetailRepository _returnDODetailRepository;
        private IDeliveryNoteHeaderRepository _deliveryNoteHeaderRepository;
        private IDeliveryNoteDetailRepository _deliveryNoteDetailRepository;
        private ISplitNoteHeaderRepository _splitNoteHeaderRepository;
        private ISplitNoteDetailRepository _splitNoteDetailRepository;
        private IPrepareNoteHeaderRepository _prepareNoteHeaderRepository;
        private IPrepareNoteDetailRepository _prepareNoteDetailRepository;
        private IProductRepository _productRepository;
        private IProductPackingRepository _productPackingRepository;
        private IUserRepository _userRepository;

        public ReturnDOPresenter(IReturnDOView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IReturnDOHeaderRepository returnDORepository,
            IReturnDODetailRepository returnDOImportLogRepository,
            IDeliveryNoteHeaderRepository deliveryNoteHeaderRepository,
            IDeliveryNoteDetailRepository deliveryNoteDetailRepository,
            ISplitNoteHeaderRepository splitNoteHeaderRepository,
            ISplitNoteDetailRepository splitNoteDetailRepository,
            IPrepareNoteHeaderRepository prepareNoteHeaderRepository,
            IPrepareNoteDetailRepository prepareNoteDetailRepository,
            IProductRepository productRepository,
            IProductPackingRepository productPackingRepository,
            IUserRepository userRepository
            )
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _returnDOHeaderRepository = returnDORepository;
            _returnDODetailRepository = returnDOImportLogRepository;
            _deliveryNoteHeaderRepository = deliveryNoteHeaderRepository;
            _deliveryNoteDetailRepository = deliveryNoteDetailRepository;
            _splitNoteHeaderRepository = splitNoteHeaderRepository;
            _splitNoteDetailRepository = splitNoteDetailRepository;
            _prepareNoteHeaderRepository = prepareNoteHeaderRepository;
            _prepareNoteDetailRepository = prepareNoteDetailRepository;
            _productRepository = productRepository;
            _productPackingRepository = productPackingRepository;
            _userRepository = userRepository;

            _mainView.ReturnDODetails = new List<ReturnDODetail>();
            _mainView.DeletedReturnDODetails = new List<ReturnDODetail>();
            _mainView.ReportDODetails = new System.Data.DataTable();
        }

        public async void OnCodeSelectChange(DataRow selectedItem)
        {
            var model = selectedItem.ToEntity<ReturnDOHeader>();
            await LoadReturnDODetails(model.ReturnDOCode);
            await LoadReportDO(model.ReturnDOCode);
            _mainView.ReturnDOHeader = model;
        }

        public async Task LoadReturnDOHeaders()
        {
            var model = await _returnDOHeaderRepository.GetAsync(p => !p.IsDeleted);
            _mainView.ReturnDOHeaders = model.OrderByDescending(a => a.CreatedDateTime).ToList().ToDataTable();
        }

        public async Task LoadReturnDODetails(string returnDOCode)
        {
            _mainView.ReturnDODetails = await _returnDODetailRepository.GetIncludeProductAsync(p => p.ReturnDOCode == returnDOCode);
        }

        public async Task LoadDetailsByDeliveryCode(string DOCode)
        {
            _mainView.ReturnDOHeader.Delivery = DOCode;
            _mainView.ReturnDODetails = await _returnDODetailRepository.LoadByDeliveryCode(DOCode);
        }

        public async Task LoadReportDO(string DOCode)
        {
            List<SqlParameter> listParam = new List<SqlParameter>();
            SqlParameter code = new SqlParameter("DOImportCode", DOCode);
            listParam.Add(code);
            SqlParameter paramFromDate = new SqlParameter("FromDate", DBNull.Value);
            listParam.Add(paramFromDate);
            SqlParameter paramToDate = new SqlParameter("ToDate", DBNull.Value);
            listParam.Add(paramToDate);
            _mainView.ReportDODetails = await _returnDOHeaderRepository.ExecuteDataTable("proc_ReportReturnDO", listParam.ToArray());
        }

        public override void Insert()
        {
            _mainView.ReturnDOHeader = new ReturnDOHeader();
            _mainView.ReturnDOHeader.ReturnDate = DateTime.Today;
            _mainView.ReturnDOHeader.Status = ReturnDOHeader.status.New;

            _mainView.ReturnDODetails = new List<ReturnDODetail>();
            _mainView.DeletedReturnDODetails = new List<ReturnDODetail>();
            _mainView.ReportDODetails = new System.Data.DataTable();
        }
        
        public void DeleteReturnDODetail(ReturnDODetail item)
        {
            try
            {
                _mainView.DeletedReturnDODetails.Add(item);
                _mainView.ReturnDODetails.Remove(item);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }


        /// <summary>
        /// Save product packing to database, 2 cases:
        ///     Case 1: insert new product
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.ReturnDOHeader;
            var details = _mainView.ReturnDODetails;
            var deletedDetails = _mainView.DeletedReturnDODetails;

            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _returnDOHeaderRepository = unitOfWork.Register(_returnDOHeaderRepository.GetType()) as IReturnDOHeaderRepository;
                    _returnDODetailRepository = unitOfWork.Register(_returnDODetailRepository.GetType()) as IReturnDODetailRepository;

                    //Get Lastest DO Code
                    //Create Next DO Code
                    //Save DO Header
                    //Save DO Detail

                    if (String.IsNullOrWhiteSpace(header.ReturnDOCode))
                    {
                        string returnDOCode = await _returnDOHeaderRepository.GetNextNBR();

                        header.ReturnDOCode = returnDOCode;
                        header.Status = ReturnDOHeader.status.New;

                        if (details.Count > 0)
                            header.ReturnDate = details[0].DeliveryDate;

                        await _returnDOHeaderRepository.Insert(header);
                    }
                    else
                    {
                        await _returnDOHeaderRepository.Update(header, new object[] { header.ReturnDOCode });
                    }

                    //// foreach deleted DO details and delete it
                    foreach (var item in deletedDetails.Where(p => !String.IsNullOrWhiteSpace(p.ReturnDOCode) && p.LineNbr > 0))
                    {
                        await _returnDODetailRepository.Delete(new object[] { item.ReturnDOCode, item.LineNbr });
                    }

                    int maxLineNbr = details.Max(p => p.LineNbr);

                    for (int i = 0; i < details.Count; i++)
                    {
                        if (details[i].LineNbr == 0)
                            details[i].LineNbr = maxLineNbr++;
                        details[i].ReturnDOCode = header.ReturnDOCode;

                        await _returnDODetailRepository.InsertOrUpdate(details[i], new object[] { details[i].ReturnDOCode, details[i].LineNbr });

                        if (details[i].RequestReturnQty > 0)
                        {
                            List<SqlParameter> listParam = new List<SqlParameter>();

                            SqlParameter code = new SqlParameter("DOImportCode", details[i].DOImportCode);
                            listParam.Add(code);

                            SqlParameter LineNbr = new SqlParameter("LineNbr", details[i].LineNbr);
                            listParam.Add(LineNbr);

                            SqlParameter Delivery = new SqlParameter("Delivery", details[i].Delivery);
                            listParam.Add(Delivery);

                            SqlParameter ProductID = new SqlParameter("ProductID", details[i].ProductID);
                            listParam.Add(ProductID);

                            SqlParameter BatchCode = new SqlParameter("BatchCode", details[i].BatchCode);
                            listParam.Add(BatchCode);

                            SqlParameter CompanyCode = new SqlParameter("CompanyCode", details[i].CompanyCode);
                            listParam.Add(CompanyCode);

                            SqlParameter RequestReturnQty = new SqlParameter("RequestReturnQty", details[i].RequestReturnQty);
                            listParam.Add(RequestReturnQty);

                            SqlParameter UserID = new SqlParameter("UserID", LoginInfo.UserID);
                            listParam.Add(UserID);

                            SqlParameter SitemapID = new SqlParameter("SitemapID", 33);//HARDCODE ReturnDOView 33
                            listParam.Add(SitemapID);

                            SqlParameter Method = new SqlParameter("Method", "ReturnDOUpdateAllTicketDetailData");
                            listParam.Add(Method);


                            _mainView.ReportDODetails = await _returnDODetailRepository.ExecuteDataTable("proc_ReturnDOUpdateAllTicketDetailData", listParam.ToArray());
                        }
                    }

                    await unitOfWork.Commit();
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh();
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }

                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _returnDOHeaderRepository = _mainPresenter.Resolve(_returnDOHeaderRepository.GetType()) as IReturnDOHeaderRepository;
                _returnDODetailRepository = _mainPresenter.Resolve(_returnDODetailRepository.GetType()) as IReturnDODetailRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadReturnDOHeaders();
                    await LoadReportDO(header.ReturnDOCode);

                    _mainView.ReturnDOHeader = header;
                }
            }

        }
        
        public async Task Print()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var header = _mainView.ReturnDOHeader;
            var details = _mainView.ReturnDODetails;

            // Creating a Excel object. 
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;
            string templatePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\ExcelTemplate\DOImport.xlsx";
            string exportPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\Report\";
            try
            {
                System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(exportPath);
                directory.Empty();
            }
            catch
            {
                //Do Nothing
            }

            bool isSuccess = false;
            int cellRowIndex = 7;
            int cellColumnIndex = 1;
            try
            {
                workbook = excelApp.Workbooks.Open(templatePath);
                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Fill Data Detail
                int i = 0;
                foreach (var item in details)
                {                    
                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = i.ToString();
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Delivery;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.CompanyName;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.Province;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.CompanyCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.ProductDescription;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCode;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = Utility.DecimalToString(item.Quantity);
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.StrDeliveryDate;
                    cellColumnIndex++;

                    sheetDetail.Cells[cellRowIndex, cellColumnIndex] = item.BatchCodeDistributor;

                    //NewRow
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }
                Microsoft.Office.Interop.Excel.Range rangeDetail = sheetDetail.get_Range("A6", "K" + (cellRowIndex - 1).ToString());
                foreach (Microsoft.Office.Interop.Excel.Range cell in rangeDetail.Cells)
                {
                    cell.BorderAround();
                }
                #endregion

                #region Fill Data Header
                string positionDelivertDate = "C4";
                string positionExportNote = "C5";
                sheetDetail.get_Range(positionDelivertDate, positionDelivertDate).Cells[0] = header.ReturnDate;
                sheetDetail.get_Range(positionExportNote, positionExportNote).Cells[0] = header.ReturnDOCode;
                #endregion

                #region Fill Page Number Info
                #endregion

                excelApp.DisplayAlerts = false;
                exportPath += "DOImport" + DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss") + ".xlsx";
                workbook.SaveAs(exportPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                #region Clean Excel Object
                try
                {
                    workbook.Close();
                    workbooks.Close();

                    if (excelApp != null)
                        excelApp.Quit();
                    if (range != null)
                        Marshal.ReleaseComObject(range);
                    if (sheetDetail != null)
                        Marshal.ReleaseComObject(sheetDetail);
                    if (workbook != null)
                        Marshal.ReleaseComObject(workbook);
                    if (workbooks != null)
                        Marshal.ReleaseComObject(workbooks);
                    if (excelApp != null)
                        Marshal.ReleaseComObject(excelApp);

                    Thread.Sleep(1000);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch (Exception ex)
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                }
                #endregion


                if (isSuccess)
                {
                    #region Open Excel File In Edit Mode
                    ProcessStartInfo startInfo;
                    startInfo = new ProcessStartInfo(exportPath);
                    startInfo.Verb = "Edit";    // <<=== put here "Edit" 
                    Process newProcess = new Process();
                    newProcess.StartInfo = startInfo;
                    newProcess.Start();  // <<=== here you default editor should start... 
                    #endregion

                    _mainPresenter.SetMessage(Messages.Information_PrintSucessfully, Utility.MessageType.Information);
                }
            }
        }
    }
}
