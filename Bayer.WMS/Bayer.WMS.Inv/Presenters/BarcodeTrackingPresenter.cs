﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IBarcodeTrackingView : IBaseView
    {
        System.Data.DataTable BarcodeTrackingDetails { get; set; }
        System.Data.DataTable BarcodeTrackingInfo { get; set; }
    }

    public interface IBarcodeTrackingPresenter : IBasePresenter
    {
        Task Search(string code);
        Task Search(string code, string type);
    }

    public class BarcodeTrackingPresenter : BasePresenter, IBarcodeTrackingPresenter
    {
        private IBarcodeTrackingView _mainView;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository1;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository2;

        public BarcodeTrackingPresenter(IBarcodeTrackingView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository1,
            IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository2)
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryOrderHeaderRepository1 = deliveryOrderHeaderRepository1;
            _deliveryOrderHeaderRepository2 = deliveryOrderHeaderRepository2;
        }

        public async Task Search(string barcode)
        {
            try
            {
                string type = String.Empty;

                if (Utility.PalletRegex.IsMatch(barcode))
                    type = "P";
                else if (Utility.CartonRegex.IsMatch(barcode))
                    type = "C";
                else if (Utility.Carton2Regex.IsMatch(barcode))
                    type = "K";
                else
                {
                    string decryptedBarcode;
                    try
                    {
                        //// scanned barcode is product
                        decryptedBarcode = Utility.AESDecrypt(barcode.Split('|')[0].Remove(0, 2));
                    }
                    catch
                    {
                        throw new WrappedException(Messages.Error_BarcodeIsInvalid);
                    }

                    //string decryptedBarcode = barcode;
                    if (Utility.ProductRegex.IsMatch(decryptedBarcode))
                    {
                        type = "E";
                        barcode = Utility.ProductRegex.Match(decryptedBarcode).Captures[0].Value;
                    }
                    else
                    {
                        throw new WrappedException(Messages.Error_BarcodeIsInvalid);
                    }
                }

                var taskGetTracking = _deliveryOrderHeaderRepository1.ExecuteDataTable("proc_Barcode_Tracking",
                     new SqlParameter { ParameterName = "Barcode", SqlDbType = SqlDbType.VarChar, Value = barcode },
                     new SqlParameter { ParameterName = "Type", SqlDbType = SqlDbType.VarChar, Value = type });

                var taskGetTrackingInfo = _deliveryOrderHeaderRepository2.ExecuteDataTable("proc_Barcode_TrackingInfo",
                    new SqlParameter { ParameterName = "Barcode", SqlDbType = SqlDbType.VarChar, Value = barcode },
                    new SqlParameter { ParameterName = "Type", SqlDbType = SqlDbType.VarChar, Value = type });

                await Task.WhenAll(taskGetTracking, taskGetTrackingInfo);

                _mainView.BarcodeTrackingDetails = taskGetTracking.Result;
                _mainView.BarcodeTrackingInfo = taskGetTrackingInfo.Result;
            }
            catch (WrappedException)
            {
                throw;
            }
        }

        public async Task Search(string barcode, string type)
        {
            try
            {
                //P,C,K,E                
                var taskGetTracking = _deliveryOrderHeaderRepository1.ExecuteDataTable("proc_Barcode_Tracking",
                     new SqlParameter { ParameterName = "Barcode", SqlDbType = SqlDbType.VarChar, Value = barcode },
                     new SqlParameter { ParameterName = "Type", SqlDbType = SqlDbType.VarChar, Value = type });

                var taskGetTrackingInfo = _deliveryOrderHeaderRepository2.ExecuteDataTable("proc_Barcode_TrackingInfo",
                    new SqlParameter { ParameterName = "Barcode", SqlDbType = SqlDbType.VarChar, Value = barcode },
                    new SqlParameter { ParameterName = "Type", SqlDbType = SqlDbType.VarChar, Value = type });

                await Task.WhenAll(taskGetTracking, taskGetTrackingInfo);

                _mainView.BarcodeTrackingDetails = taskGetTracking.Result;
                _mainView.BarcodeTrackingInfo = taskGetTrackingInfo.Result;
            }
            catch (WrappedException)
            {
                throw;
            }
        }
    }
}
