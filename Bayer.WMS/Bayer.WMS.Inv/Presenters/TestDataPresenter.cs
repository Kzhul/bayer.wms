﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Inv.Presenters
{
    public interface ITestDataView : IBaseView
    {

    }

    public interface ITestDataPresenter : IBasePresenter
    {

    }

    public class TestDataPresenter : BasePresenter, ITestDataPresenter
    {
        private ITestDataView _mainView;
        private IProductRepository _productRepository;
        public TestDataPresenter(ITestDataView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IProductRepository productRepository
            )
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _productRepository = productRepository;
        }
    }
}