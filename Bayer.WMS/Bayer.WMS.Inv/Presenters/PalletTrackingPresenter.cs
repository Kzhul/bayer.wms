﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IPalletTrackingView : IBaseView
    {
        System.Data.DataTable PalletTrackingDetails { get; set; }
        System.Data.DataTable PalletTrackingInfo { get; set; }
    }

    public interface IPalletTrackingPresenter : IBasePresenter
    {
        Task Search();
    }

    public class PalletTrackingPresenter : BasePresenter, IPalletTrackingPresenter
    {
        private IPalletTrackingView _mainView;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository1;
        private IDeliveryOrderHeaderRepository _deliveryOrderHeaderRepository2;

        public PalletTrackingPresenter(IPalletTrackingView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter,
            IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository1,
            IDeliveryOrderHeaderRepository deliveryOrderHeaderRepository2)
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deliveryOrderHeaderRepository1 = deliveryOrderHeaderRepository1;
            _deliveryOrderHeaderRepository2 = deliveryOrderHeaderRepository2;
        }

        public async Task Search()
        {
            try
            {
                string type = String.Empty;
                var taskGetTracking = _deliveryOrderHeaderRepository1.ExecuteDataTable("proc_Pallets_Tracking");

                var taskGetTrackingInfo = _deliveryOrderHeaderRepository2.ExecuteDataTable("proc_Pallets_TrackingInfo");

                await Task.WhenAll(taskGetTracking, taskGetTrackingInfo);

                _mainView.PalletTrackingDetails = taskGetTracking.Result;
                _mainView.PalletTrackingInfo = taskGetTrackingInfo.Result;
            }
            catch (WrappedException)
            {
                throw;
            }
        }
    }
}
