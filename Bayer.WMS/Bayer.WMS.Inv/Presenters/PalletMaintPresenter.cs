﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Inv.Presenters
{
    public interface IPalletMaintView : IBaseView
    {
        System.Data.DataTable Pallets { get; set; }

        IList<Pallet> DeletedPallets { get; set; }

        string PalletType { get; }

        string PalletCode { get; }
    }

    public interface IPalletMaintPresenter : IBasePresenter
    {
        Task GeneratePallet(string type, int usageTime, DateTime startDate, DateTime expiryDate, int qty);

        Task PrintPallet(string palletCode);

        Task Search(string type, string strName);

        void DeletePallet(Pallet pallet);
    }

    public class PalletMaintPresenter : BasePresenter, IPalletMaintPresenter
    {
        private IPalletMaintView _mainView;
        private IPalletRepository _palletRepository;

        public PalletMaintPresenter(IPalletMaintView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IPalletRepository palletRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _palletRepository = palletRepository;

            _mainView.Pallets = new System.Data.DataTable();
            _mainView.DeletedPallets = new List<Pallet>();
        }

        /// <summary>
        /// Get product packing from database
        /// </summary>
        /// <returns></returns>
        public override async Task LoadMainData()
        {
            try
            {
                Expression<Func<Pallet, bool>> predicate = null;
                string type = _mainView.PalletType;

                if (type == Pallet.type.Wood)
                    predicate = p => p.Type == Pallet.type.Wood;
                else if (type == Pallet.type.Plastic)
                    predicate = p => p.Type == Pallet.type.Plastic;
                else if (type == Pallet.type.Aluminum)
                    predicate = p => p.Type == Pallet.type.Aluminum;

                if (!String.IsNullOrWhiteSpace(_mainView.PalletCode))
                    predicate = predicate == null ? p => p.PalletCode.Contains(_mainView.PalletCode) : PredicateBuilder.And<Pallet>(predicate, p => p.PalletCode.Contains(_mainView.PalletCode));

                _mainView.Pallets = await _palletRepository.ExecuteDataTable("proc_Pallets_SelectManagement",
                    new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.VarChar, Value = string.Empty },
                    new SqlParameter { ParameterName = "@StrName", SqlDbType = SqlDbType.VarChar, Value = string.Empty }
                    );
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task Search(string type, string strName)
        {
            try
            {
                _mainView.Pallets = await _palletRepository.ExecuteDataTable("proc_Pallets_SelectManagement",
                     new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.VarChar, Value = type },
                     new SqlParameter { ParameterName = "@StrName", SqlDbType = SqlDbType.VarChar, Value = strName }
                     );
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task GeneratePallet(string type, int usageTime, DateTime startDate, DateTime expiryDate, int qty)
        {
            try
            {
                await _palletRepository.ExecuteNonQuery("proc_Pallets_Generate",
                    new SqlParameter { ParameterName = "@PrepareCode", SqlDbType = SqlDbType.VarChar, Value = $"{type}{startDate:yy}" },
                    new SqlParameter { ParameterName = "@Type", SqlDbType = SqlDbType.VarChar, Value = type },
                    new SqlParameter { ParameterName = "@UsageTime", SqlDbType = SqlDbType.Int, Value = usageTime },
                    new SqlParameter { ParameterName = "@StartDate", SqlDbType = SqlDbType.Date, Value = startDate },
                    new SqlParameter { ParameterName = "@ExpiryDate", SqlDbType = SqlDbType.Date, Value = expiryDate },
                    new SqlParameter { ParameterName = "@Qty", SqlDbType = SqlDbType.Int, Value = qty },
                    new SqlParameter { ParameterName = "@CreateBy", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                    new SqlParameter { ParameterName = "@CreatedBySitemapID", SqlDbType = SqlDbType.Int, Value = _mainView.Sitemap.SitemapID },
                    new SqlParameter { ParameterName = "@UpdatedBy", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                    new SqlParameter { ParameterName = "@UpdatedBySitemapID", SqlDbType = SqlDbType.Int, Value = _mainView.Sitemap.SitemapID });

                await LoadMainData();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task PrintPallet(string palletCode)
        {
            try
            {
                await _palletRepository.ExecuteNonQuery("proc_Pallets_Print",
                    new SqlParameter { ParameterName = "@PalletCode", SqlDbType = SqlDbType.VarChar, Value = palletCode },
                    new SqlParameter { ParameterName = "@UpdatedBy", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID });

                //await LoadMainData();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public void DeletePallet(Pallet pallet)
        {
            try
            {
                _mainView.DeletedPallets.Add(pallet);
                //_mainView.Pallets.Remove(pallet);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Save product packing to database, 2 cases:
        ///     Case 1: insert new product
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var pallets = _mainView.Pallets;
            var deletedPallets = _mainView.DeletedPallets;

            bool isSuccess = false;
            try
            {
                //// foreach deleted product packing and delete it
                foreach (var deletedPallet in deletedPallets)
                {
                    await _palletRepository.Update(deletedPallet, new string[] { "IsDeleted" }, new object[] { deletedPallet.PalletCode });
                }
                await _palletRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadMainData();
                }
            }
        }
    }
}
