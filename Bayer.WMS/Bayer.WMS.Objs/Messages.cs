﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs
{
    public class Messages
    {
        public const string MultiProductPerPalletReason = "Lý do không được để trống";
        public const string MultiProductPerPalletPermissiontoChange = "Bạn không được cấp quyền để thay đổi thông số trên pallet";

        public const string Information_SaveSucessfully = "Dữ liệu đã được lưu thành công.";
        public const string Information_ConfirmSucessfully = "Dữ liệu đã được xác nhận thành công.";
        public const string Information_PrintSucessfully = "Phiếu đã được xuất excel thành công.";
        public const string Information_ExportSucessfully = "Dữ liệu đã được export thành công.";
        public const string Information_ImportSucessfully = "Dữ liệu đã được import thành công.";
        public const string Information_ImportAdditionalDOSucessfully = "Dữ liệu DO Bổ Sung đã được import thành công.";
        public const string Information_CorrectAdditionalDOSucessfully = "Cập nhật DO cho DO Bổ Sung đã được import thành công.";
        public const string Information_CorrectRelateTicketDOSucessfully = "Cập nhật DO Code cho các phiếu liên quan thành công.";
        public const string Information_ImportSucessfullyWithError = "Dữ liệu đã được import thành công. Nhưng các dòng excel {0} bị lỗi, vui lòng kiểm tra lại dữ liệu tại các dòng này trong file excel.";
        public const string Information_WarehouseManagerApproveImport = "Xác nhận cho xe nâng chất hàng thành công.";
        public const string Information_GenerateSuccessfully = "Dữ liệu đã được tạo thành công.";
        public const string Information_GenerateSuccessfullyWithError = "Dữ liệu đã được tạo thành công. Nhưng một số dòng không tạo được, vui lòng kiểm tra lại dữ liệu.";
        public const string Information_HoldSuccessfully = "Dữ liệu đã được giữ thành công.";
        public const string Information_HoldSuccessfullyWithError = "Dữ liệu đã được giữ thành công. Nhưng một số dòng không tạo được, vui lòng kiểm tra lại dữ liệu.";
        public const string Information_Packaging_ScanEmployeeStep = "Vui lòng quét mã nhân viên thực hiện";
        public const string Information_Packaging_ScanProdPlanStep = "Vui lòng quét mã lô sản phẩm";
        public const string Information_Packaging_ScanPalletStep = "Vui lòng quét mã QR pallet";
        public const string Information_Packaging_ScanCartonStep = "Vui lòng quét mã QR thùng";
        public const string Information_Packaging_ScanProductStep = "Vui lòng quét mã QR thành phẩm";
        public const string Information_Packaging_ScanQuantityForNoQRCode = "Vui lòng nhập số lượng đã đóng gói";
        public const string Information_Packaging_ScanDestroyPackageStep = "Vui lòng quét mã bao bì để hủy hoặc mã kết thúc để tiếp tục đóng gói";
        public const string Information_Packaging_ScanTakeProductSampleStep = "Vui lòng quét mã sản phẩm để lấy mẫu";
        public const string Information_Packaging_ScanTakeCartonSampleStep = "Vui lòng quét mã thùng để lấy mẫu";

        public const string Question_RecoverDeleted = "Dữ liệu với mã này đã bị xóa, bạn có muốn khôi phục lại không?";
        public const string Question_FinishPackaging = "Số lượng đóng gói theo KHSX đã hoàn thành. Bạn có muốn kết thúc không?";
        public const string Question_ContinuePackaging = "Số lượng đóng gói theo KHSX đã hoàn thành. Bạn có muốn tiếp tục đóng gói?";
        public const string Question_DestroyPackage = "Bạn có muốn thực hiện hủy bao bì dư hay không?";
        public const string Question_DestroyUnPackaged = "Bạn có chắc hủy bao bì này không? Nếu không, quét mã kết thúc hủy.";

        public const string Error_Common = "Có lỗi xảy ra, vui lòng gửi log lỗi cho nhà phát triển để kiểm tra.";
        public const string Error_Login = "Tên đăng nhập hoặc mật khẩu không đúng.";
        public const string Error_Permission = "Bạn không quyền thực hiện thao tác này, vui lòng liên hệ nhân viên có thẩm quyền.";
        public const string Error_ConcurrencyUpdate = "Dữ liệu đã bị cập nhật bởi người dùng khác. Thao tác cập nhật đã bị hủy và hiển thị dữ liệu mới nhất.";
        public const string Error_DeviceCodeExisted = "Mã thiết bị/phòng đã tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductCodeExisted = "Mã sản phẩm đã tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_EmployeeCodeNotExisted = "Mã nhân viên không tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductLotNotFinishPackaging = "Lô thành phẩm hiện tại chưa đóng gói hoàn tất, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductLotIsHold = "Lô thành phẩm đang bị giữ không thể thực hiện đóng gói, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductLotPackagingIncorrectLine = "Lô thành phẩm đóng gói không đúng line, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductLotNotYetGenBarcode = "Lô thành phẩm chưa tạo mã QR, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductCartonLabelIsPrintedOut = "Nhãn thùng của lô thành phẩm đã in hết, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductionPlanDateIsInvalid = "Ngày lô thành phẩm không hợp lệ, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductLotNotExisted = "Lô thành phẩm không tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProducNotExisted = "Sản phẩm không tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductPackingCartonNotExisted = "Quy cách đóng gói thùng của thành phẩm không tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_CartonBarcodeNotExisted = "Mã QR code thùng không tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductBarcodeNotExisted = "Mã QR thành phẩm không tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductBarcodeIsPacked = "Mã QR thành phẩm đã được đóng gói, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductBarcodeIsCancelledOrDestroyed = "Mã QR thành phẩm đã bị hủy.";
        public const string Error_ProductBarcodeIsTookSample = "Mã QR thành phẩm đã được lấy mẫu.";
        public const string Error_CartonBarcodeIsCancelledOrDestroyed = "Mã QR thùng đã bị hủy.";
        public const string Error_CartonBarcodeIsTookSample = "Mã QR thùng đã được lấy mẫu.";
        public const string Error_CartonBarcodeIsPackaging = "Mã QR thùng đã/đang được đóng gói.";
        public const string Error_ProductPackingTypeIsPallet = "Thành phẩm đóng gói theo Xô/Can/Bao lớn nên không cần quét mã QR thùng, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductLotIsPackaged = "Lô thành phẩm đã đóng gói hoàn tất, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_ProductionPlanModeInvalid = "Chế độ thực hiện không đúng, vui lòng kiểm tra lại.";
        public const string Error_ProductionPlanStillExistUnuseLabel = "Bạn chưa hủy hết các nhãn không sử dụng.";
        public const string Error_ProductLotTakeEnoughSample = "Lô thành phẩm đã lấy đủ mẫu.";
        public const string Error_ProductLotStillExistUnuseLabel = "Bạn bắt buộc phải hủy hết bao bì không sử dụng.";
        public const string Error_ScanEmployeeCodeFirst = "Vui lòng quét mã QR nhân viên trước.";
        public const string Error_ScanProductLotFirst = "Vui lòng quét mã QR lô thành phẩm trước.";
        public const string Error_ScanPalletBarcodeFirst = "Vui lòng quét mã QR Pallet trước.";
        public const string Error_ScanQuantityFirst = "Vui lòng nhập số lượng.";
        public const string Error_ScanCartonBarcodeFirst = "Vui lòng quét mã QR thùng trước.";
        public const string Error_ScanDuplicateBarcode = "Mã QR đã được scan trước đó.";
        public const string Error_DestroyLabelMode = "Hệ thống đang trong chế độ hủy bao bì, bạn vui lòng thoát khỏi chế độ này để tiếp tục đóng gói.";
        public const string Error_TakeSampleMode = "Hệ thống đang trong chế độ lấy mẫu, bạn vui lòng thoát khỏi chế độ này để tiếp tục đóng gói.";
        public const string Error_PalletTypeIsInvalid = "Loại pallet không hợp lệ với thiết bị/phòng trộn, vui lòng chọn pallet khác.";
        public const string Error_PalletIsFull = "Pallet đã chất đầy hàng, vui lòng chọn pallet khác.";
        public const string Error_BarcodeIsNotExist = "Mã QR không tồn tại trong hệ thống, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_CartonIsPacked = "Thùng đã đóng gói hoàn tát, vui lòng chọn thùng khác.";
        public const string Error_PalletPackagingHasNotYetFinished = "Việc đóng gói Pallet hiện tại chưa hoàn thành, vui lòng tiếp tục hoặc liên hệ nhân viên có thẩm quyền.";
        public const string Error_PalletPackagingHadFinished = "Việc đóng gói pallet hiện tại đã hoàn thành, vui lòng quét mã QR pallet tiếp theo.";
        public const string Error_CartonPackagingHasNotYetFinished = "Việc đóng gói thùng hiện tại chưa hoàn thành.";
        public const string Error_CartonPackagingHadFinished = "Việc đóng gói thùng hiện tại đã hoàn thành, vui lòng quét mã QR thùng tiếp theo.";
        public const string Error_UsernameIsEmpty = "Vui lòng nhập tên đăng nhập.";
        public const string Error_PasswordIsEmpty = "Vui lòng nhập mật khẩu.";
        public const string Error_NoteIsEmpty = "Vui lòng nhập ghi chú.";
        public const string Error_ImportFileNotSelected = "Vui lòng chọn file import.";
        public const string Error_ExportFolderNotSelected = "Vui lòng chọn thư mục export.";
        public const string Error_DeliveryOrderMustSameDate = "Dữ liệu DO phải chùng ngày giao hàng, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_CanNotConnectPrinter = "Không thể kết nối đến được máy in, vui lòng kiểm tra lại.";
        public const string Error_PlanDateMustGreaterThanToDate = "Ngày thực hiện kế hoạch phải lớn hơn ngày hiện tại.";
        public const string Error_COMConfigSetup = "Vui lòng thiết lập cổng COM cho máy tính trước khi thực hiện.";
        public const string Error_ProductionPlanStatusIsInvalid = "Trạng thái lô thành phẩm không đúng, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_BarcodeIsInvalid = "Mã QR không tồn tại, vui lòng kiểm tra lại dữ liệu.";
        public const string Error_CartonOnPalletOverSpec = "Số lượng carton đã vượt quá quy cách trên pallet.";

        public const string Validate_Match_Password = "Mật khẩu xác nhận không giống nhau.";
        public const string Validate_Match_PIN = "Mã PIN xác nhận không giống nhau.";
        
        public const string Validate_Policy_PasswordLength = "Mật khẩu phải dài hơn 8 ký tự.";
        public const string Validate_Policy_PINLength = "Mã PIN phải dài 4 ký tự.";
        public const string Validate_Policy_PasswordContainName = "Mất khẩu không được chứa họ tên nhân viên.";
        public const string Validate_Policy_PasswordNotComplicated = "Mật khẩu không đúng policy quy định.";
        public const string Validate_Policy_PINNotComplicated = "Mã PIN chỉ gồm 4 kí tự số.";
        public const string Validate_Policy_PasswordNotIn10LastChangeTimes = "Mật khẩu không được giống 10 lần gần nhất.";
        public const string Validate_Required_RoleName = "Tên vai trò là dữ liệu bắt buộc chọn.";
        public const string Validate_Required_Username = "Tên đăng nhập là dữ liệu bắt buộc chọn.";
        public const string Validate_Required_Password = "Mật khẩu là dữ liệu bắt buộc chọn.";
        public const string Validate_Required_FirstName = "Tên là dữ liệu bắt buộc chọn.";
        public const string Validate_Required_LastName = "Họ và chữ lót là dữ liệu bắt buộc chọn.";
        public const string Validate_Required_Product = "Sản phẩm là dữ liệu bắt buộc chọn.";
        public const string Validate_Required_ProductNotInserted = "Sản phẩm {0} không tìm thấy trên hệ thống.";
        public const string Validate_Required_ProductCode = "Mã sản phẩm là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_UOM = "Đvt là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_ProductPackingType = "Loại quy cách đóng gói là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_ProductPackingQuantity = "Quy cách đóng gói là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_ProductionLot = "Mã lô là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_Device = "Thiết bị là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_PONumber = "Số PO là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_Quantity = "Số lượng là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_PlanDate = "Ngày kế hoạch là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_MFG = "Ngày sản xuất là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_EXP = "Ngày hết hạn là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_PackageQuantity = "Số lượng theo bao bì là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_RequestQty = "Số lượng yêu cầu dữ liệu bắt buộc nhập.";
        public const string Validate_Required_PackageSize = "Kích cỡ là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_Sequence = "Thứ tự thực hiện là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_PalletType = "Loại pallet là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_CustomerCode = "Mã khách hàng là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_CustomerName = "Tên khách hàng là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_SupplierName = "Nhà cung cấp là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_DeviceCode = "Mã thiết bị/phòng đóng gói là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_ExportQty = "Vui lòng nhập số lượng cần xuất.";
        public const string Validate_Required_WarehouseCode = "Mã kho là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_ZoneCode = "Mã khu vực là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_LineCode = "Mã dãy là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_LineName = "Tên dãy là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_Warehouse = "Kho là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_Category = "Nhóm sản phẩm là dữ liệu bắt buộc nhập.";
        public const string Validate_Product_NotExist = "Không tìm thấy sản phẩm \"{0}\" trong hệ thống";
        public const string Validate_Range_PackingQuantity = "Quy cách đóng gói phải lớn hơn {0}.";
        public const string Validate_Range_Quantity = "Số lượng phải lớn hơn {0}.";
        public const string Validate_Range_PackageQuantity = "Số lượng theo bao bì phải lớn hơn {0}.";
        public const string Validate_Range_RequestQty = "Số lượng yêu cầu phải lớn hơn {0}.";
        public const string Validate_Range_TransferedQty = "Số lượng chuyển kho phải lớn hơn {0}.";
        public const string Validate_Range_PackageSize = "Kích cỡ phải lớn hơn {0}.";
        public const string Validate_Range_Sequence = "Thứ tự thực hiện phải lớn hơn {0}.";

        public const string Validate_ExcelFile_Large = "File excel quá lớn, vui lòng kiểm tra lại trước khi import hoặc chia nhỏ file để có thể import nhiều lần.";

        public const string Validate_Format_Email = "Email không đúng định dạng, vui lòng nhập đúng định dạng email.";

        public const string Validate_User_RoleEmpty = "Danh sách phân quyền chưa hợp lệ";

        public const string Validate_Required_MaterialManufacturingDate = "Ngày sản xuất là dữ liệu bắt buộc nhập.";
        public const string Validate_Required_MaterialExpiredDate = "Hạn sử dụng là dữ liệu bắt buộc nhập.";


        public const string Error_QuantityExportNotGreaterThanQuantityOrder = "Số lượng xuất lớn hơn số lượng đặt: ";
        public const string Success_CreateSplitNotesFull = "Phiếu chia hàng đã được tạo thành công: {0}";
        public const string Success_CreateSplitNotes = "Phiếu chia hàng đã được tạo thành công.";
        public const string Success_CreatePrepareNotes = "Phiếu soạn hàng đã được tạo thành công.";
        public const string Validate_Required_CompanyDoCode = "Số DO chỉ được tồn tại ở một Công ty.";
        public const string Validate_Default_DeliveryDate = "Ngày giao hàng không được nhỏ hơn ngày hôm nay.";
    }
}
