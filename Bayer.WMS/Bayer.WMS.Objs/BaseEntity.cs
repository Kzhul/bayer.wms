﻿using Bayer.WMS.Objs.Models;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Xml.Serialization;

namespace Bayer.WMS.Objs
{
    [XmlInclude(typeof(CartonBarcode))]
    [XmlInclude(typeof(Company))]
    [XmlInclude(typeof(Category))]
    [XmlInclude(typeof(DeliveryNoteDetail))]
    [XmlInclude(typeof(DeliveryNoteHeader))]
    [XmlInclude(typeof(DeliveryOrderDetail))]
    [XmlInclude(typeof(DeliveryOrderHeader))]
    [XmlInclude(typeof(Device))]
    [XmlInclude(typeof(PackagingError))]
    [XmlInclude(typeof(PackagingLog))]
    [XmlInclude(typeof(PackagingSample))]
    [XmlInclude(typeof(Pallet))]
    [XmlInclude(typeof(PalletStatus))]
    [XmlInclude(typeof(ProductBarcode))]
    [XmlInclude(typeof(ProductionPlanImportLog))]
    [XmlInclude(typeof(ProductionPlan))]
    [XmlInclude(typeof(ProductPacking))]
    [XmlInclude(typeof(Product))]
    [XmlInclude(typeof(Role))]
    [XmlInclude(typeof(RoleSitemap))]
    [XmlInclude(typeof(Sitemap))]
    [XmlInclude(typeof(SplitNoteDetail))]
    [XmlInclude(typeof(SplitNoteHeader))]
    [XmlInclude(typeof(UOM))]
    [XmlInclude(typeof(User))]
    [XmlInclude(typeof(UsersRole))]
    [XmlInclude(typeof(PrepareNoteDetail))]
    [XmlInclude(typeof(PrepareNoteHeader))]
    [XmlInclude(typeof(DeliveryTicketDetail))]
    [XmlInclude(typeof(DeliveryTicketHeader))]
    [XmlInclude(typeof(Warehouse))]
    [XmlInclude(typeof(Zone))]
    [XmlInclude(typeof(ZoneLine))]
    [XmlInclude(typeof(ZoneLocation))]
    [XmlInclude(typeof(ZoneCategory))]
    [XmlInclude(typeof(ZoneTemperature))]
    [XmlInclude(typeof(WarehouseVisualization))]
    [XmlInclude(typeof(RequestMaterial))]
    [XmlInclude(typeof(RequestMaterialLine))]
    [XmlInclude(typeof(RequestMaterialLineSplit))]
    [XmlInclude(typeof(StockReceivingDetail))]
    [XmlInclude(typeof(StockReceivingHeader))]
    [XmlInclude(typeof(StockReturnDetail))]
    [XmlInclude(typeof(StockReturnHeader))]
    public abstract class BaseEntity: INotifyPropertyChanged
    {
        public BaseEntity()
        {
            _state = EntityState.Unchanged;
        }

        protected int? _createdBy;

        public int? CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        protected int? _createdBySitemapID;

        public int? CreatedBySitemapID
        {
            get { return _createdBySitemapID; }
            set { _createdBySitemapID = value; }
        }

        protected DateTime? _createdDateTime;

        public DateTime? CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        protected int? _updatedBy;

        public int? UpdatedBy
        {
            get { return _updatedBy; }
            set { _updatedBy = value; }
        }

        protected int? _updatedBySitemapID;

        public int? UpdatedBySitemapID
        {
            get { return _updatedBySitemapID; }
            set { _updatedBySitemapID = value; }
        }

        protected DateTime? _updatedDateTime;

        public DateTime? UpdatedDateTime
        {
            get { return _updatedDateTime; }
            set { _updatedDateTime = value; }
        }

        protected byte[] _rowVersion;

        [Timestamp]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] RowVersion
        {
            get { return _rowVersion; }
            set { _rowVersion = value; }
        }

        protected EntityState _state;

        [NotMapped]
        public EntityState State
        {
            get { return _state; }
            set { _state = value; }
        }

        protected bool _isRecordAuditTrail;

        [NotMapped]
        public bool IsRecordAuditTrail
        {
            get { return _isRecordAuditTrail; }
            set { _isRecordAuditTrail = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            _state = EntityState.Modified;
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }
    }
}
