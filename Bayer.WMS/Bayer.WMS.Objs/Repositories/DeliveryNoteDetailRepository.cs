﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IDeliveryNoteDetailRepository : IBaseRepository, IBaseRepository<DeliveryNoteDetail>
    {
        Task<IList<DeliveryNoteDetail>> GetIncludeProductAsync(Expression<Func<DeliveryNoteDetail, bool>> predicate = null);
        Task<IList<DeliveryNoteDetail>> GetIncludeProductAndPalletAsync(Expression<Func<DeliveryNoteDetail, bool>> predicate = null);
    }

    public class DeliveryNoteDetailRepository : BaseRepository<BayerWMSContext, DeliveryNoteDetail>, IDeliveryNoteDetailRepository
    {
        public DeliveryNoteDetailRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<DeliveryNoteDetail>> GetIncludeProductAsync(Expression<Func<DeliveryNoteDetail, bool>> predicate = null)
        {
            var DeliveryNoteDetails = await _context.DeliveryNoteDetails
                                                .Where(predicate)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { DeliveryNoteDetail = pp, Product = p })
                                                .Select(p => new //DeliveryNoteDetail()
                                                {
                                                    DOCode = p.DeliveryNoteDetail.DOCode,
                                                    ExportCode = p.DeliveryNoteDetail.ExportCode,
                                                    ProductID = p.DeliveryNoteDetail.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,
                                                    Quantity = p.DeliveryNoteDetail.Quantity,
                                                    PackQuantity = p.DeliveryNoteDetail.PackQuantity,

                                                    Shipper1 = p.DeliveryNoteDetail.Shipper1,
                                                    Shipper2 = p.DeliveryNoteDetail.Shipper2,
                                                    Shipper3 = p.DeliveryNoteDetail.Shipper3,
                                                    Receiver1 = p.DeliveryNoteDetail.Receiver1,
                                                    Receiver2 = p.DeliveryNoteDetail.Receiver2,
                                                    Receiver3 = p.DeliveryNoteDetail.Receiver3,

                                                    PackType = p.DeliveryNoteDetail.PackType,
                                                    ProductQuantity = p.DeliveryNoteDetail.ProductQuantity,
                                                    BatchCode = p.DeliveryNoteDetail.BatchCode,
                                                    BatchCodeDistributor = p.DeliveryNoteDetail.BatchCodeDistributor,

                                                    ExportedQty = p.DeliveryNoteDetail.ExportedQty,
                                                    ConfirmQty = p.DeliveryNoteDetail.ConfirmQty,
                                                    ReceivedQty = p.DeliveryNoteDetail.ReceivedQty,
                                                    RequireReturnQty = p.DeliveryNoteDetail.RequireReturnQty,
                                                    ExportReturnedQty = p.DeliveryNoteDetail.ExportReturnedQty,
                                                    ReceiveReturnedQty = p.DeliveryNoteDetail.ReceiveReturnedQty,

                                                    Status = p.DeliveryNoteDetail.Status,
                                                    CreatedBy = p.DeliveryNoteDetail.CreatedBy,
                                                    CreatedDateTime = p.DeliveryNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.DeliveryNoteDetail.CreatedBy,
                                                    UpdatedDateTime = p.DeliveryNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.DeliveryNoteDetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();
            var listUser = await _context.Users.ToListAsync();
            var listDetail = DeliveryNoteDetails.Select(p => new DeliveryNoteDetail(p)).ToList();

            foreach (DeliveryNoteDetail item in listDetail)
            {
                if (item.Receiver1 > 0)
                {
                    var receiver1 = listUser.Where(a => a.UserID == item.Receiver1.Value).FirstOrDefault();
                    item.StrReceiver1 = receiver1 == null ? string.Empty : Utility.StringParse(receiver1.LastName) + " " + Utility.StringParse(receiver1.FirstName);
                }
                if (item.Receiver2 > 0)
                {
                    var receiver2 = listUser.Where(a => a.UserID == item.Receiver2.Value).FirstOrDefault();
                    item.StrReceiver2 = receiver2 == null ? string.Empty : Utility.StringParse(receiver2.LastName) + " " + Utility.StringParse(receiver2.FirstName);
                }
                if (item.Receiver3 > 0)
                {
                    var receiver3 = listUser.Where(a => a.UserID == item.Receiver3.Value).FirstOrDefault();
                    item.StrReceiver3 = receiver3 == null ? string.Empty : Utility.StringParse(receiver3.LastName) + " " + Utility.StringParse(receiver3.FirstName);
                }
            }

            return listDetail;
        }

        public virtual async Task<IList<DeliveryNoteDetail>> GetIncludeProductAndPalletAsync(Expression<Func<DeliveryNoteDetail, bool>> predicate = null)
        {
            var DeliveryNoteDetails = await _context.DeliveryNoteDetails
                                                .Where(predicate)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { DeliveryNoteDetail = pp, Product = p })
                                                .GroupJoin(_context.ProductPackings.Where(a => a.Type == ProductPacking.type.Carton),
                                                    pk => pk.Product.ProductID,
                                                    p => p.ProductID,
                                                    (srd, p) => new { DeliveryNoteDetail = srd.DeliveryNoteDetail, Product = srd.Product, ProductPacking = p.FirstOrDefault() })
                                                .Select(p => new //DeliveryNoteDetail()
                                                {
                                                    DOCode = p.DeliveryNoteDetail.DOCode,
                                                    ExportCode = p.DeliveryNoteDetail.ExportCode,
                                                    ProductID = p.DeliveryNoteDetail.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,
                                                    Quantity = p.DeliveryNoteDetail.Quantity,
                                                    PackQuantity = p.DeliveryNoteDetail.PackQuantity,

                                                    Shipper1 = p.DeliveryNoteDetail.Shipper1,
                                                    Shipper2 = p.DeliveryNoteDetail.Shipper2,
                                                    Shipper3 = p.DeliveryNoteDetail.Shipper3,
                                                    Receiver1 = p.DeliveryNoteDetail.Receiver1,
                                                    Receiver2 = p.DeliveryNoteDetail.Receiver2,
                                                    Receiver3 = p.DeliveryNoteDetail.Receiver3,

                                                    PackType = p.DeliveryNoteDetail.PackType,
                                                    PackSize = p.ProductPacking.Quantity,
                                                    ProductQuantity = p.DeliveryNoteDetail.ProductQuantity,
                                                    BatchCode = p.DeliveryNoteDetail.BatchCode,
                                                    BatchCodeDistributor = p.DeliveryNoteDetail.BatchCodeDistributor,

                                                    ExportedQty = p.DeliveryNoteDetail.ExportedQty,
                                                    ConfirmQty = p.DeliveryNoteDetail.ConfirmQty,
                                                    ReceivedQty = p.DeliveryNoteDetail.ReceivedQty,
                                                    RequireReturnQty = p.DeliveryNoteDetail.RequireReturnQty,
                                                    ExportReturnedQty = p.DeliveryNoteDetail.ExportReturnedQty,
                                                    ReceiveReturnedQty = p.DeliveryNoteDetail.ReceiveReturnedQty,

                                                    Status = p.DeliveryNoteDetail.Status,
                                                    CreatedBy = p.DeliveryNoteDetail.CreatedBy,
                                                    CreatedDateTime = p.DeliveryNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.DeliveryNoteDetail.CreatedBy,
                                                    UpdatedDateTime = p.DeliveryNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.DeliveryNoteDetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();
            var listUser = await _context.Users.ToListAsync();
            var listDetail = DeliveryNoteDetails.Select(p => new DeliveryNoteDetail(p)).ToList();

            bool bolPalletInfo = false;
            string DOCode = string.Empty;
            string ExportCode = string.Empty;
            var ListPalletCodes = new List<PalletInfo>();
            if (listDetail.Count > 0)
            {
                DOCode = listDetail[0].DOCode;
                ExportCode = listDetail[0].ExportCode;

                ListPalletCodes = await _context.Pallets
                   .Where(p => p.DOImportCode == DOCode && p.ReferenceNbr == ExportCode)
                   .Join(_context.PalletStatuses,
                   p => p.PalletCode,
                   ps => ps.PalletCode,
                   (p, ps) => new PalletInfo { pallet = p, palletStatus = ps })
                   .Distinct()
                   .OrderBy(a => new { a.pallet.PalletCode })
                   .ToListAsync();

                bolPalletInfo = true;
            }


            foreach (DeliveryNoteDetail item in listDetail)
            {
                if (item.Receiver1 > 0)
                {
                    var receiver1 = listUser.Where(a => a.UserID == item.Receiver1.Value).FirstOrDefault();
                    item.StrReceiver1 = receiver1 == null ? string.Empty : Utility.StringParse(receiver1.LastName) + " " + Utility.StringParse(receiver1.FirstName);
                }
                if (item.Receiver2 > 0)
                {
                    var receiver2 = listUser.Where(a => a.UserID == item.Receiver2.Value).FirstOrDefault();
                    item.StrReceiver2 = receiver2 == null ? string.Empty : Utility.StringParse(receiver2.LastName) + " " + Utility.StringParse(receiver2.FirstName);
                }
                if (item.Receiver3 > 0)
                {
                    var receiver3 = listUser.Where(a => a.UserID == item.Receiver3.Value).FirstOrDefault();
                    item.StrReceiver3 = receiver3 == null ? string.Empty : Utility.StringParse(receiver3.LastName) + " " + Utility.StringParse(receiver3.FirstName);
                }

                if (bolPalletInfo)
                {
                    var itemPalletCodes = ListPalletCodes.Where(a =>
                        a.palletStatus.ProductLot == item.BatchCode
                        && a.palletStatus.ProductID == item.ProductID
                    ).Select(a => a.palletStatus.PalletCode).Distinct().ToList();

                    item.PalletCodes = String.Join(", ", itemPalletCodes.ToArray());
                }
            }

            return listDetail;
        }
    }
}
