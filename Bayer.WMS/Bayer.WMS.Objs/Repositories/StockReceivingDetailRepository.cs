﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IStockReceivingDetailRepository : IBaseRepository, IBaseRepository<StockReceivingDetail>
    {
        Task<IList<StockReceivingDetail>> GetIncludeProductAsync(Expression<Func<StockReceivingDetail, bool>> predicate = null);
    }

    public class StockReceivingDetailRepository : BaseRepository<BayerWMSContext, StockReceivingDetail>, IStockReceivingDetailRepository
    {
        public StockReceivingDetailRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<StockReceivingDetail>> GetIncludeProductAsync(Expression<Func<StockReceivingDetail, bool>> predicate = null)
        {
            var details = await _context.StockReceivingDetails
                                .Where(predicate)
                                .Join(_context.Products,
                                    srd => srd.ProductID,
                                    p => p.ProductID,
                                    (srd, p) => new { StockReceivingDetail = srd, Product = p })
                                 .GroupJoin(_context.ProductPackings.Where(a => a.Type == ProductPacking.type.Pallet),
                                    pk => pk.StockReceivingDetail.ProductID,
                                    p => p.ProductID,
                                    (srd, p) => new {
                                        StockReceivingDetail = srd.StockReceivingDetail
                                    , Product = srd.Product
                                    , ProductPacking = p.FirstOrDefault() })
                                .Select(p => new
                                {
                                    StockReceivingCode = p.StockReceivingDetail.StockReceivingCode,
                                    LineNbr = p.StockReceivingDetail.LineNbr,
                                    CompanyName = p.StockReceivingDetail.CompanyName,
                                    ProductID = p.StockReceivingDetail.ProductID,
                                    ProductCode = p.Product.ProductCode,
                                    ProductDescription = p.Product.Description,
                                    BatchCode = p.StockReceivingDetail.BatchCode,
                                    BatchCodeDistributor = p.StockReceivingDetail.BatchCodeDistributor,
                                    ExpiryDate = p.StockReceivingDetail.ExpiryDate,
                                    POCode = p.StockReceivingDetail.POCode,
                                    POLine = p.StockReceivingDetail.POLine,
                                    UOM = p.StockReceivingDetail.UOM,
                                    PalletPackaging = p.ProductPacking.Quantity,
                                    QuantityPlanning = p.StockReceivingDetail.QuantityPlanning,
                                    CartonPlanning = p.StockReceivingDetail.CartonPlanning,
                                    PalletPlanning = p.StockReceivingDetail.PalletPlanning,
                                    QuantityReceived = p.StockReceivingDetail.QuantityReceived,
                                    CartonReceived = p.StockReceivingDetail.CartonReceived,
                                    PalletReceived = p.StockReceivingDetail.PalletReceived,
                                    PalletSetLocation = p.StockReceivingDetail.PalletSetLocation,
                                    DeliveryDate = p.StockReceivingDetail.DeliveryDate,
                                    DeliveryHour = p.StockReceivingDetail.DeliveryHour,
                                    Description = p.StockReceivingDetail.Description,
                                    Receiver = p.StockReceivingDetail.Receiver,
                                    Status = p.StockReceivingDetail.Status,
                                    CreatedBy = p.StockReceivingDetail.CreatedBy,
                                    CreatedDateTime = p.StockReceivingDetail.CreatedDateTime,
                                    CreatedBySitemapID = p.StockReceivingDetail.CreatedBySitemapID,
                                    UpdatedBy = p.StockReceivingDetail.CreatedBy,
                                    UpdatedDateTime = p.StockReceivingDetail.UpdatedDateTime,
                                    UpdatedBySitemapID = p.StockReceivingDetail.UpdatedBySitemapID,
                                    RowVersion = p.StockReceivingDetail.RowVersion,
                                    BatchCodePrintedNbr = p.StockReceivingDetail.BatchCodePrintedNbr,
                                    ManufacturingDate = p.StockReceivingDetail.ManufacturingDate,
                                })
                                .OrderBy(a => new { a.LineNbr })
                                .ToListAsync();

            return details.Select(p => new StockReceivingDetail(p)).ToList();
        }
    }
}
