﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IDeliveryOrderHeaderRepository : IBaseRepository, IBaseRepository<DeliveryOrderHeader>
    {
        Task<DeliveryOrderHeader> GetLastestDO();
        Task<IList<DeliveryOrderHeader>> GetLastestDOList(Expression<Func<DeliveryOrderHeader, bool>> predicate = null);
        Task<string> GetNextNBR();
    }

    public class DeliveryOrderHeaderRepository : BaseRepository<BayerWMSContext, DeliveryOrderHeader>, IDeliveryOrderHeaderRepository
    {
        public DeliveryOrderHeaderRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }

        public virtual async Task<IList<DeliveryOrderHeader>> GetLastestDOList(Expression<Func<DeliveryOrderHeader, bool>> predicate = null)
        {
            var model = await _context.DeliveryOrderHeaders.OrderByDescending(a=>a.DOImportCode).ToListAsync();

            return model.ToList();
        }

        public virtual async Task<DeliveryOrderHeader> GetLastestDO()
        {
            var model = await _context.DeliveryOrderHeaders.OrderByDescending(a => a.DOImportCode).FirstOrDefaultAsync();

            return model;
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.DeliveryOrderHeaders.Where(a=>a.CreatedDateTime >= DateTime.Today).OrderByDescending(a => a.DOImportCode).FirstOrDefaultAsync();
            if (model != null)
            {
                lastestNumber = model.DOImportCode;
                string a = lastestNumber.Substring(lastestNumber.Length - 2);
                int NextDONumber = Convert.ToInt32(a) + 1;
                lastestNumber = "LO" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }
            else
            {
                int NextDONumber = 1;
                lastestNumber = "LO" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }

            return lastestNumber;
        }
    }
}
