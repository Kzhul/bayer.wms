﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IPrepareNoteDetailRepository : IBaseRepository, IBaseRepository<PrepareNoteDetail>
    {
        Task<IList<PrepareNoteDetail>> GetIncludeProductAsync(Expression<Func<PrepareNoteDetail, bool>> predicate = null);
        Task<IList<PrepareNoteDetail>> GetIncludeProductAndPalletAsync(Expression<Func<PrepareNoteDetail, bool>> predicate = null);        
        Task<IList<PrepareNoteDetail>> GetByDOCodeAndPartnerCode(string doCode, string partnerCode);
        Task<IList<PrepareNoteDetail>> GetByDOCodeAndAnotherPartnerCode(string doCode, string partnerCode, string PrepareCode);
    }

    public class PrepareNoteDetailRepository : BaseRepository<BayerWMSContext, PrepareNoteDetail>, IPrepareNoteDetailRepository
    {
        public PrepareNoteDetailRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<PrepareNoteDetail>> GetIncludeProductAsync(Expression<Func<PrepareNoteDetail, bool>> predicate = null)
        {
            var PrepareNoteDetails = await _context.PrepareNoteDetails
                                                .Where(predicate)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { PrepareNoteDetail = pp, Product = p })
                                                .Select(p => new //PrepareNoteDetail()
                                                {
                                                    DOCode = p.PrepareNoteDetail.DOCode,
                                                    PrepareCode = p.PrepareNoteDetail.PrepareCode,
                                                    DeliveryCode = p.PrepareNoteDetail.DeliveryCode,
                                                    ProductID = p.PrepareNoteDetail.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,
                                                    BatchCode = p.PrepareNoteDetail.BatchCode,
                                                    BatchCodeDistributor = p.PrepareNoteDetail.BatchCodeDistributor,

                                                    Quantity = p.PrepareNoteDetail.Quantity,
                                                    PackQuantity = p.PrepareNoteDetail.PackQuantity,
                                                    PackType = p.PrepareNoteDetail.PackType,
                                                    PackSize = p.PrepareNoteDetail.PackSize,
                                                    PackWeight = p.PrepareNoteDetail.PackWeight,

                                                    ProductQuantity = p.PrepareNoteDetail.ProductQuantity,
                                                    ProductCase = p.PrepareNoteDetail.ProductCase,
                                                    ProductUnit = p.PrepareNoteDetail.ProductUnit,
                                                    ProductWeight = p.PrepareNoteDetail.ProductWeight,
                                                    UserPrepare = p.PrepareNoteDetail.UserPrepare,
                                                    UserVerify = p.PrepareNoteDetail.UserVerify,

                                                    PreparedQty = p.PrepareNoteDetail.PreparedQty,
                                                    DeliveredQty = p.PrepareNoteDetail.DeliveredQty,
                                                    RequireReturnQty = p.PrepareNoteDetail.RequireReturnQty,
                                                    ReturnedQty = p.PrepareNoteDetail.ReturnedQty,

                                                    Status = p.PrepareNoteDetail.Status,
                                                    CreatedBy = p.PrepareNoteDetail.CreatedBy,
                                                    CreatedBySitemapID = p.PrepareNoteDetail.CreatedBySitemapID,
                                                    CreatedDateTime = p.PrepareNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.PrepareNoteDetail.UpdatedBy,
                                                    UpdatedBySitemapID = p.PrepareNoteDetail.UpdatedBySitemapID,
                                                    UpdatedDateTime = p.PrepareNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.PrepareNoteDetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();

            var listUser = await _context.Users.ToListAsync();
            var listDetail = PrepareNoteDetails.Select(p => new PrepareNoteDetail(p)).ToList();

            foreach (PrepareNoteDetail item in listDetail)
            {
                if (item.UserPrepare > 0)
                {
                    var user = listUser.FirstOrDefault(a => a.UserID == item.UserPrepare.Value);
                    item.StrUserPrepare = user.LastName + " " + user.FirstName;
                }
            }

            return listDetail;
        }

        public virtual async Task<IList<PrepareNoteDetail>> GetIncludeProductAndPalletAsync(Expression<Func<PrepareNoteDetail, bool>> predicate = null)
        {
            var PrepareNoteDetails = await _context.PrepareNoteDetails
                                                .Where(predicate)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { PrepareNoteDetail = pp, Product = p })
                                                .Select(p => new //PrepareNoteDetail()
                                                {
                                                    DOCode = p.PrepareNoteDetail.DOCode,
                                                    PrepareCode = p.PrepareNoteDetail.PrepareCode,
                                                    DeliveryCode = p.PrepareNoteDetail.DeliveryCode,
                                                    ProductID = p.PrepareNoteDetail.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,
                                                    BatchCode = p.PrepareNoteDetail.BatchCode,
                                                    BatchCodeDistributor = p.PrepareNoteDetail.BatchCodeDistributor,

                                                    Quantity = p.PrepareNoteDetail.Quantity,
                                                    PackQuantity = p.PrepareNoteDetail.PackQuantity,
                                                    PackType = p.PrepareNoteDetail.PackType,
                                                    PackSize = p.PrepareNoteDetail.PackSize,
                                                    PackWeight = p.PrepareNoteDetail.PackWeight,

                                                    ProductQuantity = p.PrepareNoteDetail.ProductQuantity,
                                                    ProductCase = p.PrepareNoteDetail.ProductCase,
                                                    ProductUnit = p.PrepareNoteDetail.ProductUnit,
                                                    ProductWeight = p.PrepareNoteDetail.ProductWeight,
                                                    UserPrepare = p.PrepareNoteDetail.UserPrepare,
                                                    UserVerify = p.PrepareNoteDetail.UserVerify,

                                                    PreparedQty = p.PrepareNoteDetail.PreparedQty,
                                                    DeliveredQty = p.PrepareNoteDetail.DeliveredQty,
                                                    RequireReturnQty = p.PrepareNoteDetail.RequireReturnQty,
                                                    ReturnedQty = p.PrepareNoteDetail.ReturnedQty,

                                                    Status = p.PrepareNoteDetail.Status,
                                                    CreatedBy = p.PrepareNoteDetail.CreatedBy,
                                                    CreatedBySitemapID = p.PrepareNoteDetail.CreatedBySitemapID,
                                                    CreatedDateTime = p.PrepareNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.PrepareNoteDetail.UpdatedBy,
                                                    UpdatedBySitemapID = p.PrepareNoteDetail.UpdatedBySitemapID,
                                                    UpdatedDateTime = p.PrepareNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.PrepareNoteDetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();

            var listUser = await _context.Users.ToListAsync();
            var listDetail = PrepareNoteDetails.Select(p => new PrepareNoteDetail(p)).ToList();

            bool bolPalletInfo = false;
            string DOCode = string.Empty;
            string TicketCode = string.Empty;
            var ListPalletCodes = new List<PalletInfo>();
            if (listDetail.Count > 0)
            {
                DOCode = listDetail[0].DOCode;
                TicketCode = listDetail[0].PrepareCode;

                ListPalletCodes = await _context.Pallets
                   .Where(p => p.DOImportCode == DOCode && p.ReferenceNbr == TicketCode)
                   .Join(_context.PalletStatuses,
                   p => p.PalletCode,
                   ps => ps.PalletCode,
                   (p, ps) => new PalletInfo { pallet = p, palletStatus = ps })
                   .Distinct()
                   .OrderBy(a => new { a.pallet.PalletCode })
                   .ToListAsync();

                bolPalletInfo = true;
            }

            foreach (PrepareNoteDetail item in listDetail)
            {
                if (item.UserPrepare > 0)
                {
                    var user = listUser.FirstOrDefault(a => a.UserID == item.UserPrepare.Value);
                    item.StrUserPrepare = user.LastName + " " + user.FirstName;
                }

                if (bolPalletInfo)
                {
                    var itemPalletCodes = ListPalletCodes.Where(a =>
                        a.palletStatus.ProductLot == item.BatchCode
                        && a.palletStatus.ProductID == item.ProductID
                    ).Select(a => a.palletStatus.PalletCode).Distinct().ToList();

                    item.PalletCodes = String.Join(", ", itemPalletCodes.ToArray());
                }
            }

            return listDetail;
        }

        public virtual async Task<IList<PrepareNoteDetail>> GetByDOCodeAndPartnerCode(string doCode, string partnerCode)
        {
            var PrepareNoteDetails = await _context.PrepareNoteDetails
                                                .Where(a=>a.DOCode == doCode)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { PrepareNoteDetail = pp, Product = p })
                                                .Join(_context.PrepareNoteHeaders,
                                                    pp => new { pp.PrepareNoteDetail.DOCode, pp.PrepareNoteDetail.PrepareCode },
                                                    p => new { DOCode = p.DOImportCode , p.PrepareCode},
                                                    (pp, p) => new { pp = pp, h = p })
                                                .Where(a => a.h.CompanyCode == partnerCode)
                                                .Select(p => new //PrepareNoteDetail()
                                                {
                                                    DOCode = p.pp.PrepareNoteDetail.DOCode,
                                                    PrepareCode = p.pp.PrepareNoteDetail.PrepareCode,
                                                    DeliveryCode = p.pp.PrepareNoteDetail.DeliveryCode,
                                                    ProductID = p.pp.PrepareNoteDetail.ProductID,
                                                    ProductCode = p.pp.Product.ProductCode,
                                                    ProductDescription = p.pp.Product.Description,
                                                    BatchCode = p.pp.PrepareNoteDetail.BatchCode,
                                                    BatchCodeDistributor = p.pp.PrepareNoteDetail.BatchCodeDistributor,

                                                    Quantity = p.pp.PrepareNoteDetail.Quantity,
                                                    PackQuantity = p.pp.PrepareNoteDetail.PackQuantity,
                                                    PackType = p.pp.PrepareNoteDetail.PackType,
                                                    PackSize = p.pp.PrepareNoteDetail.PackSize,
                                                    PackWeight = p.pp.PrepareNoteDetail.PackWeight,

                                                    ProductQuantity = p.pp.PrepareNoteDetail.ProductQuantity,
                                                    ProductCase = p.pp.PrepareNoteDetail.ProductCase,
                                                    ProductUnit = p.pp.PrepareNoteDetail.ProductUnit,
                                                    ProductWeight = p.pp.PrepareNoteDetail.ProductWeight,
                                                    UserPrepare = p.pp.PrepareNoteDetail.UserPrepare,
                                                    UserVerify = p.pp.PrepareNoteDetail.UserVerify,

                                                    PreparedQty = p.pp.PrepareNoteDetail.PreparedQty,
                                                    DeliveredQty = p.pp.PrepareNoteDetail.DeliveredQty,
                                                    RequireReturnQty = p.pp.PrepareNoteDetail.RequireReturnQty,
                                                    ReturnedQty = p.pp.PrepareNoteDetail.ReturnedQty,

                                                    Status = p.pp.PrepareNoteDetail.Status,
                                                    CreatedBy = p.pp.PrepareNoteDetail.CreatedBy,
                                                    CreatedBySitemapID = p.pp.PrepareNoteDetail.CreatedBySitemapID,
                                                    CreatedDateTime = p.pp.PrepareNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.pp.PrepareNoteDetail.UpdatedBy,
                                                    UpdatedBySitemapID = p.pp.PrepareNoteDetail.UpdatedBySitemapID,
                                                    UpdatedDateTime = p.pp.PrepareNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.pp.PrepareNoteDetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();

            var listUser = await _context.Users.ToListAsync();
            var listDetail = PrepareNoteDetails.Select(p => new PrepareNoteDetail(p)).ToList();

            foreach (PrepareNoteDetail item in listDetail)
            {
                if (item.UserPrepare > 0)
                {
                    var user = listUser.FirstOrDefault(a => a.UserID == item.UserPrepare.Value);
                    item.StrUserPrepare = user.LastName + " " + user.FirstName;
                }
            }

            return listDetail;
        }


        public virtual async Task<IList<PrepareNoteDetail>> GetByDOCodeAndAnotherPartnerCode(string doCode, string partnerCode, string PrepareCode)
        {
            var PrepareNoteDetails = await _context.PrepareNoteDetails
                                                .Where(a => a.DOCode == doCode)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { PrepareNoteDetail = pp, Product = p })
                                                .Join(_context.PrepareNoteHeaders,
                                                    pp => new { pp.PrepareNoteDetail.DOCode, pp.PrepareNoteDetail.PrepareCode },
                                                    p => new { DOCode = p.DOImportCode, p.PrepareCode },
                                                    (pp, p) => new { pp = pp, h = p })
                                                .Where(a => a.h.CompanyCode == partnerCode && a.h.PrepareCode != PrepareCode)
                                                .Select(p => new //PrepareNoteDetail()
                                                {
                                                    DOCode = p.pp.PrepareNoteDetail.DOCode,
                                                    PrepareCode = p.pp.PrepareNoteDetail.PrepareCode,
                                                    DeliveryCode = p.pp.PrepareNoteDetail.DeliveryCode,
                                                    ProductID = p.pp.PrepareNoteDetail.ProductID,
                                                    ProductCode = p.pp.Product.ProductCode,
                                                    ProductDescription = p.pp.Product.Description,
                                                    BatchCode = p.pp.PrepareNoteDetail.BatchCode,
                                                    BatchCodeDistributor = p.pp.PrepareNoteDetail.BatchCodeDistributor,

                                                    Quantity = p.pp.PrepareNoteDetail.Quantity,
                                                    PackQuantity = p.pp.PrepareNoteDetail.PackQuantity,
                                                    PackType = p.pp.PrepareNoteDetail.PackType,
                                                    PackSize = p.pp.PrepareNoteDetail.PackSize,
                                                    PackWeight = p.pp.PrepareNoteDetail.PackWeight,

                                                    ProductQuantity = p.pp.PrepareNoteDetail.ProductQuantity,
                                                    ProductCase = p.pp.PrepareNoteDetail.ProductCase,
                                                    ProductUnit = p.pp.PrepareNoteDetail.ProductUnit,
                                                    ProductWeight = p.pp.PrepareNoteDetail.ProductWeight,
                                                    UserPrepare = p.pp.PrepareNoteDetail.UserPrepare,
                                                    UserVerify = p.pp.PrepareNoteDetail.UserVerify,

                                                    PreparedQty = p.pp.PrepareNoteDetail.PreparedQty,
                                                    DeliveredQty = p.pp.PrepareNoteDetail.DeliveredQty,
                                                    RequireReturnQty = p.pp.PrepareNoteDetail.RequireReturnQty,
                                                    ReturnedQty = p.pp.PrepareNoteDetail.ReturnedQty,

                                                    Status = p.pp.PrepareNoteDetail.Status,
                                                    CreatedBy = p.pp.PrepareNoteDetail.CreatedBy,
                                                    CreatedBySitemapID = p.pp.PrepareNoteDetail.CreatedBySitemapID,
                                                    CreatedDateTime = p.pp.PrepareNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.pp.PrepareNoteDetail.UpdatedBy,
                                                    UpdatedBySitemapID = p.pp.PrepareNoteDetail.UpdatedBySitemapID,
                                                    UpdatedDateTime = p.pp.PrepareNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.pp.PrepareNoteDetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();

            var listUser = await _context.Users.ToListAsync();
            var listDetail = PrepareNoteDetails.Select(p => new PrepareNoteDetail(p)).ToList();

            foreach (PrepareNoteDetail item in listDetail)
            {
                if (item.UserPrepare > 0)
                {
                    var user = listUser.FirstOrDefault(a => a.UserID == item.UserPrepare.Value);
                    item.StrUserPrepare = user.LastName + " " + user.FirstName;
                }
            }

            return listDetail;
        }
    }
}