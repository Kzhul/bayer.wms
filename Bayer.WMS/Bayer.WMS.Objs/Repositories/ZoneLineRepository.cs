﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IZoneLineRepository : IBaseRepository, IBaseRepository<ZoneLine>
    {
        Task<IList<ZoneLine>> GetByWarehouseID(int warehouseID);
    }

    public class ZoneLineRepository : BaseRepository<BayerWMSContext, ZoneLine>, IZoneLineRepository
    {
        public ZoneLineRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<ZoneLine>> GetByWarehouseID(int warehouseID)
        {
            var zoneLocations = await _context.Zones
                                        .Where(p => p.WarehouseID == warehouseID && !p.IsDeleted)
                                        .Join(_context.ZoneLines,
                                            z => z.ZoneCode,
                                            zl => zl.ZoneCode,
                                            (z, zl) => new { Zone = z, ZoneLine = zl })
                                        .Select(p => new
                                        {
                                            ZoneCode = p.ZoneLine.ZoneCode,
                                            LineCode = p.ZoneLine.LineCode,
                                            LineName = p.ZoneLine.LineName,
                                            Type = p.ZoneLine.Type,
                                            Length = p.ZoneLine.Length,
                                            Level = p.ZoneLine.Level,
                                            Order = p.ZoneLine.Order,
                                            Description = p.ZoneLine.Description,
                                            Status = p.ZoneLine.Status,
                                            IsDeleted = p.Zone.IsDeleted,
                                            CreatedBy = p.Zone.CreatedBy,
                                            CreatedBySitemapID = p.Zone.CreatedBySitemapID,
                                            CreatedDateTime = p.Zone.CreatedDateTime,
                                            UpdatedBy = p.Zone.CreatedBy,
                                            UpdatedBySitemapID = p.Zone.UpdatedBySitemapID,
                                            UpdatedDateTime = p.Zone.UpdatedDateTime,
                                            RowVersion = p.Zone.RowVersion
                                        }).ToListAsync();

            return zoneLocations.Select(p => new ZoneLine(p)).ToList();
        }
    }
}
