﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IDeliveryTicketHeaderRepository : IBaseRepository, IBaseRepository<DeliveryTicketHeader>
    {
        Task<DeliveryTicketHeader> GetLastest();
        Task<string> GetNextNBR();
    }

    public class DeliveryTicketHeaderRepository : BaseRepository<BayerWMSContext, DeliveryTicketHeader>, IDeliveryTicketHeaderRepository
    {
        public DeliveryTicketHeaderRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<DeliveryTicketHeader> GetLastest()
        {
            var model = await _context.DeliveryTicketHeaders.OrderByDescending(a => a.DeliveryTicketCode).FirstOrDefaultAsync();

            return model;
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.DeliveryTicketHeaders.Where(a => a.CreatedDateTime >= DateTime.Today).OrderByDescending(a => a.DeliveryTicketCode).FirstOrDefaultAsync();
            int nextDONumber = 1;
            if (model != null)
            {
                nextDONumber = int.Parse(model.DeliveryTicketCode.Substring(model.DeliveryTicketCode.Length - 2)) + 1;
            }

            lastestNumber = $"GH{DateTime.Today:yyMMdd}{nextDONumber:00}";

            return lastestNumber;
        }
    }
}
