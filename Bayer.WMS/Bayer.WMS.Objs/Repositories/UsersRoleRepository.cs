﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IUsersRoleRepository : IBaseRepository, IBaseRepository<UsersRole>
    {
        Task<IList<UsersRole>> GetWithAllRolesAsync(Expression<Func<UsersRole, bool>> predicate = null);
    }

    public class UsersRoleRepository : BaseRepository<BayerWMSContext, UsersRole>, IUsersRoleRepository
    {
        public UsersRoleRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<UsersRole>> GetWithAllRolesAsync(Expression<Func<UsersRole, bool>> predicate = null)
        {
            var usersRoles = await _context.Roles
                                        .GroupJoin(_context.UsersRoles.Where(predicate),
                                            r => r.RoleID,
                                            ur => ur.RoleID,
                                            (r, ur) => new { Role = r, UsersRole = ur.FirstOrDefault() })
                                        .Select(p => new
                                        {
                                            UserID = p.UsersRole == null ? 0 : p.UsersRole.UserID,
                                            RoleID = p.Role.RoleID,
                                            RoleName = p.Role.RoleName,
                                            RoleDescription = p.Role.Description,
                                            Checked = p.UsersRole != null,
                                            CreatedBy = p.UsersRole != null ? p.UsersRole.CreatedBy : null,
                                            CreatedBySitemapID = p.UsersRole != null ? p.UsersRole.CreatedBySitemapID : null,
                                            CreatedDateTime = p.UsersRole != null ? p.UsersRole.CreatedDateTime : null,
                                            UpdatedBy = p.UsersRole != null ? p.UsersRole.UpdatedBy : null,
                                            UpdatedBySitemapID = p.UsersRole != null ? p.UsersRole.UpdatedBySitemapID : null,
                                            UpdatedDateTime = p.UsersRole != null ? p.UsersRole.UpdatedDateTime : null,
                                            RowVersion = p.UsersRole != null ? p.UsersRole.RowVersion : null,

                                        }).ToListAsync();

            return usersRoles.Select(p => new UsersRole(p)).OrderByDescending(p => p.Checked).ThenBy(p => p.RoleDescription).ToList();
        }
    }
}
