﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IPrepareNoteHeaderRepository : IBaseRepository, IBaseRepository<PrepareNoteHeader>
    {
        Task<PrepareNoteHeader> GetLastest();
        Task<string> GetNextNBR();
        Task<List<PrepareNoteHeader>> GetAvailable();
        Task<List<PrepareForDeliveryNote>> GetAvailableByDeliveryDateCompanyCode();        
    }

    public class PrepareNoteHeaderRepository : BaseRepository<BayerWMSContext, PrepareNoteHeader>, IPrepareNoteHeaderRepository
    {
        public PrepareNoteHeaderRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<PrepareNoteHeader> GetLastest()
        {
            var model = await _context.PrepareNoteHeaders.OrderByDescending(a => a.PrepareCode).FirstOrDefaultAsync();

            return model;
        }

        public virtual async Task<List<PrepareNoteHeader>> GetAvailable()
        {
            var model = await(
                        from pp in _context.PrepareNoteHeaders
                        join p in _context.DeliveryTicketDetails
                        on pp.PrepareCode equals p.PrepareCode
                        into c
                        from x in c.DefaultIfEmpty()
                        where pp.IsDeleted == false && x == null
                        select new {
                            PrepareCode = pp.PrepareCode,
                            PrepareDate = pp.PrepareDate,
                            DOImportCode = pp.DOImportCode,
                            CompanyCode = pp.CompanyCode,
                            CompanyName = pp.CompanyName,
                            Province = pp.Province,
                            Description = pp.Description,
                            DeliveryDate = pp.DeliveryDate,
                            IsDeleted = pp.IsDeleted,
                            Status = pp.Status,
                            UserCreateFullName = pp.UserCreateFullName,
                            UserWareHouse = pp.UserWareHouse,
                            UserReview = pp.UserReview,

                            CreatedBy = pp.CreatedBy,
                            CreatedBySitemapID = pp.CreatedBySitemapID,
                            CreatedDateTime = pp.CreatedDateTime,
                            UpdatedBy = pp.UpdatedBy,
                            UpdatedBySitemapID = pp.UpdatedBySitemapID,
                            UpdatedDateTime = pp.UpdatedDateTime,
                            RowVersion = pp.RowVersion
                        }).ToListAsync();
            return model.Select(p => new PrepareNoteHeader(p)).Distinct().ToList();
        }


        public virtual async Task<List<PrepareForDeliveryNote>> GetAvailableByDeliveryDateCompanyCode()
        {
            var model = await (
                        from pp in _context.PrepareNoteHeaders
                        join p in _context.DeliveryTicketDetails
                        on pp.PrepareCode equals p.PrepareCode
                        into c
                        from x in c.DefaultIfEmpty()
                        where pp.IsDeleted == false && x == null
                        select new
                        {
                            CompanyCode = pp.CompanyCode,
                            CompanyName = pp.CompanyName,
                            Province = string.Empty,//pp.Province,
                            DeliveryDate = pp.DeliveryDate,
                        }).Distinct().ToListAsync();
            return model.Select(p => new PrepareForDeliveryNote(p)).Distinct().ToList();
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.PrepareNoteHeaders.Where(a => a.CreatedDateTime >= DateTime.Today).OrderByDescending(a => a.PrepareCode).FirstOrDefaultAsync();
            int nextDONumber = 1;
            if (model != null)
            {
                nextDONumber = int.Parse(model.PrepareCode.Substring(model.PrepareCode.Length - 2)) + 1;
            }

            lastestNumber = $"SH{DateTime.Today:yyMMdd}{nextDONumber:00}";

            return lastestNumber;
        }
    }
}
