﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IPackagingErrorRepository : IBaseRepository, IBaseRepository<PackagingError>
    {
    }

    public class PackagingErrorRepository : BaseRepository<BayerWMSContext, PackagingError>, IPackagingErrorRepository
    {
        public PackagingErrorRepository(IBayerWMSContext context)
            : base(context)
        {

        }
    }
}
