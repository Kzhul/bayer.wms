﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IStockReturnDetailRepository : IBaseRepository, IBaseRepository<StockReturnDetail>
    {
        Task<IList<StockReturnDetail>> GetIncludeProductAsync(Expression<Func<StockReturnDetail, bool>> predicate = null);
    }

    public class StockReturnDetailRepository : BaseRepository<BayerWMSContext, StockReturnDetail>, IStockReturnDetailRepository
    {
        public StockReturnDetailRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<StockReturnDetail>> GetIncludeProductAsync(Expression<Func<StockReturnDetail, bool>> predicate = null)
        {
            var details = await _context.StockReturnDetails
                                .Where(predicate)
                                .Join(_context.Products,
                                    srd => srd.ProductID,
                                    p => p.ProductID,
                                    (srd, p) => new { StockReturnDetail = srd, Product = p })
                                .GroupJoin(_context.ProductPackings.Where(a=>a.Type != ProductPacking.type.Pallet),
                                    pk => pk.StockReturnDetail.ProductID,
                                    p => p.ProductID,                                    
                                    (srd, p) => new { StockReturnDetail = srd.StockReturnDetail, Product = srd.Product, ProductPacking = p.FirstOrDefault() })                                
                                .Select(p => new
                                {
                                    StockReturnCode = p.StockReturnDetail.StockReturnCode,
                                    LineNbr = p.StockReturnDetail.LineNbr,
                                    ProductID = p.StockReturnDetail.ProductID,
                                    ProductCode = p.Product.ProductCode,
                                    ProductDescription = p.Product.Description,
                                    BatchCode = p.StockReturnDetail.BatchCode,

                                    ReturnQty = p.StockReturnDetail.ReturnQty,
                                    PalletCode = p.StockReturnDetail.PalletCode,
                                    ReturnedFrom = p.StockReturnDetail.ReturnedFrom,
                                    ReceivedBy = p.StockReturnDetail.ReceivedBy,
                                    ReceivedQty = p.StockReturnDetail.ReceivedQty,
                                    ReceivedDate = p.StockReturnDetail.ReceivedDate,
                                    QA_QCCheck = p.StockReturnDetail.QA_QCCheck,
                                    Reason = p.StockReturnDetail.Reason,
                                    Description = p.StockReturnDetail.Description,

                                    UOM = p.Product.UOM,
                                    PackageSize = p.ProductPacking.Quantity,

                                    Status = p.StockReturnDetail.Status,
                                    CreatedBy = p.StockReturnDetail.CreatedBy,
                                    CreatedDateTime = p.StockReturnDetail.CreatedDateTime,
                                    CreatedBySitemapID = p.StockReturnDetail.CreatedBySitemapID,
                                    UpdatedBy = p.StockReturnDetail.CreatedBy,
                                    UpdatedDateTime = p.StockReturnDetail.UpdatedDateTime,
                                    UpdatedBySitemapID = p.StockReturnDetail.UpdatedBySitemapID,
                                    RowVersion = p.StockReturnDetail.RowVersion,
                                })
                                .OrderBy(a => new { a.LineNbr })
                                .ToListAsync();
            var listUser = await _context.Users.ToListAsync();
            var listDetail = details.Select(p => new StockReturnDetail(p)).ToList();

            foreach (StockReturnDetail item in listDetail)
            {
                if (item.ReceivedBy > 0)
                {
                    var receiver1 = listUser.Where(a => a.UserID == item.ReceivedBy).FirstOrDefault();
                    item.StrReceivedBy = receiver1 == null ? string.Empty : Utility.StringParse(receiver1.LastName) + " " + Utility.StringParse(receiver1.FirstName);
                }
            }

            return listDetail;
        }
    }
}
