﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IReturnDODetailRepository : IBaseRepository, IBaseRepository<ReturnDODetail>
    {
        Task<IList<ReturnDODetail>> GetIncludeProductAsync(Expression<Func<ReturnDODetail, bool>> predicate = null);

        Task<IList<ReturnDODetail>> LoadByDeliveryCode(string DeliveryCode);
    }

    public class ReturnDODetailRepository : BaseRepository<BayerWMSContext, ReturnDODetail>, IReturnDODetailRepository
    {
        public ReturnDODetailRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }

        public virtual async Task<IList<ReturnDODetail>> GetIncludeProductAsync(Expression<Func<ReturnDODetail, bool>> predicate = null)
        {
            var ReturnDODetails = await _context.ReturnDODetails
                                                .Where(predicate)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { ReturnDODetail = pp, Product = p })
                                                .Select(p => new //ReturnDODetail()
                                                {
                                                    ReturnDOCode = p.ReturnDODetail.ReturnDOCode,
                                                    DOImportCode = p.ReturnDODetail.DOImportCode,
                                                    LineNbr = p.ReturnDODetail.LineNbr,
                                                    Delivery = p.ReturnDODetail.Delivery,
                                                    ProductID = p.ReturnDODetail.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,
                                                    Quantity = p.ReturnDODetail.Quantity,
                                                    BatchCode = p.ReturnDODetail.BatchCode,
                                                    BatchCodeDistributor = p.ReturnDODetail.BatchCodeDistributor,
                                                    CompanyCode = p.ReturnDODetail.CompanyCode,
                                                    CompanyName = p.ReturnDODetail.CompanyName,
                                                    Province = p.ReturnDODetail.Province,
                                                    DeliveryDate = p.ReturnDODetail.DeliveryDate,
                                                    RequestReturnQty = p.ReturnDODetail.RequestReturnQty,
                                                    Description = p.ReturnDODetail.Description,
                                                    Status = p.ReturnDODetail.Status,
                                                    CreatedBy = p.ReturnDODetail.CreatedBy,
                                                    CreatedDateTime = p.ReturnDODetail.CreatedDateTime,
                                                    UpdatedBy = p.ReturnDODetail.CreatedBy,
                                                    UpdatedDateTime = p.ReturnDODetail.UpdatedDateTime,
                                                    RowVersion = p.ReturnDODetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();
            var listUser = await _context.Users.ToListAsync();
            var listDetail = ReturnDODetails.Select(p => new ReturnDODetail(p)).ToList();
            return listDetail;
        }

        public virtual async Task<IList<ReturnDODetail>> LoadByDeliveryCode(string DeliveryCode)
        {
            var ReturnDODetails = await _context.DeliveryOrderDetails                                                
                                                .Where(a=>a.Delivery == DeliveryCode)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { ReturnDODetail = pp, Product = p })
                                                .Select(p => new //ReturnDODetail()
                                                {
                                                    ReturnDOCode = string.Empty,
                                                    DOImportCode = p.ReturnDODetail.DOImportCode,
                                                    LineNbr = p.ReturnDODetail.LineNbr,
                                                    Delivery = p.ReturnDODetail.Delivery,
                                                    ProductID = p.ReturnDODetail.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,
                                                    Quantity = p.ReturnDODetail.Quantity,
                                                    BatchCode = p.ReturnDODetail.BatchCode,
                                                    BatchCodeDistributor = p.ReturnDODetail.BatchCodeDistributor,
                                                    CompanyCode = p.ReturnDODetail.CompanyCode,
                                                    CompanyName = p.ReturnDODetail.CompanyName,
                                                    Province = p.ReturnDODetail.ProvinceShipTo,
                                                    DeliveryDate = p.ReturnDODetail.DeliveryDate,
                                                    RequestReturnQty = 0,
                                                    Description = string.Empty,
                                                    Status = p.ReturnDODetail.Status,
                                                    CreatedBy = p.ReturnDODetail.CreatedBy,
                                                    CreatedDateTime = p.ReturnDODetail.CreatedDateTime,
                                                    UpdatedBy = p.ReturnDODetail.CreatedBy,
                                                    UpdatedDateTime = p.ReturnDODetail.UpdatedDateTime,
                                                    RowVersion = p.ReturnDODetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();
            var listDetail = ReturnDODetails.Select(p => new ReturnDODetail(p)).ToList();
            return listDetail;
        }
    }
}
