﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface ICategoryRepository : IBaseRepository, IBaseRepository<Category>
    {
       
    }

    public class CategoryRepository : BaseRepository<BayerWMSContext, Category>, ICategoryRepository
    {
        public CategoryRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }
    }
}
