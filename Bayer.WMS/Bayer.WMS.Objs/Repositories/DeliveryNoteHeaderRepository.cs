﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IDeliveryNoteHeaderRepository : IBaseRepository, IBaseRepository<DeliveryNoteHeader>
    {
        Task<DeliveryNoteHeader> GetLastest();
        Task<string> GetNextNBR();
    }

    public class DeliveryNoteHeaderRepository : BaseRepository<BayerWMSContext, DeliveryNoteHeader>, IDeliveryNoteHeaderRepository
    {
        public DeliveryNoteHeaderRepository(IBayerWMSContext context)
            : base(context)
        {

        }
        public virtual async Task<DeliveryNoteHeader> GetLastest()
        {
            var model = await _context.DeliveryNoteHeaders.OrderByDescending(a => a.ExportCode).FirstOrDefaultAsync();
            return model;
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.DeliveryNoteHeaders.Where(a => a.CreatedDateTime >= DateTime.Today.Date).OrderByDescending(a => a.ExportCode).FirstOrDefaultAsync();
            if (model != null)
            {
                lastestNumber = model.ExportCode;
                string a = lastestNumber.Substring(lastestNumber.Length - 2); ;
                int NextDONumber = Convert.ToInt32(a) + 1;
                lastestNumber = DateTime.Today.ToString("ddMMyy") + "N" + NextDONumber.ToString("00");
            }
            else
            {
                int NextDONumber = 1;
                lastestNumber = DateTime.Today.ToString("ddMMyy") + "N" + NextDONumber.ToString("00");
            }

            return lastestNumber;
        }
    }
}
