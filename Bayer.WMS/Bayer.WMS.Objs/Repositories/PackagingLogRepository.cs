﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IPackagingLogRepository : IBaseRepository, IBaseRepository<PackagingLog>
    {
    }

    public class PackagingLogRepository : BaseRepository<BayerWMSContext, PackagingLog>, IPackagingLogRepository
    {
        public PackagingLogRepository(IBayerWMSContext context)
            : base(context)
        {

        }
    }
}
