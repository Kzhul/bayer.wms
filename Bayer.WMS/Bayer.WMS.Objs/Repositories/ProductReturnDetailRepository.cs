﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IProductReturnDetailRepository : IBaseRepository, IBaseRepository<ProductReturnDetail>
    {
        Task<IList<ProductReturnDetail>> GetIncludeProductAsync(Expression<Func<ProductReturnDetail, bool>> predicate = null);
    }

    public class ProductReturnDetailRepository : BaseRepository<BayerWMSContext, ProductReturnDetail>, IProductReturnDetailRepository
    {
        public ProductReturnDetailRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<ProductReturnDetail>> GetIncludeProductAsync(Expression<Func<ProductReturnDetail, bool>> predicate = null)
        {
            var details = await _context.ProductReturnDetails
                                .Where(predicate)
                                .Join(_context.Products,
                                    srd => srd.ProductID,
                                    p => p.ProductID,
                                    (srd, p) => new { ProductReturnDetail = srd, Product = p })
                                .GroupJoin(_context.ProductPackings.Where(a=>a.Type != ProductPacking.type.Pallet),
                                    pk => pk.ProductReturnDetail.ProductID,
                                    p => p.ProductID,                                    
                                    (srd, p) => new { ProductReturnDetail = srd.ProductReturnDetail, Product = srd.Product, ProductPacking = p.FirstOrDefault() })                                
                                .Select(p => new
                                {
                                    ProductReturnCode = p.ProductReturnDetail.ProductReturnCode,
                                    LineNbr = p.ProductReturnDetail.LineNbr,
                                    ProductID = p.ProductReturnDetail.ProductID,
                                    ProductCode = p.Product.ProductCode,
                                    ProductDescription = p.Product.Description,
                                    BatchCode = p.ProductReturnDetail.BatchCode,

                                    ReturnQty = p.ProductReturnDetail.ReturnQty,
                                    DeliveredQty = p.ProductReturnDetail.DeliveredQty,
                                    Description = p.ProductReturnDetail.Description,

                                    UOM = p.Product.UOM,
                                    PackageSize = p.ProductPacking.Quantity,

                                    Status = p.ProductReturnDetail.Status,
                                    CreatedBy = p.ProductReturnDetail.CreatedBy,
                                    CreatedDateTime = p.ProductReturnDetail.CreatedDateTime,
                                    CreatedBySitemapID = p.ProductReturnDetail.CreatedBySitemapID,
                                    UpdatedBy = p.ProductReturnDetail.CreatedBy,
                                    UpdatedDateTime = p.ProductReturnDetail.UpdatedDateTime,
                                    UpdatedBySitemapID = p.ProductReturnDetail.UpdatedBySitemapID,
                                    RowVersion = p.ProductReturnDetail.RowVersion,
                                })
                                .OrderBy(a => new { a.LineNbr })
                                .ToListAsync();

            return details.Select(p => new ProductReturnDetail(p)).ToList();
        }
    }
}
