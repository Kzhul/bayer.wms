﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IProductionPlanImportLogRepository : IBaseRepository, IBaseRepository<ProductionPlanImportLog>
    {
    }

    public class ProductionPlanImportLogRepository : BaseRepository<BayerWMSContext, ProductionPlanImportLog>, IProductionPlanImportLogRepository
    {
        public ProductionPlanImportLogRepository(IBayerWMSContext context)
            : base(context)
        {

        }
    }
}
