﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface ISplitNoteHeaderRepository : IBaseRepository, IBaseRepository<SplitNoteHeader>
    {
        Task<SplitNoteHeader> GetLastest();
        Task<string> GetNextNBR();
    }

    public class SplitNoteHeaderRepository : BaseRepository<BayerWMSContext, SplitNoteHeader>, ISplitNoteHeaderRepository
    {
        public SplitNoteHeaderRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<SplitNoteHeader> GetLastest()
        {
            var model = await _context.SplitNoteHeaders.OrderByDescending(a => a.SplitCode).FirstOrDefaultAsync();

            return model;
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.SplitNoteHeaders.Where(a => a.CreatedDateTime >= DateTime.Today).OrderByDescending(a => a.SplitCode).FirstOrDefaultAsync();
            if (model != null)
            {
                lastestNumber = model.SplitCode;
                string a = lastestNumber.Substring(lastestNumber.Length - 2); ;
                int NextDONumber = Convert.ToInt32(a) + 1;
                lastestNumber = DateTime.Today.ToString("ddMMyy") + "S" + NextDONumber.ToString("00");
            }
            else
            {
                int NextDONumber = 1;
                lastestNumber = DateTime.Today.ToString("ddMMyy") + "S" + NextDONumber.ToString("00");
            }

            return lastestNumber;
        }
    }
}
