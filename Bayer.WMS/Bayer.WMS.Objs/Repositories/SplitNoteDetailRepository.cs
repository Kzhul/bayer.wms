﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface ISplitNoteDetailRepository : IBaseRepository, IBaseRepository<SplitNoteDetail>
    {
        Task<IList<SplitNoteDetail>> GetIncludeProductAsync(Expression<Func<SplitNoteDetail, bool>> predicate = null);
        Task<IList<SplitNoteDetail>> GetIncludeProductAndPalletAsync(Expression<Func<SplitNoteDetail, bool>> predicate = null);
        
        Task<IList<SplitNoteDetail>> GetByDOCodeAndPartnerCode(string doCode, string partnerCode);
        Task<IList<SplitNoteDetail>> GetByDOCodeAndAnotherPartnerCode(string doCode, string partnerCode, string splitCode);
    }

    public class SplitNoteDetailRepository : BaseRepository<BayerWMSContext, SplitNoteDetail>, ISplitNoteDetailRepository
    {
        public SplitNoteDetailRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<SplitNoteDetail>> GetIncludeProductAndPalletAsync(Expression<Func<SplitNoteDetail, bool>> predicate = null)
        {
            var SplitNoteDetails = await _context.SplitNoteDetails
                                                .Where(predicate)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { SplitNoteDetail = pp, Product = p })
                                                .GroupJoin(_context.ProductPackings.Where(a => a.Type == ProductPacking.type.Carton),
                                                    pk => pk.Product.ProductID,
                                                    p => p.ProductID,
                                                    (srd, p) => new { SplitNoteDetail = srd.SplitNoteDetail, Product = srd.Product, ProductPacking = p.FirstOrDefault() })
                                                .Select(p => new //SplitNoteDetail()
                                                {
                                                    DOCode = p.SplitNoteDetail.DOCode,
                                                    SplitCode = p.SplitNoteDetail.SplitCode,
                                                    DeliveryCode = p.SplitNoteDetail.DeliveryCode,
                                                    ProductID = p.SplitNoteDetail.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,

                                                    BatchCodeDistributor = p.SplitNoteDetail.BatchCodeDistributor,
                                                    CompanyCode = p.SplitNoteDetail.CompanyCode,                                                    
                                                    CompanyName = p.SplitNoteDetail.CompanyName,
                                                    Province = p.SplitNoteDetail.Province,

                                                    Quantity = p.SplitNoteDetail.Quantity,
                                                    PackQuantity = p.SplitNoteDetail.PackQuantity,
                                                    PackSize = p.ProductPacking.Quantity,
                                                    PackType = p.SplitNoteDetail.PackType,
                                                    ProductQuantity = p.SplitNoteDetail.ProductQuantity,

                                                    SplittedQty = p.SplitNoteDetail.SplittedQty,
                                                    RequireReturnQty = p.SplitNoteDetail.RequireReturnQty,
                                                    ReturnedQty = p.SplitNoteDetail.ReturnedQty,

                                                    BatchCode = p.SplitNoteDetail.BatchCode,
                                                    Status = p.SplitNoteDetail.Status,
                                                    CreatedBy = p.SplitNoteDetail.CreatedBy,
                                                    CreatedDateTime = p.SplitNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.SplitNoteDetail.CreatedBy,
                                                    UpdatedDateTime = p.SplitNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.SplitNoteDetail.RowVersion,                                                    
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();

            var listDetail = SplitNoteDetails.Select(p => new SplitNoteDetail(p)).ToList();

            bool bolPalletInfo = false;
            string DOCode = string.Empty;
            string TicketCode = string.Empty;
            var ListPalletCodes = new List<PalletInfo>();
            if (listDetail.Count > 0)
            {
                DOCode = listDetail[0].DOCode;
                TicketCode = listDetail[0].SplitCode;

                ListPalletCodes = await _context.Pallets
                   .Where(p => p.DOImportCode == DOCode && p.ReferenceNbr == TicketCode)
                   .Join(_context.PalletStatuses,
                   p => p.PalletCode,
                   ps => ps.PalletCode,
                   (p, ps) => new PalletInfo { pallet = p, palletStatus = ps })
                   .Distinct()
                   .OrderBy(a => new { a.pallet.PalletCode })
                   .ToListAsync();

                bolPalletInfo = true;
            }
            
            foreach (SplitNoteDetail item in listDetail)
            {
                if (bolPalletInfo)
                {
                    var itemPalletCodes = ListPalletCodes.Where(a =>
                        a.palletStatus.ProductLot == item.BatchCode
                        && a.palletStatus.ProductID == item.ProductID
                        && a.pallet.CompanyCode == item.CompanyCode
                    ).Select(a => a.palletStatus.PalletCode).Distinct().ToList();

                    item.PalletCodes = String.Join(", ", itemPalletCodes.ToArray());
                }
            }

            return listDetail;
        }

        public virtual async Task<IList<SplitNoteDetail>> GetIncludeProductAsync(Expression<Func<SplitNoteDetail, bool>> predicate = null)
        {
            var SplitNoteDetails = await _context.SplitNoteDetails
                                                .Where(predicate)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { SplitNoteDetail = pp, Product = p })
                                                .Select(p => new //SplitNoteDetail()
                                                {
                                                    DOCode = p.SplitNoteDetail.DOCode,
                                                    SplitCode = p.SplitNoteDetail.SplitCode,
                                                    DeliveryCode = p.SplitNoteDetail.DeliveryCode,
                                                    ProductID = p.SplitNoteDetail.ProductID,
                                                    ProductCode = p.Product.ProductCode,
                                                    ProductDescription = p.Product.Description,

                                                    BatchCodeDistributor = p.SplitNoteDetail.BatchCodeDistributor,
                                                    CompanyCode = p.SplitNoteDetail.CompanyCode,
                                                    CompanyName = p.SplitNoteDetail.CompanyName,
                                                    Province = p.SplitNoteDetail.Province,

                                                    Quantity = p.SplitNoteDetail.Quantity,
                                                    PackQuantity = p.SplitNoteDetail.PackQuantity,
                                                    PackType = p.SplitNoteDetail.PackType,
                                                    ProductQuantity = p.SplitNoteDetail.ProductQuantity,

                                                    SplittedQty = p.SplitNoteDetail.SplittedQty,
                                                    RequireReturnQty = p.SplitNoteDetail.RequireReturnQty,
                                                    ReturnedQty = p.SplitNoteDetail.ReturnedQty,

                                                    BatchCode = p.SplitNoteDetail.BatchCode,
                                                    Status = p.SplitNoteDetail.Status,
                                                    CreatedBy = p.SplitNoteDetail.CreatedBy,
                                                    CreatedDateTime = p.SplitNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.SplitNoteDetail.CreatedBy,
                                                    UpdatedDateTime = p.SplitNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.SplitNoteDetail.RowVersion,
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();

            return SplitNoteDetails.Select(p => new SplitNoteDetail(p)).ToList();


        }

        public virtual async Task<IList<SplitNoteDetail>> GetByDOCodeAndPartnerCode(string doCode, string partnerCode)
        {
            var SplitNoteDetails = await _context.SplitNoteDetails
                                                .Where(a=>a.DOCode == doCode)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { SplitNoteDetail = pp, Product = p })
                                                .Join(_context.SplitNoteHeaders,
                                                    pp => new { pp.SplitNoteDetail.DOCode, pp.SplitNoteDetail.SplitCode },
                                                    p => new { DOCode = p.DOImportCode , p.SplitCode},
                                                    (pp, p) => new { pp = pp, h = p })
                                                //.Where(a => a.h.PartnerCode == partnerCode)
                                                .Select(p => new //SplitNoteDetail()
                                                {
                                                    DOCode = p.pp.SplitNoteDetail.DOCode,
                                                    SplitCode = p.pp.SplitNoteDetail.SplitCode,
                                                    DeliveryCode = p.pp.SplitNoteDetail.DeliveryCode,
                                                    ProductID = p.pp.SplitNoteDetail.ProductID,
                                                    ProductCode = p.pp.Product.ProductCode,
                                                    ProductDescription = p.pp.Product.Description,

                                                    BatchCodeDistributor = p.pp.SplitNoteDetail.BatchCodeDistributor,
                                                    CompanyCode = p.pp.SplitNoteDetail.CompanyCode,
                                                    CompanyName = p.pp.SplitNoteDetail.CompanyName,
                                                    Province = p.pp.SplitNoteDetail.Province,

                                                    Quantity = p.pp.SplitNoteDetail.Quantity,
                                                    PackQuantity = p.pp.SplitNoteDetail.PackQuantity,
                                                    PackType = p.pp.SplitNoteDetail.PackType,
                                                    ProductQuantity = p.pp.SplitNoteDetail.ProductQuantity,

                                                    SplittedQty = p.pp.SplitNoteDetail.SplittedQty,
                                                    RequireReturnQty = p.pp.SplitNoteDetail.RequireReturnQty,
                                                    ReturnedQty = p.pp.SplitNoteDetail.ReturnedQty,

                                                    BatchCode = p.pp.SplitNoteDetail.BatchCode,
                                                    Status = p.pp.SplitNoteDetail.Status,
                                                    CreatedBy = p.pp.SplitNoteDetail.CreatedBy,
                                                    CreatedDateTime = p.pp.SplitNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.pp.SplitNoteDetail.CreatedBy,
                                                    UpdatedDateTime = p.pp.SplitNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.pp.SplitNoteDetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();

            return SplitNoteDetails.Select(p => new SplitNoteDetail(p)).ToList();
        }


        public virtual async Task<IList<SplitNoteDetail>> GetByDOCodeAndAnotherPartnerCode(string doCode, string partnerCode, string splitCode)
        {
            var SplitNoteDetails = await _context.SplitNoteDetails
                                                .Where(a => a.DOCode == doCode)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { SplitNoteDetail = pp, Product = p })
                                                .Join(_context.SplitNoteHeaders,
                                                    pp => new { pp.SplitNoteDetail.DOCode, pp.SplitNoteDetail.SplitCode },
                                                    p => new { DOCode = p.DOImportCode, p.SplitCode },
                                                    (pp, p) => new { pp = pp, h = p })
                                                .Where(a => a.h.SplitCode != splitCode)
                                                .Select(p => new //SplitNoteDetail()
                                                {
                                                    DOCode = p.pp.SplitNoteDetail.DOCode,
                                                    SplitCode = p.pp.SplitNoteDetail.SplitCode,
                                                    DeliveryCode = p.pp.SplitNoteDetail.DeliveryCode,
                                                    ProductID = p.pp.SplitNoteDetail.ProductID,
                                                    ProductCode = p.pp.Product.ProductCode,
                                                    ProductDescription = p.pp.Product.Description,

                                                    BatchCodeDistributor = p.pp.SplitNoteDetail.BatchCodeDistributor,
                                                    CompanyCode = p.pp.SplitNoteDetail.CompanyCode,
                                                    CompanyName = p.pp.SplitNoteDetail.CompanyName,
                                                    Province = p.pp.SplitNoteDetail.Province,

                                                    Quantity = p.pp.SplitNoteDetail.Quantity,
                                                    PackQuantity = p.pp.SplitNoteDetail.PackQuantity,
                                                    PackType = p.pp.SplitNoteDetail.PackType,
                                                    ProductQuantity = p.pp.SplitNoteDetail.ProductQuantity,

                                                    SplittedQty = p.pp.SplitNoteDetail.SplittedQty,
                                                    RequireReturnQty = p.pp.SplitNoteDetail.RequireReturnQty,
                                                    ReturnedQty = p.pp.SplitNoteDetail.ReturnedQty,

                                                    BatchCode = p.pp.SplitNoteDetail.BatchCode,
                                                    Status = p.pp.SplitNoteDetail.Status,
                                                    CreatedBy = p.pp.SplitNoteDetail.CreatedBy,
                                                    CreatedDateTime = p.pp.SplitNoteDetail.CreatedDateTime,
                                                    UpdatedBy = p.pp.SplitNoteDetail.CreatedBy,
                                                    UpdatedDateTime = p.pp.SplitNoteDetail.UpdatedDateTime,
                                                    RowVersion = p.pp.SplitNoteDetail.RowVersion
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.ProductDescription, a.Quantity, a.BatchCode })
                                                .ToListAsync();

            return SplitNoteDetails.Select(p => new SplitNoteDetail(p)).ToList();
        }
    }
}