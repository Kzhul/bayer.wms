﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IProductReturnDetailSplitRepository : IBaseRepository, IBaseRepository<ProductReturnDetailSplit>
    {
        Task<IList<ProductReturnDetailSplit>> GetIncludeProductAsync(Expression<Func<ProductReturnDetailSplit, bool>> predicate = null);
    }

    public class ProductReturnDetailSplitRepository : BaseRepository<BayerWMSContext, ProductReturnDetailSplit>, IProductReturnDetailSplitRepository
    {
        public ProductReturnDetailSplitRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<ProductReturnDetailSplit>> GetIncludeProductAsync(Expression<Func<ProductReturnDetailSplit, bool>> predicate = null)
        {
            var details = await _context.ProductReturnDetailSplits
                                .Where(predicate)
                                .Join(_context.Products,
                                    srd => srd.ProductID,
                                    p => p.ProductID,
                                    (srd, p) => new { ProductReturnDetailSplit = srd, Product = p })
                                .GroupJoin(_context.ProductPackings.Where(a=>a.Type != ProductPacking.type.Pallet),
                                    pk => pk.ProductReturnDetailSplit.ProductID,
                                    p => p.ProductID,                                    
                                    (srd, p) => new { ProductReturnDetailSplit = srd.ProductReturnDetailSplit, Product = srd.Product, ProductPacking = p.FirstOrDefault() })                                
                                .Select(p => new
                                {
                                    ProductReturnCode = p.ProductReturnDetailSplit.ProductReturnCode,
                                    LineNbr = p.ProductReturnDetailSplit.LineNbr,
                                    ProductID = p.ProductReturnDetailSplit.ProductID,
                                    ProductCode = p.Product.ProductCode,
                                    ProductDescription = p.Product.Description,
                                    BatchCode = p.ProductReturnDetailSplit.BatchCode,

                                    ProductBarcode = p.ProductReturnDetailSplit.ProductBarcode,
                                    PalletCode = p.ProductReturnDetailSplit.PalletCode,
                                    EncryptedProductBarcode = p.ProductReturnDetailSplit.EncryptedProductBarcode,
                                    CartonBarcode = p.ProductReturnDetailSplit.CartonBarcode,
                                    DeliveryDate = p.ProductReturnDetailSplit.DeliveryDate,

                                    Description = p.ProductReturnDetailSplit.Description,

                                    UOM = p.Product.UOM,
                                    PackageSize = p.ProductPacking.Quantity,

                                    Status = p.ProductReturnDetailSplit.Status,
                                    CreatedBy = p.ProductReturnDetailSplit.CreatedBy,
                                    CreatedDateTime = p.ProductReturnDetailSplit.CreatedDateTime,
                                    CreatedBySitemapID = p.ProductReturnDetailSplit.CreatedBySitemapID,
                                    UpdatedBy = p.ProductReturnDetailSplit.CreatedBy,
                                    UpdatedDateTime = p.ProductReturnDetailSplit.UpdatedDateTime,
                                    UpdatedBySitemapID = p.ProductReturnDetailSplit.UpdatedBySitemapID,
                                    RowVersion = p.ProductReturnDetailSplit.RowVersion,
                                })
                                .OrderBy(a => new { a.LineNbr })
                                .ToListAsync();

            return details.Select(p => new ProductReturnDetailSplit(p)).ToList();
        }
    }
}
