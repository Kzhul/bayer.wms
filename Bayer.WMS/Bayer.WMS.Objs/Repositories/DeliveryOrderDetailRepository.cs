﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IDeliveryOrderDetailRepository : IBaseRepository, IBaseRepository<DeliveryOrderDetail>
    {
        Task<IList<DeliveryOrderDetail>> GetIncludeProductAsync(Expression<Func<DeliveryOrderDetail, bool>> predicate = null);
    }

    public class DeliveryOrderDetailRepository : BaseRepository<BayerWMSContext, DeliveryOrderDetail>, IDeliveryOrderDetailRepository
    {
        public DeliveryOrderDetailRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }

        public virtual async Task<IList<DeliveryOrderDetail>> GetIncludeProductAsync(Expression<Func<DeliveryOrderDetail, bool>> predicate = null)
        {
            var DeliveryOrderDetails = await _context.DeliveryOrderDetails
                                                .Where(predicate)
                                                .Join(_context.Products,
                                                    pp => pp.ProductID,
                                                    p => p.ProductID,
                                                    (pp, p) => new { DeliveryOrderDetail = pp, Product = p })
                                                .Select(p => new //DeliveryOrderDetail()
                                                {
                                                    DOImportCode = p.DeliveryOrderDetail.DOImportCode,
                                                    LineNbr = p.DeliveryOrderDetail.LineNbr,
                                                    Delivery = p.DeliveryOrderDetail.Delivery,
                                                    ProductID = p.DeliveryOrderDetail.ProductID,
                                                    ProductCode = p.DeliveryOrderDetail.ProductID != 0 ? p.Product.ProductCode : p.DeliveryOrderDetail.ProductCode,
                                                    ProductDescription = p.DeliveryOrderDetail.ProductID != 0 ? p.Product.Description : p.DeliveryOrderDetail.ProductName,
                                                    Quantity = p.DeliveryOrderDetail.Quantity,
                                                    BatchCode = p.DeliveryOrderDetail.BatchCode,
                                                    BatchCodeDistributor = p.DeliveryOrderDetail.BatchCodeDistributor,
                                                    CompanyCode = p.DeliveryOrderDetail.CompanyCode,
                                                    CompanyName = p.DeliveryOrderDetail.CompanyName,
                                                    ProvinceSoldTo = p.DeliveryOrderDetail.ProvinceSoldTo,
                                                    ProvinceShipTo = p.DeliveryOrderDetail.ProvinceShipTo,
                                                    DeliveryDate = p.DeliveryOrderDetail.DeliveryDate,
                                                    Status = p.DeliveryOrderDetail.Status,
                                                    CreatedBy = p.DeliveryOrderDetail.CreatedBy,
                                                    CreatedDateTime = p.DeliveryOrderDetail.CreatedDateTime,
                                                    UpdatedBy = p.DeliveryOrderDetail.CreatedBy,
                                                    UpdatedDateTime = p.DeliveryOrderDetail.UpdatedDateTime,
                                                    RowVersion = p.DeliveryOrderDetail.RowVersion,
                                                    PreparedQty = p.DeliveryOrderDetail.PreparedQty,
                                                    DeliveredQty = p.DeliveryOrderDetail.DeliveredQty,
                                                    DOQuantity = p.DeliveryOrderDetail.DOQuantity,
                                                    RequireReturnQty = p.DeliveryOrderDetail.RequireReturnQty,
                                                    ChangeReason = p.DeliveryOrderDetail.ChangeReason,
                                                    NetWeight = p.DeliveryOrderDetail.NetWeight,
                                                    TotalWeight = p.DeliveryOrderDetail.TotalWeight,
                                                    Volume = p.DeliveryOrderDetail.Volume,
                                                    VolumeUnit = p.DeliveryOrderDetail.VolumeUnit,
                                                    UOM = p.DeliveryOrderDetail.UOM,
                                                })
                                                .Distinct()
                                                .OrderBy(a => new { a.LineNbr})
                                                .ToListAsync();

            return DeliveryOrderDetails.Select(p => new DeliveryOrderDetail(p)).ToList();
        }
    }
}
