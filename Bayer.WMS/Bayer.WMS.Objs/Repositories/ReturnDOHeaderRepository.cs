﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IReturnDOHeaderRepository : IBaseRepository, IBaseRepository<ReturnDOHeader>
    {
        Task<ReturnDOHeader> GetLastestDO();
        Task<IList<ReturnDOHeader>> GetLastestDOList(Expression<Func<ReturnDOHeader, bool>> predicate = null);
        Task<string> GetNextNBR();
    }

    public class ReturnDOHeaderRepository : BaseRepository<BayerWMSContext, ReturnDOHeader>, IReturnDOHeaderRepository
    {
        public ReturnDOHeaderRepository(IBayerWMSContext context) 
            : base(context)
        {
            
        }

        public virtual async Task<IList<ReturnDOHeader>> GetLastestDOList(Expression<Func<ReturnDOHeader, bool>> predicate = null)
        {
            var model = await _context.ReturnDOHeaders.OrderByDescending(a=>a.DOImportCode).ToListAsync();

            return model.ToList();
        }

        public virtual async Task<ReturnDOHeader> GetLastestDO()
        {
            var model = await _context.ReturnDOHeaders.OrderByDescending(a => a.DOImportCode).FirstOrDefaultAsync();

            return model;
        }

        public virtual async Task<string> GetNextNBR()
        {
            //210113R01
            //ddMMyyExx
            string lastestNumber = string.Empty;
            var model = await _context.ReturnDOHeaders.Where(a=>a.CreatedDateTime >= DateTime.Today).OrderByDescending(a => a.ReturnDOCode).FirstOrDefaultAsync();
            if (model != null)
            {
                lastestNumber = model.ReturnDOCode;
                string a = lastestNumber.Substring(lastestNumber.Length - 2); ;
                int NextDONumber = Convert.ToInt32(a) + 1;
                lastestNumber = "RD" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }
            else
            {
                int NextDONumber = 1;
                lastestNumber = "RD" + DateTime.Today.ToString("yyMMdd") + NextDONumber.ToString("00");
            }

            return lastestNumber;
        }
    }
}
