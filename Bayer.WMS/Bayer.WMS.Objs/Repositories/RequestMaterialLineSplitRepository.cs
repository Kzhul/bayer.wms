﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Bayer.WMS.Objs.Repositories
{
    public interface IRequestMaterialLineSplitRepository : IBaseRepository, IBaseRepository<RequestMaterialLineSplit>
    {
        Task<IList<RequestMaterialLineSplit>> GetIncludeProductAsync(Expression<Func<RequestMaterialLineSplit, bool>> predicate = null);
    }

    public class RequestMaterialLineSplitRepository : BaseRepository<BayerWMSContext, RequestMaterialLineSplit>, IRequestMaterialLineSplitRepository
    {
        public RequestMaterialLineSplitRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<RequestMaterialLineSplit>> GetIncludeProductAsync(Expression<Func<RequestMaterialLineSplit, bool>> predicate = null)
        {
            var reqMaterialLineSplits = await _context.RequestMaterialLineSplits
                                        .Where(predicate)
                                        .Join(_context.Products,
                                            rml => rml.ProductID,
                                            p => p.ProductID,
                                            (rml, p) => new { RequestMaterialLineSplit = rml, Product = p })
                                        .Select(p => new
                                        {
                                            DocumentNbr = p.RequestMaterialLineSplit.DocumentNbr,
                                            LineNbr = p.RequestMaterialLineSplit.LineNbr,
                                            ProductID = p.RequestMaterialLineSplit.ProductID,
                                            ProductCode = p.Product.ProductCode,
                                            ProductDescription = p.Product.Description,
                                            PalletCode = p.RequestMaterialLineSplit.PalletCode,
                                            ProductLot = p.RequestMaterialLineSplit.ProductLot,
                                            Quantity = p.RequestMaterialLineSplit.Quantity,
                                            Status = p.RequestMaterialLineSplit.Status,
                                            CreatedBy = p.RequestMaterialLineSplit.CreatedBy,
                                            CreatedBySitemapID = p.RequestMaterialLineSplit.CreatedBySitemapID,
                                            CreatedDateTime = p.RequestMaterialLineSplit.CreatedDateTime,
                                            UpdatedBy = p.RequestMaterialLineSplit.CreatedBy,
                                            UpdatedBySitemapID = p.RequestMaterialLineSplit.UpdatedBySitemapID,
                                            UpdatedDateTime = p.RequestMaterialLineSplit.UpdatedDateTime,
                                            RowVersion = p.RequestMaterialLineSplit.RowVersion
                                        }).ToListAsync();

            return reqMaterialLineSplits.Select(p => new RequestMaterialLineSplit(p)).ToList();
        }
    }
}
