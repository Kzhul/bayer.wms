﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Objs.Repositories
{
    public interface ICartonBarcodeRepository : IBaseRepository, IBaseRepository<CartonBarcode>
    {
        Task<IList<CartonBarcode>> GetCartonBarcodeNotYetPrinted(string productLot, int qty);
    }

    public class CartonBarcodeRepository : BaseRepository<BayerWMSContext, CartonBarcode>, ICartonBarcodeRepository
    {
        public CartonBarcodeRepository(IBayerWMSContext context)
            : base(context)
        {

        }

        public virtual async Task<IList<CartonBarcode>> GetCartonBarcodeNotYetPrinted(string productLot, int currentLabel)
        {
            var cartonBarcodes = (await GetAsync(p => p.ProductLot == productLot)).OrderBy(p => p.Barcode).Skip(currentLabel).ToList();
            return cartonBarcodes;
            //return cartonBarcodes.Where(p => int.Parse(p.Barcode.Replace($"{productLot}C", String.Empty)) > currentLabel).ToList();
        }
    }
}
