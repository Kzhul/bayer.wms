﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace Bayer.WMS.Objs
{
    public interface IBaseRepository
    {
        void Refresh(object entity);

        Task Commit();
    }

    public interface IBaseRepository<T> : IBaseRepository where T : class
    {
        Task Insert(T entity);

        Task Update(T entity, object[] keyValues);

        Task Update(T entity, string[] updatedFields, object[] keyValues);

        Task Update(T entity, string[] updatedFields, string[] notUpdatedFields, object[] keyValues);

        Task Delete(T entity);

        Task Delete(object[] keyValues);

        Task InsertOrUpdate(T entity, object[] keyValues);

        Task InsertOrUpdate(T entity, string[] updatedFields, object[] keyValues);

        Task InsertOrUpdate(T entity, string[] updatedFields, string[] notUpdatedFields, object[] keyValues);

        Task<int> GetTotalRecord(Expression<Func<T, bool>> predicate = null);

        T GetSingle(Expression<Func<T, bool>> predicate = null);

        IList<T> Get(Expression<Func<T, bool>> predicate = null);

        Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate = null);

        Task<T> GetFirstAsync<TKey>(Expression<Func<T, TKey>> keySelector, Expression<Func<T, bool>> predicate = null);

        Task<T> GetLastAsync<TKey>(Expression<Func<T, TKey>> keySelector, Expression<Func<T, bool>> predicate = null);

        Task<IList<T>> GetAsync(Expression<Func<T, bool>> predicate = null, int top = 0);

        Task<IList<T>> GetByPaginationAsync<TKey>(int page, Expression<Func<T, TKey>> keySelector, Expression<Func<T, bool>> predicate = null);

        Task ExecuteNonQuery(string storeName, params SqlParameter[] parameters);

        Task ExecuteNonQueryLongPeriod(string storeName, params SqlParameter[] parameters);

        Task ExecuteNonQuery_Trans(string storeName, params SqlParameter[] parameters);

        Task<DataTable> ExecuteDataTable(string storeName, params SqlParameter[] parameters);

        Task<IList<T>> ExecuteEntityList(string storeName, params SqlParameter[] parameters);

        Task<string> ExecuteScalar(string storeName, params SqlParameter[] parameters);

        Task<string> ExecuteScalar_Trans(string storeName, params SqlParameter[] parameters);
    }

    public abstract class BaseRepository<C, T> : IBaseRepository<T> where T : class where C : BayerWMSContext, new()
    {
        protected BayerWMSContext _context;

        public BaseRepository(IBayerWMSContext context)
        {
            _context = context as BayerWMSContext;
        }

        public void Refresh(object entity)
        {
            _context.Refresh(entity);
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }

        public virtual async Task Insert(T entity)
        {
            try
            {
                await Task.Run(() =>
                   {
                       _context.Set<T>().Add(entity);
                   });
            }
            catch
            {
                throw;
            }
        }

        public virtual async Task Update(T entity, object[] keyValues)
        {
            try
            {
                var existEntity = await _context.Set<T>().FindAsync(keyValues);
                var attachedEntry = _context.Entry<T>(existEntity);
                attachedEntry.CurrentValues.SetValues(entity);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new DbUpdateConcurrencyException("Dữ liệu đã bị cập nhật bởi người dùng khác. Thao tác cập nhật đã bị hủy và hiển thị dữ liệu mới nhất.");
            }
            catch
            {
                throw;
            }
        }

        public virtual async Task Update(T entity, string[] updatedFields, object[] keyValues)
        {
            try
            {
                var existEntity = await _context.Set<T>().FindAsync(keyValues);
                var attachedEntry = _context.Entry<T>(existEntity);
                var newEntry = _context.Entry<T>(entity);

                if (updatedFields != null)
                {
                    foreach (string field in updatedFields)
                    {
                        attachedEntry.Property(field).IsModified = true;
                        attachedEntry.Property(field).CurrentValue = newEntry.Property(field).CurrentValue;
                    }
                }
                else
                {
                    attachedEntry.CurrentValues.SetValues(entity);
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new DbUpdateConcurrencyException("Dữ liệu đã bị cập nhật bởi người dùng khác. Thao tác cập nhật đã bị hủy và hiển thị dữ liệu mới nhất.");
            }
            catch
            {
                throw;
            }
        }

        public virtual async Task Update(T entity, string[] updatedFields, string[] notUpdatedFields, object[] keyValues)
        {
            try
            {
                var existEntity = await _context.Set<T>().FindAsync(keyValues);
                var attachedEntry = _context.Entry<T>(existEntity);
                var newEntry = _context.Entry<T>(entity);

                if (updatedFields != null)
                {
                    foreach (string field in updatedFields)
                    {
                        attachedEntry.Property(field).IsModified = true;
                        attachedEntry.Property(field).CurrentValue = newEntry.Property(field).CurrentValue;
                    }
                }
                else
                {
                    attachedEntry.CurrentValues.SetValues(entity);
                }

                if (notUpdatedFields != null)
                {
                    foreach (string field in notUpdatedFields)
                    {
                        attachedEntry.Property(field).IsModified = false;
                    }
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new DbUpdateConcurrencyException("Dữ liệu đã bị cập nhật bởi người dùng khác. Thao tác cập nhật đã bị hủy và hiển thị dữ liệu mới nhất.");
            }
            catch
            {
                throw;
            }
        }

        public virtual async Task Delete(T entity)
        {
            try
            {
                await Task.Run(() =>
                   {
                       var dbSet = _context.Set<T>();
                       dbSet.Attach(entity);
                       dbSet.Remove(entity);
                   });
            }
            catch
            {
                throw;
            }
        }

        public virtual async Task Delete(object[] keyValues)
        {
            try
            {
                var entity = await _context.Set<T>().FindAsync(keyValues);
                _context.Set<T>().Remove(entity);
            }
            catch
            {
                throw;
            }
        }

        public virtual async Task InsertOrUpdate(T entity, object[] keyValues)
        {
            try
            {
                var existEntity = await _context.Set<T>().FindAsync(keyValues);
                if (existEntity == null)
                {
                    _context.Set<T>().Add(entity);
                }
                else
                {
                    var attachedEntry = _context.Entry<T>(existEntity);
                    attachedEntry.CurrentValues.SetValues(entity);
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new DbUpdateConcurrencyException("Dữ liệu đã bị cập nhật bởi người dùng khác. Thao tác cập nhật đã bị hủy và hiển thị dữ liệu mới nhất.");
            }
            catch (Exception ex)
            {
                //throw;
                //Do nothung
                Utility.LogEx(ex);
                Thread.Sleep(2000);
                await InsertOrUpdate(entity, keyValues);
            }
        }

        public virtual async Task InsertOrUpdate(T entity, string[] updatedFields, object[] keyValues)
        {
            try
            {
                var existEntity = await _context.Set<T>().FindAsync(keyValues);
                if (existEntity == null)
                {
                    _context.Set<T>().Add(entity);
                }
                else
                {
                    var attachedEntry = _context.Entry<T>(existEntity);
                    var newEntry = _context.Entry<T>(entity);

                    if (updatedFields != null)
                    {
                        foreach (string field in updatedFields)
                        {
                            attachedEntry.Property(field).IsModified = true;
                            attachedEntry.Property(field).CurrentValue = newEntry.Property(field).CurrentValue;
                        }
                    }
                    else
                    {
                        attachedEntry.CurrentValues.SetValues(entity);
                    }
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw new DbUpdateConcurrencyException("Dữ liệu đã bị cập nhật bởi người dùng khác. Thao tác cập nhật đã bị hủy và hiển thị dữ liệu mới nhất.");
            }
            catch
            {
                throw;
            }
        }

        //private void SetTransactionManagerField(string fieldName, object value)
        //{
        //    typeof(TransactionManager).GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, value);
        //}

        //public TransactionScope CreateTransactionScope(TimeSpan timeout)
        //{
        //    SetTransactionManagerField("_cachedMaxTimeout", true);
        //    SetTransactionManagerField("_maximumTimeout", timeout);
        //    return new TransactionScope(TransactionScopeOption.RequiresNew, timeout);
        //}

        public virtual async Task InsertOrUpdate(T entity, string[] updatedFields, string[] notUpdatedFields, object[] keyValues)
        {
            //using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromMinutes(20)))
            //{
            try
            {
                var existEntity = await _context.Set<T>().FindAsync(keyValues);
                if (existEntity == null)
                {
                    _context.Set<T>().Add(entity);
                }
                else
                {
                    var attachedEntry = _context.Entry<T>(existEntity);
                    var newEntry = _context.Entry<T>(entity);

                    if (updatedFields != null)
                    {
                        foreach (string field in updatedFields)
                        {
                            attachedEntry.Property(field).IsModified = true;
                            attachedEntry.Property(field).CurrentValue = newEntry.Property(field).CurrentValue;
                        }
                    }
                    else
                    {
                        attachedEntry.CurrentValues.SetValues(entity);
                    }

                    if (notUpdatedFields != null)
                    {
                        foreach (string field in notUpdatedFields)
                        {
                            attachedEntry.Property(field).IsModified = false;
                        }
                    }
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                //scope.Dispose();
                throw new DbUpdateConcurrencyException("Dữ liệu đã bị cập nhật bởi người dùng khác. Thao tác cập nhật đã bị hủy và hiển thị dữ liệu mới nhất.");
            }
            catch
            {

                //scope.Dispose();
                throw;
            }
            //Your stuff goes here

            //scope.Complete();
            //}
        }

        public virtual async Task<int> GetTotalRecord(Expression<Func<T, bool>> predicate = null)
        {
            try
            {
                if (predicate != null)
                    return await _context.Set<T>().CountAsync(predicate);
                return await _context.Set<T>().CountAsync();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual T GetSingle(Expression<Func<T, bool>> predicate = null)
        {
            try
            {
                if (predicate != null)
                    return _context.Set<T>().FirstOrDefault(predicate);
                return _context.Set<T>().FirstOrDefault();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual IList<T> Get(Expression<Func<T, bool>> predicate = null)
        {
            try
            {
                if (predicate != null)
                    return _context.Set<T>().Where(predicate).ToList();
                return _context.Set<T>().ToList();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual async Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate = null)
        {
            try
            {
                if (predicate != null)
                    return await _context.Set<T>().FirstOrDefaultAsync(predicate);
                return await _context.Set<T>().FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual async Task<T> GetFirstAsync<TKey>(Expression<Func<T, TKey>> keySelector, Expression<Func<T, bool>> predicate = null)
        {
            try
            {
                if (predicate != null)
                    return await _context.Set<T>().OrderBy(keySelector).FirstOrDefaultAsync(predicate);
                return await _context.Set<T>().OrderBy(keySelector).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual async Task<T> GetLastAsync<TKey>(Expression<Func<T, TKey>> keySelector, Expression<Func<T, bool>> predicate = null)
        {
            try
            {
                if (predicate != null)
                    return await _context.Set<T>().OrderByDescending(keySelector).FirstOrDefaultAsync(predicate);
                return await _context.Set<T>().OrderByDescending(keySelector).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual async Task<IList<T>> GetAsync(Expression<Func<T, bool>> predicate = null, int top = 0)
        {
            try
            {
                if (predicate != null)
                {
                    if (top == 0)
                        return await _context.Set<T>().Where(predicate).ToListAsync();
                    else
                        return await _context.Set<T>().Where(predicate).Take(top).ToListAsync();
                }
                else
                {
                    if (top == 0)
                        return await _context.Set<T>().ToListAsync();
                    else
                        return await _context.Set<T>().Take(top).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual async Task<IList<T>> GetByPaginationAsync<TKey>(int page, Expression<Func<T, TKey>> keySelector, Expression<Func<T, bool>> predicate = null)
        {
            return null;
            //int skip = (page - 1) * Utility.PageSize;
            //int take = Utility.PageSize;

            //if (predicate != null)
            //    return await _context.Set<T>().Where(predicate).OrderBy(keySelector).Skip(skip).Take(take).ToListAsync();
            //return await _context.Set<T>().OrderBy(keySelector).Skip(skip).Take(take).ToListAsync();
        }


        public virtual async Task ExecuteNonQueryLongPeriod(string storeName, params SqlParameter[] parameters)
        {
            try
            {
                try
                {
                    var conn = _context.Database.Connection as SqlConnection;
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    using (var cmd = new SqlCommand(storeName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 300; //5 minutes
                        cmd.Parameters.Clear();

                        if (parameters != null)
                        {
                            foreach (var param in parameters)
                            {
                                cmd.Parameters.Add(param);
                            }
                        }

                        await cmd.ExecuteNonQueryAsync();
                    }
                }
                finally
                {
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual async Task ExecuteNonQuery(string storeName, params SqlParameter[] parameters)
        {
            try
            {
                try
                {
                    var conn = _context.Database.Connection as SqlConnection;
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    using (var cmd = new SqlCommand(storeName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 300; //5 minutes
                        cmd.Parameters.Clear();


                        if (parameters != null)
                        {
                            foreach (var param in parameters)
                            {
                                cmd.Parameters.Add(param);
                            }
                        }

                        await cmd.ExecuteNonQueryAsync();
                    }
                }
                finally
                {
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual async Task ExecuteNonQuery_Trans(string storeName, params SqlParameter[] parameters)
        {
            try
            {
                try
                {
                    var conn = _context.Database.Connection as SqlConnection;
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    using (var trans = conn.BeginTransaction())
                    {
                        using (var cmd = new SqlCommand(storeName, conn, trans))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 300; //5 minutes
                            cmd.Parameters.Clear();

                            if (parameters != null)
                            {
                                foreach (var param in parameters)
                                {
                                    cmd.Parameters.Add(param);
                                }
                            }

                            await cmd.ExecuteNonQueryAsync();
                        }

                        trans.Commit();
                    }
                }
                finally
                {
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }

        public virtual async Task<DataTable> ExecuteDataTable(string storeName, params SqlParameter[] parameters)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Constraints.Clear();
                try
                {
                    var conn = _context.Database.Connection as SqlConnection;
                    if (conn.State != ConnectionState.Open && conn.State != ConnectionState.Connecting)
                        conn.Open();

                    int threshold = 0;
                    while (conn.State == ConnectionState.Connecting)
                    {
                        threshold++;
                        if (threshold == 10)
                            throw new WrappedException("Kết nối không thành công, vui lòng kiểm tra lại cáp mạng hoặc wifi");
                        Thread.Sleep(1000);
                    }

                    using (var cmd = new SqlCommand(storeName, conn))
                    {
                        cmd.CommandTimeout = 300; //5 minutes
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        if (parameters != null)
                        {
                            foreach (var param in parameters)
                            {
                                cmd.Parameters.Add(param);
                            }
                        }

                        var reader = await cmd.ExecuteReaderAsync();
                        dt.Load(reader);
                    }

                    return dt;
                }
                finally
                {
                }
            }
            catch (Exception ex)
            {
                //throw;
                Utility.LogEx(ex);
                return new DataTable();
            }
        }

        public virtual async Task<string> ExecuteScalar_Trans(string storeName, params SqlParameter[] parameters)
        {
            string dt = string.Empty;
            try
            {
                try
                {
                    var conn = _context.Database.Connection as SqlConnection;
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    using (var trans = conn.BeginTransaction())
                    {
                        using (var cmd = new SqlCommand(storeName, conn, trans))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 300; //5 minutes
                            cmd.Parameters.Clear();

                            if (parameters != null)
                            {
                                foreach (var param in parameters)
                                {
                                    cmd.Parameters.Add(param);
                                }
                            }
                            dt = (string) await cmd.ExecuteScalarAsync();
                        }

                        trans.Commit();
                    }
                }
                finally
                {
                }
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
            return dt;
        }

        public virtual async Task<string> ExecuteScalar(string storeName, params SqlParameter[] parameters)
        {
            try
            {
                string strdt = string.Empty;

                try
                {
                    var conn = _context.Database.Connection as SqlConnection;
                    if (conn.State != ConnectionState.Open && conn.State != ConnectionState.Connecting)
                        conn.Open();

                    int threshold = 0;
                    while (conn.State == ConnectionState.Connecting)
                    {
                        threshold++;
                        if (threshold == 10)
                            throw new WrappedException("Kết nối không thành công, vui lòng kiểm tra lại cáp mạng hoặc wifi");
                        Thread.Sleep(1000);
                    }

                    using (var cmd = new SqlCommand(storeName, conn))
                    {
                        cmd.CommandTimeout = 300; //5 minutes
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();

                        if (parameters != null)
                        {
                            foreach (var param in parameters)
                            {
                                cmd.Parameters.Add(param);
                            }
                        }

                        DataTable dt = new DataTable();
                        dt.Constraints.Clear();
                        var reader = await cmd.ExecuteReaderAsync();
                        dt.Load(reader);

                        strdt = dt.Rows[0][0].ToString();
                    }

                    return strdt;
                }
                finally
                {
                    //return dt;
                }
            }
            catch (Exception ex)
            {
                //throw;
                Utility.LogEx(ex);
                return string.Empty;
            }
        }


        public virtual async Task<IList<T>> ExecuteEntityList(string storeName, params SqlParameter[] parameters)
        {
            try
            {
                DataTable dt = await ExecuteDataTable(storeName, parameters);
                IList<T> list = new List<T>();
                foreach (DataRow row in dt.Rows)
                {
                    list.Add(row.ToEntity<T>());
                }

                return list;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex);
                throw;
            }
        }
    }
}
