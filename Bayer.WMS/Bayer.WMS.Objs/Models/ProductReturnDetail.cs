namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class ProductReturnDetail : BaseEntity
    {
        public ProductReturnDetail()
        {
            _status = ProductReturnHeader.status.New;
        }

        public ProductReturnDetail(dynamic p)
        {
            _productReturnCode = p.ProductReturnCode;
            _lineNbr = p.LineNbr;
            _productID = p.ProductID;
            _batchCode = p.BatchCode;
            _returnQty = p.ReturnQty;
            _deliveredQty = p.DeliveredQty;
            _description = p.Description;
            _status = p.Status;
            _createdBy = p.CreatedBy;
            _createdDateTime = p.CreatedDateTime;
            _createdBySitemapID = p.CreatedBySitemapID;
            _updatedBy = p.UpdatedBy;
            _updatedDateTime = p.UpdatedDateTime;
            _updatedBySitemapID = p.UpdatedBySitemapID;
            _rowVersion = p.RowVersion;

            _productCode = p.ProductCode;
            _productDescription = p.ProductDescription;
            PackageSize = p.PackageSize;
            UOM = p.UOM;
        }

        private string _productReturnCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string ProductReturnCode
        {
            get { return _productReturnCode; }
            set
            {
                _productReturnCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductReturnCode"));
            }
        }

        private int _lineNbr;

        [Key]
        [Column(Order = 1)]
        public int LineNbr
        {
            get { return _lineNbr; }
            set
            {
                _lineNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineNbr"));
            }
        }

        private int _productID;

        public int ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _batchCode;

        [StringLength(255)]
        public string BatchCode
        {
            get { return _batchCode; }
            set
            {
                _batchCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCode"));
            }
        }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        private decimal _returnQty;

        public decimal ReturnQty
        {
            get { return _returnQty; }
            set
            {
                _returnQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ReturnQty"));
            }
        }
        
        private decimal? _deliveredQty;

        public decimal? DeliveredQty
        {
            get { return _deliveredQty; }
            set
            {
                _deliveredQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DeliveredQty"));
            }
        }
        
        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay { get => ProductReturnHeader.status.GetDisplay(_status); }

        [NotMapped]
        public string UOM { get; set; }

        [NotMapped]
        public decimal? PackageSize { get; set; }
    }

    public class ProductReturnDetailConfiguration : EntityTypeConfiguration<ProductReturnDetail>
    {
        public ProductReturnDetailConfiguration()
        {
            ToTable("ProductReturnDetails");
        }
    }
}
