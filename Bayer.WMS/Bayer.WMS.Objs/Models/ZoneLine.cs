﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class ZoneLine : BaseEntity
    {
        public ZoneLine()
        {
            _status = status.Active;
            _type = type.Line;
            _isDeleted = false;
        }

        public ZoneLine(dynamic zoneLine)
        {
            _zoneCode = zoneLine.ZoneCode;
            _lineCode = zoneLine.LineCode;
            _lineName = zoneLine.LineName;
            _type = zoneLine.Type;
            _length = zoneLine.Length;
            _level = zoneLine.Level;
            _order = zoneLine.Order;
            _description = zoneLine.Description;
            _status = zoneLine.Status;
            _isDeleted = zoneLine.IsDeleted;
            _createdBy = zoneLine.CreatedBy;
            _createdBySitemapID = zoneLine.CreatedBySitemapID;
            _createdDateTime = zoneLine.CreatedDateTime;
            _updatedBy = zoneLine.CreatedBy;
            _updatedBySitemapID = zoneLine.UpdatedBySitemapID;
            _updatedDateTime = zoneLine.UpdatedDateTime;
            _rowVersion = zoneLine.RowVersion;
        }

        private string _zoneCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string ZoneCode
        {
            get { return _zoneCode; }
            set
            {
                _zoneCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ZoneCode"));
            }
        }

        private string _lineCode;

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_LineCode)]
        public string LineCode
        {
            get { return _lineCode; }
            set
            {
                _lineCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineCode"));
            }
        }

        private string _lineName;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_LineName)]
        public string LineName
        {
            get { return _lineName; }
            set
            {
                _lineName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineName"));
            }
        }

        private string _type;

        [Required]
        [StringLength(1)]
        public string Type
        {
            get { return _type; }
            set
            {
                _type = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Type"));
            }
        }

        public class type
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Line = "L";
            public const string Path = "P";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<type> Get()
            {
                return new List<type>
                {
                    new type { Value = "L", Display = "Dãy" },
                    new type { Value = "P", Display = "Đường đi" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Line:
                        display = "Dãy";
                        break;
                    case Path:
                        display = "Đường đi";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private int? _length;
        
        public int? Length
        {
            get { return _length; }
            set
            {
                _length = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Length"));
            }
        }

        private int? _level;

        public int? Level
        {
            get { return _level; }
            set
            {
                _level = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Level"));
            }
        }

        private int _order;

        public int Order
        {
            get { return _order; }
            set
            {
                _order = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Order"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Active = "A";
            public const string Inactive = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = "A", Display = "Đang hiệu lực" },
                    new status { Value = "I", Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Active:
                        display = "Đang hiệu lực";
                        break;
                    case Inactive:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }
    }

    public class ZoneLineConfiguration : EntityTypeConfiguration<ZoneLine>
    {
        public ZoneLineConfiguration()
        {
            Property(e => e.ZoneCode).IsUnicode(false);
            Property(e => e.LineCode).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
