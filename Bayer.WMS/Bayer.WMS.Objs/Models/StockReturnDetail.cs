namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class StockReturnDetail : BaseEntity
    {
        public StockReturnDetail()
        {
            _status = StockReturnHeader.status.New;
        }

        public StockReturnDetail(dynamic p)
        {
            _stockReturnCode = p.StockReturnCode;
            _lineNbr = p.LineNbr;
            _productID = p.ProductID;
            _batchCode = p.BatchCode;
            _returnQty = p.ReturnQty;
            _palletCode = p.PalletCode;
            _returnedFrom = p.ReturnedFrom;
            _receivedBy = p.ReceivedBy;
            _receivedQty = p.ReceivedQty;
            _receivedDate = p.ReceivedDate;
            _qA_QCCheck = p.QA_QCCheck;
            _reason = p.Reason;
            _description = p.Description;
            _status = p.Status;
            _createdBy = p.CreatedBy;
            _createdDateTime = p.CreatedDateTime;
            _createdBySitemapID = p.CreatedBySitemapID;
            _updatedBy = p.UpdatedBy;
            _updatedDateTime = p.UpdatedDateTime;
            _updatedBySitemapID = p.UpdatedBySitemapID;
            _rowVersion = p.RowVersion;

            _productCode = p.ProductCode;
            _productDescription = p.ProductDescription;
            PackageSize = p.PackageSize;
            UOM = p.UOM;
        }

        private string _stockReturnCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string StockReturnCode
        {
            get { return _stockReturnCode; }
            set
            {
                _stockReturnCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("StockReturnCode"));
            }
        }

        private int _lineNbr;

        [Key]
        [Column(Order = 1)]
        public int LineNbr
        {
            get { return _lineNbr; }
            set
            {
                _lineNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineNbr"));
            }
        }

        private int _productID;

        public int ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _batchCode;

        [StringLength(255)]
        public string BatchCode
        {
            get { return _batchCode; }
            set
            {
                _batchCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCode"));
            }
        }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        private decimal _returnQty;

        public decimal ReturnQty
        {
            get { return _returnQty; }
            set
            {
                _returnQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ReturnQty"));
            }
        }

        private string _palletCode;

        [StringLength(255)]
        public string PalletCode
        {
            get { return _palletCode; }
            set
            {
                _palletCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletCode"));
            }
        }

        private string _returnedFrom;

        [StringLength(255)]
        public string ReturnedFrom
        {
            get { return _returnedFrom; }
            set
            {
                _returnedFrom = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ReturnedFrom"));
            }
        }

        private decimal? _receivedQty;

        public decimal? ReceivedQty
        {
            get { return _receivedQty; }
            set
            {
                _receivedQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ReceivedQty"));
            }
        }

        private int _receivedBy;

        public int ReceivedBy

        {
            get { return _receivedBy; }
            set
            {
                _receivedBy = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ReceivedBy"));
            }
        }

        [NotMapped]
        public string StrReceivedBy { get; set; }

        private string _qA_QCCheck;

        public string QA_QCCheck
        {
            get { return _qA_QCCheck; }
            set
            {
                _qA_QCCheck = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("QA_QCCheck"));
            }
        }

        private DateTime? _receivedDate;

        [Column(TypeName = "date")]
        public DateTime? ReceivedDate
        {
            get { return _receivedDate; }
            set
            {
                _receivedDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ReceivedDate"));
            }
        }

        private string _reason;

        [StringLength(255)]
        public string Reason
        {
            get { return _reason; }
            set
            {
                _reason = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Reason"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay { get => StockReturnHeader.status.GetDisplay(_status); }

        [NotMapped]
        public string UOM { get; set; }

        [NotMapped]
        public decimal? PackageSize { get; set; }
    }

    public class StockReturnDetailConfiguration : EntityTypeConfiguration<StockReturnDetail>
    {
        public StockReturnDetailConfiguration()
        {
            ToTable("StockReturnDetails");
        }
    }
}
