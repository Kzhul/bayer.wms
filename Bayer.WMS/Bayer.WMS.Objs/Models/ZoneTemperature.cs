﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class ZoneTemperature : BaseEntity
    {
        public ZoneTemperature()
        {
        }

        public ZoneTemperature(dynamic zoneTemperature)
        {
            _zoneCode = zoneTemperature.ZoneCode;
            _groupCode = zoneTemperature.GroupCode;
            _refValue = zoneTemperature.RefValue;
            _referenceDescription = zoneTemperature.ReferenceDescription;
            _checked = zoneTemperature.Checked;
            _createdBy = zoneTemperature.CreatedBy;
            _createdBySitemapID = zoneTemperature.CreatedBySitemapID;
            _createdDateTime = zoneTemperature.CreatedDateTime;
            _updatedBy = zoneTemperature.CreatedBy;
            _updatedBySitemapID = zoneTemperature.UpdatedBySitemapID;
            _updatedDateTime = zoneTemperature.UpdatedDateTime;
            _rowVersion = zoneTemperature.RowVersion;
        }

        private string _zoneCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string ZoneCode
        {
            get { return _zoneCode; }
            set
            {
                _zoneCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ZoneCode"));
            }
        }

        private string _refValue;

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string RefValue
        {
            get { return _refValue; }
            set
            {
                _refValue = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RefValue"));
            }
        }

        private string _groupCode;

        [Key]
        [Column(Order = 2)]
        [StringLength(255)]
        public string GroupCode
        {
            get { return _groupCode; }
            set
            {
                _groupCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("GroupCode"));
            }
        }

        private string _referenceDescription;

        [NotMapped]
        public string ReferenceDescription
        {
            get { return _referenceDescription; }
            set
            {
                _referenceDescription = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ReferenceDescription"));
            }
        }

        private bool _checked;

        [NotMapped]
        public bool Checked
        {
            get { return _checked; }
            set
            {
                _checked = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Checked"));
            }
        }
    }

    public class ZoneTemperatureConfiguration : EntityTypeConfiguration<ZoneTemperature>
    {
        public ZoneTemperatureConfiguration()
        {
            Property(e => e.ZoneCode).IsUnicode(false);
            Property(e => e.RefValue).IsUnicode(false);
            Property(e => e.GroupCode).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
