﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RequestMaterial : BaseEntity
    {
        public RequestMaterial()
        {
            _status = status.New;
        }

        private string _documentNbr;

        [Key]
        [StringLength(255)]
        public string DocumentNbr
        {
            get { return _documentNbr; }
            set
            {
                _documentNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DocumentNbr"));
            }
        }

        private string _note;

        [StringLength(255)]
        public string Note
        {
            get { return _note; }
            set
            {
                _note = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Note"));
            }
        }

        private DateTime? _date;

        [Column(TypeName = "date")]
        public DateTime? Date
        {
            get { return _date; }
            set
            {
                _date = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Date"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string New = "N";
            public const string Processing = "P";
            public const string Completed = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = Processing, Display = "Đang xử lý" },
                    new status { Value = Completed, Display = "Đã hoàn tất" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case New:
                        display = "Mới";
                        break;
                    case Processing:
                        display = "Đang xử lý";
                        break;
                    case Completed:
                        display = "Đã hoàn tất";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }
    }
}
