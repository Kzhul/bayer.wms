namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class PackagingError : BaseEntity
    {
        public PackagingError()
        {

        }

        public PackagingError(string barcode, string encryptedBarcode, string type, string productLot, int? productID, 
            DateTime? packagingDate, DateTime? processDate, int? userID, string device, string note)
        {
            _barcode = barcode;
            _encryptedBarcode = encryptedBarcode;
            _type = type;
            _productLot = productLot;
            _productID = productID;
            _packagingDate = packagingDate;
            _processDate = processDate;
            _userID = userID;
            _device = device;
            _note = note;
        }

        private int _iD;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID
        {
            get { return _iD; }
            set
            {
                _iD = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ID"));
            }
        }

        private string _barcode;

        [StringLength(255)]
        public string Barcode
        {
            get { return _barcode; }
            set
            {
                _barcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Barcode"));
            }
        }

        private string _encryptedBarcode;

        [StringLength(255)]
        public string EncryptedBarcode
        {
            get { return _encryptedBarcode; }
            set
            {
                _encryptedBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("EncryptedBarcode"));
            }
        }

        private string _type;

        [StringLength(1)]
        public string Type
        {
            get { return _type; }
            set
            {
                _type = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Type"));
            }
        }

        public class type
        {
            public const string Carton = "C";
            public const string Product = "E";
        }

        private string _productLot;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_ProductionLot)]
        public string ProductLot
        {
            get { return _productLot; }
            set
            {
                _productLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductLot"));
            }
        }

        private int? _productID;

        public int? ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private DateTime? _packagingDate;

        public DateTime? PackagingDate
        {
            get { return _packagingDate; }
            set
            {
                _packagingDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PackagingDate"));
            }
        }

        private DateTime? _processDate;

        public DateTime? ProcessDate
        {
            get { return _processDate; }
            set
            {
                _processDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProcessDate"));
            }
        }

        private int? _userID;

        public int? UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UserID"));
            }
        }

        private string _device;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false)]
        public string Device
        {
            get { return _device; }
            set
            {
                _device = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Device"));
            }
        }

        private string _note;

        [StringLength(255)]
        public string Note
        {
            get { return _note; }
            set
            {
                _note = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Note"));
            }
        }
    }

    public class PackagingErrorConfiguration : EntityTypeConfiguration<PackagingError>
    {
        public PackagingErrorConfiguration()
        {
            Property(e => e.Barcode).IsUnicode(false);
            Property(e => e.EncryptedBarcode).IsUnicode(false);
            Property(e => e.Type).IsUnicode(false).IsFixedLength();
            Property(e => e.ProductLot).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
