﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class ProductBarcode : BaseEntity
    {
        public ProductBarcode()
        {
        }

        public ProductBarcode(string barcode, string productLot, int? productID, string status)
        {
            _barcode = barcode;
            _encryptedBarcode = $"SP{Utility.AESEncrypt(barcode)}";
            _productLot = productLot;
            _productID = productID;
            _status = status;
        }

        private string _barcode;

        [Key]
        [StringLength(255)]
        public string Barcode
        {
            get { return _barcode; }
            set
            {
                _barcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Barcode"));
            }
        }

        private string _encryptedBarcode;

        [StringLength(255)]
        public string EncryptedBarcode
        {
            get { return _encryptedBarcode; }
            set
            {
                _encryptedBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("EncryptedBarcode"));
            }
        }

        private string _productLot;

        [StringLength(255)]
        public string ProductLot
        {
            get { return _productLot; }
            set
            {
                _productLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductLot"));
            }
        }

        private int? _productID;

        public int? ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _cartonBarcode;

        [StringLength(255)]
        public string CartonBarcode
        {
            get { return _cartonBarcode; }
            set
            {
                _cartonBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CartonBarcode"));
            }
        }

        private string _palletCode;

        [StringLength(255)]
        public string PalletCode
        {
            get { return _palletCode; }
            set
            {
                _palletCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletCode"));
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string New = "N";
            public const string Packaged = "P";
            public const string Cancelled = "X";
            public const string Destroyed = "D";
            public const string TookSample = "S";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = Packaged, Display = "Đã đóng gói" },
                    new status { Value = Cancelled, Display = "Hư bỏ" },
                    new status { Value = Destroyed, Display = "Dư bỏ" },
                    new status { Value = TookSample, Display = "Đã lấy mẫu" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case New:
                        display = "Mới";
                        break;
                    case Packaged:
                        display = "Đã đóng gói";
                        break;
                    case Cancelled:
                        display = "Hư bỏ";
                        break;
                    case Destroyed:
                        display = "Dư bỏ";
                        break;
                    case TookSample:
                        display = "Đã lấy mẫu";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }
    }

    public class ProductBarcodeConfiguration : EntityTypeConfiguration<ProductBarcode>
    {
        public ProductBarcodeConfiguration()
        {
            Property(e => e.Barcode).IsUnicode(false);
            Property(e => e.EncryptedBarcode).IsUnicode(false);
            Property(e => e.ProductLot).IsUnicode(false);
            Property(e => e.CartonBarcode).IsUnicode(false);
            Property(e => e.PalletCode).IsUnicode(false);
            Property(e => e.Status).IsFixedLength().IsUnicode(false);
        }
    }
}
