namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public class PalletInfo
    {
        public Pallet pallet { get; set; }
        public PalletStatus palletStatus { get; set; }
    }

    public partial class DeliveryNoteDetail : BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string DOCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string ExportCode { get; set; }
        
        [Key]
        [Column(Order = 2)]
        public int ProductID { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(255)]
        public string BatchCode { get; set; }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

       

        public string BatchCodeDistributor { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? PackQuantity { get; set; }

        public string PackType { get; set; }

        [NotMapped]
        public string StrPackType
        {
            get { return PackType == "C" ? "T" : PackType; }
            set { }
        }

        public decimal? ProductQuantity { get; set; }

        public decimal? Shipper1 { get; set; }
        
        public decimal? Shipper2 { get; set; }
        
        public decimal? Shipper3 { get; set; }

        public int? Receiver1 { get; set; }
        
        public int? Receiver2 { get; set; }
        
        public int? Receiver3 { get; set; }

        [NotMapped]
        public string StrReceiver1 { get; set; }

        [NotMapped]
        public decimal? PackSize { get; set; }

        [NotMapped]
        public string StrReceiver2 { get; set; }

        [NotMapped]
        public string StrReceiver3 { get; set; }


        public decimal? ExportedQty { get; set; }
        public decimal? ConfirmQty { get; set; }
        public decimal? ReceivedQty { get; set; }
        public decimal? RequireReturnQty { get; set; }
        public decimal? ExportReturnedQty { get; set; }
        public decimal? ReceiveReturnedQty { get; set; }

        [NotMapped]
        public string PalletCodes { get; set; }


        [StringLength(1)]
        public string Status { get; set; }

        public DeliveryNoteDetail()
        {
        }

        public DeliveryNoteDetail(dynamic p)
        {            
            DOCode = p.DOCode;
            ExportCode = p.ExportCode;
            ProductID = p.ProductID;
            _productCode = p.ProductCode;                        
            _productDescription = p.ProductDescription;
            Quantity = p.Quantity;            
            PackQuantity = p.PackQuantity;
            PackType = p.PackType;
            PackSize = p.PackSize;
            ProductQuantity = p.ProductQuantity;
            BatchCode = p.BatchCode;

            Receiver1 = p.Receiver1;
            Receiver2 = p.Receiver2;
            Receiver3 = p.Receiver3;
            Shipper1 = p.Shipper1;
            Shipper2 = p.Shipper2;
            Shipper3 = p.Shipper3;

            ExportedQty = p.ExportedQty;
            ConfirmQty = p.ConfirmQty;
            ReceivedQty = p.ReceivedQty;
            RequireReturnQty = p.RequireReturnQty;
            ExportReturnedQty = p.ExportReturnedQty;
            ReceiveReturnedQty = p.ReceiveReturnedQty;

            Status = p.Status;
            _createdBy = p.CreatedBy;
            _createdDateTime = p.CreatedDateTime;
            _updatedBy = p.UpdatedBy;
            _updatedDateTime = p.UpdatedDateTime;
            _rowVersion = p.RowVersion;
        }
    }

    public class DeliveryNoteDetailConfiguration : EntityTypeConfiguration<DeliveryNoteDetail>
    {
        public DeliveryNoteDetailConfiguration()
        {
            ToTable("DeliveryNoteDetails");
        }
    }
}
