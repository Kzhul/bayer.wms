﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    [Table("BarcodeBank")]
    public partial class BarcodeBank : BaseEntity
    {
        public BarcodeBank()
        {

        }

        private string _barcode;

        [Key]
        [StringLength(255)]
        public string Barcode
        {
            get { return _barcode; }
            set
            {
                _barcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Barcode"));
            }
        }

        private int? _year;

        public int? Year
        {
            get { return _year; }
            set
            {
                _year = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Year"));
            }
        }

        private int? _sequence;

        public int? Sequence
        {
            get { return _sequence; }
            set
            {
                _sequence = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Sequence"));
            }
        }

        private bool _isExport;

        public bool IsExport
        {
            get { return _isExport; }
            set
            {
                _isExport = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsExport"));
            }
        }

    }

    public class BarcodeBankConfiguration : EntityTypeConfiguration<BarcodeBank>
    {
        public BarcodeBankConfiguration()
        {
            Property(e => e.Barcode).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
