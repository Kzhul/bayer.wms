namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    [Table("UOM")]
    public partial class UOM : BaseEntity
    {
        [Key]
        [Column("UOM")]
        [StringLength(255)]
        public string UOM1 { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public bool IsDeleted { get; set; }
    }

    public class UOMConfiguration : EntityTypeConfiguration<UOM>
    {
        public UOMConfiguration()
        {
            Property(e => e.UOM1).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
