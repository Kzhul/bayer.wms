﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class Reference : BaseEntity
    {
        public Reference()
        {
        }

        private string _groupCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string GroupCode
        {
            get { return _groupCode; }
            set
            {
                _groupCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("GroupCode"));
            }
        }

        private string _refValue;

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string RefValue
        {
            get { return _refValue; }
            set
            {
                _refValue = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RefValue"));
            }
        }

        private string _refName;

        [StringLength(255)]
        public string RefName
        {
            get { return _refName; }
            set
            {
                _refName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RefName"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private int? _sortID;

        public int? SortID
        {
            get { return _sortID; }
            set
            {
                _sortID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("SortID"));
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }
    }

    public class ReferenceConfiguration : EntityTypeConfiguration<Reference>
    {
        public ReferenceConfiguration()
        {
            Property(e => e.GroupCode).IsUnicode(false);
            Property(e => e.RefValue).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
