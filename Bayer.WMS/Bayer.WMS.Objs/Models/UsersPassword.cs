namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class UsersPassword
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int? UserID { get; set; }

        [StringLength(255)]
        public string Password { get; set; }

        public DateTime? LastUpdated { get; set; }
    }

    public class UsersPasswordConfiguration : EntityTypeConfiguration<UsersPassword>
    {
        public UsersPasswordConfiguration()
        {
            Property(e => e.Password).IsUnicode(false);
        }
    }
}
