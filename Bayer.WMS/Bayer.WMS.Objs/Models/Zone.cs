﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class Zone : BaseEntity
    {
        public Zone()
        {
            Status = status.A;
            IsDeleted = false;
        }

        public Zone(dynamic zone)
        {
            _zoneCode = zone.ZoneCode;
            _zoneName = zone.ZoneName;
            _description = zone.Description;
            _warehouseID = zone.WarehouseID;
            _warehouseDescription = zone.WarehouseDescription;
            _status = zone.Status;
            _isDeleted = zone.IsDeleted;
            _createdBy = zone.CreatedBy;
            _createdBySitemapID = zone.CreatedBySitemapID;
            _createdDateTime = zone.CreatedDateTime;
            _updatedBy = zone.CreatedBy;
            _updatedBySitemapID = zone.UpdatedBySitemapID;
            _updatedDateTime = zone.UpdatedDateTime;
            _rowVersion = zone.RowVersion;
        }

        private string _zoneCode;

        [Key]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_ZoneCode)]
        public string ZoneCode
        {
            get { return _zoneCode; }
            set
            {
                _zoneCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ZoneCode"));
            }
        }

        private string _zoneName;

        [StringLength(255)]
        public string ZoneName
        {
            get { return _zoneName; }
            set
            {
                _zoneName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ZoneName"));
            }
        }


        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private int? _warehouseID;

        [Required(ErrorMessage = Messages.Validate_Required_Warehouse)]
        public int? WarehouseID
        {
            get { return _warehouseID; }
            set
            {
                _warehouseID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("WarehouseID"));
            }
        }

        private string _warehouseDescription;

        [NotMapped]
        public string WarehouseDescription
        {
            get { return _warehouseDescription; }
            set { _warehouseDescription = value; }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string A = "A";
            public const string I = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = "A", Display = "Đang hiệu lực" },
                    new status { Value = "I", Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case A:
                        display = "Đang hiệu lực";
                        break;
                    case I:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }
    }

    public class ZoneConfiguration : EntityTypeConfiguration<Zone>
    {
        public ZoneConfiguration()
        {
            Property(e => e.ZoneCode).IsUnicode(false);
            Property(e => e.Status).IsFixedLength();
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
