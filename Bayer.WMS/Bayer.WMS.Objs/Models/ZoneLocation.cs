﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class ZoneLocation : BaseEntity
    {
        public ZoneLocation()
        {
            IsDeleted = false;
        }

        public ZoneLocation(dynamic zoneLocation)
        {
            _locationCode = zoneLocation.LocationCode;
            _zoneCode = zoneLocation.ZoneCode;
            _lineCode = zoneLocation.LineCode;
            _locationName = zoneLocation.LocationName;
            _lengthID = zoneLocation.LengthID;
            _levelID = zoneLocation.LevelID;
            _description = zoneLocation.Description;
            _currentPalletCode = zoneLocation.CurrentPalletCode;
            _status = zoneLocation.Status;
            _isDeleted = zoneLocation.IsDeleted;
            _createdBy = zoneLocation.CreatedBy;
            _createdBySitemapID = zoneLocation.CreatedBySitemapID;
            _createdDateTime = zoneLocation.CreatedDateTime;
            _updatedBy = zoneLocation.UpdatedBy;
            _updatedBySitemapID = zoneLocation.UpdatedBySitemapID;
            _updatedDateTime = zoneLocation.UpdatedDateTime;
            _rowVersion = zoneLocation.RowVersion;
        }

        private string _locationCode;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string LocationCode
        {
            get { return _locationCode; }
            set
            {
                _locationCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LocationCode"));
            }
        }

        private string _zoneCode;

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string ZoneCode
        {
            get { return _zoneCode; }
            set
            {
                _zoneCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ZoneCode"));
            }
        }

        private string _lineCode;

        [StringLength(255)]
        public string LineCode
        {
            get { return _lineCode; }
            set
            {
                _lineCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineCode"));
            }
        }

        private string _locationName;

        [StringLength(255)]
        public string LocationName
        {
            get { return _locationName; }
            set
            {
                _locationName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LocationName"));
            }
        }

        private int _lengthID;

        public int LengthID
        {
            get { return _lengthID; }
            set
            {
                _lengthID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LengthID"));
            }
        }

        private int _levelID;

        public int LevelID
        {
            get { return _levelID; }
            set
            {
                _levelID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LevelID"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _currentPalletCode;

        [StringLength(255)]
        public string CurrentPalletCode
        {
            get { return _currentPalletCode; }
            set
            {
                _currentPalletCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CurrentPalletCode"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Active = "A";
            public const string Inactive = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = "A", Display = "Đang hiệu lực" },
                    new status { Value = "I", Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Active:
                        display = "Đang hiệu lực";
                        break;
                    case Inactive:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }
    }

    public class ZoneLocationConfiguration : EntityTypeConfiguration<ZoneLocation>
    {
        public ZoneLocationConfiguration()
        {
            Property(e => e.LocationCode).IsUnicode(false);
            Property(e => e.ZoneCode).IsUnicode(false);
            Property(e => e.LineCode).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
