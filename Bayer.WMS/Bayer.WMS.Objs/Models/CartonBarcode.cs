﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class CartonBarcode : BaseEntity
    {
        public CartonBarcode()
        {
        }

        public CartonBarcode(string barcode, string productLot, string status)
        {
            _barcode = barcode;
            _productLot = productLot;
            _status = status;
        }

        private string _barcode;

        [Key]
        [StringLength(255)]
        public string Barcode
        {
            get { return _barcode; }
            set
            {
                _barcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Barcode"));
            }
        }

        private string _productLot;

        [StringLength(255)]
        public string ProductLot
        {
            get { return _productLot; }
            set
            {
                _productLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductLot"));
            }
        }

        private string _palletCode;

        [StringLength(255)]
        public string PalletCode
        {
            get { return _palletCode; }
            set
            {
                _palletCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletCode"));
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string New = "N";
            public const string Packaged = "P";
            public const string Cancelled = "X";
            public const string Destroyed = "D";
            public const string TookSample = "S";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = Packaged, Display = "Đã đóng gói" },
                    new status { Value = Cancelled, Display = "Hư bỏ" },
                     new status { Value = Destroyed, Display = "Dư bỏ" },
                    new status { Value = TookSample, Display = "Đã lấy mẫu" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case New:
                        display = "Mới";
                        break;
                    case Packaged:
                        display = "Đã đóng gói";
                        break;
                    case Cancelled:
                        display = "Hư bỏ";
                        break;
                    case Destroyed:
                        display = "Dư bỏ";
                        break;
                    case TookSample:
                        display = "Đã lấy mẫu";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }
    }

    public class CartonBarcodeConfiguration : EntityTypeConfiguration<CartonBarcode>
    {
        public CartonBarcodeConfiguration()
        {
            Property(e => e.Barcode).IsUnicode(false);
            Property(e => e.ProductLot).IsUnicode(false);
            Property(e => e.PalletCode).IsUnicode(false);
            Property(e => e.Status).IsFixedLength().IsUnicode(false);
        }
    }
}
