﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    public partial class Device : BaseEntity
    {
        public Device()
        {
            _status = status.Active;
        }
       
        private string _deviceCode;

        [Key]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_DeviceCode)]
        public string DeviceCode
        {
            get { return _deviceCode; }
            set
            {
                _deviceCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductCode"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _palletType;

        [StringLength(255)]
        public string PalletType
        {
            get { return _palletType; }
            set
            {
                _palletType = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletType"));
            }
        }

        private string _status;

        [StringLength(1)]
        [Required]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(_status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string Active = "A";
            public const string Inactive = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = Active, Display = "Đang hiệu lực" },
                    new status { Value = Inactive, Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case Active:
                        display = "Đang hiệu lực";
                        break;
                    case Inactive:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

    }

    public class DeviceConfiguration : EntityTypeConfiguration<Device>
    {
        public DeviceConfiguration()
        {
            Property(e => e.DeviceCode).IsUnicode(false);
            Property(e => e.PalletType).IsUnicode(false);
            Property(e => e.Status).IsFixedLength().IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
