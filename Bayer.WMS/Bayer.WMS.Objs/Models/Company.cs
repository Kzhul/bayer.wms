﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class Company : BaseEntity
    {
        [Key]
        [StringLength(30)]
        public string CompanyCode { get; set; }

        [StringLength(250)]
        public string CompanyName { get; set; }

        [StringLength(255)]
        public string CompanyAddress { get; set; }

        [StringLength(255)]
        public string ProvinceSoldTo { get; set; }

        [StringLength(255)]
        public string ProvinceShipTo { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public bool IsDeleted { get; set; }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string Active = "A";
            public const string InActive = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = Active, Display = "Đang hoạt động" },
                    new status { Value = InActive, Display = "Ngưng giao dịch" }
                };
            }

            public static string GetDisplay(string Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case Active:
                        display = "Đang hoạt động";
                        break;
                    case InActive:
                        display = "Ngưng giao dịch";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }
    }

    public class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
