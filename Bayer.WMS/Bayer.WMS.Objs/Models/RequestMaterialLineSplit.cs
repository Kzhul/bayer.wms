namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RequestMaterialLineSplit : BaseEntity
    {
        public RequestMaterialLineSplit()
        {
            _status = RequestMaterial.status.New;
        }

        public RequestMaterialLineSplit(dynamic reqMaterialLineSplit)
        {
            _documentNbr = reqMaterialLineSplit.DocumentNbr;
            _lineNbr = reqMaterialLineSplit.LineNbr;
            _productID = reqMaterialLineSplit.ProductID;
            _productCode = reqMaterialLineSplit.ProductCode;
            _productDescription = reqMaterialLineSplit.ProductDescription;
            _palletCode = reqMaterialLineSplit.PalletCode;
            _productLot = reqMaterialLineSplit.ProductLot;
            _quantity = reqMaterialLineSplit.Quantity;
            _status = reqMaterialLineSplit.Status;
            _createdBy = reqMaterialLineSplit.CreatedBy;
            _createdBySitemapID = reqMaterialLineSplit.CreatedBySitemapID;
            _createdDateTime = reqMaterialLineSplit.CreatedDateTime;
            _updatedBy = reqMaterialLineSplit.CreatedBy;
            _updatedBySitemapID = reqMaterialLineSplit.UpdatedBySitemapID;
            _updatedDateTime = reqMaterialLineSplit.UpdatedDateTime;
            _rowVersion = reqMaterialLineSplit.RowVersion;

        }

        private string _documentNbr;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string DocumentNbr
        {
            get { return _documentNbr; }
            set
            {
                _documentNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DocumentNbr"));
            }
        }

        private int _lineNbr;

        [Key]
        [Column(Order = 1)]
        public int LineNbr
        {
            get { return _lineNbr; }
            set
            {
                _lineNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineNbr"));
            }
        }

        private int? _productID;

        public int? ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        private string _palletCode;

        [Key]
        [Column(Order = 2)]
        public string PalletCode
        {
            get { return _palletCode; }
            set
            {
                _palletCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletCode"));
            }
        }

        private string _productLot;

        [Key]
        [Column(Order = 3)]
        public string ProductLot
        {
            get { return _productLot; }
            set
            {
                _productLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductLot"));
            }
        }

        private decimal? _quantity;

        [Range(typeof(Decimal), "0.00", "1000000000000000000", ErrorMessage = Messages.Validate_Range_RequestQty)]
        public decimal? Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Quantity"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return RequestMaterial.status.GetDisplay(Status); }
        }
    }
}
