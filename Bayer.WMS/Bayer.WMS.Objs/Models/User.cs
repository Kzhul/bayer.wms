﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;
    using System.Text.RegularExpressions;

    public partial class User : BaseEntity
    {
        public User()
        {
            Status = status.A;
            IsDeleted = false;
        }

        public DateTime? PINChangedDate { get; set; }        

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserID { get; set; }

        private string _username;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_Username)]
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Username"));
            }
        }

        private string _password;

        [DataType(DataType.Password)]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Match_Password)]
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Password"));
            }
        }

        private string _confirmPassword;

        [NotMapped]
        [DataType(DataType.Password)]
        [StringLength(255)]
        public string ConfirmPassword
        {
            get { return _confirmPassword; }
            set
            {
                _confirmPassword = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ConfirmPassword"));
            }
        }

        private string _firstName;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_FirstName)]
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("FirstName"));
            }
        }

        private string _lastName;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_LastName)]
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LastName"));
            }
        }

        private string _email;

        [StringLength(255)]
        [RegularExpression(@"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$", ErrorMessage = Messages.Validate_Format_Email)]
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Email"));
            }
        }

        private int? _sitemapID;

        public int? SitemapID
        {
            get { return _sitemapID; }
            set
            {
                _sitemapID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("SitemapID"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(Status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string A = "A";
            public const string I = "I";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = "A", Display = "Đang hiệu lực" },
                    new status { Value = "I", Display = "Không hiệu lực" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case A:
                        display = "Đang hiệu lực";
                        break;
                    case I:
                        display = "Không hiệu lực";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private bool _passwordChangeOnNextLogin;


        public bool PasswordChangeOnNextLogin
        {
            get { return _passwordChangeOnNextLogin; }
            set
            {
                _passwordChangeOnNextLogin = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PasswordChangeOnNextLogin"));
            }
        }

        private bool _isDeleted;

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsDeleted"));
            }
        }

        public bool ValidatePassword()
        {
            if (_password != _confirmPassword)
                throw new WrappedException(Messages.Validate_Match_Password);

            if (_password.Length < 8)
                throw new WrappedException(Messages.Validate_Policy_PasswordLength);

            if (_password.ToLower().Contains(_firstName.ToLower()) || _password.ToLower().Contains(_lastName.ToLower()))
                throw new WrappedException(Messages.Validate_Policy_PasswordContainName);

            int condition = 0;
            if (Regex.IsMatch(_password, "[!@#$%^&*]"))
                condition += 1;
            if (Regex.IsMatch(_password, "[a-z]"))
                condition += 1;
            if (Regex.IsMatch(_password, "[A-Z]"))
                condition += 1;
            if (Regex.IsMatch(_password, "[0-9]"))
                condition += 1;

            if (condition < 3)
                throw new WrappedException(Messages.Validate_Policy_PasswordNotComplicated);

            return true;
        }

        private string _pin;

        [DataType(DataType.Password)]
        [StringLength(255)]
        public string PIN
        {
            get { return _pin; }
            set
            {
                _pin = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PIN"));
            }
        }

        private string _confirmPIN;

        [NotMapped]
        [DataType(DataType.Password)]
        [StringLength(255)]
        public string ConfirmPIN
        {
            get { return _confirmPIN; }
            set
            {
                _confirmPIN = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ConfirmPIN"));
            }
        }

        public bool ValidatePIN()
        {
            if (_pin != _confirmPIN)
                throw new WrappedException(Messages.Validate_Match_PIN);

            if (_pin.Length != 4)
                throw new WrappedException(Messages.Validate_Policy_PINLength);

            int condition = 0;
            if (Regex.IsMatch(_pin, "[0-9]"))
                condition += 1;

            if (condition < 1)
                throw new WrappedException(Messages.Validate_Policy_PINNotComplicated);

            return true;
        }
    }

    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            Property(e => e.Username).IsUnicode(false);
            Property(e => e.Password).IsUnicode(false);
            Property(e => e.Email).IsUnicode(false);
            Property(e => e.Status).IsFixedLength();
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
