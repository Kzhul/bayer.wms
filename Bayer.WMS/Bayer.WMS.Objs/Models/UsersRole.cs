﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class UsersRole : BaseEntity
    {
        public UsersRole()
        {

        }

        public UsersRole(dynamic usersRole)
        {
            _userID = usersRole.UserID;
            _roleID = usersRole.RoleID;
            _roleName = usersRole.RoleName;
            _roleDescription = usersRole.RoleDescription;
            _checked = usersRole.Checked;
            _createdBy = usersRole.CreatedBy;
            _createdBySitemapID = usersRole.CreatedBySitemapID;
            _createdDateTime = usersRole.CreatedDateTime;
            _updatedBy = usersRole.CreatedBy;
            _updatedBySitemapID = usersRole.UpdatedBySitemapID;
            _updatedDateTime = usersRole.UpdatedDateTime;
            _rowVersion = usersRole.RowVersion;
        }

        private int _userID;

        [Key]
        [Column(Order = 0)]
        public int UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UserID"));
            }
        }

        private int _roleID;

        [Key]
        [Column(Order = 1)]
        public int RoleID
        {
            get { return _roleID; }
            set
            {
                _roleID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RoleID"));
            }
        }

        private string _roleName;

        [NotMapped]
        public string RoleName
        {
            get { return _roleName; }
            set { _roleName = value; }
        }

        private string _roleDescription;

        [NotMapped]
        public string RoleDescription
        {
            get { return _roleDescription; }
            set { _roleDescription = value; }
        }

        private bool _checked;

        [NotMapped]
        public bool Checked
        {
            get { return _checked; }
            set
            {
                _checked = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Checked"));
            }
        }
    }

    public class UsersRoleConfiguration : EntityTypeConfiguration<UsersRole>
    {
        public UsersRoleConfiguration()
        {
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
