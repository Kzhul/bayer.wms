﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public class StockReturnFull
    {
        public StockReturnHeader header { get; set; }
        public List<StockReturnDetail> listDetail { get; set; }
    }

    public partial class StockReturnHeader : BaseEntity
    {
        public StockReturnHeader()
        {
            _Date = DateTime.Today;
            _status = status.New;
        }

        public StockReturnHeader(dynamic header)
        {
            _StockReturnCode = header.StockReturnCode;
            _Date = header.Date;
            _description = header.Description;
            _status = header.Status;
            _createdByName = header.CreatedByName;
            _createdBy = header.CreatedBy;
            _createdDateTime = header.CreatedDateTime;
            _createdBySitemapID = header.CreatedBySitemapID;
            _updatedBy = header.CreatedBy;
            _updatedDateTime = header.UpdatedDateTime;
            _updatedBySitemapID = header.UpdatedBySitemapID;
            _rowVersion = header.RowVersion;
        }

        private string _StockReturnCode;

        [Key]
        [StringLength(255)]
        public string StockReturnCode
        {
            get { return _StockReturnCode; }
            set
            {
                _StockReturnCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("StockReturnCode"));
            }
        }

        private DateTime _Date;

        [Column(TypeName = "date")]
        public DateTime Date
        {
            get { return _Date; }
            set
            {
                _Date = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Date"));
            }
        }
        
        [NotMapped]
        public string StrDate { get => _Date.ToString("dd/MM/yyyy"); }
        
        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }
        
        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay { get => status.GetDisplay(_status); }

        public bool IsDeleted { get; set; }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string New = "N";
            public const string Processing = "P";
            public const string Completed = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = Processing, Display = "Đang xử lý" },
                    new status { Value = Completed, Display = "Đã hoàn tất" }
                };
            }

            public static string GetDisplay(string Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case New:
                        display = "Mới";
                        break;
                    case Processing:
                        display = "Đang xử lý";
                        break;
                    case Completed:
                        display = "Đã hoàn tất";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private string _createdByName;

        [NotMapped]
        public string CreatedByName
        {
            get => _createdByName;
            set => _createdByName = value;
        }
        public dynamic Header { get; }
    }

    public class StockReturnHeaderConfiguration : EntityTypeConfiguration<StockReturnHeader>
    {
        public StockReturnHeaderConfiguration()
        {
            ToTable("StockReturnHeaders");
        }
    }
}
