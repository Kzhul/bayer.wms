namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class PackagingForceEnd : BaseEntity
    {
        public PackagingForceEnd()
        {

        }

        public PackagingForceEnd(string productLot, int userID, string note)
        {
            _productLot = productLot;
            _userID = userID;
            _note = note;
        }

        private string _productLot;

        [Key]
        [StringLength(255)]
        public string ProductLot
        {
            get { return _productLot; }
            set
            {
                _productLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductLot"));
            }
        }

        private int? _userID;

        public int? UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UserID"));
            }
        }

        private string _note;

        [StringLength(255)]
        public string Note
        {
            get { return _note; }
            set
            {
                _note = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Note"));
            }
        }
    }

    public class PackagingForceEndConfiguration : EntityTypeConfiguration<PackagingForceEnd>
    {
        public PackagingForceEndConfiguration()
        {
            Property(e => e.ProductLot).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
