﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public class StockReceivingFull
    {
        public StockReceivingHeader header { get; set; }
        public List<StockReceivingDetail> listDetail { get; set; }
    }

    public partial class StockReceivingHeader : BaseEntity
    {
        public StockReceivingHeader()
        {
            _verifyStatus = status.New;
            _importStatus = status.New;
        }

        public StockReceivingHeader(dynamic header)
        {
            _stockReceivingCode = header.StockReceivingCode;
            _deliveryDate = header.DeliveryDate;
            _companyName = header.CompanyName;
            _truckNo = header.TruckNo;
            _driver = header.Driver;
            _description = header.Description;
            _materialType = header.MaterialType;
            _verifyStatus = header.VerifyStatus;
            _importStatus = header.ImportStatus;
            _createdByName = header.CreatedByName;
            _createdBy = header.CreatedBy;
            _createdDateTime = header.CreatedDateTime;
            _createdBySitemapID = header.CreatedBySitemapID;
            _updatedBy = header.CreatedBy;
            _updatedDateTime = header.UpdatedDateTime;
            _updatedBySitemapID = header.UpdatedBySitemapID;
            _rowVersion = header.RowVersion;
        }

        private string _stockReceivingCode;

        [Key]
        [StringLength(255)]
        public string StockReceivingCode
        {
            get { return _stockReceivingCode; }
            set
            {
                _stockReceivingCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("StockReceivingCode"));
            }
        }

        private DateTime _deliveryDate;

        [Column(TypeName = "date")]
        public DateTime DeliveryDate
        {
            get { return _deliveryDate; }
            set
            {
                _deliveryDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DeliveryDate"));
            }
        }

        private string _companyName;

        [StringLength(255)]
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                _companyName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CompanyName"));
            }
        }

        [NotMapped]
        public string StrDeliveryDate { get => _deliveryDate.ToString("dd/MM/yyyy"); }

        private string _truckNo;

        [StringLength(255)]
        public string TruckNo
        {
            get { return _truckNo; }
            set
            {
                _truckNo = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("TruckNo"));
            }
        }

        private string _driver;

        [StringLength(255)]
        public string Driver
        {
            get { return _driver; }
            set
            {
                _driver = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Driver"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }

        private string _materialType;

        [StringLength(1)]
        public string MaterialType
        {
            get { return _materialType; }
            set
            {
                _materialType = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("MaterialType"));
            }
        }

        private string _verifyStatus;

        [StringLength(1)]
        public string VerifyStatus
        {
            get { return _verifyStatus; }
            set
            {
                _verifyStatus = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("VerifyStatus"));
            }
        }

        [NotMapped]
        public string VerifyStatusDisplay { get => status.GetDisplay(_verifyStatus); }

        private string _importStatus;

        [StringLength(1)]
        public string ImportStatus
        {
            get { return _importStatus; }
            set
            {
                _importStatus = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ImportStatus"));
            }
        }

        [NotMapped]
        public string ImportStatusDisplay { get => status.GetDisplay(_importStatus); }

        public bool IsDeleted { get; set; }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";

            public const string New = "N";
            public const string Processing = "P";
            public const string Completed = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = Processing, Display = "Đang xử lý" },
                    new status { Value = Completed, Display = "Đã hoàn tất" }
                };
            }

            public static string GetDisplay(string Status)
            {
                string display = String.Empty;
                switch (Status)
                {
                    case New:
                        display = "Mới";
                        break;
                    case Processing:
                        display = "Đang xử lý";
                        break;
                    case Completed:
                        display = "Đã hoàn tất";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }

        private string _createdByName;

        [NotMapped]
        public string CreatedByName
        {
            get => _createdByName;
            set => _createdByName = value;
        }
        public dynamic Header { get; }
    }

    public class StockReceivingHeaderConfiguration : EntityTypeConfiguration<StockReceivingHeader>
    {
        public StockReceivingHeaderConfiguration()
        {
            ToTable("StockReceivingHeaders");
        }
    }
}
