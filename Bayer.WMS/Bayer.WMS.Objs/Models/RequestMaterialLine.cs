namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RequestMaterialLine : BaseEntity
    {
        public RequestMaterialLine()
        {
            _status = RequestMaterial.status.New;
        }

        public RequestMaterialLine(dynamic reqMaterialLine)
        {
            _documentNbr = reqMaterialLine.DocumentNbr;
            _lineNbr = reqMaterialLine.LineNbr;
            _productID = reqMaterialLine.ProductID;
            _productCode = reqMaterialLine.ProductCode;
            _productDescription = reqMaterialLine.ProductDescription;
            _requestQty = reqMaterialLine.RequestQty;
            _bookedQty = reqMaterialLine.BookedQty;
            _pickedQty = reqMaterialLine.PickedQty;
            _transferedQty = reqMaterialLine.TransferedQty;
            _uom = reqMaterialLine.UOM;
            _note = reqMaterialLine.Note;
            _status = reqMaterialLine.Status;
            _createdBy = reqMaterialLine.CreatedBy;
            _createdBySitemapID = reqMaterialLine.CreatedBySitemapID;
            _createdDateTime = reqMaterialLine.CreatedDateTime;
            _updatedBy = reqMaterialLine.CreatedBy;
            _updatedBySitemapID = reqMaterialLine.UpdatedBySitemapID;
            _updatedDateTime = reqMaterialLine.UpdatedDateTime;
            _rowVersion = reqMaterialLine.RowVersion;

        }

        private string _documentNbr;

        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string DocumentNbr
        {
            get { return _documentNbr; }
            set
            {
                _documentNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DocumentNbr"));
            }
        }

        private int _lineNbr;

        [Key]
        [Column(Order = 1)]
        public int LineNbr
        {
            get { return _lineNbr; }
            set
            {
                _lineNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineNbr"));
            }
        }

        private int? _productID;

        public int? ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        private decimal _requestQty;

        [Range(typeof(Decimal), "0.00", "1000000000000000000", ErrorMessage = Messages.Validate_Range_RequestQty)]
        public decimal RequestQty
        {
            get { return _requestQty; }
            set
            {
                _requestQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RequestQty"));
            }
        }

        private decimal? _bookedQty;
        public decimal? BookedQty
        {
            get { return _bookedQty; }
            set
            {
                _bookedQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BookedQty"));
            }
        }

        private decimal? _pickedQty;
        public decimal? PickedQty
        {
            get { return _pickedQty; }
            set
            {
                _pickedQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PickedQty"));
            }
        }

        private decimal? _transferedQty;

        [Range(typeof(Decimal), "0.00", "1000000000000000000", ErrorMessage = Messages.Validate_Range_TransferedQty)]
        public decimal? TransferedQty
        {
            get { return _transferedQty; }
            set
            {
                _transferedQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("TransferedQty"));
            }
        }

        private string _uom;

        [StringLength(255)]
        public string UOM
        {
            get { return _uom; }
            set
            {
                _uom = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UOM"));
            }
        }

        private string _note;

        [StringLength(255)]
        public string Note
        {
            get { return _note; }
            set
            {
                _note = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Note"));
            }
        }

        private string _status;

        [Required]
        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return RequestMaterial.status.GetDisplay(Status); }
        }
    }
}
