namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class PrepareNoteDetail : BaseEntity
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string DOCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string DeliveryCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(30)]
        public string PrepareCode { get; set; }

        [Key]
        [Column(Order = 3)]
        public int ProductID { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(30)]
        public string BatchCode { get; set; }
        
        public decimal? Quantity { get; set; }

        public decimal? PackQuantity { get; set; }

        [StringLength(30)]
        public string PackType { get; set; }

        [NotMapped]
        public string StrPackType
        {
            get { return PackType == "C" ? "T" : PackType; }
            set { }
        }

        [StringLength(30)]
        public string BatchCodeDistributor { get; set; }        

        public decimal? ProductQuantity { get; set; }

        public decimal? PreparedQty { get; set; }
        public decimal? DeliveredQty { get; set; }
        public decimal? RequireReturnQty { get; set; }
        public decimal? ReturnedQty { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        
        public decimal? PackSize { get; set; }
        public decimal? PackWeight { get; set; }
        
        public decimal? ProductCase { get; set; }
        public string ProductUnit { get; set; }
        public decimal? ProductWeight { get; set; }

        public int? UserPrepare { get; set; }
        [NotMapped]
        public string StrUserPrepare { get; set; }

        [NotMapped]
        public string StrUserVerify { get; set; }

        [StringLength(250)]
        public string UserVerify { get; set; }

        [NotMapped]
        public string PalletCodes { get; set; }

        public PrepareNoteDetail()
        {
        }

        public PrepareNoteDetail(dynamic p)
        {
            DOCode = p.DOCode;
            PrepareCode = p.PrepareCode;
            DeliveryCode = p.DeliveryCode;
            ProductID = p.ProductID;
            _productCode = p.ProductCode;
            _productDescription = p.ProductDescription;
            BatchCode = p.BatchCode;
            BatchCodeDistributor = p.BatchCodeDistributor;
            Quantity = p.Quantity;
            PackQuantity = p.PackQuantity;
            PackType = p.PackType;
            PackSize = p.PackSize;
            PackWeight = p.PackWeight;
            ProductQuantity = p.ProductQuantity;
            ProductCase = p.ProductCase;
            ProductUnit = p.ProductUnit;
            ProductWeight = p.ProductWeight;
            UserPrepare = p.UserPrepare;
            UserVerify = p.UserVerify;
            PreparedQty = p.PreparedQty;
            DeliveredQty = p.DeliveredQty;
            RequireReturnQty = p.RequireReturnQty;
            ReturnedQty = p.ReturnedQty;
            Status = p.Status;
            _createdBy = p.CreatedBy;
            CreatedBySitemapID = p.CreatedBySitemapID;
            _createdDateTime = p.CreatedDateTime;
            _updatedBy = p.UpdatedBy;
            UpdatedBySitemapID = p.UpdatedBySitemapID;
            _updatedDateTime = p.UpdatedDateTime;
            _rowVersion = p.RowVersion;
        }
    }

    public partial class PrepareNoteDetailReport
    {
        public string DOImportCode { get; set; }
        public string CompanyCode { get; set; }
        public string BatchCode { get; set; }
        public int ProductID { get; set; }
        public string ProductCode { get; set; }
        public string UOM { get; set; }
        public string ProductDescription { get; set; }
        public decimal DOQuantity { get; set; }
        public int PreparedQty { get; set; }
        public int DeliveredQty { get; set; }
        public int CartonQty { get; set; }
        public decimal PackageSize { get; set; }
        public int OddQty { get; set; }
        public int ProductQty { get; set; }
        public string PackingType { get; set; }
        public string OddPackingType { get; set; }
    }

    public class PrepareNoteDetailConfiguration : EntityTypeConfiguration<PrepareNoteDetail>
    {
        public PrepareNoteDetailConfiguration()
        {
            
        }
    }
}
