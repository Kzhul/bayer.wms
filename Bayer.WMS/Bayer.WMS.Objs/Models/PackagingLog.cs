namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class PackagingLog : BaseEntity
    {
        public PackagingLog()
        {

        }

        public PackagingLog(string barcode, string encryptedBarcode, string productLot, int? productID, DateTime date, 
            string cartonBarcode, string palletBarcode, int? userID, string device)
        {
            _barcode = barcode;
            _encryptedBarcode = encryptedBarcode;
            _productLot = productLot;
            _productID = productID;
            _date = date;
            _cartonBarcode = cartonBarcode;
            _palletBarcode = palletBarcode;
            _userID = userID;
            _device = device;
        }

        public PackagingLog(string barcode, string encryptedBarcode, string productLot, int? productID, DateTime date,
           string cartonBarcode, string palletBarcode, int? userID, string device, decimal? qty)
        {
            _barcode = barcode;
            _encryptedBarcode = encryptedBarcode;
            _productLot = productLot;
            _productID = productID;
            _date = date;
            _cartonBarcode = cartonBarcode;
            _palletBarcode = palletBarcode;
            _userID = userID;
            _device = device;
            _qty = qty;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        private string _barcode;

        
        [StringLength(255)]
        public string Barcode
        {
            get { return _barcode; }
            set
            {
                _barcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Barcode"));
            }
        }

        private string _encryptedBarcode;

        [StringLength(255)]
        public string EncryptedBarcode
        {
            get { return _encryptedBarcode; }
            set
            {
                _encryptedBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("EncryptedBarcode"));
            }
        }

        private string _productLot;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_ProductionLot)]
        public string ProductLot
        {
            get { return _productLot; }
            set
            {
                _productLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductLot"));
            }
        }

        private int? _productID;

        [Required(ErrorMessage = Messages.Validate_Required_Product)]
        public int? ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private DateTime? _date;

        public DateTime? Date
        {
            get { return _date; }
            set
            {
                _date = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Date"));
            }
        }

        private string _cartonBarcode;

        [StringLength(255)]
        public string CartonBarcode
        {
            get { return _cartonBarcode; }
            set
            {
                _cartonBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CartonBarcode"));
            }
        }

        private string _palletBarcode;

        [StringLength(255)]
        public string PalletBarcode
        {
            get { return _palletBarcode; }
            set
            {
                _palletBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PalletBarcode"));
            }
        }

        private int? _userID;

        public int? UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UserID"));
            }
        }

        private decimal? _qty;

        public decimal? Qty
        {
            get { return _qty; }
            set
            {
                _qty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Qty"));
            }
        }

        private string _device;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false)]
        public string Device
        {
            get { return _device; }
            set
            {
                _device = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Device"));
            }
        }
    }

    public class PackagingLogConfiguration : EntityTypeConfiguration<PackagingLog>
    {
        public PackagingLogConfiguration()
        {
            Property(e => e.Barcode).IsUnicode(false);
            Property(e => e.EncryptedBarcode).IsUnicode(false);
            Property(e => e.ProductLot).IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
