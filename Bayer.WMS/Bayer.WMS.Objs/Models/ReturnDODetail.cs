namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class ReturnDODetail : BaseEntity
    {
        public ReturnDODetail()
        {
        }

        public ReturnDODetail(string delivery, int productID, string productCode, string productDescription, string batchCode, decimal? quantity, string companyCode, string companyName, string province, DateTime? deliveryDate, string status, string batchCodeDistributor)
        {
            _delivery = delivery;
            _productID = productID;
            _batchCode = batchCode;
            _quantity = quantity;
            _companyCode = companyCode;
            _companyName = companyName;
            _province = province;
            _deliveryDate = deliveryDate;
            _status = status;
            _productDescription = productDescription;
            _productCode = productCode;
            _batchCodeDistributor = batchCodeDistributor;
        }

        public ReturnDODetail(dynamic p)
        {
            ReturnDOCode = p.ReturnDOCode;
            DOImportCode = p.DOImportCode;
            _lineNbr = p.LineNbr;
            _delivery = p.Delivery;
            _productID = p.ProductID;
            _productCode = p.ProductCode;
            _productDescription = p.ProductDescription;
            _requestReturnQty = p.RequestReturnQty;
            _quantity = p.Quantity;
            Description = p.Description;
            _batchCode = p.BatchCode;
            _batchCodeDistributor = p.BatchCodeDistributor;
            _companyCode = p.CompanyCode;
            _companyName = p.CompanyName;
            _province = p.Province;
            _deliveryDate = p.DeliveryDate;
            _status = p.Status;
            _createdBy = p.CreatedBy;
            _createdDateTime = p.CreatedDateTime;
            _updatedBy = p.UpdatedBy;
            _updatedDateTime = p.UpdatedDateTime;
            _rowVersion = p.RowVersion;
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string ReturnDOCode { get; set; }
        
        [StringLength(20)]
        public string DOImportCode { get; set; }

        private int _lineNbr;

        [Key]
        [Column(Order = 1)]
        //[StringLength(255)]
        public int LineNbr
        {
            get { return _lineNbr; }
            set
            {
                _lineNbr = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("LineNbr"));
            }
        }

        private string _delivery;

        [StringLength(255)]
        public string Delivery
        {
            get { return _delivery; }
            set
            {
                _delivery = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Delivery"));
            }
        }

        private int _productID;

        public int ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        private string _batchCode;

        [StringLength(50)]
        public string BatchCode
        {
            get { return _batchCode; }
            set
            {
                _batchCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCode"));
            }
        }

        private string _batchCodeDistributor;

        [StringLength(50)]
        public string BatchCodeDistributor
        {
            get { return _batchCodeDistributor; }
            set
            {
                _batchCodeDistributor = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("BatchCodeDistributor"));
            }
        }

        private decimal? _quantity;

        public decimal? Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Quantity"));
            }
        }

        private string _companyCode;

        [StringLength(50)]
        public string CompanyCode
        {
            get { return _companyCode; }
            set
            {
                _companyCode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CompanyCode"));
            }
        }

        private string _companyName;

        [StringLength(255)]
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                _companyName = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CompanyName"));
            }
        }

        private string _province;

        [StringLength(255)]
        public string Province
        {
            get { return _province; }
            set
            {
                _province = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Province"));
            }
        }

        private DateTime? _deliveryDate;

        [Column(TypeName = "date")]
        public DateTime? DeliveryDate
        {
            get { return _deliveryDate; }
            set
            {
                _deliveryDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("DeliveryDate"));
            }
        }

        [NotMapped]
        public string StrDeliveryDate
        {
            get { return DeliveryDate.HasValue ? DeliveryDate.Value.ToString("dd/MM/yyyy") : DateTime.Today.ToString("dd/MM/yyyy"); }
            set
            {

            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        //public bool IsDeleted { get; set; }

        private decimal? _requestReturnQty;

        public decimal? RequestReturnQty
        {
            get { return _requestReturnQty; }
            set
            {
                _requestReturnQty = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("RequestReturnQty"));
            }
        }

        private string _description;

        [StringLength(255)]
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Description"));
            }
        }
    }

    public class ReturnDODetailConfiguration : EntityTypeConfiguration<ReturnDODetail>
    {
        public ReturnDODetailConfiguration()
        {
            ToTable("ReturnDODetails");
        }
    }
}
