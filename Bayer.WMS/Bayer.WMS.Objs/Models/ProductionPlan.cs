﻿namespace Bayer.WMS.Objs.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.Spatial;

    public partial class ProductionPlan : BaseEntity
    {
        public ProductionPlan()
        {

        }

        public ProductionPlan(dynamic productionPlan)
        {
            _productLot = productionPlan.ProductLot;
            _supplierLot = productionPlan.SupplierLot;
            _device = productionPlan.Device;
            _pONumber = productionPlan.PONumber;
            _productID = productionPlan.ProductID;
            _productCode = productionPlan.ProductCode;
            _productDescription = productionPlan.ProductDescription;
            _productPackingType = productionPlan.ProductPackingType;
            _quantity = productionPlan.Quantity;
            _planDate = productionPlan.PlanDate;
            _manufacturingDate = productionPlan.ManufacturingDate;
            _expiryDate = productionPlan.ExpiryDate;
            _packageQuantity = productionPlan.PackageQuantity;
            _packageSize = productionPlan.PackageSize;
            _uom = productionPlan.UOM;
            _sequence = productionPlan.Sequence;
            _note = productionPlan.Note;
            _isGenProductBarcode = productionPlan.IsGenProductBarcode;
            _isGenCartonBarcode = productionPlan.IsGenCartonBarcode;
            _currentLabelCartonBarcode = productionPlan.CurrentLabelCartonBarcode;
            _maxLabelCartonBarcode = productionPlan.MaxLabelCartonBarcode;
            _packagingStartTime = productionPlan.PackagingStartTime;
            _packagingEndTime = productionPlan.PackagingEndTime;
            _status = productionPlan.Status;
            _createdBy = productionPlan.CreatedBy;
            _createdDateTime = productionPlan.CreatedDateTime;
            _updatedBy = productionPlan.CreatedBy;
            _updatedDateTime = productionPlan.UpdatedDateTime;
            _rowVersion = productionPlan.RowVersion;
        }

        public ProductionPlan(string productLot, string supplierLot, string device, string pONumber, int? productID, decimal quantity,
            DateTime planDate, DateTime? manufacturingDate, DateTime? expiryDate, decimal packageQuantity, decimal packageSize, string uom, int sequence, string note,
            bool isGenProductBarcode, bool isGenCartonBarcode, int? currentLabelCartonBarcode, int? maxLabelCartonBarcode, string status)
        {
            _productLot = productLot.Trim();
            _supplierLot = supplierLot?.Trim();
            _device = device.Trim();
            _pONumber = pONumber.Trim();
            _productID = productID;
            _quantity = quantity;
            _planDate = planDate;
            _manufacturingDate = manufacturingDate;
            _expiryDate = expiryDate;
            _packageQuantity = packageQuantity;
            _packageSize = packageSize;
            _uom = uom;
            _sequence = sequence;
            _note = note;
            _isGenProductBarcode = isGenProductBarcode;
            _isGenCartonBarcode = isGenCartonBarcode;
            _currentLabelCartonBarcode = currentLabelCartonBarcode;
            _maxLabelCartonBarcode = maxLabelCartonBarcode;
            _status = status;
        }

        private string _productLot;

        [Key]
        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_ProductionLot)]
        public string ProductLot
        {
            get { return _productLot; }
            set
            {
                _productLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductLot"));
            }
        }

        private string _supplierLot;

        [StringLength(255)]
        public string SupplierLot
        {
            get { return _supplierLot; }
            set
            {
                _supplierLot = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("SupplierLot"));
            }
        }

        private string _device;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_Device)]
        public string Device
        {
            get { return _device; }
            set
            {
                _device = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Device"));
            }
        }

        private string _pONumber;

        [StringLength(255)]
        [Required(AllowEmptyStrings = false, ErrorMessage = Messages.Validate_Required_PONumber)]
        public string PONumber
        {
            get { return _pONumber; }
            set
            {
                _pONumber = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PONumber"));
            }
        }

        private int? _productID;

        [Required(ErrorMessage = Messages.Validate_Required_Product)]
        public int? ProductID
        {
            get { return _productID; }
            set
            {
                _productID = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ProductID"));
            }
        }

        private string _productCode;

        [NotMapped]
        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private string _productDescription;

        [NotMapped]
        public string ProductDescription
        {
            get { return _productDescription; }
            set { _productDescription = value; }
        }

        private string _productPackingType;

        [NotMapped]
        public string ProductPackingType
        {
            get { return _productPackingType; }
            set { _productPackingType = value; }
        }

        private decimal? _cartonPackingQty;

        [NotMapped]
        public decimal? CartonPackingQty
        {
            get { return _cartonPackingQty; }
            set { _cartonPackingQty = value; }
        }

        private decimal? _palletPackingQty;

        [NotMapped]
        public decimal? PalletPackingQty
        {
            get { return _palletPackingQty; }
            set { _palletPackingQty = value; }
        }

        private string _cartonWeight;

        [NotMapped]
        public string CartonWeight
        {
            get { return _cartonWeight; }
            set { _cartonWeight = value; }
        }

        private decimal? _actualPackingQty;

        [NotMapped]
        public decimal? ActualPackingQty
        {
            get { return _actualPackingQty; }
            set { _actualPackingQty = value; }
        }

        [NotMapped]
        public string ActualPackingQtyOnPlan
        {
            get { return $"{_actualPackingQty ?? 0:N0} / {_quantity ?? 0:N0}"; }
        }

        [NotMapped]
        public int ProductSampleQty { get; set; }

        [NotMapped]
        public int TookProductSampleQty { get; set; }

        [NotMapped]
        public int TookCartonSampleQty { get; set; }

        [NotMapped]
        public string StrTookProductSampleProgress
        {
            get { return $"{TookProductSampleQty:N0} / {ProductSampleQty:N0}"; }
        }

        [NotMapped]
        public string StrTookCartonSampleProgress
        {
            get
            {
                if (TookCartonSampleQty == -1)
                    return "0 / 0";
                return $"{TookCartonSampleQty:N0} / 1";
            }
        }

        private decimal? _quantity;

        [Required(ErrorMessage = Messages.Validate_Required_Quantity)]
        [Range(typeof(Decimal), "0.00", "1000000000000000000", ErrorMessage = Messages.Validate_Range_Quantity)]
        public decimal? Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Quantity"));
            }
        }

        private DateTime? _planDate;

        [Column(TypeName = "date")]
        [Required(ErrorMessage = Messages.Validate_Required_PlanDate)]
        public DateTime? PlanDate
        {
            get { return _planDate; }
            set
            {
                _planDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PlanDate"));
            }
        }

        private DateTime? _manufacturingDate;

        [Column(TypeName = "date")]
        [Required(ErrorMessage = Messages.Validate_Required_MFG)]
        public DateTime? ManufacturingDate
        {
            get { return _manufacturingDate; }
            set
            {
                _manufacturingDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ManufacturingDate"));
            }
        }

        private DateTime? _expiryDate;

        [Column(TypeName = "date")]
        [Required(ErrorMessage = Messages.Validate_Required_EXP)]
        public DateTime? ExpiryDate
        {
            get { return _expiryDate; }
            set
            {
                _expiryDate = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("ExpiryDate"));
            }
        }

        private decimal? _packageQuantity;

        [Required(ErrorMessage = Messages.Validate_Required_PackageQuantity)]
        [Range(typeof(Decimal), "0.00", "1000000000000000000", ErrorMessage = Messages.Validate_Range_PackageQuantity)]
        public decimal? PackageQuantity
        {
            get { return _packageQuantity; }
            set
            {
                _packageQuantity = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PackageQuantity"));
            }
        }

        private decimal? _packageSize;

        [Required(ErrorMessage = Messages.Validate_Required_PackageSize)]
        [Range(typeof(Decimal), "0.0000", "1000000000000000000.0000", ErrorMessage = Messages.Validate_Range_PackageSize)]
        public decimal? PackageSize
        {
            get { return _packageSize; }
            set
            {
                _packageSize = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PackageSize"));
            }
        }

        private string _uom;

        public string UOM
        {
            get { return _uom; }
            set
            {
                _uom = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("UOM"));
            }
        }

        private int? _sequence;

        public int? Sequence
        {
            get { return _sequence; }
            set
            {
                _sequence = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Sequence"));
            }
        }

        private string _note;

        public string Note
        {
            get { return _note; }
            set
            {
                _note = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Note"));
            }
        }

        private bool _isGenProductBarcode;

        public bool IsGenProductBarcode
        {
            get { return _isGenProductBarcode; }
            set
            {
                _isGenProductBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        private bool _isGenCartonBarcode;

        public bool IsGenCartonBarcode
        {
            get { return _isGenCartonBarcode; }
            set
            {
                _isGenCartonBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("IsGenCartonBarcode"));
            }
        }

        private int? _currentLabelCartonBarcode;

        public int? CurrentLabelCartonBarcode
        {
            get { return _currentLabelCartonBarcode; }
            set
            {
                _currentLabelCartonBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("CurrentLabelCartonBarcode"));
            }
        }

        private int? _maxLabelCartonBarcode;

        public int? MaxLabelCartonBarcode
        {
            get { return _maxLabelCartonBarcode; }
            set
            {
                _maxLabelCartonBarcode = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("MaxLabelCartonBarcode"));
            }
        }

        private DateTime? _packagingStartTime;

        public DateTime? PackagingStartTime
        {
            get { return _packagingStartTime; }
            set
            {
                _packagingStartTime = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PackagingStartTime"));
            }
        }

        private DateTime? _packagingEndTime;

        public DateTime? PackagingEndTime
        {
            get { return _packagingEndTime; }
            set
            {
                _packagingEndTime = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("PackagingEndTime"));
            }
        }

        private string _status;

        [StringLength(1)]
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                InvokePropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        [NotMapped]
        public string StatusDisplay
        {
            get { return status.GetDisplay(_status); }
        }

        public class status
        {
            public const string ValueMember = "Value";
            public const string DisplayMember = "Display";
            public const string New = "N";
            public const string Hold = "H";
            public const string InProgress = "I";
            public const string Packaged = "P";
            public const string Destroying = "D";
            public const string Completed = "C";

            public string Value { get; set; }

            public string Display { get; set; }

            public static List<status> Get()
            {
                return new List<status>
                {
                    new status { Value = New, Display = "Mới" },
                    new status { Value = Hold, Display = "Giữ" },
                    new status { Value = InProgress, Display = "Đang đóng gói" },
                    new status { Value = Packaged, Display = "Đã đóng gói" },
                    new status { Value = Destroying, Display = "Đang hủy bì dư" },
                    new status { Value = Completed, Display = "Đã hoàn tất" }
                };
            }

            public static string GetDisplay(string value)
            {
                string display = String.Empty;
                switch (value)
                {
                    case New:
                        display = "Mới";
                        break;
                    case Hold:
                        display = "Giữ";
                        break;
                    case InProgress:
                        display = "Đang đóng gói";
                        break;
                    case Packaged:
                        display = "Đã đóng gói";
                        break;
                    case Destroying:
                        display = "Đang hủy bì dư";
                        break;
                    case Completed:
                        display = "Đã hoàn tất";
                        break;
                    default:
                        break;
                }

                return display;
            }
        }
    }

    public class ProductionPlanConfiguration : EntityTypeConfiguration<ProductionPlan>
    {
        public ProductionPlanConfiguration()
        {
            Property(e => e.ProductLot).IsUnicode(false);
            Property(e => e.Device).IsUnicode(false);
            Property(e => e.PONumber).IsUnicode(false);
            Property(e => e.PackageSize).HasPrecision(18, 4);
            Property(e => e.Status).IsFixedLength().IsUnicode(false);
            Property(e => e.RowVersion).IsFixedLength();
        }
    }
}
