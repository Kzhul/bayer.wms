﻿using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Bayer.WMS.Objs
{
    public partial interface IUnitOfWork : IDisposable
    {
        IBaseRepository Register(Type repositoryType);

        IBaseRepository GetRepository(string typeName);

        Task Commit();

        Task Commit2();

        //IDbTransaction GetTransaction();
    }
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Dictionary<string, IBaseRepository> _repositories;
        private readonly BayerWMSContext _context;
        private readonly ObjectContext _objectContext;
        private TransactionScope _scope;
        private IDbTransaction _tran;

        public UnitOfWork(IBayerWMSContext context, bool isTransactiopnScope)
        {
            _repositories = new Dictionary<string, IBaseRepository>();
            _context = context as BayerWMSContext;
            _objectContext = ((IObjectContextAdapter)this._context).ObjectContext;


            if (_objectContext.Connection.State != ConnectionState.Open)
                _objectContext.Connection.Open();

            if (isTransactiopnScope)
                _scope = new TransactionScope();
            else
                _tran = _objectContext.Connection.BeginTransaction();
        }

        public IBaseRepository Register(Type repositoryType)
        {
            IBaseRepository baseRepository;
            if (!_repositories.ContainsKey(repositoryType.Name))
            {
                baseRepository = Activator.CreateInstance(repositoryType, new object[] { _context }) as IBaseRepository;
                _repositories.Add(repositoryType.Name, baseRepository);
            }
            else
            {
                baseRepository = _repositories[repositoryType.Name];
            }

            return baseRepository;
        }

        public IBaseRepository GetRepository(string typeName)
        {
            if (_repositories.ContainsKey(typeName))
                return _repositories[typeName];
            return null;
        }

        public async Task Commit()
        {
            try
            {
                await _context.SaveChangesAsync();
                //_transaction.Commit();

                _scope.Complete();
                _scope.Dispose();
            }
            catch (Exception)
            {
                Rollback();
                throw;
            }
        }

        public async Task Commit2()
        {
            try
            {
                await _context.SaveChangesAsync();
                _tran.Commit();
            }
            catch (Exception)
            {
                Rollback2();
                throw;
            }
        }

        private void Rollback()
        {
            //_transaction.Rollback();

            foreach (var entry in _context.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        private void Rollback2()
        {
            _tran.Rollback();

            foreach (var entry in _context.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        public void Dispose()
        {
            if (_objectContext.Connection.State == ConnectionState.Open)
            {
                _objectContext.Connection.Close();
            }

            if (_scope != null)
            {
                _scope.Dispose();
                _scope = null;
            }

            if(_tran!=null)
            {
                _tran.Dispose();
                _tran = null;
            }

            GC.SuppressFinalize(this);
        }

        //public IDbTransaction GetTransaction()
        //{
        //    return _transaction;
        //}
    }
}
