﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Bayer.WMS.Objs.Utility;

namespace Bayer.WMS.Cof.Presenters
{
    public interface IMainView : IBaseView
    {
        Color MessageColor { set; }

        string Message { set; }

        IList<Sitemap> Sitemaps { set; }

        Task RefreshPresenter(int sitemapID);

        object Resolve(Type type, params ResolverOverride[] resolverOverride);
    }

    public interface IMainPresenter : IBasePresenter
    {
        void RegisterSitemap(string key, Sitemap sitemap);

        void UnregisterSitemap(string key);

        Sitemap GetSitemapRegistered(string key);
    }

    public class MainPresenter : BasePresenter, IMainPresenter
    {
        private readonly IMainView _mainView;
        private readonly ISitemapRepository _sitemapRepository;
        private IDictionary<string, Sitemap> _sitemapRegister;

        public MainPresenter(IMainView view, IUnitOfWorkManager unitOfWorkManager)
            : base(view, unitOfWorkManager)
        {
            _mainView = view;
            _sitemapRepository = new SitemapRepository(_unitOfWorkManager.CurrentContext);
            _sitemapRegister = new Dictionary<string, Sitemap>();
        }

        public void RegisterSitemap(string key, Sitemap sitemap)
        {
            _sitemapRegister.Add(key, sitemap);
        }

        public void UnregisterSitemap(string key)
        {
            _sitemapRegister.Remove(key);
        }

        public Sitemap GetSitemapRegistered(string key)
        {
            return _sitemapRegister[key];
        }

        public override async Task Refresh(int sitemapID)
        {
            await _mainView.RefreshPresenter(sitemapID);
        }

        public override void SetMessage(string message, MessageType messageType, string parent = "")
        {
            switch (messageType)
            {
                case MessageType.Warning:
                    _mainView.MessageColor = Color.YellowGreen;
                    break;
                case MessageType.Error:
                    _mainView.MessageColor = Color.Red;
                    break;
                case MessageType.Information:
                    _mainView.MessageColor = Color.Blue;
                    break;
                default:
                    _mainView.MessageColor = Color.Black;
                    break;
            }
            _mainView.Message = message;
        }

        public override object Resolve(Type type, params ResolverOverride[] resolverOverride)
        {
            return _mainView.Resolve(type, resolverOverride);
        }
    }
}
