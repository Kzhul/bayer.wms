﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS.Cof.Presenters
{
    public interface IRoleMaintView : IBaseView
    {
        DataTable Roles { set; }

        Role Role { get; set; }

        IList<RoleSitemap> RoleSitemaps { get; set; }
    }

    public interface IRoleMaintPresenter : IBasePresenter
    {
        Task LoadRoles();

        Task LoadRoleSitemaps(int roleID = 0);
    }

    public class RoleMaintPresenter : BasePresenter, IRoleMaintPresenter
    {
        private IRoleMaintView _mainView;
        private IRoleRepository _roleRepository;
        private IRoleSiteMapRepository _roleSiteMapRepository;

        public RoleMaintPresenter(IRoleMaintView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IRoleRepository roleRepository, IRoleSiteMapRepository roleSiteMapRepository)
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;

            _roleRepository = roleRepository;
            _roleSiteMapRepository = roleSiteMapRepository;
        }

        /// <summary>
        /// Event trigger when user select item in combobox
        /// </summary>
        /// <param name="selectedItem"></param>
        public async void OnRolenameSelectChange(DataRow selectedItem)
        {
            try
            {
                var role = selectedItem.ToEntity<Role>();
                await LoadRoleSitemaps(role.RoleID);

                role.State = EntityState.Unchanged;
                _mainView.Role = role;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get roles from database for combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadRoles()
        {
            try
            {
                var Roles = await _roleRepository.GetAsync(p => !p.IsDeleted);
                _mainView.Roles = Roles.ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get role-sitemaps from database for datagridview
        /// </summary>
        /// <returns></returns>
        public async Task LoadRoleSitemaps(int roleID = 0)
        {
            try
            {
                var roleSitemaps = await _roleSiteMapRepository.GetWithAllSitemapsAsync(p => p.RoleID == roleID);
                var sortedRoleSitemaps = new List<RoleSitemap>();

                //// sort role sitemaps to display on screen
                foreach (var item in roleSitemaps.Where(p => !p.SitemapParentID.HasValue).OrderBy(p => p.SitemapOrder))
                {
                    item.SitemapLevel = 0;
                    item.SitemapTree = item.SitemapOrder.ToString();
                    sortedRoleSitemaps.Add(item);

                    Stack<RoleSitemap> stackSitemap = new Stack<RoleSitemap>();
                    stackSitemap.Push(item);

                    while (stackSitemap.Count > 0)
                    {
                        var siteMap = stackSitemap.Pop();

                        foreach (var subItem in roleSitemaps.Where(p => p.SitemapParentID == siteMap.SitemapID).OrderBy(p => p.SitemapOrder))
                        {
                            subItem.SitemapLevel = siteMap.SitemapLevel + 1;
                            subItem.SitemapTree = $"{siteMap.SitemapTree}{subItem.SitemapOrder}";
                            for (int i = 0; i < subItem.SitemapLevel; i++)
                            {
                                subItem.SitemapDescription = $"     {subItem.SitemapDescription}";
                            }

                            stackSitemap.Push(subItem);
                            sortedRoleSitemaps.Add(subItem);
                        }
                    }
                }

                _mainView.RoleSitemaps = sortedRoleSitemaps.OrderBy(p => p.SitemapTree).ToList();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async void Insert()
        {
            try
            {
                _mainView.Role = new Role();
                await LoadRoleSitemaps();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var role = _mainView.Role;
            var roleSitemaps = _mainView.RoleSitemaps;

            bool isSuccess = false;
            try
            {
                using (var unitOfWork = (_mainPresenter.Resolve(_unitOfWorkManager.GetType()) as IUnitOfWorkManager).NewUnitOfWork())
                {
                    _roleRepository = unitOfWork.Register(_roleRepository.GetType()) as IRoleRepository;
                    _roleSiteMapRepository = unitOfWork.Register(_roleSiteMapRepository.GetType()) as IRoleSiteMapRepository;

                    role.IsRecordAuditTrail = true;
                    if (role.RoleID == 0)
                    {
                        await _roleRepository.Insert(role);

                        //// Commit to get new role id
                        await _roleRepository.Commit();
                    }
                    else
                        await _roleRepository.Update(role, new object[] { role.RoleID });

                    foreach (var item in roleSitemaps)
                    {
                        item.RoleID = role.RoleID;
                        item.IsRecordAuditTrail = true;
                        await _roleSiteMapRepository.InsertOrUpdate(item, new object[] { item.RoleID, item.SitemapID });

                    }

                    await unitOfWork.Commit();
                }

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing product is updated by another Role
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    _mainPresenter.SetMessage(message, Utility.MessageType.Error);
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _roleRepository = _mainPresenter.Resolve(_roleRepository.GetType()) as IRoleRepository;
                _roleSiteMapRepository = _mainPresenter.Resolve(_roleSiteMapRepository.GetType()) as IRoleSiteMapRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadRoles();

                    _mainView.Role = role;
                    _mainView.RoleSitemaps = roleSitemaps;
                }
            }
        }
    }
}
