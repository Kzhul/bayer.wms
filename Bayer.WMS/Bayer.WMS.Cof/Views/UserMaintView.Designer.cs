﻿namespace Bayer.WMS.Cof.Views
{
    partial class UserMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.dtgRole = new System.Windows.Forms.DataGridView();
            this.colRole_Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colRole_RoleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRole_RoleDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsUsersRole = new System.Windows.Forms.BindingSource(this.components);
            this.cmbUsername = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbSitemapID = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSitemapID_ReadOnly = new System.Windows.Forms.TextBox();
            this.txtStatus_ReadOnly = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtConfirmPIN = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPIN = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtgRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsUsersRole)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên đăng nhập";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 101);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 129);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Họ && chữ lót";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(138, 98);
            this.txtFirstName.MaxLength = 255;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(200, 22);
            this.txtFirstName.TabIndex = 3;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(138, 126);
            this.txtLastName.MaxLength = 255;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(200, 22);
            this.txtLastName.TabIndex = 4;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(138, 154);
            this.txtEmail.MaxLength = 255;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(400, 22);
            this.txtEmail.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 157);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 215);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Trạng thái";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(138, 212);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(200, 24);
            this.cmbStatus.TabIndex = 7;
            this.cmbStatus.SelectedValueChanged += new System.EventHandler(this.cmbStatus_SelectedValueChanged);
            // 
            // dtgRole
            // 
            this.dtgRole.AllowUserToAddRows = false;
            this.dtgRole.AllowUserToDeleteRows = false;
            this.dtgRole.AllowUserToOrderColumns = true;
            this.dtgRole.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgRole.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgRole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgRole.AutoGenerateColumns = false;
            this.dtgRole.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgRole.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgRole.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colRole_Check,
            this.colRole_RoleName,
            this.colRole_RoleDescription});
            this.dtgRole.DataSource = this.bdsUsersRole;
            this.dtgRole.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dtgRole.GridColor = System.Drawing.SystemColors.Control;
            this.dtgRole.Location = new System.Drawing.Point(12, 268);
            this.dtgRole.Name = "dtgRole";
            this.dtgRole.Size = new System.Drawing.Size(926, 257);
            this.dtgRole.TabIndex = 8;
            this.dtgRole.CurrentCellDirtyStateChanged += new System.EventHandler(this.dtgRole_CurrentCellDirtyStateChanged);
            // 
            // colRole_Check
            // 
            this.colRole_Check.DataPropertyName = "Checked";
            this.colRole_Check.HeaderText = "";
            this.colRole_Check.Name = "colRole_Check";
            this.colRole_Check.Width = 50;
            // 
            // colRole_RoleName
            // 
            this.colRole_RoleName.DataPropertyName = "RoleName";
            this.colRole_RoleName.HeaderText = "Tên vai trò";
            this.colRole_RoleName.Name = "colRole_RoleName";
            this.colRole_RoleName.ReadOnly = true;
            this.colRole_RoleName.Width = 200;
            // 
            // colRole_RoleDescription
            // 
            this.colRole_RoleDescription.DataPropertyName = "RoleDescription";
            this.colRole_RoleDescription.HeaderText = "Mô tả";
            this.colRole_RoleDescription.Name = "colRole_RoleDescription";
            this.colRole_RoleDescription.ReadOnly = true;
            this.colRole_RoleDescription.Width = 400;
            // 
            // cmbUsername
            // 
            this.cmbUsername.Columns = null;
            this.cmbUsername.DropDownHeight = 1;
            this.cmbUsername.DropDownWidth = 500;
            this.cmbUsername.FormattingEnabled = true;
            this.cmbUsername.IntegralHeight = false;
            this.cmbUsername.Location = new System.Drawing.Point(138, 12);
            this.cmbUsername.MaxLength = 255;
            this.cmbUsername.Name = "cmbUsername";
            this.cmbUsername.PageSize = 0;
            this.cmbUsername.PresenterInfo = null;
            this.cmbUsername.Size = new System.Drawing.Size(400, 24);
            this.cmbUsername.Source = null;
            this.cmbUsername.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(13, 249);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 16);
            this.label8.TabIndex = 31;
            this.label8.Text = "Danh sách vai trò";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(138, 42);
            this.txtPassword.MaxLength = 255;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(400, 22);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 45);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 16);
            this.label6.TabIndex = 36;
            this.label6.Text = "Mật khẩu";
            // 
            // cmbSitemapID
            // 
            this.cmbSitemapID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSitemapID.FormattingEnabled = true;
            this.cmbSitemapID.Location = new System.Drawing.Point(138, 182);
            this.cmbSitemapID.Name = "cmbSitemapID";
            this.cmbSitemapID.Size = new System.Drawing.Size(400, 24);
            this.cmbSitemapID.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 185);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 16);
            this.label7.TabIndex = 38;
            this.label7.Text = "Màn hình mặc định";
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Location = new System.Drawing.Point(138, 70);
            this.txtConfirmPassword.MaxLength = 255;
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.Size = new System.Drawing.Size(400, 22);
            this.txtConfirmPassword.TabIndex = 2;
            this.txtConfirmPassword.UseSystemPasswordChar = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 73);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 16);
            this.label9.TabIndex = 40;
            this.label9.Text = "Nhập lại mật khẩu";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(544, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 16);
            this.label10.TabIndex = 41;
            this.label10.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(344, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 16);
            this.label11.TabIndex = 42;
            this.label11.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(344, 129);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 16);
            this.label12.TabIndex = 43;
            this.label12.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(544, 45);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 16);
            this.label13.TabIndex = 44;
            this.label13.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(544, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(13, 16);
            this.label14.TabIndex = 45;
            this.label14.Text = "*";
            // 
            // txtSitemapID_ReadOnly
            // 
            this.txtSitemapID_ReadOnly.Location = new System.Drawing.Point(138, 182);
            this.txtSitemapID_ReadOnly.MaxLength = 255;
            this.txtSitemapID_ReadOnly.Multiline = true;
            this.txtSitemapID_ReadOnly.Name = "txtSitemapID_ReadOnly";
            this.txtSitemapID_ReadOnly.ReadOnly = true;
            this.txtSitemapID_ReadOnly.Size = new System.Drawing.Size(400, 24);
            this.txtSitemapID_ReadOnly.TabIndex = 46;
            // 
            // txtStatus_ReadOnly
            // 
            this.txtStatus_ReadOnly.Location = new System.Drawing.Point(138, 212);
            this.txtStatus_ReadOnly.MaxLength = 255;
            this.txtStatus_ReadOnly.Multiline = true;
            this.txtStatus_ReadOnly.Name = "txtStatus_ReadOnly";
            this.txtStatus_ReadOnly.ReadOnly = true;
            this.txtStatus_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtStatus_ReadOnly.TabIndex = 47;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(579, 43);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 16);
            this.label15.TabIndex = 51;
            this.label15.Text = "Nhập lại mã PIN";
            // 
            // txtConfirmPIN
            // 
            this.txtConfirmPIN.Location = new System.Drawing.Point(704, 40);
            this.txtConfirmPIN.MaxLength = 255;
            this.txtConfirmPIN.Name = "txtConfirmPIN";
            this.txtConfirmPIN.Size = new System.Drawing.Size(95, 22);
            this.txtConfirmPIN.TabIndex = 49;
            this.txtConfirmPIN.UseSystemPasswordChar = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(579, 15);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 16);
            this.label16.TabIndex = 50;
            this.label16.Text = "Mã PIN";
            // 
            // txtPIN
            // 
            this.txtPIN.Location = new System.Drawing.Point(704, 12);
            this.txtPIN.MaxLength = 255;
            this.txtPIN.Name = "txtPIN";
            this.txtPIN.Size = new System.Drawing.Size(95, 22);
            this.txtPIN.TabIndex = 48;
            this.txtPIN.UseSystemPasswordChar = true;
            // 
            // UserMaintView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 537);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtConfirmPIN);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtPIN);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtConfirmPassword);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dtgRole);
            this.Controls.Add(this.cmbUsername);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbSitemapID);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.txtSitemapID_ReadOnly);
            this.Controls.Add(this.txtStatus_ReadOnly);
            this.Name = "UserMaintView";
            this.Text = "UserMaintView";
            ((System.ComponentModel.ISupportInitialize)(this.dtgRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsUsersRole)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbStatus;
        private CustomControls.MultiColumnComboBox cmbUsername;
        private System.Windows.Forms.DataGridView dtgRole;
        private System.Windows.Forms.BindingSource bdsUsersRole;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbSitemapID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colRole_Check;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRole_RoleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRole_RoleDescription;
        private System.Windows.Forms.TextBox txtSitemapID_ReadOnly;
        private System.Windows.Forms.TextBox txtStatus_ReadOnly;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtConfirmPIN;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPIN;
    }
}