﻿using Bayer.WMS.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs;
using Bayer.WMS.CustomControls;
using System.Data.Entity;
using Bayer.WMS.Cof.Presenters;
using System.Reflection;

namespace Bayer.WMS.Cof.Views
{
    public partial class RoleMaintView : BaseForm, IRoleMaintView
    {
        public RoleMaintView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgSitemap);
        }

        private void dtgSitemap_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dtgSitemap.EndEdit();
            if (dtgSitemap.IsCurrentCellDirty)
            {
                dtgSitemap.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        public override void InitializeComboBox()
        {
            cmbRoleName.PageSize = 20;
            cmbRoleName.ValueMember = "RoleID";
            cmbRoleName.DisplayMember = "RoleName";
            cmbRoleName.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("RoleName", "Tên vai trò", 150),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbRoleName.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "GetRoles", "OnRolenameSelectChange");

            colRoleSitemap_AccessRights.ValueMember = RoleSitemap.accessRights.ValueMember;
            colRoleSitemap_AccessRights.DisplayMember = RoleSitemap.accessRights.DisplayMember;
            colRoleSitemap_AccessRights.DataSource = RoleSitemap.accessRights.Get();
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var roleMaintPresenter = _presenter as IRoleMaintPresenter;

                await roleMaintPresenter.LoadRoles();

                if (!isRefresh)
                    roleMaintPresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DataTable Roles
        {
            set { cmbRoleName.Source = value; }
        }

        private IList<RoleSitemap> _roleSiteMaps;

        public IList<RoleSitemap> RoleSitemaps
        {
            get { return _roleSiteMaps; }
            set
            {
                _roleSiteMaps = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsRoleSiteMap.DataSource = _roleSiteMaps;
                    });
                }
                else
                {
                    bdsRoleSiteMap.DataSource = _roleSiteMaps;
                }
            }
        }

        private Role _role;

        public Role Role
        {
            get { return _role; }
            set
            {
                _role = value;

                cmbRoleName.DataBindings.Clear();
                txtDescription.DataBindings.Clear();

                cmbRoleName.DataBindings.Add("Text", Role, "RoleName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", Role, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    txtDescription.ReadOnly = true;
                    dtgSitemap.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }
    }
}
