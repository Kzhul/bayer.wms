﻿namespace Bayer.WMS.Cof.Views
{
    partial class CompanyView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsRoleSiteMap = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbRoleName = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCompanyAddress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtProvinceShipTo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.rdbActive = new System.Windows.Forms.RadioButton();
            this.rdbInactive = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dtgProductPacking = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtProvinceSoldTo = new System.Windows.Forms.TextBox();
            this.CompanyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompanyAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProvinceSoldTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colProvinceShipTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatusDisplay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRoleSiteMap)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 40);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 16);
            this.label4.TabIndex = 39;
            this.label4.Text = "Tên công ty";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(118, 39);
            this.txtCompanyName.MaxLength = 255;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(400, 22);
            this.txtCompanyName.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 16);
            this.label3.TabIndex = 35;
            this.label3.Text = "Mã công ty";
            // 
            // cmbRoleName
            // 
            this.cmbRoleName.Columns = null;
            this.cmbRoleName.DropDownHeight = 1;
            this.cmbRoleName.DropDownWidth = 500;
            this.cmbRoleName.FormattingEnabled = true;
            this.cmbRoleName.IntegralHeight = false;
            this.cmbRoleName.Location = new System.Drawing.Point(118, 9);
            this.cmbRoleName.MaxLength = 255;
            this.cmbRoleName.Name = "cmbRoleName";
            this.cmbRoleName.PageSize = 0;
            this.cmbRoleName.PresenterInfo = null;
            this.cmbRoleName.Size = new System.Drawing.Size(400, 24);
            this.cmbRoleName.Source = null;
            this.cmbRoleName.TabIndex = 45;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(524, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 16);
            this.label7.TabIndex = 46;
            this.label7.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(524, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 16);
            this.label1.TabIndex = 47;
            this.label1.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(524, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 16);
            this.label2.TabIndex = 50;
            this.label2.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 68);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 49;
            this.label5.Text = "Địa chỉ";
            // 
            // txtCompanyAddress
            // 
            this.txtCompanyAddress.Location = new System.Drawing.Point(118, 67);
            this.txtCompanyAddress.MaxLength = 255;
            this.txtCompanyAddress.Name = "txtCompanyAddress";
            this.txtCompanyAddress.Size = new System.Drawing.Size(400, 22);
            this.txtCompanyAddress.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(524, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 16);
            this.label6.TabIndex = 53;
            this.label6.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 124);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 16);
            this.label8.TabIndex = 52;
            this.label8.Text = "Tỉnh thành (Ship)";
            // 
            // txtProvinceShipTo
            // 
            this.txtProvinceShipTo.Location = new System.Drawing.Point(118, 123);
            this.txtProvinceShipTo.MaxLength = 255;
            this.txtProvinceShipTo.Name = "txtProvinceShipTo";
            this.txtProvinceShipTo.Size = new System.Drawing.Size(400, 22);
            this.txtProvinceShipTo.TabIndex = 51;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 152);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 16);
            this.label10.TabIndex = 55;
            this.label10.Text = "Mô tả";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(118, 151);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(400, 22);
            this.txtDescription.TabIndex = 54;
            // 
            // rdbActive
            // 
            this.rdbActive.AutoSize = true;
            this.rdbActive.Checked = true;
            this.rdbActive.Location = new System.Drawing.Point(118, 180);
            this.rdbActive.Name = "rdbActive";
            this.rdbActive.Size = new System.Drawing.Size(121, 20);
            this.rdbActive.TabIndex = 57;
            this.rdbActive.TabStop = true;
            this.rdbActive.Text = "Đang hoạt động";
            this.rdbActive.UseVisualStyleBackColor = true;
            // 
            // rdbInactive
            // 
            this.rdbInactive.AutoSize = true;
            this.rdbInactive.Location = new System.Drawing.Point(245, 179);
            this.rdbInactive.Name = "rdbInactive";
            this.rdbInactive.Size = new System.Drawing.Size(124, 20);
            this.rdbInactive.TabIndex = 56;
            this.rdbInactive.Text = "Ngưng giao dịch";
            this.rdbInactive.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 180);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 16);
            this.label9.TabIndex = 58;
            this.label9.Text = "Trạng thái";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1000, 603);
            this.tabControl1.TabIndex = 59;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.txtProvinceSoldTo);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.txtCompanyName);
            this.tabPage1.Controls.Add(this.rdbActive);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.rdbInactive);
            this.tabPage1.Controls.Add(this.cmbRoleName);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtDescription);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtCompanyAddress);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtProvinceShipTo);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(992, 574);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Chi tiết";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtgProductPacking);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(992, 574);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Danh sách ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dtgProductPacking
            // 
            this.dtgProductPacking.AllowUserToAddRows = false;
            this.dtgProductPacking.AllowUserToDeleteRows = false;
            this.dtgProductPacking.AllowUserToOrderColumns = true;
            this.dtgProductPacking.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgProductPacking.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgProductPacking.AutoGenerateColumns = false;
            this.dtgProductPacking.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductPacking.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyCode,
            this.CompanyName,
            this.colCompanyAddress,
            this.colProvinceSoldTo,
            this.colProvinceShipTo,
            this.colStatusDisplay,
            this.colDescription});
            this.dtgProductPacking.DataSource = this.bdsRoleSiteMap;
            this.dtgProductPacking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgProductPacking.GridColor = System.Drawing.SystemColors.Control;
            this.dtgProductPacking.Location = new System.Drawing.Point(3, 3);
            this.dtgProductPacking.Name = "dtgProductPacking";
            this.dtgProductPacking.ReadOnly = true;
            this.dtgProductPacking.Size = new System.Drawing.Size(986, 568);
            this.dtgProductPacking.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(524, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 16);
            this.label11.TabIndex = 61;
            this.label11.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 98);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(108, 16);
            this.label12.TabIndex = 60;
            this.label12.Text = "Tỉnh thành (Sold)";
            // 
            // txtProvinceSoldTo
            // 
            this.txtProvinceSoldTo.Location = new System.Drawing.Point(118, 95);
            this.txtProvinceSoldTo.MaxLength = 255;
            this.txtProvinceSoldTo.Name = "txtProvinceSoldTo";
            this.txtProvinceSoldTo.Size = new System.Drawing.Size(400, 22);
            this.txtProvinceSoldTo.TabIndex = 59;
            // 
            // CompanyCode
            // 
            this.CompanyCode.DataPropertyName = "CompanyCode";
            this.CompanyCode.HeaderText = "Mã Công Ty";
            this.CompanyCode.Name = "CompanyCode";
            this.CompanyCode.ReadOnly = true;
            // 
            // CompanyName
            // 
            this.CompanyName.DataPropertyName = "CompanyName";
            this.CompanyName.HeaderText = "Tên Công Ty";
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.ReadOnly = true;
            this.CompanyName.Width = 300;
            // 
            // colCompanyAddress
            // 
            this.colCompanyAddress.DataPropertyName = "CompanyAddress";
            this.colCompanyAddress.HeaderText = "Địa chỉ";
            this.colCompanyAddress.Name = "colCompanyAddress";
            this.colCompanyAddress.ReadOnly = true;
            this.colCompanyAddress.Width = 200;
            // 
            // colProvinceSoldTo
            // 
            this.colProvinceSoldTo.DataPropertyName = "ProvinceSoldTo";
            this.colProvinceSoldTo.HeaderText = "Tỉnh thành (Sold To)";
            this.colProvinceSoldTo.Name = "colProvinceSoldTo";
            this.colProvinceSoldTo.ReadOnly = true;
            this.colProvinceSoldTo.Width = 200;
            // 
            // colProvinceShipTo
            // 
            this.colProvinceShipTo.HeaderText = "Tỉnh thành (Ship To)";
            this.colProvinceShipTo.Name = "colProvinceShipTo";
            this.colProvinceShipTo.ReadOnly = true;
            this.colProvinceShipTo.Width = 200;
            // 
            // colStatusDisplay
            // 
            this.colStatusDisplay.DataPropertyName = "StatusDisplay";
            this.colStatusDisplay.HeaderText = "Trạng thái";
            this.colStatusDisplay.Name = "colStatusDisplay";
            this.colStatusDisplay.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.DataPropertyName = "Description";
            this.colDescription.HeaderText = "Mô tả";
            this.colDescription.Name = "colDescription";
            this.colDescription.ReadOnly = true;
            this.colDescription.Width = 200;
            // 
            // CompanyView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 603);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "CompanyView";
            this.Text = "CompanyView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsRoleSiteMap)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductPacking)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label label3;
        private CustomControls.MultiColumnComboBox cmbRoleName;
        private System.Windows.Forms.BindingSource bdsRoleSiteMap;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCompanyAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtProvinceShipTo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.RadioButton rdbActive;
        private System.Windows.Forms.RadioButton rdbInactive;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dtgProductPacking;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtProvinceSoldTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompanyAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProvinceSoldTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProvinceShipTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatusDisplay;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDescription;
    }
}