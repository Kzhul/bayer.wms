﻿using Bayer.WMS.Cof.Presenters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Bayer.WMS.CustomControls;
using Bayer.WMS.Base;
using Bayer.WMS.Objs;
using System.Reflection;

namespace Bayer.WMS.Cof.Views
{
    public partial class UserMaintView : BaseForm, IUserMaintView
    {
        public UserMaintView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgRole);
        }

        public override void InitializeComboBox()
        {
            cmbSitemapID.ValueMember = "SitemapID";
            cmbSitemapID.DisplayMember = "Description";

            cmbStatus.ValueMember = User.status.ValueMember;
            cmbStatus.DisplayMember = User.status.DisplayMember;
            cmbStatus.DataSource = User.status.Get();

            cmbUsername.PageSize = 20;
            cmbUsername.ValueMember = "UserID";
            cmbUsername.DisplayMember = "Username";
            cmbUsername.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("Username", "Tên đăng nhập", 150),
                new MultiColumnComboBox.ComboBoxColumn("FirstName", "Tên", 150),
                new MultiColumnComboBox.ComboBoxColumn("LastName", "Họ & chữ lót", 150),
                new MultiColumnComboBox.ComboBoxColumn("Email", "Email", 200),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbUsername.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "GetUsers", "OnUsernameSelectChange");
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var userMaintPresenter = _presenter as IUserMaintPresenter;

                await Task.WhenAll(userMaintPresenter.LoadUsers(), userMaintPresenter.LoadSitemaps());

                if (!isRefresh)
                    userMaintPresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        private void cmbSitemapID_SelectedValueChanged(object sender, EventArgs e)
        {
            txtSitemapID_ReadOnly.Text = cmbSitemapID.Text;
        }

        private void cmbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            txtStatus_ReadOnly.Text = cmbStatus.Text;
        }

        private void dtgRole_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            dtgRole.EndEdit();
            if (dtgRole.IsCurrentCellDirty)
            {
                dtgRole.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        public DataTable Users
        {
            set { cmbUsername.Source = value; }
        }

        public IList<Sitemap> Sitemaps
        {
            set
            {
                cmbSitemapID.SelectedValueChanged -= cmbSitemapID_SelectedValueChanged;

                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        cmbSitemapID.DataSource = value;
                    });
                }
                else
                {
                    cmbSitemapID.DataSource = value;
                }

                cmbSitemapID.SelectedValueChanged += cmbSitemapID_SelectedValueChanged;
            }
        }

        private IList<UsersRole> _usersRoles;

        public IList<UsersRole> UsersRoles
        {
            get { return _usersRoles; }
            set
            {
                _usersRoles = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsUsersRole.DataSource = _usersRoles;
                    });
                }
                else
                {
                    bdsUsersRole.DataSource = _usersRoles;
                }
            }
        }

        private User _user;

        public User User
        {
            get { return _user; }
            set
            {
                _user = value;

                cmbUsername.DataBindings.Clear();
                txtPassword.DataBindings.Clear();
                txtConfirmPassword.DataBindings.Clear();
                txtFirstName.DataBindings.Clear();
                txtLastName.DataBindings.Clear();
                txtEmail.DataBindings.Clear();
                cmbSitemapID.DataBindings.Clear();
                cmbStatus.DataBindings.Clear();
                txtPIN.DataBindings.Clear();
                txtConfirmPIN.DataBindings.Clear();

                cmbUsername.DataBindings.Add("Text", User, "Username", true, DataSourceUpdateMode.OnPropertyChanged);
                txtPassword.DataBindings.Add("Text", User, "Password", true, DataSourceUpdateMode.OnPropertyChanged);
                txtConfirmPassword.DataBindings.Add("Text", User, "ConfirmPassword", true, DataSourceUpdateMode.OnPropertyChanged);
                txtFirstName.DataBindings.Add("Text", User, "FirstName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtLastName.DataBindings.Add("Text", User, "LastName", true, DataSourceUpdateMode.OnPropertyChanged);
                txtEmail.DataBindings.Add("Text", User, "Email", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbSitemapID.DataBindings.Add("SelectedValue", User, "SitemapID", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbStatus.DataBindings.Add("SelectedValue", User, "Status", true, DataSourceUpdateMode.OnPropertyChanged);

                txtPIN.DataBindings.Add("Text", User, "PIN", true, DataSourceUpdateMode.OnPropertyChanged);
                txtConfirmPIN.DataBindings.Add("Text", User, "ConfirmPIN", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    txtPassword.ReadOnly = true;
                    txtConfirmPassword.ReadOnly = true;
                    txtFirstName.ReadOnly = true;
                    txtLastName.ReadOnly = true;
                    txtEmail.ReadOnly = true;
                    cmbSitemapID.Visible = false;
                    cmbStatus.Visible = false;
                    dtgRole.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }
    }
}
