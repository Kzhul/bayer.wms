﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS
{
    public partial class LoginView : Form
    {
        private IUsersPasswordRepository _usersPasswordRepository;
        private IUserRepository _userRepository;
        private ISitemapRepository _sitemapRepository;

        public LoginView(IUserRepository userRepository, IUsersPasswordRepository usersPasswordRepository, ISitemapRepository sitemapRepository)
        {
            InitializeComponent();

            _userRepository = userRepository;
            _usersPasswordRepository = usersPasswordRepository;
            _sitemapRepository = sitemapRepository;

            #region DebugModeOnly
            try
            {
                bool debugMode = Convert.ToBoolean(ConfigurationManager.AppSettings["DebugMode"]);
                if (debugMode)
                {
                    txtUsername.Text = "admin";
                    txtPassword.Text = "123456aA@";
                    Utility.debugMode = true;
                }
            }
            catch (Exception ex)
            {
                //Do Nothing here
            }
            #endregion

        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                btnLogin.Enabled = false;

                await Login();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                btnLogin.Enabled = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private async Task Login()
        {
            string encryptedPwd = Utility.MD5Encrypt(txtPassword.Text);

            var user = await _userRepository.GetSingleAsync(p => p.Username == txtUsername.Text && p.Password == encryptedPwd && p.Status == User.status.A);
            if (user != null && user.UserID > 0)
            {
                LoginInfo.UserID = user.UserID;
                LoginInfo.Username = user.Username;
                LoginInfo.FirstName = user.FirstName;
                LoginInfo.LastName = user.LastName;
                LoginInfo.SitemapID = user.SitemapID;

                Utility.Sitemaps = await _sitemapRepository.ExecuteEntityList("proc_AccessRights_Select",
                    new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.VarChar, Value = user.UserID });

                if (user.PasswordChangeOnNextLogin)
                {
                    ChangePassword(user);
                }
                else
                    DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(Messages.Error_Login, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ChangePassword(User user)
        {
            using (var view = new ChangePwdView(_userRepository, _usersPasswordRepository))
            {
                view.User = user;
                view.MustChange = true;
                if (view.ShowDialog() == DialogResult.OK)
                    DialogResult = DialogResult.OK;
            }
        }

        private async void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                try
                {
                    btnLogin.Enabled = false;

                    await Login();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    btnLogin.Enabled = true;
                }
            }
        }
    }
}
