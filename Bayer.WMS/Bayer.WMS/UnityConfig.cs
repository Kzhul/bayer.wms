﻿using Bayer.WMS.Cof.Presenters;
using Bayer.WMS.Cof.Views;
using Bayer.WMS.Inv.Presenters;
using Bayer.WMS.Inv.Views;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Bayer.WMS.Prd.Presenters;
using Bayer.WMS.Prd.Views;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bayer.WMS
{
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType<IBayerWMSContext, BayerWMSContext>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<IUnitOfWorkManager, UnitOfWorkManager>();
            container.RegisterType<IBasePresenter, BasePresenter>();
            container.RegisterType<IMainPresenter, MainPresenter>();
            container.RegisterType<ISitemapRepository, SitemapRepository>();

            //// Audit Trail
            container.RegisterType<IAuditTrailView, AuditTrailView>();
            container.RegisterType<IAuditTrailPresenter, AuditTrailPresenter>();
            container.RegisterType<IAuditTrailRepository, AuditTrailRepository>();

            //// User
            container.RegisterType<IUserMaintView, UserMaintView>();
            container.RegisterType<IUserMaintPresenter, UserMaintPresenter>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IUsersRoleRepository, UsersRoleRepository>();
            container.RegisterType<IUsersPasswordRepository, UsersPasswordRepository>();

            //// Role
            container.RegisterType<IRoleMaintView, RoleMaintView>();
            container.RegisterType<IRoleMaintPresenter, RoleMaintPresenter>();
            container.RegisterType<IRoleRepository, RoleRepository>();
            container.RegisterType<IRoleSiteMapRepository, RoleSiteMapRepository>();

            //// Product
            container.RegisterType<IProductMaintView, ProductMaintView>();
            container.RegisterType<IProductMaintPresenter, ProductMaintPresenter>();
            container.RegisterType<IProductRepository, ProductRepository>();

            //// Product packing
            container.RegisterType<IProductPackingMaintView, ProductPackingMaintView>();
            container.RegisterType<IProductPackingMaintPresenter, ProductPackingMaintPresenter>();
            container.RegisterType<IProductPackingRepository, ProductPackingRepository>();

            //// Pallet
            container.RegisterType<IPalletMaintView, PalletMaintView>();
            container.RegisterType<IPalletMaintPresenter, PalletMaintPresenter>();
            container.RegisterType<IPalletRepository, PalletRepository>();
            container.RegisterType<IPalletStatusRepository, PalletStatusRepository>();

            //// Device
            container.RegisterType<IDeviceView, PackagingDeviceMaintView>();
            container.RegisterType<IDeviceMaintPresenter, DeviceMaintPresenter>();
            container.RegisterType<IDeviceRepository, DeviceRepository>();

            //// Barcode Bank
            container.RegisterType<IBarcodeBankMaintView, BarcodeBankMaintView>();
            container.RegisterType<IBarcodeBankMaintPresenter, BarcodeBankMaintPresenter>();
            container.RegisterType<IBarcodeBankRepository, BarcodeBankRepository>();

            //// Production plan
            container.RegisterType<IProductionPlanView, ProductionPlanView>();
            container.RegisterType<IProductionPlanPresenter, ProductionPlanPresenter>();
            container.RegisterType<IProductionPlanRepository, ProductionPlanRepository>();
            container.RegisterType<IProductionPlanImportLogRepository, ProductionPlanImportLogRepository>();
            container.RegisterType<IProductBarcodeRepository, ProductBarcodeRepository>();
            container.RegisterType<ICartonBarcodeRepository, CartonBarcodeRepository>();

            //// Packaging
            container.RegisterType<IPackagingView, PackagingView>();
            container.RegisterType<IPackagingPresenter, PackagingPresenter>();
            container.RegisterType<IPackagingLogRepository, PackagingLogRepository>();
            container.RegisterType<IPackagingErrorRepository, PackagingErrorRepository>();
            container.RegisterType<IPackagingSampleRepository, PackagingSampleRepository>();
            container.RegisterType<IPackagingForceEndRepository, PackagingForceEndRepository>();

            //// Warehouse
            container.RegisterType<IWarehouseMaintView, WarehouseMaintView>();
            container.RegisterType<IWarehouseMaintPresenter, WarehouseMaintPresenter>();
            container.RegisterType<IWarehouseRepository, WarehouseRepository>();

            //// Warehouse Visualization
            container.RegisterType<IWarehouseVisualizationView, WarehouseVisualizationView>();
            container.RegisterType<IWarehouseVisualizationPresenter, WarehouseVisualizationPresenter>();
            container.RegisterType<IWarehouseVisualizationRepository, WarehouseVisualizationRepository>();

            //// Zone
            container.RegisterType<IZoneMaintView, ZoneMaintView>();
            container.RegisterType<IZoneMaintPresenter, ZoneMaintPresenter>();
            container.RegisterType<IZoneRepository, ZoneRepository>();
            container.RegisterType<IZoneLineRepository, ZoneLineRepository>();
            container.RegisterType<IZoneLocationRepository, ZoneLocationRepository>();
            container.RegisterType<IZoneCategoryRepository, ZoneCategoryRepository>();
            container.RegisterType<IZoneTemperatureRepository, ZoneTemperatureRepository>();

            //// Stock Receive
            container.RegisterType<IStockReceivingView, StockReceivingView>();
            container.RegisterType<IStockReceivingPresenter, StockReceivingPresenter>();
            container.RegisterType<IStockReceivingHeaderRepository, StockReceivingHeaderRepository>();
            container.RegisterType<IStockReceivingDetailRepository, StockReceivingDetailRepository>();

            //// Stock Return
            container.RegisterType<IStockReturnView, StockReturnView>();
            container.RegisterType<IStockReturnPresenter, StockReturnPresenter>();
            container.RegisterType<IStockReturnHeaderRepository, StockReturnHeaderRepository>();
            container.RegisterType<IStockReturnDetailRepository, StockReturnDetailRepository>();

            //// Product Return
            container.RegisterType<IProductReturnView, ProductReturnView>();
            container.RegisterType<IProductReturnPresenter, ProductReturnPresenter>();
            container.RegisterType<IProductReturnHeaderRepository, ProductReturnHeaderRepository>();
            container.RegisterType<IProductReturnDetailRepository, ProductReturnDetailRepository>();

            //// Request Material
            container.RegisterType<IRequestMaterialView, RequestMaterialView>();
            container.RegisterType<IRequestMaterialPresenter, RequestMaterialPresenter>();
            container.RegisterType<IRequestMaterialRepository, RequestMaterialRepository>();
            container.RegisterType<IRequestMaterialLineRepository, RequestMaterialLineRepository>();
            container.RegisterType<IRequestMaterialLineSplitRepository, RequestMaterialLineSplitRepository>();

            //// Packaging Track and Trace
            container.RegisterType<IPackagingReportView, PackagingReportView>();
            container.RegisterType<IPackagingReportPresenter, PackagingReportPresenter>();

            //// Config
            container.RegisterType<IConfigView, ConfigView>();
            container.RegisterType<IConfigPresenter, ConfigPresenter>();

            //// Other
            container.RegisterType<ICategoryRepository, CategoryRepository>();
            container.RegisterType<IUOMRepository, UOMRepository>();

            ////Delivery            
            #region WareHouseForm
            container.RegisterType<IDeliveryOrderDetailRepository, DeliveryOrderDetailRepository>();
            container.RegisterType<IDeliveryOrderHeaderRepository, DeliveryOrderHeaderRepository>();
            container.RegisterType<IDeliveryOrderView, DeliveryOrderView>();
            container.RegisterType<IDeliveryOrderPresenter, DeliveryOrderPresenter>();

            container.RegisterType<IDeliveryNoteDetailRepository, DeliveryNoteDetailRepository>();
            container.RegisterType<IDeliveryNoteHeaderRepository, DeliveryNoteHeaderRepository>();
            container.RegisterType<IDeliveryNoteView, DeliveryNoteView>();
            container.RegisterType<IDeliveryNotePresenter, DeliveryNotePresenter>();

            container.RegisterType<ISplitNoteDetailRepository, SplitNoteDetailRepository>();
            container.RegisterType<ISplitNoteHeaderRepository, SplitNoteHeaderRepository>();
            container.RegisterType<ISplitNoteView, SplitNoteView>();
            container.RegisterType<ISplitNotePresenter, SplitNotePresenter>();

            container.RegisterType<IPrepareNoteDetailRepository, PrepareNoteDetailRepository>();
            container.RegisterType<IPrepareNoteHeaderRepository, PrepareNoteHeaderRepository>();
            container.RegisterType<IPrepareNoteView, PrepareNoteView>();
            container.RegisterType<IPrepareNotePresenter, PrepareNotePresenter>();

            container.RegisterType<IDeliveryTicketDetailRepository, DeliveryTicketDetailRepository>();
            container.RegisterType<IDeliveryTicketHeaderRepository, DeliveryTicketHeaderRepository>();
            container.RegisterType<IDeliveryTicketView, DeliveryTicketView>();
            container.RegisterType<IDeliveryTicketPresenter, DeliveryTicketPresenter>();

            container.RegisterType<IReturnDODetailRepository, ReturnDODetailRepository>();
            container.RegisterType<IReturnDOHeaderRepository, ReturnDOHeaderRepository>();
            container.RegisterType<IReturnDOView, ReturnDOView>();
            container.RegisterType<IReturnDOPresenter, ReturnDOPresenter>();

            //container.RegisterType<IStockReceivingDetailRepository, StockReceivingDetailRepository>();
            //container.RegisterType<IStockReceivingHeaderRepository, StockReceivingHeaderRepository>();
            //container.RegisterType<IStockReceivingView, StockReceivingView>();
            //container.RegisterType<IStockReceivingPresenter, StockReceivingPresenter>();
            #endregion

            #region Report
            container.RegisterType<IReportDOView, ReportDOView>();
            container.RegisterType<IReportDOPresenter, ReportDOPresenter>();

            container.RegisterType<IReportBay4sView, ReportBay4sView>();
            container.RegisterType<IReportBay4sPresenter, ReportBay4sPresenter>();

            container.RegisterType<IReportDeliveryBarcodeHistoryView, ReportDeliveryBarcodeHistoryView>();
            container.RegisterType<IReportDeliveryBarcodeHistoryPresenter, ReportDeliveryBarcodeHistoryPresenter>();

            container.RegisterType<IBarcodeTrackingView, Inv.Views.BarcodeTrackingView>();
            container.RegisterType<IBarcodeTrackingPresenter, BarcodeTrackingPresenter>();

            container.RegisterType<IReportLocationView, ReportLocationView>();
            container.RegisterType<IReportLocationPresenter, ReportLocationPresenter>();

            container.RegisterType<IPalletTrackingView, PalletTrackingView>();
            container.RegisterType<IPalletTrackingPresenter, PalletTrackingPresenter>();
            #endregion

            #region Company
            container.RegisterType<ICompanyRepository, CompanyRepository>();
            container.RegisterType<ICompanyView, CompanyView>();
            container.RegisterType<ICompanyPresenter, CompanyPresenter>();
            #endregion
        }
    }
}
