﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS
{
    public partial class ChangePwdView : Form
    {
        private IUserRepository _userRepository;
        private IUsersPasswordRepository _usersPasswordRepository;

        public ChangePwdView(IUserRepository userRepository, IUsersPasswordRepository usersPasswordRepository)
        {
            InitializeComponent();

            _userRepository = userRepository;
            _usersPasswordRepository = usersPasswordRepository;
        }

        private async void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                User.Password = txtPassword.Text;
                User.ConfirmPassword = txtConfirmPassword.Text;

                await ValidatePassword(User);

                User.Password = Utility.MD5Encrypt(User.Password);

                await _userRepository.ExecuteNonQuery("proc_Users_Update_Password",
                    new SqlParameter { ParameterName = "@UserID", SqlDbType = SqlDbType.VarChar, Value = User.UserID },
                    new SqlParameter { ParameterName = "@Password", SqlDbType = SqlDbType.VarChar, Value = User.Password },
                    new SqlParameter { ParameterName = "@PasswordChangeOnNextLogin", SqlDbType = SqlDbType.Bit, Value = false });

                DialogResult = DialogResult.OK;
            }
            catch (WrappedException ex)
            {
                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void ChangePwdView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
                Application.Exit();
        }

        private async Task ValidatePassword(User user)
        {
            user.ValidatePassword();
            string password = Utility.MD5Encrypt(user.Password);

            //// Validate password with last 10 last change times
            if (await _usersPasswordRepository.GetSingleAsync(p => p.UserID == user.UserID && p.Password == password) != null)
                throw new WrappedException(Messages.Validate_Policy_PasswordNotIn10LastChangeTimes);
        }

        public User User { get; set; }

        public bool MustChange { get; set; }
    }
}
