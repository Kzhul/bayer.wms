﻿using Bayer.WMS.Cof.Presenters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using System.Resources;
using Bayer.WMS.Properties;
using Microsoft.Practices.Unity;
using Bayer.WMS.Objs;
using Bayer.WMS.Base;
using System.IO;
using System.Configuration;
using System.Xml;
using Bayer.WMS.Prd.Views;
using System.Reflection;
using System.Diagnostics;
using Bayer.WMS.Inv.Views;

namespace Bayer.WMS
{
    public partial class MainView : Form, IMainView
    {
        private readonly IUnityContainer _container;
        private readonly IMainPresenter _presenter;
        private readonly Timer _timer;        

        public MainView()
        {
            InitializeComponent();
            lblMessage.Text = String.Empty;

            Utility.ConnStr = Utility.DescryptConnString(ConfigurationManager.ConnectionStrings["BayerWMSContext"].ConnectionString);
            #region DebugModeOnly
            try
            {
                bool debugMode = Convert.ToBoolean(ConfigurationManager.AppSettings["DebugMode"]);
                if (debugMode)
                    Utility.ConnStr = ConfigurationManager.ConnectionStrings["BayerWMSContext"].ConnectionString;
                else
                    Utility.ConnStr = Utility.DescryptConnString(ConfigurationManager.ConnectionStrings["BayerWMSContext"].ConnectionString);
            }
            catch (Exception ex)
            {
                //Do Nothing here
            }
            #endregion
            

            _container = UnityConfig.GetConfiguredContainer();
            _presenter = _container.Resolve<IMainPresenter>(new ParameterOverride("view", this));

            _timer = new Timer();
            _timer.Interval = 5000;
            _timer.Tick += _timer_Tick;
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            _timer.Stop();
            lblMessage.Text = String.Empty;
        }

        private async void MainView_Load(object sender, EventArgs e)
        {
            try
            {
                Visible = false;

                using (var loginView = new LoginView(_container.Resolve<IUserRepository>(), _container.Resolve<IUsersPasswordRepository>(), _container.Resolve<ISitemapRepository>()))
                {
                    if (loginView.ShowDialog() != DialogResult.OK)
                    {
                        Application.Exit();
                        return;
                    }
                    else
                        LoadSitemap();
                }

                Visible = true;

                if (LoginInfo.SitemapID.HasValue)
                {
                    await OpenDefaultSitemap();
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        /// <summary>
        /// Event handle user click on menu item
        /// purpose to open a new view or focus on exist view
        /// </summary>
        private int _packagingSitemapID = 7;
        private int _barcodeBankSitemapID = 36;
        private int _AutoUpdateSiteMap = 47;
        private int _BarcodeTrackingSiteMap = 31;
        private async void MenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var menuItem = sender as ToolStripMenuItem;
                int sitemapID = int.Parse(menuItem.Name.Split('_')[1]);
                var sitemap = Utility.Sitemaps.First(p => p.SitemapID == sitemapID);
                if (sitemap.SitemapID == _packagingSitemapID)
                    await OpenPackaging(sitemap);
                else if (sitemap.SitemapID == _barcodeBankSitemapID)
                    await OpenBarcodeBank(sitemap);
                else if (sitemap.SitemapID == _AutoUpdateSiteMap)
                    await AutoUpdateClick();
                else if (sitemap.SitemapID == _BarcodeTrackingSiteMap)
                    await OpenBarcodeTracking(sitemap);
                else
                    await OpenTabPage(sitemap);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        private async Task AutoUpdateClick()
        {
            Process.Start(Path.Combine(Application.StartupPath, "Bayer.WMS.Launcher.exe"));
        }

        private void tbcMainTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sitemap = tbcMainTabControl.TabCount > 0 ? _presenter.GetSitemapRegistered(tbcMainTabControl.SelectedTab.Name) : new Sitemap();
            HandlePermission(sitemap);

            btnClose.Visible = tbcMainTabControl.TabCount > 0;
        }

        private async void btnRefresh_Click(object sender, EventArgs e)
        {
            btnRefresh.Enabled = false;

            try
            {
                await RefreshPresenter(_presenter.GetSitemapRegistered(tbcMainTabControl.SelectedTab.Name).SitemapID);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnRefresh.Enabled = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            btnAdd.Enabled = false;

            try
            {
                var sitemap = _presenter.GetSitemapRegistered(tbcMainTabControl.SelectedTab.Name);
                var view = Application.OpenForms[sitemap.ClassName];
                var methodInfo = view.GetType().GetMethod("Insert");
                methodInfo.Invoke(view, null);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnAdd.Enabled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;

            try
            {
                var sitemap = _presenter.GetSitemapRegistered(tbcMainTabControl.SelectedTab.Name);
                var view = Application.OpenForms[sitemap.ClassName];
                var methodInfo = view.GetType().GetMethod("Save");
                methodInfo.Invoke(view, null);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnSave.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;

            try
            {
                var sitemap = _presenter.GetSitemapRegistered(tbcMainTabControl.SelectedTab.Name);
                var view = Application.OpenForms[sitemap.ClassName];
                var methodInfo = view.GetType().GetMethod("Delete");
                methodInfo.Invoke(view, null);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnDelete.Enabled = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            var sitemap = _presenter.GetSitemapRegistered(tbcMainTabControl.SelectedTab.Name);
            var view = Application.OpenForms[sitemap.ClassName];
            view.Dispose();

            _presenter.UnregisterSitemap(tbcMainTabControl.SelectedTab.Name);

            tbcMainTabControl.TabPages.RemoveAt(tbcMainTabControl.SelectedIndex);
        }

        private void LoadSitemap()
        {
            Sitemaps = Utility.Sitemaps;
        }

        private async Task OpenDefaultSitemap()
        {
            var sitemap = Utility.Sitemaps.FirstOrDefault(p => p.SitemapID == LoginInfo.SitemapID);
            if (LoginInfo.SitemapID != _packagingSitemapID)
                await OpenTabPage(sitemap);
            else
                await OpenPackaging(sitemap);
        }

        private void HandlePermission(Sitemap sitemap)
        {
            btnRefresh.Enabled = sitemap.EnableRefresh == true;
            btnPrint.Enabled = sitemap.EnablePrint == true;

            btnAdd.Enabled = sitemap.EnableInsert == true && (sitemap.AccessRights == RoleSitemap.accessRights.Write || sitemap.AccessRights == RoleSitemap.accessRights.Modify);
            btnSave.Enabled = sitemap.EnableSave == true && (sitemap.AccessRights == RoleSitemap.accessRights.Write || sitemap.AccessRights == RoleSitemap.accessRights.Modify);
            btnDelete.Enabled = sitemap.EnableDelete == true && (sitemap.AccessRights == RoleSitemap.accessRights.Write || sitemap.AccessRights == RoleSitemap.accessRights.Modify);
        }

        /// <summary>
        /// Create view instance based on sitemap information
        /// </summary>
        private async Task<Form> CreateForm(Sitemap sitemap, string prefix = "")
        {
            Type viewType = Type.GetType($"{sitemap.AssemblyName}.Views.{sitemap.ClassName}, {sitemap.AssemblyName}");
            var view = _container.Resolve(viewType) as BaseForm;
            view.Name = sitemap.ClassName + prefix;
            view.Text = sitemap.Description;
            view.Sitemap = sitemap;
            view.HandlePermission(sitemap.AccessRights);

            Type presenterType = Type.GetType($"{sitemap.AssemblyName}.Presenters.{sitemap.InterfaceName}, {sitemap.AssemblyName}");

            var presenter = _container.Resolve(presenterType,
                new ParameterOverride("view", view),
                new ParameterOverride("mainPresenter", _presenter));
            await view.SetPresenter(presenter as IBasePresenter);

            return view;
        }

        /// <summary>
        /// Add new view to tabcontrol or focus on exist view
        /// </summary>
        private async Task OpenTabPage(Sitemap sitemap)
        {
            var view = Application.OpenForms[sitemap.ClassName];
            if (view == null)
                view = await CreateForm(sitemap);

            tbcMainTabControl.Show();

            view.TopLevel = false;
            view.Dock = DockStyle.Fill;

            string key = $"{sitemap.AssemblyName}.{sitemap.ClassName}";

            //// check tabcontrol have already had the view
            //// if not yet, add view to tabcontrol
            if (!tbcMainTabControl.TabPages.ContainsKey(key))
            {
                _presenter.RegisterSitemap(key, sitemap);

                tbcMainTabControl.TabPages.Add(key, view.Text);
                tbcMainTabControl.TabPages[key].Controls.Add(view);
                view.Show();
            }

            //// focus on particular tab
            tbcMainTabControl.SelectTab(key);

            if (tbcMainTabControl.TabCount == 1)
                tbcMainTabControl_SelectedIndexChanged(tbcMainTabControl, new EventArgs());
        }

        private async Task OpenPackaging(Sitemap sitemap)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            XmlDocument doc = new XmlDocument();
            doc.Load(config.FilePath);
            var node = doc.SelectSingleNode("//packagingConfig");

            if (node == null)
                throw new WrappedException(Messages.Error_COMConfigSetup);

            //// don't use multi screen
            bool isMultScreen = false;
            if (node == null || !bool.TryParse(node.Attributes["IsMultScreen"].Value, out isMultScreen) || !isMultScreen)
            {
                var child = node.ChildNodes[0];

                var view = await CreateForm(sitemap) as PackagingView;
                if (child != null && !String.IsNullOrWhiteSpace(child.Attributes["PortName"].Value))
                {
                    view.CreateCOM(child.Attributes["PortName"].Value, int.Parse(child.Attributes["BaudRate"].Value), child.Attributes["Parity"].Value, int.Parse(child.Attributes["DataBits"].Value), child.Attributes["StopBits"].Value);

                }
                view.PackagingLine = child.Attributes["PackagingLine"].Value;
                view.WindowState = FormWindowState.Maximized;
                view.Show();
                view.BringToFront();
            }
            else
            {
                foreach (XmlNode child in node.ChildNodes)
                {
                    var view = (await CreateForm(sitemap, child.Attributes["PackagingLine"].Value)) as PackagingView;
                    view.CreateCOM(child.Attributes["PortName"].Value, int.Parse(child.Attributes["BaudRate"].Value), child.Attributes["Parity"].Value, int.Parse(child.Attributes["DataBits"].Value), child.Attributes["StopBits"].Value);
                    view.IsParallel = true;
                    view.PackagingLine = child.Attributes["PackagingLine"].Value;
                    view.Location = Screen.AllScreens[int.Parse(child.Attributes["Screen"].Value)].WorkingArea.Location;
                    view.WindowState = FormWindowState.Maximized;
                    view.Show();
                }
            }
        }

        private async Task OpenBarcodeTracking(Sitemap sitemap)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            XmlDocument doc = new XmlDocument();
            doc.Load(config.FilePath);
            var node = doc.SelectSingleNode("//packagingConfig");

            if (node == null)
                throw new WrappedException(Messages.Error_COMConfigSetup);
            var child = node.ChildNodes[0];

            var view = await CreateForm(sitemap) as BarcodeTrackingView;
            if (child != null && !String.IsNullOrWhiteSpace(child.Attributes["PortName"].Value))
            {
                view.CreateCOM(child.Attributes["PortName"].Value, int.Parse(child.Attributes["BaudRate"].Value), child.Attributes["Parity"].Value, int.Parse(child.Attributes["DataBits"].Value), child.Attributes["StopBits"].Value);

            }
            view.Show();
        }

        private async Task OpenBarcodeBank(Sitemap sitemap)
        {
            var view = await CreateForm(sitemap) as BarcodeBankMaintView;
            view.ShowDialog();
        }

        public IList<Sitemap> Sitemaps
        {
            set
            {
                foreach (var root in value.Where(p => !p.ParentID.HasValue).OrderBy(p => p.Order))
                {
                    var rootMenuItem = new ToolStripMenuItem();
                    rootMenuItem.Name = $"mn_{root.SitemapID}";
                    rootMenuItem.Text = root.Description;

                    if (!String.IsNullOrWhiteSpace(root.Icon))
                        rootMenuItem.Image = Base.Resources.ResourceManager.GetObject(root.Icon) as Image;

                    mnMain.Items.Add(rootMenuItem);

                    Stack<ToolStripMenuItem> stackMenuItem = new Stack<ToolStripMenuItem>();
                    stackMenuItem.Push(rootMenuItem);
                    Stack<Sitemap> stackSitemap = new Stack<Sitemap>();
                    stackSitemap.Push(root);

                    while (stackSitemap.Count > 0)
                    {
                        var menuItem = stackMenuItem.Pop();
                        var siteMap = stackSitemap.Pop();

                        var childList = value.Where(p => p.ParentID == siteMap.SitemapID && p.Type != Sitemap.type.Feature).OrderBy(p => p.Order).ToList();
                        if (childList.Count > 0)
                        {
                            int childInvisible = 0;
                            foreach (var child in childList)
                            {
                                var childMenuItem = new ToolStripMenuItem();
                                childMenuItem.Name = $"mn_{child.SitemapID}";
                                childMenuItem.Text = child.Description;

                                //// just set permission on last level
                                int count = value.Count(p => p.ParentID == child.SitemapID && p.Type != Sitemap.type.Feature);
                                if (count == 0)
                                {
                                    bool hasPermission = Utility.Sitemaps.Any(p => p.SitemapID == child.SitemapID && p.AccessRights != RoleSitemap.accessRights.NotSet);
                                    childMenuItem.Visible = hasPermission;
                                    childMenuItem.Enabled = hasPermission;
                                    childInvisible += hasPermission ? 0 : 1;
                                }

                                if (!String.IsNullOrWhiteSpace(child.Icon))
                                    childMenuItem.Image = Base.Resources.ResourceManager.GetObject(child.Icon) as Image;
                                if (!String.IsNullOrWhiteSpace(child.ClassName))
                                    childMenuItem.Click += MenuItem_Click;

                                menuItem.DropDownItems.Add(childMenuItem);

                                stackMenuItem.Push(childMenuItem);
                                stackSitemap.Push(child);
                            }

                            if (childInvisible == childList.Count)
                            {
                                menuItem.Visible = false;
                                menuItem.Enabled = false;
                            }
                        }
                    }

                    rootMenuItem.Visible = false;
                    foreach (ToolStripMenuItem item in rootMenuItem.DropDownItems)
                    {
                        if (item.Enabled == true)
                        {
                            rootMenuItem.Visible = true;
                            break;
                        }
                    }
                }
            }
        }

        public Color MessageColor
        {
            set { lblMessage.ForeColor = value; }
        }

        public string Message
        {
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        if (!String.IsNullOrWhiteSpace(value))
                            _timer.Start();
                        lblMessage.Text = value;
                    });
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(value))
                        _timer.Start();
                    lblMessage.Text = value;
                }
            }
        }

        public Sitemap Sitemap { get; set; }

        public void InitializeBinding()
        {
            throw new NotImplementedException();
        }

        public bool GetConfirm(string message)
        {
            throw new NotImplementedException();
        }

        public void SetMessage(string message, Utility.MessageType messageType, string parent = "")
        {
            throw new NotImplementedException();
        }

        public async Task RefreshPresenter(int sitemapID)
        {
            var sitemap = Utility.Sitemaps.FirstOrDefault(p => p.SitemapID == sitemapID);
            var view = Application.OpenForms[sitemap.ClassName] as BaseForm;

            Type presenterType = Type.GetType($"{sitemap.AssemblyName}.Presenters.{sitemap.InterfaceName}, {sitemap.AssemblyName}");

            var presenter = _container.Resolve(presenterType,
                new ParameterOverride("view", view),
                new ParameterOverride("mainPresenter", _presenter));
            await view.SetPresenter(presenter as IBasePresenter, true);
        }

        public object Resolve(Type type, params ResolverOverride[] resolverOverride)
        {
            object resoved;
            if (resolverOverride != null)
                resoved = _container.Resolve(type, resolverOverride);
            else
                resoved = _container.Resolve(type);
            return resoved;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            btnPrint.Enabled = false;

            try
            {
                var sitemap = _presenter.GetSitemapRegistered(tbcMainTabControl.SelectedTab.Name);
                var view = Application.OpenForms[sitemap.ClassName];
                var methodInfo = view.GetType().GetMethod("Print");
                methodInfo.Invoke(view, null);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnPrint.Enabled = true;
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {

        }
    }
}
