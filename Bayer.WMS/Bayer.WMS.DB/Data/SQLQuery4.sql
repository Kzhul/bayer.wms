USE [Bayer.WMS]
GO
/****** Object:  StoredProcedure [dbo].[proc_AuditTrails_Insert]    Script Date: 12/3/2017 4:06:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ReportLocation3]
AS
BEGIN
	SET NOCOUNT ON
--Zone Selection
SELECT DISTINCT
	 Z.ZoneName
	 , ZL.LineName
	 , L.LocationCode
	 , P.PalletCode
	 , PS.ProductLot
	 , PR.ProductCode
	 , PR.Description AS ProductName
	 , PR.UOM
	 , PPS.Quantity AS PackSize
	 , COUNT(DISTINCT PS.CartonBarcode) AS CartonQuantity
	 , COUNT(DISTINCT PS.ProductBarcode) AS Quantity	 
	 , ManufacturingDate = FORMAT(PP.ManufacturingDate, 'dd/MM/yyyy')
	 , ExpiryDate = FORMAT(PP.ExpiryDate, 'dd/MM/yyyy')
	 , LocationPutDate = FORMAT(P.LocationPutDate, 'dd/MM/yyyy')
FROM 
	Zones Z
	LEFT JOIN [dbo].[ZoneLines] ZL
		ON Z.ZoneCode = ZL.ZoneCode
	LEFT JOIN [dbo].[ZoneLocations] L
		ON ZL.ZoneCode = L.ZoneCode
		AND ZL.LineCode = L.LineCode
	LEFT JOIN [dbo].[Pallets] P
		ON P.LocationCode = L.LocationCode
		AND P.LocationCode IS NOT NULL
		AND P.LocationCode != ''
	LEFT JOIN PalletStatuses PS
		ON P.PalletCode = PS.PalletCode
	LEFT JOIN Products PR
		 ON PS.ProductID = PR.ProductID
	LEFT JOIN ProductionPlans pp
		ON PS.ProductLot = PP.ProductLot
	LEFT JOIN [dbo].[ProductPackings] PPS
		ON PR.ProductID = PPS.ProductID
		AND PPS.Type != 'P'
WHERE
	P.PalletCode IS NOT NULL
GROUP BY
	Z.ZoneName
	 , ZL.LineName
	 , L.LocationCode
	 , P.PalletCode
	 , PS.ProductLot
	 , PR.ProductCode
	 , PR.Description-- AS ProductName
	 , PR.UOM
	 , PPS.Quantity-- AS PackSize
	 , PP.ManufacturingDate
	 , PP.ExpiryDate
	 , P.LocationPutDate
ORDER BY 
	Z.ZoneName
	 , ZL.LineName
	 , L.LocationCode

END

