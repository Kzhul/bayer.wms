﻿CREATE PROCEDURE [dbo].[proc_Packaging_CountQuantityInCarton]
	@CartonBarcode NVARCHAR(50)
AS
BEGIN
DECLARE @_CartonBarcode NVARCHAR(50) = @CartonBarcode--'DO18052503'
SELECT DISTINCT 
	CONVERT(NVARCHAR(50), COUNT(DISTINCT PS.EncryptedProductBarcode))
FROM 
	PalletStatuses PS WITH (NOLOCK)
WHERE 
	PS.CartonBarcode = @_CartonBarcode

END