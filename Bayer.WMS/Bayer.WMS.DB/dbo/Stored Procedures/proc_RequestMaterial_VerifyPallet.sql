﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_VerifyPallet]
	@DocumentNbr NVARCHAR(50)
	,@PalletCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_PalletCode NVARCHAR(50) = @PalletCode
	
--Kiểm tra nguyên liệu trên pallet có hợp lệ theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.ProductID, srd.ExpiredDate ASC)
	,PalletCodeSuggest = PS.PalletCode
	,PD.ProductID
	,PS.ProductLot
	,P.Status
	,P.CompanyCode
	,srd.DeliveryDate
	,srd.ExpiredDate
	,RM.RequestQty
	,SUM(ISNULL(PS.Qty, 1)) AS PalletQuantity
	,PicQuantity = 0
INTO #TBPick
FROM
	PalletStatuses PS WITH (NOLOCK) 		
	LEFT JOIN RequestMaterialLines RM WITH (NOLOCK) 
		ON RM.ProductID = PS.ProductID
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON PS.PalletCode = P.PalletCode
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON PS.ProductID = PD.ProductID
	LEFT JOIN dbo.StockReceivingDetails srd WITH (NOLOCK) 
		ON srd.ProductID = ps.ProductID
		AND srd.BatchCode = ps.ProductLot
WHERE
	P.Status = 'N'--Trạng thái pallet là mới
	AND RM.DocumentNbr = @_DocumentNbr
	AND PS.PalletCode = @_PalletCode
GROUP BY
	PS.PalletCode
	,PD.ProductID
	,PS.ProductLot
	,P.Status
	,P.CompanyCode
	,srd.DeliveryDate
	,srd.ExpiredDate
	,RM.RequestQty
ORDER BY
	PD.ProductID
	,srd.ExpiredDate ASC

SELECT DISTINCT * 
FROM
	#TBPick
WHERE
	RequestQty < PalletQuantity 
	--Kiểm tra xem nguyên liệu này cho phép chuyển dư hay ko
	

--Nếu như table #TBPick có dữ liệu
--Suy ra pallet có chứa hàng ko được xuất dư, hoặc có hàng ko yêu cầu xuất trên này
--Cẩn phải san pallet mới được xác nhận

END