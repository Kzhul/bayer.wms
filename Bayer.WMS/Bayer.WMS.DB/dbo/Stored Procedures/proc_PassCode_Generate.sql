﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PassCode_Generate]
	@UserID INT
AS
BEGIN
	SET NOCOUNT ON

    IF NOT EXISTS(
		SELECT * FROM [dbo].[PassCode] WHERE DATEDIFF(dd, [WorkingDate], GETDATE()) = 0
	)
	BEGIN
		DECLARE @MIN  INT = 1000; --We define minimum value, it can be generated.
		DECLARE @MAX  INT = 9999; --We define maximum value, it can be generated.
		DECLARE @Code INT = (SELECT @MIN+FLOOR((@MAX-@MIN+1)*RAND(CONVERT(VARBINARY,NEWID())))); 
		--And then this T-SQL snippet generates an integer between minimum and maximum integer values.

		INSERT INTO [dbo].[PassCode]
				   ([WorkingDate]
				   ,[Code]
				   ,[CreatedBy]
				   ,[CreatedBySitemapID]
				   ,[CreatedDateTime]
				   ,[UpdatedBy]
				   ,[UpdatedBySitemapID]
				   ,[UpdatedDateTime])
			 VALUES
				   (GETDATE()
				   ,@Code
				   ,@UserID
				   ,1
				   ,GETDATE()
				   ,@UserID
				   ,1
				   ,GETDATE())	
	END

	SELECT * FROM [dbo].[PassCode] WHERE DATEDIFF(dd, [WorkingDate], GETDATE()) = 0
END