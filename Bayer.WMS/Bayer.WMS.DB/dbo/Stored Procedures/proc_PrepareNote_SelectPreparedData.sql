﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PrepareNote_SelectPreparedData]
	@DOImportCode NVARCHAR(50)
	,@CompanyCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_DOImportCode NVARCHAR(20) = @DOImportCode
	DECLARE @_CompanyCode NVARCHAR(20) = @CompanyCode
	--WHEN @PackingType = 'C' THEN ' T'--Thùng
--WHEN @PackingType = 'CC' THEN ' T'--Thùng chẵn
--WHEN @PackingType = 'CSH' THEN ' T'--Thùng lẻ
--WHEN @PackingType = 'B' THEN ' B'--Bao
--WHEN @PackingType = 'S' THEN ' X'--Xô
--WHEN @PackingType = 'A' THEN ' C'--Can

	SELECT DISTINCT
		DH.DOImportCode
		, DH.CompanyCode
		, DH.BatchCode
		, DH.ProductID
		, PD.ProductCode	
		, PD.[UOM]
		, ProductDescription = PD.Description 
		, DH.DOQuantity
		, DH.PreparedQty
		, DH.[DeliveredQty]
		, CartonQty = CASE WHEN PD.PackingType = 'C' THEN ISNULL(tmp.CartonQty,0) ELSE ISNULL(tmp.OddQty,0) END
		, [PackageSize] = CONVERT(INT,ISNULL(PPS.Quantity,0))
		, OddQty = CASE WHEN PD.PackingType = 'C' THEN ISNULL(tmp.OddQty,0) ELSE 0 END
		, ProductQty = ISNULL(tmp.ProductQty,0)
		, PackingType = CASE WHEN PD.PackingType = 'C' THEN 'T'
							 WHEN PD.PackingType = 'B' THEN ' B'--Bao
							 WHEN PD.PackingType = 'S' THEN ' X'--Xô
							 WHEN PD.PackingType = 'A' THEN ' C'--Can
							 ELSE ''
							 END
		, OddPackingType = CASE WHEN PD.PackingType = 'C' THEN 'G' ELSE '' END
	FROM 
		DeliveryOrderSum DH WITH (NOLOCK) 
		LEFT JOIN (
			SELECT DISTINCT 
				DH.DOImportCode
				, DH.CompanyCode
				, DH.ProductID
				, DH.BatchCode
				, CartonQty = COUNT(DISTINCT CASE WHEN ps.CartonBarcode != '' THEN ps.CartonBarcode ELSE NULL END)
				, OddQty = SUM(CASE WHEN ps.CartonBarcode = '' THEN 1 ELSE 0 END)
				, ProductQty = SUM(ISNULL(ps.Qty, 1))
			FROM
				DeliveryOrderSum DH WITH (NOLOCK)
				JOIN Pallets P WITH (NOLOCK)
					ON DH.DOImportCode = P.DOImportCode
					AND DH.CompanyCode = P.CompanyCode
				JOIN dbo.PalletStatuses ps WITH (NOLOCK)
					ON PS.PalletCode = P.PalletCode
					AND DH.ProductID = PS.ProductID
					AND DH.BatchCode = PS.ProductLot
			WHERE
				DH.DOQuantity > 0
				AND DH.DOImportCode = @_DOImportCode
				AND DH.CompanyCode = @_CompanyCode
			GROUP BY
				DH.DOImportCode
				, DH.CompanyCode
				, DH.ProductID
				, DH.BatchCode

			UNION

			SELECT DISTINCT 
				DH.DOImportCode
				, DH.CompanyCode
				, DH.ProductID
				, DH.BatchCode
				, CartonQty = COUNT(DISTINCT CASE WHEN ps.CartonBarcode != '' THEN ps.CartonBarcode ELSE NULL END)
				, OddQty = SUM(CASE WHEN ps.CartonBarcode = '' THEN 1 ELSE 0 END)
				, ProductQty = COUNT(DISTINCT PS.[ProductBarcode])
			FROM
				DeliveryOrderSum DH WITH (NOLOCK)
				JOIN dbo.DeliveryHistories ps WITH (NOLOCK)
					ON DH.DOImportCode = ps.DeliveryTicketCode
					AND DH.CompanyCode = PS.CompanyCode
					AND DH.ProductID = PS.ProductID
					AND DH.BatchCode = PS.ProductLot
			WHERE
				DH.DOQuantity > 0
				AND DH.DOImportCode = @_DOImportCode
				AND DH.CompanyCode = @_CompanyCode
			GROUP BY
				DH.DOImportCode
				, DH.CompanyCode
				, DH.ProductID
				, DH.BatchCode
		)tmp
			ON DH.DOImportCode = tmp.DOImportCode
			AND DH.CompanyCode = tmp.CompanyCode
			AND DH.ProductID = tmp.ProductID
			AND DH.BatchCode = tmp.BatchCode
		LEFT JOIN Products PD WITH (NOLOCK)
			ON DH.ProductID = PD.ProductID
		LEFT JOIN [dbo].[ProductPackings] PPS
			ON PD.ProductID = PPS.ProductID
			AND PPS.Type != 'P'
	WHERE
		DH.DOQuantity > 0
		AND DH.DOImportCode = @_DOImportCode
		AND DH.CompanyCode = @_CompanyCode
END