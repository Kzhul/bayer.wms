﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_ReportDeliveryBarcodeHistory] '', NULL, NULL
CREATE PROCEDURE [dbo].[proc_ReportDeliveryBarcodeHistory3]
	@CompanyCode NVARCHAR(255)
	,@FromDate Date
	,@ToDate Date
	,@Sheet INT
AS
BEGIN
	SET NOCOUNT ON

DECLARE @_FromDate DATE = @FromDate
DECLARE @_ToDate DATE  = @ToDate
DECLARE @_CompanyCode NVARCHAR(50) = @CompanyCode

--CACHE DATA
SELECT DISTINCT 
	DH.*
	,DO.DOQuantity
INTO #A
FROM 
	DeliveryHistories DH WITH (NOLOCK)
	LEFT JOIN DeliveryOrderSum DO WITH (NOLOCK)
		ON DH.DeliveryTicketCode = DO.DOImportCode
		AND DH.CompanyCode = DO.CompanyCode
		AND DH.ProductID = DO.ProductID
		AND DH.ProductLot = DO.BatchCode
		AND DO.DOQuantity > 0
WHERE
	(@_CompanyCode = '' OR DH.CompanyCode = @_CompanyCode)
	AND (@_FromDate IS NULL OR DH.DeliveryDate >= @_FromDate)
	AND (@_ToDate IS NULL OR DH.DeliveryDate <= @_ToDate)
	AND DH.CompanyCode IS NOT NULL
	AND DH.DeliveryDate IS NOT NULL


--SELECT SUM Product
IF (@Sheet = 1)
BEGIN
SELECT DISTINCT
	P.DeliveryDate
	, StrDeliveryDate = Format(P.[DeliveryDate],'dd/MM/yyyy')
	, P.DeliveryTicketCode AS DOImportCode
	, P.DOQuantity
	, P.CompanyCode
	, P.CompanyName
	, ProductDescription = pd.Description
	, tmp.ProductLot
	, tmp.Quantity
	, CartonOddQuantity = CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0))-- + 'T / ' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
	--, pd.PackingType
	--, pd.ProductCode	
	--, pd.ProductID
FROM
	(
		SELECT DISTINCT 
			CompanyCode
			, DeliveryDate
			, CompanyName
			, DeliveryTicketCode
			, DOQuantity
			, ProductLot
		FROM 
			#A
	) P 
	LEFT JOIN 
	(
		SELECT DISTINCT
			ps.CompanyCode
			, ps.DeliveryDate
			, ps.ProductID
			, ps.ProductLot
			, Quantity = COUNT(DISTINCT ps.ProductBarcode)
			, OddQuantity = SUM(CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN NULL ELSE 1 END)
		FROM
			#A AS ps
		GROUP BY
			ps.CompanyCode
			, ps.DeliveryDate
			, ps.ProductID
			, ps.ProductLot
	) tmp
		ON P.CompanyCode = tmp.CompanyCode
		AND P.DeliveryDate = tmp.DeliveryDate
		AND P.ProductLot = tmp.ProductLot
	LEFT JOIN 
	(
		SELECT DISTINCT
			ps.CompanyCode
			, ps.DeliveryDate
			, ps.ProductID
			, ps.ProductLot
			, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
		FROM
			#A AS ps
		WHERE
			CartonBarcode IS NOT NULL 
			AND CartonBarcode != ''
		GROUP BY
			ps.CompanyCode
			, ps.DeliveryDate
			, ps.ProductID
			, ps.ProductLot
	) tmpC
		ON P.CompanyCode = tmpC.CompanyCode
		AND P.DeliveryDate = tmpC.DeliveryDate
		AND tmp.ProductID = tmpC.ProductID
		AND P.ProductLot = tmpC.ProductLot
	LEFT JOIN dbo.Products pd WITH (NOLOCK) ON pd.ProductID = tmp.ProductID
ORDER BY 
	P.DeliveryDate
	, P.CompanyCode
	, P.CompanyName
	, pd.Description
	, P.ProductLot
END


IF (@Sheet = 2)
BEGIN
--SUM BY CARTON
SELECT DISTINCT 
	DeliveryDate
	,StrDeliveryDate = Format([DeliveryDate],'dd/MM/yyyy')
	, CompanyCode
	, CompanyName
	, ProductDescription
	, ProductLot
	, CartonBarcode	
	, ProductBarcode
	, ProductQty
FROM
(
	SELECT DISTINCT
		tmp.DeliveryDate
		, tmp.CompanyCode
		, tmp.CompanyName
		, ProductDescription = p.Description
		, tmp.ProductLot
		, tmp.CartonBarcode		
		, tmp.ProductBarcode		
		, tmp.ProductQty		
		, p.ProductCode		
		, p.ProductID
	FROM
		(
			SELECT DISTINCT
				ps.DeliveryDate
				, ps.CompanyCode
				, ps.CompanyName
				, ps.ProductID
				, ps.ProductLot
				, ps.CartonBarcode
				, ProductBarcode = ''
				, ProductQty = COUNT(DISTINCT EncryptedProductBarcode)
			FROM
				#A ps WITH (NOLOCK)
			WHERE
				CartonBarcode IS NOT NULL 
				AND CartonBarcode != ''
			GROUP BY
				ps.DeliveryDate
				, ps.CompanyCode
				, ps.CompanyName
				, ps.ProductID
				, ps.ProductLot
				, ps.CartonBarcode
		)tmp
		LEFT JOIN dbo.Products p WITH (NOLOCK) 
			ON p.ProductID = tmp.ProductID
			AND p.IsDeleted = 0
	

	UNION

	SELECT DISTINCT
		tmp.DeliveryDate
		, tmp.CompanyCode
		, tmp.CompanyName
		, ProductDescription = p.Description
		, tmp.ProductLot
		, tmp.CartonBarcode		
		, tmp.ProductBarcode
		, tmp.ProductQty		
		, p.ProductCode		
		, p.ProductID
	FROM
		(
			SELECT DISTINCT
				ps.DeliveryDate
				, ps.CompanyCode
				, ps.CompanyName
				, ps.ProductID
				, ps.ProductLot
				, CartonBarcode = ''
				, ProductBarcode = ps.EncryptedProductBarcode
				, ProductQty = 1
			FROM
				#A ps WITH (NOLOCK)
			WHERE
				CartonBarcode IS NULL OR CartonBarcode = ''
		)tmp
		LEFT JOIN dbo.Products p WITH (NOLOCK) 
			ON p.ProductID = tmp.ProductID
			AND p.IsDeleted = 0
) AS A
ORDER BY 
	DeliveryDate
	, CompanyCode
	, CompanyName
	, ProductDescription
	, ProductLot
	, CartonBarcode	
END


IF (@Sheet = 3)
BEGIN
--SELECT DETAIL QR CODE
SELECT DISTINCT 
	DH.[DeliveryDate]
	,StrDeliveryDate = Format(DH.[DeliveryDate],'dd/MM/yyyy')
	,DH.[CompanyCode]
	,DH.[CompanyName]	
	,ProductDescription = P.Description
	,DH.ProductLot
	,[CartonBarcode] = ISNULL( DH.[CartonBarcode], '')
	,DH.EncryptedProductBarcode
	,ProductBarcode = DH.EncryptedProductBarcode
	,Quantity = 1
	--,P.ProductCode
	--,P.ProductID
FROM 
	#A DH WITH (NOLOCK)
	LEFT JOIN Products P WITH (NOLOCK)
		ON DH.ProductID = P.ProductID
WHERE
	(@_CompanyCode = '' OR DH.CompanyCode = @_CompanyCode)
	AND (@_FromDate IS NULL OR DH.DeliveryDate >= @_FromDate)
	AND (@_ToDate IS NULL OR DH.DeliveryDate <= @_ToDate)
	AND DH.CompanyCode IS NOT NULL
	AND DH.DeliveryDate IS NOT NULL

ORDER BY 
	DH.DeliveryDate
	, DH.CompanyCode
	, DH.CompanyName
	, P.Description
	, DH.ProductLot
END
END