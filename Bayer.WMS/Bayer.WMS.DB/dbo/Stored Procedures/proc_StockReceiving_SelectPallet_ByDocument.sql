﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceiving_SelectPallet_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description, SD.BatchCode ,RM.PalletCode ASC)
	,PalletCode = RM.PalletCode
	,LocationCode = ISNULL(ZL.LocationName,'')
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,ProductLot = SD.BatchCode
	,RM.Status
	,RM.Quantity AS PalletQuantity
FROM
	StockReceivingDetailSplits RM WITH (NOLOCK)
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RM.ProductID = PD.ProductID
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON RM.[PalletCode] = P.PalletCode
	LEFT JOIN ZoneLocations ZL WITH (NOLOCK) 
		ON ZL.LocationCode = P.LocationCode
	LEFT JOIN [dbo].[StockReceivingDetails] SD WITH (NOLOCK) 
		ON RM.[StockReceivingCode] = SD.[StockReceivingCode]
		AND RM.[LineNbr] = SD.[LineNbr]
WHERE
	RM.StockReceivingCode = @_DocumentNbr
ORDER BY
	PD.Description, SD.BatchCode ,RM.PalletCode
END