﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_SplitNote_Update_SplittedQty]
	@SplitCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @ProductID INT
	, @BatchCode VARCHAR(255)
	, @SplittedQty DECIMAL(18, 2)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_SplitCode VARCHAR(255) = @SplitCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_BatchCode VARCHAR(255) = @BatchCode
	DECLARE @_SplittedQty DECIMAL(18, 2) = @SplittedQty
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	
	SELECT
		RowNbr = ROW_NUMBER() OVER(ORDER BY snd.Quantity DESC)
		, *
	INTO #tmp
	FROM
		dbo.SplitNoteDetails snd WITH (NOLOCK)
	WHERE
		snd.SplitCode = @_SplitCode
		AND snd.CompanyCode = @_CompanyCode
		AND snd.ProductID = @_ProductID
		AND snd.BatchCode = @_BatchCode

	IF (SELECT COUNT(1) FROM #tmp) > 1
		BEGIN
			DECLARE @_I INT = 1
			DECLARE @_Max INT = (SELECT MAX(RowNbr) FROM #tmp)
			DECLARE @_DeliveryCode VARCHAR(255) 
			DECLARE @_Qty DECIMAL(18, 2)
			DECLARE @_ReturnedQty DECIMAL(18, 2)
			DECLARE @_TempSplittedQty DECIMAL(18, 2)

			WHILE @_I <= @_Max
				BEGIN
					SELECT @_DeliveryCode = DeliveryCode, @_Qty = Quantity, @_ReturnedQty = ISNULL(ReturnedQty, 0) FROM #tmp WHERE RowNbr = @_I

					SET @_TempSplittedQty = CASE WHEN @_SplittedQty >= @_Qty + @_ReturnedQty THEN @_Qty + @_ReturnedQty ELSE @_SplittedQty + @_ReturnedQty END
					SET @_SplittedQty = CASE WHEN @_SplittedQty > 0 THEN @_SplittedQty - (@_TempSplittedQty - @_ReturnedQty) ELSE 0 END

					PRINT @_TempSplittedQty
					PRINT @_SplittedQty
					PRINT '-----'

					UPDATE dbo.SplitNoteDetails
					SET
						SplittedQty = @_TempSplittedQty
						, Status = CASE WHEN @_TempSplittedQty - @_ReturnedQty = Quantity THEN 'C' ELSE 'I' END
						, UpdatedBy = @_UserID
						, UpdatedBySitemapID = @_SitemapID
						, UpdatedDateTime = @_Date
					WHERE
						SplitCode = @_SplitCode
						AND DeliveryCode = @_DeliveryCode
						AND CompanyCode = @_CompanyCode
						AND ProductID = @_ProductID
						AND BatchCode = @_BatchCode
					
					SET @_I = @_I + 1
				END
		END
	ELSE
		UPDATE dbo.SplitNoteDetails
		SET
			SplittedQty = @_SplittedQty + ISNULL(ReturnedQty, 0)
			, Status = CASE WHEN @_SplittedQty = Quantity THEN 'C' ELSE 'I' END
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE
			SplitCode = @_SplitCode
			AND CompanyCode = @_CompanyCode
			AND ProductID = @_ProductID
			AND BatchCode = @_BatchCode
	
	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="SplitNoteDetail">' 
		+ (SELECT * FROM dbo.SplitNoteDetails WHERE SplitCode = @_SplitCode AND CompanyCode = @_CompanyCode AND ProductID = @_ProductID AND BatchCode = @_BatchCode FOR XML PATH(''))
		+ '</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'SplitNoteDetails', @_Data, 'UPD', @_Method, @_Date, @_Date

	IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.SplitNoteDetails WHERE SplitCode = @_SplitCode AND ISNULL(SplittedQty, 0) != Quantity)
		UPDATE dbo.SplitNoteHeaders
		SET
			Status = 'C'
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE
			SplitCode = @_SplitCode
	ELSE
		UPDATE dbo.SplitNoteHeaders
		SET
			Status = 'I'
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE
			SplitCode = @_SplitCode

	INSERT INTO dbo.DeliveryOrderExecutors
	( 
		DOImportCode
		, UserID
		, Type 
	)
	VALUES
	(
		@_DOImportCode
		, @_UserID
		, 'S'
	)
END