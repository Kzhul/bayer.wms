﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterialSplit_Select_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

    --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ASC)
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,P.CompanyCode
	,NeedQty = ISNULL(RM.RequestQty,0) - ISNULL(RM.[PickedQty],0)
	,RequestQty = ISNULL(RM.[RequestQty],0)
	,PickedQty = ISNULL(RM.[PickedQty],0)
	,SUM(ISNULL(PS.Quantity, 1)) AS PalletQuantity
	,RM.UOM
--INTO #TBPick
FROM
	RequestMaterialLines RM WITH (NOLOCK) 
	JOIN RequestMaterialLineSplits PS WITH (NOLOCK) 
		ON RM.ProductID = PS.ProductID
		AND RM.DocumentNbr = PS.DocumentNbr
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON PS.PalletCode = P.PalletCode
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RM.ProductID = PD.ProductID
WHERE
	--P.Status = 'N'--Trạng thái pallet là mới
	--AND 
	RM.DocumentNbr = @_DocumentNbr
GROUP BY
	PD.ProductID
	,P.CompanyCode
	,RM.RequestQty
	,RM.[PickedQty]
	,RM.LineNbr
	,PD.ProductCode
	,PD.Description
	,RM.UOM
ORDER BY PD.Description
END