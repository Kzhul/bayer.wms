﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[proc_Pallets_Clear_Audit]
	@PalletCode VARCHAR(255)
	, @ProductLot VARCHAR(255) = NULL
	, @NotProductLot VARCHAR(255) = NULL
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	--Bộ phận soạn hàng sử dụng store này khi soạn hàng
	--Clear toàn bộ data khách hàng liên quan, 
	--Không clear thủ kho và xe nâng

	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_ProductLot VARCHAR(255) = NULLIF(@ProductLot, '')
	DECLARE @_NotProductLot VARCHAR(255) = NULLIF(@NotProductLot, '')
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'CLR:' + @PalletCode
	
	--WriteHistory
	INSERT INTO [PalletStatusesHistory]
		([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime]
           ,[DOImportCode]
	      ,[ReferenceNbr]
	      ,[CompanyCode]
           )
     
	( 
		SELECT DISTINCT
		   PS.[PalletCode]
	      ,PS.[CartonBarcode]
	      ,PS.[ProductBarcode]
	      ,PS.[EncryptedProductBarcode]
	      ,PS.[ProductID]
	      ,PS.[ProductLot]
	      ,PS.[Qty]
	      ,PS.[CreatedBy]
	      ,PS.[CreatedBySitemapID]
	      ,PS.[CreatedDateTime]
	      ,PS.[UpdatedBy]
	      ,PS.[UpdatedBySitemapID]
	      ,PS.[UpdatedDateTime]
	      ,P.[DOImportCode]
	      ,P.[ReferenceNbr]
	      ,P.[CompanyCode]
	   	FROM 
	   		dbo.PalletStatuses PS
	   		LEFT JOIN Pallets P 
	   			ON PS.PalletCode = P.PalletCode
	   	WHERE 
			PS.PalletCode = @_PalletCode
			AND (@_ProductLot IS NULL OR ProductLot = @_ProductLot) 
			AND (@_NotProductLot IS NULL OR ProductLot != @_NotProductLot)
	)
	--WriteHistory_DONE

	--Move to Default Pallet
	UPDATE dbo.PalletStatuses SET PalletCode = CASE WHEN @_PalletCode LIKE 'PW%' THEN 'PW071799999'
														WHEN @_PalletCode LIKE 'PA%' THEN 'PA071799999'
														WHEN @_PalletCode LIKE 'PP%' THEN 'PP071799999'
														WHEN @_PalletCode LIKE 'PF%' THEN 'PF071799999'
														ELSE @_PalletCode
														END
	WHERE 
		PalletCode = @_PalletCode 
		AND (@_ProductLot IS NULL OR ProductLot = @_ProductLot) 
		AND (@_NotProductLot IS NULL OR ProductLot != @_NotProductLot)
		--AND EncryptedProductBarcode != ''

	--Clear pallet material
	DELETE FROM dbo.PalletStatuses 
	WHERE 
		PalletCode = @_PalletCode 
		AND (@_ProductLot IS NULL OR ProductLot = @_ProductLot) 
		AND (@_NotProductLot IS NULL OR ProductLot != @_NotProductLot)
		AND ISNULL(EncryptedProductBarcode,'') = ''

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallet">' 
		+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode FOR XML PATH(''))
		+ '</BaseEntity>'

	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'CLR', @_Method, @_Date, @_Date, @_AuditDescription

	UPDATE dbo.Pallets 
	SET 
		DOImportCode = NULL
		, ReferenceNbr = NULL
		, CompanyCode = NULL		
		, [LocationCode] = NULL
		, [LocationSuggestion] = NULL
		, [BatchCode] = NULL
		, [BatchCodeDistributor] = NULL
		, [ImportStatus] = NULL
		, [Weigh] = NULL
		, [CartonNumber] = NULL
		, WarehouseKeeper = NULL
		, WarehouseVerifyDate = NULL
		, Driver = NULL
		, DriverReceivedDate = NULL
		, LocationPutDate = NULL
		, Status = 'N' 
	WHERE 
		PalletCode = @_PalletCode

	SELECT * FROM dbo.Pallets 
	WHERE PalletCode = @_PalletCode

	--UPDATE PreparedQty
	EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity] @_PalletCode
END