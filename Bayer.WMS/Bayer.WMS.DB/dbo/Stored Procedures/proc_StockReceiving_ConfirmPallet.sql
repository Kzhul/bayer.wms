﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceiving_ConfirmPallet]
	@DocumentNbr NVARCHAR(50)
	, @LineNbr INT
	, @LocationCode VARCHAR(255)
	, @PalletCode VARCHAR(255)
	, @ProductLot VARCHAR(255)
	, @ProductID INT
	, @Quantity INT
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_LineNbr INT = @LineNbr
	DECLARE @_LocationCode VARCHAR(255) = @LocationCode
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_ProductLot VARCHAR(255) = @ProductLot
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_Quantity INT = @Quantity
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()

	--1.Insert vào bảng [dbo].[StockReceivingDetailSplits]
	--2.Update trạng thái pallet, vị trí pallet
	--3.Insert vào bảng PalletStatus
	--4.Update vào bảng [dbo].[StockReceivingDetail]

--1.Insert vào bảng [dbo].[StockReceivingDetailSplits]
INSERT INTO [dbo].[StockReceivingDetailSplits]
           ([StockReceivingCode]
           ,[LineNbr]
           ,[ProductID]
           ,[PalletCode]
           ,[ProductLot]
           ,[Quantity]
           ,[ReceiverID]
           ,[ReceiveDateTime]
           ,[ForkliftID]
           ,[ForkliftDateTime]
           ,[SetLocationDateTime]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
SELECT
	[StockReceivingCode] = @_DocumentNbr
    ,[LineNbr] = @_LineNbr
    ,[ProductID] = @_ProductID
    ,[PalletCode] = @_PalletCode
    ,[ProductLot] = @_ProductLot
    ,[Quantity] = @_Quantity
    ,[ReceiverID] = @_UserID
    ,[ReceiveDateTime] = @_Date
    ,[ForkliftID] = 0
    ,[ForkliftDateTime] = NULL
    ,[SetLocationDateTime] = NULL
    ,[Status] = 'B'
    ,[CreatedBy] = @_UserID
    ,[CreatedBySitemapID] = @_SitemapID
    ,[CreatedDateTime] = @_Date
    , UpdatedBy = @_UserID
	, UpdatedBySitemapID = @_SitemapID
	, UpdatedDateTime = @_Date


--2.Update trạng thái pallet, vị trí pallet
	UPDATE Pallets
	SET 
		[Status] = 'B'
		, ReferenceNbr = @_DocumentNbr
		, LocationCode = @_LocationCode
		, LocationPutDate = GETDATE()
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
		, WarehouseKeeper  = @_UserID
		, WarehouseVerifyDate = GETDATE()
		, BatchCode = @_ProductLot
	WHERE
		PalletCode = @_PalletCode


--3.Insert vào bảng PalletStatus
	IF NOT EXISTS (
		SELECT * 
		FROM 
			dbo.PalletStatuses
		WHERE
			PalletCode = @_PalletCode
			AND ProductLot = @_ProductLot
	)
	BEGIN
		INSERT INTO [dbo].[PalletStatuses]
           ([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
     SELECT
		[PalletCode] = @_PalletCode
        ,[CartonBarcode] = ''
        ,[ProductBarcode] = ''
        ,[EncryptedProductBarcode] = NULL
        ,[ProductID] = @_ProductID
        ,[ProductLot] = @_ProductLot
        ,[Qty] = @_Quantity
        ,[CreatedBy] = @_UserID
        ,[CreatedBySitemapID] = @_SitemapID
        ,[CreatedDateTime] = @_Date
        , UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	END
	ELSE
	BEGIN
		UPDATE dbo.PalletStatuses
		SET
			Qty = @Quantity
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE
			PalletCode = @_PalletCode
			AND ProductLot = @_ProductLot
	END

--4.Update vào bảng [dbo].[StockReceivingDetail]
	UPDATE dbo.StockReceivingDetails
	SET
		QuantityReceived = ISNULL(QuantityReceived,0) + @Quantity
		, PalletReceived = ISNULL(PalletReceived,0) + 1
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		StockReceivingCode = @_DocumentNbr
		AND LineNbr = @_LineNbr
	--Update AUDIT REASON and OLD QUANTITY HERE


END