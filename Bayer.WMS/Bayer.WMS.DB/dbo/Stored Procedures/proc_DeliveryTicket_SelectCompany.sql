﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryTicket_SelectCompany]
	@DOImportCode VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @DeliveryCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	, @_CompanyCode VARCHAR(255) = @CompanyCode
	, @_DeliveryCode VARCHAR(255) = @DeliveryCode

	SELECT TOP 1 
		C.CompanyCode, 
		C.CompanyName 
	FROM 
		dbo.Companies C WITH (NOLOCK)
		JOIN DeliveryTicketHeaders DH WITH (NOLOCK)
			ON C.CompanyCode = DH.CompanyCode
		JOIN DeliveryTicketDetails DD WITH (NOLOCK)
			ON DH.DeliveryTicketCode = DD.DeliveryTicketCode
		JOIN [dbo].[PrepareNoteHeaders] PH WITH (NOLOCK)
			ON DD.PrepareCode = PH.PrepareCode
	WHERE 
		(@_CompanyCode = '' OR C.CompanyCode = @_CompanyCode)
		AND (@_DOImportCode = '' OR PH.DOImportCode = @_DOImportCode)
		AND (@_DeliveryCode = '' OR DH.DeliveryTicketCode = @_DeliveryCode)
END