﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Barcode_Generate]
	@CreateBy INT
	, @CreatedBySitemapID INT
	, @UpdatedBy INT
	, @UpdatedBySitemapID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Year INT = CONVERT(INT, RIGHT(YEAR(GETDATE()), 2))
	DECLARE @_CreateBy INT = @CreateBy
	DECLARE @_CreatedBySitemapID INT = @CreatedBySitemapID
	DECLARE @_UpdatedBy INT = @UpdatedBy
	DECLARE @_UpdatedBySitemapID INT = @UpdatedBySitemapID
	DECLARE @_MaxSequence INT

	SET @_MaxSequence = ISNULL((SELECT MAX(Sequence) FROM dbo.BarcodeBank WITH (NOLOCK) WHERE Year = @_Year), 0)

	DECLARE @_i INT = 1
	WHILE @_i <= 100000
		BEGIN
			INSERT INTO dbo.BarcodeBank
			( 
				Barcode
			    , Year
			    , Sequence
				, IsExport
			    , CreatedBy
			    , CreatedBySitemapID
			    , CreatedDateTime
			    , UpdatedBy
			    , UpdatedBySitemapID
			    , UpdatedDateTime
			)
			VALUES  
			( 
				CONVERT(VARCHAR(4), @_Year) + RIGHT('00000000' + CONVERT(VARCHAR(255), @_MaxSequence + @_i), 8)
				, @_Year
				, @_MaxSequence + @_i
				, 0
				, @_CreateBy
				, @_CreatedBySitemapID
				, GETDATE()
				, @_UpdatedBy
				, @_CreatedBySitemapID
				, GETDATE()
			)

			SET @_i = @_i + 1
		END
END