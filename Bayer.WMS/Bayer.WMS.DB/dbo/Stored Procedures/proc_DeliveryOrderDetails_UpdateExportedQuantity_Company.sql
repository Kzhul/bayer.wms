﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity]
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_UpdateExportedQuantity_Company]
	@CompanyCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	IF(@_CompanyCode != '')
	BEGIN
		--RESET
		UPDATE DeliveryNoteDetails
		SET ExportedQty = 0
		FROM
			DeliveryNoteDetails DO WITH (NOLOCK)
		WHERE
			DO.DOCode = @_DOImportCode

		SELECT DISTINCT
			PS.ProductLot
			,PS.ProductID
			,ExportedQty = ISNULL(COUNT(DISTINCT ps.ProductBarcode),0)	
		INTO #A
		FROM 
			PalletStatusesExport PS WITH (NOLOCK)
			LEFT JOIN Pallets p WITH (NOLOCK)
				ON ps.PalletCode = p.PalletCode
		WHERE
			P.DOImportCode = @_DOImportCode
		GROUP BY 
			PS.ProductLot
			,PS.ProductID

		IF EXISTS (SELECT * FROM #A)
		BEGIN
			UPDATE DeliveryNoteDetails
			SET ExportedQty = ISNULL(A.ExportedQty,0)
			FROM
				DeliveryNoteDetails DO
				LEFT JOIN #A AS A
					ON DO.BatchCode = A.ProductLot
					AND DO.ProductID = A.ProductID
			WHERE
				DO.DOCode = @_DOImportCode
		END
	END
END