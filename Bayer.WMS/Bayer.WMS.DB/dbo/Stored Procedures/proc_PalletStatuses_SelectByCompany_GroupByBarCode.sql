﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectByCompany_GroupByBarCode]
	@CompanyCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_Barcode VARCHAR(255) = @CompanyCode

	SELECT DISTINCT
		# = ROW_NUMBER() OVER(ORDER BY pd.Description, tmp.ProductLot ASC),
		pd.ProductCode				
		, tmp.Quantity
		, CartonOddQuantity = CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) + ' /' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
		, tmp.ProductLot
		, pd.Description
		, ProductDescription = pd.Description
		, pd.PackingType
		, pd.ProductID
	FROM
		Pallets P WITH (NOLOCK)
		LEFT JOIN 
		(
			SELECT
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
				, Quantity = SUM(ISNULL(ps.Qty, 1))
				, OddQuantity = SUM(CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN NULL ELSE ISNULL(ps.Qty, 1) END)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_Barcode
			GROUP BY
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
		) tmp
			ON P.PalletCode = tmp.PalletCode
		LEFT JOIN 
		(
			SELECT
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
				, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_Barcode
				AND CartonBarcode IS NOT NULL 
				AND CartonBarcode != ''
			GROUP BY
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
		) tmpC
			ON P.PalletCode = tmpC.PalletCode
			AND tmp.ProductID = tmpC.ProductID
			AND tmp.ProductLot = tmpC.ProductLot
		LEFT JOIN dbo.Products pd WITH (NOLOCK) ON pd.ProductID = tmp.ProductID
												  AND p.IsDeleted = 0
		WHERE
			P.CompanyCode = @_Barcode
	ORDER BY pd.Description, tmp.ProductLot
END