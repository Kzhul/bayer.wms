﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectByCompany_GroupByBarCode]
	@CompanyCode VARCHAR(255)
	,@DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_Barcode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	SELECT DISTINCT
		# = ROW_NUMBER() OVER(ORDER BY pd.Description, DO.BatchCode ASC)
		, ProductLot = DO.BatchCode
		, ProductDescription = pd.Description		
		, DOQuantity = ISNULL(DO.DOQuantity,0)
		, PreparedQty = ISNULL(DO.PreparedQty,0)
		, NeedPrepareQuantity = ISNULL(DO.DOQuantity,0) - ISNULL(DO.PreparedQty,0)
		, DeliveredQty = ISNULL(DO.DeliveredQty,0)
		, Quantity = ISNULL(tmp1.Quantity,0)
		, CartonOddQuantity = CASE WHEN pd.PackingType = 'C' THEN 
																CASE WHEN ISNULL(tmpC.CartonQuantity,0) = 0 AND ISNULL(tmp.OddQuantity, 0) = 0
																	THEN ''
																   WHEN ISNULL(tmp.OddQuantity, 0) = 0
																	THEN CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
																		+ 'T'
																	ELSE 
																		CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
																		+ 'T / ' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
																	END

										WHEN pd.PackingType = 'B' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' B' 
										WHEN pd.PackingType = 'S' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' X' 
										WHEN pd.PackingType = 'A' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' C'
										ELSE '0' END
		
		, pd.PackingType
		, pd.ProductID	
		, pd.Description	
		, pd.ProductCode
	INTO #TB
	FROM
		DeliveryOrderSum DO WITH (NOLOCK)
		LEFT JOIN dbo.Products pd WITH (NOLOCK) 
			ON pd.ProductID = DO.ProductID
		--Get total quantity
		LEFT JOIN 
		(
			SELECT
				ps.ProductID
				, ps.ProductLot
				, Quantity = COUNT(DISTINCT ps.ProductBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_Barcode
				AND pl.DOImportCode = @_DOImportCode
			GROUP BY
				ps.ProductID
				, ps.ProductLot
		) tmp1
			ON DO.ProductID = tmp1.ProductID
			AND DO.BatchCode = tmp1.ProductLot
		--Get odd quantity
		LEFT JOIN 
		(
			SELECT
				ps.ProductID
				, ps.ProductLot
				, OddQuantity = COUNT(DISTINCT ps.ProductBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_Barcode
				AND pl.DOImportCode = @_DOImportCode
				AND (ps.CartonBarcode IS NULL OR CartonBarcode = '')
			GROUP BY
				ps.ProductID
				, ps.ProductLot
		) tmp
			ON tmp.ProductID = tmp1.ProductID
			AND tmp.ProductLot = tmp1.ProductLot
		--Get carton quantity
		LEFT JOIN 
		(
			SELECT
				ps.ProductID
				, ps.ProductLot
				, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_Barcode
				AND pl.DOImportCode = @_DOImportCode
				AND CartonBarcode IS NOT NULL 
				AND CartonBarcode != ''
			GROUP BY
				ps.ProductID
				, ps.ProductLot
		) tmpC
			ON tmp1.ProductID = tmpC.ProductID
			AND tmp1.ProductLot = tmpC.ProductLot
	WHERE
		DO.DOImportCode = @_DOImportCode
		AND DO.CompanyCode = @_Barcode
	--UPDATE #TB
	--SET DOQuantity = ISNULL((
	--					SELECT SUM(Quantity) 
	--					FROM 
	--						DeliveryOrderSum DO WITH (NOLOCK)
	--					WHERE 
	--						DO.[DeliveryDate] >= CONVERT(DATE,GETDATE())
	--						AND DO.ProductID = #TB.ProductID
	--						AND DO.BatchCode = #TB.ProductLot
	--						AND DO.CompanyCode = @_Barcode
	--				 ),0)
	--UPDATE #TB
	--SET NeedPrepareQuantity = DOQuantity - Quantity

	SELECT DISTINCT 
		* 
	FROM 
		#TB AS A WITH (NOLOCK)
	WHERE
		(DOQuantity - DeliveredQty) > 0 -- Chỉ lấy hàng chưa giao
	ORDER BY 
		A.NeedPrepareQuantity DESC,
		A.Description, 
		A.ProductLot
		
END