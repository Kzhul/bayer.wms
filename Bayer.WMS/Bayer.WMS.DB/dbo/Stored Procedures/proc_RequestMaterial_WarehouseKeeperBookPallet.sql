﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_WarehouseKeeperBookPallet]
	@DocumentNbr NVARCHAR(50)
	,@UserID INT
	,@SitemapID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID

	--1.Lấy danh sách pallet đang N
	--2.Cập nhật Split về trạng thái booking
	--3.Cập nhật Pallet về trạng thái booking nếu pallet này là N
	--Trường hợp để quá lâu mà ko lấy, pallet đã được lấy thì coi như phải lấy pallet khác
	
	SELECT DISTINCT 
		S.*
	INTO #A
	FROM
		[RequestMaterialLineSplits] S WITH (NOLOCK)
		JOIN Pallets P WITH (NOLOCK)
			ON S.PalletCode = P.PalletCode
	WHERE
		DocumentNbr = @_DocumentNbr
		AND S.Status = 'N'
		AND P.Status = 'N'
		
	UPDATE [RequestMaterialLineSplits]
	SET 
		[Status] = 'B'
		,[UpdatedBy] = @_UserID
		,[UpdatedBySitemapID] = @_SitemapID
		,[UpdatedDateTime] = GETDATE()
		,WarehouseKeeperAllowDropID = @_UserID
		,WarehouseVerifyAllowDropTime = GETDATE()
	FROM 
		[RequestMaterialLineSplits] AS S
		JOIN #A AS A
			ON S.DocumentNbr = A.DocumentNbr
			AND S.LineNbr = A.LineNbr
			AND S.PalletCode = A.PalletCode

	UPDATE Pallets 
	SET 
		[Status] = 'B' --Booking
		, ReferenceNbr = @_DocumentNbr
		, [UpdatedBy] = @_UserID
		, [UpdatedBySitemapID] = @_SitemapID
		, [UpdatedDateTime] = GETDATE()
	FROM 
		Pallets AS S
		JOIN #A AS A
			ON S.PalletCode = A.PalletCode
END