﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_AuditTrails_Select]
	@FromDate DATE
	, @ToDate DATE
	, @Code NVARCHAR(150)
	, @User NVARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_FromDate DATE = @FromDate
	DECLARE @_ToDate DATE = @ToDate

	SELECT DISTINCT TOP 1000
		[User] = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
		, Action = R.Description
		, tmp.Date
		, tmp.DateTime
		, Sitemap = ISNULL(sm.Description,'')
		, tmp.Description
	FROM
		(
			SELECT
				at.UserID
				, at.SitemapID
				, at.Action
				, at.Method
				, at.Date
				, at.DateTime
				, at.Description
			FROM
				dbo.AuditTrails at WITH (NOLOCK)
			WHERE
				at.Date BETWEEN @_FromDate AND @_ToDate
				AND (@Code = '' OR (Charindex(@Code, CAST(at.[Data] AS NVARCHAR(MAX))) > 0) OR at.Description LIKE '%' + @Code + '%')
		)tmp
		JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = tmp.UserID
		LEFT JOIN dbo.Sitemaps sm WITH (NOLOCK) ON sm.SitemapID = tmp.SitemapID
		LEFT JOIN dbo.[References] R
			ON R.GroupCode = 'AuditTrailAction'
			AND R.RefValue = tmp.Action
	WHERE 
		@User = ''
		OR (ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')) LIKE '%' + @User + '%' 
		OR u.Username = @User
	ORDER BY tmp.DateTime DESC




END