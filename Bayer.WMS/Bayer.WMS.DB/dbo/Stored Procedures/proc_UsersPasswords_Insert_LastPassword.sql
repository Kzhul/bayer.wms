﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_UsersPasswords_Insert_LastPassword]
	@UserID INT
	, @Password VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_UserID INT = @UserID
	DECLARE @_Password VARCHAR(255) = @Password

	;WITH cte AS
	(
		SELECT
			ID
		FROM
			(
				SELECT
					RowNum = ROW_NUMBER() OVER(ORDER BY ID DESC)
					, ID
				FROM
					dbo.UsersPasswords WITH (NOLOCK)
				WHERE
					UserID = @_UserID
			)tmp
		WHERE
			tmp.RowNum = 10
	)

    DELETE FROM dbo.UsersPasswords WHERE ID IN (SELECT cte.ID FROM cte)

	INSERT INTO dbo.UsersPasswords
	(
		UserID
		, Password
		, LastUpdated )
	VALUES
	(
		@_UserID
		, @_Password
		, GETDATE()
	)
END