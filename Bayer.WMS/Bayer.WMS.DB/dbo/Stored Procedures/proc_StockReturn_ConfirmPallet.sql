﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReturn_ConfirmPallet]
	@DocumentNbr NVARCHAR(50)
	, @LocationCode VARCHAR(255)
	, @PalletCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_LocationCode VARCHAR(255) = @LocationCode
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()

	--2.Update trạng thái pallet, vị trí pallet
	--3.Insert vào bảng PalletStatus
	--4.Update vào bảng [dbo].[StockReceivingDetail]

--2.Update trạng thái pallet, vị trí pallet
	UPDATE Pallets
	SET 
		[Status] = 'B'
		, ReferenceNbr = @_DocumentNbr
		, LocationCode = @_LocationCode
		, LocationPutDate = GETDATE()
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
		, WarehouseKeeper  = @_UserID
		, WarehouseVerifyDate = GETDATE()
	WHERE
		PalletCode = @_PalletCode


--3.Insert vào bảng PalletStatus
		INSERT INTO [dbo].[PalletStatuses]
           ([PalletCode]
           ,[CartonBarcode]
           ,[ProductBarcode]
           ,[EncryptedProductBarcode]
           ,[ProductID]
           ,[ProductLot]
           ,[Qty]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
     SELECT
		[PalletCode] = @_PalletCode
        ,[CartonBarcode] = ''
        ,[ProductBarcode] = ''
        ,[EncryptedProductBarcode] = NULL
        ,[ProductID] = SR.ProductID
        ,[ProductLot] = SR.BatchCode
        ,[Qty] = SR.ReturnQty
        ,[CreatedBy] = @_UserID
        ,[CreatedBySitemapID] = @_SitemapID
        ,[CreatedDateTime] = @_Date
        , UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	FROM
		StockReturnDetails SR
	WHERE
		StockReturnCode = @_DocumentNbr
		AND PalletCode = @_PalletCode


--4.Update vào bảng [dbo].[StockReturnDetail]
	UPDATE dbo.StockReturnDetails
	SET
		ReceivedBy = @_UserID
		, ReceivedDate = GETDATE()
		, ReceivedQty = ReturnQty
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
		, Status = 'C'
	WHERE
		StockReturnCode = @_DocumentNbr
		AND PalletCode = @_PalletCode




--Update AUDIT REASON and OLD QUANTITY HERE


END