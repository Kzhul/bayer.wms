﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryNoteDetailSplits_Insert]
	@PalletCode VARCHAR(255)
	,@DOImportCode VARCHAR(255)
	,@ReferenceNbr VARCHAR(255)
	,@UserID INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_ReferenceNbr VARCHAR(255) = @ReferenceNbr
	DECLARE @_UserID VARCHAR(255) = @UserID
	DECLARE @_LocationCode VARCHAR(255) = (SELECT TOP 1 LocationCode FROM Pallets WHERE PalletCode = @_DOImportCode)

IF NOT EXISTS (
	SELECT DISTINCT 
		[PalletCode]
        ,[DOImportCode]
        ,[ReferenceNbr]
	FROM [DeliveryNoteDetailSplits]
	WHERE
		[PalletCode] = @_PalletCode
        AND [DOImportCode] = @_DOImportCode
        AND [ReferenceNbr] = @_ReferenceNbr
)
BEGIN
INSERT INTO [dbo].[DeliveryNoteDetailSplits]
           ([PalletCode]
           ,[DOImportCode]
           ,[ReferenceNbr]
           ,[Status]
           ,[IsDeleted]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime]
           ,[LocationCode]
           ,[LocationSuggestion]
           ,[WarehouseKeeper]
           ,[WarehouseVerifyDate]
           ,[Driver]
           ,[LocationPutDate]
           ,[Received]
           ,[ReceivedDate])
     VALUES
           (
			@_PalletCode
           ,@_DOImportCode
           ,@_ReferenceNbr
           ,'N'
           ,0
           ,@_UserID
           ,0
           ,GETDATE()
           ,@_UserID
           ,0
           ,GETDATE()
           ,@_LocationCode
           ,''
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
		   )
END
ELSE
BEGIN
	UPDATE
		[DeliveryNoteDetailSplits]
	SET IsDeleted = 0
		,[Status] = 'N'
		,UpdatedBy = @_UserID
		,UpdatedDateTime = GETDATE()
	WHERE
		[DOImportCode] = @_DOImportCode
        AND [ReferenceNbr] = @_ReferenceNbr
END
END