﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_DeliveryOrderDetailHistories]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN

DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method

INSERT INTO [dbo].[DeliveryOrderDetailHistories]
           ([DOImportCode]
           ,[LineNbr]
           ,[Delivery]
           ,[ProductID]
           ,[ProductCode]
           ,[ProductName]
           ,[BatchCode]
           ,[BatchCodeDistributor]
           ,[Quantity]
           ,[DOQuantity]
           ,[PreparedQty]
           ,[DeliveredQty]
           ,[RequireReturnQty]
           ,[CompanyCode]
           ,[CompanyName]
           ,[ProvinceSoldTo]
           ,[ProvinceShipTo]
           ,[DeliveryDate]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime]
           ,[ChangeReason])
SELECT DISTINCT
	[DOImportCode]
    ,[LineNbr]
    ,[Delivery]
    ,[ProductID]
    ,[ProductCode]
    ,[ProductName]
    ,[BatchCode]
    ,[BatchCodeDistributor]
    ,[Quantity]
    ,[DOQuantity]
    ,[PreparedQty]
    ,[DeliveredQty]
    ,[RequireReturnQty]
    ,[CompanyCode]
    ,[CompanyName]
    ,[ProvinceSoldTo]
    ,[ProvinceShipTo]
    ,[DeliveryDate]
    ,[Status]
    ,[CreatedBy]
    ,[CreatedBySitemapID]
    ,[CreatedDateTime]
    ,[UpdatedBy] = @_UserID 
    ,[UpdatedBySitemapID] = @_SitemapID
    ,[UpdatedDateTime] = @_SyncDate
    ,[ChangeReason]
FROM
	[dbo].[DeliveryOrderDetails] WITH (NOLOCK)
WHERE
	DOImportCode = @_DOImportCode

DELETE FROM [dbo].[DeliveryOrderDetails]
WHERE
	DOImportCode = @_DOImportCode
END