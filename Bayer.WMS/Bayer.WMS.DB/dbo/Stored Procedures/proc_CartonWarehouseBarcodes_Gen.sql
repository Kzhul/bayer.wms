﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_CartonWarehouseBarcodes_Gen]
	@UserID INT
	, @SitemapID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = ''
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_i INT = 0

	DECLARE @_PrepareCode VARCHAR(255) = ''
	SET @_PrepareCode = 'SH' + FORMAT(GETDATE(),'yyMMdd01')

	DECLARE @_MaxBarcode VARCHAR(255) = (SELECT MAX(Barcode) FROM dbo.CartonWarehouseBarcodes WHERE PrepareCode = @_PrepareCode)
	DECLARE @_LastNumber INT = CASE WHEN @_MaxBarcode IS NULL THEN 0 ELSE CONVERT(INT, REPLACE(@_MaxBarcode, @_PrepareCode + 'C', '')) END
	SET @_Barcode = @_PrepareCode + 'C' + RIGHT('00' + CONVERT(VARCHAR(255), @_LastNumber + 1), 3)
	
	INSERT INTO dbo.CartonWarehouseBarcodes
	(
		Barcode
		, PrepareCode
		, Weight
		, Status
		, CreatedBy
		, CreatedBySitemapID
		, CreatedDateTime
		, UpdatedBy
		, UpdatedBySitemapID
		, UpdatedDateTime
	)
	VALUES
	(
		@_Barcode
		, @_PrepareCode
		, 0
		, NULL
		, @_UserID
		, @_SitemapID
		, @_Date
		, @_UserID
		, @_SitemapID
		, @_Date
	)

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CartonWarehouseBarcodes">' 
		+ (SELECT * FROM dbo.CartonWarehouseBarcodes p WHERE p.Barcode = @_Barcode FOR XML PATH(''))
		+ '</BaseEntity>'
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'CartonWarehouseBarcodes', @_Data, 'INS', '', @_Date, @_Date, @_Barcode

	--SELECT * FROM CartonWarehouseBarcodes WITH (NOLOCK) WHERE Barcode = @_Barcode
	SELECT LastNumber = @_Barcode
END