﻿CREATE PROCEDURE [dbo].[proc_UpdateStatusAllDocumentDOImport]
	@DOImportCode NVARCHAR(250)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	-----------------------------------DeliveryNote------------------------------------------
	EXEC [dbo].[pp_DOImport_Merge_DeliveryNoteHeader] @DOImportCode, @UserID, @SitemapID, @Method
		EXEC [dbo].[pp_DOImport_Merge_DeliveryNoteDetail] @DOImportCode, @UserID, @SitemapID, @Method

	-----------------------------------SplitNote------------------------------------------
	EXEC [dbo].[pp_DOImport_Merge_SplitNoteHeader] @DOImportCode, @UserID, @SitemapID, @Method
	EXEC [dbo].[pp_DOImport_Merge_SplitNoteDetail] @DOImportCode, @UserID, @SitemapID, @Method

	-----------------------------------PrepareNote------------------------------------------
	EXEC [dbo].[pp_DOImport_Merge_PrepareNoteHeader] @DOImportCode, @UserID, @SitemapID, @Method
	EXEC [dbo].[pp_DOImport_Merge_PrepareNoteDetail] @DOImportCode, @UserID, @SitemapID, @Method

	-----------------------------------DeliveryTicket------------------------------------------
	EXEC [dbo].[pp_DOImport_Merge_DeliveryTicketHeader] @DOImportCode, @UserID, @SitemapID, @Method
	EXEC [dbo].[pp_DOImport_Merge_DeliveryTicketDetail] @DOImportCode, @UserID, @SitemapID, @Method


	-----------------------------------UPDATE STATUS------------------------------------------
	EXEC dbo.[proc_UpdateStatusAllDocument] '', @DOImportCode, 'DeliveryNote', 'N'		
	EXEC dbo.[proc_UpdateStatusAllDocument] '', @DOImportCode, 'SplitNote', 'N'		
	EXEC dbo.[proc_UpdateStatusAllDocument] '', @DOImportCode, 'PrepareNote', 'N'		
	EXEC dbo.[proc_UpdateStatusAllDocument] '', @DOImportCode, 'DeliveryTicket', 'N'
END