﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.proc_StockReceivingHeaders_Delete
	@StockReceivingCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_StockReceivingCode VARCHAR(255) = @StockReceivingCode

	DELETE FROM dbo.StockReceivingHeaders WHERE StockReceivingCode = @_StockReceivingCode
	DELETE FROM dbo.StockReceivingDetails WHERE StockReceivingCode = @_StockReceivingCode
END