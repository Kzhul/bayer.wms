﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallet_Select_FEFO]
	@ProductID INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_ProductID INT = @ProductID

    SELECT
		ps.PalletCode
		, ps.ProductID
		, ps.ProductLot
		, ExpiredDate = ISNULL(srd.ExpiredDate, GETDATE())
		, Quantity = SUM(ISNULL(ps.Qty, 1))
	FROM
		dbo.PalletStatuses ps (NOLOCK)
		JOIN dbo.Pallets p (NOLOCK) ON 
			p.PalletCode = ps.PalletCode
			AND p.Status = 'N'
			AND p.IsDeleted = 0
		LEFT JOIN dbo.StockReceivingDetails srd (NOLOCK) 
			ON srd.ProductID = ps.ProductID
			AND srd.BatchCode = ps.ProductLot
	WHERE
		ps.ProductID = @_ProductID
	GROUP BY
		ps.PalletCode
		, ps.ProductID
		, ps.ProductLot
		, srd.ExpiredDate
	ORDER BY
		srd.ExpiredDate ASC
END