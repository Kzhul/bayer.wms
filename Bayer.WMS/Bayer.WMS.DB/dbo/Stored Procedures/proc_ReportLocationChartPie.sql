﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ReportLocationChartPie]
	@ZoneCode VARCHAR(50)
AS

DECLARE @_ZoneCode VARCHAR(50) = @ZoneCode

BEGIN
	SET NOCOUNT ON
--Zone Selection
SELECT DISTINCT
	Z.ZoneCode
	, Z.ZoneName
	, COUNT(DISTINCT L.LocationCode) AS Total
	, COUNT(DISTINCT P.LocationCode) AS CheckedLocation
	, EmptyLocation = COUNT(DISTINCT L.LocationCode) - COUNT(DISTINCT P.LocationCode)
INTO #Z
FROM 
	Zones Z WITH (NOLOCK)
	LEFT JOIN [dbo].[ZoneLines] ZL WITH (NOLOCK)
		ON Z.ZoneCode = ZL.ZoneCode
	LEFT JOIN [dbo].[ZoneLocations] L WITH (NOLOCK)
		ON ZL.ZoneCode = L.ZoneCode
		AND ZL.LineCode = L.LineCode
	LEFT JOIN [dbo].[Pallets] P WITH (NOLOCK)
		ON P.LocationCode = L.LocationCode
		AND P.LocationCode IS NOT NULL
		AND P.LocationCode != ''
WHERE
	(Z.ZoneCode = @_ZoneCode OR @_ZoneCode = '')
GROUP BY
	Z.ZoneCode
	, Z.ZoneName

SELECT 
	ZoneCode
	,ZoneName 
	,ValueName = N'Đang chứa hàng'
	,Value = CheckedLocation
FROM
	#Z

UNION

SELECT 
	ZoneCode
	,ZoneName 
	,ValueName = N'Trống'
	,Value = EmptyLocation
FROM
	#Z
END