﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdateDeliveredQuantity]
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_UpdateDeliveredQuantity]
	@CompanyCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	IF(@_CompanyCode != '')
	BEGIN
		SELECT DISTINCT
			PS.CompanyCode 
			,PS.ProductLot
			,PS.ProductID
			,DeliveredQuantity = COUNT(DISTINCT ps.ProductBarcode)	
		INTO #A
		FROM 
			DeliveryHistories PS WITH (NOLOCK)
		WHERE
			PS.CompanyCode = @_CompanyCode
		GROUP BY 
			PS.CompanyCode 
			,PS.ProductLot
			,PS.ProductID

		UPDATE DeliveryOrderDetails
		SET DeliveredQty = A.DeliveredQuantity
		FROM
			DeliveryOrderDetails DO
			LEFT JOIN #A AS A
				ON DO.BatchCode = A.ProductLot
				AND DO.CompanyCode = A.CompanyCode
				AND DO.ProductID = A.ProductID
				AND DATEDIFF(DD,DO.DeliveryDate,GETDATE()) >= 0
		WHERE
			DO.CompanyCode = @_CompanyCode
	END
END