﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Barcode_Tracking]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type

	CREATE TABLE #tmp
	(
		LocationCode NVARCHAR(255)
		, PalletCode NVARCHAR(255)
		, CartonBarcode NVARCHAR(255)
		, ProductBarcode NVARCHAR(255)
		, EncryptedProductBarcode NVARCHAR(255)
		, ProductLot NVARCHAR(255)
		, ProductID INT
		, ProductCode NVARCHAR(255)
		, ProductDescription NVARCHAR(255)
	)

	IF @_Type = 'P'
		BEGIN
			INSERT INTO #tmp
			SELECT DISTINCT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = SUM(ISNULL(ps.Qty,1))
				, EncryptedProductBarcode = ''
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.PalletCode = @_Barcode 
			GROUP BY 
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT DISTINCT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.PalletCode = @_Barcode 
		END
	ELSE IF @_Type = 'C'
		BEGIN
			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.CartonBarcode = @_Barcode 

			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.CartonBarcode = @_Barcode 
		END
	ELSE IF @_Type = 'E'
		BEGIN
			INSERT INTO #tmp
			SELECT TOP 100
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, EncryptedProductBarcode = 'Trong Kho: ' + ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmp
			SELECT TOP 100
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, EncryptedProductBarcode = N'Đã Giao: ' + ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
				
			INSERT INTO #tmp
			SELECT TOP 100
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, EncryptedProductBarcode = N'Đã Xuất: ' + ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
		END
	ELSE IF @_Type = 'L'
		BEGIN
			--------Carton Sample--------
			INSERT INTO #tmp
			SELECT DISTINCT TOP 1000
				PL.LocationCode
				, ps.PalletCode
				, CartonBarcode = N'_Mẫu:' + ISNULL(ps.Barcode,'')
				, ProductBarcode = ''
				, EncryptedProductBarcode = ''
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.CartonBarcodes ps WITH (NOLOCK) 
				LEFT JOIN ProductionPlans pp WITH (NOLOCK) 
					ON ps.ProductLot = pp.ProductLot
				LEFT JOIN dbo.Products p WITH (NOLOCK) 
					ON p.ProductID = pp.ProductID
					AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot = @_Barcode 
				AND ps.[Status] = 'S'

			--------Product Sample--------
			INSERT INTO #tmp
			SELECT DISTINCT TOP 1000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = N'_Mẫu: ' + ISNULL(ps.Barcode,'')
				, EncryptedProductBarcode = N'_Mẫu: ' + ISNULL(ps.EncryptedBarcode,'')
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.ProductBarcodes ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot = @_Barcode 
				AND ps.[Status] = 'S'

			INSERT INTO #tmp
			SELECT DISTINCT
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = SUM(ISNULL(ps.Qty,1))
				, EncryptedProductBarcode = N'Trong Kho'
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot = @_Barcode 
			GROUP BY
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT DISTINCT
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = COUNT(DISTINCT ProductBarcode)
				, EncryptedProductBarcode = N'Đã giao'
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot = @_Barcode 
			GROUP BY
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, EncryptedProductBarcode = N'Đã Giao: ' + ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot = @_Barcode 
				
			INSERT INTO #tmp
			SELECT DISTINCT
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = COUNT(DISTINCT ProductBarcode)
				, EncryptedProductBarcode = N'Đã Xuất'
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot = @_Barcode 
			GROUP BY
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, EncryptedProductBarcode = N'Đã Xuất: ' + ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductLot = @_Barcode 
		END
	ELSE IF @_Type = 'M'
		BEGIN
			DECLARE @ProductID INT = (SELECT TOP 1 ProductID FROM Products WITH (NOLOCK) WHERE ProductCode = @_Barcode)
			INSERT INTO #tmp
			SELECT DISTINCT
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = SUM(ISNULL(ps.Qty,1))
				, EncryptedProductBarcode = N'Trong Kho'
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductID = @ProductID
			GROUP BY
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductID = @ProductID
				
			INSERT INTO #tmp
			SELECT DISTINCT
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ProductBarcode = COUNT(DISTINCT ProductBarcode)
				, EncryptedProductBarcode = N'Đã xuất'
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductID = @ProductID
			GROUP BY
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, p.Description

			INSERT INTO #tmp
			SELECT TOP 10000
				PL.LocationCode
				, ps.PalletCode
				, ps.CartonBarcode
				, ps.ProductBarcode
				, EncryptedProductBarcode = N'Đã Xuất: ' + ps.EncryptedProductBarcode
				, ps.ProductLot
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK) 
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
				LEFT JOIN Pallets PL WITH (NOLOCK) 
					ON ps.PalletCode = PL.PalletCode
			WHERE
				ps.ProductID = @ProductID
		END

	SELECT * FROM #tmp --ORDER BY PalletCode, CartonBarcode, ProductLot, ProductDescription, ProductBarcode
END