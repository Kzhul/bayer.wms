﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_ReportProcess]
	@DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	SELECT DISTINCT
		DO.CompanyCode
		,C.CompanyName
		,DO.ProductID
		,ProductCode = CASE WHEN DO.ProductID != 0 
								THEN P.ProductCode
								ELSE DO.ProductCode
								END
		,ProductName  = CASE WHEN DO.ProductID != 0 
								THEN P.Description
								ELSE DO.ProductName
								END
		,DO.BatchCode	
		,DO.DeliveryDate
		,DOQuantity = SUM(DO.Quantity)
		,PreparedQty = 0
		,DeliveredQty = 0
	INTO #DO
	FROM 
		DeliveryOrderDetails DO WITH(NOLOCK)
		LEFT JOIN Companies C WITH(NOLOCK)
			ON DO.CompanyCode = C.CompanyCode
		LEFT JOIN Products P WITH(NOLOCK)
			ON DO.ProductID = P.ProductID
	WHERE 
		DOImportCode = @_DOImportCode
	GROUP BY
		DO.CompanyCode
		,C.CompanyName
		,DO.ProductID
		,P.ProductCode
		,DO.ProductCode
		,P.Description
		,DO.ProductName
		,DO.BatchCode	
		,DO.DeliveryDate

	UPDATE #DO
	SET PreparedQty = ISNULL((
			SELECT TOP 1 PreparedQty 
			FROM DeliveryOrderDetails WITH (NOLOCK) 
			WHERE 
				DOImportCode = @_DOImportCode
				AND CompanyCode = #DO.CompanyCode
				AND BatchCode = #DO.BatchCode
				AND ProductID = #DO.ProductID
		),0)
	, DeliveredQty = ISNULL((
			SELECT TOP 1 DeliveredQty 
			FROM DeliveryOrderDetails WITH (NOLOCK) 
			WHERE 
				DOImportCode = @_DOImportCode
				AND CompanyCode = #DO.CompanyCode
				AND BatchCode = #DO.BatchCode
				AND ProductID = #DO.ProductID
		),0)
	SELECT * 
	FROM #DO
	ORDER BY 
		CompanyName
		,ProductName
		,BatchCode
END