﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Select_Details]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type

    IF @_Type = 'P'
		SELECT
			# = ROW_NUMBER() OVER(ORDER BY p.Description, p.ProductCode ASC)
			, ps.PalletCode
			, ps.CartonBarcode
			, ps.ProductBarcode
			, ps.EncryptedProductBarcode
			, ps.ProductLot
			, Qty = ISNULL(ps.Qty, 1)
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
		FROM
			dbo.PalletStatuses ps WITH (NOLOCK)
			LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
													  AND p.IsDeleted = 0
		WHERE
			PalletCode = @_Barcode
	ELSE IF @_Type = 'C'
		SELECT
			# = ROW_NUMBER() OVER(ORDER BY p.Description, p.ProductCode ASC)
			, ps.PalletCode
			, ps.CartonBarcode
			, ps.ProductBarcode
			, ps.EncryptedProductBarcode
			, ps.ProductLot
			, Qty = ISNULL(ps.Qty, 1)
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
		FROM
			dbo.PalletStatuses ps WITH (NOLOCK)
			LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
													  AND p.IsDeleted = 0
		WHERE
			CartonBarcode = @_Barcode
	ELSE IF @_Type = 'E'
		SELECT
			# = ROW_NUMBER() OVER(ORDER BY p.Description, p.ProductCode ASC)
			, ps.PalletCode
			, ps.CartonBarcode
			, ps.ProductBarcode
			, ps.EncryptedProductBarcode
			, ps.ProductLot
			, Qty = ISNULL(ps.Qty, 1)
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
		FROM
			dbo.PalletStatuses ps WITH (NOLOCK)
			LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
													  AND p.IsDeleted = 0
		WHERE
			ProductBarcode = @_Barcode
END