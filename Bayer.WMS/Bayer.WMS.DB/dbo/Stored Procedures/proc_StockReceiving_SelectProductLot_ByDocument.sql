﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceiving_SelectProductLot_ByDocument]
	@DocumentNbr NVARCHAR(50)
	,@ProductLot NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_ProductLot NVARCHAR(50) = @ProductLot
	
   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ,SD.BatchCode ASC)
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,ProductLot = SD.BatchCode
	,SD.Status
	,CONVERT(INT,ISNULL(SD.QuantityPlanning,0)) AS RequestQty
	,CONVERT(INT,ISNULL(SD.QuantityReceived,0)) AS PickedQty
	,CONVERT(INT,ISNULL(PP.Quantity,0)) AS PackageQuantity
	,CONVERT(INT,ISNULL(SD.LineNbr,0)) AS LineNbr
FROM
	[dbo].[StockReceivingDetails] SD WITH (NOLOCK) 
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON SD.ProductID = PD.ProductID
	LEFT JOIN [ProductPackings] PP WITH (NOLOCK) 
		ON PP.ProductID = PD.ProductID
		AND PP.[Type] = 'P'
WHERE
	SD.StockReceivingCode = @_DocumentNbr
	AND SD.BatchCode = @_ProductLot
ORDER BY
	PD.Description
	,SD.BatchCode ASC
END