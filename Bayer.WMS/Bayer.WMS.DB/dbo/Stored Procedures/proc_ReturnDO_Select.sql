﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ReturnDO_Select]
	@ReturnDOCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_ReturnDOCode VARCHAR(255) = @ReturnDOCode

	SELECT
		rdoh.ReturnDOCode
		, rdod.DOImportCode
		, rdod.BatchCode
		, rdod.Quantity
		, rdod.CompanyCode
		, RemainQty = rdod.RequestReturnQty
		, ExportedReturnQty = ISNULL(rdod.ExportedReturnQty, 0)
		, ReceivedReturnQty = ISNULL(rdod.ReceivedReturnQty, 0)
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
	FROM
		dbo.ReturnDOHeaders rdoh WITH (NOLOCK)
		JOIN dbo.ReturnDODetails rdod WITH (NOLOCK) ON rdod.ReturnDOCode = rdoh.ReturnDOCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = rdod.ProductID
												  AND p.IsDeleted = 0
	WHERE
		rdoh.ReturnDOCode = @_ReturnDOCode
END