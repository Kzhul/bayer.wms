﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_WHPackaging2]
	@PalletCode VARCHAR(255)
	, @CartonBarcode VARCHAR(255)
	, @ProductBarcode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_CartonBarcode VARCHAR(255) = @CartonBarcode
	DECLARE @_ProductBarcode VARCHAR(255) = @ProductBarcode
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'WPK:' + @CartonBarcode

	UPDATE dbo.PalletStatuses
	SET
		PalletCode = @_PalletCode
		, CartonBarcode = @_CartonBarcode
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		ProductBarcode = @_ProductBarcode
	
	--SET @_Data = 
	--	'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PalletStatus">' 
	--	+ (SELECT * FROM dbo.PalletStatuses ps WHERE ProductBarcode = @_ProductBarcode FOR XML PATH('PalletStatus'))
	--	+ '</BaseEntity>'

	--EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'PalletStatuses', @_Data, 'UPD', @_Method, @_Date, @_Date, @_AuditDescription
	EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity] @_PalletCode
END