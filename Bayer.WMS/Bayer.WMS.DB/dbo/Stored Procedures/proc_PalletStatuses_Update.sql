﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
	, @NewPalletCode VARCHAR(255) = NULL
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type
	DECLARE @_OldPalletCode VARCHAR(255)
    DECLARE @_NewPalletCode VARCHAR(255) = NULLIF(@NewPalletCode, '')
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

	IF @_Type = 'C'
		BEGIN
			SET @_OldPalletCode = (SELECT TOP 1 PalletCode FROM dbo.PalletStatuses WHERE CartonBarcode = @_Barcode)
			IF NOT EXISTS (SELECT TOP 1 PalletCode FROM dbo.PalletStatuses WHERE PalletCode = @_OldPalletCode AND CartonBarcode != @_Barcode)
				BEGIN
					UPDATE dbo.Pallets SET Status = 'N' WHERE PalletCode = @_OldPalletCode

					SET @_Data = 
						'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallets">' 
						+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_OldPalletCode FOR XML PATH(''))
						+ '</BaseEntity>'

					EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'UPD', @_Method, @_Date, @_Date
				END

			UPDATE dbo.PalletStatuses
			SET
				PalletCode = @_NewPalletCode
				, UpdatedBy = @_UserID
				, UpdatedBySitemapID = @_SitemapID
				, UpdatedDateTime = @_Date
			WHERE
				CartonBarcode = @_Barcode

			SET @_Data = 
				'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PalletStatus">' 
				+ (SELECT * FROM dbo.PalletStatuses ps WHERE ps.CartonBarcode = @_Barcode FOR XML PATH('PalletStatus'))
				+ '</BaseEntity>'
		END
	ELSE IF @_Type = 'E'
		BEGIN
			SET @_OldPalletCode = (SELECT TOP 1 PalletCode FROM dbo.PalletStatuses WHERE ProductBarcode = @_Barcode)
			IF NOT EXISTS (SELECT TOP 1 PalletCode FROM dbo.PalletStatuses WHERE PalletCode = @_OldPalletCode AND ProductBarcode != @_Barcode)
				BEGIN
					UPDATE dbo.Pallets SET Status = 'N' WHERE PalletCode = @_OldPalletCode

					SET @_Data = 
						'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallets">' 
						+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_OldPalletCode FOR XML PATH(''))
						+ '</BaseEntity>'

					EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'UPD', @_Method, @_Date, @_Date
				END

			UPDATE dbo.PalletStatuses
			SET
				PalletCode = @_NewPalletCode
				, CartonBarcode = NULL
				, UpdatedBy = @_UserID
				, UpdatedBySitemapID = @_SitemapID
				, UpdatedDateTime = @_Date
			WHERE
				ProductBarcode = @_Barcode

			SET @_Data = 
				'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PalletStatus">' 
				+ (SELECT * FROM dbo.PalletStatuses ps WHERE ps.ProductBarcode = @_Barcode FOR XML PATH(''))
				+ '</BaseEntity>'
		END

	IF @_Type = 'C' OR @_Type = 'E'
		EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'PalletStatuses', @_Data, 'UPD', @_Method, @_Date, @_Date
END