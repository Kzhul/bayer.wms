﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PassCode_Get]
AS
BEGIN
	SET NOCOUNT ON
	SELECT * FROM [dbo].[PassCode] WHERE DATEDIFF(dd, [WorkingDate], GETDATE()) = 0
END