﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SelectManagement]
	@Type NVARCHAR(255)
	, @StrName NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_Type NVARCHAR(255) = @Type
	, @_StrName NVARCHAR(255) = @StrName
	
	SELECT DISTINCT
		Z.ZoneName
		, ZL.LineName
		, L.LocationName	
		,P.PalletCode
		,P.Type 
		,PalletType = dbo.[fnPalletType](P.Type)
		,StartDate = FORMAT(P.StartDate, 'dd/MM/yyyy')
		,ExpiryDate = FORMAT(P.ExpiryDate, 'dd/MM/yyyy')
		,UpdatedDateTime = FORMAT(P.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
		,UserFullName = U.LastName + ' ' + U.FirstName
		,StrStatus = dbo.[fnPalletStatus](P.[Status])
		,CartonQuantity = dbo.[fnSelectQuantityByAllPackagingType](P.PalletCode)
		,StrIsDeleted = CASE WHEN P.IsDeleted = 0 THEN N'Đang sử dụng' ELSE N'Ngưng sử dụng' END
		, StrLocationPutDate = FORMAT(P.LocationPutDate, 'dd/MM/yyyy HH:mm:ss')
		, StrDriver = ISNULL(D.LastName + ' ', '') + ISNULL(D.FirstName, '')
		, StrWarehouseVerifyDate = FORMAT(P.WarehouseVerifyDate, 'dd/MM/yyyy HH:mm:ss')
		, StrWarehouseKeeper = ISNULL(W.LastName + ' ', '') + ISNULL(W.FirstName, '')
		, L.LocationCode
		,PrintDateTime = CASE WHEN P.PrintDate IS NULL THEN '' ELSE FORMAT(P.PrintDate, 'dd/MM/yyyy HH:mm:ss') END
		,UserPrintFullName = CASE WHEN PU.UserID IS NULL THEN '' ELSE PU.LastName + ' ' + PU.FirstName END
		,PrintedTime = ISNULL(P.PrintedTime,0)
	FROM
		Pallets P WITH (NOLOCK)
		LEFT JOIN Users U WITH (NOLOCK)
			ON P.UpdatedBy = U.UserID
		LEFT JOIN Users PU WITH (NOLOCK)
			ON P.UserPrint = PU.UserID
		LEFT JOIN Users W WITH (NOLOCK)
			 ON P.WarehouseKeeper = W.UserID
		LEFT JOIN Users D WITH (NOLOCK)
			 ON P.Driver = D.UserID	
		LEFT JOIN [dbo].[ZoneLocations] L WITH (NOLOCK)
			ON P.LocationCode = L.LocationCode
		LEFT JOIN Zones Z WITH (NOLOCK)
			ON L.ZoneCode = Z.ZoneCode
		LEFT JOIN [dbo].[ZoneLines] ZL WITH (NOLOCK)
			ON L.LineCode = ZL.LineCode
			AND L.ZoneCode = ZL.ZoneCode
	WHERE
		(@_Type = '' OR P.Type = @_Type)
		AND (@_StrName = '' OR P.PalletCode LIKE '%' + @_StrName + '%')
	ORDER BY 
		UpdatedDateTime DESC

END