﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_ReportDeliveryBarcodeHistory] '', NULL, NULL
CREATE PROCEDURE [dbo].[proc_ReportDeliveryBarcodeHistory2]
	@CompanyCode NVARCHAR(255)
	,@FromDate Date
	,@ToDate Date
AS
BEGIN
	SET NOCOUNT ON

DECLARE @_FromDate DATE = @FromDate
DECLARE @_ToDate DATE  = @ToDate
DECLARE @_CompanyCode NVARCHAR(50) = @CompanyCode

--CACHE DATA
SELECT DISTINCT 
	*
INTO #A
FROM 
	DeliveryHistories DH WITH (NOLOCK)
WHERE
	(@_CompanyCode = '' OR DH.CompanyCode = @_CompanyCode)
	AND (@_FromDate IS NULL OR DH.DeliveryDate >= @_FromDate)
	AND (@_ToDate IS NULL OR DH.DeliveryDate <= @_ToDate)
	AND DH.CompanyCode IS NOT NULL
	AND DH.DeliveryDate IS NOT NULL


--SELECT SUM Product
SELECT DISTINCT
	P.DeliveryDate
	, P.CompanyCode
	, P.CompanyName
	, ProductDescription = pd.Description
	, tmp.ProductLot
	, tmp.Quantity
	, CartonOddQuantity = CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) + 'T / ' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
	--, pd.PackingType
	--, pd.ProductCode	
	--, pd.ProductID
FROM
	(
		SELECT DISTINCT 
			CompanyCode
			, DeliveryDate
			, CompanyName
		FROM 
			#A
	) P 
	LEFT JOIN 
	(
		SELECT DISTINCT
			ps.CompanyCode
			, ps.DeliveryDate
			, ps.ProductID
			, ps.ProductLot
			, Quantity = COUNT(DISTINCT ps.ProductBarcode)
			, OddQuantity = SUM(CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN NULL ELSE 1 END)
		FROM
			#A AS ps
		GROUP BY
			ps.CompanyCode
			, ps.DeliveryDate
			, ps.ProductID
			, ps.ProductLot
	) tmp
		ON P.CompanyCode = tmp.CompanyCode
		AND P.DeliveryDate = tmp.DeliveryDate
	LEFT JOIN 
	(
		SELECT DISTINCT
			ps.CompanyCode
			, ps.DeliveryDate
			, ps.ProductID
			, ps.ProductLot
			, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
		FROM
			#A AS ps
		WHERE
			CartonBarcode IS NOT NULL 
			AND CartonBarcode != ''
		GROUP BY
			ps.CompanyCode
			, ps.DeliveryDate
			, ps.ProductID
			, ps.ProductLot
	) tmpC
		ON P.CompanyCode = tmpC.CompanyCode
		AND tmp.DeliveryDate = tmpC.DeliveryDate
		AND tmp.ProductID = tmpC.ProductID
		AND tmp.ProductLot = tmpC.ProductLot
	LEFT JOIN dbo.Products pd WITH (NOLOCK) ON pd.ProductID = tmp.ProductID
ORDER BY 
	P.DeliveryDate
	, P.CompanyCode
	, P.CompanyName
	, pd.Description
	, tmp.ProductLot


--SUM BY CARTON
SELECT DISTINCT 
	DeliveryDate
	, CompanyCode
	, CompanyName
	, ProductDescription
	, ProductLot
	, CartonBarcode	
	, ProductBarcode
	, ProductQty
FROM
(
	SELECT DISTINCT
		tmp.DeliveryDate
		, tmp.CompanyCode
		, tmp.CompanyName
		, ProductDescription = p.Description
		, tmp.ProductLot
		, tmp.CartonBarcode		
		, tmp.ProductBarcode		
		, tmp.ProductQty		
		, p.ProductCode		
		, p.ProductID
	FROM
		(
			SELECT DISTINCT
				ps.DeliveryDate
				, ps.CompanyCode
				, ps.CompanyName
				, ps.ProductID
				, ps.ProductLot
				, ps.CartonBarcode
				, ProductBarcode = ''
				, ProductQty = COUNT(DISTINCT EncryptedProductBarcode)
			FROM
				#A ps WITH (NOLOCK)
			WHERE
				CartonBarcode IS NOT NULL 
				AND CartonBarcode != ''
			GROUP BY
				ps.DeliveryDate
				, ps.CompanyCode
				, ps.CompanyName
				, ps.ProductID
				, ps.ProductLot
				, ps.CartonBarcode
		)tmp
		LEFT JOIN dbo.Products p WITH (NOLOCK) 
			ON p.ProductID = tmp.ProductID
			AND p.IsDeleted = 0
	

	UNION

	SELECT DISTINCT
		tmp.DeliveryDate
		, tmp.CompanyCode
		, tmp.CompanyName
		, ProductDescription = p.Description
		, tmp.ProductLot
		, tmp.CartonBarcode		
		, tmp.ProductBarcode
		, tmp.ProductQty		
		, p.ProductCode		
		, p.ProductID
	FROM
		(
			SELECT DISTINCT
				ps.DeliveryDate
				, ps.CompanyCode
				, ps.CompanyName
				, ps.ProductID
				, ps.ProductLot
				, CartonBarcode = ''
				, ProductBarcode = ps.EncryptedProductBarcode
				, ProductQty = 1
			FROM
				#A ps WITH (NOLOCK)
			WHERE
				CartonBarcode IS NULL OR CartonBarcode = ''
		)tmp
		LEFT JOIN dbo.Products p WITH (NOLOCK) 
			ON p.ProductID = tmp.ProductID
			AND p.IsDeleted = 0
) AS A
ORDER BY 
	DeliveryDate
	, CompanyCode
	, CompanyName
	, ProductDescription
	, ProductLot
	, CartonBarcode	




--SELECT DETAIL QR CODE
SELECT DISTINCT 
	DH.[DeliveryDate]
	,DH.[CompanyCode]
	,DH.[CompanyName]	
	,ProductDescription = P.Description
	,DH.ProductLot
	,[CartonBarcode] = ISNULL( DH.[CartonBarcode], '')
	,DH.EncryptedProductBarcode
	--,P.ProductCode
	--,P.ProductID
FROM 
	#A DH WITH (NOLOCK)
	LEFT JOIN Products P WITH (NOLOCK)
		ON DH.ProductID = P.ProductID
WHERE
	(@_CompanyCode = '' OR DH.CompanyCode = @_CompanyCode)
	AND (@_FromDate IS NULL OR DH.DeliveryDate >= @_FromDate)
	AND (@_ToDate IS NULL OR DH.DeliveryDate <= @_ToDate)
	AND DH.CompanyCode IS NOT NULL
	AND DH.DeliveryDate IS NOT NULL

ORDER BY 
	DH.DeliveryDate
	, DH.CompanyCode
	, DH.CompanyName
	, P.Description
	, DH.ProductLot
END