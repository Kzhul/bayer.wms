﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Select_GroupByBarcode]
	@PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode

	SELECT
		pl.PalletCode
		, pl.Status
		, pl.ReferenceNbr
		, pl.DOImportCode
		, pl.CompanyCode
		, tmp.Barcode
		, tmp.EncryptedBarcode
		, tmp.ProductLot
		, tmp.Quantity
		, OddQuantity = ISNULL(tmp.OddQuantity, 0)
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
		, p.PackingType
	FROM
		(
			SELECT
				ps.PalletCode
				, Barcode = CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN ps.CartonBarcode ELSE ps.ProductBarcode END
				, EncryptedBarcode = CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN ps.CartonBarcode ELSE ps.EncryptedProductBarcode END
				, ps.ProductID
				, ps.ProductLot
				, Quantity = SUM(ISNULL(ps.Qty, 1))
				, OddQuantity = SUM(CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN NULL ELSE ISNULL(ps.Qty, 1) END)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.PalletCode = @_PalletCode
			GROUP BY
				ps.PalletCode
				, CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN ps.CartonBarcode ELSE ps.ProductBarcode END
				, CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN ps.CartonBarcode ELSE ps.EncryptedProductBarcode END
				, ps.ProductID
				, ps.ProductLot
		)tmp
		JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = tmp.PalletCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = tmp.ProductID
												  AND p.IsDeleted = 0
END