﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallet_ProductWaitingForConfirm]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT DISTINCT TOP 50
		Z.LocationCode
		,ROW_NUMBER() OVER(ORDER BY Z.LocationCode) AS ID
	INTO #A
	FROM
		ZoneLocations Z WITH (NOLOCK)
		JOIN ZoneLines ZL WITH (NOLOCK)
			ON Z.ZoneCode = ZL.ZoneCode
			AND Z.LineCode = ZL.LineCode
		LEFT JOIN Pallets P WITH (NOLOCK)
			ON Z.LocationCode = P.LocationCode
	WHERE
		ZL.[Type] = 'L'
		AND (P.LocationCode IS NULL or P.LocationCode = '')
		AND (Z.CurrentPalletCode IS NULL or Z.CurrentPalletCode = '')
		

	SELECT DISTINCT TOP 50
		 P.PalletCode
		 , [ManufacturingDate] = FORMAT(PP.[ManufacturingDate],'dd/MM/yyyy')
		 , [ExpiryDate] = FORMAT(PP.[ExpiryDate],'dd/MM/yyyy')
		 , PP.ProductID
		 , PD.ProductCode
		 , PD.Description AS ProductName
		 , PP.ProductLot
		 , PP.Note
		 , [PackageSize] = CONVERT(INT,PPS.Quantity)
		 , PP.[UOM]
		 , StrCartonQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'C')
		 , CartonQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'C')
		 , COUNT(DISTINCT PS.ProductBarCode) AS Quantity
		 , ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC) AS ID
		 , P.LocationCode
		 , P.LocationSuggestion
		 , ZL.LocationName
		 , LocationSuggestionName = ZLS.LocationName
	INTO #B
	FROM 
		[ProductionPlans] PP WITH (NOLOCK)
		JOIN PalletStatuses PS WITH (NOLOCK)
			ON PP.ProductLot = PS.ProductLot
		JOIN Pallets P WITH (NOLOCK)
			ON PS.PalletCode = P.PalletCode
		JOIN Products PD WITH (NOLOCK)
			ON PP.ProductID = PD.ProductID
		LEFT JOIN [dbo].[ProductPackings] PPS
			ON PD.ProductID = PPS.ProductID
			AND PPS.Type != 'P'
		LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
			ON P.LocationCode = ZL.LocationCode
		LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
			ON P.LocationSuggestion = ZLS.LocationCode
	WHERE 
		PP.[Status] = 'C'
		AND 
		(
			P.Status = 'N' --Trạng thái mới, chưa có vị trí
			OR P.Status = 'T' --Trạng thái mới, chưa có vị trí
		)
		AND P.PalletCode NOT LIKE '%99999'
		--AND (P.LocationCode IS NULL or P.LocationCode = '')
		AND ISNULL(P.WarehouseKeeper,0) = 0
	GROUP BY
		P.PalletCode
		 , PP.[ManufacturingDate]
		 , PP.[ExpiryDate]
		 , PPS.Quantity
		 , PP.[UOM]
		 , PP.ProductID
		 , PD.ProductCode
		 , PD.Description
		 , PP.ProductLot
		 , PP.Note
		 , P.LocationCode
		 , P.LocationSuggestion
		 , ZL.LocationName
		 , ZLS.LocationName

	--Set pallet location suggest
	UPDATE Pallets
	SET LocationSuggestion = A.LocationCode
	FROM
		#B AS B
		JOIN #A AS A
			ON B.ID= A.ID
	WHERE
		PAllets.PalletCode = B.PalletCode
	
	SELECT DISTINCT TOP 50
		A.LocationCode ,
		B.*
		, ROW_NUMBER() OVER(ORDER BY B.ProductName
									,B.ProductLot) AS IDS
	FROM 
		#B AS B
		JOIN #A AS A
			ON B.ID= A.ID
	ORDER BY
		--Điều kiện lưu trữ
		B.ProductName
		,B.ProductLot
		
END