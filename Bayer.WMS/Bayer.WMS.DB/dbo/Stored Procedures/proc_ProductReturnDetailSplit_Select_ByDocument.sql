﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ProductReturnDetailSplit_Select_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

SELECT DISTINCT
	StrStatus = N'Đã trả'
	,LocationName = Z.LocationName
	,PalletCode = S.PalletCode
	,ProductCode = PD.ProductCode
	,ProductName = PD.Description
	,StrExpiredDate = FORMAT(SD.ExpiryDate, 'dd/MM/yyyy')
	,ProductLot = S.BatchCode
	,Quantity = COUNT(DISTINCT S.ProductBarcode)
	,UOM = PD.UOM
	,PackageQuantity = PP.Quantity
	,Exporter = ISNULL(RC.LastName + ' ', '') + ISNULL(RC.FirstName, '')
	,StrExportDate = FORMAT(CONVERT(DATE,S.CreatedDateTime), 'dd/MM/yyyy')-- HH:mm:ss
FROM
	[dbo].[ProductReturnDetailSplits] S WITH (NOLOCK)
	LEFT JOIN Pallets P WITH (NOLOCK)
		ON S.PalletCode = P.PalletCode
	LEFT JOIN ZoneLocations Z WITH (NOLOCK)
		ON P.LocationCode = Z.LocationCode
	LEFT JOIN Products PD WITH (NOLOCK)
		ON S.ProductID = PD.ProductID
	LEFT JOIN ProductPackings PP WITH (NOLOCK)
		ON PP.ProductID = PD.ProductID
		AND PP.[Type] = 'P'
	LEFT JOIN ProductionPlans SD WITH (NOLOCK)
		ON S.ProductID = SD.ProductID
		AND S.BatchCode = SD.ProductLot
	LEFT JOIN [Users] AS RC WITH (NOLOCK)
		ON S.CreatedBy = RC.UserID
WHERE
	S.ProductReturnCode = @_DocumentNbr
GROUP BY
	Z.LocationName
	,S.PalletCode
	,PD.ProductCode
	,PD.Description
	,SD.ExpiryDate
	,S.BatchCode
	,PD.UOM
	,PP.Quantity
	,RC.LastName
	,RC.FirstName
	,CONVERT(DATE,S.CreatedDateTime)
ORDER BY 
	PD.Description
	,S.BatchCode
	,S.PalletCode
END