﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_Select_With_PalletStatuses]
	@PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode

    SELECT
		pl.PalletCode
		, pl.Status
		, pl.ReferenceNbr
		, pl.CompanyCode
		, tmp.ProductLot
		, ProductQty = ISNULL(tmp.ProductQty, 0)
		, CartonQty = ISNULL(tmp.CartonQty, 0)
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
		, [Type] = 'P'
	FROM
		dbo.Pallets pl WITH (NOLOCK)
		LEFT JOIN
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonQty = COUNT(DISTINCT ps.CartonBarcode)
					, ProductQty = SUM(ISNULL(ps.Qty, 1))
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			)tmp ON tmp.PalletCode = pl.PalletCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = tmp.ProductID
												  AND p.IsDeleted = 0
	WHERE
		pl.PalletCode = @_PalletCode
END