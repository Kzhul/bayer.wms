﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectPalletIncludeLocation2]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
	, @ProductBarcode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_PalletCode VARCHAR(255) = ''
	DECLARE @_ProductBarcode VARCHAR(255) = @ProductBarcode
	DECLARE @_ProductLot VARCHAR(255) = ''
	DECLARE @_Type CHAR(1) = @Type

	IF(@_ProductBarcode != '')
	BEGIN
		SET @_ProductLot = (SELECT DISTINCT TOP 1 ProductLot FROM PalletStatuses PS WITH (NOLOCK)
											WHERE
												PS.ProductBarcode = @_ProductBarcode
												OR PS.EncryptedProductBarcode = @_ProductBarcode
												OR PS.CartonBarcode = @_ProductBarcode
											)
	END

	
	
	IF(@Type = 'P')
	BEGIN
		SET @_PalletCode = @_Barcode
	END
	ELSE IF(@Type = 'Y')
	BEGIN
		DECLARE @ProductLot VARCHAR(55) = SUBSTRING(@Barcode, 1, 9);
		DECLARE @ProductLotNBR VARCHAR(55) = SUBSTRING(@Barcode, 11, 3);
		SET @_PalletCode = (SELECT TOP 1 PalletCode FROM StockReceivingDetailSplits PS WITH (NOLOCK)
											WHERE
												PS.ProductLot = @ProductLot
												AND 
												(
													PS.ProductLot_NBR = @ProductLotNBR
													OR PS.ProductLot_NBR = @Barcode
												)
											)
		PRINT(@ProductLot)
		PRINT(@ProductLotNBR)
	END
	ELSE
	BEGIN
		SET @_PalletCode = (SELECT TOP 1 PalletCode FROM PalletStatuses PS WITH (NOLOCK)
											WHERE
												(PS.PalletCode = @_Barcode)
												OR (PS.ProductBarcode = @_Barcode)
												OR (PS.EncryptedProductBarcode = @_Barcode)
												OR (PS.CartonBarcode = @_Barcode)
											)
	END

	PRINT(@_PalletCode)
	PRINT(@_Type)
	PRINT(@_Barcode)
	PRINT(@_ProductBarcode)

	IF(@_PalletCode LIKE '%99999%')
	BEGIN
		SELECT TOP 1
			pl.PalletCode
			, pl.DOImportCode
			, pl.Status
			, pl.ReferenceNbr
			, pl.CompanyCode
			, tmp.ProductLot
			, CartonQuantity = 0--dbo.[fnSelectQuantityByPackagingTypeEmpty](pl.PalletCode,'C')
			, tmp.ProductQty
			, tmp.CartonQty
			, PD.ProductID
			, PD.ProductCode
			, [PackageSize] = CONVERT(INT,PPS.Quantity)
			, PD.[UOM]
			, ProductDescription = PD.Description
			, PL.LocationCode
			, PL.LocationSuggestion
			, ZL.LocationName
			, LocationSuggestionName = ZLS.LocationName
			, WarehouseKeeper = ISNULL(PL.WarehouseKeeper,0)
			, StrStatus = dbo.[fnPalletStatus](pl.[Status])
		FROM		
			dbo.Pallets pl WITH (NOLOCK) 
			LEFT JOIN (
				SELECT DISTINCT TOP 1
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonQty = COUNT(DISTINCT ps.CartonBarcode)
					, ProductQty = SUM(ISNULL(ps.Qty, 1))
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					PS.PalletCode = @_Barcode
					AND (@_ProductLot = '' OR PS.ProductLot = @_ProductLot)
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			)tmp
				ON pl.PalletCode = tmp.PalletCode
			LEFT JOIN Products PD WITH (NOLOCK)
				ON tmp.ProductID = PD.ProductID
			LEFT JOIN [dbo].[ProductPackings] PPS
				ON PD.ProductID = PPS.ProductID
				AND PPS.Type != 'P'
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON pl.LocationCode = ZL.LocationCode
			LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
				ON pl.LocationSuggestion = ZLS.LocationCode
		WHERE
			PL.PalletCode = @_Barcode
	END
	ELSE
	BEGIN
	SELECT
		pl.PalletCode
		, pl.DOImportCode
		, pl.Status
		, pl.ReferenceNbr
		, pl.CompanyCode
		, tmp.ProductLot
		, CartonQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](pl.PalletCode,'C')
		, tmp.ProductQty
		, tmp.CartonQty
		, PD.ProductID
		, PD.ProductCode
		, [PackageSize] = CONVERT(INT,PPS.Quantity)
		, PD.[UOM]
		, ProductDescription = PD.Description
		, PL.LocationCode
		, PL.LocationSuggestion
		, ZL.LocationName
		, LocationSuggestionName = ZLS.LocationName
		, WarehouseKeeper = ISNULL(PL.WarehouseKeeper,0)
		, StrStatus = dbo.[fnPalletStatus](pl.[Status])
	FROM		
		dbo.Pallets pl WITH (NOLOCK) 
		LEFT JOIN (
			SELECT DISTINCT
				ps.PalletCode
				, ps.ProductID
				, ps.ProductLot
				, CartonQty = COUNT(DISTINCT ps.CartonBarcode)
				, ProductQty = SUM(ISNULL(ps.Qty, 1))
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				PS.PalletCode = @_Barcode
				--AND (@_ProductLot = '' OR PS.ProductLot = @_ProductLot)
			GROUP BY
				ps.PalletCode
				, ps.ProductID
				, ps.ProductLot
		)tmp
			ON pl.PalletCode = tmp.PalletCode
		LEFT JOIN Products PD WITH (NOLOCK)
			ON tmp.ProductID = PD.ProductID
		LEFT JOIN [dbo].[ProductPackings] PPS
			ON PD.ProductID = PPS.ProductID
			AND PPS.Type != 'P'
		LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
			ON pl.LocationCode = ZL.LocationCode
		LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
			ON pl.LocationSuggestion = ZLS.LocationCode
	WHERE
		PL.PalletCode = @_Barcode
	END
END