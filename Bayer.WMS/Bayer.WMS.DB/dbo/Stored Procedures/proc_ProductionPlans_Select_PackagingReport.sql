﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ProductionPlans_Select_PackagingReport]
	@FromDate DATE = NULL
	, @ToDate DATE = NULL
	, @ProductLot VARCHAR(255) = NULL
	, @ProductID INT = NULL
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_FromDate DATE = @FromDate
	DECLARE @_ToDate DATE = @ToDate
	DECLARE @_ProductLot VARCHAR(255) = NULLIF(RTRIM(LTRIM(@ProductLot)), '')
	DECLARE @_ProductID INT = NULLIF(@ProductID, 0)

    SELECT
		pp.ProductLot
		, pp.ProductID
		, pp.PlanDate
		, pp.PackagingStartTime
		, pp.PackagingEndTime
		, pp.Status
		, PlanQty = CONVERT(INT, pp.PackageQuantity)
	INTO #tmpProductionPlans
	FROM
		dbo.ProductionPlans pp WITH (NOLOCK)
	WHERE
		pp.PlanDate BETWEEN @_FromDate AND @_ToDate
		AND (@_ProductLot IS NULL OR pp.ProductLot = @_ProductLot)
		AND (@_ProductID IS NULL OR pp.ProductID = @_ProductID)

	SELECT
		pl.ProductLot
		, pl.UserID
		, pl.Device
		, Date = CONVERT(DATE, pl.Date)
		, ActualQty = COUNT(pl.Barcode)
		, PackagingStartTime = MIN(pl.Date)
		, PackagingEndTime = MAX(pl.Date)
	INTO #tmpPackagingLogs
	FROM
		dbo.PackagingLogs pl WITH (NOLOCK)
	WHERE
		EXISTS (SELECT TOP 1 1 FROM #tmpProductionPlans pp WHERE pp.ProductLot = pl.ProductLot)
	GROUP BY
		pl.ProductLot
		, pl.UserID
		, pl.Device
		, CONVERT(DATE, pl.Date)

	SELECT
		[Asc] = ROW_NUMBER() OVER (PARTITION BY pl.ProductLot ORDER BY pl.PackagingStartTime ASC)
		, [Desc] = ROW_NUMBER() OVER (PARTITION BY pl.ProductLot ORDER BY pl.PackagingStartTime DESC)
		, pp.ProductLot
		, pp.PlanDate
		, pp.Status
		, pp.PlanQty
		, pl.Device
		, pl.ActualQty
		, pl.PackagingStartTime
		, pl.PackagingEndTime
		, pl.Date
		, p.ProductCode
		, Quantity = CONVERT(VARCHAR(255), pl.ActualQty) + ' / ' + CONVERT(VARCHAR(255), pp.PlanQty)
		, ProductDescription = p.Description
		, Executor = ISNULL(u.LastName + ' ', '') + u.FirstName
	INTO #tmpResult
	FROM
		#tmpProductionPlans pp
		LEFT JOIN #tmpPackagingLogs pl ON pl.ProductLot = pp.ProductLot
		LEFT JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UserID
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = pp.ProductID

	UPDATE r
	SET
		r.PackagingStartTime = pp.PackagingStartTime
	FROM
		#tmpResult r
		JOIN #tmpProductionPlans pp ON pp.ProductLot = r.ProductLot
	WHERE
		r.[Asc] = 1

	UPDATE r
	SET
		r.PackagingEndTime = CASE WHEN r.Status = 'C' THEN pp.PackagingEndTime ELSE NULL END
	FROM
		#tmpResult r
		JOIN #tmpProductionPlans pp ON pp.ProductLot = r.ProductLot
	WHERE
		r.[Desc] = 1

	SELECT * FROM #tmpResult ORDER BY PlanDate, Date
END