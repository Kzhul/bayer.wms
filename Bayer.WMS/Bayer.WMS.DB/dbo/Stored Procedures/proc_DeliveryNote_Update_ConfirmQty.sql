﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryNote_Update_ConfirmQty]
	@ExportCode VARCHAR(255)
	, @ProductID INT
	, @BatchCode VARCHAR(255)
	, @ConfirmQty DECIMAL(18, 2)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @_ExportCode VARCHAR(255) = @ExportCode
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_BatchCode VARCHAR(255) = @BatchCode
	DECLARE @_ConfirmQty DECIMAL(18, 2) = @ConfirmQty
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

    UPDATE dbo.DeliveryNoteDetails
	SET
		ConfirmQty = @_ConfirmQty
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		ExportCode = @_ExportCode
		AND ProductID = @_ProductID
		AND BatchCode = @_BatchCode
	
	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="DeliveryNoteDetail">' 
		+ (SELECT * FROM dbo.DeliveryNoteDetails dnd WHERE dnd.ExportCode = @_ExportCode AND dnd.ProductID = @_ProductID AND dnd.BatchCode = @_BatchCode FOR XML PATH(''))
		+ '</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'DeliveryNoteDetails', @_Data, 'UPD', @_Method, @_Date
END