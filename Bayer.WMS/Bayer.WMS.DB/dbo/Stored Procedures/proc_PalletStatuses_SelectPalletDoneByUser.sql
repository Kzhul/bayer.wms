﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectPalletDoneByUser]
	@UserID INT
	, @WorkType VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_UserID INT = @UserID
	DECLARE @_WorkType VARCHAR(10) = @WorkType
	DECLARE @DateToGet DATE = CONVERT(DATE, DATEADD(dd,0,GETDATE()))

	SELECT DISTINCT TOP 50
			pl.PalletCode
			, DOImportCode = ISNULL(pl.DOImportCode,'')
			, Status = ISNULL(pl.Status,'')
			, ReferenceNbr = ISNULL(pl.ReferenceNbr,'')
			, CompanyCode = ISNULL(pl.CompanyCode,'')
			, ProductLot = ISNULL(tmp.ProductLot,'')
			, CartonQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](pl.PalletCode,'C')
			, ProductQty = ISNULL(tmp.ProductQty,0)
			, CartonQty = ISNULL(tmp.CartonQty,0)
			, ProductID = ISNULL(PD.ProductID,0)
			, ProductCode = ISNULL(PD.ProductCode,'')
			, [PackageSize] = CONVERT(INT,ISNULL(PPS.Quantity,0.00))
			, [UOM] = ISNULL(PD.[UOM],'')
			, ProductName = ISNULL(PD.Description,'')
			, PL.LocationCode
			, PL.LocationSuggestion
			, ZL.LocationName
			, Z.ZoneName
			, ZLL.LineName
			, LocationSuggestionName = ZLS.LocationName
			, StrTime = CASE WHEN AD.[DateTime] IS NOT NULL THEN FORMAT(AD.[DateTime],'dd/MM HH:mm') ELSE '' END
			, PL.WarehouseVerifyDate 
			, UpdatedDateTime = AD.[DateTime]
			, ROW_NUMBER() OVER(ORDER BY AD.[DateTime] DESC) AS IDS
		FROM
			dbo.Pallets pl WITH (NOLOCK) 
			-----------------------JOIN VS AUDIT TRAILS TO SELECT CORRECT WORK TYPE
			JOIN AuditTrails AD WITH (NOLOCK) 
				ON AD.UserID = @_UserID
				AND AD.Method = 'Pallets_SetLocation'
				AND AD.[Action] = @_WorkType
				AND AD.[Date] = @DateToGet
			LEFT JOIN(
				SELECT TOP 50
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonQty = COUNT(DISTINCT ps.CartonBarcode)
					, ProductQty = SUM(ISNULL(ps.Qty, 1))
					, pl.WarehouseVerifyDate
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
					JOIN Pallets pl WITH (NOLOCK)
						ON ps.PalletCode = pl.PalletCode
				WHERE
					(
						pl.WarehouseKeeper = @_UserID
						OR pl.DriverReceived = @_UserID
						OR pl.Driver = @_UserID
						OR pl.UpdatedBy = @_UserID
					)
					AND pl.PalletCode NOT LIKE '%99999'
					AND 
					(
						pl.UpdatedDateTime >= @DateToGet
						OR pl.LocationPutDate >= @DateToGet
						OR pl.WarehouseVerifyDate >= @DateToGet
					)
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, pl.WarehouseVerifyDate
				ORDER BY pl.WarehouseVerifyDate DESC
			)tmp
			ON pl.PalletCode = tmp.PalletCode
			LEFT JOIN Products PD WITH (NOLOCK)
				ON tmp.ProductID = PD.ProductID
			LEFT JOIN [dbo].[ProductPackings] PPS
				ON PD.ProductID = PPS.ProductID
				AND PPS.Type != 'P'
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON pl.LocationCode = ZL.LocationCode
			LEFT JOIN ZoneLines ZLL  WITH (NOLOCK) 
				ON ZLL.ZoneCode = ZL.ZoneCode
				AND ZLL.LineCode = ZL.LineCode
			LEFT JOIN Zones Z  WITH (NOLOCK) 
				ON Z.ZoneCode = ZL.ZoneCode
			LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
				ON pl.LocationSuggestion = ZLS.LocationCode
		WHERE
			(
				pl.WarehouseKeeper = @_UserID
				OR pl.DriverReceived = @_UserID
				OR pl.Driver = @_UserID
				OR pl.UpdatedBy = @_UserID
			)
			AND pl.PalletCode NOT LIKE '%99999'
			AND 
			(
				pl.UpdatedDateTime >= @DateToGet
				OR pl.LocationPutDate >= @DateToGet
				OR pl.WarehouseVerifyDate >= @DateToGet
			)
		ORDER BY AD.[DateTime] DESC
	END