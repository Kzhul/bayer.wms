﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.proc_RequestMaterials_Delete
	@DocumentNbr VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_DocumentNbr VARCHAR(255) = @DocumentNbr

	DELETE FROM dbo.RequestMaterials WHERE DocumentNbr = @_DocumentNbr
	DELETE FROM dbo.RequestMaterialLines WHERE DocumentNbr = @_DocumentNbr
	UPDATE dbo.Pallets SET Status = 'N' WHERE PalletCode IN (SELECT rmls.PalletCode FROM dbo.RequestMaterialLineSplits rmls WHERE rmls.DocumentNbr = @_DocumentNbr)
	DELETE FROM dbo.RequestMaterialLineSplits WHERE DocumentNbr = @_DocumentNbr
END