﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_PalletStatuses_SelectByPallet_GroupByBarCode] 'PW071700337'

CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectDeliveryByPallet_GroupByBarCode]
	@Barcode VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	--Lấy sản phẩm trên pallet đang soạn hàng trong kho
	IF EXISTS(
		SELECT TOP 1 * FROM Pallets WITH (NOLOCK) WHERE PalletCode = @_Barcode AND CompanyCode = @_CompanyCode
	)
	BEGIN
		SELECT DISTINCT
			# = ROW_NUMBER() OVER(ORDER BY pd.Description, tmp.ProductLot ASC),
			pd.ProductCode				
			, tmp1.Quantity
			, CartonOddQuantity = CASE WHEN pd.PackingType = 'C' THEN CASE WHEN ISNULL(tmpC.CartonQuantity,0) = 0 AND ISNULL(tmp.OddQuantity, 0) = 0
																		THEN ''
																	   WHEN ISNULL(tmp.OddQuantity, 0) = 0
																		THEN CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
																			+ 'T'
																		ELSE 
																			CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
																			+ 'T / ' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
																		END

											WHEN pd.PackingType = 'B' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' B' 
											WHEN pd.PackingType = 'S' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' X' 
											WHEN pd.PackingType = 'A' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' C'
											ELSE '0' END
			, tmp1.ProductLot
			, pd.Description
			, ProductDescription = pd.Description
			, pd.PackingType
			, pd.ProductID
			, DOQuantity = ISNULL(DO.DOQuantity,0)
			, PreparedQty = ISNULL(DO.PreparedQty,0)
			, NeedPrepareQuantity = ISNULL(DO.DOQuantity,0) - ISNULL(DO.PreparedQty,0)
			, DeliveredQty = ISNULL(DO.DeliveredQty,0)
		FROM
			Pallets P WITH (NOLOCK)		
			LEFT JOIN 
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, Quantity = COUNT(DISTINCT ps.ProductBarcode)
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			) tmp1
				ON P.PalletCode = tmp1.PalletCode
			LEFT JOIN 
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, OddQuantity = COUNT(DISTINCT ps.ProductBarcode)
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
					AND (ps.CartonBarcode IS NULL OR CartonBarcode = '')
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			) tmp
				ON P.PalletCode = tmp.PalletCode
				AND tmp.ProductID = tmp1.ProductID
				AND tmp.ProductLot = tmp1.ProductLot
			LEFT JOIN 
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
					AND CartonBarcode IS NOT NULL 
					AND CartonBarcode != ''
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			) tmpC
				ON P.PalletCode = tmpC.PalletCode
				AND tmp1.ProductID = tmpC.ProductID
				AND tmp1.ProductLot = tmpC.ProductLot
			JOIN dbo.Products pd WITH (NOLOCK) 
				ON pd.ProductID = tmp1.ProductID
				AND pd.IsDeleted = 0
			LEFT JOIN [dbo].[DeliveryOrderSum] DO WITH (NOLOCK)
				ON DO.CompanyCode = @_CompanyCode
				AND DO.DOImportCode = @_DOImportCode
				AND tmp1.ProductID = DO.ProductID
				AND tmp1.ProductLot = DO.BatchCode
			WHERE
				P.PalletCode = @_Barcode
		ORDER BY 
			ISNULL(DO.DOQuantity,0) - ISNULL(DO.PreparedQty,0) DESC,
			pd.Description, 
			tmp1.ProductLot
	END
	ELSE
	BEGIN
		SELECT DISTINCT
			# = ROW_NUMBER() OVER(ORDER BY pd.Description, tmp.ProductLot ASC),
			pd.ProductCode				
			, tmp1.Quantity
			, CartonOddQuantity = CASE WHEN pd.PackingType = 'C' THEN CASE WHEN ISNULL(tmpC.CartonQuantity,0) = 0 AND ISNULL(tmp.OddQuantity, 0) = 0
																		THEN ''
																	   WHEN ISNULL(tmp.OddQuantity, 0) = 0
																		THEN CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
																			+ 'T'
																		ELSE 
																			CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
																			+ 'T / ' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
																		END

											WHEN pd.PackingType = 'B' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' B' 
											WHEN pd.PackingType = 'S' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' X' 
											WHEN pd.PackingType = 'A' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' C'
											ELSE '0' END
			, tmp1.ProductLot
			, pd.Description
			, ProductDescription = pd.Description
			, pd.PackingType
			, pd.ProductID
			, DOQuantity = ISNULL(DO.DOQuantity,0)
			, PreparedQty = ISNULL(DO.PreparedQty,0)
			, NeedPrepareQuantity = ISNULL(DO.DOQuantity,0) - ISNULL(DO.PreparedQty,0)
			, DeliveredQty = ISNULL(DO.DeliveredQty,0)
		FROM
			Pallets P WITH (NOLOCK)		
			LEFT JOIN 
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, Quantity = COUNT(DISTINCT ps.ProductBarcode)
				FROM
					dbo.DeliveryHistories ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			) tmp1
				ON P.PalletCode = tmp1.PalletCode
			LEFT JOIN 
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, OddQuantity = COUNT(DISTINCT ps.ProductBarcode)
				FROM
					dbo.DeliveryHistories ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
					AND (ps.CartonBarcode IS NULL OR CartonBarcode = '')
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			) tmp
				ON P.PalletCode = tmp.PalletCode
				AND tmp.ProductID = tmp1.ProductID
				AND tmp.ProductLot = tmp1.ProductLot
			LEFT JOIN 
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
				FROM
					dbo.DeliveryHistories ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
					AND CartonBarcode IS NOT NULL 
					AND CartonBarcode != ''
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			) tmpC
				ON P.PalletCode = tmpC.PalletCode
				AND tmp1.ProductID = tmpC.ProductID
				AND tmp1.ProductLot = tmpC.ProductLot
			JOIN dbo.Products pd WITH (NOLOCK) 
				ON pd.ProductID = tmp1.ProductID
				AND pd.IsDeleted = 0
			LEFT JOIN [dbo].[DeliveryOrderSum] DO WITH (NOLOCK)
				ON DO.CompanyCode = @_CompanyCode
				AND DO.DOImportCode = @_DOImportCode
				AND tmp1.ProductID = DO.ProductID
				AND tmp1.ProductLot = DO.BatchCode
			WHERE
				P.PalletCode = @_Barcode
		ORDER BY 
			ISNULL(DO.DOQuantity,0) - ISNULL(DO.PreparedQty,0) DESC,
			pd.Description, 
			tmp1.ProductLot
	END
END