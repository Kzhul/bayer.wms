﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectPalletIncludeLocation]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_PalletCode VARCHAR(255) = (SELECT TOP 1 PalletCode FROM PalletStatuses PS WITH (NOLOCK)
										WHERE
											PS.PalletCode = @Barcode
											OR PS.ProductBarcode = @Barcode
											OR PS.CartonBarcode = @Barcode
										)
	DECLARE @_Type CHAR(1) = @Type


	--CLEAR LOCATION OF THIS PALLET
	UPDATE Pallets SET LocationCode = NULL, LocationPutDate = NULL
	FROM 
		Pallets P
		JOIN PalletStatuses PS
			ON P.PalletCode = PS.PalletCode
	WHERE
		PS.PalletCode = @_PalletCode

	SELECT
		pl.PalletCode
		, pl.DOImportCode
		, pl.Status
		, pl.ReferenceNbr
		, pl.CompanyCode
		, tmp.ProductLot
		, tmp.ProductQty
		, tmp.CartonQty
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
		, PL.LocationCode
	FROM
		(
			SELECT
				ps.PalletCode
				, ps.ProductID
				, ps.ProductLot
				, CartonQty = COUNT(DISTINCT ps.CartonBarcode)
				, ProductQty = SUM(ISNULL(ps.Qty, 1))
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				PS.PalletCode = @_PalletCode
			GROUP BY
				ps.PalletCode
				, ps.ProductID
				, ps.ProductLot
		)tmp
		JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = tmp.PalletCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = tmp.ProductID
													AND p.IsDeleted = 0
END