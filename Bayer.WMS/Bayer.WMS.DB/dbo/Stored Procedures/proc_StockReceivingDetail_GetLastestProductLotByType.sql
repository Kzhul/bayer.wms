﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceivingDetail_GetLastestProductLotByType]
	@Type NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Type NVARCHAR(50) = @Type
	SELECT TOP 1 [BatchCode] FROM [dbo].[StockReceivingDetails] WITH (NOLOCK)
	WHERE [BatchCode] LIKE @Type + '%'
	ORDER BY [BatchCode] DESC
END