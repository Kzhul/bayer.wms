﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_Update_ExportStatus]
	@PalletCode VARCHAR(255)
	, @DOImportCode VARCHAR(255) = NULL
	, @ReferenceNbr VARCHAR(255) = NULL
	, @CompanyCode VARCHAR(255) = NULL
	, @Status CHAR(1)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_DOImportCode VARCHAR(255) = NULLIF(LTRIM(RTRIM(@DOImportCode)), '')
	DECLARE @_ReferenceNbr VARCHAR(255) = NULLIF(LTRIM(RTRIM(@ReferenceNbr)), '')
	DECLARE @_CompanyCode VARCHAR(255) = NULLIF(LTRIM(RTRIM(@CompanyCode)), '')
	DECLARE @_CompanyCodeUpdate VARCHAR(255) = ISNULL((SELECT TOP 1 CompanyCode FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode),'')
	DECLARE @_DOImportCodeUpdate VARCHAR(255) = ISNULL((SELECT TOP 1 DOImportCode FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode),'')
	DECLARE @_Status CHAR(1) = @Status
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

    UPDATE dbo.Pallets
	SET
		DOImportCode = @_DOImportCode
		, ReferenceNbr = @_ReferenceNbr
		, CompanyCode = @_CompanyCode
		, Status = @_Status
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallet">' 
		+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode FOR XML PATH(''))
		+ '</BaseEntity>'

	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'UPD', @_Method, @_Date, @_Date, @_PalletCode

	IF(@_Status = 'X')
	BEGIN
		EXEC proc_PalletStatusesExport_Insert @_PalletCode, @_UserID
	END
	ELSE IF (@_Status = 'N')
	BEGIN
		EXEC proc_PalletStatusesExport_Delete @_PalletCode, @_UserID
	END

	
	IF(@_CompanyCodeUpdate IS NOT NULL OR @_CompanyCodeUpdate != '')
	BEGIN
		EXEC [proc_DeliveryOrderDetails_UpdateExportedQuantity_Company] @_CompanyCodeUpdate, @_DOImportCodeUpdate
	END
	IF(@_CompanyCode IS NOT NULL OR @_CompanyCode != '')
	BEGIN
		EXEC [proc_DeliveryOrderDetails_UpdateExportedQuantity_Company] @_CompanyCode, @_DOImportCode
	END
	--UPDATE PreparedQty
	EXEC [proc_DeliveryOrderDetails_UpdateExportedQuantity] @_PalletCode

END