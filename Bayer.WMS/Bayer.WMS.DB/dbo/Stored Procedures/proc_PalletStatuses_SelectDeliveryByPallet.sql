﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectDeliveryByPallet]
	@Barcode VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_Type CHAR(1) = @Type

	--Mã pallet
	--Số thùng
	--Số SP
	--Số SP lẻ
	--Danh sách thùng
	--Danh sách SP lẻ

	--Lấy sản phẩm trên pallet đang soạn hàng trong kho
	IF EXISTS(
		SELECT TOP 1 * FROM Pallets WHERE PalletCode = @_Barcode AND CompanyCode = @_CompanyCode
	)
	BEGIN
		SELECT 
			# = ROW_NUMBER() OVER(ORDER BY ProductDescription, CartonBarcode ASC),
			* 
		FROM
		(
			SELECT DISTINCT
				tmp.PalletCode
				, tmp.CartonBarcode
				, tmp.ProductLot
				, tmp.ProductQty
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				(
					SELECT
						ps.PalletCode
						, ps.ProductID
						, ps.ProductLot
						, ps.CartonBarcode
						, ProductQty = COUNT(DISTINCT ProductBarcode)
					FROM
						dbo.PalletStatuses ps WITH (NOLOCK)
					WHERE
						ps.PalletCode = @_Barcode
						AND CartonBarcode IS NOT NULL 
						AND CartonBarcode != ''
					GROUP BY
						ps.PalletCode
						, ps.ProductID
						, ps.ProductLot
						, ps.CartonBarcode
				)tmp
				LEFT JOIN dbo.Products p WITH (NOLOCK) 
					ON p.ProductID = tmp.ProductID
					AND p.IsDeleted = 0
	

		UNION

		SELECT DISTINCT
			tmp.PalletCode
			, tmp.CartonBarcode
			, tmp.ProductLot
			, tmp.ProductQty
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
		FROM
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonBarcode = ps.EncryptedProductBarcode
					, ProductQty = 1
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
					AND (ps.CartonBarcode IS NULL OR ps.CartonBarcode = '')
			)tmp
			LEFT JOIN dbo.Products p WITH (NOLOCK) 
				ON p.ProductID = tmp.ProductID
				AND p.IsDeleted = 0
		) AS A
		ORDER BY ProductDescription, CartonBarcode									  
	END

	ELSE 
	--Lấy sản phẩm trên pallet đã giao cho company
	BEGIN
		SELECT 
			# = ROW_NUMBER() OVER(ORDER BY ProductDescription, CartonBarcode ASC),
			* 
		FROM
		(
			SELECT DISTINCT
				tmp.PalletCode
				, tmp.CartonBarcode
				, tmp.ProductLot
				, tmp.ProductQty
				, p.ProductID
				, p.ProductCode
				, ProductDescription = p.Description
			FROM
				(
					SELECT
						ps.PalletCode
						, ps.ProductID
						, ps.ProductLot
						, ps.CartonBarcode
						, ProductQty = COUNT(DISTINCT ProductBarcode)
					FROM
						dbo.DeliveryHistories ps WITH (NOLOCK)
					WHERE
						ps.PalletCode = @_Barcode
						AND CartonBarcode IS NOT NULL 
						AND CartonBarcode != ''
						AND ps.DeliveryTicketCode = @_DOImportCode
						AND ps.CompanyCode = @_CompanyCode
					GROUP BY
						ps.PalletCode
						, ps.ProductID
						, ps.ProductLot
						, ps.CartonBarcode
				)tmp
				LEFT JOIN dbo.Products p WITH (NOLOCK) 
					ON p.ProductID = tmp.ProductID
					AND p.IsDeleted = 0
		UNION
		SELECT DISTINCT
			tmp.PalletCode
			, tmp.CartonBarcode
			, tmp.ProductLot
			, tmp.ProductQty
			, p.ProductID
			, p.ProductCode
			, ProductDescription = p.Description
		FROM
			(
				SELECT
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonBarcode = ps.EncryptedProductBarcode
					, ProductQty = 1
				FROM
					dbo.DeliveryHistories ps WITH (NOLOCK)
				WHERE
					ps.PalletCode = @_Barcode
					AND (ps.CartonBarcode IS NULL OR ps.CartonBarcode = '')
					AND ps.DeliveryTicketCode = @_DOImportCode
					AND ps.CompanyCode = @_CompanyCode
			)tmp
			LEFT JOIN dbo.Products p WITH (NOLOCK) 
				ON p.ProductID = tmp.ProductID
				AND p.IsDeleted = 0
		) AS A
		ORDER BY ProductDescription, CartonBarcode		
	END

END