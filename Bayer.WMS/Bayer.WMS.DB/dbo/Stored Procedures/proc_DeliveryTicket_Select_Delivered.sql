﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryTicket_Select_Delivered]
	@DeliveryTicketCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_DeliveryTicketCode VARCHAR(255) = @DeliveryTicketCode

	SELECT
		dh.DeliveryTicketCode
		, p.PackingType
		, StrPackingType =	CASE p.PackingType
								WHEN 'C' THEN N'Thùng'
								WHEN 'B' THEN N'Bao'
								WHEN 'S' THEN N'Xô'
								WHEN 'A' THEN N'Can'
							END
		, Quantity = COUNT(DISTINCT CASE WHEN NULLIF(dh.CartonBarcode, '') IS NOT NULL THEN dh.CartonBarcode ELSE dh.ProductBarcode END)
	FROM
		dbo.DeliveryHistories dh WITH (NOLOCK)
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = dh.ProductID
												  AND p.IsDeleted = 0
	WHERE
		dh.DeliveryTicketCode = @_DeliveryTicketCode
	GROUP BY
		dh.DeliveryTicketCode
		, p.PackingType
END