﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PrepareNote_Update_PreparedQty]
	@PrepareCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
	, @ProductID INT
	, @BatchCode VARCHAR(255)
	, @PreparedQty DECIMAL(18, 2)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PrepareCode VARCHAR(255) = @PrepareCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_BatchCode VARCHAR(255) = @BatchCode
	DECLARE @_PreparedQty DECIMAL(18, 2) = @PreparedQty
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

	UPDATE dbo.PrepareNoteDetails
	SET
		PreparedQty = @_PreparedQty
		, Status = CASE WHEN @_PreparedQty = Quantity THEN 'C' ELSE 'I'END
		, UserPrepare = @_UserID
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PrepareCode = @_PrepareCode
		AND ProductID = @_ProductID
		AND BatchCode = @_BatchCode
	
	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PrepareNoteDetail">' 
		+ (SELECT * FROM dbo.PrepareNoteDetails WHERE PrepareCode = @_PrepareCode AND ProductID = @_ProductID AND BatchCode = @_BatchCode FOR XML PATH(''))
		+ '</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'PrepareNoteDetails', @_Data, 'UPD', @_Method, @_Date, @_Date

	IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.PrepareNoteDetails WHERE PrepareCode = @_PrepareCode AND ISNULL(PreparedQty, 0) != Quantity)
		UPDATE dbo.PrepareNoteHeaders
		SET
			Status = 'C'
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE
			PrepareCode = @_PrepareCode
	ELSE
		UPDATE dbo.PrepareNoteHeaders
		SET
			Status = 'I'
			, UpdatedBy = @_UserID
			, UpdatedBySitemapID = @_SitemapID
			, UpdatedDateTime = @_Date
		WHERE
			PrepareCode = @_PrepareCode

	INSERT INTO dbo.DeliveryOrderExecutors
	( 
		DOImportCode
		, UserID
		, Type 
	)
	VALUES
	(
		@_DOImportCode
		, @_UserID
		, 'P'
	)
END