﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SelectByCompany2]
	@CompanyCode VARCHAR(255)
	,@DOImportCode VARCHAR(255)
	,@ProductID INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_ProductID INT = @ProductID

	SELECT DISTINCT
		# = ROW_NUMBER() OVER(ORDER BY P.[Status] DESC, P.PalletCode ASC),
		P.PalletCode
		,UserFullName = U.LastName + ' ' + U.FirstName
		,CartonQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'C')
		,BagQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'B')
		,ShoveQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'S')
		,CanQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'A')
		,StrStatus = dbo.[fnPalletStatus](P.[Status])
		,P.[Status]
	FROM 
		Pallets P WITH (NOLOCK)
		LEFT JOIN Users U WITH (NOLOCK)
			ON P.UpdatedBy = U.UserID
	WHERE 
		P.CompanyCode = @_CompanyCode
		AND P.DOImportCode = @_DOImportCode
	ORDER BY P.[Status] DESC, P.PalletCode ASC
END