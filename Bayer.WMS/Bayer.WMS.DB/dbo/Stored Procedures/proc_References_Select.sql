﻿CREATE PROCEDURE [dbo].[proc_References_Select]
	@GroupName NVARCHAR(50)
AS
BEGIN

DECLARE @_GroupName NVARCHAR(50) = @GroupName
SELECT 
	[GroupCode]
    ,[SortID]
    ,[RefValue]
    ,[RefName]
    ,[Description]
    ,[Status]
FROM [References] R WITH (NOLOCK)
WHERE
	GroupCode = @_GroupName
ORDER BY SortID
END