﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Lot_Select_For_Mixing]
	@LotCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_LotCode VARCHAR(255) = @LotCode
	DECLARE @_LotID INT
	DECLARE @_PONumber VARCHAR(255)
	DECLARE @_LotCount INT

	SELECT TOP 1 @_LotID = LotID, @_PONumber = PONumber FROM dbo.Lots WITH (NOLOCK) WHERE Code = @_LotCode AND IsDeleted = 0
	SELECT @_LotCount = (SELECT COUNT(1) FROM dbo.Lots WITH (NOLOCK) WHERE PONumber = @_PONumber AND IsDeleted = 0)

	SELECT
		wl.ProductID
		, Qty = COUNT(1)
	INTO #tmpBlending
	FROM
		dbo.Blending b (NOLOCK)
		JOIN dbo.BlendingLines bl (NOLOCK) ON bl.BlendingID = b.BlendingID
		JOIN dbo.WeighingLines wl (NOLOCK) ON wl.WeighingLineID = bl.WeighingLineID
	WHERE
		b.LotID = @_LotID
	GROUP BY
		wl.ProductID
	
	SELECT
		wl.ProductID
		, Qty = COUNT(1)
	INTO #tmpWeighing
	FROM
		dbo.WeighingLines wl (NOLOCK)
	WHERE
		wl.LotID = @_LotID
	GROUP BY
		wl.ProductID

	SELECT
		p.ProductID
		, p.ProductCode
		, p.ProductName
		, Lot = ISNULL(lb.MaterialLot, pb.Lot)
		, RequireWeight = pb.BOMQuantity / @_LotCount
		, MinWeight = ROUND(pb.BOMQuantity / @_LotCount * p.ToleranceMin, 3)
		, MaxWeight = ROUND(pb.BOMQuantity / @_LotCount * p.ToleranceMax, 3)
		, MixingQty = 0
		, WeighingQty = 0
	INTO #tmp
	FROM
		dbo.ProductBOM pb (NOLOCK)
		JOIN dbo.Products p (NOLOCK) ON p.ProductID = pb.ProductBOMID
											 AND p.IsDeleted = 0
		LEFT JOIN dbo.LotBOM lb (NOLOCK) ON lb.BOMID = pb.BOMID
												 AND lb.LotID = @_LotID
	WHERE
		pb.PONumber = @_PONumber
	
	UPDATE #tmp
	SET
		MixingQty = MixingQty + #tmpBlending.Qty
	FROM
		#tmp
		JOIN #tmpBlending ON #tmpBlending.ProductID = #tmp.ProductID
	
	UPDATE #tmp
	SET
		WeighingQty = WeighingQty + #tmpWeighing.Qty
	FROM
		#tmp
		JOIN #tmpWeighing ON #tmpWeighing.ProductID = #tmp.ProductID

	SELECT l.LotID, l.Code, l.DeviceID, l.Status FROM dbo.Lots l WITH (NOLOCK) WHERE l.LotID = @_LotID
	SELECT b.BlendingID FROM dbo.Blending b WITH (NOLOCK) WHERE b.LotID = @_LotID
	SELECT * FROM #tmp
END