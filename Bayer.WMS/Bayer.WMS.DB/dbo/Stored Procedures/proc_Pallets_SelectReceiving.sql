﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SelectReceiving]
	@CompanyCode VARCHAR(255)
	,@DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	SELECT DISTINCT *
	FROM
	(
		SELECT DISTINCT
			# = ROW_NUMBER() OVER(ORDER BY P.PalletCode ASC),
			P.PalletCode
			,UserFullName = U.LastName + ' ' + U.FirstName
			,CartonQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'C')
			,BagQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'B')
			,ShoveQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'S')
			,CanQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'A')
			,StrStatus = CASE 
							WHEN P.[Status] = 'N' THEN N'Mới'
							WHEN P.[Status] = 'P' THEN N'Soạn hàng'
							WHEN P.[Status] = 'B' THEN N'Chờ xe nâng'
							WHEN P.[Status] = 'A' THEN N'Đang giữ'
							WHEN P.[Status] = 'X' THEN N'Chờ nhận hàng'
							WHEN P.[Status] = 'R' THEN N'Đã nhận hàng'
							WHEN P.[Status] = 'T' THEN N'Trả lại cho kho'
							ELSE '' END
			,SortID = CASE WHEN P.[Status] = 'R' THEN 1 ELSE 0 END
		FROM 
			Pallets P WITH (NOLOCK)
			LEFT JOIN Users U WITH (NOLOCK)
				ON P.UpdatedBy = U.UserID
		WHERE 
			P.CompanyCode = @_CompanyCode
			AND P.DOImportCode = @_DOImportCode
	) AS P
	ORDER BY  P.SortID, P.PalletCode
END