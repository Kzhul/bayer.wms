﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.proc_Packaging_Check_PackagedQuantity
	@ProductLot VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @_ProductLot VARCHAR(255) = @ProductLot
	DECLARE @_ProductID INT
	DECLARE @_Quantity DECIMAL(18, 2)

	SELECT @_ProductID = ProductID FROM dbo.ProductionPlans (NOLOCK) WHERE ProductLot = @_ProductLot
	SELECT @_Quantity = Quantity FROM dbo.ProductPackings (NOLOCK) WHERE ProductID = @_ProductID AND Type = 'C'

	SELECT
		pl.CartonBarcode
		, PackagedQty = COUNT(1)
	FROM
		dbo.PackagingLogs pl (NOLOCK)
	WHERE
		pl.ProductLot = @_ProductLot
	GROUP BY
		pl.CartonBarcode
	HAVING
		COUNT(1) != @_Quantity
END