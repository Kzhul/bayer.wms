﻿CREATE PROCEDURE [dbo].[proc_Packaging_CountQuantityInPallet]
	@PalletBarcode NVARCHAR(50)
AS
BEGIN
DECLARE @_PalletBarcode NVARCHAR(50) = @PalletBarcode--'DO18052503'
SELECT DISTINCT 
	CONVERT(NVARCHAR(50), COUNT(DISTINCT PS.EncryptedProductBarcode))
FROM 
	PalletStatuses PS WITH (NOLOCK)
WHERE 
	PS.PalletCode = @_PalletBarcode

END