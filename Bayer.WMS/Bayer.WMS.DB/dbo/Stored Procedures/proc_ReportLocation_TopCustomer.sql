﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_ReportLocation_TopCustomer] '2017-11-01','2017-11-30'
CREATE PROCEDURE [dbo].[proc_ReportLocation_TopCustomer]
	@FromDate VARCHAR(255)
	, @ToDate DATE
AS
BEGIN
	SET NOCOUNT ON

	---------HARCODE HERE FOR DEMO ONLY-----------
	SET @FromDate = '2017-11-01'
	SET @ToDate = '2017-11-30'

	DECLARE @_FromDate Date = @FromDate
	DECLARE @_ToDate DATE = @ToDate

	SELECT DISTINCT
		DO.CompanyCode
		,DOQuantity = SUM(DO.Quantity)
	INTO #DO
	FROM 
		DeliveryOrderDetails DO WITH(NOLOCK)
	WHERE 
		DeliveryDate >= @_FromDate
		AND DeliveryDate <= @_ToDate
	GROUP BY
		CompanyCode

	CREATE CLUSTERED INDEX #idx_DO ON #DO(CompanyCode)  

	SELECT DISTINCT
		DO.CompanyCode
		,DeliveryQuantity = COUNT(DISTINCT ProductBarcode)
	INTO #DN
	FROM 
		DeliveryHistories DO WITH(NOLOCK)
	WHERE 
		DeliveryDate >= @_FromDate
		AND DeliveryDate <= @_ToDate
	GROUP BY
		CompanyCode

	CREATE CLUSTERED INDEX #idx_DN ON #DN(CompanyCode)  

	SELECT DISTINCT TOP 10
		DO.CompanyCode
		,C.CompanyName
		,DO.DOQuantity
		,OrderQuantity = FORMAT(DO.DOQuantity,'N0')
		,DeliveryQuantity = FORMAT(DN.DeliveryQuantity,'N0')
		,DeliveryPercent = FORMAT(CASE WHEN (DO.DOQuantity) > 0.00
								THEN (DN.DeliveryQuantity) * 100.00 / (DO.DOQuantity)
								ELSE 0.00 END,'N2')
		,NotYetDelivery = FORMAT((DO.DOQuantity) - (DN.DeliveryQuantity),'N0')
	FROM
		#DO DO
		JOIN #DN DN
			ON DO.CompanyCode = DN.CompanyCode
		JOIN Companies C
			ON DO.CompanyCode = C.CompanyCode
	WHERE
		((DN.DeliveryQuantity) * 100.00 / (DO.DOQuantity)) > 95
	ORDER BY (DO.DOQuantity) DESC
END