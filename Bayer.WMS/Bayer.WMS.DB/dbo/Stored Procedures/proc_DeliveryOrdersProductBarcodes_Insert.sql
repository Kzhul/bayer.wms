﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryOrdersProductBarcodes_Insert]
	@DOImportCode VARCHAR(255)
	, @CompanyCode VARCHAR(255)
	, @CompanyName NVARCHAR(255)
	, @PalletCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_CompanyName NVARCHAR(255) = @CompanyName
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

	INSERT INTO dbo.DeliveryOrderProductBarcodes
	( 
		DOImportCode
		, ProductID
		, BatchCode
		, PalletCode
		, CartonBarcode
		, ProductBarcode
		, CompanyCode
		, CompanyName
		, CreatedBy
		, CreatedBySitemapID
		, CreatedDateTime
		, UpdatedBy
		, UpdatedBySitemapID
		, UpdatedDateTime
	)
	SELECT
		@_DOImportCode
		, ps.ProductID
		, ps.ProductLot
		, ps.PalletCode
		, ps.CartonBarcode
		, ps.ProductBarcode
		, @_CompanyCode
		, @_CompanyName
		, @_UserID
		, @_SitemapID
		, GETDATE()
		, @_UserID
		, @_SitemapID
		, GETDATE()
	FROM
		dbo.PalletStatuses ps WITH (NOLOCK)
	WHERE
		ps.PalletCode = @_PalletCode

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="DeliveryOrderProductBarcode">' 
		+ (SELECT * FROM dbo.DeliveryOrderProductBarcodes dopb WHERE dopb.DOImportCode = @_DOImportCode AND dopb.PalletCode = @_PalletCode FOR XML PATH('DeliveryOrderProductBarcode'))
		+ '</BaseEntity>'

	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'DeliveryOrderProductBarcodes', @_Data, 'Insert', @_Method, @_Date
END