﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_SplitView_SelectCompany]
	@DOImportCode NVARCHAR(50)
	,@BarCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_BarCode NVARCHAR(50) = @BarCode
	DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
	DECLARE @ProductID INT = ISNULL((
								SELECT TOP 1 PS.ProductID 
								FROM PalletStatuses PS WITH (NOLOCK)
									LEFT JOIN Products P WITH (NOLOCK)
										ON PS.ProductID = P.ProductID
								WHERE
									PS.ProductBarcode = @_BarCode
									OR PS.EncryptedProductBarcode = @_BarCode
									OR PS.CartonBarcode = @_BarCode
									OR PS.ProductLot = @_BarCode
									OR P.ProductCode = @_BarCode
							 ),0)

	PRINT (@ProductID)
	SELECT DISTINCT
		P.CompanyCode
		,C.CompanyName
		,P.BatchCode AS ProductLot
		,P.DeliveryDate
		,StrDeliveryDate = FORMAT(P.DeliveryDate, 'dd/MM/yyyy')
		,DOQuantity = CONVERT(INT, P.DOQuantity)
		,CASE WHEN PP.Quantity > 0 THEN CONVERT(NVARCHAR(20), CONVERT(INT,PP.Quantity)) ELSE '' END AS QC
		,SuggestSplit = CASE WHEN SL.PackType = 'C'----hàng thùng
							THEN 
								CASE WHEN SL.PackQuantity > 0 THEN
									CONVERT(NVARCHAR(20), ISNULL(CONVERT(INT, SL.PackQuantity),0)) + ' T '
									+ CASE WHEN SL.ProductQuantity > 0 THEN '/' ELSE '' END
									ELSE '' END
								+ CASE WHEN SL.ProductQuantity > 0 THEN
									CONVERT(NVARCHAR(20),ISNULL(CONVERT(INT, SL.ProductQuantity), 0)) + N' lẻ'
									ELSE '' END
							ELSE----hàng Xô, Bao, Can
								CONVERT(NVARCHAR(20),ISNULL(CONVERT(INT, SL.Quantity), 0))
								+ CASE WHEN SL.PackType = 'S' THEN ' X'
									WHEN SL.PackType = 'B' THEN ' B'
									WHEN SL.PackType = 'A' THEN ' C'
								END
							END
		,PreparedQty = CONVERT(INT, P.PreparedQty)
		,DeliveredQty = CONVERT(INT, P.DeliveredQty)
		,P.ProductID
		,SL.PackType
	INTO #A
	FROM 
		DeliveryOrderSum P WITH (NOLOCK)
		LEFT JOIN Companies C WITH (NOLOCK)
			ON P.CompanyCode = C.CompanyCode
		LEFT JOIN SplitNoteDetails SL WITH (NOLOCK)
			ON P.DOImportCode = SL.DOCode
			AND P.ProductID = SL.ProductID
			AND P.BatchCode = SL.BatchCode
			AND P.CompanyCode = SL.CompanyCode
		LEFT JOIN ProductPackings PP WITH (NOLOCK)
			ON P.ProductID = PP.ProductID
			AND PP.[Type] = 'C'
	WHERE
		P.DOImportCode = @_DOImportCode
		AND (P.ProductID = @ProductID OR @ProductID = 0)

	SELECT DISTINCT 
		* 
	FROM #A AS C
	--WHERE
	--	DeliveredQty <= DOQuantity
	ORDER BY C.CompanyName, C.ProductLot
END