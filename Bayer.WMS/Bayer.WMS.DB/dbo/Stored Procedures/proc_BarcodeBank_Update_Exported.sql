﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_BarcodeBank_Update_Exported]
	@Quantity INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Quantity INT = @Quantity
	DECLARE @_MinYear INT = YEAR(GETDATE()) - 2000-- (SELECT MIN(Year) FROM dbo.BarcodeBank WITH (NOLOCK) WHERE IsExport = 0)
	DECLARE @_MinSequence INT = (SELECT MIN(Sequence) FROM dbo.BarcodeBank WITH (NOLOCK) WHERE IsExport = 0 AND Year = @_MinYear)
	UPDATE dbo.BarcodeBank
	SET
		IsExport = 1
	WHERE
		IsExport = 0
		AND Sequence BETWEEN @_MinSequence AND @_MinSequence + @_Quantity - 1
		AND Year = @_MinYear
END