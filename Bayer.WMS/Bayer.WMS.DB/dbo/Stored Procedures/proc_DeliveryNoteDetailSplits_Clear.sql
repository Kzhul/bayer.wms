﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryNoteDetailSplits_Clear]
	@DOImportCode VARCHAR(255)
	,@ReferenceNbr VARCHAR(255)
	,@UserID INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_ReferenceNbr VARCHAR(255) = @ReferenceNbr
	DECLARE @_UserID VARCHAR(255) = @UserID

	UPDATE
		[DeliveryNoteDetailSplits]
	SET IsDeleted = 1
		,UpdatedBy = @_UserID
		,UpdatedDateTime = GETDATE()
	WHERE
		[DOImportCode] = @_DOImportCode
        AND [ReferenceNbr] = @_ReferenceNbr
END