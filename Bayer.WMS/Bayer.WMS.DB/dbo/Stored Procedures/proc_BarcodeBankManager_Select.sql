﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_BarcodeBankManager_Select]
AS
BEGIN
	SET NOCOUNT ON
	SELECT DISTINCT 
		B.* 
		,StrExportDate = FORMAT(B.CreatedDateTime, 'dd/MM/yyyy HH:mm:ss')
		,P.Description AS ProductDescription
		,UserExport = U.LastName + ' ' + U.FirstName
	FROM 
		[BarcodeBankManager] B WITH (NOLOCK)
		LEFT JOIN Users U WITH (NOLOCK)
			ON B.UserID = U.UserID
		LEFT JOIN Products P WITH (NOLOCK)
			ON B.ProductCode = P.ProductCode
	ORDER BY B.UpdatedDateTime DESC
END