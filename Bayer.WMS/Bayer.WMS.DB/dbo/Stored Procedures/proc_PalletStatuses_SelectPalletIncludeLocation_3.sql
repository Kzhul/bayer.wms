﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectPalletIncludeLocation]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_PalletCode VARCHAR(255) = ''
	DECLARE @_Type CHAR(1) = @Type
	
	IF(@Type = 'P')
	BEGIN
		SET @_PalletCode = @_Barcode
	END
	ELSE IF(@Type = 'Y')
	BEGIN
		DECLARE @ProductLot VARCHAR(55) = SUBSTRING(@Barcode, 1, 9);
		DECLARE @ProductLotNBR VARCHAR(55) = SUBSTRING(@Barcode, 11, 3);
		SET @_PalletCode = (SELECT TOP 1 PalletCode FROM StockReceivingDetailSplits PS WITH (NOLOCK)
											WHERE
												PS.ProductLot = @ProductLot
												AND 
												(
													PS.ProductLot_NBR = @ProductLotNBR
													OR PS.ProductLot_NBR = @Barcode
												)
											)
		PRINT(@ProductLot)
		PRINT(@ProductLotNBR)
	END
	ELSE
	BEGIN
		SET @_PalletCode = (SELECT TOP 1 PalletCode FROM PalletStatuses PS WITH (NOLOCK)
											WHERE
												(@_Type = 'P' AND PS.PalletCode = @_Barcode)
												OR (@_Type = 'E' AND PS.ProductBarcode = @_Barcode)
												OR (@_Type = 'E' AND PS.EncryptedProductBarcode = @_Barcode)
												OR (@_Type = 'C' AND PS.CartonBarcode = @_Barcode)
											)
	END

	IF(@_PalletCode LIKE '%99999%')
	BEGIN
		SELECT TOP 1
			pl.PalletCode
			, pl.DOImportCode
			, pl.Status
			, pl.ReferenceNbr
			, pl.CompanyCode
			, tmp.ProductLot
			, CartonQuantity = 0--dbo.[fnSelectQuantityByPackagingTypeEmpty](pl.PalletCode,'C')
			, tmp.ProductQty
			, tmp.CartonQty
			, PD.ProductID
			, PD.ProductCode
			, [PackageSize] = CONVERT(INT,PPS.Quantity)
			, PD.[UOM]
			, ProductDescription = PD.Description
			, PL.LocationCode
			, PL.LocationSuggestion
			, ZL.LocationName
			, LocationSuggestionName = ZLS.LocationName
			, WarehouseKeeper = ISNULL(PL.WarehouseKeeper,0)
			, StrStatus = dbo.[fnPalletStatus](pl.[Status])
		FROM		
			dbo.Pallets pl WITH (NOLOCK) 
			LEFT JOIN (
				SELECT DISTINCT TOP 1
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
					, CartonQty = 0--COUNT(DISTINCT ps.CartonBarcode)
					, ProductQty = 0--SUM(ISNULL(ps.Qty, 1))
				FROM
					dbo.PalletStatuses ps WITH (NOLOCK)
				WHERE
					PS.PalletCode = @_PalletCode
				GROUP BY
					ps.PalletCode
					, ps.ProductID
					, ps.ProductLot
			)tmp
				ON pl.PalletCode = tmp.PalletCode
			LEFT JOIN Products PD WITH (NOLOCK)
				ON tmp.ProductID = PD.ProductID
			LEFT JOIN [dbo].[ProductPackings] PPS
				ON PD.ProductID = PPS.ProductID
				AND PPS.Type != 'P'
			LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
				ON pl.LocationCode = ZL.LocationCode
			LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
				ON pl.LocationSuggestion = ZLS.LocationCode
		WHERE
			PL.PalletCode = @_PalletCode
	END
	ELSE
	BEGIN
	SELECT
		pl.PalletCode
		, pl.DOImportCode
		, pl.Status
		, pl.ReferenceNbr
		, pl.CompanyCode
		, tmp.ProductLot
		, CartonQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](pl.PalletCode,'C')
		, tmp.ProductQty
		, tmp.CartonQty
		, PD.ProductID
		, PD.ProductCode
		, [PackageSize] = CONVERT(INT,PPS.Quantity)
		, PD.[UOM]
		, ProductDescription = PD.Description
		, PL.LocationCode
		, PL.LocationSuggestion
		, ZL.LocationName
		, LocationSuggestionName = ZLS.LocationName
		, WarehouseKeeper = ISNULL(PL.WarehouseKeeper,0)
		, StrStatus = dbo.[fnPalletStatus](pl.[Status])
	FROM		
		dbo.Pallets pl WITH (NOLOCK) 
		LEFT JOIN (
			SELECT DISTINCT
				ps.PalletCode
				, ps.ProductID
				, ps.ProductLot
				, CartonQty = COUNT(DISTINCT ps.CartonBarcode)
				, ProductQty = SUM(ISNULL(ps.Qty, 1))
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				PS.PalletCode = @_PalletCode
			GROUP BY
				ps.PalletCode
				, ps.ProductID
				, ps.ProductLot
		)tmp
			ON pl.PalletCode = tmp.PalletCode
		LEFT JOIN Products PD WITH (NOLOCK)
			ON tmp.ProductID = PD.ProductID
		LEFT JOIN [dbo].[ProductPackings] PPS
			ON PD.ProductID = PPS.ProductID
			AND PPS.Type != 'P'
		LEFT JOIN ZoneLocations ZL  WITH (NOLOCK) 
			ON pl.LocationCode = ZL.LocationCode
		LEFT JOIN ZoneLocations ZLS  WITH (NOLOCK) 
			ON pl.LocationSuggestion = ZLS.LocationCode
	WHERE
		PL.PalletCode = @_PalletCode
	END
END