﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

--EXEC [proc_UpdateStatusAllDocument] 'XH17063001', '080617D01', 'DeliveryNote', 'C'

CREATE PROCEDURE [dbo].[proc_UpdateStatusAllDocument]
	@Code NVARCHAR(255)
	,@DOImportCode NVARCHAR(250)
	,@Type NVARCHAR(255)
	,@Status NVARCHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @QuantityDO INT = 0
	DECLARE @QuantityDOCreated INT = 0
	DECLARE @QuantityDOConfirmed INT = 0
	DECLARE @ProductDO INT = 0
	DECLARE @ProductCreated INT = 0
	DECLARE @ProductConfirmed INT = 0
	--DECLARE @DOImportCode NVARCHAR(250)

	--IF(@Code = '')
	--BEGIN
	--	EXEC dbo.[proc_UpdateStatusAllDocument] '', @DOImportCode, 'DeliveryNote', 'N'
		
	--	EXEC dbo.[proc_UpdateStatusAllDocument] '', @DOImportCode, 'SplitNote', 'N'
		
	--	EXEC dbo.[proc_UpdateStatusAllDocument] '', @DOImportCode, 'PrepareNote', 'N'
		
	--	EXEC dbo.[proc_UpdateStatusAllDocument] '', @DOImportCode, 'DeliveryTicket', 'N'
	--END
	

	IF(@Type = 'DeliveryNote')
		BEGIN
			if(@DOImportCode = '')
			BEGIN
				SET @DOImportCode = (SELECT TOP 1 DOImportCode FROM [dbo].[DeliveryNoteHeaders] WHERE ExportCode = @Code AND IsDeleted = 0)
			END

			IF(@Status = 'C')
			BEGIN
				--UPDATE DETAIL IS FINISH
					UPDATE [DeliveryNoteDetails] 
						SET [Status] = 'C' 
							,[UpdatedBySitemapID] = -1
							,[UpdatedDateTime] = GETDATE()
					WHERE 
						ExportCode = @Code
				--UPDATE HEADER IS FINISH
					UPDATE [DeliveryNoteHeaders] 
						SET [Status] = 'C' 
							,[UpdatedBySitemapID] = -1
							,[UpdatedDateTime] = GETDATE()
					WHERE 
						ExportCode = @Code
			END
		
			--UPDATE DELIVERY STATUS OF DELIVERYORDER
			SET @QuantityDO = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[DeliveryOrderDetails] DT
										JOIN [dbo].[DeliveryOrderHeaders] DH
											ON DT.DOImportCode = DH.DOImportCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
								),0)

			SET @QuantityDOCreated = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[DeliveryNoteDetails] DT
										JOIN [dbo].[DeliveryNoteHeaders] DH
											ON DT.DOCode = DH.DOImportCode
											AND DT.ExportCode = DH.ExportCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
										AND DH.Status != 'C'
								),0)

			SET @QuantityDOConfirmed = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[DeliveryNoteDetails] DT
										JOIN [dbo].[DeliveryNoteHeaders] DH
											ON DT.DOCode = DH.DOImportCode
											AND DT.ExportCode = DH.ExportCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
										AND DH.Status = 'C'
								),0)
			
			PRINT('QuantityDO')
			PRINT(@QuantityDO)
			PRINT('QuantityDOCreated')
			PRINT(@QuantityDOCreated)
			PRINT('QuantityDOConfirmed')
			PRINT(@QuantityDOConfirmed)
			--FINISH
			IF(@QuantityDOConfirmed = @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsExportFull = 4 --FINISH
					WHERE
						DOImportCode = @DOImportCode
					PRINT('FINISH')
				END
			--WORKING
			ELSE IF(@QuantityDOConfirmed > 0 AND @QuantityDOConfirmed < @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsExportFull = 3 --WORKING
					WHERE
						DOImportCode = @DOImportCode
					PRINT('WORKING')
				END
			--Created Full
			ELSE IF(@QuantityDOConfirmed = 0 AND @QuantityDOCreated > 0 AND @QuantityDOCreated = @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsExportFull = 2 --Created Full
					WHERE
						DOImportCode = @DOImportCode
					PRINT('Created Full')
				END
			--Create Apart
			ELSE IF(@QuantityDOConfirmed = 0 AND @QuantityDOCreated > 0 AND @QuantityDOCreated < @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsExportFull = 1 --Create apart
					WHERE
						DOImportCode = @DOImportCode
					PRINT('Create apart')
				END
			--Empty
			ELSE 
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsExportFull = 0 --Empty
					WHERE
						DOImportCode = @DOImportCode
					PRINT('Empty')
				END
		END
	ELSE IF(@Type = 'SplitNote')
		BEGIN
			if(@DOImportCode = '')
			BEGIN
				SET @DOImportCode = (SELECT TOP 1 DOImportCode FROM [dbo].[SplitNoteHeaders] WHERE SplitCode = @Code AND IsDeleted = 0)
			END

			IF(@Status = 'C')
			BEGIN
				--UPDATE DETAIL IS FINISH
					UPDATE [SplitNoteDetails] 
						SET [Status] = 'C' 
							,[UpdatedBySitemapID] = -1
							,[UpdatedDateTime] = GETDATE()
					WHERE 
						SplitCode = @Code
				--UPDATE HEADER IS FINISH
					UPDATE [SplitNoteHeaders] 
						SET [Status] = 'C' 
							,[UpdatedBySitemapID] = -1
							,[UpdatedDateTime] = GETDATE()
					WHERE 
						SplitCode = @Code
			END
		
			--UPDATE DELIVERY STATUS OF DELIVERYORDER
			SET @QuantityDO = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[DeliveryOrderDetails] DT
										JOIN [dbo].[DeliveryOrderHeaders] DH
											ON DT.DOImportCode = DH.DOImportCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
								),0)

			SET @QuantityDOCreated = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[SplitNoteDetails] DT
										JOIN [dbo].[SplitNoteHeaders] DH
											ON DT.DOCode = DH.DOImportCode
											AND DT.SplitCode = DH.SplitCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
										AND DH.Status != 'C'
								),0)

			SET @QuantityDOConfirmed = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[SplitNoteDetails] DT
										JOIN [dbo].[SplitNoteHeaders] DH
											ON DT.DOCode = DH.DOImportCode
											AND DT.SplitCode = DH.SplitCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
										AND DH.Status = 'C'
								),0)
			--FINISH
			IF(@QuantityDOConfirmed = @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsSplitFull = 4 --FINISH
					WHERE
						DOImportCode = @DOImportCode
				END
			--WORKING
			ELSE IF(@QuantityDOConfirmed > 0 AND @QuantityDOConfirmed < @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsSplitFull = 3 --WORKING
					WHERE
						DOImportCode = @DOImportCode
				END
			--Created Full
			ELSE IF(@QuantityDOConfirmed = 0 AND @QuantityDOCreated > 0 AND @QuantityDOCreated = @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsSplitFull = 2 --Created Full
					WHERE
						DOImportCode = @DOImportCode
				END
			--Create Apart
			ELSE IF(@QuantityDOConfirmed = 0 AND @QuantityDOCreated > 0 AND @QuantityDOCreated < @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsSplitFull = 1 --Create apart
					WHERE
						DOImportCode = @DOImportCode
				END
			--Empty
			ELSE 
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsSplitFull = 0 --Empty
					WHERE
						DOImportCode = @DOImportCode
				END
		END
	ELSE IF(@Type = 'PrepareNote')
		BEGIN
			if(@DOImportCode = '')
			BEGIN
				SET @DOImportCode = (SELECT TOP 1 DOImportCode FROM [dbo].[PrepareNoteHeaders] WHERE PrepareCode = @Code AND IsDeleted = 0)
			END
		
			IF(@Status = 'C')
			BEGIN
				--UPDATE DETAIL IS FINISH
					UPDATE [PrepareNoteDetails] 
						SET [Status] = 'C' 
							,[UpdatedBySitemapID] = -1
							,[UpdatedDateTime] = GETDATE()
					WHERE 
						PrepareCode = @Code
				--UPDATE HEADER IS FINISH
					UPDATE [PrepareNoteHeaders] 
						SET [Status] = 'C' 
							,[UpdatedBySitemapID] = -1
							,[UpdatedDateTime] = GETDATE()
					WHERE 
						PrepareCode = @Code
			END
			
			SET @QuantityDO = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[DeliveryOrderDetails] DT
										JOIN [dbo].[DeliveryOrderHeaders] DH
											ON DT.DOImportCode = DH.DOImportCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
								),0)

			SET @QuantityDOCreated = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[PrepareNoteDetails] DT
										JOIN [dbo].[PrepareNoteHeaders] DH
											ON DT.DOCode = DH.DOImportCode
											AND DT.PrepareCode = DH.PrepareCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
										AND DH.Status != 'C'
								),0)

			SET @QuantityDOConfirmed = ISNULL((
									SELECT SUM(Quantity) 
									FROM 
										[dbo].[PrepareNoteDetails] DT
										JOIN [dbo].[PrepareNoteHeaders] DH
											ON DT.DOCode = DH.DOImportCode
											AND DT.PrepareCode = DH.PrepareCode
											AND DH.IsDeleted = 0
									WHERE 
										DH.DOImportCode = @DOImportCode
										AND DH.Status = 'C'
								),0)


			--UPDATE DELIVERY STATUS OF DELIVERYORDER
			--FINISH
			IF(@QuantityDOConfirmed = @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsPrepareFull = 4 --FINISH
					WHERE
						DOImportCode = @DOImportCode
				END
			--WORKING
			ELSE IF(@QuantityDOConfirmed > 0 AND @QuantityDOConfirmed < @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsPrepareFull = 3 --WORKING
					WHERE
						DOImportCode = @DOImportCode
				END
			--Created Full
			ELSE IF(@QuantityDOConfirmed = 0 AND @QuantityDOCreated > 0 AND @QuantityDOCreated = @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsPrepareFull = 2 --Created Full
					WHERE
						DOImportCode = @DOImportCode
				END
			--Create Apart
			ELSE IF(@QuantityDOConfirmed = 0 AND @QuantityDOCreated > 0 AND @QuantityDOCreated < @QuantityDO)
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsPrepareFull = 1 --Create apart
					WHERE
						DOImportCode = @DOImportCode
				END
			--Empty
			ELSE 
				BEGIN
					UPDATE [DeliveryOrderHeaders]
					SET IsPrepareFull = 0 --Empty
					WHERE
						DOImportCode = @DOImportCode
				END
		END
	ELSE IF(@Type = 'DeliveryTicket')
		BEGIN
			IF(@Status = 'C')
			BEGIN
				--UPDATE DETAIL IS FINISH
					UPDATE [DeliveryTicketDetails] 
						SET [Status] = 'C' 
							,[UpdatedBySitemapID] = -1
							,[UpdatedDateTime] = GETDATE()
					WHERE 
						PrepareCode = @Code
				--UPDATE HEADER IS FINISH
					UPDATE [DeliveryTicketHeaders] 
						SET [Status] = 'C' 
							,[UpdatedBySitemapID] = -1
							,[UpdatedDateTime] = GETDATE()
					WHERE 
						DeliveryTicketCode = @Code
			END

			--FOREACH PREPARE CODE
			DECLARE @IDS TABLE(IDX INT IDENTITY(1,1), ID NVARCHAR(50))
			
			if(@Code != '')
			BEGIN
				INSERT INTO @IDS
				SELECT DISTINCT 
					PrepareCode
				FROM DeliveryTicketDetails
				WHERE
					DeliveryTicketCode = @Code
			END
			ELSE
			BEGIN
				INSERT INTO @IDS
				SELECT DISTINCT 
					PrepareCode
				FROM PrepareNoteHeaders
				WHERE
					DOImportCode = @DOImportCode
			END

			DECLARE @I INT
			DECLARE @CNT INT
			DECLARE @ID NVARCHAR(50)
			SELECT @I = MIN(IDX) - 1, @CNT = MAX(IDX) FROM @IDS
			WHILE @I < @CNT
			BEGIN
				SELECT @I = @I + 1
				SET @ID = (SELECT TOP 1 ID FROM @IDS WHERE IDX = @I)
		
				--CALL STORE UPDATE EACH PREPARE CODE
				EXEC [proc_UpdateStatusAllDocument] @ID, '', 'PrepareNote', ''
			END
		END
END