﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.proc_Packaging_Insert
	@PackagingStartTime DATETIME = NULL
	, @PackagingEndTime DATETIME = NULL
	, @ProdPlanStatus CHAR(1)
	, @Barcode VARCHAR(255)
	, @EncryptedBarcode VARCHAR(255)
	, @ProductLot VARCHAR(255)
	, @ProductID INT
	, @Date DATETIME
	, @CartonBarcode VARCHAR(255)
	, @PalletCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Device VARCHAR(255)
	, @CartonStatus CHAR(1)
	
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PackagingStartTime DATETIME = @PackagingStartTime
	DECLARE @_PackagingEndTime DATETIME = @PackagingEndTime
	DECLARE @_ProdPlanStatus CHAR(1) = @ProdPlanStatus
    DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_EncryptedBarcode VARCHAR(255) = @EncryptedBarcode
	DECLARE @_ProductLot VARCHAR(255) = @ProductLot
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_Date DATETIME = @Date
	DECLARE @_CartonBarcode VARCHAR(255) = @CartonBarcode
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Device VARCHAR(255) = @Device
	DECLARE @_CartonStatus CHAR(1) = @CartonStatus

	INSERT INTO dbo.PackagingLogs
	(
		Barcode
		, EncryptedBarcode
		, ProductLot
		, ProductID
		, Date
		, CartonBarcode
		, PalletBarcode
		, UserID
		, Device
		, CreatedBy
		, CreatedBySitemapID
		, CreatedDateTime
		, UpdatedBy
		, UpdatedBySitemapID
		, UpdatedDateTime
	)
	VALUES
	(
		@_Barcode
		, @_EncryptedBarcode
		, @_ProductLot
		, @_ProductID
		, @_Date
		, @_CartonBarcode
		, @_PalletCode
		, @_UserID
		, @_Device
		, @_UserID
		, @_SitemapID
		, GETDATE()
		, @_UserID
		, @_SitemapID
		, GETDATE()
	)

	INSERT INTO dbo.PalletStatuses
	(
		ProductBarcode
		, EncryptedProductBarcode
		, ProductLot
		, ProductID
		, CartonBarcode
		, PalletCode
		, Qty
		, CreatedBy
		, CreatedBySitemapID
		, CreatedDateTime
		, UpdatedBy
		, UpdatedBySitemapID
		, UpdatedDateTime
	)
	VALUES
	(
		@_Barcode
		, @_EncryptedBarcode
		, @_ProductLot
		, @_ProductID
		, @_CartonBarcode
		, @_PalletCode
		, 1
		, @_UserID
		, @_SitemapID
		, GETDATE()
		, @_UserID
		, @_SitemapID
		, GETDATE()
	)

	DELETE FROM dbo.BarcodeBank WHERE Barcode = @_Barcode

	UPDATE ProductionPlans SET PackagingStartTime = @_PackagingStartTime, PackagingEndTime = @_PackagingEndTime, Status = @_ProdPlanStatus WHERE ProductLot = @_ProductLot
	UPDATE ProductBarcodes SET CartonBarcode = @_CartonBarcode, PalletCode = @_PalletCode, Status = 'P' WHERE Barcode = @_Barcode
	UPDATE CartonBarcodes SET Status = @_CartonStatus WHERE Barcode = @_CartonBarcode
END