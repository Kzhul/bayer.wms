﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC proc_DeliveryOrderDetails_UpdateReceivedQuantity 'PP071700067'
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_UpdateReceivedQuantity]
	@PalletCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_DOImportCode VARCHAR(255) = ISNULL((SELECT TOP 1 DOImportCode FROM Pallets WITH (NOLOCK) WHERE PalletCode = @_PalletCode),'')
	BEGIN
		SELECT DISTINCT
			PS.ProductLot
			,PS.ProductID
			,ExportedQty = ISNULL(COUNT(DISTINCT ps.ProductBarcode),0)	
		INTO #A
		FROM 
			PalletStatusesExport PS WITH (NOLOCK)
			LEFT JOIN Pallets p WITH (NOLOCK)
				ON ps.PalletCode = p.PalletCode
		WHERE
			P.DOImportCode = @_DOImportCode
			AND PS.Received = 1
		GROUP BY 
			PS.ProductLot
			,PS.ProductID

		IF EXISTS (SELECT * FROM #A)
		BEGIN
			UPDATE DeliveryNoteDetails
			SET [ReceivedQty] = ISNULL(A.ExportedQty,0)
			FROM
				DeliveryNoteDetails DO
				LEFT JOIN #A AS A
					ON DO.BatchCode = A.ProductLot
					AND DO.ProductID = A.ProductID
			WHERE
				DO.DOCode = @_DOImportCode
		END
	END
END