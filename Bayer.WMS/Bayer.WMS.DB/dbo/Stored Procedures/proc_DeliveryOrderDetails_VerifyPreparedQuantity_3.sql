﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity]
--EXEC [proc_DeliveryOrderDetails_VerifyPreparedQuantity] '1100189', '170901F08C009'
CREATE PROCEDURE [dbo].[proc_DeliveryOrderDetails_VerifyPreparedQuantity]
	@DOImportCode VARCHAR(255)
	,@CompanyCode VARCHAR(255)
	,@BarCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_BarCode VARCHAR(255) = @BarCode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	SELECT DISTINCT 
		ProductID
		,ProductLot
		,Qty = ISNULL(SUM(ISNULL(Qty,1)),0)
	INTO #PS
	FROM 
		PalletStatuses PS WITH (NOLOCK) 
		JOIN Pallets P WITH (NOLOCK) 
			ON PS.PalletCode = P.PalletCode
	WHERE 
		(
			PS.CartonBarcode = @_BarCode 
			OR PS.ProductBarcode = @_BarCode 
			OR PS.EncryptedProductBarcode = @_BarCode
			OR PS.PalletCode = @_BarCode 
		)
	GROUP BY
		ProductID
		,ProductLot

	SELECT DISTINCT 
		DO.ProductID
		,ProductLot = DO.BatchCode
		,ISNULL(DO.DOQuantity,0) AS DOQuantity
		,ISNULL(DO.PreparedQty,0) AS PreparedQty
		,NeedQty = ISNULL(DOQuantity,0) - ISNULL(PreparedQty,0)
		,NeedQtyExcludePallet = ISNULL(DOQuantity,0) - ISNULL(PreparedQty,0) + ISNULL(PS.Qty,0)
	INTO #DO
	FROM 
		DeliveryOrderSum DO WITH (NOLOCK) 
		LEFT JOIN 
		(
			SELECT DISTINCT 
				ProductID
				,ProductLot
				,Qty = SUM(ISNULL(Qty,1))
			FROM 
				PalletStatuses PS WITH (NOLOCK) 
				JOIN Pallets P WITH (NOLOCK) 
					ON PS.PalletCode = P.PalletCode
			WHERE 
				(
					PS.CartonBarcode = @_BarCode 
					OR PS.ProductBarcode = @_BarCode 
					OR PS.EncryptedProductBarcode = @_BarCode
					OR PS.PalletCode = @_BarCode 
				)
				AND P.CompanyCode = @CompanyCode
				AND P.DOImportCode = @DOImportCode
			GROUP BY
				ProductID
				,ProductLot
		) AS PS
		ON DO.ProductID = PS.ProductID
		AND DO.BatchCode = PS.ProductLot
	WHERE 
		DOImportCode = @DOImportCode
		AND CompanyCode = @CompanyCode

	SELECT DISTINCT
		PS.ProductLot
		, ProductCode = P.ProductCode
		, ProductName = P.Description
		, DOQuantity = ISNULL(DO.DOQuantity,0)
		, PreparedQuantity = ISNULL(DO.NeedQtyExcludePallet,0)
		, P.ProductID
		, NextQuantity = ISNULL(PS.Qty,0)
		, NeedQtyExcludePallet = ISNULL(DO.NeedQtyExcludePallet,0)
	FROM 
		#PS PS
		LEFT JOIN #DO DO
			ON PS.ProductID = DO.ProductID
			AND PS.ProductLot = DO.ProductLot
		LEFT JOIN Products P WITH (NOLOCK)
			ON PS.ProductID = P.ProductID
	WHERE
		ISNULL(PS.Qty,0) > ISNULL(DO.NeedQtyExcludePallet,0)
END