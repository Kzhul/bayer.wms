﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_DeliveryTicketDetail]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN

DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method
DECLARE @_DeliveryDate DATE = (SELECT TOP 1 DeliveryDate FROM DeliveryOrderDetails WHERE DOImportCode = @_DOImportCode)
--------------SOURCE-------------------
DECLARE @ids TABLE(idx int identity(1,1), DOImportCode NVARCHAR(50), CompanyCode NVARCHAR(50), DeliveryTicketCode NVARCHAR(50), PrepareCode NVARCHAR(50))

INSERT INTO @ids
SELECT DISTINCT 
	DD.DOImportCode
	,DD.CompanyCode
	,DH.DeliveryTicketCode
	,PH.PrepareCode
FROM
	[DeliveryOrderSum] DD
	LEFT JOIN DeliveryTicketHeaders DH
		ON DD.CompanyCode = DH.CompanyCode
		AND DD.DeliveryDate = DH.DeliveryDate
	LEFT JOIN PrepareNoteHeaders PH
		ON DD.CompanyCode = PH.CompanyCode
		AND DD.DOImportCode = PH.DOImportCode
WHERE
	DD.DOImportCode = @DOImportCode

DECLARE @i INT
DECLARE @cnt INT
DECLARE @id NVARCHAR(50)
SELECT @i = MIN(idx) - 1, @cnt = MAX(idx) FROM @ids
WHILE @i < @cnt
	BEGIN
		SELECT @i = @i + 1
		SET @id = (SELECT TOP 1 CompanyCode FROM @ids WHERE idx = @i)
	
		IF NOT EXISTS (
				SELECT * 
				FROM 
					DeliveryTicketDetails DD
					JOIN PrepareNoteHeaders PH
						ON DD.PrepareCode = PH.PrepareCode
				WHERE 
					PH.DOImportCode = @_DOImportCode 
					AND PH.CompanyCode = @id
		)
		BEGIN
			INSERT INTO [dbo].[DeliveryTicketDetails]
			   ([DeliveryTicketCode]
			   ,[PrepareCode]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedBySitemapID]
			   ,[CreatedDateTime]
			   ,[UpdatedBy]
			   ,[UpdatedBySitemapID]
			   ,[UpdatedDateTime])
			VALUES (
				(SELECT TOP 1 DeliveryTicketCode FROM @ids WHERE idx = @i)
				,(SELECT TOP 1 PrepareCode FROM @ids WHERE idx = @i)
				,'N'
				,@_UserID--SOURCE.[CreatedBy]
				,@_SitemapID--SOURCE.[CreatedBySitemapID]
				,@_SyncDate--SOURCE.[CreatedDateTime]
				,@_UserID--SOURCE.[UpdatedBy]
				,@_SitemapID--[UpdatedBySitemapID]
				,@_SyncDate--[UpdatedDateTime]
				)
		END
	END
END