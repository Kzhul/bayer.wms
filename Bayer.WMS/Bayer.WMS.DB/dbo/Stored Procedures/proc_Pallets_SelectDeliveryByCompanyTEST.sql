﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_SelectDeliveryByCompanyTEST]
	@CompanyCode VARCHAR(255)
	,@DOImportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode

	SELECT DISTINCT
		P.PalletCode
		,[StrStatus] = N'Chưa giao'
		,UserFullName = U.LastName + ' ' + U.FirstName
		,StrDeliveryDate = FORMAT(P.UpdatedDateTime, 'dd/MM/yyyy')
		,CartonQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'C')
		,BagQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'B')
		,ShoveQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'S')
		,CanQuantity = dbo.[fnSelectQuantityByPackagingTypeEmpty](P.PalletCode,'A')
	FROM 
		Pallets P WITH (NOLOCK)
		LEFT JOIN Users U WITH (NOLOCK)
			ON P.UpdatedBy = U.UserID
	WHERE 
		P.CompanyCode = @_CompanyCode
		AND P.DOImportCode = @_DOImportCode
		AND P.[Status] = 'P'
	

	UNION 

	SELECT DISTINCT
		DH.PalletCode
		,[StrStatus] = N'Đã giao'
		,UserFullName = U.LastName + ' ' + U.FirstName
		,StrDeliveryDate = FORMAT(DH.ActualDeliveryDate, 'dd/MM/yyyy')
		,CartonQuantity =  dbo.[fnSelectQuantityByPackagingTypeEmptyForDeliveredPallet](DH.PalletCode ,@_CompanyCode,@_DOImportCode,'C')
		,BagQuantity = dbo.[fnSelectQuantityByPackagingTypeEmptyForDeliveredPallet](DH.PalletCode ,@_CompanyCode,@_DOImportCode,'B')
		,ShoveQuantity = dbo.[fnSelectQuantityByPackagingTypeEmptyForDeliveredPallet](DH.PalletCode ,@_CompanyCode,@_DOImportCode,'S')
		,CanQuantity = dbo.[fnSelectQuantityByPackagingTypeEmptyForDeliveredPallet](DH.PalletCode ,@_CompanyCode,@_DOImportCode,'A')
	FROM 
		DeliveryHistories DH WITH (NOLOCK)
		LEFT JOIN Users U WITH (NOLOCK)
			ON DH.UpdatedBy = U.UserID
	WHERE 
		DH.CompanyCode = @_CompanyCode
		AND DH.DeliveryTicketCode = @_DOImportCode
END