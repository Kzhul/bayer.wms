﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReturn_SelectPallet_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

   --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT 
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ,RM.PalletCode ASC)
	,PalletCode = RM.PalletCode
	,LocationCode = ISNULL(ZL.LocationName,'')
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,ProductLot = RM.BatchCode
	,RM.Status
	,RM.ReturnQty AS ReturnQty
FROM
	StockReturnDetails RM WITH (NOLOCK)
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RM.ProductID = PD.ProductID
	LEFT JOIN Pallets P WITH (NOLOCK) 
		ON RM.[PalletCode] = P.PalletCode
	LEFT JOIN ZoneLocations ZL WITH (NOLOCK) 
		ON ZL.LocationCode = P.LocationCode
WHERE
	RM.StockReturnCode = @_DocumentNbr
	AND RM.Status = 'N'
ORDER BY
	PD.Description
	,RM.PalletCode ASC
END