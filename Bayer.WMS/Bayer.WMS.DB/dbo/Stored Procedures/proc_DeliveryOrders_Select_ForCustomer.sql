﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryOrders_Select_ForCustomer]
	@CompanyCode VARCHAR(255)
	, @DeliveryDate DATE
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode
	DECLARE @_DeliveryDate DATE = @DeliveryDate

	SELECT
		dod.DOImportCode
		, dod.CompanyCode
		, dod.CompanyName
		, dod.DeliveryDate
		, dod.BatchCode
		, Quantity = SUM(dod.Quantity)
		, DeliveredQty = 0
		, p.ProductID
		, p.ProductCode
		, p.Description
	INTO #tmp
	FROM
		dbo.DeliveryOrderHeaders doh WITH (NOLOCK)
		JOIN dbo.DeliveryOrderDetails dod WITH (NOLOCK) ON dod.DOImportCode = doh.DOImportCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = dod.ProductID
												  AND p.IsDeleted = 0
	WHERE
		dod.CompanyCode = @_CompanyCode
		AND dod.DeliveryDate = @_DeliveryDate
	GROUP BY
		dod.DOImportCode
		, dod.CompanyCode
		, dod.CompanyName
		, dod.DeliveryDate
		, dod.BatchCode
		, p.ProductID
		, p.ProductCode
		, p.Description
	
	IF EXISTS (SELECT 1 FROM #tmp)
		BEGIN
			DECLARE @_DOImportCode VARCHAR(255)
			SET @_DOImportCode = (SELECT TOP 1 DOImportCode FROM #tmp)

			UPDATE #tmp
			SET
				DeliveredQty = tmp.DeliveredQty
			FROM
				#tmp
				JOIN
					(
						SELECT
							dopb.DOImportCode
							, dopb.ProductID
							, dopb.BatchCode
							, dopb.CompanyCode
							, DeliveredQty = COUNT(1)
						FROM
							dbo.DeliveryOrderProductBarcodes dopb WITH (NOLOCK)
						WHERE
							dopb.DOImportCode = @_DOImportCode
							AND dopb.CompanyCode = @_CompanyCode
						GROUP BY
							dopb.DOImportCode
							, dopb.ProductID
							, dopb.BatchCode
							, dopb.CompanyCode
					)tmp ON tmp.DOImportCode = dod.DOImportCode
							AND tmp.ProductID = dod.ProductID
							AND tmp.BatchCode = dod.BatchCode
		END

	SELECT * FROM #tmp
END