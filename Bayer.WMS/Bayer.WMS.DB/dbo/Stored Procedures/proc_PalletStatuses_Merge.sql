﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Merge]
	@PalletCode1 VARCHAR(255)
	, @PalletCode2 VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode1 VARCHAR(255) = @PalletCode1
	DECLARE @_PalletCode2 VARCHAR(255) = @PalletCode2
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'MERGE:' + @_PalletCode1 + '->'+ @_PalletCode2

	UPDATE dbo.PalletStatuses
	SET
		PalletCode = @_PalletCode2
	WHERE
		PalletCode = @_PalletCode1

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Pallet">' 
		+ (SELECT * FROM dbo.Pallets p WHERE p.PalletCode = @_PalletCode1 FOR XML PATH(''))
		+ '</BaseEntity>'
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'Pallets', @_Data, 'MER', @_Method, @_Date, @_Date, @_AuditDescription

	UPDATE dbo.Pallets
	SET
		DOImportCode = NULL
		, ReferenceNbr = NULL
		, CompanyCode = NULL
		, Status = 'N'
	WHERE
		PalletCode = @_PalletCode1
END