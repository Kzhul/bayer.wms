﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Barcode_TrackingInfo]
	@Barcode VARCHAR(255)
	, @Type CHAR(1)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type
	
	CREATE TABLE #tmpResult
	(
		InfoName NVARCHAR(500)
		, InfoValue NVARCHAR(500)
		, Sort INT DEFAULT 1
	)

	IF @_Type = 'P'
		BEGIN
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = dbo.[fnPalletStatus](PS.[Status])
				, Sort = 1
			FROM 
				Pallets ps WITH (NOLOCK)
			WHERE
				ps.PalletCode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Ngày đóng gói'
				, InfoValue = CONVERT(VARCHAR(10), pl.Date, 103) + ' ' + CONVERT(VARCHAR(10), pl.Date, 108)
				, Sort = 2
			FROM 
				dbo.PackagingLogs pl WITH (NOLOCK)
			WHERE
				pl.PalletBarcode = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Vị trí kệ'
				, InfoValue = LocationCode
				, Sort = 2
			FROM 
				dbo.Pallets pl WITH (NOLOCK)
			WHERE
				pl.PalletCode = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Ngày nhập vị trí'
				, InfoValue = FORMAT(pl.LocationPutDate, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 2
			FROM 
				dbo.Pallets pl WITH (NOLOCK)
			WHERE
				pl.PalletCode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người đóng gói'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 3
			FROM 
				dbo.PackagingLogs pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UserID
			WHERE
				pl.PalletBarcode = @_Barcode 
			
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'DOImport'
				, InfoValue = ps.DOImportCode
				, Sort = 4
			FROM 
				Pallets ps WITH (NOLOCK)
			WHERE
				ps.PalletCode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Mã Phiếu'
				, InfoValue = ps.ReferenceNbr
				, Sort = 5
			FROM 
				Pallets ps WITH (NOLOCK)
			WHERE
				ps.PalletCode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Mã Khách Hàng'
				, InfoValue = ps.CompanyCode
				, Sort = 6
			FROM 
				Pallets ps WITH (NOLOCK)
			WHERE
				ps.PalletCode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Tên Khách Hàng'
				, InfoValue = DD.CompanyName
				, Sort = 7
			FROM 
				Pallets ps WITH (NOLOCK)
				LEFT JOIN Companies DD WITH (NOLOCK)
					ON PS.CompanyCode = DD.CompanyCode
			WHERE
				ps.PalletCode = @_Barcode 			
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName =	CASE p.PackingType
								WHEN 'C' THEN N'Thùng'
								WHEN 'B' THEN N'Bao'
								WHEN 'S' THEN N'Xô'
								WHEN 'A' THEN N'Can'
							END
				, InfoValue = COUNT(DISTINCT CASE WHEN NULLIF(ps.CartonBarcode, '') IS NOT NULL THEN ps.CartonBarcode ELSE ps.ProductBarcode END)
				, Sort = 8
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = ps.ProductID
														  AND p.IsDeleted = 0
			WHERE
				ps.PalletCode = @_Barcode 
			GROUP BY
				p.PackingType
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 9
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.PalletCode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(dh.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 10
			FROM 
				dbo.Pallets dh WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = dh.UpdatedBy
			WHERE
				dh.PalletCode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 11
			FROM 
				dbo.Pallets pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.PalletCode = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Vị trí'
				, InfoValue = pl.LocationCode
				, Sort = 12
			FROM 
				dbo.Pallets pl WITH (NOLOCK)
			WHERE
				pl.PalletCode = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Ngày đặt'
				, InfoValue =  CONVERT(VARCHAR(10), pl.LocationPutDate, 103) + ' ' + CONVERT(VARCHAR(10), pl.LocationPutDate, 108)
				, Sort = 12
			FROM 
				dbo.Pallets pl WITH (NOLOCK)
			WHERE
				pl.PalletCode = @_Barcode 
			---- ===============================================
		END
	ELSE IF @_Type = 'C'
		BEGIN
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = dbo.[fnPalletStatus](PS.[Status])
				, Sort = 1
			FROM 
				PalletStatuses pp WITH (NOLOCK)
				LEFT JOIN Pallets ps WITH (NOLOCK)
					ON pp.PalletCode = ps.PalletCode
			WHERE
				pp.CartonBarcode = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = N'Đã Giao Hàng'
				, Sort = 1
			FROM 
				DeliveryHistories pp WITH (NOLOCK)
			WHERE
				pp.CartonBarcode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = N'Đã Xuất Hàng'
				, Sort = 1
			FROM 
				ThirdPartyPalletStatuses pp WITH (NOLOCK)
			WHERE
				pp.CartonBarcode = @_Barcode 
			---- ===============================================
			SELECT TOP 1
				ps.PalletCode
				, ps.ProductID
				, Sort = 2
			INTO #tmpPallet
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.CartonBarcode = @_Barcode 

			---- ===============================================
			INSERT INTO #tmpResult
			SELECT DISTINCT TOP 1000 
				InfoName = N'Mã Thùng'
				, InfoValue = CartonBarcode
				, Sort = 3
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.CartonBarcode = @_Barcode 
			---- ===============================================

			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Ngày đóng gói'
				, InfoValue = CONVERT(VARCHAR(10), pl.Date, 103) + ' ' + CONVERT(VARCHAR(10), pl.Date, 108)
				, Sort = 4
			FROM 
				dbo.PackagingLogs pl WITH (NOLOCK)
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người đóng gói'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 5
			FROM 
				dbo.PackagingLogs pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UserID
			WHERE
				pl.CartonBarcode = @_Barcode 
			---- ===============================================

			---- ===============================================
			INSERT INTO #tmpResult
			SELECT  
				InfoName = N'Pallet'
				, InfoValue = PalletCode
				, Sort = 1
			FROM
				#tmpPallet
			---- ===============================================

			---- ===============================================
			INSERT INTO #tmpResult
			SELECT
				InfoName = N'Quy cách thùng'
				, InfoValue = CONVERT(INT, pp.Quantity)
				, Sort = 6
			FROM
				#tmpPallet tmp
				LEFT JOIN dbo.ProductPackings pp WITH (NOLOCK) ON pp.ProductID = tmp.ProductID
																  AND pp.[Type] = 'C'

			INSERT INTO #tmpResult
			SELECT
				InfoName = N'Trọng lượng'
				, InfoValue = pp.[Weight]
				, Sort = 7
			FROM
				#tmpPallet tmp
				LEFT JOIN dbo.ProductPackings pp WITH (NOLOCK) ON pp.ProductID = tmp.ProductID
																  AND pp.[Type] = 'C'

			INSERT INTO #tmpResult
			SELECT
				InfoName = N'Kích thước'
				, InfoValue = pp.Size
				, Sort = 8
			FROM
				#tmpPallet tmp
				LEFT JOIN dbo.ProductPackings pp WITH (NOLOCK) ON pp.ProductID = tmp.ProductID
																  AND pp.[Type] = 'C'
			---- ===============================================

			---- ===============================================
			INSERT INTO #tmpResult
			SELECT
				InfoName = N'Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 9
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.CartonBarcode = @_Barcode 
			---- ===============================================

			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Mã khách hàng'
				, InfoValue = dh.CompanyCode
				, Sort = 10
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Tên khách hàng'
				, InfoValue = dh.CompanyName
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Phiếu giao hàng'
				, InfoValue = dh.DeliveryTicketCode
				, Sort = 12
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Ngày giao hàng'
				, InfoValue = CONVERT(VARCHAR(10), dh.DeliveryDate, 103)
				, Sort = 13
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 
				
			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Phiếu xuất hàng'
				, InfoValue = dh.DOImportCode
				, Sort = 12
			FROM
				dbo.ThirdPartyPalletStatuses dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 
				
			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Ngày xuất hàng'
				, InfoValue = CONVERT(VARCHAR(10), dh.DeliveryDate, 103)
				, Sort = 13
			FROM
				dbo.ThirdPartyPalletStatuses dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(pl.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 14
			FROM 
				dbo.PalletStatuses pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 15
			FROM 
				dbo.PalletStatuses pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(pl.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 14
			FROM 
				dbo.DeliveryHistories pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 15
			FROM 
				dbo.DeliveryHistories pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 
				
			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(pl.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 14
			FROM 
				dbo.ThirdPartyPalletStatuses pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 15
			FROM 
				dbo.ThirdPartyPalletStatuses pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 
			---- ===============================================
		END
	ELSE IF @_Type = 'K'
		BEGIN
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = dbo.[fnPalletStatus](PS.[Status])
				, Sort = 1
			FROM 
				PalletStatuses pp WITH (NOLOCK)
				LEFT JOIN Pallets ps WITH (NOLOCK)
					ON pp.PalletCode = ps.PalletCode
			WHERE
				pp.CartonBarcode = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = N'Đã Giao Hàng'
				, Sort = 1
			FROM 
				DeliveryHistories pp WITH (NOLOCK)
			WHERE
				pp.CartonBarcode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = N'Đã Xuất Hàng'
				, Sort = 1
			FROM 
				ThirdPartyPalletStatuses pp WITH (NOLOCK)
			WHERE
				pp.CartonBarcode = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Pallet'
				, InfoValue = ps.PalletCode
				, Sort = 1
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT DISTINCT
				InfoName = N'Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 1
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.CartonBarcode = @_Barcode 
			---- ===============================================

			---- ===============================================
			INSERT INTO #tmpResult
			SELECT DISTINCT TOP 1
				InfoName = N'Mã khách hàng'
				, InfoValue = dh.CompanyCode
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Tên khách hàng'
				, InfoValue = dh.CompanyName
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Phiếu giao hàng'
				, InfoValue = dh.DeliveryTicketCode
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Ngày giao hàng'
				, InfoValue = CONVERT(VARCHAR(10), dh.DeliveryDate, 103)
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 
				
			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Phiếu giao hàng'
				, InfoValue = dh.DOImportCode
				, Sort = 1
			FROM
				dbo.ThirdPartyPalletStatuses dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1
				InfoName = N'Ngày giao hàng'
				, InfoValue = CONVERT(VARCHAR(10), dh.DeliveryDate, 103)
				, Sort = 1
			FROM
				dbo.ThirdPartyPalletStatuses dh WITH (NOLOCK)
			WHERE
				dh.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(pl.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 14
			FROM 
				dbo.PalletStatuses pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 15
			FROM 
				dbo.PalletStatuses pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(pl.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 14
			FROM 
				dbo.DeliveryHistories pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 15
			FROM 
				dbo.DeliveryHistories pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 
				
			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(pl.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 14
			FROM 
				dbo.ThirdPartyPalletStatuses pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 15
			FROM 
				dbo.ThirdPartyPalletStatuses pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UpdatedBy
			WHERE
				pl.CartonBarcode = @_Barcode 
			---- ===============================================
		END
	ELSE IF @_Type = 'L'
		BEGIN
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Kho: Số Pallet'
				, InfoValue = COUNT(DISTINCT ps.PalletCode)
				, Sort = 1
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Kho: Số Thùng'
				, InfoValue = COUNT(DISTINCT ps.CartonBarcode)
				, Sort = 2
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Kho: Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 3
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đóng gói: Số Thùng'
				, InfoValue = COUNT(DISTINCT ps.CartonBarcode)
				, Sort = 4
			FROM
				dbo.PackagingLogs ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đóng gói: Số lượng '
				, InfoValue = COUNT(ps.Barcode)
				, Sort = 5
			FROM
				dbo.PackagingLogs ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
			------ ===============================================
			--INSERT INTO #tmpResult
			--SELECT 
			--	InfoName = N'Đã Giao: Số Pallet'
			--	, InfoValue = COUNT(DISTINCT ps.PalletCode)
			--	, Sort = 6
			--FROM
			--	dbo.DeliveryHistories ps WITH (NOLOCK)
			--WHERE
			--	ps.ProductLot = @_Barcode 
			------ ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đã Giao: Số Thùng'
				, InfoValue = COUNT(DISTINCT ps.CartonBarcode)
				, Sort = 7
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đã Giao: Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 8
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đã Xuất: Số Thùng'
				, InfoValue = COUNT(DISTINCT ps.CartonBarcode)
				, Sort = 7
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đã Xuất: Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 8
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductLot = @_Barcode 
			---- ===============================================
		END
	ELSE IF @_Type = 'E'
		BEGIN
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = dbo.[fnPalletStatus](PS.[Status])
				, Sort = 1
			FROM 
				PalletStatuses pp WITH (NOLOCK)
				LEFT JOIN Pallets ps WITH (NOLOCK)
					ON pp.PalletCode = ps.PalletCode
			WHERE
				pp.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR pp.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = N'Đã Giao Hàng'
				, Sort = 1
			FROM 
				DeliveryHistories pp WITH (NOLOCK)
			WHERE
				pp.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR pp.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT TOP 1 
				InfoName = N'Trạng Thái'
				, InfoValue = N'Đã Xuất Hàng'
				, Sort = 1
			FROM 
				ThirdPartyPalletStatuses pp WITH (NOLOCK)
			WHERE
				pp.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR pp.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT DISTINCT
				InfoName = N'Ngày đóng gói'
				, InfoValue = CONVERT(VARCHAR(10), pl.Date, 103) + ' ' + CONVERT(VARCHAR(10), pl.Date, 108)
				, Sort = 1
			FROM 
				dbo.PackagingLogs pl WITH (NOLOCK)
			WHERE
				pl.Barcode LIKE '%' + @_Barcode + '%'
				OR pl.EncryptedBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Người đóng gói'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 1
			FROM 
				dbo.PackagingLogs pl WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = pl.UserID
			WHERE
				pl.Barcode LIKE '%' + @_Barcode + '%'
				OR pl.EncryptedBarcode LIKE '%' + @_Barcode + '%'
			---- ===============================================

			---- ===============================================
			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Pallet'
				, InfoValue = ps.PalletCode
				, Sort = 1
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Thùng'
				, InfoValue = ps.CartonBarcode
				, Sort = 1
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Số lô'
				, InfoValue = ps.ProductLot
				, Sort = 1
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Pallet'
				, InfoValue = ps.PalletCode
				, Sort = 1
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Thùng'
				, InfoValue = ps.CartonBarcode
				, Sort = 1
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Số lô'
				, InfoValue = ps.ProductLot
				, Sort = 1
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Pallet'
				, InfoValue = ps.PalletCode
				, Sort = 1
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Thùng'
				, InfoValue = ps.CartonBarcode
				, Sort = 1
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT  DISTINCT
				InfoName = N'Số lô'
				, InfoValue = ps.ProductLot
				, Sort = 1
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR ps.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT DISTINCT
				InfoName = N'Mã khách hàng'
				, InfoValue = dh.CompanyCode
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT
				InfoName = N'Tên khách hàng'
				, InfoValue = C.CompanyName
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
					LEFT JOIN Companies C WITH (NOLOCK)
					ON dh.CompanyCode = C.CompanyCode
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT DISTINCT
				InfoName = N'Phiếu giao hàng'
				, InfoValue = dh.DeliveryTicketCode
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT DISTINCT
				InfoName = N'Ngày giao hàng'
				, InfoValue = CONVERT(VARCHAR(10), dh.DeliveryDate, 103)
				, Sort = 1
			FROM
				dbo.DeliveryHistories dh WITH (NOLOCK)
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'
				
			INSERT INTO #tmpResult
			SELECT DISTINCT
				InfoName = N'Phiếu xuất hàng'
				, InfoValue = dh.DOImportCode
				, Sort = 1
			FROM
				dbo.ThirdPartyPalletStatuses dh WITH (NOLOCK)
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT DISTINCT
				InfoName = N'Ngày xuất hàng'
				, InfoValue = CONVERT(VARCHAR(10), dh.DeliveryDate, 103)
				, Sort = 1
			FROM
				dbo.ThirdPartyPalletStatuses dh WITH (NOLOCK)
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(dh.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 1
			FROM 
				dbo.PalletStatuses dh WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = dh.UpdatedBy
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%' 

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 1
			FROM 
				dbo.PalletStatuses dh WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = dh.UpdatedBy
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(dh.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 1
			FROM 
				dbo.DeliveryHistories dh WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = dh.UpdatedBy
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 1
			FROM 
				dbo.DeliveryHistories dh WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = dh.UpdatedBy
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%' 
				
			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'TG tác động cuối'
				, InfoValue = FORMAT(dh.UpdatedDateTime, 'dd/MM/yyyy HH:mm:ss')
				, Sort = 1
			FROM 
				dbo.ThirdPartyPalletStatuses dh WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = dh.UpdatedBy
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%'

			INSERT INTO #tmpResult
			SELECT TOP 1  
				InfoName = N'Người tác động cuối'
				, InfoValue = ISNULL(u.LastName + ' ', '') + ISNULL(u.FirstName, '')
				, Sort = 1
			FROM 
				dbo.ThirdPartyPalletStatuses dh WITH (NOLOCK)
				JOIN dbo.Users u WITH (NOLOCK) ON u.UserID = dh.UpdatedBy
			WHERE
				dh.ProductBarcode LIKE '%' + @_Barcode + '%'
				OR dh.EncryptedProductBarcode LIKE '%' + @_Barcode + '%' 
			---- ===============================================
			---- ===============================================
		END
	ELSE IF @_Type = 'M'
		BEGIN
			DECLARE @ProductID INT = (SELECT TOP 1 ProductID FROM Products WITH (NOLOCK) WHERE ProductCode = @_Barcode)
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Kho: Số Pallet'
				, InfoValue = COUNT(DISTINCT ps.PalletCode)
				, Sort = 1
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Kho: Số Thùng'
				, InfoValue = COUNT(DISTINCT ps.CartonBarcode)
				, Sort = 2
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Kho: Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 3
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đóng gói: Số Thùng'
				, InfoValue = COUNT(DISTINCT ps.CartonBarcode)
				, Sort = 4
			FROM
				dbo.PackagingLogs ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đóng gói: Số lượng '
				, InfoValue = COUNT(ps.Barcode)
				, Sort = 5
			FROM
				dbo.PackagingLogs ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
			------ ===============================================
			--INSERT INTO #tmpResult
			--SELECT 
			--	InfoName = N'Đã Giao: Số Pallet'
			--	, InfoValue = COUNT(DISTINCT ps.PalletCode)
			--	, Sort = 6
			--FROM
			--	dbo.DeliveryHistories ps WITH (NOLOCK)
			--WHERE
			--	ps.ProductLot = @_Barcode 
			------ ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đã Giao: Số Thùng'
				, InfoValue = COUNT(DISTINCT ps.CartonBarcode)
				, Sort = 7
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đã Giao: Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 8
			FROM
				dbo.DeliveryHistories ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đã Xuất: Số Thùng'
				, InfoValue = COUNT(DISTINCT ps.CartonBarcode)
				, Sort = 7
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
			---- ===============================================
			INSERT INTO #tmpResult
			SELECT 
				InfoName = N'Đã Xuất: Số lượng'
				, InfoValue = COUNT(ps.ProductBarcode)
				, Sort = 8
			FROM
				dbo.ThirdPartyPalletStatuses ps WITH (NOLOCK)
			WHERE
				ps.ProductID = @ProductID
			---- ===============================================
		END
	SELECT * FROM #tmpResult
END