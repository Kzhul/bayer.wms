﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceiving_Select_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

    --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT DISTINCT
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description,RM.BatchCode ASC)
	,RM.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,RM.Status
	,NeedQty = ISNULL(RM.QuantityPlanning,0) - ISNULL(RM.QuantityReceived,0)
	,RequestQty = ISNULL(RM.QuantityPlanning,0)
	,PickedQty = ISNULL(RM.QuantityReceived,0)
	,RM.UOM
	,ProductLot = RM.BatchCode
--INTO #TBPick
FROM
	StockReceivingDetails RM WITH (NOLOCK) 
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RM.ProductID = PD.ProductID
WHERE
	RM.StockReceivingCode = @_DocumentNbr
ORDER BY PD.Description,RM.BatchCode
END