﻿CREATE PROCEDURE [dbo].[pp_DOImport_Merge_DeliveryTicketHeader]
	-- Add the parameters for the stored procedure here
	@DOImportCode NVARCHAR(50)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN

DECLARE @_SyncDate DATETIME = GETDATE()
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode
DECLARE @_UserID INT = @UserID
DECLARE @_SitemapID INT = @SitemapID
DECLARE @_Method VARCHAR(255) = @Method
DECLARE @_DeliveryDate DATE = (SELECT TOP 1 DeliveryDate FROM DeliveryOrderDetails WHERE DOImportCode = @_DOImportCode)
--------------SOURCE-------------------
DECLARE @ids TABLE(idx int identity(1,1), DOImportCode NVARCHAR(50), CompanyCode NVARCHAR(50))

INSERT INTO @ids
SELECT DISTINCT 
	DOImportCode
	,CompanyCode
FROM
	[DeliveryOrderSum] 
WHERE
	DOImportCode = @DOImportCode

DECLARE @i INT
DECLARE @cnt INT
DECLARE @id NVARCHAR(50)
SELECT @i = MIN(idx) - 1, @cnt = MAX(idx) FROM @ids
WHILE @i < @cnt
	BEGIN
		SELECT @i = @i + 1
		SET @id = (SELECT TOP 1 CompanyCode FROM @ids WHERE idx = @i)
	
		IF NOT EXISTS (
				SELECT * 
				FROM 
					DeliveryTicketDetails DD
					JOIN PrepareNoteHeaders PH
						ON DD.PrepareCode = PH.PrepareCode
				WHERE 
					PH.DOImportCode = @_DOImportCode 
					AND PH.CompanyCode = @id
		)
		BEGIN
			INSERT INTO [dbo].[DeliveryTicketHeaders]
			   ([DeliveryTicketCode]
			   ,[Description]
			   ,[DeliveryDate]
			   ,[DeliveryAddress]
			   ,[CompanyCode]
			   ,[CompanyName]
			   ,[Province]
			   ,[UserCreateFullName]
			   ,[UserWareHouse]
			   ,[UserReview]
			   ,[ListOrderNumbers]
			   ,[ListFreeItems]
			   ,[TotalBoxes]
			   ,[TotalBags]
			   ,[TotalBuckets]
			   ,[TotalCans]
			   ,[GrossWeight]
			   ,[TruckNo]
			   ,[Driver]
			   ,[DriverIdentification]
			   ,[ReceiptBy]
			   ,[DeliveverSign]
			   ,[DriverSign]
			   ,[Status]
			   ,[IsDeleted]
			   ,[CreatedBy]
			   ,[CreatedBySitemapID]
			   ,[CreatedDateTime]
			   ,[UpdatedBy]
			   ,[UpdatedBySitemapID]
			   ,[UpdatedDateTime])
			VALUES (
				dbo.[fnTicketNextNBR]('GH')
				,''--[Description]
				,@_DeliveryDate
				,(SELECT TOP 1 [ProvinceShipTo] FROM Companies WHERE CompanyCode = @id)--[DeliveryAddress]
				,@id
				,(SELECT TOP 1 [CompanyName] FROM Companies WHERE CompanyCode = @id)
				,(SELECT TOP 1 [ProvinceShipTo] FROM Companies WHERE CompanyCode = @id)
				,''--[UserCreateFullName]
				,''--[UserWareHouse]
				,''--[UserReview]
				,''--[ListOrderNumbers]
				,''--[ListFreeItems]
				,0--[TotalBoxes]
				,0--[TotalBags]
				,0--[TotalBuckets]
				,0--[TotalCans]
				,0--[GrossWeight]
				,''--[TruckNo]
				,''--[Driver]
				,''--[DriverIdentification]
				,''--[ReceiptBy]
				,''--[DeliveverSign]
				,''--[DriverSign]
				,'N'
				,0
				,@_UserID--SOURCE.[CreatedBy]
				,@_SitemapID--SOURCE.[CreatedBySitemapID]
				,@_SyncDate--SOURCE.[CreatedDateTime]
				,@_UserID--SOURCE.[UpdatedBy]
				,@_SitemapID--[UpdatedBySitemapID]
				,@_SyncDate--[UpdatedDateTime]
				)
		END
	END
END