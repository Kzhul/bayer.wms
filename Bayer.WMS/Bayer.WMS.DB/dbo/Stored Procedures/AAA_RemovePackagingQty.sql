﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AAA_RemovePackagingQty]
	@Pallet VARCHAR(255)
	, @Carton VARCHAR(255)
	, @Barcode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

    DELETE FROM PalletStatuses WHERE ProductBarcode = @Barcode
	DELETE FROM PackagingLogs WHERE Barcode = @Barcode
	UPDATE ProductBarcodes SET PalletCode = NULL, CartonBarcode = NULL, Status = 'N' WHERE Barcode = @Barcode
END