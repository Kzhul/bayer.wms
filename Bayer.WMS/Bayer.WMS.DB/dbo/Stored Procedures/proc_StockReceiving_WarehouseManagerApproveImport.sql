﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceiving_WarehouseManagerApproveImport]
	@DocumentNbr NVARCHAR(50)
	,@UserID INT
	,@MethodID NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_UserID INT = @UserID

UPDATE StockReceivingHeaders
SET 
	WMApproveImportDate = GETDATE()
	,WarehouseManagerUserID = @_UserID
WHERE
	StockReceivingCode = @_DocumentNbr
END