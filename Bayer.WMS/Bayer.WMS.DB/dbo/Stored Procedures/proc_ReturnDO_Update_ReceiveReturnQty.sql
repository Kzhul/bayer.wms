﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_ReturnDO_Update_ReceiveReturnQty]
	@ReturnDOCode VARCHAR(255)
	, @DOImportCode VARCHAR(255)
	, @ProductID INT
	, @BatchCode VARCHAR(255)
	, @ReceivedReturnQty DECIMAL(18, 2)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @_ReturnDOCode VARCHAR(255) = @ReturnDOCode
	DECLARE @_DOImportCode VARCHAR(255) = @DOImportCode
	DECLARE @_ProductID INT = @ProductID
	DECLARE @_BatchCode VARCHAR(255) = @BatchCode
	DECLARE @_ReceivedReturnQty DECIMAL(18, 2) = @ReceivedReturnQty
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML

    UPDATE dbo.ReturnDODetails
	SET
		ReceivedReturnQty = @_ReceivedReturnQty
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		ReturnDOCode = @_ReturnDOCode
		AND ProductID = @_ProductID
		AND BatchCode = @_BatchCode
	
	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ReturnDODetail">' 
		+ (SELECT * FROM dbo.ReturnDODetails dnd WHERE dnd.ReturnDOCode = @_ReturnDOCode AND dnd.ProductID = @_ProductID AND dnd.BatchCode = @_BatchCode FOR XML PATH(''))
		+ '</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'ReturnDODetails', @_Data, 'UPD', @_Method, @_Date, @_Date
END