﻿CREATE PROCEDURE [dbo].[proc_Barcode_Clear]
AS
BEGIN
	SET NOCOUNT ON;

    TRUNCATE TABLE dbo.[_BarCodeTest]
END