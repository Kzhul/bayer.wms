﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.proc_ProductionPlans_SelectByProductLot
	@ProductLot VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_ProductLot VARCHAR(255) = @ProductLot

	SELECT
		pp.ProductLot
		, pp.ManufacturingDate
		, pp.ExpiryDate
		, pp.UOM
		, pp.PackageSize
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
	FROM
		dbo.ProductionPlans pp WITH (NOLOCK)
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = pp.ProductID
												  AND p.IsDeleted = 0
	WHERE
		pp.ProductLot = @_ProductLot
END