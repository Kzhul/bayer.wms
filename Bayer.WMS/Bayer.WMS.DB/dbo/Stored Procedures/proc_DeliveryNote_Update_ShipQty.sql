﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryNote_Update_ShipQty]
	@ExportCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_ExportCode VARCHAR(255) = @ExportCode
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_ExportTime INT

	SELECT
		@_ExportTime =	CASE
							WHEN MAX(dnd.Receiver3) IS NOT NULL THEN 4
							WHEN MAX(dnd.Receiver2) IS NOT NULL THEN 3
							WHEN MAX(dnd.Receiver1) IS NOT NULL THEN 2
						END
	FROM
		dbo.DeliveryNoteDetails dnd WITH (NOLOCK)
	WHERE
		dnd.ExportCode = @_ExportCode

	SET @_ExportTime = ISNULL(@_ExportTime, 1)

	UPDATE dbo.DeliveryNoteDetails
	SET
		Shipper1 =	CASE WHEN @_ExportTime = 1 THEN ReceivedQty ELSE Shipper1 END
		, Shipper2 = CASE WHEN @_ExportTime = 2 THEN ReceivedQty ELSE Shipper2 END
		, Shipper3 = CASE WHEN @_ExportTime = 3 THEN ReceivedQty ELSE Shipper3 END
		, Receiver1 = CASE WHEN @_ExportTime = 1 THEN @_UserID ELSE Receiver1 END
		, Receiver2 = CASE WHEN @_ExportTime = 2 THEN @_UserID ELSE Receiver2 END
		, Receiver3 = CASE WHEN @_ExportTime = 3 THEN @_UserID ELSE Receiver3 END
	WHERE
		ExportCode = @_ExportCode

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="DeliveryNoteDetail">' 
		+ (SELECT * FROM dbo.DeliveryNoteDetails dnd WHERE dnd.ExportCode = @_ExportCode FOR XML PATH('DeliveryNoteDetail'))
		+ '</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'DeliveryNoteDetails', @_Data, 'UPD', @_Method, @_Date, @_Date
END