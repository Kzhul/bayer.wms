﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Export_SelectDO]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @DateToGet DATE = CONVERT(DATE, DATEADD(dd,-30,GETDATE()))

	--PRINT (@DateToGet)
	SELECT DISTINCT 
		DH.DOImportCode AS DOImportCode
		,ExportDate = FORMAT(ExportDate, 'dd/MM/yyyy')
		,DOQuantity = SUM(Quantity)
		,ExportedQty = SUM(ExportedQty)
		,ReceivedQty = SUM(ReceivedQty)
		,ExportDate as OrgExportDate
	FROM 
		DeliveryNoteDetails DND WITH (NOLOCK)
		JOIN [dbo].[DeliveryNoteHeaders] DH WITH (NOLOCK)
			ON DND.DOCode = DH.DOImportCode
	WHERE 
		DH.ExportDate >= @DateToGet
	GROUP BY
		DH.DOImportCode
		,DH.ExportDate
	ORDER BY DH.ExportDate DESC
END