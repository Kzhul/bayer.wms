﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_WHPackaging3]
	@PalletCode VARCHAR(255)
	, @CartonBarcodeOld VARCHAR(255)
	, @CartonBarcodeNew VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_CartonBarcodeOld VARCHAR(255) = @CartonBarcodeOld
	DECLARE @_CartonBarcodeNew VARCHAR(255) = @CartonBarcodeNew
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'WPK:' + @CartonBarcodeNew

	UPDATE dbo.PalletStatuses
	SET
		PalletCode = @_PalletCode
		, CartonBarcode = @CartonBarcodeNew
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		CartonBarcode = @CartonBarcodeOld
	
	--SET @_Data = 
	--	'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PalletStatus">' 
	--	+ (SELECT Top 1 * FROM dbo.PalletStatuses ps WHERE CartonBarcode = @CartonBarcodeOld FOR XML PATH('PalletStatus'))
	--	+ '</BaseEntity>'

	--EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'PalletStatuses', @_Data, 'UPD', @_Method, @_Date, @_Date, @_AuditDescription
	EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity] @_PalletCode
END