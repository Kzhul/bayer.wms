﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [dbo].[proc_StockReceivingDetailSplit_Select_ByDocument] 'SR18062801'
CREATE PROCEDURE [dbo].[proc_StockReceivingDetailSplit_Select_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

SELECT DISTINCT
	StrStatus = CASE WHEN S.Status = 'N' THEN N'Chưa nhận'
					 WHEN S.Status = 'B' THEN N'Chờ lên kệ'
					 WHEN S.Status = 'C' THEN N'Đã nhập kho'
					 ELSE '' END
	,LocationName = Z.LocationName
	,PalletCode = S.PalletCode
	,ProductCode = PD.ProductCode
	,ProductName = PD.Description
	,StrExpiredDate = FORMAT(SD.ExpiryDate, 'dd/MM/yyyy')
	,ProductLot = S.ProductLot
	,Quantity = S.Quantity
	,UOM = PD.UOM
	,PackageQuantity = PP.Quantity
	,Receiver = ISNULL(RC.LastName + ' ', '') + ISNULL(RC.FirstName, '')
	,StrReceiveDateTime = FORMAT(S.[ReceiveDateTime], 'dd/MM/yyyy HH:mm:ss')
	,Forklift = ISNULL(FL.LastName + ' ', '') + ISNULL(FL.FirstName, '')
	,StrSetLocationDateTime = FORMAT(S.[SetLocationDateTime], 'dd/MM/yyyy HH:mm:ss')
	,ProductLot_NBR
FROM
	[dbo].[StockReceivingDetailSplits] S WITH (NOLOCK)
	LEFT JOIN Pallets P WITH (NOLOCK)
		ON S.PalletCode = P.PalletCode
	LEFT JOIN ZoneLocations Z WITH (NOLOCK)
		ON P.LocationCode = Z.LocationCode
	LEFT JOIN Products PD WITH (NOLOCK)
		ON S.ProductID = PD.ProductID
	LEFT JOIN ProductPackings PP WITH (NOLOCK)
		ON PP.ProductID = PD.ProductID
		AND PP.[Type] = 'P'
	LEFT JOIN StockReceivingDetails SD WITH (NOLOCK)
		ON S.[StockReceivingCode] = SD.[StockReceivingCode]
		AND S.ProductLot = SD.BatchCode
	LEFT JOIN [Users] AS RC WITH (NOLOCK)
		ON S.[ReceiverID] = RC.UserID
	LEFT JOIN [Users] AS FL WITH (NOLOCK)
		ON S.[ForkliftID] = FL.UserID
WHERE
	S.StockReceivingCode = @_DocumentNbr
ORDER BY 
	PD.Description
	,S.ProductLot
	,S.PalletCode
END