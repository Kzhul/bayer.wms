﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_Select_ByDocument]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr

    --Lấy nguyên liệu theo mã phiếu yêu cầu
SELECT distinct
	ID = ROW_NUMBER() OVER ( ORDER BY PD.Description ASC)
	,PD.ProductID
	,PD.ProductCode
	,PD.Description AS ProductName
	,RM.Status
	,NeedQty = ISNULL(RM.RequestQty,0) - ISNULL(RM.[PickedQty],0)
	,RequestQty = ISNULL(RM.[RequestQty],0)
	,PickedQty = ISNULL(RM.[PickedQty],0)
	--,SUM(ISNULL(PS.Qty, 1)) AS PalletQuantity
	,RM.UOM
--INTO #TBPick
FROM
	RequestMaterialLines RM WITH (NOLOCK) 
	--LEFT JOIN PalletStatuses PS WITH (NOLOCK) 
	--	ON RM.ProductID = PS.ProductID
	--LEFT JOIN Pallets P WITH (NOLOCK) 
	--	ON PS.PalletCode = P.PalletCode
	LEFT JOIN Products PD WITH (NOLOCK) 
		ON RM.ProductID = PD.ProductID
WHERE
	--P.Status = 'N'--Trạng thái pallet là mới
	--AND 
	RM.DocumentNbr = @_DocumentNbr
--GROUP BY
--	PD.ProductID
--	,P.Status
--	,P.CompanyCode
--	,RM.RequestQty
--	,RM.[PickedQty]
--	,LineNbr
--	,PD.ProductCode
--	,PD.Description
--	,RM.UOM
ORDER BY PD.Description
END