﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--EXEC [proc_ReportDO] '', NULL, NULL
CREATE PROCEDURE [dbo].[proc_ReportDO]
	@DOImportCode VARCHAR(255)
	,@FromDate Date
	,@ToDate Date
AS
BEGIN
	SET NOCOUNT ON

SELECT DISTINCT
	DO.DOImportCode
	,DO.DeliveryDate
	,dbo.fnStatusOfTicket(DO.[Status]) as DOStatus
	,dbo.fnStatusOfTicket(EX.[Status]) as EXStatus
	,dbo.fnStatusOfTicket(SP.[Status]) as SPStatus
	,dbo.fnStatusOfTicket(PR.[Status]) as PRStatus
	,dbo.fnStatusOfTicket(DH.[Status]) as DHStatus
	,EX.ExportCode
	,SP.SplitCode
	,PR.PrepareCode
	,PR.CompanyCode
	,PR.CompanyName
	,DT.[DeliveryTicketCode]
FROM 
	DeliveryOrderHeaders DO
	LEFT JOIN DeliveryNoteHeaders EX
		ON ex.DOImportCode = DO.DOImportCode
	LEFT JOIN SplitNoteHeaders SP
		ON SP.DOImportCode = DO.DOImportCode
	LEFT JOIN SplitNoteDetails SND
		ON SND.DOCode = DO.DOImportCode
	LEFT JOIN PrepareNoteHeaders PR
		ON PR.DOImportCode = DO.DOImportCode
		AND SND.CompanyCode = PR.CompanyCode
	LEFT JOIN [dbo].[DeliveryTicketDetails] DT
		ON DT.PrepareCode = PR.PrepareCode
	LEFT JOIN [dbo].[DeliveryTicketHeaders] DH
		ON DT.[DeliveryTicketCode] = DH.[DeliveryTicketCode]	
WHERE
	(@DOImportCode = '' 
		OR
		( 
			DO.DOImportCode = @DOImportCode
			OR EX.ExportCode = @DOImportCode
			OR SP.SplitCode = @DOImportCode
			OR PR.PrepareCode = @DOImportCode
			OR DT.[DeliveryTicketCode] = @DOImportCode
		)
	)
	AND (@FromDate IS NULL OR DO.DeliveryDate >= @FromDate)
	AND (@ToDate IS NULL OR DO.DeliveryDate <= @ToDate)
ORDER BY PR.CompanyName



END



/****** Object:  StoredProcedure [dbo].[proc_UpdateStatusAllDocumentDOImport]    Script Date: 9/20/2017 9:05:58 AM ******/
SET ANSI_NULLS ON