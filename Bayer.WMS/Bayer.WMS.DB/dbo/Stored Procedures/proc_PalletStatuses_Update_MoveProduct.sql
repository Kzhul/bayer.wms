﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_Update_MoveProduct]
	@PalletCode VARCHAR(255)
	, @Barcode VARCHAR(255)
	, @Type CHAR(1)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
    DECLARE @_Barcode VARCHAR(255) = @Barcode
	DECLARE @_Type CHAR(1) = @Type
	DECLARE @_OldPalletCode VARCHAR(255)
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()
	DECLARE @_Data XML
	DECLARE @_AuditDescription VARCHAR(255) = 'MOV:' + @_Barcode + '->' + @_PalletCode

	---- Get current pallet
	SET @_OldPalletCode = (SELECT TOP 1 PalletCode FROM dbo.PalletStatuses WITH (NOLOCK) WHERE (@_Type = 'C' AND CartonBarcode = @_Barcode) OR (@_Type = 'E' AND (ProductBarcode = @_Barcode OR EncryptedProductBarcode = @_Barcode)))

	UPDATE dbo.PalletStatuses
	SET
		PalletCode = @_PalletCode
		, CartonBarcode = CASE WHEN @_Type = 'C' THEN CartonBarcode ELSE '' END
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		(
			(@_Type = 'C' AND CartonBarcode = @_Barcode) 
			OR (@_Type = 'E' AND (ProductBarcode = @_Barcode OR EncryptedProductBarcode = @_Barcode))
			OR (@_Type = 'P' AND PalletCode = @_Barcode) 
		)

	SET @_Data = 
		'<BaseEntity xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="PalletStatus">' + 
			(SELECT * FROM dbo.PalletStatuses ps WHERE (@_Type = 'C' AND ps.CartonBarcode = @_Barcode) OR (@_Type = 'E' AND ps.ProductBarcode = @_Barcode) FOR XML PATH('PalletStatus')) + 
		'</BaseEntity>'
	
	EXEC dbo.proc_AuditTrails_Insert @_UserID, @_SitemapID, 'PalletStatuses', @_Data, 'UPD', @_Method, @_Date, @_Date

	--CLear pallet if not have any product
	IF NOT EXISTS (SELECT TOP 1 PalletCode FROM dbo.PalletStatuses WHERE PalletCode = @_OldPalletCode)
		EXEC dbo.proc_Pallets_Update_Status @_OldPalletCode, @_OldPalletCode, NULL, NULL, 'N', @_UserID, @_SitemapID, @_Method

	--UPDATE PreparedQty
	EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity] @_OldPalletCode
	EXEC [proc_DeliveryOrderDetails_UpdatePreparedQuantity] @_PalletCode
END