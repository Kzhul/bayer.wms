﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_DeliveryNote_Select]
	@ExportCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_ExportCode VARCHAR(255) = @ExportCode
	
	SELECT
		dnh.ExportCode
		, dnh.DOImportCode
		, dnh.DeliveryDate
		, dnd.BatchCode
		, dnd.Quantity
		, dnd.PackQuantity
		, dnd.ProductQuantity
		, RemainQty = dnd.Quantity
		, ExportedQty = ISNULL(dnd.ExportedQty, 0) - ISNULL(dnd.ExportReturnedQty, 0)
		, ConfirmQty = ISNULL(dnd.ConfirmQty, 0)
		, ReceivedQty = ISNULL(dnd.ReceivedQty, 0) - ISNULL(dnd.ReceiveReturnedQty, 0)
		, ExportReturnedQty = ISNULL(dnd.ExportReturnedQty, 0)
		, ReceiveReturnedQty = ISNULL(dnd.ReceiveReturnedQty, 0)
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
	FROM
		dbo.DeliveryNoteHeaders dnh WITH (NOLOCK)
		JOIN dbo.DeliveryNoteDetails dnd WITH (NOLOCK) ON dnd.ExportCode = dnh.ExportCode
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = dnd.ProductID
												  AND p.IsDeleted = 0
	WHERE
		dnh.ExportCode = @_ExportCode
	ORDER BY
		p.Description
END