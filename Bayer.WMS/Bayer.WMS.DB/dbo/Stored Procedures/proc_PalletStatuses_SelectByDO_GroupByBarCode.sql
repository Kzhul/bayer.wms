﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_PalletStatuses_SelectByDO_GroupByBarCode]
	@CompanyCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_Barcode VARCHAR(255) = @CompanyCode

	SELECT DISTINCT
		# = ROW_NUMBER() OVER(ORDER BY pd.Description, P.ProductLot ASC)
		, P.ProductLot		
		, ProductDescription = pd.Description		
		, DOQuantity = 0
		, NeedPrepareQuantity = 0
		, Quantity = ISNULL(tmp1.Quantity,0)
		, CartonOddQuantity = CASE WHEN pd.PackingType = 'C' THEN 
																CASE WHEN ISNULL(tmpC.CartonQuantity,0) = 0 AND ISNULL(tmp.OddQuantity, 0) = 0
																	THEN ''
																   WHEN ISNULL(tmp.OddQuantity, 0) = 0
																	THEN CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
																		+ 'T'
																	ELSE 
																		CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
																		+ 'T / ' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
																	END

										WHEN pd.PackingType = 'B' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' B' 
										WHEN pd.PackingType = 'S' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' X' 
										WHEN pd.PackingType = 'A' THEN CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0)) + ' C'
										ELSE '0' END
		
		, pd.PackingType
		, pd.ProductID	
		, pd.Description	
		, pd.ProductCode
	INTO #TB
	FROM
		--Pallets P WITH (NOLOCK)
		--Get list of ProductID and ProductLot for this company
		(
			SELECT DISTINCT
				 PS.ProductID
				 ,PS.ProductLot
			FROM 
				Pallets P WITH (NOLOCK)
				LEFT JOIN PalletStatuses PS WITH (NOLOCK)
					ON P.PalletCode = PS.PalletCode
			WHERE
				P.CompanyCode = @_Barcode
			UNION
			SELECT DISTINCT
				 PS.ProductID
				 ,ProductLot = PS.BatchCode
			FROM 
				DeliveryOrderDetails PS WITH (NOLOCK)
			WHERE
				DATEDIFF(DD,PS.DeliveryDate,GETDATE()) >= 0 
				AND PS.CompanyCode = @_Barcode
		) AS P
		--Get total quantity
		LEFT JOIN 
		(
			SELECT
				ps.ProductID
				, ps.ProductLot
				, Quantity = COUNT(DISTINCT ps.ProductBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_Barcode
			GROUP BY
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
		) tmp1
			ON P.ProductID = tmp1.ProductID
			AND P.ProductLot = tmp1.ProductLot
		--Get odd quantity
		LEFT JOIN 
		(
			SELECT
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
				, OddQuantity = COUNT(DISTINCT ps.ProductBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_Barcode
				AND (ps.CartonBarcode IS NULL OR CartonBarcode = '')
			GROUP BY
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
		) tmp
			ON tmp.ProductID = tmp1.ProductID
			AND tmp.ProductLot = tmp1.ProductLot
		--Get carton quantity
		LEFT JOIN 
		(
			SELECT
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
				, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
			FROM
				dbo.PalletStatuses ps WITH (NOLOCK)
				JOIN dbo.Pallets pl WITH (NOLOCK) ON pl.PalletCode = ps.PalletCode
			WHERE
				pl.CompanyCode = @_Barcode
				AND CartonBarcode IS NOT NULL 
				AND CartonBarcode != ''
			GROUP BY
				pl.PalletCode
				, ps.ProductID
				, ps.ProductLot
		) tmpC
			ON tmp1.ProductID = tmpC.ProductID
			AND tmp1.ProductLot = tmpC.ProductLot
		LEFT JOIN dbo.Products pd WITH (NOLOCK) ON pd.ProductID = P.ProductID

	UPDATE #TB
	SET DOQuantity = ISNULL((
						SELECT SUM(Quantity) 
						FROM 
							DeliveryOrderDetails DO WITH (NOLOCK)
						WHERE 
							DATEDIFF(DD,DO.DeliveryDate,GETDATE()) >= 0 
							AND DO.ProductID = #TB.ProductID
							AND DO.BatchCode = #TB.ProductLot
							AND DO.CompanyCode = @_Barcode
					 ),0)
	UPDATE #TB
	SET NeedPrepareQuantity = DOQuantity - Quantity

	SELECT DISTINCT 
		* 
	FROM 
		#TB AS A WITH (NOLOCK)
	ORDER BY A.Description, A.ProductLot
END