﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterials_PickupPallet]
	@DocumentNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_DocumentNbr VARCHAR(255) = @DocumentNbr

--Chỉ xử lý cho hàng nguyên liệu, bao bì
SELECT DISTINCT 
	P.*
INTO #A
FROM
	Products P WITH (NOLOCK)
	JOIN RequestMaterialLines  L WITH (NOLOCK)
		ON L.ProductID = P.ProductID
WHERE
	--P.CategoryID IN (5,6)
	--AND 
	L.DocumentNbr = @_DocumentNbr

--Lấy ra danh sách chưa lấy đủ hàng
SELECT DISTINCT
	ID = ROW_NUMBER() OVER(ORDER BY L.DocumentNbr, L.LineNbr, L.ProductID)
	,[DocumentNbr] = L.DocumentNbr
	,[LineNbr] = L.LineNbr
	,[ProductID] = L.ProductID
	,[RequestQty]
	,[BookedQty] = ISNULL(SUM(SL.Quantity),0)
	--,[ActualPickedQty] = ISNULL(SUM(SL.ActualQuantity),0)
	,[NeedQuantity] = [RequestQty] - ISNULL(SUM(SL.Quantity),0)
	,[WarehouseQuantity] = 0
INTO #TBPicked
FROM 
	RequestMaterialLines L WITH (NOLOCK)
	JOIN #A A
		ON L.ProductID = A.ProductID
	LEFT JOIN [dbo].[RequestMaterialLineSplits] SL WITH (NOLOCK)
		ON L.DocumentNbr = SL.DocumentNbr
		AND L.LineNbr = SL.LineNbr
		AND L.ProductID = SL.ProductID
WHERE
	L.DocumentNbr = @_DocumentNbr
GROUP BY
	L.DocumentNbr
	,L.LineNbr
	,L.ProductID
	,L.RequestQty
HAVING
	ISNULL(SUM(SL.Quantity),0) < [RequestQty]

--Nếu như có sản phẩm còn chưa lấy hết và trong kho còn hàng, bắt đầu lấy hàng
IF EXISTS(
	SELECT TOP 1 * 
	FROM
		#TBPicked P
		JOIN PalletStatuses PS WITH (NOLOCK)
			ON P.ProductID = PS.ProductID
		JOIN #A A
			ON PS.ProductID = A.ProductID
	WHERE
		P.NeedQuantity > 0
		AND PS.PalletCode NOT IN (
			SELECT DISTINCT
				PalletCode
			FROM 
				[RequestMaterialLineSplits] SL WITH (NOLOCK)
			WHERE
				@_DocumentNbr = SL.DocumentNbr
		)
)
BEGIN
	-- Lấy ra số lượng yêu cầu
	-- Sau khi có SL yêu cầu, đi pick từng pallet sao cho đủ SL yêu cầu, ưu tiên lấy pallet hết hạn trước, số lượng ít trước
	DECLARE @I INT
	DECLARE @CNT INT
	DECLARE @ID NVARCHAR(50)
	DECLARE @NeedQuantity INT
	SELECT @I = MIN(ID) - 1, @CNT = MAX(ID) FROM #TBPicked
	WHILE @I < @CNT
	BEGIN
		SELECT @I = @I + 1
		SET @ID = (SELECT TOP 1 ID FROM #TBPicked WHERE ID = @I)
		SET @NeedQuantity = ISNULL((SELECT TOP 1 NeedQuantity FROM #TBPicked WHERE ID = @I),0)
		IF(@NeedQuantity > 0)
		BEGIN
			INSERT INTO [dbo].[RequestMaterialLineSplits]
				   ([DocumentNbr]
				   ,[LineNbr]
				   ,[ProductID]
				   ,[PalletCode]
				   ,[ProductLot]
				   ,[Quantity]
				   ,[Note]
				   ,[Driver]
				   ,[DriverExportDate]
				   ,[WarehouseKeeper]
				   ,[WarehouseVerifyDate]
				   ,[ActualQuantity]
				   ,[DriverToProduction]
				   ,[DriverToProductionDate]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedBySitemapID]
				   ,[CreatedDateTime]
				   ,[UpdatedBy]
				   ,[UpdatedBySitemapID]
				   ,[UpdatedDateTime]
				   ,[ExpiredDate])
	
			SELECT DISTINCT TOP 1
				[DocumentNbr] = (SELECT TOP 1 [DocumentNbr] FROM #TBPicked WHERE ID = @I)
				,[LineNbr] = (SELECT TOP 1 [LineNbr] FROM #TBPicked WHERE ID = @I)
				,[ProductID] = ps.ProductID
				,[PalletCode] = ps.PalletCode
				,[ProductLot] = ps.ProductLot
				,[Quantity] = ISNULL(ps.Qty, 1)
				,[Note] = ''
				,[Driver] = NULL
				,[DriverExportDate] = NULL
				,[WarehouseKeeper] = NULL
				,[WarehouseVerifyDate] = NULL
				,[ActualQuantity] = ISNULL(ps.Qty, 1)
				,[DriverToProduction] = NULL
				,[DriverToProductionDate] = NULL
				,[Status] = 'N'
				,[CreatedBy] = NULL
				,[CreatedBySitemapID] = NULL
				,[CreatedDateTime] = NULL
				,[UpdatedBy] = NULL
				,[UpdatedBySitemapID] = NULL
				,[UpdatedDateTime] = NULL
				,ExpiredDate = ISNULL(srd.ExpiredDate,GETDATE())
			FROM 
				PalletStatuses PS WITH (NOLOCK)
				JOIN #A A
					ON PS.ProductID = A.ProductID
				JOIN #TBPicked PC
					ON PS.ProductID = PC.ProductID
				JOIN dbo.Pallets p  WITH (NOLOCK) ON 
					p.PalletCode = ps.PalletCode
					AND p.Status = 'N'
					AND p.IsDeleted = 0
				LEFT JOIN dbo.StockReceivingDetails srd  WITH (NOLOCK)
					ON srd.ProductID = ps.ProductID
					AND srd.BatchCode = ps.ProductLot
			WHERE
				PC.ID = @ID
				AND PS.PalletCode NOT IN (
					SELECT DISTINCT
						PalletCode
					FROM 
						[RequestMaterialLineSplits] SL WITH (NOLOCK)
					WHERE
						@_DocumentNbr = SL.DocumentNbr
				)
			--ORDER BY
			--	srd.ExpiredDate ASC -- Hết hạn trước lấy trước
				--,ISNULL(ps.Qty, 1) --Có hàng lẻ lấy trước
		END
	END
	
	--Nạp lại số đã lấy
	UPDATE #TBPicked
	SET
		BookedQty = ISNULL((
			SELECT SUM(SL.Quantity) 
			FROM 
				[dbo].[RequestMaterialLineSplits] SL WITH (NOLOCK)
			WHERE
				SL.DocumentNbr = #TBPicked.DocumentNbr
				AND SL.LineNbr = #TBPicked.LineNbr
				AND SL.ProductID = #TBPicked.ProductID
		),0)
	UPDATE #TBPicked
	SET NeedQuantity = RequestQty - BookedQty

	--Nạp lại số đã lấy
	UPDATE [RequestMaterialLines]
	SET
		BookedQty = ISNULL((
			SELECT SUM(SL.Quantity) 
			FROM 
				[dbo].[RequestMaterialLineSplits] SL WITH (NOLOCK)
			WHERE
				SL.DocumentNbr = [RequestMaterialLines].DocumentNbr
				AND SL.LineNbr = [RequestMaterialLines].LineNbr
				AND SL.ProductID = [RequestMaterialLines].ProductID
		),0)

	--Debug
	--SELECT * FROM #TBPicked
END

--Debug
--SELECT TOP 1 * 
--FROM
--	#TBPicked P	
--	JOIN PalletStatuses PS WITH (NOLOCK)
--		ON P.ProductID = PS.ProductID
--	JOIN #A A
--		ON PS.ProductID = A.ProductID
--WHERE
--	P.PickedQty < RequestQty
--	AND PS.PalletCode NOT IN (
--		SELECT DISTINCT
--			PalletCode
--		FROM 
--			[RequestMaterialLineSplits] SL WITH (NOLOCK)
--		WHERE
--			@_DocumentNbr = SL.DocumentNbr
--	)

--Nếu như có sản phẩm còn chưa lấy hết và trong kho còn hàng, bắt đầu lấy hàng
IF EXISTS(
	SELECT TOP 1 * 
	FROM
		#TBPicked P
		JOIN PalletStatuses PS WITH (NOLOCK)
			ON P.ProductID = PS.ProductID
		JOIN #A A
			ON PS.ProductID = A.ProductID
	WHERE
		P.BookedQty <= RequestQty
		AND PS.PalletCode NOT IN (
			SELECT DISTINCT
				PalletCode
			FROM 
				[RequestMaterialLineSplits] SL WITH (NOLOCK)
			WHERE
				@_DocumentNbr = SL.DocumentNbr
		)
)
BEGIN
	EXEC [proc_RequestMaterials_PickupPallet] @_DocumentNbr
END
END