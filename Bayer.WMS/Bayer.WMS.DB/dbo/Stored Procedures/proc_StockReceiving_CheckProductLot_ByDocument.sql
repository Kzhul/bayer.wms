﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_StockReceiving_CheckProductLot_ByDocument]
	@DocumentNbr NVARCHAR(50)
	,@ProductLot NVARCHAR(50)
	,@ProductLotNbr NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_ProductLot NVARCHAR(50) = @ProductLot
	DECLARE @_ProductLotNbr NVARCHAR(50) = @ProductLotNbr
	DECLARE @_Result NVARCHAR(50) = '0'
--1.Kiểm tra xem mã @_ProductLotNbr đã quét rồi hay chưa, quét rồi báo lỗi -1
	IF EXISTS(SELECT * FROM [dbo].[StockReceivingDetailSplits] WITH (NOLOCK) WHERE [ProductLot_NBR] = @_ProductLotNbr)
		BEGIN
			SET @_Result = '-1'
		END
--2.Nếu chưa quét, kiểm tra mã lô này có đúng với phiếu nhập hay không, nếu không báo lỗi -2
	ELSE IF NOT EXISTS (SELECT * FROM [dbo].StockReceivingDetails WITH (NOLOCK) WHERE BatchCode = @_ProductLot)
		BEGIN
			SET @_Result = '-2'
		END
--3.Nếu đúng, trả về 1
	ELSE 
		BEGIN
			SET @_Result = '1'
		END

	SELECT @_Result
END