﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_Pallets_Select2]
	@PalletCode VARCHAR(255)
	,@CompanyCode VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_CompanyCode VARCHAR(255) = @CompanyCode

	SELECT
		*
	FROM
		dbo.Pallets
	WHERE
		PalletCode = @_PalletCode
		AND CompanyCode = @_CompanyCode
END