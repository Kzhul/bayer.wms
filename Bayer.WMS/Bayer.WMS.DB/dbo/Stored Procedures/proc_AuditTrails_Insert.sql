﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_AuditTrails_Insert]
	@UserID INT = NULL
	, @SitemapID INT = NULL
	, @TableName VARCHAR(255) = NULL
	, @Data XML = NULL
	, @Action VARCHAR(255) = NULL
	, @Method VARCHAR(255) = NULL
	, @Date DATE = NULL
	, @DateTime DATETIME = NULL
	, @Description NVARCHAR(MAX) = NULL
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_TableName VARCHAR(255) = @TableName
	DECLARE @_Data XML = @Data
	DECLARE @_Action VARCHAR(255) = @Action
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATE = @Date
	DECLARE @_DateTime DATETIME = @DateTime
	DECLARE @_Description NVARCHAR(MAX) = @Description

    INSERT INTO dbo.AuditTrails
    ( 
		UserID
        , SitemapID
        , TableName
        , Data
        , Action
        , Method
        , Date
		, DateTime
		, Description
    )
    VALUES
	(
		@_UserID
		, @_SitemapID
		, @_TableName
		, @_Data
		, @_Action
		, @_Method
		, @_Date
		, @_DateTime
		, @_Description
	)
END