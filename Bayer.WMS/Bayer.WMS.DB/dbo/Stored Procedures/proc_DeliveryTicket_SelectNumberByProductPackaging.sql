﻿CREATE PROCEDURE [dbo].[proc_DeliveryTicket_SelectNumberByProductPackaging]
	@DeliveryTicketCode NVARCHAR(50)
	,@DOImportCode NVARCHAR(50)
	,@CompanyCode NVARCHAR(50)
	,@PackagingType NVARCHAR(50)
AS
BEGIN

--CASE 
--WHEN @PackingType = 'C' THEN ' T'--Thùng
--WHEN @PackingType = 'CC' THEN ' T'--Thùng chẵn
--WHEN @PackingType = 'CSH' THEN ' T'--Thùng lẻ
--WHEN @PackingType = 'B' THEN ' B'--Bao
--WHEN @PackingType = 'S' THEN ' X'--Xô
--WHEN @PackingType = 'A' THEN ' C'--Can

DECLARE @_DeliveryTicketCode NVARCHAR(50) = @DeliveryTicketCode--'DO18052503'
DECLARE @_DOImportCode NVARCHAR(50) = @DOImportCode--'DO18052503'
DECLARE @_CompanyCode        NVARCHAR(50) = @CompanyCode--'2745613'
DECLARE @_PackagingType      NVARCHAR(50) = @PackagingType--'2745613'

--SELECT DISTINCT 
--	*
--FROM 
--	Pallets P WITH (NOLOCK)
--	JOIN PalletStatuses PS WITH (NOLOCK)
--		ON p.PalletCode = PS.PalletCode
--	JOIN PrepareNoteHeaders PN WITH (NOLOCK)
--		ON P.DOImportCode = PN.DOImportCode
--	JOIN DeliveryTicketDetails DT WITH (NOLOCK)
--		ON DT.PrepareCode = PN.PrepareCode
--WHERE 
--	(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
--	AND (@_DOImportCode = '' OR P.DOImportCode = @_DOImportCode)
--	AND (@_CompanyCode = '' OR P.CompanyCode = @_CompanyCode)

--SELECT DISTINCT 
--	*
--FROM 
--	DeliveryHistories PS WITH (NOLOCK)
--	JOIN PrepareNoteHeaders PN WITH (NOLOCK)
--		ON PS.DeliveryTicketCode = PN.DOImportCode
--	JOIN DeliveryTicketDetails DT WITH (NOLOCK)
--		ON DT.PrepareCode = PN.PrepareCode
--WHERE 
--	(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
--	AND (@_DOImportCode = '' OR PN.DOImportCode = @_DOImportCode)
--	AND (@_CompanyCode = '' OR PN.CompanyCode = @_CompanyCode)



IF(@_PackagingType = 'C')
BEGIN
	SELECT 
		CONVERT(NVARCHAR(50), COUNT(DISTINCT CartonBarcode))
	FROM
	(
		SELECT DISTINCT 
			PS.CartonBarcode
		FROM 
			Pallets P WITH (NOLOCK)
			JOIN PalletStatuses PS WITH (NOLOCK)
				ON p.PalletCode = PS.PalletCode
			JOIN PrepareNoteHeaders PN WITH (NOLOCK)
				ON P.DOImportCode = PN.DOImportCode
			JOIN DeliveryTicketDetails DT WITH (NOLOCK)
				ON DT.PrepareCode = PN.PrepareCode
		WHERE 
			(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
			AND (@_DOImportCode = '' OR P.DOImportCode = @_DOImportCode)
			AND (@_CompanyCode = '' OR P.CompanyCode = @_CompanyCode)
			AND PS.CartonBarcode != ''

		UNION

		SELECT DISTINCT 
			PS.CartonBarcode
		FROM 
			DeliveryHistories PS WITH (NOLOCK)
			JOIN PrepareNoteHeaders PN WITH (NOLOCK)
				ON PS.DeliveryTicketCode = PN.DOImportCode
			JOIN DeliveryTicketDetails DT WITH (NOLOCK)
				ON DT.PrepareCode = PN.PrepareCode
		WHERE 
			(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
			AND (@_DOImportCode = '' OR PN.DOImportCode = @_DOImportCode)
			AND (@_CompanyCode = '' OR PN.CompanyCode = @_CompanyCode)
			AND PS.CartonBarcode != ''
	) AS TB
END
ELSE IF(@_PackagingType = 'CC')
BEGIN
	SELECT 
		CONVERT(NVARCHAR(50), COUNT(DISTINCT CartonBarcode))
	FROM
	(
		SELECT DISTINCT 
			PS.CartonBarcode
		FROM 
			Pallets P WITH (NOLOCK)
			JOIN PalletStatuses PS WITH (NOLOCK)
				ON p.PalletCode = PS.PalletCode
			JOIN PrepareNoteHeaders PN WITH (NOLOCK)
				ON P.DOImportCode = PN.DOImportCode
			JOIN DeliveryTicketDetails DT WITH (NOLOCK)
				ON DT.PrepareCode = PN.PrepareCode
		WHERE 
			(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
			AND (@_DOImportCode = '' OR P.DOImportCode = @_DOImportCode)
			AND (@_CompanyCode = '' OR P.CompanyCode = @_CompanyCode)
			AND PS.CartonBarcode != ''
			AND PS.CartonBarcode NOT LIKE 'SH%'

		UNION

		SELECT DISTINCT 
			PS.CartonBarcode
		FROM 
			DeliveryHistories PS WITH (NOLOCK)
			JOIN PrepareNoteHeaders PN WITH (NOLOCK)
				ON PS.DeliveryTicketCode = PN.DOImportCode
			JOIN DeliveryTicketDetails DT WITH (NOLOCK)
				ON DT.PrepareCode = PN.PrepareCode
		WHERE 
			(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
			AND (@_DOImportCode = '' OR PN.DOImportCode = @_DOImportCode)
			AND (@_CompanyCode = '' OR PN.CompanyCode = @_CompanyCode)
			AND PS.CartonBarcode != ''
			AND PS.CartonBarcode NOT LIKE 'SH%'
	) AS TB
END
ELSE IF(@_PackagingType = 'CSH')
BEGIN
	SELECT 
		CONVERT(NVARCHAR(50), COUNT(DISTINCT CartonBarcode))
	FROM
	(
		SELECT DISTINCT 
			PS.CartonBarcode
		FROM 
			Pallets P WITH (NOLOCK)
			JOIN PalletStatuses PS WITH (NOLOCK)
				ON p.PalletCode = PS.PalletCode
			JOIN PrepareNoteHeaders PN WITH (NOLOCK)
				ON P.DOImportCode = PN.DOImportCode
			JOIN DeliveryTicketDetails DT WITH (NOLOCK)
				ON DT.PrepareCode = PN.PrepareCode
		WHERE 
			(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
			AND (@_DOImportCode = '' OR P.DOImportCode = @_DOImportCode)
			AND (@_CompanyCode = '' OR P.CompanyCode = @_CompanyCode)
			AND PS.CartonBarcode != ''
			AND PS.CartonBarcode LIKE 'SH%'

		UNION

		SELECT DISTINCT 
			PS.CartonBarcode
		FROM 
			DeliveryHistories PS WITH (NOLOCK)
			JOIN PrepareNoteHeaders PN WITH (NOLOCK)
				ON PS.DeliveryTicketCode = PN.DOImportCode
			JOIN DeliveryTicketDetails DT WITH (NOLOCK)
				ON DT.PrepareCode = PN.PrepareCode
		WHERE 
			(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
			AND (@_DOImportCode = '' OR PN.DOImportCode = @_DOImportCode)
			AND (@_CompanyCode = '' OR PN.CompanyCode = @_CompanyCode)
			AND PS.CartonBarcode != ''
			AND PS.CartonBarcode LIKE 'SH%'
	) AS TB
END
ELSE
BEGIN
	SELECT 
		CONVERT(NVARCHAR(50), COUNT(DISTINCT ProductBarcode))
	FROM
	(
		SELECT DISTINCT 
			PS.ProductBarcode
		FROM 
			Pallets P WITH (NOLOCK)
			JOIN PalletStatuses PS WITH (NOLOCK)
				ON p.PalletCode = PS.PalletCode
			JOIN PrepareNoteHeaders PN WITH (NOLOCK)
				ON P.DOImportCode = PN.DOImportCode
			JOIN DeliveryTicketDetails DT WITH (NOLOCK)
				ON DT.PrepareCode = PN.PrepareCode
			JOIN Products PD WITH (NOLOCK)
				ON PS.ProductID = PD.ProductID
		WHERE 
			(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
			AND (@_DOImportCode = '' OR P.DOImportCode = @_DOImportCode)
			AND (@_CompanyCode = '' OR P.CompanyCode = @_CompanyCode)
			AND PD.PackingType = @_PackagingType

		UNION

		SELECT DISTINCT 
			PS.ProductBarcode
		FROM 
			DeliveryHistories PS WITH (NOLOCK)
			JOIN PrepareNoteHeaders PN WITH (NOLOCK)
				ON PS.DeliveryTicketCode = PN.DOImportCode
			JOIN DeliveryTicketDetails DT WITH (NOLOCK)
				ON DT.PrepareCode = PN.PrepareCode
			JOIN Products PD WITH (NOLOCK)
				ON PS.ProductID = PD.ProductID
		WHERE 
			(@_DeliveryTicketCode = '' OR DT.DeliveryTicketCode = @_DeliveryTicketCode)
			AND (@_DOImportCode = '' OR PN.DOImportCode = @_DOImportCode)
			AND (@_CompanyCode = '' OR PN.CompanyCode = @_CompanyCode)
			AND PD.PackingType = @_PackagingType

	) AS TB
END

END