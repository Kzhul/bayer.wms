﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_ReturnPallet]
	@DocumentNbr NVARCHAR(50)
	,@PalletCode NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @_DocumentNbr NVARCHAR(50) = @DocumentNbr
	DECLARE @_PalletCode NVARCHAR(50) = @PalletCode

	UPDATE Pallets 
	SET 
		[Status] = 'N' --Booking
		, ReferenceNbr = ''
	WHERE
		PalletCode = @_PalletCode

	DELETE FROM
		[RequestMaterialLineSplits]
	WHERE
		PalletCode = @_PalletCode

	UPDATE [dbo].[RequestMaterialLines]
	SET 
		[PickedQty] = 
		(
			SELECT
				SUM(RS.[Quantity])
			FROM
				[RequestMaterialLines] RML
				JOIN [dbo].[RequestMaterialLineSplits] RS
					ON RML.DocumentNbr = RS.DocumentNbr
					AND RML.ProductID = RS.ProductID
			WHERE
				RML.DocumentNbr = @_DocumentNbr
				AND [RequestMaterialLines].ProductID = RML.ProductID
		)
	WHERE
		DocumentNbr = @_DocumentNbr
END