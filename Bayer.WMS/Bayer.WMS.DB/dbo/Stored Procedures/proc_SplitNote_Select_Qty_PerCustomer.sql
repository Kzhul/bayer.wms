﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_SplitNote_Select_Qty_PerCustomer]
	@SplitCode VARCHAR(255)
	, @ProductIDList VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

    DECLARE @_SplitCode VARCHAR(255) = @SplitCode
	DECLARE @_ProductIDList VARCHAR(MAX) = @ProductIDList

	SELECT
		snd.SplitCode
		, snd.CompanyCode
		, snd.CompanyName
		, snd.Quantity
		, SplittedQty = ISNULL(snd.SplittedQty, 0)
		, p.ProductID
		, p.ProductCode
		, ProductDescription = p.Description
	FROM
		dbo.SplitNoteDetails snd WITH (NOLOCK)
		LEFT JOIN dbo.Products p WITH (NOLOCK) ON p.ProductID = snd.ProductID
												  AND p.IsDeleted = 0
	WHERE
		snd.SplitCode = @_SplitCode
		AND ',' + @_ProductIDList + ',' LIKE '%,' + CONVERT(VARCHAR(255), p.ProductID) + ',%'
END