﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[proc_RequestMaterial_DriverPickedPallet]
	@PalletCode VARCHAR(255)
	, @UserID INT
	, @SitemapID INT
	, @Method VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @_PalletCode VARCHAR(255) = @PalletCode
	DECLARE @_DocumentNbr NVARCHAR(50) = (SELECT TOP 1 ReferenceNbr FROM Pallets WITH (NOLOCK) WHERE PalletCode = @PalletCode)
	DECLARE @_UserID INT = @UserID
	DECLARE @_SitemapID INT = @SitemapID
	DECLARE @_Method VARCHAR(255) = @Method
	DECLARE @_Date DATETIME = GETDATE()

--1.Update [Pallets] set trạng thái về P, Đã mang hàng xuống, đợi thủ kho duyệt, hoặc mang thẳng xuống SX
	UPDATE Pallets
	SET 
		[Status] = 'P'
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode

--2.Update [RequestMaterialLineSplits] SET trạng thái là P
	UPDATE RequestMaterialLineSplits
	SET 
		[Status] = 'P'
		, Driver = @_UserID
		, DriverExportDate = @_Date
		, UpdatedBy = @_UserID
		, UpdatedBySitemapID = @_SitemapID
		, UpdatedDateTime = @_Date
	WHERE
		PalletCode = @_PalletCode
		AND DocumentNbr = @_DocumentNbr

--3.Update [StockReceivingDetail] 
	--Nạp lại số đã lấy
	UPDATE [RequestMaterialLines]
	SET
		PickedQty = (
			SELECT SUM(SL.Quantity) 
			FROM 
				[dbo].[RequestMaterialLineSplits] SL WITH (NOLOCK)
			WHERE
				SL.DocumentNbr = [RequestMaterialLines].DocumentNbr
				AND SL.LineNbr = [RequestMaterialLines].LineNbr
				AND SL.ProductID = [RequestMaterialLines].ProductID
				AND [Status] = 'P'
		)


END