﻿CREATE TABLE [dbo].[DeliveryOrderProductBarcodes] (
    [DOImportCode]       VARCHAR (255)  NULL,
    [ProductID]          INT            NULL,
    [BatchCode]          VARCHAR (255)  NULL,
    [PalletCode]         VARCHAR (255)  NULL,
    [CartonBarcode]      VARCHAR (255)  NULL,
    [ProductBarcode]     VARCHAR (255)  NOT NULL,
    [CompanyCode]        VARCHAR (255)  NULL,
    [CompanyName]        NVARCHAR (255) NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_DeliveryOrderProductBarcodes_1] PRIMARY KEY CLUSTERED ([ProductBarcode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DeliveryOrderProductBarcodes_DOImportCode_CompanyCode]
    ON [dbo].[DeliveryOrderProductBarcodes]([DOImportCode] ASC, [CompanyCode] ASC);

