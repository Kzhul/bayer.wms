﻿CREATE TABLE [dbo].[Bay4SData] (
    [ID]                   INT             IDENTITY (1, 1) NOT NULL,
    [ProductCode]          NVARCHAR (50)   NULL,
    [ProductName]          NVARCHAR (250)  NULL,
    [BatchCode]            NVARCHAR (50)   NULL,
    [BatchCodeDistributor] NVARCHAR (50)   NULL,
    [UOM]                  NVARCHAR (10)   NULL,
    [ExpiredDate]          DATETIME        NULL,
    [Unrestricted]         DECIMAL (18, 2) NULL,
    [InQualityInsp]        DECIMAL (18, 2) NULL,
    [Blocked]              DECIMAL (18, 2) NULL,
    [ManufacturingDate]    DATETIME        NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    CONSTRAINT [PK_Bay4SData] PRIMARY KEY CLUSTERED ([ID] ASC)
);

