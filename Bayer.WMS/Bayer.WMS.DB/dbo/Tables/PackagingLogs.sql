﻿CREATE TABLE [dbo].[PackagingLogs] (
    [Barcode]            VARCHAR (255) NOT NULL,
    [EncryptedBarcode]   VARCHAR (255) NULL,
    [ProductLot]         VARCHAR (255) NULL,
    [ProductID]          INT           NULL,
    [Date]               DATETIME      NULL,
    [CartonBarcode]      VARCHAR (255) NULL,
    [PalletBarcode]      VARCHAR (255) NULL,
    [UserID]             INT           NULL,
    [Device]             VARCHAR (255) NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_PackagingLogs] PRIMARY KEY CLUSTERED ([Barcode] ASC)
);














GO
CREATE NONCLUSTERED INDEX [IX_PackagingLogs_ProductLot]
    ON [dbo].[PackagingLogs]([ProductLot] ASC)
    INCLUDE([Barcode]);

