﻿CREATE TABLE [dbo].[PassCode] (
    [ID]                 INT           IDENTITY (1, 1) NOT NULL,
    [WorkingDate]        DATE          NOT NULL,
    [Code]               NVARCHAR (50) NOT NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_PassCode] PRIMARY KEY CLUSTERED ([ID] ASC)
);

