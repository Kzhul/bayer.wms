﻿CREATE TABLE [dbo].[DeliveryOrderSum] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [DOImportCode]       VARCHAR (255)   NULL,
    [CompanyCode]        VARCHAR (255)   NULL,
    [ProductID]          INT             NULL,
    [BatchCode]          VARCHAR (255)   NULL,
    [DeliveryDate]       DATE            NULL,
    [DOQuantity]         DECIMAL (38, 2) NULL,
    [ExportedQty]        INT             NULL,
    [ReceivedQty]        INT             NULL,
    [PreparedQty]        INT             NOT NULL,
    [DeliveredQty]       INT             NOT NULL,
    [CreatedBy]          INT             NULL,
    [CreatedBySitemapID] INT             NULL,
    [CreatedDateTime]    DATETIME        NULL,
    [UpdatedBy]          INT             NULL,
    [UpdatedBySitemapID] INT             NULL,
    [UpdatedDateTime]    DATETIME        NULL,
    [RowVersion]         ROWVERSION      NULL,
    CONSTRAINT [PK_DeliveryOrderSum] PRIMARY KEY CLUSTERED ([ID] ASC)
);



