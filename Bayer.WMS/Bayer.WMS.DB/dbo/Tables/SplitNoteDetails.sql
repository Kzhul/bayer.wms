﻿CREATE TABLE [dbo].[SplitNoteDetails] (
    [SplitCode]            NVARCHAR (30)   NOT NULL,
    [DOCode]               NVARCHAR (30)   NOT NULL,
    [DeliveryCode]         NVARCHAR (30)   NOT NULL,
    [ProductID]            INT             NOT NULL,
    [BatchCode]            NVARCHAR (30)   NOT NULL,
    [BatchCodeDistributor] NVARCHAR (30)   NULL,
    [CompanyCode]          NVARCHAR (30)   NOT NULL,
    [CompanyName]          NVARCHAR (255)  NULL,
    [Province]             NVARCHAR (255)  NULL,
    [Quantity]             DECIMAL (18, 2) NULL,
    [PackQuantity]         DECIMAL (18, 2) NULL,
    [PackType]             VARCHAR (255)   NULL,
    [ProductQuantity]      DECIMAL (18, 2) NULL,
    [SplittedQty]          DECIMAL (18, 2) NULL,
    [RequireReturnQty]     DECIMAL (18, 2) NULL,
    [ReturnedQty]          DECIMAL (18, 2) NULL,
    [Status]               CHAR (1)        NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    [LastQuantity]         DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_SplitNoteDetails] PRIMARY KEY CLUSTERED ([SplitCode] ASC, [DOCode] ASC, [DeliveryCode] ASC, [ProductID] ASC, [BatchCode] ASC, [CompanyCode] ASC)
);

























