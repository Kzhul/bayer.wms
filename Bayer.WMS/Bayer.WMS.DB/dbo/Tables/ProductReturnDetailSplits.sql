﻿CREATE TABLE [dbo].[ProductReturnDetailSplits] (
    [ProductReturnCode]       NVARCHAR (50)  NOT NULL,
    [LineNbr]                 INT            NOT NULL,
    [ProductID]               INT            NULL,
    [BatchCode]               NVARCHAR (50)  NULL,
    [ProductBarcode]          NVARCHAR (255) NOT NULL,
    [EncryptedProductBarcode] NVARCHAR (255) NULL,
    [CartonBarcode]           NVARCHAR (255) NULL,
    [PalletCode]              NVARCHAR (50)  NULL,
    [DeliveryDate]            DATETIME       NULL,
    [Description]             NVARCHAR (255) NULL,
    [Status]                  NVARCHAR (1)   NULL,
    [IsDeleted]               INT            NULL,
    [CreatedBy]               INT            NULL,
    [CreatedBySitemapID]      INT            NULL,
    [CreatedDateTime]         DATETIME       NULL,
    [UpdatedBy]               INT            NULL,
    [UpdatedBySitemapID]      INT            NULL,
    [UpdatedDateTime]         DATETIME       NULL,
    [RowVersion]              ROWVERSION     NULL,
    CONSTRAINT [PK_ProductReturnDetailSplits] PRIMARY KEY CLUSTERED ([ProductReturnCode] ASC, [LineNbr] ASC, [ProductBarcode] ASC)
);

