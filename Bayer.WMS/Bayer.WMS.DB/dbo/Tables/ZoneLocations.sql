﻿CREATE TABLE [dbo].[ZoneLocations] (
    [LocationCode]       VARCHAR (255)  NOT NULL,
    [ZoneCode]           VARCHAR (255)  NOT NULL,
    [LineCode]           VARCHAR (255)  NULL,
    [LocationName]       NVARCHAR (255) NULL,
    [Description]        NVARCHAR (500) NULL,
    [LengthID]           INT            NULL,
    [LevelID]            INT            NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    [CurrentPalletCode]  VARCHAR (50)   NULL,
    CONSTRAINT [PK_ZoneLocations_1] PRIMARY KEY CLUSTERED ([LocationCode] ASC, [ZoneCode] ASC)
);







