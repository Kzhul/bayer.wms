﻿CREATE TABLE [dbo].[Users] (
    [UserID]                    INT            IDENTITY (1, 1) NOT NULL,
    [Username]                  VARCHAR (255)  NOT NULL,
    [Password]                  VARCHAR (255)  NULL,
    [FirstName]                 NVARCHAR (255) NOT NULL,
    [LastName]                  NVARCHAR (255) NOT NULL,
    [Email]                     VARCHAR (255)  NULL,
    [SitemapID]                 INT            NULL,
    [Status]                    CHAR (1)       NOT NULL,
    [PasswordChangeOnNextLogin] BIT            NULL,
    [IsDeleted]                 BIT            NULL,
    [CreatedBy]                 INT            NULL,
    [CreatedBySitemapID]        INT            NULL,
    [CreatedDateTime]           DATETIME       NULL,
    [UpdatedBy]                 INT            NULL,
    [UpdatedBySitemapID]        INT            NULL,
    [UpdatedDateTime]           DATETIME       NULL,
    [RowVersion]                ROWVERSION     NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserID] ASC),
    CONSTRAINT [UK_Users_Username] UNIQUE NONCLUSTERED ([Username] ASC)
);



