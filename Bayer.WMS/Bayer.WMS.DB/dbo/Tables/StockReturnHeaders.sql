﻿CREATE TABLE [dbo].[StockReturnHeaders] (
    [StockReturnCode]    VARCHAR (255)  NOT NULL,
    [Description]        NVARCHAR (255) NULL,
    [Date]               DATE           NULL,
    [IsDeleted]          BIT            NULL,
    [Status]             CHAR (1)       NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_StockReturnCode] PRIMARY KEY CLUSTERED ([StockReturnCode] ASC)
);

