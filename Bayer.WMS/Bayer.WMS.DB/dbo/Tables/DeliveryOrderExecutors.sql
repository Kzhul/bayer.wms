﻿CREATE TABLE [dbo].[DeliveryOrderExecutors] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [DOImportCode] VARCHAR (255) NULL,
    [UserID]       INT           NULL,
    [Type]         CHAR (1)      NULL,
    CONSTRAINT [PK_DeliveryOrderExecutors] PRIMARY KEY CLUSTERED ([ID] ASC)
);

