﻿CREATE TABLE [dbo].[DeliveryTicketDetails] (
    [DeliveryTicketCode] NVARCHAR (30) NOT NULL,
    [PrepareCode]        NVARCHAR (30) NOT NULL,
    [Status]             CHAR (1)      NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_DeliveryTicketDetails] PRIMARY KEY CLUSTERED ([DeliveryTicketCode] ASC, [PrepareCode] ASC)
);

