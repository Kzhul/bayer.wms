﻿CREATE TABLE [dbo].[StockReceivingDetails] (
    [StockReceivingCode]   NVARCHAR (50)   NOT NULL,
    [LineNbr]              INT             NOT NULL,
    [CompanyName]          NVARCHAR (255)  NULL,
    [ProductID]            INT             NULL,
    [BatchCode]            NVARCHAR (50)   NULL,
    [BatchCodePrintedNbr]  INT             NULL,
    [BatchCodeDistributor] NVARCHAR (50)   NULL,
    [ExpiredDate]          DATE            NULL,
    [POCode]               NVARCHAR (50)   NULL,
    [POLine]               NVARCHAR (50)   NULL,
    [UOM]                  NVARCHAR (50)   NULL,
    [QuantityPlanning]     DECIMAL (18, 2) NULL,
    [CartonPlanning]       DECIMAL (18, 2) NULL,
    [PalletPlanning]       DECIMAL (18, 2) NULL,
    [QuantityReceived]     DECIMAL (18, 2) NULL,
    [CartonReceived]       DECIMAL (18, 2) NULL,
    [PalletReceived]       DECIMAL (18, 2) NULL,
    [PalletSetLocation]    DECIMAL (18, 2) NULL,
    [DeliveryDate]         DATE            NULL,
    [Description]          NVARCHAR (255)  NULL,
    [Status]               NVARCHAR (1)    NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    [ExpiryDate]           DATE            NULL,
    [DeliveryHour]         NVARCHAR (255)  NULL,
    [Receiver]             NVARCHAR (255)  NULL,
    [ManufacturingDate]    DATE            NULL,
    CONSTRAINT [PK_StockReceivingDetails] PRIMARY KEY CLUSTERED ([StockReceivingCode] ASC, [LineNbr] ASC)
);










GO
CREATE NONCLUSTERED INDEX [IX_StockReceivingDetails]
    ON [dbo].[StockReceivingDetails]([BatchCode] ASC, [ProductID] ASC);

