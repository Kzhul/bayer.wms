﻿CREATE TABLE [dbo].[Companies] (
    [CompanyCode]        NVARCHAR (30)  NOT NULL,
    [CompanyName]        NVARCHAR (250) NULL,
    [CompanyAddress]     NVARCHAR (250) NULL,
    [ProvinceSoldTo]     NVARCHAR (250) NULL,
    [ProvinceShipTo]     NVARCHAR (250) NULL,
    [Description]        NVARCHAR (255) NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED ([CompanyCode] ASC)
);





