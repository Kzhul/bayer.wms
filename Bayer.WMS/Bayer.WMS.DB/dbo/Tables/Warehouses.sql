﻿CREATE TABLE [dbo].[Warehouses] (
    [WarehouseID]        INT            IDENTITY (1, 1) NOT NULL,
    [WarehouseCode]      VARCHAR (255)  NULL,
    [Description]        NVARCHAR (255) NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_Warehouses] PRIMARY KEY CLUSTERED ([WarehouseID] ASC),
    CONSTRAINT [UK_Warehouses_WarehouseCode] UNIQUE NONCLUSTERED ([WarehouseCode] ASC)
);

