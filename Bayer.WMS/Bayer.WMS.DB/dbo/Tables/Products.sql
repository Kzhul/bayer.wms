﻿CREATE TABLE [dbo].[Products] (
    [ProductID]            INT             IDENTITY (1, 1) NOT NULL,
    [ProductCode]          VARCHAR (255)   NULL,
    [Description]          NVARCHAR (255)  NULL,
    [CategoryID]           INT             NULL,
    [Type]                 CHAR (1)        NULL,
    [UOM]                  VARCHAR (255)   NULL,
    [Status]               CHAR (1)        NULL,
    [IsDeleted]            BIT             NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    [PackingType]          CHAR (1)        NULL,
    [SampleQty]            INT             NULL,
    [PrintLabelPercentage] DECIMAL (18, 2) NULL,
    [PalletSize]           INT             NULL,
    [QuantityByUnit]       DECIMAL (18, 2) NULL,
    [QuantityByCarton]     DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([ProductID] ASC),
    CONSTRAINT [UK_Products_ProductCode] UNIQUE NONCLUSTERED ([ProductCode] ASC)
);











