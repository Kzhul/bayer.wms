﻿CREATE TABLE [dbo].[PackagingErrors] (
    [ID]                 INT            IDENTITY (1, 1) NOT NULL,
    [Barcode]            VARCHAR (255)  NULL,
    [EncryptedBarcode]   VARCHAR (255)  NULL,
    [Type]               CHAR (1)       NULL,
    [ProductLot]         VARCHAR (255)  NULL,
    [ProductID]          INT            NULL,
    [PackagingDate]      DATETIME       NULL,
    [ProcessDate]        DATETIME       NULL,
    [UserID]             INT            NULL,
    [Device]             VARCHAR (255)  NULL,
    [Note]               NVARCHAR (255) NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_PackagingErrors] PRIMARY KEY CLUSTERED ([ID] ASC)
);










GO
CREATE NONCLUSTERED INDEX [IX_PackagingErrors_ProductLot]
    ON [dbo].[PackagingErrors]([ProductLot] ASC)
    INCLUDE([ID]);

