﻿CREATE TABLE [dbo].[ReturnDODetails] (
    [ReturnDOCode]         NVARCHAR (50)   NOT NULL,
    [LineNbr]              INT             NOT NULL,
    [DOImportCode]         NVARCHAR (50)   NULL,
    [Delivery]             NVARCHAR (50)   NOT NULL,
    [ProductID]            INT             NOT NULL,
    [BatchCode]            NVARCHAR (50)   NOT NULL,
    [BatchCodeDistributor] NVARCHAR (50)   NULL,
    [Quantity]             DECIMAL (18, 2) NULL,
    [CompanyCode]          NVARCHAR (50)   NULL,
    [CompanyName]          NVARCHAR (255)  NULL,
    [Province]             NVARCHAR (255)  NULL,
    [DeliveryDate]         DATE            NULL,
    [RequestReturnQty]     DECIMAL (18, 2) NULL,
    [ExportedReturnQty]    DECIMAL (18, 2) NULL,
    [ReceivedReturnQty]    DECIMAL (18, 2) NULL,
    [Description]          NVARCHAR (255)  NULL,
    [Status]               CHAR (1)        NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    CONSTRAINT [PK_ReturnDODetails] PRIMARY KEY CLUSTERED ([ReturnDOCode] ASC, [LineNbr] ASC)
);



