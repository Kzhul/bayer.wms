﻿CREATE TABLE [dbo].[Pallets] (
    [PalletCode]            VARCHAR (255)  NOT NULL,
    [Type]                  CHAR (2)       NULL,
    [StartDate]             DATE           NULL,
    [UsageTime]             INT            NULL,
    [ExpiryDate]            DATE           NULL,
    [Sequence]              INT            NULL,
    [DOImportCode]          VARCHAR (255)  NULL,
    [ReferenceNbr]          VARCHAR (255)  NULL,
    [CompanyCode]           VARCHAR (255)  NULL,
    [CapacityStatus]        CHAR (1)       NULL,
    [Status]                CHAR (1)       NULL,
    [IsDeleted]             BIT            NULL,
    [CreatedBy]             INT            NULL,
    [CreatedBySitemapID]    INT            NULL,
    [CreatedDateTime]       DATETIME       NULL,
    [UpdatedBy]             INT            NULL,
    [UpdatedBySitemapID]    INT            NULL,
    [UpdatedDateTime]       DATETIME       NULL,
    [RowVersion]            ROWVERSION     NULL,
    [LocationCode]          VARCHAR (255)  NULL,
    [LocationSuggestion]    VARCHAR (255)  NULL,
    [BatchCode]             VARCHAR (255)  NULL,
    [BatchCodeDistributor]  VARCHAR (255)  NULL,
    [ImportStatus]          CHAR (1)       NULL,
    [Weigh]                 DECIMAL (18)   NULL,
    [CartonNumber]          DECIMAL (18)   NULL,
    [WarehouseKeeper]       INT            NULL,
    [WarehouseVerifyDate]   DATETIME       NULL,
    [WarehouseVerifyStatus] NVARCHAR (255) NULL,
    [WarehouseVerifyNote]   NVARCHAR (255) NULL,
    [Driver]                INT            NULL,
    [LocationPutDate]       DATETIME       NULL,
    [DriverReceived]        INT            NULL,
    [DriverReceivedDate]    DATETIME       NULL,
    [PrintDate]             DATETIME       NULL,
    [UserPrint]             INT            NULL,
    [PrintedTime]           INT            NULL,
    [LastLocationCode]      VARCHAR (255)  NULL,
    [LastDriver]            INT            NULL,
    [LastLocationPutDate]   DATETIME       NULL,
    CONSTRAINT [PK_Pallets] PRIMARY KEY CLUSTERED ([PalletCode] ASC)
);





















