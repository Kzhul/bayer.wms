﻿CREATE TABLE [dbo].[CartonWarehouseBarcodes] (
    [Barcode]            VARCHAR (255)   NOT NULL,
    [PrepareCode]        VARCHAR (255)   NULL,
    [Weight]             DECIMAL (18, 2) NULL,
    [Status]             CHAR (1)        NULL,
    [CreatedBy]          INT             NULL,
    [CreatedBySitemapID] INT             NULL,
    [CreatedDateTime]    DATETIME        NULL,
    [UpdatedBy]          INT             NULL,
    [UpdatedBySitemapID] INT             NULL,
    [UpdatedDateTime]    DATETIME        NULL,
    [RowVersion]         ROWVERSION      NULL,
    CONSTRAINT [PK_CartonWarehouseBarcodes] PRIMARY KEY CLUSTERED ([Barcode] ASC)
);



