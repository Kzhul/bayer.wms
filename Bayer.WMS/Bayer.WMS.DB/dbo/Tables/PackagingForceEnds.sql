﻿CREATE TABLE [dbo].[PackagingForceEnds] (
    [ProductLot]         VARCHAR (255)  NOT NULL,
    [UserID]             INT            NULL,
    [Note]               NVARCHAR (255) NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_PackagingForceEnds] PRIMARY KEY CLUSTERED ([ProductLot] ASC)
);

