﻿CREATE TABLE [dbo].[UsersRoles] (
    [UserID]             INT        NOT NULL,
    [RoleID]             INT        NOT NULL,
    [CreatedBy]          INT        NULL,
    [CreatedBySitemapID] INT        NULL,
    [CreatedDateTime]    DATETIME   NULL,
    [UpdatedBy]          INT        NULL,
    [UpdatedBySitemapID] INT        NULL,
    [UpdatedDateTime]    DATETIME   NULL,
    [RowVersion]         ROWVERSION NULL,
    CONSTRAINT [PK_UsersRoles] PRIMARY KEY CLUSTERED ([UserID] ASC, [RoleID] ASC)
);



