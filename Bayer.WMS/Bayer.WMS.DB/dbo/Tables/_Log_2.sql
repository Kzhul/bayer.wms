﻿CREATE TABLE [dbo].[_Log] (
    [ID]                        INT            IDENTITY (1, 1) NOT NULL,
    [Message]                   NVARCHAR (MAX) NULL,
    [InnerException_Message]    NVARCHAR (MAX) NULL,
    [StackTrace]                NVARCHAR (MAX) NULL,
    [InnerException_StackTrace] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__Log] PRIMARY KEY CLUSTERED ([ID] ASC)
);

