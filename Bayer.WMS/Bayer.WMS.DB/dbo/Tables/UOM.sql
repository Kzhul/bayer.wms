﻿CREATE TABLE [dbo].[UOM] (
    [UOM]                VARCHAR (255)  NOT NULL,
    [Description]        NVARCHAR (255) NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_UOM] PRIMARY KEY CLUSTERED ([UOM] ASC)
);



