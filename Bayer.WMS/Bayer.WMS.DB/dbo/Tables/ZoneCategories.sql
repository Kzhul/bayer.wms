﻿CREATE TABLE [dbo].[ZoneCategories] (
    [ZoneCode]           VARCHAR (255) NOT NULL,
    [CategoryID]         INT           NOT NULL,
    [CreatedBy]          INT           NULL,
    [CreatedBySitemapID] INT           NULL,
    [CreatedDateTime]    DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedBySitemapID] INT           NULL,
    [UpdatedDateTime]    DATETIME      NULL,
    [RowVersion]         ROWVERSION    NULL,
    CONSTRAINT [PK_ZoneCategories] PRIMARY KEY CLUSTERED ([ZoneCode] ASC, [CategoryID] ASC)
);







