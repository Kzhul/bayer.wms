﻿CREATE TABLE [dbo].[References] (
    [GroupCode]          VARCHAR (50)   NOT NULL,
    [SortID]             INT            NOT NULL,
    [RefValue]           VARCHAR (50)   NOT NULL,
    [RefName]            NVARCHAR (50)  NULL,
    [Description]        NVARCHAR (500) NULL,
    [Status]             CHAR (1)       NULL,
    [IsDeleted]          BIT            NULL,
    [CreatedBy]          INT            NULL,
    [CreatedBySitemapID] INT            NULL,
    [CreatedDateTime]    DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedBySitemapID] INT            NULL,
    [UpdatedDateTime]    DATETIME       NULL,
    [RowVersion]         ROWVERSION     NULL,
    CONSTRAINT [PK_Reference] PRIMARY KEY CLUSTERED ([GroupCode] ASC, [RefValue] ASC)
);



