﻿CREATE TABLE [dbo].[StockReceivingDetailSplits] (
    [StockReceivingCode]  VARCHAR (50)    NOT NULL,
    [LineNbr]             INT             NOT NULL,
    [ProductID]           INT             NULL,
    [PalletCode]          VARCHAR (50)    NOT NULL,
    [ProductLot]          VARCHAR (50)    NOT NULL,
    [ProductLot_NBR]      VARCHAR (50)    NULL,
    [Quantity]            DECIMAL (18, 2) NULL,
    [ReceiverID]          INT             NULL,
    [ReceiveDateTime]     DATETIME        NULL,
    [ForkliftID]          INT             NULL,
    [ForkliftDateTime]    DATETIME        NULL,
    [SetLocationDateTime] DATETIME        NULL,
    [Status]              CHAR (1)        NULL,
    [CreatedBy]           INT             NULL,
    [CreatedBySitemapID]  INT             NULL,
    [CreatedDateTime]     DATETIME        NULL,
    [UpdatedBy]           INT             NULL,
    [UpdatedBySitemapID]  INT             NULL,
    [UpdatedDateTime]     DATETIME        NULL,
    [RowVersion]          ROWVERSION      NULL,
    CONSTRAINT [PK_StockReceivingDetailSplits] PRIMARY KEY CLUSTERED ([StockReceivingCode] ASC, [LineNbr] ASC, [PalletCode] ASC, [ProductLot] ASC)
);



