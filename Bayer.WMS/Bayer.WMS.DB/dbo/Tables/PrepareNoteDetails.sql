﻿CREATE TABLE [dbo].[PrepareNoteDetails] (
    [DOCode]               NVARCHAR (30)   NOT NULL,
    [PrepareCode]          NVARCHAR (30)   NOT NULL,
    [DeliveryCode]         NVARCHAR (30)   NOT NULL,
    [ProductID]            INT             NOT NULL,
    [BatchCode]            NVARCHAR (30)   NOT NULL,
    [BatchCodeDistributor] NVARCHAR (255)  NULL,
    [Quantity]             DECIMAL (18, 2) NULL,
    [PackQuantity]         DECIMAL (18, 2) NULL,
    [PackType]             NVARCHAR (30)   NULL,
    [PackSize]             DECIMAL (18, 2) NULL,
    [PackWeight]           DECIMAL (18, 2) NULL,
    [ProductQuantity]      DECIMAL (18, 2) NULL,
    [ProductCase]          DECIMAL (18, 2) NULL,
    [ProductUnit]          NVARCHAR (50)   NULL,
    [ProductWeight]        NCHAR (10)      NULL,
    [UserPrepare]          INT             NULL,
    [UserVerify]           NVARCHAR (255)  NULL,
    [PreparedQty]          DECIMAL (18, 2) NULL,
    [DeliveredQty]         DECIMAL (18, 2) NULL,
    [RequireReturnQty]     DECIMAL (18, 2) NULL,
    [ReturnedQty]          DECIMAL (18, 2) NULL,
    [Status]               CHAR (1)        NULL,
    [CreatedBy]            INT             NULL,
    [CreatedBySitemapID]   INT             NULL,
    [CreatedDateTime]      DATETIME        NULL,
    [UpdatedBy]            INT             NULL,
    [UpdatedBySitemapID]   INT             NULL,
    [UpdatedDateTime]      DATETIME        NULL,
    [RowVersion]           ROWVERSION      NULL,
    [LastQuantity]         DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_PrepareNoteDetails] PRIMARY KEY CLUSTERED ([DOCode] ASC, [PrepareCode] ASC, [DeliveryCode] ASC, [ProductID] ASC, [BatchCode] ASC)
);











