﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnSelectQuantityByPackagingType]
(
	@PalletCode NVARCHAR(50)
	, @PackingType NVARCHAR(5)
)
RETURNS NVARCHAR(50)
AS
BEGIN
	RETURN CASE 
			WHEN @PackingType = 'C' THEN 
					(
						SELECT TOP 1
							OddQuantity = CASE WHEN ISNULL(tmpC.CartonQuantity,0) = 0 AND ISNULL(tmp.OddQuantity, 0) = 0
												THEN ''
											   WHEN ISNULL(tmp.OddQuantity, 0) = 0
												THEN CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
													+ 'T'
												ELSE 
													CONVERT(NVARCHAR(20), ISNULL(tmpC.CartonQuantity,0)) 
													+ 'T / ' + CONVERT(NVARCHAR(20),ISNULL(tmp.OddQuantity, 0))
												END
						FROM
							Pallets P WITH (NOLOCK)
							LEFT JOIN 
							(
								SELECT
									ps.PalletCode
									, OddQuantity = COUNT(DISTINCT ProductBarcode)
								FROM
									dbo.PalletStatuses ps WITH (NOLOCK)
									JOIN Products P ON ps.ProductID = P.ProductID
								WHERE
									ps.PalletCode = @PalletCode
									AND P.PackingType = @PackingType
									AND (ps.CartonBarcode IS NULL OR CartonBarcode = '')
									AND (ps.ProductBarcode IS NOT NULL AND ProductBarcode != '')
								GROUP BY
									ps.PalletCode
							) tmp
								ON P.PalletCode = tmp.PalletCode
							LEFT JOIN 
							(
								SELECT
									ps.PalletCode
									, CartonQuantity = COUNT(DISTINCT ps.CartonBarcode)
								FROM
									dbo.PalletStatuses ps WITH (NOLOCK)
									JOIN Products P ON ps.ProductID = P.ProductID
								WHERE
									ps.PalletCode = @PalletCode
									AND P.PackingType = @PackingType
									AND CartonBarcode IS NOT NULL 
									AND CartonBarcode != ''
								GROUP BY
									ps.PalletCode
							) tmpC
								ON P.PalletCode = tmpC.PalletCode
							WHERE
								P.PalletCode = @PalletCode
					)
			ELSE (
					SELECT TOP 1
						OddQuantity = CONVERT(NVARCHAR(20), ISNULL(COUNT(DISTINCT ps.ProductBarcode),0)) 
											+ CASE WHEN @PackingType = 'S' THEN ' X'
													WHEN @PackingType = 'B' THEN ' B'
													WHEN @PackingType = 'A' THEN ' C'
													ELSE '' END
					FROM
						dbo.PalletStatuses ps WITH (NOLOCK)
						JOIN Products P ON ps.ProductID = P.ProductID
					WHERE
						ps.PalletCode = @PalletCode
						AND P.PackingType = @PackingType
						AND (ps.CartonBarcode IS NULL OR CartonBarcode = '')
				) 
	END
END