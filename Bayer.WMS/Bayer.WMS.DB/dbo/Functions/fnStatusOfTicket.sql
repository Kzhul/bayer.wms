﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION dbo.fnStatusOfTicket
(
	@Status nvarchar(50)
)
RETURNS nvarchar(250)
AS
BEGIN
	SET @Status = LTRIM(RTRIM(@Status))
	RETURN CASE 
		WHEN @Status = 'E' THEN  N'Chưa tạo'
		WHEN @Status = 'P' THEN  N'Đã tạo 1 phần'
		WHEN @Status = 'N' THEN  N'Đã tạo hết'
		WHEN @Status = 'I' THEN  N'Đang thực hiện'
		WHEN @Status = 'C' THEN  N'Đã thực hiện xong'
	ELSE N'' END

END