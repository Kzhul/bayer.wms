﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnPalletType]
(
	@Type NVARCHAR(5)
)
RETURNS NVARCHAR(100)
AS
BEGIN
	RETURN CASE WHEN @Type = 'PW' THEN N'Gỗ'
						WHEN @Type = 'PP' THEN N'Nhựa phẳng'
						WHEN @Type = 'PF' THEN N'Nhựa lỗ'
						WHEN @Type = 'PA' THEN N'Nhôm'
						ELSE '' END
END