﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnPackagingType]
(
	@ProductID INT
)
RETURNS NVARCHAR(1)
AS
BEGIN
	RETURN (SELECT TOP 1 [PackingType] FROM Products WHERE ProductID = @ProductID)
END