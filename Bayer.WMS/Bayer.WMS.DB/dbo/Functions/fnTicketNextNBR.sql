﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnTicketNextNBR]
(
	@Type nvarchar(50)
)
RETURNS NVARCHAR(50)
AS
BEGIN
	DECLARE @Date DATE = GETDATE()
	DECLARE @Code NVARCHAR(20) = ''
	SET @Code = CASE WHEN @Type = 'XH' THEN
										(SELECT MAX(ExportCode) FROM [DeliveryNoteHeaders] WHERE DATEDIFF(dd,CreatedDateTime, @Date) = 0)
					WHEN @Type = 'CH' THEN
										(SELECT MAX(SplitCode) FROM [SplitNoteHeaders] WHERE DATEDIFF(dd,CreatedDateTime, @Date) = 0)
					WHEN @Type = 'SH' THEN
										(SELECT MAX(PrepareCode) FROM [PrepareNoteHeaders] WHERE DATEDIFF(dd,CreatedDateTime, @Date) = 0)
					WHEN @Type = 'GH' THEN
										(SELECT MAX(DeliveryTicketCode) FROM [DeliveryTicketHeaders] WHERE DATEDIFF(dd,CreatedDateTime, @Date) = 0)
					ELSE '' END

	SET @Code = CASE WHEN @Code != '' THEN @Code ELSE @Type + FORMAT(@Date, 'yyMMdd') + '00' END
	SET @Code = @Type + FORMAT(@Date, 'yyMMdd') + FORMAT(CONVERT(INT, RIGHT(@Code,2)) + 1, '00')
	--PRINT(@Code)
	RETURN @Code
END