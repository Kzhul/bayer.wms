﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnPackagingQuantity]
(
	@ProductID INT
	,@Quantity DECIMAL(18,2)
	,@IsCartonPackaging BIT
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @PakagingQuantity DECIMAL(18,2) = ISNULL( 
													(SELECT TOP 1 Quantity FROM ProductPackings WHERE [Type] != 'P' AND ProductID = @ProductID ) 
													,0.00
													)
	IF(@IsCartonPackaging = 1)
		RETURN CASE WHEN @PakagingQuantity != 0 THEN CONVERT(INT, @Quantity / @PakagingQuantity) ELSE 0 END
	ELSE
		RETURN CASE WHEN @PakagingQuantity != 0 THEN CONVERT(INT, @Quantity % @PakagingQuantity) ELSE 0 END

	RETURN 0
END