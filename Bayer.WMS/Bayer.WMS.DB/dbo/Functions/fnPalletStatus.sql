﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnPalletStatus]
(
	@Status NVARCHAR(5)
)
RETURNS NVARCHAR(100)
AS
BEGIN
	RETURN CASE WHEN             
							 @Status = 'N' THEN N'Mới'
						WHEN @Status = 'B' THEN N'Chờ xe nâng chất hàng'
						WHEN @Status = 'D' THEN N'Chờ xe nâng rớt hàng'
						WHEN @Status = 'A' THEN N'Đang giữ'--Thủ kho đang giữ để san hàng, chất hàng chờ xuất
						WHEN @Status = 'X' THEN N'Chờ nhận hàng'--Thủ kho đã xác nhận xuất
						WHEN @Status = 'T' THEN N'Trả lại cho kho'
						WHEN @Status = 'R' THEN N'Đã nhận hàng'
						WHEN @Status = 'S' THEN N'Đang Chia'
						WHEN @Status = 'U' THEN N'Đã Chia'
						WHEN @Status = 'P' THEN N'Đang Soạn'
						WHEN @Status = 'V' THEN N'Đã Soạn'
						ELSE '' END

--'A' 'Đang giữ'
--'B' 'Chờ xe nâng chất hàng'
--'D' 'Chờ xe nâng rớt hàng'
--'N' 'Mới'
--'P' 'Đang Soạn'
--'R' 'Đã nhận hàng'
--'S' 'Đang Chia'
--'T' 'Trả lại cho kho'
--'U' 'Đã Chia'
--'V' 'Đã Soạn'
--'X' 'Chờ nhận hàng'

END