﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Bayer.WMS.Launcher
{
    public partial class ConnectionView : Form
    {
        private string _pwd;

        public ConnectionView()
        {
            InitializeComponent();
        }

        private void ConnectionView_Load(object sender, EventArgs e)
        {
            ExeConfigurationFileMap configFile = new ExeConfigurationFileMap { ExeConfigFilename = "Bayer.WMS.exe.config" };
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFile, ConfigurationUserLevel.None);

            string[] connStrComponent = config.ConnectionStrings.ConnectionStrings["BayerWMSContext"].ConnectionString.Split(';');

            string dataSource = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("data source"));
            string initialCatalog = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("initial catalog"));
            string[] splitUserID = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("user id")).Split('=');
            string[] splitPwd = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("pwd")).Split('=');

            string userID = connStrComponent.FirstOrDefault(p => p.ToLower().StartsWith("user id"));

            _pwd = splitPwd[1].Trim() + (splitPwd.Length == 3 ? "=" : splitPwd.Length == 4 ? "==" : String.Empty);

            if (!String.IsNullOrEmpty(dataSource))
                txtDataSource.Text = dataSource.Split('=')[1];
            if (!String.IsNullOrEmpty(initialCatalog))
                txtInitCatalog.Text = initialCatalog.Split('=')[1];
            if (!String.IsNullOrEmpty(userID))
                txtUserID.Text = Program.AESDecrypt(splitUserID[1].Trim() + (splitUserID.Length == 3 ? "=" : splitUserID.Length == 4 ? "==" : String.Empty));
            txtPassword.Text = "********";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string configPath = Path.Combine(Application.StartupPath, "Bayer.WMS.exe.config");

            XmlDocument doc = new XmlDocument();
            doc.Load(configPath);

            XmlNode node = doc.SelectSingleNode("//connectionStrings");

            if (node == null)
                throw new Exception("Không tìm được connectionStrings trong file config.");

            string userid = Program.AESEncrypt(txtUserID.Text);
            string pwd;
            if (txtPassword.Text == "********")
                pwd = _pwd;
            else
                pwd = Program.AESEncrypt(txtPassword.Text);

            string connStr = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Pwd={3};MultipleActiveResultSets=True;Persist Security Info=True;",
                    txtDataSource.Text, txtInitCatalog.Text, userid, pwd);

            XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@name='{0}']", "BayerWMSContext"));
            if (elem != null)
                elem.SetAttribute("connectionString", connStr);

            doc.Save(configPath);
            ConfigurationManager.RefreshSection("connectionStrings");

            DialogResult = DialogResult.OK;
        }
    }
}
