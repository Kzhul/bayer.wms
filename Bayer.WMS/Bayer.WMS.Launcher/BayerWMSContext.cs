namespace Bayer.WMS.Launcher
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BayerWMSContext : DbContext
    {
        public BayerWMSContext()
            : base(Program.ConnStr)
        {
        }

        public virtual DbSet<AppSetting> AppSettings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppSetting>()
                .Property(e => e.SettingID)
                .IsUnicode(false);

            modelBuilder.Entity<AppSetting>()
                .Property(e => e.SettingValue)
                .IsUnicode(false);
        }
    }
}
