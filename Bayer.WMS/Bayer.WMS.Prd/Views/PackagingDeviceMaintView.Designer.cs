﻿namespace Bayer.WMS.Prd.Views
{
    partial class PackagingDeviceMaintView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbDeviceCode = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStatus_ReadOnly = new System.Windows.Forms.TextBox();
            this.clbPalletType = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(545, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 16);
            this.label10.TabIndex = 48;
            this.label10.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(545, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 16);
            this.label7.TabIndex = 47;
            this.label7.Text = "*";
            // 
            // cmbDeviceCode
            // 
            this.cmbDeviceCode.Columns = null;
            this.cmbDeviceCode.DropDownHeight = 1;
            this.cmbDeviceCode.DropDownWidth = 500;
            this.cmbDeviceCode.FormattingEnabled = true;
            this.cmbDeviceCode.IntegralHeight = false;
            this.cmbDeviceCode.Location = new System.Drawing.Point(136, 12);
            this.cmbDeviceCode.MaxLength = 255;
            this.cmbDeviceCode.Name = "cmbDeviceCode";
            this.cmbDeviceCode.PageSize = 0;
            this.cmbDeviceCode.PresenterInfo = null;
            this.cmbDeviceCode.Size = new System.Drawing.Size(400, 24);
            this.cmbDeviceCode.Source = null;
            this.cmbDeviceCode.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 73);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 45;
            this.label5.Text = "Trạng thái:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(136, 42);
            this.txtDescription.MaxLength = 255;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(400, 22);
            this.txtDescription.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 42;
            this.label2.Text = "Mô tả:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 16);
            this.label1.TabIndex = 41;
            this.label1.Text = "Mã phòng/thiết bị:";
            // 
            // txtStatus_ReadOnly
            // 
            this.txtStatus_ReadOnly.Location = new System.Drawing.Point(136, 70);
            this.txtStatus_ReadOnly.MaxLength = 255;
            this.txtStatus_ReadOnly.Multiline = true;
            this.txtStatus_ReadOnly.Name = "txtStatus_ReadOnly";
            this.txtStatus_ReadOnly.ReadOnly = true;
            this.txtStatus_ReadOnly.Size = new System.Drawing.Size(200, 24);
            this.txtStatus_ReadOnly.TabIndex = 53;
            // 
            // clbPalletType
            // 
            this.clbPalletType.CheckOnClick = true;
            this.clbPalletType.FormattingEnabled = true;
            this.clbPalletType.Location = new System.Drawing.Point(136, 100);
            this.clbPalletType.Name = "clbPalletType";
            this.clbPalletType.Size = new System.Drawing.Size(200, 89);
            this.clbPalletType.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 16);
            this.label3.TabIndex = 55;
            this.label3.Text = "Loại pallet s.dụng:";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(136, 70);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(200, 24);
            this.cmbStatus.TabIndex = 2;
            this.cmbStatus.SelectedIndexChanged += new System.EventHandler(this.cmbStatus_SelectedIndexChanged);
            // 
            // PackagingDeviceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 617);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.txtStatus_ReadOnly);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.clbPalletType);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbDeviceCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "PackagingDeviceView";
            this.Text = "PackagingDeviceView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private CustomControls.MultiColumnComboBox cmbDeviceCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStatus_ReadOnly;
        private System.Windows.Forms.CheckedListBox clbPalletType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbStatus;
    }
}