﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class PackagingDestroyForceEndView : BaseForm
    {
        private int _userID;

        public PackagingDestroyForceEndView()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(txtUsername.Text))
                    throw new WrappedException(Messages.Error_UsernameIsEmpty);
                if (String.IsNullOrWhiteSpace(txtPassword.Text))
                    throw new WrappedException(Messages.Error_PasswordIsEmpty);
                if (String.IsNullOrWhiteSpace(txtNote.Text))
                    throw new WrappedException(Messages.Error_NoteIsEmpty);

                string encryptedPwd = Utility.MD5Encrypt(txtPassword.Text);

                BayerWMSContext db = new BayerWMSContext();

                var user = db.Users.FirstOrDefault(p => p.Username == txtUsername.Text && p.Password == encryptedPwd && p.Status == User.status.A);
                if (user == null)
                    throw new WrappedException(Messages.Error_Login);

                _userID = user.UserID;

                DialogResult = DialogResult.OK;
            }
            catch (WrappedException ex)
            {
                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public int UserID
        {
            get { return _userID; }
        }

        public string Note
        {
            get { return txtNote.Text; }
        }
    }
}
