﻿namespace Bayer.WMS.Prd.Views
{
    partial class PackagingQuantityCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtgCheck = new System.Windows.Forms.DataGridView();
            this.colCartonBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagedQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._btnOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCheck)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgCheck
            // 
            this.dtgCheck.AllowUserToAddRows = false;
            this.dtgCheck.AllowUserToDeleteRows = false;
            this.dtgCheck.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgCheck.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgCheck.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgCheck.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCheck.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCartonBarcode,
            this.colPackagedQty});
            this.dtgCheck.GridColor = System.Drawing.SystemColors.Control;
            this.dtgCheck.Location = new System.Drawing.Point(12, 21);
            this.dtgCheck.Name = "dtgCheck";
            this.dtgCheck.ReadOnly = true;
            this.dtgCheck.RowTemplate.Height = 35;
            this.dtgCheck.Size = new System.Drawing.Size(510, 203);
            this.dtgCheck.TabIndex = 12;
            // 
            // colCartonBarcode
            // 
            this.colCartonBarcode.DataPropertyName = "CartonBarcode";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colCartonBarcode.DefaultCellStyle = dataGridViewCellStyle2;
            this.colCartonBarcode.HeaderText = "Mã thùng";
            this.colCartonBarcode.Name = "colCartonBarcode";
            this.colCartonBarcode.ReadOnly = true;
            this.colCartonBarcode.Width = 300;
            // 
            // colPackagedQty
            // 
            this.colPackagedQty.DataPropertyName = "PackagedQty";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colPackagedQty.DefaultCellStyle = dataGridViewCellStyle3;
            this.colPackagedQty.HeaderText = "Số lượng";
            this.colPackagedQty.Name = "colPackagedQty";
            this.colPackagedQty.ReadOnly = true;
            this.colPackagedQty.Width = 150;
            // 
            // _btnOK
            // 
            this._btnOK.BackColor = System.Drawing.Color.Green;
            this._btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnOK.ForeColor = System.Drawing.Color.White;
            this._btnOK.Location = new System.Drawing.Point(217, 230);
            this._btnOK.Name = "_btnOK";
            this._btnOK.Size = new System.Drawing.Size(100, 40);
            this._btnOK.TabIndex = 13;
            this._btnOK.Text = "OK";
            this._btnOK.UseVisualStyleBackColor = false;
            this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
            // 
            // PackagingQuantityCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 277);
            this.Controls.Add(this._btnOK);
            this.Controls.Add(this.dtgCheck);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PackagingQuantityCheck";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lý do hủy";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.dtgCheck)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagedQty;
        private System.Windows.Forms.Button _btnOK;
    }
}