﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class ProductionPlanImportDescrView : BaseForm
    {
        public ProductionPlanImportDescrView()
        {
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(txtNote.Text))
                DialogResult = DialogResult.OK;
            else
                MessageBox.Show(Messages.Error_NoteIsEmpty, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public string Note
        {
            get { return txtNote.Text; }
        }
    }
}
