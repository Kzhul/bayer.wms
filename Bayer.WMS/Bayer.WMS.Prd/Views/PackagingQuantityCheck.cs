﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class PackagingQuantityCheck : BaseForm
    {
        public PackagingQuantityCheck()
        {
            InitializeComponent();
        }

        public void Show(Control parent)
        {
            Top = (parent.Height - Height) / 2;
            Left = (parent.Width - Width) / 2 + parent.Left;

            if (parent != null)
                Name = $"{parent.Name}_PackagingQuantityCheck";

            Show();
            BringToFront();
        }

        private void _btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        public DataTable Data { set => dtgCheck.DataSource = value; }
    }
}
