﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class ProductionPlanUpdateView : BaseForm
    {
        public ProductionPlanUpdateView()
        {
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            try
            {
                if (dtpPlanDate.Value < DateTime.Today)
                    throw new WrappedException(Messages.Error_PlanDateMustGreaterThanToDate);
                if (cmbDevice.SelectedValue == null)
                    throw new WrappedException(Messages.Validate_Required_Device);
                if (String.IsNullOrWhiteSpace(txtPONumber.Text))
                    throw new WrappedException(Messages.Validate_Required_PONumber);
                if (String.IsNullOrWhiteSpace(txtNote.Text))
                    throw new WrappedException(Messages.Error_NoteIsEmpty);

                DialogResult = DialogResult.OK;
            }
            catch (WrappedException ex)
            {
                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public IList<Device> Devices
        {
            set
            {
                cmbDevice.ValueMember = "DeviceCode";
                cmbDevice.DisplayMember = "DeviceCode";
                cmbDevice.DataSource = value;
            }
        }

        public DateTime? PlanDate
        {
            get { return dtpPlanDate.Value; }
            set
            {
                dtpPlanDate.Value = value.Value;
                txtPlanDate.Text = $"{value:dd.MM.yyyy}";
            }
        }

        public string ProductLot
        {
            set { txtProductLot.Text = value; }
        }

        public string Device
        {
            get { return cmbDevice.SelectedValue.ToString(); }
            set
            {
                cmbDevice.SelectedValue = value;
                txtDevice.Text = value;
            }
        }

        public string PONumber
        {
            get { return txtPONumber.Text; }
            set { txtPONumber.Text = value; }
        }

        public string SupplierLot
        {
            get { return txtSupplierLot.Text; }
            set { txtSupplierLot.Text = value; }
        }

        public string ProductCode
        {
            set { txtProductCode.Text = value; }
        }

        public string ProductDescription
        {
            set { txtProductDescription.Text = value; }
        }

        public string status
        {
            set
            {
                if (value != ProductionPlan.status.New && value != ProductionPlan.status.InProgress)
                {
                    txtPlanDate.BringToFront();
                    txtDevice.BringToFront();
                    txtPONumber.ReadOnly = true;
                    btnUpdate.Enabled = false;
                    btnUpdate.Click -= btnUpdate_Click;
                }
            }
        }

        public string Note
        {
            get { return txtNote.Text; }
        }
    }
}
