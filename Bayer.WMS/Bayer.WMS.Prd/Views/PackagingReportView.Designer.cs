﻿namespace Bayer.WMS.Prd.Views
{
    partial class PackagingReportView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PackagingReportView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bdsPackagingReport = new System.Windows.Forms.BindingSource(this.components);
            this.cmbProduct = new Bayer.WMS.CustomControls.MultiColumnComboBox(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.txtProductionLot = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtgPackagingReport = new System.Windows.Forms.DataGridView();
            this.colPackagingReport_ProductLot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingReport_PackagingLine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingReport_ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingReport_ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingReport_Status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colPackagingReport_User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingReport_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingReport_PackagingStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPackagingReport_PackagingEndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackagingReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPackagingReport)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbProduct
            // 
            this.cmbProduct.Columns = null;
            this.cmbProduct.DropDownHeight = 1;
            this.cmbProduct.DropDownWidth = 500;
            this.cmbProduct.FormattingEnabled = true;
            this.cmbProduct.IntegralHeight = false;
            this.cmbProduct.Location = new System.Drawing.Point(360, 40);
            this.cmbProduct.MaxLength = 255;
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.PageSize = 0;
            this.cmbProduct.PresenterInfo = null;
            this.cmbProduct.Size = new System.Drawing.Size(400, 24);
            this.cmbProduct.Source = null;
            this.cmbProduct.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(281, 43);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 51;
            this.label4.Text = "Sản phẩm:";
            // 
            // txtProductionLot
            // 
            this.txtProductionLot.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtProductionLot.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.txtProductionLot.Location = new System.Drawing.Point(64, 40);
            this.txtProductionLot.Name = "txtProductionLot";
            this.txtProductionLot.Size = new System.Drawing.Size(200, 22);
            this.txtProductionLot.TabIndex = 47;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 50;
            this.label2.Text = "Mã lô:";
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(766, 37);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(50, 30);
            this.btnSearch.TabIndex = 46;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(159, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 16);
            this.label1.TabIndex = 45;
            this.label1.Text = "-";
            // 
            // dtpToDate
            // 
            this.dtpToDate.CustomFormat = "dd.MM.yyyy";
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToDate.Location = new System.Drawing.Point(174, 12);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(90, 22);
            this.dtpToDate.TabIndex = 43;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.CustomFormat = "dd.MM.yyyy";
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(64, 12);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(90, 22);
            this.dtpFromDate.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 44;
            this.label3.Text = "Ngày:";
            // 
            // dtgPackagingReport
            // 
            this.dtgPackagingReport.AllowUserToAddRows = false;
            this.dtgPackagingReport.AllowUserToOrderColumns = true;
            this.dtgPackagingReport.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this.dtgPackagingReport.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dtgPackagingReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgPackagingReport.AutoGenerateColumns = false;
            this.dtgPackagingReport.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtgPackagingReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPackagingReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPackagingReport_ProductLot,
            this.colPackagingReport_PackagingLine,
            this.colPackagingReport_ProductCode,
            this.colPackagingReport_ProductDescription,
            this.colPackagingReport_Status,
            this.colPackagingReport_User,
            this.colPackagingReport_Quantity,
            this.colPackagingReport_PackagingStartTime,
            this.colPackagingReport_PackagingEndTime});
            this.dtgPackagingReport.DataSource = this.bdsPackagingReport;
            this.dtgPackagingReport.GridColor = System.Drawing.SystemColors.Control;
            this.dtgPackagingReport.Location = new System.Drawing.Point(12, 73);
            this.dtgPackagingReport.Name = "dtgPackagingReport";
            this.dtgPackagingReport.ReadOnly = true;
            this.dtgPackagingReport.Size = new System.Drawing.Size(844, 315);
            this.dtgPackagingReport.TabIndex = 41;
            // 
            // colPackagingReport_ProductLot
            // 
            this.colPackagingReport_ProductLot.DataPropertyName = "ProductLot";
            this.colPackagingReport_ProductLot.HeaderText = "Mã lô";
            this.colPackagingReport_ProductLot.Name = "colPackagingReport_ProductLot";
            this.colPackagingReport_ProductLot.ReadOnly = true;
            this.colPackagingReport_ProductLot.Width = 80;
            // 
            // colPackagingReport_PackagingLine
            // 
            this.colPackagingReport_PackagingLine.DataPropertyName = "Device";
            this.colPackagingReport_PackagingLine.HeaderText = "Line";
            this.colPackagingReport_PackagingLine.Name = "colPackagingReport_PackagingLine";
            this.colPackagingReport_PackagingLine.ReadOnly = true;
            this.colPackagingReport_PackagingLine.Width = 80;
            // 
            // colPackagingReport_ProductCode
            // 
            this.colPackagingReport_ProductCode.DataPropertyName = "ProductCode";
            this.colPackagingReport_ProductCode.HeaderText = "Mã sản phẩm";
            this.colPackagingReport_ProductCode.Name = "colPackagingReport_ProductCode";
            this.colPackagingReport_ProductCode.ReadOnly = true;
            // 
            // colPackagingReport_ProductDescription
            // 
            this.colPackagingReport_ProductDescription.DataPropertyName = "ProductDescription";
            this.colPackagingReport_ProductDescription.HeaderText = "Mô tả";
            this.colPackagingReport_ProductDescription.Name = "colPackagingReport_ProductDescription";
            this.colPackagingReport_ProductDescription.ReadOnly = true;
            this.colPackagingReport_ProductDescription.Width = 300;
            // 
            // colPackagingReport_Status
            // 
            this.colPackagingReport_Status.DataPropertyName = "Status";
            this.colPackagingReport_Status.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.colPackagingReport_Status.HeaderText = "Trạng thái";
            this.colPackagingReport_Status.Name = "colPackagingReport_Status";
            this.colPackagingReport_Status.ReadOnly = true;
            this.colPackagingReport_Status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colPackagingReport_Status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // colPackagingReport_User
            // 
            this.colPackagingReport_User.DataPropertyName = "Executor";
            this.colPackagingReport_User.HeaderText = "Người thực hiện";
            this.colPackagingReport_User.Name = "colPackagingReport_User";
            this.colPackagingReport_User.ReadOnly = true;
            this.colPackagingReport_User.Width = 150;
            // 
            // colPackagingReport_Quantity
            // 
            this.colPackagingReport_Quantity.DataPropertyName = "Quantity";
            this.colPackagingReport_Quantity.HeaderText = "Số lượng đóng gói";
            this.colPackagingReport_Quantity.Name = "colPackagingReport_Quantity";
            this.colPackagingReport_Quantity.ReadOnly = true;
            // 
            // colPackagingReport_PackagingStartTime
            // 
            this.colPackagingReport_PackagingStartTime.DataPropertyName = "PackagingStartTime";
            dataGridViewCellStyle2.Format = "dd.MM.yyyy - HH:mm";
            this.colPackagingReport_PackagingStartTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.colPackagingReport_PackagingStartTime.HeaderText = "Thời gian bắt đầu";
            this.colPackagingReport_PackagingStartTime.Name = "colPackagingReport_PackagingStartTime";
            this.colPackagingReport_PackagingStartTime.ReadOnly = true;
            this.colPackagingReport_PackagingStartTime.Width = 130;
            // 
            // colPackagingReport_PackagingEndTime
            // 
            this.colPackagingReport_PackagingEndTime.DataPropertyName = "PackagingEndTime";
            dataGridViewCellStyle3.Format = "dd.MM.yyyy - HH:mm";
            this.colPackagingReport_PackagingEndTime.DefaultCellStyle = dataGridViewCellStyle3;
            this.colPackagingReport_PackagingEndTime.HeaderText = "Thời gian kết thúc";
            this.colPackagingReport_PackagingEndTime.Name = "colPackagingReport_PackagingEndTime";
            this.colPackagingReport_PackagingEndTime.ReadOnly = true;
            this.colPackagingReport_PackagingEndTime.Width = 130;
            // 
            // PackagingReportView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 400);
            this.Controls.Add(this.cmbProduct);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtProductionLot);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtgPackagingReport);
            this.Name = "PackagingReportView";
            this.Text = "PackagingReportView";
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackagingReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPackagingReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgPackagingReport;
        private System.Windows.Forms.BindingSource bdsPackagingReport;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label label3;
        private CustomControls.MultiColumnComboBox cmbProduct;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtProductionLot;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingReport_ProductLot;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingReport_PackagingLine;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingReport_ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingReport_ProductDescription;
        private System.Windows.Forms.DataGridViewComboBoxColumn colPackagingReport_Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingReport_User;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingReport_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingReport_PackagingStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackagingReport_PackagingEndTime;
    }
}