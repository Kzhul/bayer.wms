﻿using Bayer.WMS.Base;
using Bayer.WMS.CustomControls;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Prd.Presenters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class PackagingDeviceMaintView : BaseForm, IDeviceView
    {
        public PackagingDeviceMaintView()
        {
            InitializeComponent();
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtStatus_ReadOnly.Text = cmbStatus.Text;
        }

        public override void InitializeComboBox()
        {
            cmbDeviceCode.PageSize = 20;
            cmbDeviceCode.ValueMember = "DeviceCode";
            cmbDeviceCode.DisplayMember = "DeviceCode";
            cmbDeviceCode.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("DeviceCode", "Mã thiết bị/phòng", 150),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
            cmbDeviceCode.PresenterInfo = new MultiColumnComboBox.ComboBoxPresenterInfo(_presenter, "LoadDevices", "OnDeviceCodeSelectChange");

            cmbStatus.ValueMember = Device.status.ValueMember;
            cmbStatus.DisplayMember = Device.status.DisplayMember;
            cmbStatus.DataSource = Device.status.Get();

            clbPalletType.ValueMember = Pallet.type.ValueMember;
            clbPalletType.DisplayMember = Pallet.type.DisplayMember;
            foreach (var item in Pallet.type.Get())
            {
                clbPalletType.Items.Add(item);
            }
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var devicePresenter = _presenter as IDeviceMaintPresenter;

                await Task.WhenAll(devicePresenter.LoadDevices());

                if (!isRefresh)
                    devicePresenter.Insert();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DataTable Devices
        {
            set { cmbDeviceCode.Source = value; }
        }

        private Device _device;

        public Device Device
        {
            get { return _device; }
            set
            {
                _device = value;
                PalletType = value.PalletType;

                cmbDeviceCode.DataBindings.Clear();
                txtDescription.DataBindings.Clear();
                cmbStatus.DataBindings.Clear();

                cmbDeviceCode.DataBindings.Add("Text", Device, "DeviceCode", true, DataSourceUpdateMode.OnPropertyChanged);
                txtDescription.DataBindings.Add("Text", Device, "Description", true, DataSourceUpdateMode.OnPropertyChanged);
                cmbStatus.DataBindings.Add("SelectedValue", Device, "Status", true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        public string PalletType
        {
            get
            {
                string[] palletType = new string[clbPalletType.CheckedItems.Count];
                for (int i = 0; i < clbPalletType.CheckedItems.Count; i++)
                {
                    var type = clbPalletType.CheckedItems[i] as Pallet.type;
                    palletType[i] = type.Value;
                }

                return String.Join(",", palletType);
            }
            set
            {
                string palletType = value ?? String.Empty;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        for (int i = 0; i < clbPalletType.Items.Count; i++)
                        {
                            var type = clbPalletType.Items[i] as Pallet.type;
                            clbPalletType.SetItemChecked(i, palletType.Contains(type.Value));
                        }
                    });
                }
                else
                {
                    for (int i = 0; i < clbPalletType.Items.Count; i++)
                    {
                        var type = clbPalletType.Items[i] as Pallet.type;
                        clbPalletType.SetItemChecked(i, palletType.Contains(type.Value));
                    }
                }
            }
        }

        public override void HandlePermission(string accessRight)
        {
            switch (accessRight)
            {
                case "R":
                    txtDescription.ReadOnly = true;
                    txtStatus_ReadOnly.BringToFront();
                    break;
                default:
                    break;
            }
        }
    }
}
