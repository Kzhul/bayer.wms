﻿namespace Bayer.WMS.Prd.Views
{
    partial class PackagingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bdsPackagingLog = new System.Windows.Forms.BindingSource(this.components);
            this.bdsPackagingError = new System.Windows.Forms.BindingSource(this.components);
            this.btnOK = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPallet = new System.Windows.Forms.TextBox();
            this.txtCarton = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCartonSampleQty = new System.Windows.Forms.TextBox();
            this.txtProductSampleQty = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCartonWeight = new System.Windows.Forms.TextBox();
            this.txtPackagingEndTime = new System.Windows.Forms.TextBox();
            this.txtPackagingLine = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPackagingStartTime = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtExecutor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblProductDescription = new System.Windows.Forms.Label();
            this.lblProductLot = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPallet_CartonQty = new System.Windows.Forms.Label();
            this.lblStepHints = new System.Windows.Forms.Label();
            this.lblPallet_PackingQty = new System.Windows.Forms.Label();
            this.lblPallet_PackingQty_Label = new System.Windows.Forms.Label();
            this.lblCarton_PackingQty_Label = new System.Windows.Forms.Label();
            this.lblPallet_CartonQty_Label = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblCarton_PackingQty = new System.Windows.Forms.Label();
            this.lblPackingQty = new System.Windows.Forms.Label();
            this.pnlPackaging = new System.Windows.Forms.Panel();
            this.lblUOMProductLot = new System.Windows.Forms.Label();
            this.lblUOMPallet = new System.Windows.Forms.Label();
            this.lblQuantityOnPallet = new System.Windows.Forms.Label();
            this.pnlDestroy = new System.Windows.Forms.Panel();
            this.lblDestroyLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblCancelLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackagingLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackagingError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.pnlPackaging.SuspendLayout();
            this.pnlDestroy.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(543, 12);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(87, 38);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Nhập";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label11);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.txtPallet);
            this.splitContainer1.Panel1.Controls.Add(this.txtCarton);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.txtCartonSampleQty);
            this.splitContainer1.Panel1.Controls.Add(this.txtProductSampleQty);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.txtCartonWeight);
            this.splitContainer1.Panel1.Controls.Add(this.txtPackagingEndTime);
            this.splitContainer1.Panel1.Controls.Add(this.txtPackagingLine);
            this.splitContainer1.Panel1.Controls.Add(this.label13);
            this.splitContainer1.Panel1.Controls.Add(this.txtPackagingStartTime);
            this.splitContainer1.Panel1.Controls.Add(this.label9);
            this.splitContainer1.Panel1.Controls.Add(this.txtExecutor);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lblProductDescription);
            this.splitContainer1.Panel2.Controls.Add(this.lblProductLot);
            this.splitContainer1.Panel2.Controls.Add(this.txtBarcode);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.btnOK);
            this.splitContainer1.Size = new System.Drawing.Size(1276, 185);
            this.splitContainer1.SplitterDistance = 630;
            this.splitContainer1.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(2, 155);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 24);
            this.label11.TabIndex = 27;
            this.label11.Text = "Mã Pallet:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(313, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 24);
            this.label4.TabIndex = 26;
            this.label4.Text = "Mã Thùng:";
            // 
            // txtPallet
            // 
            this.txtPallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPallet.Location = new System.Drawing.Point(131, 152);
            this.txtPallet.Name = "txtPallet";
            this.txtPallet.ReadOnly = true;
            this.txtPallet.Size = new System.Drawing.Size(180, 29);
            this.txtPallet.TabIndex = 25;
            this.txtPallet.TabStop = false;
            // 
            // txtCarton
            // 
            this.txtCarton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCarton.Location = new System.Drawing.Point(447, 152);
            this.txtCarton.Name = "txtCarton";
            this.txtCarton.ReadOnly = true;
            this.txtCarton.Size = new System.Drawing.Size(180, 29);
            this.txtCarton.TabIndex = 24;
            this.txtCarton.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(313, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 24);
            this.label3.TabIndex = 23;
            this.label3.Text = "SL Mẫu Thùng:";
            // 
            // txtCartonSampleQty
            // 
            this.txtCartonSampleQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCartonSampleQty.Location = new System.Drawing.Point(448, 117);
            this.txtCartonSampleQty.Name = "txtCartonSampleQty";
            this.txtCartonSampleQty.ReadOnly = true;
            this.txtCartonSampleQty.Size = new System.Drawing.Size(180, 29);
            this.txtCartonSampleQty.TabIndex = 22;
            this.txtCartonSampleQty.TabStop = false;
            // 
            // txtProductSampleQty
            // 
            this.txtProductSampleQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductSampleQty.Location = new System.Drawing.Point(131, 117);
            this.txtProductSampleQty.Name = "txtProductSampleQty";
            this.txtProductSampleQty.ReadOnly = true;
            this.txtProductSampleQty.Size = new System.Drawing.Size(180, 29);
            this.txtProductSampleQty.TabIndex = 21;
            this.txtProductSampleQty.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 24);
            this.label2.TabIndex = 20;
            this.label2.Text = "SL Mẫu TP:";
            // 
            // txtCartonWeight
            // 
            this.txtCartonWeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCartonWeight.Location = new System.Drawing.Point(131, 82);
            this.txtCartonWeight.Name = "txtCartonWeight";
            this.txtCartonWeight.ReadOnly = true;
            this.txtCartonWeight.Size = new System.Drawing.Size(180, 29);
            this.txtCartonWeight.TabIndex = 11;
            this.txtCartonWeight.TabStop = false;
            // 
            // txtPackagingEndTime
            // 
            this.txtPackagingEndTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPackagingEndTime.Location = new System.Drawing.Point(448, 47);
            this.txtPackagingEndTime.Name = "txtPackagingEndTime";
            this.txtPackagingEndTime.ReadOnly = true;
            this.txtPackagingEndTime.Size = new System.Drawing.Size(180, 29);
            this.txtPackagingEndTime.TabIndex = 6;
            this.txtPackagingEndTime.TabStop = false;
            // 
            // txtPackagingLine
            // 
            this.txtPackagingLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtPackagingLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPackagingLine.Location = new System.Drawing.Point(131, 12);
            this.txtPackagingLine.Name = "txtPackagingLine";
            this.txtPackagingLine.ReadOnly = true;
            this.txtPackagingLine.Size = new System.Drawing.Size(180, 29);
            this.txtPackagingLine.TabIndex = 4;
            this.txtPackagingLine.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(2, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 24);
            this.label13.TabIndex = 19;
            this.label13.Text = "K.lượng thùng:";
            // 
            // txtPackagingStartTime
            // 
            this.txtPackagingStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPackagingStartTime.Location = new System.Drawing.Point(131, 47);
            this.txtPackagingStartTime.Name = "txtPackagingStartTime";
            this.txtPackagingStartTime.ReadOnly = true;
            this.txtPackagingStartTime.Size = new System.Drawing.Size(180, 29);
            this.txtPackagingStartTime.TabIndex = 5;
            this.txtPackagingStartTime.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(313, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 24);
            this.label9.TabIndex = 15;
            this.label9.Text = "Kết thúc:";
            // 
            // txtExecutor
            // 
            this.txtExecutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExecutor.Location = new System.Drawing.Point(448, 12);
            this.txtExecutor.Name = "txtExecutor";
            this.txtExecutor.ReadOnly = true;
            this.txtExecutor.Size = new System.Drawing.Size(180, 29);
            this.txtExecutor.TabIndex = 3;
            this.txtExecutor.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 24);
            this.label5.TabIndex = 11;
            this.label5.Text = "Line đóng gói:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(313, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "Thực hiện:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(2, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 24);
            this.label8.TabIndex = 14;
            this.label8.Text = "Bắt đầu:";
            // 
            // lblProductDescription
            // 
            this.lblProductDescription.AutoSize = true;
            this.lblProductDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductDescription.Location = new System.Drawing.Point(124, 115);
            this.lblProductDescription.Name = "lblProductDescription";
            this.lblProductDescription.Size = new System.Drawing.Size(226, 31);
            this.lblProductDescription.TabIndex = 13;
            this.lblProductDescription.Text = "[Tên thành phẩm]";
            // 
            // lblProductLot
            // 
            this.lblProductLot.AutoSize = true;
            this.lblProductLot.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductLot.Location = new System.Drawing.Point(124, 63);
            this.lblProductLot.Name = "lblProductLot";
            this.lblProductLot.Size = new System.Drawing.Size(209, 31);
            this.lblProductLot.TabIndex = 12;
            this.lblProductLot.Text = "[Lô thành phẩm]";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.Location = new System.Drawing.Point(130, 12);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(407, 38);
            this.txtBarcode.TabIndex = 0;
            this.txtBarcode.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 31);
            this.label1.TabIndex = 10;
            this.label1.Text = "QR code";
            // 
            // lblPallet_CartonQty
            // 
            this.lblPallet_CartonQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPallet_CartonQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPallet_CartonQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPallet_CartonQty.ForeColor = System.Drawing.Color.Blue;
            this.lblPallet_CartonQty.Location = new System.Drawing.Point(238, 238);
            this.lblPallet_CartonQty.Name = "lblPallet_CartonQty";
            this.lblPallet_CartonQty.Size = new System.Drawing.Size(800, 150);
            this.lblPallet_CartonQty.TabIndex = 18;
            this.lblPallet_CartonQty.Text = "0 / 0";
            this.lblPallet_CartonQty.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblStepHints
            // 
            this.lblStepHints.BackColor = System.Drawing.Color.LimeGreen;
            this.lblStepHints.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblStepHints.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStepHints.ForeColor = System.Drawing.Color.White;
            this.lblStepHints.Location = new System.Drawing.Point(0, 775);
            this.lblStepHints.Name = "lblStepHints";
            this.lblStepHints.Size = new System.Drawing.Size(1276, 160);
            this.lblStepHints.TabIndex = 23;
            // 
            // lblPallet_PackingQty
            // 
            this.lblPallet_PackingQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPallet_PackingQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPallet_PackingQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPallet_PackingQty.ForeColor = System.Drawing.Color.Blue;
            this.lblPallet_PackingQty.Location = new System.Drawing.Point(238, 238);
            this.lblPallet_PackingQty.Name = "lblPallet_PackingQty";
            this.lblPallet_PackingQty.Size = new System.Drawing.Size(800, 150);
            this.lblPallet_PackingQty.TabIndex = 21;
            this.lblPallet_PackingQty.Text = "0 / 0";
            this.lblPallet_PackingQty.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblPallet_PackingQty.Visible = false;
            // 
            // lblPallet_PackingQty_Label
            // 
            this.lblPallet_PackingQty_Label.AutoSize = true;
            this.lblPallet_PackingQty_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPallet_PackingQty_Label.Location = new System.Drawing.Point(238, 201);
            this.lblPallet_PackingQty_Label.Name = "lblPallet_PackingQty_Label";
            this.lblPallet_PackingQty_Label.Size = new System.Drawing.Size(397, 37);
            this.lblPallet_PackingQty_Label.TabIndex = 20;
            this.lblPallet_PackingQty_Label.Text = "SL đã đóng gói trên Pallet:";
            this.lblPallet_PackingQty_Label.Visible = false;
            // 
            // lblCarton_PackingQty_Label
            // 
            this.lblCarton_PackingQty_Label.AutoSize = true;
            this.lblCarton_PackingQty_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarton_PackingQty_Label.Location = new System.Drawing.Point(238, 14);
            this.lblCarton_PackingQty_Label.Name = "lblCarton_PackingQty_Label";
            this.lblCarton_PackingQty_Label.Size = new System.Drawing.Size(283, 37);
            this.lblCarton_PackingQty_Label.TabIndex = 14;
            this.lblCarton_PackingQty_Label.Text = "SL đã đóng thùng:";
            // 
            // lblPallet_CartonQty_Label
            // 
            this.lblPallet_CartonQty_Label.AutoSize = true;
            this.lblPallet_CartonQty_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPallet_CartonQty_Label.Location = new System.Drawing.Point(238, 201);
            this.lblPallet_CartonQty_Label.Name = "lblPallet_CartonQty_Label";
            this.lblPallet_CartonQty_Label.Size = new System.Drawing.Size(304, 37);
            this.lblPallet_CartonQty_Label.TabIndex = 15;
            this.lblPallet_CartonQty_Label.Text = "SL thùng trên pallet:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(238, 388);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(245, 37);
            this.label16.TabIndex = 16;
            this.label16.Text = "SL đã đóng gói:";
            // 
            // lblCarton_PackingQty
            // 
            this.lblCarton_PackingQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCarton_PackingQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblCarton_PackingQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarton_PackingQty.ForeColor = System.Drawing.Color.Blue;
            this.lblCarton_PackingQty.Location = new System.Drawing.Point(238, 51);
            this.lblCarton_PackingQty.Name = "lblCarton_PackingQty";
            this.lblCarton_PackingQty.Size = new System.Drawing.Size(800, 150);
            this.lblCarton_PackingQty.TabIndex = 17;
            this.lblCarton_PackingQty.Text = "0 / 0";
            this.lblCarton_PackingQty.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPackingQty
            // 
            this.lblPackingQty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPackingQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPackingQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPackingQty.ForeColor = System.Drawing.Color.Blue;
            this.lblPackingQty.Location = new System.Drawing.Point(238, 425);
            this.lblPackingQty.Name = "lblPackingQty";
            this.lblPackingQty.Size = new System.Drawing.Size(800, 150);
            this.lblPackingQty.TabIndex = 19;
            this.lblPackingQty.Text = "0 / 0";
            this.lblPackingQty.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pnlPackaging
            // 
            this.pnlPackaging.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPackaging.Controls.Add(this.lblUOMProductLot);
            this.pnlPackaging.Controls.Add(this.lblUOMPallet);
            this.pnlPackaging.Controls.Add(this.lblCarton_PackingQty_Label);
            this.pnlPackaging.Controls.Add(this.lblCarton_PackingQty);
            this.pnlPackaging.Controls.Add(this.lblPallet_PackingQty);
            this.pnlPackaging.Controls.Add(this.lblPallet_CartonQty);
            this.pnlPackaging.Controls.Add(this.lblPackingQty);
            this.pnlPackaging.Controls.Add(this.lblPallet_PackingQty_Label);
            this.pnlPackaging.Controls.Add(this.lblPallet_CartonQty_Label);
            this.pnlPackaging.Controls.Add(this.label16);
            this.pnlPackaging.Controls.Add(this.lblQuantityOnPallet);
            this.pnlPackaging.Location = new System.Drawing.Point(0, 191);
            this.pnlPackaging.Name = "pnlPackaging";
            this.pnlPackaging.Size = new System.Drawing.Size(1276, 581);
            this.pnlPackaging.TabIndex = 24;
            // 
            // lblUOMProductLot
            // 
            this.lblUOMProductLot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUOMProductLot.AutoSize = true;
            this.lblUOMProductLot.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUOMProductLot.Location = new System.Drawing.Point(1037, 547);
            this.lblUOMProductLot.Name = "lblUOMProductLot";
            this.lblUOMProductLot.Size = new System.Drawing.Size(93, 31);
            this.lblUOMProductLot.TabIndex = 23;
            this.lblUOMProductLot.Text = "[UOM]";
            this.lblUOMProductLot.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblUOMPallet
            // 
            this.lblUOMPallet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUOMPallet.AutoSize = true;
            this.lblUOMPallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUOMPallet.Location = new System.Drawing.Point(1035, 357);
            this.lblUOMPallet.Name = "lblUOMPallet";
            this.lblUOMPallet.Size = new System.Drawing.Size(93, 31);
            this.lblUOMPallet.TabIndex = 14;
            this.lblUOMPallet.Text = "[UOM]";
            this.lblUOMPallet.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblQuantityOnPallet
            // 
            this.lblQuantityOnPallet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQuantityOnPallet.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantityOnPallet.Location = new System.Drawing.Point(693, 388);
            this.lblQuantityOnPallet.Name = "lblQuantityOnPallet";
            this.lblQuantityOnPallet.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblQuantityOnPallet.Size = new System.Drawing.Size(345, 75);
            this.lblQuantityOnPallet.TabIndex = 22;
            this.lblQuantityOnPallet.Text = "            ";
            // 
            // pnlDestroy
            // 
            this.pnlDestroy.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlDestroy.Controls.Add(this.lblDestroyLabel);
            this.pnlDestroy.Controls.Add(this.label10);
            this.pnlDestroy.Controls.Add(this.label7);
            this.pnlDestroy.Controls.Add(this.lblCancelLabel);
            this.pnlDestroy.Location = new System.Drawing.Point(0, 191);
            this.pnlDestroy.Name = "pnlDestroy";
            this.pnlDestroy.Size = new System.Drawing.Size(1276, 581);
            this.pnlDestroy.TabIndex = 22;
            // 
            // lblDestroyLabel
            // 
            this.lblDestroyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDestroyLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblDestroyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDestroyLabel.ForeColor = System.Drawing.Color.Blue;
            this.lblDestroyLabel.Location = new System.Drawing.Point(238, 347);
            this.lblDestroyLabel.Name = "lblDestroyLabel";
            this.lblDestroyLabel.Size = new System.Drawing.Size(800, 150);
            this.lblDestroyLabel.TabIndex = 21;
            this.lblDestroyLabel.Text = "0";
            this.lblDestroyLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(238, 310);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(118, 37);
            this.label10.TabIndex = 20;
            this.label10.Text = "Hủy dư";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(238, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(159, 37);
            this.label7.TabIndex = 19;
            this.label7.Text = "Hủy sự cố";
            // 
            // lblCancelLabel
            // 
            this.lblCancelLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCancelLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblCancelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCancelLabel.ForeColor = System.Drawing.Color.Blue;
            this.lblCancelLabel.Location = new System.Drawing.Point(238, 89);
            this.lblCancelLabel.Name = "lblCancelLabel";
            this.lblCancelLabel.Size = new System.Drawing.Size(800, 150);
            this.lblCancelLabel.TabIndex = 18;
            this.lblCancelLabel.Text = "0";
            this.lblCancelLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PackagingView
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 935);
            this.Controls.Add(this.lblStepHints);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pnlPackaging);
            this.Controls.Add(this.pnlDestroy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "PackagingView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ĐÓNG GÓI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PackagingView_FormClosing);
            this.Load += new System.EventHandler(this.PackagingView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackagingLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPackagingError)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.pnlPackaging.ResumeLayout(false);
            this.pnlPackaging.PerformLayout();
            this.pnlDestroy.ResumeLayout(false);
            this.pnlDestroy.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource bdsPackagingLog;
        private System.Windows.Forms.BindingSource bdsPackagingError;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblProductDescription;
        private System.Windows.Forms.Label lblProductLot;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPallet_CartonQty;
        private System.Windows.Forms.Label lblStepHints;
        private System.Windows.Forms.Label lblPallet_PackingQty;
        private System.Windows.Forms.Label lblPallet_PackingQty_Label;
        private System.Windows.Forms.Label lblCarton_PackingQty_Label;
        private System.Windows.Forms.Label lblPallet_CartonQty_Label;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblCarton_PackingQty;
        private System.Windows.Forms.Label lblPackingQty;
        private System.Windows.Forms.TextBox txtCartonWeight;
        private System.Windows.Forms.TextBox txtPackagingEndTime;
        private System.Windows.Forms.TextBox txtPackagingLine;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPackagingStartTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtExecutor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtProductSampleQty;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCartonSampleQty;
        private System.Windows.Forms.Panel pnlPackaging;
        private System.Windows.Forms.Panel pnlDestroy;
        private System.Windows.Forms.Label lblDestroyLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblCancelLabel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPallet;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Label lblQuantityOnPallet;
        private System.Windows.Forms.Label lblUOMPallet;
        private System.Windows.Forms.Label lblUOMProductLot;
    }
}