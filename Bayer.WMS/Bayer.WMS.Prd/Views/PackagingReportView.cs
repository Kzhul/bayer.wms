﻿using Bayer.WMS.Base;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Prd.Presenters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bayer.WMS.Prd.Views
{
    public partial class PackagingReportView : BaseForm, IPackagingReportView
    {
        public PackagingReportView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgPackagingReport);
        }

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            try
            {
                await _presenter.LoadMainData();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnSearch.Enabled = true;
            }
        }

        public override void InitializeComboBox()
        {
            colPackagingReport_Status.DataSource = ProductionPlan.status.Get();
            colPackagingReport_Status.ValueMember = ProductionPlan.status.ValueMember;
            colPackagingReport_Status.DisplayMember = ProductionPlan.status.DisplayMember;
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var packagingTraceAndTracePresenter = _presenter as IPackagingReportPresenter;

                await packagingTraceAndTracePresenter.LoadProducts();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DateTime FromDate
        {
            get { return dtpFromDate.Value.Date; }
        }

        public DateTime ToDate
        {
            get { return dtpToDate.Value.Date; }
        }

        public string ProductLot
        {
            get { return txtProductionLot.Text; }
        }

        public int ProductID
        {
            get
            {
                if (cmbProduct.SelectedItem == null)
                    return 0;
                else
                    return int.Parse($"{cmbProduct.SelectedItem["ProductID"]}");
            }
        }

        public DataTable Products
        {
            set { cmbProduct.Source = value; }
        }

        public DataTable Reports
        {
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsPackagingReport.DataSource = value;
                    });
                }
                else
                {
                    bdsPackagingReport.DataSource = value;
                }
            }
        }
    }
}
