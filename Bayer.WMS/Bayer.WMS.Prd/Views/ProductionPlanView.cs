﻿using Bayer.WMS.Base;
using Bayer.WMS.Prd.Presenters;
using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bayer.WMS.CustomControls;
using System.Reflection;
using System.IO.Ports;
using System.Xml;
using System.Configuration;

namespace Bayer.WMS.Prd.Views
{
    public partial class ProductionPlanView : BaseForm, IProductionPlanView
    {
        private int _rowIndex;

        public ProductionPlanView()
        {
            InitializeComponent();
            SetDoubleBuffered(dtgProductionPlan);

            dtpFromDate.Value = DateTime.Today.GetFirstDayOfMonth();
            dtpToDate.Value = DateTime.Today.GetLastDayOfMonth();

            dtgProductionPlan.Columns[colProductionPlan_IsGenProductBarcode.Index].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dtgProductionPlan.Columns[colProductionPlan_IsGenCartonBarcode.Index].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dtgProductionPlan.Columns[colProductionPlan_Quantity.Index].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dtgProductionPlan.Columns[colProductionPlan_PackageSize.Index].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dtgProductionPlan.Columns[colProductionPlan_PackageQuantity.Index].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            toolTip1.SetToolTip(btnUpload, "Import KHSX");
            toolTip1.SetToolTip(btnExportExcel, "Export mã QR Thùng/sản phẩm");
        }

        private async void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            try
            {
                await _presenter.LoadMainData();
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnSearch.Enabled = true;
            }
        }

        private async void btnUpload_Click(object sender, EventArgs e)
        {
            btnUpload.Enabled = false;
            try
            {
                DateTime fromDate, toDate;
                string fileName = String.Empty, note = String.Empty;
                using (var view = new ProductionPlanImportView())
                {
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    if (String.IsNullOrWhiteSpace(view.FileName))
                        return;

                    fromDate = view.FromDate;
                    toDate = view.ToDate;
                    fileName = view.FileName;

                }

                var presenter = _presenter as IProductionPlanPresenter;
                if (await presenter.IsExistImportedLot(fromDate, toDate))
                {
                    using (var view = new ProductionPlanImportDescrView())
                    {
                        if (view.ShowDialog() != DialogResult.OK)
                            return;

                        note = view.Note;
                    }
                }

                await presenter.ImportExcel(fromDate, toDate, fileName, note);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnUpload.Enabled = true;
            }
        }

        private async void btnExportExcel_Click(object sender, EventArgs e)
        {
            btnExportExcel.Enabled = false;
            try
            {
                bool isExportCarton = false;
                bool isExportProduct = false;
                string folderName = String.Empty;
                using (var view = new ProductionPlanExportView())
                {
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    isExportCarton = view.IsExportCarton;
                    isExportProduct = view.IsExportProduct;
                    folderName = view.FolderName;
                }

                var productLots = new List<string>();

                foreach (DataGridViewRow row in dtgProductionPlan.SelectedRows)
                {
                    string productLot = row.Cells[colProductionPlan_ProductLot.Index].Value.ToString();
                    productLots.Add(productLot);
                }

                await (_presenter as IProductionPlanPresenter).ExportQRCode(productLots, isExportCarton, isExportProduct, folderName);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnExportExcel.Enabled = true;
            }
        }

        private async void btnGenBarcode_Click(object sender, EventArgs e)
        {
            btnGenBarcode.Enabled = false;
            try
            {
                bool isGenCarton;
                bool isGenProduct;
                using (var view = new GenBarcode())
                {
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    isGenCarton = view.IsGenCarton;
                    isGenProduct = view.IsGenProduct;
                }

                var productLotsForProductBarcode = new List<string>();
                var productLotsForCartonBarcode = new List<string>();

                foreach (DataGridViewRow row in dtgProductionPlan.SelectedRows)
                {
                    string productLot = row.Cells[colProductionPlan_ProductLot.Index].Value.ToString();

                    var value = dtgProductionPlan.SelectedRows[0].Cells[colProductionPlan_IsGenProductBarcode.Index].Value;
                    bool isGenProductBarcode = value == null ? false : bool.Parse(value.ToString());

                    value = dtgProductionPlan.SelectedRows[0].Cells[colProductionPlan_IsGenCartonBarcode.Index].Value;
                    bool isGenCartonBarcode = value == null ? false : bool.Parse(value.ToString());

                    if (!isGenProductBarcode)
                        productLotsForProductBarcode.Add(productLot);

                    string productPackingType = row.Cells[colProductionPlan_ProductPackingType.Index].Value.ToString();
                    if (productPackingType == Product.packingType.Carton && !isGenCartonBarcode)
                        productLotsForCartonBarcode.Add(productLot);
                }

                if (isGenProduct)
                    await (_presenter as IProductionPlanPresenter).GenerateProductBarcode(productLotsForProductBarcode);
                if (isGenCarton)
                    await (_presenter as IProductionPlanPresenter).GenerateCartonBarcode(productLotsForCartonBarcode);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnGenBarcode.Enabled = true;
            }
        }

        private async void btnGenAdditionBarcode_Click(object sender, EventArgs e)
        {
            if (dtgProductionPlan.SelectedRows.Count == 0)
                return;

            var value = dtgProductionPlan.SelectedRows[0].Cells[colProductionPlan_IsGenProductBarcode.Index].Value;
            bool isGenProductBarcode = value == null ? false : bool.Parse(value.ToString());

            value = dtgProductionPlan.SelectedRows[0].Cells[colProductionPlan_IsGenCartonBarcode.Index].Value;
            bool isGenCartonBarcode = value == null ? false : bool.Parse(value.ToString());

            if (!isGenProductBarcode && !isGenCartonBarcode)
                return;

            btnGenAdditionBarcode.Enabled = false;
            try
            {
                string productLot = dtgProductionPlan.SelectedRows[0].Cells[colProductionPlan_ProductLot.Index].Value.ToString();
                int productID = int.Parse(dtgProductionPlan.SelectedRows[0].Cells[colProductionPlan_ProductID.Index].Value.ToString());
                int qty = 0;
                string barcodeType;
                using (var view = new GenAdditionBarcode())
                {
                    view.ProductLot = productLot;
                    view.ProductPackingType = dtgProductionPlan.SelectedRows[0].Cells[colProductionPlan_ProductPackingType.Index].Value.ToString();

                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    qty = view.Qty;
                    barcodeType = view.BarcodeType;
                }

                if (qty == 0)
                    return;

                if (barcodeType == "P")
                    await (_presenter as IProductionPlanPresenter).GenerateAdditionProductBarcode(productLot, productID, qty);
                else
                    await (_presenter as IProductionPlanPresenter).GenerateAdditionCartonBarcode(productLot, productID, qty);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnGenAdditionBarcode.Enabled = true;
            }
        }

        private async void btnHold_Click(object sender, EventArgs e)
        {
            btnHold.Enabled = false;
            try
            {
                var productLots = new List<string>();

                foreach (DataGridViewRow row in dtgProductionPlan.SelectedRows)
                {
                    string productLot = row.Cells[colProductionPlan_ProductLot.Index].Value.ToString();
                    productLots.Add(productLot);
                }

                await (_presenter as IProductionPlanPresenter).HoldProductionPlan(productLots);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnHold.Enabled = true;
            }
        }

        private async void btnUnHold_Click(object sender, EventArgs e)
        {
            btnHold.Enabled = false;
            try
            {
                var productLots = new List<string>();

                foreach (DataGridViewRow row in dtgProductionPlan.SelectedRows)
                {
                    string productLot = row.Cells[colProductionPlan_ProductLot.Index].Value.ToString();
                    productLots.Add(productLot);
                }

                await (_presenter as IProductionPlanPresenter).UnHoldProductionPlan(productLots);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
            finally
            {
                btnHold.Enabled = true;
            }
        }

        private async void dtgProductionPlan_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1 || e.ColumnIndex == -1)
                    return;

                if (e.ColumnIndex == colProductionPlan_Print.Index)
                    await PrintCartonBarcode(bdsProductionPlans.Current as ProductionPlan);
                else
                    await UpdateProductionPlan(bdsProductionPlans.Current as ProductionPlan);
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        private void dtgProductionPlan_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int rowIndex = dtgProductionPlan.HitTest(e.X, e.Y).RowIndex;
                if (rowIndex == -1)
                    contextMenuStrip1.Enabled = false;
                else
                {
                    contextMenuStrip1.Enabled = true;
                    _rowIndex = rowIndex;
                }
            }
        }

        private void tsmiChangeStatusInProgress_Click(object sender, EventArgs e)
        {
            try
            {
                if (_rowIndex != -1)
                {
                    var productionPlan = bdsProductionPlans.Current as ProductionPlan;
                    (_presenter as IProductionPlanPresenter).UpdateProductionPlanStatus(productionPlan.ProductLot, ProductionPlan.status.InProgress);
                }
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        private SerialPort GetPrinterSerialPort()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            XmlDocument doc = new XmlDocument();
            doc.Load(config.FilePath);
            var node = doc.SelectSingleNode("//printerConfig");

            if (node == null)
                throw new WrappedException(Messages.Error_COMConfigSetup);

            Parity p = Parity.None;
            switch (node.Attributes["Parity"].Value)
            {
                case "Even":
                    p = Parity.Even;
                    break;
                case "Mark":
                    p = Parity.Mark;
                    break;
                case "Odd":
                    p = Parity.Odd;
                    break;
                case "Space":
                    p = Parity.Space;
                    break;
                default:
                    break;
            }

            StopBits sb = StopBits.None;
            switch (node.Attributes["StopBits"].Value)
            {
                case "1":
                    sb = StopBits.One;
                    break;
                case "2":
                    sb = StopBits.Two;
                    break;
                case "1.5":
                    sb = StopBits.OnePointFive;
                    break;
                default:
                    break;
            }

            var serialPort = new SerialPort(node.Attributes["PortName"].Value, int.Parse(node.Attributes["BaudRate"].Value), p, int.Parse(node.Attributes["DataBits"].Value), sb);

            return serialPort;
        }

        private async Task UpdateProductionPlan(ProductionPlan productionPlan)
        {
            using (var view = new ProductionPlanUpdateView())
            {
                view.Devices = _devices;
                view.PlanDate = productionPlan.PlanDate;
                view.ProductLot = productionPlan.ProductLot;
                view.Device = productionPlan.Device;
                view.PONumber = productionPlan.PONumber;
                view.SupplierLot = productionPlan.SupplierLot;
                view.ProductCode = productionPlan.ProductCode;
                view.ProductDescription = productionPlan.ProductDescription;
                view.status = productionPlan.Status;

                if (view.ShowDialog() != DialogResult.OK)
                    return;

                await (_presenter as IProductionPlanPresenter).UpdateProductionPlan(productionPlan.ProductLot, view.PlanDate, view.Device, view.PONumber, view.SupplierLot, view.Note);
            }
        }

        private async Task PrintCartonBarcode(ProductionPlan productionPlan)
        {
            try
            {
                if (!productionPlan.IsGenCartonBarcode)
                    throw new WrappedException(Messages.Error_ProductLotNotYetGenBarcode);

                if ((productionPlan.MaxLabelCartonBarcode ?? 0) - (productionPlan.CurrentLabelCartonBarcode ?? 0) == 0)
                    throw new WrappedException(Messages.Error_ProductCartonLabelIsPrintedOut);

                var serialPort = GetPrinterSerialPort();

                int qty = 0;
                string note;
                using (var view = new ProductionPlanPrintConfirmView())
                {
                    view.Quantity = (productionPlan.MaxLabelCartonBarcode ?? 0) - (productionPlan.CurrentLabelCartonBarcode ?? 0);
                    if (view.ShowDialog() != DialogResult.OK)
                        return;

                    qty = view.Quantity;
                    note = view.Note;
                }

                try
                {
                    if (!serialPort.IsOpen)
                        serialPort.Open();

                    var cartonLabels = await (_presenter as IProductionPlanPresenter).GetCartonLabelToPrint(productionPlan, qty, note);

                    foreach (var label in cartonLabels)
                    {
                        byte[] b = Encoding.UTF8.GetBytes(label);
                        serialPort.Write(b, 0, b.Length);
                    }
                }
                finally
                {
                    if (serialPort.IsOpen)
                        serialPort.Close();
                }
            }
            catch (WrappedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_CanNotConnectPrinter);
            }
        }

        public override void InitializeComboBox()
        {
            cmbProduct.PageSize = 20;
            cmbProduct.ValueMember = "ProductID";
            cmbProduct.DisplayMember = "ProductCode - Description";
            cmbProduct.Columns = new List<MultiColumnComboBox.ComboBoxColumn>
            {
                new MultiColumnComboBox.ComboBoxColumn("ProductCode", "Mã sản phẩm", 130),
                new MultiColumnComboBox.ComboBoxColumn("Description", "Mô tả", 200),
                new MultiColumnComboBox.ComboBoxColumn("TypeDisplay", "Loại sản phẩm", 150),
                new MultiColumnComboBox.ComboBoxColumn("UOM", "ĐVT", 100),
                new MultiColumnComboBox.ComboBoxColumn("StatusDisplay", "Trạng thái", 100)
            };
        }

        public override async Task SetPresenter(IBasePresenter presenter, bool isRefresh = false)
        {
            try
            {
                await base.SetPresenter(presenter);

                InitializeComboBox();

                var productionPlanPresenter = _presenter as IProductionPlanPresenter;

                await Task.WhenAll(productionPlanPresenter.LoadDevices(), productionPlanPresenter.LoadProducts(), productionPlanPresenter.LoadMainData());
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public override async Task Delete()
        {
            try
            {
                foreach (DataGridViewRow row in dtgProductionPlan.SelectedRows)
                {
                    var prodPlan = bdsProductionPlans[row.Index] as ProductionPlan;

                    bdsProductionPlans.Remove(prodPlan);
                    (_presenter as IProductionPlanPresenter).DeleteProductionPlans(prodPlan);
                }

                await Task.Run(() => { });
            }
            catch (WrappedException ex)
            {
                _presenter.SetMessage(ex.Message, Utility.MessageType.Error);
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                _presenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
            }
        }

        public DateTime FromDate
        {
            get { return dtpFromDate.Value.Date; }
        }

        public DateTime ToDate
        {
            get { return dtpToDate.Value.Date; }
        }

        public string ProductLot
        {
            get { return txtProductionLot.Text; }
        }

        public int ProductID
        {
            get
            {
                if (cmbProduct.SelectedItem == null)
                    return 0;
                else
                    return int.Parse($"{cmbProduct.SelectedItem["ProductID"]}");
            }
        }

        private IList<Device> _devices;

        public IList<Device> Devices
        {
            set { _devices = value; }
        }

        public DataTable Products
        {
            set { cmbProduct.Source = value; }
        }

        private IList<ProductionPlan> _productionPlans;

        public IList<ProductionPlan> ProductionPlans
        {
            get { return _productionPlans; }
            set
            {
                _productionPlans = value;
                if (InvokeRequired)
                {
                    BeginInvoke((MethodInvoker)delegate
                    {
                        bdsProductionPlans.DataSource = _productionPlans;
                    });
                }
                else
                {
                    bdsProductionPlans.DataSource = _productionPlans;
                }
            }
        }

        private IList<ProductionPlan> _deletedProductionPlans;

        public IList<ProductionPlan> DeletedProductionPlans
        {
            get { return _deletedProductionPlans; }
            set { _deletedProductionPlans = value; }
        }

        public override void HandlePermission(string accessRight)
        {
            //// permission import production plan
            string prmImExl = Utility.Sitemaps.First(p => p.SitemapID == 19).AccessRights;
            //// permission gen QR code
            string prmGenQRCode = Utility.Sitemaps.First(p => p.SitemapID == 20).AccessRights;
            //// permission export QR code
            string prmExportQRCode = Utility.Sitemaps.First(p => p.SitemapID == 27).AccessRights;
            //// permission hold production plan
            string prmHoldProdPlan = Utility.Sitemaps.First(p => p.SitemapID == 26).AccessRights;
            //// permission change production plan status
            string prmChangeStatus = Utility.Sitemaps.First(p => p.SitemapID == 34).AccessRights;

            if (prmImExl == RoleSitemap.accessRights.NotSet || prmImExl == RoleSitemap.accessRights.Read)
            {
                btnUpload.Enabled = false;
                btnUpload.Click -= btnUpload_Click;
            }

            if (prmGenQRCode == RoleSitemap.accessRights.NotSet || prmGenQRCode == RoleSitemap.accessRights.Read)
            {
                btnGenBarcode.Enabled = false;
                btnGenBarcode.Click -= btnGenBarcode_Click;

                btnGenAdditionBarcode.Enabled = false;
                btnGenAdditionBarcode.Click -= btnGenAdditionBarcode_Click;
            }

            if (prmExportQRCode == RoleSitemap.accessRights.NotSet || prmGenQRCode == RoleSitemap.accessRights.Read)
            {
                btnExportExcel.Enabled = false;
                btnExportExcel.Click -= btnExportExcel_Click;
            }

            if (prmHoldProdPlan == RoleSitemap.accessRights.NotSet || prmGenQRCode == RoleSitemap.accessRights.Read)
            {
                btnHold.Enabled = false;
                btnHold.Click -= btnHold_Click;

                btnUnHold.Enabled = false;
                btnUnHold.Click -= btnUnHold_Click;
            }

            if (prmChangeStatus == RoleSitemap.accessRights.NotSet || prmImExl == RoleSitemap.accessRights.Read)
            {
                dtgProductionPlan.ContextMenuStrip = null;
            }
        }
    }
}
