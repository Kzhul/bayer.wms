﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Prd.Presenters
{
    public interface IDeviceView : IBaseView
    {
        System.Data.DataTable Devices { set; }

        Device Device { get; set; }

        string PalletType { get; set; }
    }

    public interface IDeviceMaintPresenter : IBasePresenter
    {
        Task LoadDevices();
    }

    public class DeviceMaintPresenter : BasePresenter, IDeviceMaintPresenter
    {
        private IDeviceView _mainView;
        private IDeviceRepository _deviceRepository;

        public DeviceMaintPresenter(IDeviceView view, IUnitOfWorkManager unitOfWordManager, IBasePresenter mainPresenter, IDeviceRepository deviceRepository)
            : base(view, unitOfWordManager, mainPresenter)
        {
            _mainView = view;
            _deviceRepository = deviceRepository;
        }

        /// <summary>
        /// Event trigger when user select item in combobox
        /// </summary>
        /// <param name="selectedItem"></param>
        public async Task OnDeviceCodeSelectChange(DataRow selectedItem)
        {
            try
            {
                Device device = new Device();
                await Task.Run(() => 
                {
                    device = selectedItem.ToEntity<Device>();
                    device.State = EntityState.Unchanged;
                });
                _mainView.Device = device;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        /// <summary>
        /// Get devices from database to combobox
        /// </summary>
        /// <returns></returns>
        public async Task LoadDevices()
        {
            try
            {
                var devices = await _deviceRepository.GetAsync(p => !p.IsDeleted);
                _mainView.Devices = devices.ToDataTable();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public override void Insert()
        {
            _mainView.Device = new Device();
        }

        /// <summary>
        /// Save device to database, 2 cases:
        ///     Case 1: insert new device
        ///     Case 2: update existing record
        /// </summary>
        /// <returns></returns>
        public override async Task Save()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var device = _mainView.Device;
            bool isSuccess = false;
            try
            {
                device.PalletType = _mainView.PalletType;
                device.IsRecordAuditTrail = true;

                await _deviceRepository.InsertOrUpdate(device, new object[] { device.DeviceCode });
                await _deviceRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            //// Exception when existing device is updated by another user
            catch (DbUpdateConcurrencyException)
            {
                await Refresh(_mainView.Sitemap.SitemapID);
                throw new WrappedException(Messages.Error_ConcurrencyUpdate);
            }
            //// Exception when data is not valid
            catch (DbEntityValidationException ex)
            {
                List<string> message = new List<string>();
                foreach (var entityError in ex.EntityValidationErrors)
                {
                    foreach (var error in entityError.ValidationErrors)
                    {
                        message.Add(error.ErrorMessage);
                    }
                }
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(String.Join(" ", message));
            }
            //// Exception when execute at database
            catch (DbUpdateException ex)
            {
                string message = String.Empty;
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                {
                    SqlException sqlException = ex.InnerException.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        switch (sqlException.Number)
                        {
                            case 2627: //// Duplicate unique key
                                message = await ResolveDuplicate(device);
                                break;
                            default: //// unknown exception
                                message = Messages.Error_Common;
                                break;
                        }
                    }
                }

                if (!String.IsNullOrWhiteSpace(message))
                {
                    Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                    throw new WrappedException(message);
                }
                else
                {
                    isSuccess = true;
                }
            }
            //// unknown exception
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                _deviceRepository = _mainPresenter.Resolve(_deviceRepository.GetType()) as IDeviceRepository;

                //// if insert or update sucessfully, refresh combobox
                if (isSuccess)
                {
                    await LoadDevices();

                    _mainView.Device = device;
                }
            }
        }

        /// <summary>
        /// Delete logical
        /// </summary>
        /// <returns></returns>
        public override async Task Delete()
        {
            SetAuditTrailInformation(MethodBase.GetCurrentMethod());

            var device = _mainView.Device;
            bool isSuccess = false;
            try
            {
                device.IsDeleted = true;
                device.IsRecordAuditTrail = true;

                await _deviceRepository.Update(device, new string[] { "IsDeleted" }, new object[] { device.DeviceCode });
                await _deviceRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                _mainPresenter.SetMessage(Messages.Error_Common, Utility.MessageType.Error);
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
            }
            finally
            {
                //// if delete sucessfully, refresh combobox and change screen to insert state
                if (isSuccess)
                {
                    await LoadDevices();
                    Insert();
                }
            }
        }

        /// <summary>
        /// Resolve when insert new device which device code is already existed, 2 cases:
        ///     Case 1: existing device is not deleted, throw exception.
        ///     Case 2: existing device is deleted, ask user to undelete it
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public async Task<string> ResolveDuplicate(Device device)
        {
            string message = String.Empty;
            var existDevice = await _deviceRepository.GetSingleAsync(p => p.DeviceCode == device.DeviceCode);
            if (!existDevice.IsDeleted)
            {
                message = Messages.Error_DeviceCodeExisted;
            }
            else if (_mainView.GetConfirm(Messages.Question_RecoverDeleted))
            {
                device.DeviceCode = existDevice.DeviceCode;
                device.RowVersion = existDevice.RowVersion;
                await _deviceRepository.Update(device, new object[] { device.DeviceCode });
                await _deviceRepository.Commit();

                _mainPresenter.SetMessage(Messages.Information_SaveSucessfully, Utility.MessageType.Information);
            }

            return message;
        }
    }
}
