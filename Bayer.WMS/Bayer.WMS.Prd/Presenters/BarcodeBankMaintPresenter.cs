﻿using Bayer.WMS.Objs;
using Bayer.WMS.Objs.Models;
using Bayer.WMS.Objs.Repositories;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bayer.WMS.Prd.Presenters
{
    public interface IBarcodeBankMaintView : IBaseView
    {
        int TotalNotYetUsed { set; }

        int TotalExported { set; }

        int TotalNotYetExport { set; }

        System.Data.DataTable Products { set; }

        System.Data.DataTable ReportHistory { set; }
    }

    public interface IBarcodeBankMaintPresenter : IBasePresenter
    {
        Task GenerateBarcode();

        Task ExportQRProductCode(int qty, string filePath, string fileName, string productCode, string description);
    }

    public class BarcodeBankMaintPresenter : BasePresenter, IBarcodeBankMaintPresenter
    {
        private IBarcodeBankMaintView _mainView;
        private IBarcodeBankRepository _barcodeBankRepository;
        private IProductRepository _productRepository;

        private int _totalNotYetUsed;
        private int _totalExported;

        public BarcodeBankMaintPresenter(IBarcodeBankMaintView view, IUnitOfWorkManager unitOfWorkManager, IBasePresenter mainPresenter,
            IBarcodeBankRepository barcodeBankRepository
            , IProductRepository productRepository
            )
            : base(view, unitOfWorkManager, mainPresenter)
        {
            _mainView = view;
            _barcodeBankRepository = barcodeBankRepository;
            _productRepository = productRepository;
            _mainView.Products = new System.Data.DataTable();
            _mainView.ReportHistory = new System.Data.DataTable();
        }

        public override async Task LoadMainData()
        {
            try
            {
                int yearID = Convert.ToInt32(DateTime.Today.ToString("yy"));
                _totalNotYetUsed = await _barcodeBankRepository.GetTotalRecord(p=>p.Year == yearID);
                _totalExported = await _barcodeBankRepository.GetTotalRecord(p => p.Year == yearID && p.IsExport);
                var products = await _productRepository.GetIncludeCategoryAsync(p => !p.IsDeleted);
                _mainView.Products = products.OrderBy(a => a.Description).ToList().ToDataTable();
                await LoadHistory();
                _mainView.TotalNotYetUsed = _totalNotYetUsed;
                _mainView.TotalExported = _totalExported;
                _mainView.TotalNotYetExport = _totalNotYetUsed - _totalExported;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task LoadHistory()
        {
            _mainView.ReportHistory = await _barcodeBankRepository.ExecuteDataTable("proc_BarcodeBankManager_Select", null);
        }

        public async Task GenerateBarcode()
        {
            try
            {
                SqlParameter outputAvailableParam = new SqlParameter("@Available", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };

                SqlParameter outputExportedParam = new SqlParameter("@Exported", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };

                await _barcodeBankRepository.ExecuteNonQueryLongPeriod("proc_BarcodeBank_Generate",
                    new SqlParameter { ParameterName = "@CreateBy", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                    new SqlParameter { ParameterName = "@CreatedBySitemapID", SqlDbType = SqlDbType.Int, Value = _mainView.Sitemap.SitemapID },
                    new SqlParameter { ParameterName = "@UpdatedBy", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                    new SqlParameter { ParameterName = "@UpdatedBySitemapID", SqlDbType = SqlDbType.Int, Value = _mainView.Sitemap.SitemapID },
                    outputAvailableParam,
                    outputExportedParam
                    );

                _totalNotYetUsed = int.Parse(outputAvailableParam.Value.ToString());
                _totalExported = int.Parse(outputExportedParam.Value.ToString());
                _mainView.TotalNotYetUsed = _totalNotYetUsed;
                _mainView.TotalExported = _totalExported;
                _mainView.TotalNotYetExport = _totalNotYetUsed - _totalExported;
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
        }

        public async Task ExportQRProductCode(int qty, string filePath, string fileName, string productCode, string description)
        {
            Application excelApp = new Application();
            _Workbook workbook = excelApp.Workbooks.Add(Missing.Value);
            _Worksheet sheetDetail = null;
            Range range = null;
            Workbooks workbooks = excelApp.Workbooks;

            try
            {
                string template = Path.Combine(Utility.AppPath, @"Templates\QR_Product.xlsx");

                workbook = workbooks.Open(template, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                sheetDetail = (Worksheet)workbook.ActiveSheet;

                #region Get Data
                SqlParameter fromSequence = new SqlParameter("@FromSequence", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };

                SqlParameter toSequence = new SqlParameter("@ToSequence", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                var dt = await _barcodeBankRepository.ExecuteDataTable("proc_BarcodeBank_Select_Export",
                    new SqlParameter { ParameterName = "Quantity", SqlDbType = SqlDbType.Int, Value = qty },
                    fromSequence,
                    toSequence
                    );
                #endregion
                #region Encrypt and Save
                string strInfo = "|";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sheetDetail.Cells[2 + i, 1] = Utility.AESEncrypt(dt.Rows[i]["Barcode"].ToString()) + strInfo;
                }

                excelApp.DisplayAlerts = false;
                workbook.SaveAs(filePath, XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false,
                    XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                #endregion

                #region Update Exported
                await _barcodeBankRepository.ExecuteNonQuery("proc_BarcodeBank_Update_Exported",
                            new SqlParameter { ParameterName = "Quantity", SqlDbType = SqlDbType.Int, Value = qty });
                #endregion

                #region AuditTrail
                await _barcodeBankRepository.ExecuteDataTable("proc_BarcodeBankManager_Insert",
                    new SqlParameter { ParameterName = "UserID", SqlDbType = SqlDbType.Int, Value = LoginInfo.UserID },
                    new SqlParameter { ParameterName = "ProductCode", SqlDbType = SqlDbType.NVarChar, Value = productCode },
                    new SqlParameter { ParameterName = "Description", SqlDbType = SqlDbType.NVarChar, Value = description },
                    new SqlParameter { ParameterName = "FileName", SqlDbType = SqlDbType.NVarChar, Value = fileName },
                    new SqlParameter { ParameterName = "Year", SqlDbType = SqlDbType.Int, Value = DateTime.Now.Year },
                    new SqlParameter { ParameterName = "Quantity", SqlDbType = SqlDbType.Int, Value = qty },
                    new SqlParameter { ParameterName = "SequenceFrom", SqlDbType = SqlDbType.Int, Value = fromSequence.Value },
                    new SqlParameter { ParameterName = "SequenceTo", SqlDbType = SqlDbType.Int, Value = toSequence.Value }
                    );

                RecordAuditTrail(AuditTrail.action.ExportProductBarcode, MethodBase.GetCurrentMethod());
                #endregion


                _totalExported += qty;
                _mainView.TotalExported = _totalExported;
                _mainView.TotalNotYetExport = _totalNotYetUsed - _totalExported;

                await LoadHistory();
            }
            catch (Exception ex)
            {
                Utility.LogEx(ex, MethodBase.GetCurrentMethod());
                throw new WrappedException(Messages.Error_Common);
            }
            finally
            {
                workbook.Close(false, Type.Missing, Type.Missing);
                workbooks.Close();

                if (excelApp != null)
                    excelApp.Quit();
                if (range != null)
                    Marshal.ReleaseComObject(range);
                if (sheetDetail != null)
                    Marshal.ReleaseComObject(sheetDetail);
                if (workbook != null)
                    Marshal.ReleaseComObject(workbook);
                if (workbooks != null)
                    Marshal.ReleaseComObject(workbooks);
                if (excelApp != null)
                    Marshal.ReleaseComObject(excelApp);
                Thread.Sleep(1000);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}