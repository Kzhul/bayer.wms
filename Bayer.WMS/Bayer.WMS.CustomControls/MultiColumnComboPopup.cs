﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Bayer.WMS.CustomControls
{
    public delegate void AfterRowSelectEventHandler(object sender, DataRow selectedItem);
    /// <summary>
    /// Summary description for MultiColumnComboPopup.
    /// </summary>
    public class MultiColumnComboPopup : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer _components;
        private DataRow _selectedItem;
        private DataTable _source;
        private DataGridView _dataGridView;
        private BindingSource _bindingSource;
        private TextBox _txtSearch;
        private Label _lblSearch;
        private string _searchColumns;

        public event AfterRowSelectEventHandler AfterRowSelectEvent;

        public MultiColumnComboPopup()
        {
            InitializeComponent();
            SetDoubleBuffered();
        }

        public MultiColumnComboPopup(DataTable source, string valueMember, List<MultiColumnComboBox.ComboBoxColumn> columns)
        {
            _source = source;
            _searchColumns = String.Join(" + ", columns.Select(p => p.Name));
            InitializeComponent();
            SetDoubleBuffered();
            InitializeDataGrid(source, valueMember, columns);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_components != null)
                {
                    _components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        protected void SetDoubleBuffered()
        {
            Type dgvType = _dataGridView.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(_dataGridView, true, null);
        }

        private void MultiColumnComboPopup_Load(object sender, EventArgs e)
        {

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            var searchResult = _source.Select(String.Format("{0} LIKE '%{1}%'", _searchColumns, _txtSearch.Text));
            var source = _source.Clone();
            if (searchResult.Length > 0)
                source = searchResult.CopyToDataTable();
            _bindingSource.DataSource = source;
        }

        private void MultiColumnComboPopup_Deactivate(object sender, EventArgs e)
        {
            Close();
        }

        private void MultiColumnComboPopup_Leave(object sender, EventArgs e)
        {
            Close();
        }

        private void MultiColumnComboPopup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                Close();
            }
        }

        private void _dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                _selectedItem = (_bindingSource.Current as DataRowView).Row;
                if (AfterRowSelectEvent != null)
                    AfterRowSelectEvent(sender, _selectedItem);

                Close();
            }
        }

        private void InitializeDataGrid(DataTable source, string valueMember, List<MultiColumnComboBox.ComboBoxColumn> columns)
        {
            _bindingSource.DataSource = source;
            _dataGridView.AutoGenerateColumns = false;

            int index = _dataGridView.Columns.Add(valueMember, valueMember);
            _dataGridView.Columns[index].Visible = false;
            foreach (var col in columns)
            {
                index = _dataGridView.Columns.Add(col.Name, col.Caption);
                _dataGridView.Columns[index].DataPropertyName = col.Name;
                _dataGridView.Columns[index].Width = col.Width;
            }
            index = _dataGridView.Columns.Add("last", "");
            _dataGridView.Columns[index].Resizable = DataGridViewTriState.True;
            _dataGridView.Columns[index].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this._dataGridView = new System.Windows.Forms.DataGridView();
            this._bindingSource = new System.Windows.Forms.BindingSource(this._components);
            this._txtSearch = new System.Windows.Forms.TextBox();
            this._lblSearch = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _dataGridView
            // 
            this._dataGridView.AllowUserToAddRows = false;
            this._dataGridView.AllowUserToDeleteRows = false;
            this._dataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Azure;
            this._dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._dataGridView.AutoGenerateColumns = false;
            this._dataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dataGridView.DataSource = this._bindingSource;
            this._dataGridView.GridColor = System.Drawing.SystemColors.Control;
            this._dataGridView.Location = new System.Drawing.Point(2, 40);
            this._dataGridView.MultiSelect = false;
            this._dataGridView.Name = "_dataGridView";
            this._dataGridView.ReadOnly = true;
            this._dataGridView.RowHeadersVisible = false;
            this._dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dataGridView.Size = new System.Drawing.Size(392, 127);
            this._dataGridView.TabIndex = 0;
            this._dataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._dataGridView_CellDoubleClick);
            // 
            // _txtSearch
            // 
            this._txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtSearch.Location = new System.Drawing.Point(186, 12);
            this._txtSearch.Name = "_txtSearch";
            this._txtSearch.Size = new System.Drawing.Size(200, 22);
            this._txtSearch.TabIndex = 1;
            this._txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // _lblSearch
            // 
            this._lblSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._lblSearch.AutoSize = true;
            this._lblSearch.Location = new System.Drawing.Point(117, 15);
            this._lblSearch.Name = "_lblSearch";
            this._lblSearch.Size = new System.Drawing.Size(63, 16);
            this._lblSearch.TabIndex = 2;
            this._lblSearch.Text = "Tìm kiếm";
            // 
            // MultiColumnComboPopup
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(398, 170);
            this.ControlBox = false;
            this.Controls.Add(this._lblSearch);
            this.Controls.Add(this._txtSearch);
            this.Controls.Add(this._dataGridView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "MultiColumnComboPopup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.Deactivate += new System.EventHandler(this.MultiColumnComboPopup_Deactivate);
            this.Load += new System.EventHandler(this.MultiColumnComboPopup_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MultiColumnComboPopup_KeyDown);
            this.Leave += new System.EventHandler(this.MultiColumnComboPopup_Leave);
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public void FocusSearch()
        {
            _txtSearch.Focus();
        }

        public void SetSelected(object value)
        {

        }

        public object SelectedItem
        {
            get { return _selectedItem; }
        }

        public string ValueMember { get; set; }

        public int HeaderHeight
        {
            get { return _dataGridView.ColumnHeadersHeight; }
        }

        public int RowHeight
        {
            get { return _dataGridView.RowTemplate.Height; }
        }
    }
}
