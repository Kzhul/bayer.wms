﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Bayer.WMS.CustomControls
{
    public class CustomMessageBox : Form
    {
        private PictureBox _pictureBox1;
        private Button _btnOK;
        private Label _lblMessage;

        public CustomMessageBox()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomMessageBox));
            this._lblMessage = new System.Windows.Forms.Label();
            this._pictureBox1 = new System.Windows.Forms.PictureBox();
            this._btnOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // _lblMessage
            // 
            this._lblMessage.BackColor = System.Drawing.SystemColors.Control;
            this._lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblMessage.Location = new System.Drawing.Point(68, 9);
            this._lblMessage.Name = "_lblMessage";
            this._lblMessage.Size = new System.Drawing.Size(504, 117);
            this._lblMessage.TabIndex = 0;
            this._lblMessage.Text = "Message";
            // 
            // _pictureBox1
            // 
            this._pictureBox1.Location = new System.Drawing.Point(12, 12);
            this._pictureBox1.Name = "_pictureBox1";
            this._pictureBox1.Size = new System.Drawing.Size(50, 50);
            this._pictureBox1.TabIndex = 1;
            this._pictureBox1.TabStop = false;
            // 
            // _btnOK
            // 
            this._btnOK.BackColor = System.Drawing.Color.Green;
            this._btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnOK.ForeColor = System.Drawing.Color.White;
            this._btnOK.Location = new System.Drawing.Point(242, 129);
            this._btnOK.Name = "_btnOK";
            this._btnOK.Size = new System.Drawing.Size(100, 40);
            this._btnOK.TabIndex = 2;
            this._btnOK.Text = "OK";
            this._btnOK.UseVisualStyleBackColor = false;
            this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
            // 
            // CustomMessageBox
            // 
            this.ClientSize = new System.Drawing.Size(584, 181);
            this.Controls.Add(this._btnOK);
            this.Controls.Add(this._pictureBox1);
            this.Controls.Add(this._lblMessage);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomMessageBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this._pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        private void _btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        public void ShowMessage(Control parent, string message, string caption, Bitmap image)
        {
            AcceptButton = _btnOK;
            Top = (parent.Height - Height) / 2;
            Left = (parent.Width - Width) / 2 + parent.Left;

            Text = caption;
            _lblMessage.Text = message;
            _pictureBox1.Image = image;

            Show();
            BringToFront();
        }
    }
}
