using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;

namespace Bayer.WMS.CustomControls
{
    /// <summary>
    /// Summary description for MultiColumnComboBox.
    /// </summary>
    public delegate void AfterSelectEventHandler();
    public class MultiColumnComboBox : ComboBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container _components;
        private object _selectedValue;
        private DataRow _selectedItem;
        private DataTable _source;
        private List<MultiColumnComboBox.ComboBoxColumn> _columns;
        private ComboBoxPresenterInfo _presenterInfo;
        private int _pageSize;

        public MultiColumnComboBox(IContainer container)
        {
            /// <summary>
            /// Required for Windows.Forms Class Composition Designer support
            /// </summary>
            container.Add(this);
            InitializeComponent();

            this.TextChanged += MultiColumnComboBox_TextChanged;
            this.KeyPress += MultiColumnComboBox_KeyPress;
        }

        public MultiColumnComboBox()
        {
            InitializeComponent();
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            _components = new Container();
            DropDownHeight = 1;
        }

        #endregion

        protected override void OnDropDown(EventArgs e)
        {
            Form parent = FindForm();
            parent.ActiveControl = null;

            base.OnDropDown(e);
            DroppedDown = false;

            if (_source != null)
            {
                int recordsOnPage = _source.Rows.Count > _pageSize ? _pageSize : _source.Rows.Count;
                var point = parent.PointToScreen(Location);

                var popup = new MultiColumnComboPopup(_source, ValueMember, _columns);
                popup.AfterRowSelectEvent += new AfterRowSelectEventHandler(MultiColumnComboBox_AfterSelectEvent);
                popup.Location = new Point(point.X, point.Y + Height);
                popup.Width = DropDownWidth;
                popup.Height = popup.RowHeight * (recordsOnPage > 20 ? 20 : recordsOnPage) + popup.HeaderHeight + 70;
                popup.Show();
                popup.Focus();
                popup.FocusSearch();
            }
        }

        protected void MultiColumnComboBox_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(this.Text))
            {
                _selectedItem = null;
            }
        }

        private void MultiColumnComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {

            }
        }

        protected void MultiColumnComboBox_AfterSelectEvent(object sender, DataRow selectedItem)
        {
            _selectedItem = selectedItem;
            _selectedValue = _selectedItem[ValueMember];

            var regex = new Regex(@"[-|.]");
            string seperator = regex.Match(DisplayMember).Value;
            string[] displayMembers = regex.Split(DisplayMember);
            string[] displayText = new string[displayMembers.Length];
            for (int i = 0; i < displayMembers.Length; i++)
            {
                displayText[i] = $"{_selectedItem[displayMembers[i].Trim()]}";
            }

            Text = String.Join(" - ", displayText);

            InvokeSelectedChangeMethod();
        }

        protected void InvokeSelectedChangeMethod()
        {
            if (_presenterInfo != null && !String.IsNullOrWhiteSpace(_presenterInfo.OnSelectedChangeMethod))
            {
                MethodInfo methodInfo = _presenterInfo.Presenter.GetType().GetMethod(_presenterInfo.OnSelectedChangeMethod);
                methodInfo.Invoke(_presenterInfo.Presenter, new object[] { _selectedItem });
            }
        }

        public void Load()
        {
            var methodInfo = _presenterInfo.Presenter.GetType().GetMethod(_presenterInfo.LoadDataMethod);
            methodInfo.Invoke(_presenterInfo.Presenter, null);
        }

        public new object SelectedValue
        {
            get { return _selectedValue; }
            set
            {
                _selectedValue = value;
                if (value != null && value != DBNull.Value)
                {
                    DataRow[] rows;
                    if (_source.Columns[ValueMember].DataType == typeof(Int32))
                        rows = _source.Select($"{ValueMember} = {value}");
                    else
                        rows = _source.Select($"{ValueMember} = '{value}'");
                    if (rows.Length > 0)
                    {
                        _selectedItem = rows[0];
                        MultiColumnComboBox_AfterSelectEvent(this, _selectedItem);
                    }
                }
            }
        }

        public new DataRow SelectedItem
        {
            get { return _selectedItem; }
        }

        public DataTable Source
        {
            get { return _source; }
            set
            {
                _source = value;
                if (_selectedItem != null)
                {
                    if (_selectedValue != null && _selectedValue != DBNull.Value)
                    {
                        try
                        {
                            var selectedRows = _source.Select(String.Format("{0} = '{1}'", ValueMember, _selectedValue));
                            _selectedItem = selectedRows.Length > 0 ? selectedRows[0] : null;
                        }
                        catch
                        {
                            // Do Nothing
                        }
                    }
                    else
                        _selectedItem = null;

                    InvokeSelectedChangeMethod();
                }
            }
        }

        public List<MultiColumnComboBox.ComboBoxColumn> Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }

        public ComboBoxPresenterInfo PresenterInfo
        {
            get { return _presenterInfo; }
            set { _presenterInfo = value; }
        }

        public int PageSize
        {
            get { return _pageSize; }
            set { _pageSize = value; }
        }

        public class ComboBoxPresenterInfo
        {
            private object _presenter;
            private string _loadDataMethod;
            private string _onSelectedChangeMethod;

            public ComboBoxPresenterInfo() { }

            public ComboBoxPresenterInfo(object presenter, string loadDataMethod, string onSelectedChangeMethod)
            {
                _presenter = presenter;
                _loadDataMethod = loadDataMethod;
                _onSelectedChangeMethod = onSelectedChangeMethod;
            }

            public object Presenter
            {
                get { return _presenter; }
                set { _presenter = value; }
            }

            public string LoadDataMethod
            {
                get { return _loadDataMethod; }
                set { _loadDataMethod = value; }
            }

            public string OnSelectedChangeMethod
            {
                get { return _onSelectedChangeMethod; }
                set { _onSelectedChangeMethod = value; }
            }
        }

        public class ComboBoxColumn
        {
            private string _name;
            private string _caption;
            private int _width;

            public ComboBoxColumn() { }

            public ComboBoxColumn(string name, string caption, int width)
            {
                _name = name;
                _caption = caption;
                _width = width;
            }

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public string Caption
            {
                get { return _caption; }
                set { _caption = value; }
            }

            public int Width
            {
                get { return _width; }
                set { _width = value; }
            }
        }
    }
}
