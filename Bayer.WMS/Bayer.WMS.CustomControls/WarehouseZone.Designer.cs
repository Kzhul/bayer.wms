﻿namespace Bayer.WMS.CustomControls
{
    partial class WarehouseZone
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblZoneName = new System.Windows.Forms.Label();
            this.tlpZoneLine = new System.Windows.Forms.TableLayoutPanel();
            this.SuspendLayout();
            // 
            // lblZoneName
            // 
            this.lblZoneName.AutoSize = true;
            this.lblZoneName.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblZoneName.Location = new System.Drawing.Point(3, 0);
            this.lblZoneName.Name = "lblZoneName";
            this.lblZoneName.Size = new System.Drawing.Size(35, 13);
            this.lblZoneName.TabIndex = 0;
            this.lblZoneName.Text = "label1";
            // 
            // tlpZoneLine
            // 
            this.tlpZoneLine.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tlpZoneLine.ColumnCount = 1;
            this.tlpZoneLine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlpZoneLine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpZoneLine.Cursor = System.Windows.Forms.Cursors.Default;
            this.tlpZoneLine.Location = new System.Drawing.Point(3, 16);
            this.tlpZoneLine.Name = "tlpZoneLine";
            this.tlpZoneLine.RowCount = 1;
            this.tlpZoneLine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tlpZoneLine.Size = new System.Drawing.Size(50, 50);
            this.tlpZoneLine.TabIndex = 1;
            // 
            // WarehouseZone
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpZoneLine);
            this.Controls.Add(this.lblZoneName);
            this.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.Name = "WarehouseZone";
            this.Size = new System.Drawing.Size(56, 69);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblZoneName;
        private System.Windows.Forms.TableLayoutPanel tlpZoneLine;
    }
}
