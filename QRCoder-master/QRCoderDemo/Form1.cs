using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using QRCoder;
using System.Drawing.Imaging;
using System.IO;

namespace QRCoderDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RenderQrCode();
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            RenderQrCode();
        }

        private void RenderQrCode()
        {
            string level = "L";
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(level == "L" ? 0 : level == "M" ? 1 : level == "Q" ? 2 : 3);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(textBoxQRCode.Text, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        Bitmap a = qrCode.GetGraphic(20, Color.Black, Color.White, null, 0);
                        using (Graphics g = Graphics.FromImage(a))
                        {
                            g.DrawString(textBoxQRCode.Text, new Font("Tahoma", 20, FontStyle.Bold), Brushes.Black, new Point(20, 20)); // requires font, brush etc
                        }
                        pictureBoxQRCode.BackgroundImage = a;

                        this.pictureBoxQRCode.Size = new System.Drawing.Size(pictureBoxQRCode.Width, pictureBoxQRCode.Height);
                        //Set the SizeMode to center the image.
                        this.pictureBoxQRCode.SizeMode = PictureBoxSizeMode.CenterImage;

                        pictureBoxQRCode.SizeMode = PictureBoxSizeMode.StretchImage;
                    }
                }
            }
        }

        private void RenderQrCode(string inputValue)
        {
            string level = "L";
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(level == "L" ? 0 : level == "M" ? 1 : level == "Q" ? 2 : 3);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(inputValue, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        //pictureBoxQRCode.BackgroundImage = qrCode.GetGraphic(20, Color.Black, Color.White,
                        //    null, 0);

                        //this.pictureBoxQRCode.Size = new System.Drawing.Size(pictureBoxQRCode.Width, pictureBoxQRCode.Height);
                        ////Set the SizeMode to center the image.
                        //this.pictureBoxQRCode.SizeMode = PictureBoxSizeMode.CenterImage;

                        //pictureBoxQRCode.SizeMode = PictureBoxSizeMode.StretchImage;

                        //save to image
                        try
                        {
                            Bitmap a = qrCode.GetGraphic(20, Color.Black, Color.White, null, 0);
                            using (Graphics g = Graphics.FromImage(a))
                            {
                                g.DrawString(inputValue, new Font("Tahoma", 15, FontStyle.Bold), Brushes.Black, new Point(10, 10)); // requires font, brush etc
                            }
                            a.Save(@"" + txtFolder.Text + @"\" + MakeValidFileName(inputValue) + ".png");
                        }
                        catch
                        {
                            //MessageBox.Show("Please choose folder.");
                        }
                    }
                }
            }
        }

        private static string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);
            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }

        private void RenderQrCode(string inputValue, string displayText)
        {
            string level = "L";
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(level == "L" ? 0 : level == "M" ? 1 : level == "Q" ? 2 : 3);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(inputValue, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        //pictureBoxQRCode.BackgroundImage = qrCode.GetGraphic(20, Color.Black, Color.White,
                        //    null, 0);

                        //this.pictureBoxQRCode.Size = new System.Drawing.Size(pictureBoxQRCode.Width, pictureBoxQRCode.Height);
                        ////Set the SizeMode to center the image.
                        //this.pictureBoxQRCode.SizeMode = PictureBoxSizeMode.CenterImage;

                        //pictureBoxQRCode.SizeMode = PictureBoxSizeMode.StretchImage;

                        //save to image
                        try
                        {
                            Bitmap a = qrCode.GetGraphic(20, Color.Black, Color.White, null, 0);
                            using (Graphics g = Graphics.FromImage(a))
                            {
                                g.DrawString(displayText+ "_"+ inputValue, new Font("Tahoma", 15, FontStyle.Bold), Brushes.Black, new Point(10, 10)); // requires font, brush etc
                            }
                            a.Save(@"" + txtFolder.Text + @"\" + ConvertToValidFileName(displayText + "_" + inputValue) + ".png");
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
            }
        }

        private string ConvertToValidFileName(string fileName)
        {
            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            {
                fileName = fileName.Replace(c, '_');
            }
            return fileName;
        }

        private void selectIconBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDlg = new OpenFileDialog();
            openFileDlg.Title = "Select Excel file";
            openFileDlg.Multiselect = false;
            openFileDlg.CheckFileExists = true;
            if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                iconPath.Text = openFileDlg.FileName;
            }
            else
            {
                iconPath.Text = "";
            }
        }


        private void btn_save_Click(object sender, EventArgs e)
        {

            // Displays a SaveFileDialog so the user can save the Image
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Bitmap Image|*.bmp|PNG Image|*.png|JPeg Image|*.jpg|Gif Image|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                using (FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile())
                {
                    // Saves the Image in the appropriate ImageFormat based upon the
                    // File type selected in the dialog box.
                    // NOTE that the FilterIndex property is one-based.

                    ImageFormat imageFormat = null;
                    switch (saveFileDialog1.FilterIndex)
                    {
                        case 1:
                            imageFormat = ImageFormat.Bmp;
                            break;
                        case 2:
                            imageFormat = ImageFormat.Png;
                            break;
                        case 3:
                            imageFormat = ImageFormat.Jpeg;
                            break;
                        case 4:
                            imageFormat = ImageFormat.Gif;
                            break;
                        default:
                            throw new NotSupportedException("File extension is not supported");
                    }

                    pictureBoxQRCode.BackgroundImage.Save(fs, imageFormat);
                    fs.Close();
                }
            }
        }

        public void ExportToBmp(string path)
        {

        }

        private void textBoxQRCode_TextChanged(object sender, EventArgs e)
        {
            RenderQrCode();
        }

        private void comboBoxECC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RenderQrCode();
        }

        private void textBoxQRCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                RenderQrCode();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //using (StreamReader sr = iconPath.Text)

            
            {
                int i = 0;
                foreach(string s in iconPath.Lines.ToList())
                {
                    string[] splitCode = s.Split('\t');
                    if (splitCode.Count() > 1)
                    {
                        RenderQrCode(splitCode[0], splitCode[1]);
                    }
                    else
                    {
                        RenderQrCode(s);
                    }

                    i++;
                }
                MessageBox.Show("Created " + i.ToString() + " QRCode");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] fileEntries = Directory.GetFiles(@"G:\QRCodeCreated");
            createVerticalBigImage(fileEntries.Take(5).ToArray(), fileEntries.Skip(5).Take(5).ToArray(), @"G:\QRCodeCreated\01.png");
            createHorizontalBigImage(fileEntries.Take(5).ToArray(), fileEntries.Skip(5).Take(5).ToArray(), @"G:\QRCodeCreated\02.png");
        }

        public Bitmap createVerticalBigImage(string[] listImages, string[] listImages2, string fileName)
        {
            Bitmap bigImage = new Bitmap(2320, 2900, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2320, 2900);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            Bitmap orig1 = createVerticalBigImage(listImages,null);
            Bitmap orig2 = createVerticalBigImage(listImages2, null);

            using (Graphics gr = Graphics.FromImage(bigImage))
            {
                gr.DrawImage(orig1, new Rectangle(0, 0, orig1.Width, orig1.Height));
                gr.DrawImage(orig2, new Rectangle(1160, 0, orig2.Width, orig2.Height));
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public Bitmap createHorizontalBigImage(string[] listImages, string[] listImages2, string fileName)
        {
            Bitmap bigImage = new Bitmap(2900, 2320, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2900, 2320);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            Bitmap orig1 = createHorizontalBigImage(listImages, null);
            Bitmap orig2 = createHorizontalBigImage(listImages2, null);

            using (Graphics gr = Graphics.FromImage(bigImage))
            {
                gr.DrawImage(orig1, new Rectangle(0, 0, orig1.Width, orig1.Height));
                gr.DrawImage(orig2, new Rectangle(0, 1160, orig2.Width, orig2.Height));
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public Bitmap createVerticalBigImage(string[] listImages, string fileName)
        {
            Bitmap bigImage = new Bitmap(1160, 2900, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 1160, 2900);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            int i = 1;
            foreach (string a in listImages)
            {
                BitmapBigFromSmallVertical(a, bigImage, i);
                i++;
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public Bitmap createHorizontalBigImage(string[] listImages, string fileName)
        {
            Bitmap bigImage = new Bitmap(2900, 1160, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2900, 1160);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            int i = 1;
            foreach (string a in listImages)
            {
                BitmapBigFromSmallHorizontal(a, bigImage, i);
                i++;
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public void BitmapBigFromSmallHorizontal(string filename, Bitmap bigImage, int order)
        {
            using (Graphics g = Graphics.FromImage(bigImage))
            {
                Bitmap orig = new Bitmap(filename);
                using (Graphics gr = Graphics.FromImage(bigImage))
                {
                    if (order == 1)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 0, orig.Width, orig.Height));
                    }
                    else if (order == 2)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 580, orig.Width, orig.Height));
                    }
                    else if (order == 3)
                    {
                        gr.DrawImage(orig, new Rectangle(1160, 0, orig.Width, orig.Height));
                    }
                    else if (order == 4)
                    {
                        gr.DrawImage(orig, new Rectangle(1740, 580, orig.Width, orig.Height));
                    }
                    else if (order == 5)
                    {
                        gr.DrawImage(orig, new Rectangle(2320, 0, orig.Width, orig.Height));
                    }
                }
            }
        }

        public void BitmapBigFromSmallVertical(string filename, Bitmap bigImage, int order)
        {
            using (Graphics g = Graphics.FromImage(bigImage))
            {
                Bitmap orig = new Bitmap(filename);
                using (Graphics gr = Graphics.FromImage(bigImage))
                {
                    if (order == 1)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 0, orig.Width, orig.Height));
                    }
                    else if (order == 2)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 580, orig.Width, orig.Height));
                    }
                    else if (order == 3)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 1160, orig.Width, orig.Height));
                    }
                    else if (order == 4)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 1740, orig.Width, orig.Height));
                    }
                    else if (order == 5)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 2320, orig.Width, orig.Height));
                    }
                }
            }
        }

        //Create browse file
        //Create txt to 
        //Create Generate Button
        //Read data from Excel to list<string>
        //Code QRCode to render it

    }
}
