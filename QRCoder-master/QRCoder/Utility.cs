﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace QRCoder
{
    public static class Utility
    {
        public static Bitmap RenderQrCodeNoText(string inputValue)
        {
            string level = "L";
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(level == "L" ? 0 : level == "M" ? 1 : level == "Q" ? 2 : 3);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(inputValue, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        //save to image
                        try
                        {
                            Bitmap a = qrCode.GetGraphic(20, Color.Black, Color.White, null, 0);
                            return a;
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return null;
        }

        public static Bitmap RenderQrCode(string inputValue)
        {
            string level = "L";
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(level == "L" ? 0 : level == "M" ? 1 : level == "Q" ? 2 : 3);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(inputValue, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        //save to image
                        try
                        {
                            Bitmap a = qrCode.GetGraphic(20, Color.Black, Color.White, null, 0);
                            using (Graphics g = Graphics.FromImage(a))
                            {
                                g.DrawString(inputValue, new Font("Tahoma", 30, FontStyle.Bold), Brushes.Black, new Point(20, 20)); // requires font, brush etc
                            }
                            return a;
                        }
                        catch
                        {
                            
                        }
                    }
                }
            }
            return null;
        }

        public static Bitmap RenderQrCode(string inputValue, string displayText)
        {
            string level = "L";
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(level == "L" ? 0 : level == "M" ? 1 : level == "Q" ? 2 : 3);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(inputValue, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        //save to image
                        try
                        {
                            Bitmap a = qrCode.GetGraphic(20, Color.Black, Color.White, null, 0);
                            using (Graphics g = Graphics.FromImage(a))
                            {
                                g.DrawString(displayText, new Font("Tahoma", 30, FontStyle.Bold), Brushes.Black, new Point(70, 520)); // requires font, brush etc
                            }
                            return a;
                        }
                        catch
                        {
                            
                        }
                    }
                }
            }
            return null;
        }

        public static Bitmap createVerticalBigImage(List<Bitmap> listImages, List<Bitmap> listImages2, string fileName)
        {
            Bitmap bigImage = new Bitmap(2320, 2900, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2320, 2900);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            Bitmap orig1 = createVerticalBigImage(listImages, null);
            Bitmap orig2 = createVerticalBigImage(listImages2, null);

            using (Graphics gr = Graphics.FromImage(bigImage))
            {
                gr.DrawImage(orig1, new Rectangle(0, 0, orig1.Width, orig1.Height));
                gr.DrawImage(orig2, new Rectangle(1160, 0, orig2.Width, orig2.Height));
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public static Bitmap createVerticalBigImage(string[] listImages, string[] listImages2, string fileName)
        {
            Bitmap bigImage = new Bitmap(2320, 2900, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2320, 2900);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            Bitmap orig1 = createVerticalBigImage(listImages, null);
            Bitmap orig2 = createVerticalBigImage(listImages2, null);

            using (Graphics gr = Graphics.FromImage(bigImage))
            {
                gr.DrawImage(orig1, new Rectangle(0, 0, orig1.Width, orig1.Height));
                gr.DrawImage(orig2, new Rectangle(1160, 0, orig2.Width, orig2.Height));
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public static Bitmap createHorizontalBigImage(List<Bitmap> listImages, List<Bitmap> listImages2, string fileName)
        {
            Bitmap bigImage = new Bitmap(2900, 2320, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2900, 2320);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            Bitmap orig1 = createHorizontalBigImage(listImages, null);
            Bitmap orig2 = createHorizontalBigImage(listImages2, null);

            using (Graphics gr = Graphics.FromImage(bigImage))
            {
                gr.DrawImage(orig1, new Rectangle(0, 0, orig1.Width, orig1.Height));
                gr.DrawImage(orig2, new Rectangle(0, 1160, orig2.Width, orig2.Height));
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public static Bitmap createHorizontalBigImage(string[] listImages, string[] listImages2, string fileName)
        {
            Bitmap bigImage = new Bitmap(2900, 2320, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2900, 2320);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            Bitmap orig1 = createHorizontalBigImage(listImages, null);
            Bitmap orig2 = createHorizontalBigImage(listImages2, null);

            using (Graphics gr = Graphics.FromImage(bigImage))
            {
                gr.DrawImage(orig1, new Rectangle(0, 0, orig1.Width, orig1.Height));
                gr.DrawImage(orig2, new Rectangle(0, 1160, orig2.Width, orig2.Height));
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public static Bitmap createVerticalBigImage(List<Bitmap> listImages, string fileName)
        {
            Bitmap bigImage = new Bitmap(1160, 2900, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 1160, 2900);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            int i = 1;
            if (listImages != null)
            {
                foreach (Bitmap a in listImages)
                {
                    BitmapBigFromSmallVertical(a, bigImage, i);
                    i++;
                }


            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public static Bitmap createVerticalBigImage(string[] listImages, string fileName)
        {
            Bitmap bigImage = new Bitmap(1160, 2900, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 1160, 2900);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            int i = 1;
            foreach (string a in listImages)
            {
                BitmapBigFromSmallVertical(a, bigImage, i);
                i++;
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public static Bitmap createHorizontalBigImage(List<Bitmap> listImages, string fileName)
        {
            Bitmap bigImage = new Bitmap(2900, 1160, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2900, 1160);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            int i = 1;
            if (listImages != null)
            {
                foreach (Bitmap a in listImages)
                {
                    BitmapBigFromSmallHorizontal(a, bigImage, i);
                    i++;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public static Bitmap createHorizontalBigImage(string[] listImages, string fileName)
        {
            Bitmap bigImage = new Bitmap(2900, 1160, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 2900, 1160);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            int i = 1;
            foreach (string a in listImages)
            {
                BitmapBigFromSmallHorizontal(a, bigImage, i);
                i++;
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }

        public static void BitmapBigFromSmallHorizontal(Bitmap orig, Bitmap bigImage, int order)
        {
            using (Graphics g = Graphics.FromImage(bigImage))
            {
                using (Graphics gr = Graphics.FromImage(bigImage))
                {
                    if (order == 1)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 0, orig.Width, orig.Height));
                    }
                    else if (order == 2)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 580, orig.Width, orig.Height));
                    }
                    else if (order == 3)
                    {
                        gr.DrawImage(orig, new Rectangle(1160, 0, orig.Width, orig.Height));
                    }
                    else if (order == 4)
                    {
                        gr.DrawImage(orig, new Rectangle(1740, 580, orig.Width, orig.Height));
                    }
                    else if (order == 5)
                    {
                        gr.DrawImage(orig, new Rectangle(2320, 0, orig.Width, orig.Height));
                    }
                }
            }
        }

        public static void BitmapBigFromSmallHorizontal(string filename, Bitmap bigImage, int order)
        {
            using (Graphics g = Graphics.FromImage(bigImage))
            {
                Bitmap orig = new Bitmap(filename);
                using (Graphics gr = Graphics.FromImage(bigImage))
                {
                    if (order == 1)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 0, orig.Width, orig.Height));
                    }
                    else if (order == 2)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 580, orig.Width, orig.Height));
                    }
                    else if (order == 3)
                    {
                        gr.DrawImage(orig, new Rectangle(1160, 0, orig.Width, orig.Height));
                    }
                    else if (order == 4)
                    {
                        gr.DrawImage(orig, new Rectangle(1740, 580, orig.Width, orig.Height));
                    }
                    else if (order == 5)
                    {
                        gr.DrawImage(orig, new Rectangle(2320, 0, orig.Width, orig.Height));
                    }
                }
            }
        }

        public static void BitmapBigFromSmallVertical(Bitmap orig, Bitmap bigImage, int order)
        {
            using (Graphics g = Graphics.FromImage(bigImage))
            {
                using (Graphics gr = Graphics.FromImage(bigImage))
                {
                    if (order == 1)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 0, orig.Width, orig.Height));
                    }
                    else if (order == 2)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 580, orig.Width, orig.Height));
                    }
                    else if (order == 3)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 1160, orig.Width, orig.Height));
                    }
                    else if (order == 4)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 1740, orig.Width, orig.Height));
                    }
                    else if (order == 5)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 2320, orig.Width, orig.Height));
                    }
                }
            }
        }

        public static void BitmapBigFromSmallVertical(string filename, Bitmap bigImage, int order)
        {
            using (Graphics g = Graphics.FromImage(bigImage))
            {
                Bitmap orig = new Bitmap(filename);
                using (Graphics gr = Graphics.FromImage(bigImage))
                {
                    if (order == 1)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 0, orig.Width, orig.Height));
                    }
                    else if (order == 2)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 580, orig.Width, orig.Height));
                    }
                    else if (order == 3)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 1160, orig.Width, orig.Height));
                    }
                    else if (order == 4)
                    {
                        gr.DrawImage(orig, new Rectangle(580, 1740, orig.Width, orig.Height));
                    }
                    else if (order == 5)
                    {
                        gr.DrawImage(orig, new Rectangle(0, 2320, orig.Width, orig.Height));
                    }
                }
            }
        }

        public static Bitmap createVerticalBigImageWithText(List<Bitmap> listImages, List<string> listLocationNames, string fileName)
        {
            Bitmap bigImage = new Bitmap(1160, 2900, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            using (Graphics graph = Graphics.FromImage(bigImage))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 1160, 2900);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            int i = 1;
            if (listImages != null)
            {
                foreach (Bitmap a in listImages)
                {
                    BitmapBigFromSmallVertical(a, bigImage, i);
                    i++;
                }
            }

            using (Graphics g = Graphics.FromImage(bigImage))
            {
                g.DrawString(listLocationNames[0], new Font("Tahoma", 20, FontStyle.Bold), Brushes.Black, new Point(10, 10)); // requires font, brush etc
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                bigImage.Save(fileName);
            }
            return bigImage;
        }


    }
}
