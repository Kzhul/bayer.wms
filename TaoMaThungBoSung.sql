﻿--Script gen auto của Số Lô đến số thùng = 999
--Input = số lô, số thùng muốn in bổ sung
DECLARE @ProductLot VARCHAR(9) = '171206R02' -- số lô
DECLARE @BonusNumber INT = 200 -- số thùng muốn in bổ sung



----------------------PROCESSING-----------------------------
DECLARE @MaxLabelCartonBarcode VARCHAR(20) = ''
DECLARE @CurrentNumber INT = 0
DECLARE @MaxNumber INT = 0
UPDATE ProductionPlans SET MaxLabelCartonBarcode = 999 WHERE ProductLot = @ProductLot
SET @MaxLabelCartonBarcode = (SELECT TOP 1 BarCode FROM CartonBarcodes WHERE ProductLot = @ProductLot ORDER BY BarCode DESC)
SET @CurrentNumber         = CONVERT(INT, RIGHT(@MaxLabelCartonBarcode,3))
SET @MaxNumber             = @CurrentNumber + @BonusNumber
SET @CurrentNumber         = @CurrentNumber + 1

PRINT(@CurrentNumber)
PRINT(@MaxNumber)

WHILE @CurrentNumber <= @MaxNumber
BEGIN
   INSERT INTO [dbo].[CartonBarcodes]
           ([Barcode]
           ,[ProductLot]
           ,[PalletCode]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedBySitemapID]
           ,[CreatedDateTime]
           ,[UpdatedBy]
           ,[UpdatedBySitemapID]
           ,[UpdatedDateTime])
	SELECT [Barcode] = @ProductLot + 'C' + RIGHT('000' + CONVERT(NVARCHAR(5),@CurrentNumber),3)
           ,[ProductLot] = @ProductLot
           ,[PalletCode] = NULL
           ,[Status] = 'N'
           ,[CreatedBy] = 1
           ,[CreatedBySitemapID] = 0
           ,[CreatedDateTime] = GETDATE()
           ,[UpdatedBy] = 1
           ,[UpdatedBySitemapID] = 0
           ,[UpdatedDateTime] = GETDATE()
   PRINT(@ProductLot + 'C' + RIGHT('000' + CONVERT(NVARCHAR(5),@CurrentNumber),3))
   SET @CurrentNumber = @CurrentNumber + 1;
END;

--SELECT * FROM CartonBarcodes WITH (NOLOCK) WHERE ProductLot = @ProductLot ORDER BY BarCode











