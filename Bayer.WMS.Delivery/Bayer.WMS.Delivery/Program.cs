﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Bayer.WMS.Delivery
{
    static class Program
    {
        public static string AppPath;
        public static string ConnStr;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            Application.Run(new DeliveryView());
        }
    }
}