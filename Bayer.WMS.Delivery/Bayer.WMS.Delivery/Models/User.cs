﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Bayer.WMS.Delivery.Models
{
    public class User
    {
        public int UserID { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public static User Load(string userName)
        {
            User user;

            using (var conn = new SqlConnection(Program.ConnStr))
            {
                conn.Open();

                try
                {
                    string query = "SELECT UserID, Username, FirstName, LastName FROM dbo.Users WHERE Username = '{0}'";
                    using (var cmd = new SqlCommand(String.Format(query, userName), conn))
                    {
                        cmd.CommandType = CommandType.Text;

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            var dt = new DataTable();
                            adapt.Fill(dt);

                            if (dt.Rows.Count == 0)
                                throw new Exception("Không tìm thấy nhân viên");

                            user = new User
                            {
                                UserID = int.Parse(dt.Rows[0]["UserID"].ToString()),
                                Username = dt.Rows[0]["Username"].ToString(),
                                FirstName = dt.Rows[0]["FirstName"].ToString(),
                                LastName = dt.Rows[0]["LastName"].ToString()
                            };
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }

            return user;
        }
    }
}
