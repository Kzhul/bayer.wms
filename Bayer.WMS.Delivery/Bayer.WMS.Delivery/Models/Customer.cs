﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Bayer.WMS.Delivery.Models
{
    public class Company
    {
        public string CompanyCode { get; set; }

        public string CompanyName { get; set; }

        public static Company Load(string companyCode)
        {
            Company company;

            using (var conn = new SqlConnection(Program.ConnStr))
            {
                conn.Open();

                try
                {
                    string query = "SELECT TOP 1 CompanyCode, CompanyName FROM dbo.DeliveryOrderDetails WHERE CompanyCode = '{0}'";
                    using (var cmd = new SqlCommand(String.Format(query, companyCode), conn))
                    {
                        cmd.CommandType = CommandType.Text;

                        using (var adapt = new SqlDataAdapter(cmd))
                        {
                            var dt = new DataTable();
                            adapt.Fill(dt);

                            if (dt.Rows.Count == 0)
                                throw new Exception("Không tìm thấy khách hàng");

                            company = new Company
                            {
                                CompanyCode = dt.Rows[0]["CompanyCode"].ToString(),
                                CompanyName = dt.Rows[0]["CompanyName"].ToString()
                            };
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }

            return company;
        }
    }
}
