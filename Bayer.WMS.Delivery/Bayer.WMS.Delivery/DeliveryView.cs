﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DataCollection;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;
using Bayer.WMS.Delivery.Models;
using System.Data.SqlClient;

namespace Bayer.WMS.Delivery
{
    public partial class DeliveryView : Form
    {
        private bool _isDeleteMode = false;
        private bool _isScanPallet = false;
        private bool _isScanRest = false;
        private string _preHint;
        private string _palletCode;
        private bool _isOffline = true;
        private byte[] _bytes = Encoding.ASCII.GetBytes("BayerWMS");
        private List<string> _rest = new List<string>();
        private DataTable _dt;
        private BarcodeReader _barcodeReader;
        private User _user;
        private Company _company;
        private Regex _employeeBarcodeRegex = new Regex(@"^NV_", RegexOptions.IgnoreCase);
        private Regex _palletBarcodeRegex = new Regex(@"^P[A-Z][01]\d\d{2}\d{5}$", RegexOptions.IgnoreCase);
        private Regex _cartonBarcodeRegex = new Regex(@"^\d{6}\w\d{2}C\d{3}$", RegexOptions.IgnoreCase);
        private Regex CartonBarcodeRegex = new Regex(@"^\d{6}\w\d{2}C\d{3}$", RegexOptions.IgnoreCase);
        private Regex CartonBarcode2Regex = new Regex(@"^SH\d{8}C\d{3}$", RegexOptions.IgnoreCase);
        private Regex CartonBarcode3Regex = new Regex(@"\d{6}\w\d{2}(C|c)\w{3}$", RegexOptions.IgnoreCase);
        private Regex _productBarcodeRegex = new Regex(@"^\d{6}\w\d{2}\d{5}", RegexOptions.IgnoreCase);
        private Regex _productBarcode2Regex = new Regex(@"\d{10}", RegexOptions.IgnoreCase);
        private Regex _productBarcode3Regex = new Regex(@"\d{26}", RegexOptions.IgnoreCase);
        private Regex _customerBarcodeRegex = new Regex(@"^KH_", RegexOptions.IgnoreCase);

        public DeliveryView()
        {
            InitializeComponent();
            InitDataGridView();
            LoadConfig();
            //CheckConnection();

            try
            {
                _barcodeReader = new BarcodeReader();
                _barcodeReader.ThreadedRead(true);
                _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;
            }
            catch
            {
                _barcodeReader = null;
            }

            _dt = new DataTable();
            _dt.TableName = "Delivery";
            _dt.Columns.Add("EmployeeName", typeof(string));
            _dt.Columns.Add("CustomerCode", typeof(string));
            _dt.Columns.Add("Barcode", typeof(string));
            _dt.Columns.Add("Type", typeof(string));
            _dt.Columns.Add("DisplayType", typeof(string));
            _dt.Columns.Add("ProductLot", typeof(string));
            _dt.Columns.Add("Rest", typeof(string));

            bdsDelivery.DataSource = _dt;
        }

        private void _barcodeReader_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            try
            {
                string barcode = bre.strDataBuffer;
                ProcessBarcode(barcode);
            }
            finally
            {
                txtBarcode.Focus();
                txtBarcode.SelectAll();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ProcessBarcode(txtBarcode.Text);
        }

        private void mniConfirm_Click(object sender, EventArgs e)
        {
            using (var sw = new StreamWriter(Path.Combine(Program.AppPath, "Delivery.txt"), true))
            {
                foreach (DataRow row in _dt.Rows)
                {
                    sw.WriteLine("{0}-{1}-{2}-{3}-{4}-{5:yyyy.MM.dd}-{6}",
                        row["EmployeeName"],
                        row["CustomerCode"],
                        row["Barcode"],
                        row["Type"],
                        row["ProductLot"],
                        DateTime.Today,
                        row["Rest"]);
                }
            }

            _dt.Clear();
        }

        private void mniDelete_Click(object sender, EventArgs e)
        {
            _isDeleteMode = true;

            _preHint = lblHints.Text;
            lblHints.Text = "Quét mã QR cần hủy";
            lblHints.BackColor = Color.Red;

            mniDelete.Enabled = false;
            mniEndDelete.Enabled = true;
            mniScanPallet.Enabled = false;
        }

        private void mniEndDelete_Click(object sender, EventArgs e)
        {
            _isDeleteMode = false;

            lblHints.Text = _preHint;
            lblHints.BackColor = Color.FromArgb(0, 192, 0);

            mniDelete.Enabled = true;
            mniEndDelete.Enabled = false;
            mniScanPallet.Enabled = true;
        }

        private void mniScanPallet_Click(object sender, EventArgs e)
        {
            _isScanRest = true;

            mniScanPallet.Enabled = false;
            mniEndScanPallet.Enabled = true;
            mniDelete.Enabled = false;
        }

        private void mniEndScanPallet_Click(object sender, EventArgs e)
        {
            if (_rest.Count > 0)
            {
                _dt.Rows.Add(lblEmployeeName.Text, lblCustomerCode.Text, _palletCode, "P", "Pallet", _rest[0].Substring(0, 9), String.Join(",", _rest.ToArray()));
            }
            _rest = new List<string>();
            _isScanRest = false;

            mniScanPallet.Enabled = true;
            mniEndScanPallet.Enabled = false;
            mniDelete.Enabled = true;
        }

        private void mniExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void InitDataGridView()
        {
            var dtgStyle = new DataGridTableStyle { MappingName = "Delivery" };

            var colBarcode = new ColumnStyle(1) { Width = 170, HeaderText = "Barcode", MappingName = "Barcode" };
            dtgStyle.GridColumnStyles.Add(colBarcode);

            var colType = new ColumnStyle(1) { Width = 80, HeaderText = "Loại", MappingName = "DisplayType" };
            dtgStyle.GridColumnStyles.Add(colType);

            dtgDelivery.TableStyles.Add(dtgStyle);
        }

        private void LoadConfig()
        {
            Program.AppPath = Path.GetDirectoryName(Assembly.GetCallingAssembly().GetName().CodeBase);

            string configPath = Path.Combine(Program.AppPath, "AppConfig.txt");

            var config = new Dictionary<string, string>();
            using (var sr = new StreamReader(configPath))
            {
                while (!sr.EndOfStream)
                {
                    string[] tmp = sr.ReadLine().Split(':');
                    if (tmp.Length == 2)
                        config.Add(tmp[0].Trim(), tmp[1].Trim());
                }
            }

            string[] connStrComponent = config["ConnectionString"].Split(';');
            string server = connStrComponent.FirstOrDefault(p => p.StartsWith("Server")).Replace("Server=", String.Empty).Trim();
            string database = connStrComponent.FirstOrDefault(p => p.StartsWith("Database")).Replace("Database=", String.Empty).Trim();
            string userid = connStrComponent.FirstOrDefault(p => p.StartsWith("Uid")).Replace("Uid=", String.Empty).Trim();
            string pwd = connStrComponent.FirstOrDefault(p => p.StartsWith("Pwd")).Replace("Pwd=", String.Empty).Trim();
            string connTimeout = connStrComponent.FirstOrDefault(p => p.StartsWith("Connection Timeout")).Replace("Connection Timeout=", String.Empty).Trim();

            //string connStr = String.Format("Server={0};Database={1};Uid={2};pwd={3};",
            //    server, database, AESDecrypt(userid), AESDecrypt(pwd));
            Program.ConnStr = config["ConnectionString"];
        }

        private void ProcessBarcode(string barcode)
        {
            if (_barcodeReader != null)
                _barcodeReader.BarcodeRead -= _barcodeReader_BarcodeRead;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                barcode = barcode.Replace("\r", String.Empty).Replace("\n", String.Empty);
                txtBarcode.Text = barcode;

                if (String.IsNullOrEmpty(barcode.Trim()))
                    return;

                //// step 1: scan employee
                if (_employeeBarcodeRegex.IsMatch(barcode))
                    ProcessEmployeeBarcode(barcode);
                //// step 2: scan customer
                else if (_customerBarcodeRegex.IsMatch(barcode))
                    ProcessCustomerBarcode(barcode);
                //// step 3: scan pallet/carton/product
                else if (!_isDeleteMode)
                    ProcessProductBarcode(barcode);
                else
                    ProcessDelete(barcode);
            }
            catch (Exception ex)
            {
                WriteLog(barcode);
                WriteLog(ex);
                MessageBox.Show(ex.Message, "Lỗi");
            }
            finally
            {
                if (_barcodeReader != null)
                    _barcodeReader.BarcodeRead += _barcodeReader_BarcodeRead;

                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessEmployeeBarcode(string barcode)
        {
            if (_isScanPallet)
                throw new Exception("Quét không đúng bước");

            barcode = barcode.Remove(0, 3);
            if (!_isOffline)
            {
                _user = User.Load(barcode);
                lblEmployeeName.Text = String.Format("{0} {1}", _user.LastName, _user.FirstName);
            }
            else
                lblEmployeeName.Text = barcode;

            lblHints.Text = "Quét mã khách hàng";
        }

        private void ProcessCustomerBarcode(string barcode)
        {
            if (_isScanPallet)
                throw new Exception("Quét không đúng bước");

            barcode = barcode.Remove(0, 3);
            if (!_isOffline)
            {
                _company = Company.Load(barcode.Remove(0, 3));
                lblCustomerCode.Text = _company.CompanyName;
            }
            else
                lblCustomerCode.Text = barcode;

            _dt.Rows.Clear();
            lblHints.Text = "Quét mã pallet / thùng / lẻ";
        }

        private void ProcessProductBarcode(string barcode)
        {
            if (String.IsNullOrEmpty(lblCustomerCode.Text) || lblCustomerCode.Text == "[Tên khách hàng]")
                throw new Exception("Chưa quét mã khách hàng");

            string type = String.Empty;
            if (!_isScanPallet && !_isScanRest)
            {
                if (_palletBarcodeRegex.IsMatch(barcode))
                {
                    type = "P";
                    _palletCode = barcode;
                    _isScanPallet = true;
                    _rest.Clear();

                    lblHints.Text = "Quét mã bao / xô / can";
                    return;
                }
                else if (CartonBarcodeRegex.IsMatch(barcode)
                    || CartonBarcode2Regex.IsMatch(barcode)
                    || CartonBarcode3Regex.IsMatch(barcode)
                    )
                {
                    type = "C";
                }
                else
                {
                    try
                    {
                        barcode = barcode.Split('|')[0];
                        barcode = barcode.StartsWith("SP") ? barcode.Remove(0, 2) : barcode;
                        barcode = AESDecrypt(barcode);
                        if (_productBarcodeRegex.IsMatch(barcode) 
                            || _productBarcode2Regex.IsMatch(barcode)
                            || _productBarcode3Regex.IsMatch(barcode)
                            )
                            type = "E";
                        else
                            throw new Exception();
                    }
                    catch
                    {
                        throw new Exception("Mã không hợp lệ");
                    }
                }

                if (_dt.Select(String.Format("Barcode = '{0}' AND Type = '{1}'", barcode, type)).Length > 0)
                    throw new Exception("Mã QR đã được quét, vui lòng kiểm tra lại");
                else
                {
                    if (type == "C")
                        lblCarton.Text = (int.Parse(lblCarton.Text) + 1).ToString();
                    else
                        lblEach.Text = (int.Parse(lblEach.Text) + 1).ToString();

                    _dt.Rows.Add(lblEmployeeName.Text, lblCustomerCode.Text, barcode, type, type == "C" ? "Thùng" : "Lẻ", barcode.Substring(0, 9), String.Empty);
                }
            }
            else if(_isScanPallet)
            {
                try
                {
                    barcode = barcode.Split('|')[0];
                    barcode = barcode.StartsWith("SP") ? barcode.Remove(0, 2) : barcode;
                    barcode = AESDecrypt(barcode);
                    if (!_productBarcodeRegex.IsMatch(barcode) && !_productBarcode2Regex.IsMatch(barcode) && !_productBarcode3Regex.IsMatch(barcode))
                        throw new Exception();

                    _isScanPallet = false;
                    _dt.Rows.Add(lblEmployeeName.Text, lblCustomerCode.Text, _palletCode, "P", "Pallet", barcode.Substring(0, 9), String.Empty);
                    lblHints.Text = "Quét mã pallet / thùng / lẻ";
                }
                catch
                {
                    throw new Exception("Mã không hợp lệ");
                }
            }
            else if (_isScanRest)
            {
                try
                {
                    barcode = barcode.Split('|')[0];
                    barcode = barcode.StartsWith("SP") ? barcode.Remove(0, 2) : barcode;
                    barcode = AESDecrypt(barcode);
                    if (!_productBarcodeRegex.IsMatch(barcode) && !_productBarcode2Regex.IsMatch(barcode) && !_productBarcode3Regex.IsMatch(barcode))
                        throw new Exception();

                    _rest.Add(barcode);
                }
                catch
                {
                    throw new Exception("Mã không hợp lệ");
                }
            }
        }

        private void ProcessDelete(string barcode)
        {
            string type;
            if (_palletBarcodeRegex.IsMatch(barcode))
                type = "P";
            else if (CartonBarcodeRegex.IsMatch(barcode)
                    || CartonBarcode2Regex.IsMatch(barcode)
                    || CartonBarcode3Regex.IsMatch(barcode)
                    )
            {
                type = "C";
            }
            else
            {
                try
                {
                    barcode = barcode.Split('|')[0];
                    barcode = barcode.StartsWith("SP") ? barcode.Remove(0, 2) : barcode;
                    barcode = AESDecrypt(barcode);
                    if (_productBarcodeRegex.IsMatch(barcode) || _productBarcode2Regex.IsMatch(barcode) || _productBarcode3Regex.IsMatch(barcode))
                        type = "E";
                    else
                        throw new Exception();
                }
                catch
                {
                    throw new Exception("Mã không hợp lệ");
                }
            }

            var rows = _dt.Select(String.Format("Barcode = '{0}' AND Type = '{1}'", barcode, type));
            if (rows.Length > 0)
                _dt.Rows.Remove(rows[0]);
        }

        private void CheckConnection()
        {
            using (var conn = new SqlConnection(Program.ConnStr))
            {
                try
                {
                    conn.Open();
                    conn.Close();
                }
                catch
                {
                    MessageBox.Show("Không kết nối được đến server. Chuyển sang chế độ offline mode.");
                    _isOffline = true;
                }
            }
        }

        private string AESDecrypt(string cipherText)
        {
            if (String.IsNullOrEmpty(cipherText))
                return String.Empty;

            var cryptoProvider = new DESCryptoServiceProvider();
            var memoryStream = new MemoryStream(Convert.FromBase64String(cipherText));
            var cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(_bytes, _bytes), CryptoStreamMode.Read);
            var reader = new StreamReader(cryptoStream);

            return reader.ReadToEnd();
            //return cipherText;
        }


        public static void WriteLog(Exception ex)
        {
            //Write log
            using (var sw = new StreamWriter(Path.Combine(Program.AppPath, "Error.txt"), true))
            {
                sw.WriteLine(String.Format("{0:yyyy.MM.dd HH:mm:ss}", DateTime.Now));
                sw.WriteLine("================Exception===================");
                sw.WriteLine(ex.Message);
                sw.WriteLine(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    sw.WriteLine("================Exception.InnerException===================");
                    sw.WriteLine(ex.InnerException.Message);
                    sw.WriteLine(ex.InnerException.StackTrace);

                    if (ex.InnerException.InnerException != null)
                    {
                        sw.WriteLine("================Exception.InnerException.InnerException===================");
                        sw.WriteLine(ex.InnerException.InnerException.Message);
                        sw.WriteLine(ex.InnerException.InnerException.StackTrace);

                        if (ex.InnerException.InnerException.InnerException != null)
                        {
                            sw.WriteLine("================InnerException.InnerException.InnerException===================");
                            sw.WriteLine(ex.InnerException.InnerException.InnerException.Message);
                            sw.WriteLine(ex.InnerException.InnerException.InnerException.StackTrace);
                        }
                    }
                }
                sw.WriteLine();
                sw.WriteLine();
            }
        }

        public static void WriteLog(string ex)
        {
            //Write log
            using (var sw = new StreamWriter(Path.Combine(Program.AppPath, "Error.txt"), true))
            {
                sw.WriteLine(String.Format("{0:yyyy.MM.dd HH:mm:ss}", DateTime.Now) + ": " + ex);
                sw.WriteLine();
            }
        }
    }
}