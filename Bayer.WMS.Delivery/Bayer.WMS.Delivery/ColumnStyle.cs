using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace Bayer.WMS.Delivery
{
    public delegate void CheckCellEventHandler(object sender, DataGridEnableEventArgs e);

    public class DataGridEnableEventArgs : EventArgs
    {
        private int _column;
        private int _row;
        private int _meetsCriteria;

        public DataGridEnableEventArgs(int row, int col, bool val)
        {
            _row = row;
            _column = col;
        }

        public int Column
        {
            get { return _column; }
            set { _column = value; }
        }

        public int Row
        {
            get { return _row; }
            set { _row = value; }
        }

        public int MeetsCriteria
        {
            get { return _meetsCriteria; }
            set { _meetsCriteria = value; }
        }
    }

    public class ColumnStyle : DataGridTextBoxColumn
    {
        public event CheckCellEventHandler CheckCellEquals;

        private int _col;

        public ColumnStyle(int column)
        {
            _col = column;
        }

        protected override void Paint(Graphics g, Rectangle Bounds, CurrencyManager Source, int RowNum, Brush BackBrush, Brush ForeBrush, bool AlignToRight)
        {
            bool enabled = true;

            //out third event handler , we are going to use this draw a rectangle around our
            //cell so we call it after we have called the base class paint method
            if (CheckCellEquals != null)
            {
                DataGridEnableEventArgs e = new DataGridEnableEventArgs(RowNum, _col, enabled);
                CheckCellEquals(this, e);
                if (e.MeetsCriteria == 2)
                {
                    ForeBrush = new SolidBrush(Color.Blue);
                    base.Paint(g, Bounds, Source, RowNum, BackBrush, ForeBrush, AlignToRight);
                    g.DrawRectangle(new Pen(Color.Blue, 2), Bounds.X + 1, Bounds.Y + 1, Bounds.Width - 2, Bounds.Height - 2);
                }
                else if (e.MeetsCriteria == 1)
                {
                    ForeBrush = new SolidBrush(Color.Orange);
                    base.Paint(g, Bounds, Source, RowNum, BackBrush, ForeBrush, AlignToRight);
                    g.DrawRectangle(new Pen(Color.Orange, 2), Bounds.X + 1, Bounds.Y + 1, Bounds.Width - 2, Bounds.Height - 2);
                }
                else
                    base.Paint(g, Bounds, Source, RowNum, BackBrush, ForeBrush, AlignToRight);
            }
            else
                base.Paint(g, Bounds, Source, RowNum, BackBrush, ForeBrush, AlignToRight);
        }

    }
}
